#ifndef DISKCHECKER_H
#define DISKCHECKER_H

#include <string>

using namespace std;

namespace Common
{
	class DiskChecker
	{
	public:
		// 10 M;
		static bool isDiskFull(const string& path, int maxBytes = 10 * 1024 * 1024);
	};
}

#endif // DISKCHECKER_H
