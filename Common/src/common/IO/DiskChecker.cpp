#include "DiskChecker.h"
#if WIN32
#include <Windows.h>
#elif __APPLE__
#include <sys/param.h>
#include <sys/mount.h>
#else
#include <sys/vfs.h>
#endif

namespace Common
{
	bool DiskChecker::isDiskFull(const string& path, int maxBytes)
	{
#if WIN32
		ULARGE_INTEGER available, total, free;
		if (GetDiskFreeSpaceEx(path.c_str(), &available, &total, &free))
		{
			return (available.QuadPart < maxBytes);
		}
#else
		struct statfs ds; 
		if(statfs(path.c_str(), &ds) >= 0) 
		{ 
			return (ds.f_bfree < (int64_t)maxBytes/1024L);	// unit: k
		}     
#endif
		return false;
	}
}
