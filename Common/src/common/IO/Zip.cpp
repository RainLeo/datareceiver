#include "Zip.h"
#include "../exception/Exception.h"
#include "../system/Convert.h"
#include "Directory.h"
#include "File.h"
#include "Path.h"
#include <sys/stat.h>
#include <fcntl.h>
#if WIN32
#include <Windows.h>
#include <memory.h>
#include <fcntl.h>
#include <io.h>
#include <direct.h>
#elif __APPLE__
#include <sys/param.h>
#include <sys/mount.h>
#include <libgen.h>
#include <dirent.h>
#include <unistd.h>
#else
#include <sys/vfs.h>
#include <libgen.h>
#include <limits.h>
#include <unistd.h>
#include <dirent.h>
#endif

int zipRead(void * context, char * buffer, int len)
{
    zip_file* zf = static_cast<zip_file*>(context);
    if(zf != NULL)
    {
        return (int)zip_fread(zf, buffer, len);
    }
    return 0;
}
int zipClose(void * context)
{
    zip_file* zf = static_cast<zip_file*>(context);
    if(zf != NULL)
    {
        zip_fclose(zf);
    }
    return 0;
}

namespace Common
{
	Zip::Zip(const string& filename) : Zip(filename.c_str())
	{
	}
    Zip::Zip(const char* filename)
    {
        if(filename == NULL)
            throw ArgumentException("filename");
        
        //Open the ZIP archive
        int err = 0;
        _zip = zip_open(filename, 0, &err);
    }

	Zip::~Zip()
	{
        //And close the archive
        if(isValid())
        {
            zip_close(_zip);
            _zip = NULL;
        }
	}
    
    bool Zip::isValid() const
    {
        return _zip != NULL;
    }
    zip_file* Zip::open(const char* filename) const
    {
        if(filename == NULL)
            throw ArgumentException("filename");
        
        return isValid() ? zip_fopen(_zip, filename, 0) : NULL;
    }
    zip_file* Zip::open(const string& filename) const
    {
        return open(filename.c_str());
    }
    void Zip::close(zip_file* zf)
    {
        if(zf != NULL)
        {
            zip_fclose(zf);
        }
    }
    
    bool Zip::read(const char* filename, ByteArray& buffer)
    {
        zip_file* zf = open(filename);
        if(zf)
        {
            const int count = 1024;
            byte temp[count];
            zip_int64_t len = 0;
            do
            {
                len = zip_fread(zf, temp, count);
                if(len > 0)
                {
                    buffer.addRange(temp, (uint)len);
                }
            }while(len > 0);
            
            close(zf);
            return true;
        }
        return false;
    }
    bool Zip::read(const string& zipfile, ByteArray& buffer)
    {
        return read(zipfile.c_str(), buffer);
    }

	bool Zip::extract(const char* zipfile, const char* path)
	{
		if (zipfile == NULL)
			throw ArgumentNullException("zipfile");
		if (path == NULL)
			throw ArgumentNullException("path");

		if (!Directory::exists(path))
		{
			Directory::createDirectory(path);
		}

		//Open the ZIP archive
		int err = 0;
		zip *za = zip_open(zipfile, 0, &err);
		if (za == NULL)
			return false;

		for (int i = 0; i < zip_get_num_entries(za, 0); i++)
		{
			struct zip_stat sb;
			if (zip_stat_index(za, i, 0, &sb) == 0)
			{
				zip_int64_t len = strlen(sb.name);
				string fullFileName = Path::combine(path, sb.name);
				if (sb.name[len - 1] == '/')
				{
					Directory::createDirectory(fullFileName);
				}
				else
				{
					struct zip_file *zf = zip_fopen_index(za, i, 0);
					if (!zf)
						continue;

                    int fd = ::open(fullFileName.c_str(), O_RDWR | O_TRUNC | O_CREAT, 0644);
					if (fd < 0)
						continue;

					char buf[1024];
					zip_uint64_t sum = 0;
					while (sum != sb.size)
					{
						len = zip_fread(zf, buf, sizeof(buf));
						if (len < 0)
							continue;

						write(fd, buf, (size_t)len);
						sum += len;
					}
                    ::close(fd);
					zip_fclose(zf);
				}
			}
			else
			{
				return false;
			}
		}

		//And close the archive
		zip_close(za);
		return true;
	}
	bool Zip::extract(const string& zipfile, const string& path)
	{
		return extract(zipfile.c_str(), path.c_str());
	}

    bool Zip::compress(const char* path, const char* zipfile)
    {
        if (path == NULL)
            throw ArgumentNullException("path");
        if (zipfile == NULL)
            throw ArgumentNullException("zipfile");
        
        if (!Directory::exists(path))
        {
            return false;
        }
        
        StringArray files;
        if(!Directory::getFiles(path, "*", SearchOption::AllDirectories, files))
            return false;
        
        return compressFile(path, files, zipfile);
    }
    bool Zip::compress(const string& path, const string& zipfile)
    {
        return compress(path.c_str(), zipfile.c_str());
    }
    bool Zip::compressFile(const char* filename, const char* zipfile)
    {
        if (filename == NULL)
            throw ArgumentNullException("filename");
        if (zipfile == NULL)
            throw ArgumentNullException("zipfile");
        
        StringArray files(filename, nullptr);
        string path = Path::getDirectoryName(filename);
        return compressFile(path, files, zipfile);
    }
    bool Zip::compressFile(const string& filename, const string& zipfile)
    {
        return compressFile(filename.c_str(), zipfile.c_str());
    }
    bool Zip::compressFile(const string& cpath, const StringArray& filenames, const string& zipfile)
    {
        //Open the ZIP archive
        int err = 0;
        zip *za = zip_open(zipfile.c_str(), ZIP_CREATE, &err);
        if (za == NULL)
            return false;
        
        for (uint i=0; i<filenames.count(); i++)
        {
            string filename = filenames[i];
            if(File::exists(filename))
            {
                zip_source* zs = zip_source_file(za, filename.c_str(), 0, 0);
                if(zs != nullptr)
                {
                    string cfilename = filename;
                    Convert::replaceStr(cfilename, cpath, "");
                    if(cfilename.length() > 0 &&
                       cfilename[0] == Path::DirectorySeparatorChar)
                    {
                        cfilename = cfilename.substr(1, cfilename.length());
                    }
                    if(zip_add(za, cfilename.c_str(), zs) < 0)
                    {
                        zip_close(za);
                        return false;
                    }
                }
            }
        }
        
        //And close the archive
        zip_close(za);
        return true;
    }
}
