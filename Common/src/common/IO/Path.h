#ifndef PATH_H
#define PATH_H

#include <string>

using namespace std;

namespace Common
{
	class Path
	{
	public:
		static bool isPathRooted(const char* path);
		static bool isPathRooted(const string& path);
		static string combine(const char* path1, const char* path2);
		static string combine(const string& path1, const string& path2);
		static string getFileName(const char* path);
		static string getFileName(const string& path);
		static string getFileNameWithoutExtension(const char* path);
		static string getFileNameWithoutExtension(const string& path);
		static string getDirectoryName(const char* path);
		static string getDirectoryName(const string& path);
		static string getExtension(const char* path);
		static string getExtension(const string& path);

	private:
		static bool hasIllegalCharacters(const char* path, bool checkAdditional);
		static string combineNoChecks(const char* path1, const char* path2);
		static void checkInvalidPathChars(const char* path, bool checkAdditional = false);

	public:
#if WIN32
		const static char DirectorySeparatorChar = '\\';
#else
		const static char DirectorySeparatorChar = '/';
#endif

	private:
		const static char AltDirectorySeparatorChar = '/';
		const char* InvalidFileNameChars = new char[41] {
				'"', '<', '>', '|', '\0', '\x0001', '\x0002', '\x0003', '\x0004', '\x0005', '\x0006', '\a', '\b', '\t', '\n', '\v',
				'\f', '\r', '\x000e', '\x000f', '\x0010', '\x0011', '\x0012', '\x0013', '\x0014', '\x0015', '\x0016', '\x0017', '\x0018', '\x0019', '\x001a', '\x001b',
				'\x001c', '\x001d', '\x001e', '\x001f', ':', '*', '?', '\\', '/'
		};
		const static  int MAX_DIRECTORY_PATH = 0xf8;
		//const static  int MAX_PATH = 260;
		const static  int MaxDirectoryLength = 0xff;
		const static  int MaxLongPath = 0x7d00;
		const static  int MaxPath = 260;
		const static  char PathSeparator = ';';
		const char* Prefix = "\\\?\\";
		const char* RealInvalidPathChars = new char[37] {
			'"', '<', '>', '|', '\0', '\x0001', '\x0002', '\x0003', '\x0004', '\x0005', '\x0006', '\a', '\b', '\t', '\n', '\v',
				'\f', '\r', '\x000e', '\x000f', '\x0010', '\x0011', '\x0012', '\x0013', '\x0014', '\x0015', '\x0016', '\x0017', '\x0018', '\x0019', '\x001a', '\x001b',
				'\x001c', '\x001d', '\x001e', '\x001f'
		};
		const char* s_Base32Char = new char[32] {
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
				'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5'
		};
		const char* TrimEndChars = new char[8] { '\t', '\n', '\v', '\f', '\r', ' ', '\x0085', '\x00a0' };
		const static  char VolumeSeparatorChar = ':';

	};
}

#endif // PATH_H
