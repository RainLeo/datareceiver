#ifndef STREAM_H
#define STREAM_H

#if !WIN32
#include <sys/time.h>
#endif
#include <time.h>
#include <stdint.h>
#include <assert.h>
#include "../data/ByteArray.h"

namespace Common
{
    enum SeekOrigin
    {
        // These constants match Win32's FILE_BEGIN, FILE_CURRENT, and FILE_END
        SeekBegin = SEEK_SET,
        SeekCurrent = SEEK_CUR,
        SeekEnd = SEEK_END,
    };
    
	class Stream
	{
	public:
		Stream();
		virtual ~Stream();

		virtual int64_t write(const byte* array, int offset, int count) = 0;
		virtual int64_t read(byte* array, int offset, int count) = 0;

		virtual int64_t position() const = 0;
		virtual int64_t length() const = 0;
		virtual bool seek(int64_t position, SeekOrigin origin = SeekOrigin::SeekBegin) = 0;
        
        virtual bool canWrite() const;
        virtual bool canRead() const;
        virtual bool canSeek() const;

		void writeStr(const string& str, int lengthCount = 2);
		string readStr(int lengthCount = 2);
		void writeFixedLengthStr(const string& str, int length);
		string readFixedLengthStr(int length);
        
		void write(const ByteArray* array);
		void read(ByteArray* array);
        
		void writeBoolean(bool value);
		bool readBoolean();
        
		void writeFloat(float value, bool bigEndian = true);
		float readFloat(bool bigEndian = true);
        
		void writeDouble(double value, bool bigEndian = true);
		double readDouble(bool bigEndian = true);
        
		void writeInt64(int64_t value, bool bigEndian = true);
        int64_t readInt64(bool bigEndian = true);
        
        void writeInt48(int64_t value, bool bigEndian = true);
        int64_t readInt48(bool bigEndian = true);
        
		void writeUInt64(uint64_t value, bool bigEndian = true);
        uint64_t readUInt64(bool bigEndian = true);
        
        void writeUInt48(uint64_t value, bool bigEndian = true);
        uint64_t readUInt48(bool bigEndian = true);
        
		void writeInt32(int value, bool bigEndian = true);
        int readInt32(bool bigEndian = true);
        
		void writeInt24(int value, bool bigEndian = true);
        int readInt24(bool bigEndian = true);
        
		void writeInt16(short value, bool bigEndian = true);
        short readInt16(bool bigEndian = true);
        
		void writeUInt16(ushort value, bool bigEndian = true);
        ushort readUInt16(bool bigEndian = true);
        
		void writeByte(byte value);
		byte readByte();
        
		void writeBCDInt32(int value);
		uint readBCDInt32();
        
        void writeUInt32(uint value, bool bigEndian = true);
		int readUInt32(bool bigEndian = true);

        void writeUInt24(uint value, bool bigEndian = true);
		int readUInt24(bool bigEndian = true);
		
		void writeBCDUInt32(uint value);
		uint readBCDUInt32();
        
		void writeBCDByte(byte value);
		byte readBCDByte();
        
		void writeBCDUInt16(ushort value);
		ushort readBCDUInt16();
        
		void writeBCDInt16(short value);
		short readBCDInt16();
        
		void writeBCDValue(int64_t value, int length);
		uint readBCDValue(int length);

		void writeBCDCurrentTime();
		void writeBCDDateTime(const time_t timep, bool includedSec = true);
		time_t readBCDDateTime(bool includedSec = true);
        
		void writeBCDDate(const time_t timep);
		time_t readBCDDate();
        
		void writeBCDTime(const time_t timep);
		time_t readBCDTime();

	public:
		static bool isLittleEndian()
		{
			int x = 1;
			return *(char*)&x == '\1';
		}
		static bool isBigEndian()
		{
			return !isLittleEndian();
		}
	};
}

#endif // STREAM_H
