//
//  SerialInfo.h
//  common
//
//  Created by baowei on 15/7/31.
//  Copyright (c) 2015 com. All rights reserved.
//

#ifndef __common__SerialInfo__
#define __common__SerialInfo__

#include <string>
#include "common/xml/XmlTextReader.h"
#include "common/xml/XmlTextWriter.h"

using namespace std;
using namespace Common;

namespace Common
{
    struct SerialInfo
    {
    public:
        enum DataBitsType
        {
            DATA_5 = 5,
            DATA_6,
            DATA_7,
            DATA_8
        };
        
        enum ParityType
        {
            PAR_NONE = 0,
            PAR_ODD,
            PAR_EVEN,
            PAR_MARK,               //WINDOWS ONLY
            PAR_SPACE
        };
        
        enum StopBitsType
        {
            STOP_1 = 1,
            STOP_1_5,               //WINDOWS ONLY
            STOP_2
        };
        
        enum HandshakeType
        {
            FLOW_OFF = 0,
            FLOW_HARDWARE,
            FLOW_XONXOFF
        };
        
        string portName;
        int baudRate;
        DataBitsType dataBits;
        StopBitsType stopBits;
        HandshakeType handshake;
        ParityType parity;
        bool rtsEnable;
        bool dtrEnable;
        bool useSignal;
        
        SerialInfo(const string& portName = "");
        
        void read(XmlTextReader& reader);
        void write(XmlTextWriter& writer);

		string dataBitsStr() const;
		string parityStr() const;
		string stopBitsStr() const;
		string handshakeStr() const;
        
        static DataBitsType parseDataBits(const string& str);
        static string convertDataBitsStr(DataBitsType dataBits);
        
        static ParityType parseParity(const string& str);
        static string convertParityStr(ParityType parity);
        
        static StopBitsType parseStopBits(const string& str);
        static string convertStopBitsStr(StopBitsType stopBits);
        
        static HandshakeType parseHandshake(const string& str);
        static string convertHandshakeStr(HandshakeType handshake);
    };
}

#endif /* defined(__common__SerialInfo__) */
