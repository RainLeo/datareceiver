#include "MappingStream.h"
#include "common/IO/File.h"
#if WIN32
#include <Windows.h>
#else
#include <sys/mman.h>
#endif
#include "common/system/Math.h"

namespace Common
{
	MappingStream::View::View(ViewMapping mapping, int64_t offset, int64_t size)
    {
        this->offset = offset;
        this->size = size;
        _accessor = NULL;
		_mapping = mapping;
    }
    MappingStream::View::~View()
    {
        close();
    }
    
    int64_t MappingStream::View::write(int64_t position, const byte* array, int offset, int count)
    {
        updateViewAccessor();
        
        if(isValid())
        {
            memcpy((byte*)_accessor + (int)position, array + offset, count);
            
            return count;
        }
        
        return 0;
    }
    int64_t MappingStream::View::read(int64_t position, byte* array, int offset, int count)
    {
        updateViewAccessor();
        
        if(isValid())
        {
            memcpy(array + offset, (byte*)_accessor + (int)position, count);
            return count;
        }
        
        return 0;
    }
    
    void MappingStream::View::close()
    {
        if(isValid())
        {
			flush();
#if WIN32
			UnmapViewOfFile(_accessor);
#else
            if ((munmap(_accessor, (size_t)size)) == -1) {
                Debug::writeLine("munmap");
            }
#endif
            _accessor = NULL;
        }
    }
    bool MappingStream::View::flush()
    {
        if(isValid())
        {
#if WIN32
#else
            if ((msync(_accessor, (size_t)size, MS_SYNC)) == -1)
            {
                Debug::writeLine("msync");
                return false;
            }
            return true;
#endif
        }
        return false;
    }
    
    bool MappingStream::View::isValid() const
    {
        return _accessor != NULL;
    }
    void MappingStream::View::updateViewAccessor()
    {
        if(!isValid())
        {
#if WIN32
			if((_accessor = MapViewOfFile(_mapping, FILE_MAP_ALL_ACCESS, 0, (DWORD)offset, (SIZE_T)size)) == NULL)
			{
				Debug::writeLine("mmap wrong!");
			}
#else
#if !defined(MAP_NOCACHE)
#define MAP_NOCACHE 0
#endif
			if ((_accessor = mmap(NULL, (size_t)size, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_NOCACHE, _mapping, offset)) == MAP_FAILED)
            {
                Debug::writeLine("mmap wrong!");
            }
#endif
        }
    }
    
    MappingStream::MappingStream(const string& fileName, int64_t fileSize, FileAccess access)
    {
        if (fileName.empty())
        {
            throw new ArgumentNullException("fileName");
        }
        if (fileSize <= 0)
        {
            throw ArgumentOutOfRangeException("fileSize", "fileSize must be greater than or equal to zero.");
        }
		if (fileSize > (int64_t)2 * 1024 * 1024 * 1024)	// 2G
		{
			// 2G is the largest.
			throw ArgumentOutOfRangeException("fileSize", "fileSize must be less than 2G.");
		}
        
        if (!File::exists(fileName))
        {
            FileStream fs(fileName, FileMode::FileCreate, FileAccess::FileReadWrite);
            fs.setLength(fileSize);
        }

		ViewMapping mapping;
#if WIN32
		_file = CreateFile(fileName.c_str(), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, NULL);
		_mapFile = CreateFileMapping(_file, NULL, PAGE_READWRITE, 0, (DWORD)fileSize, NULL);
		mapping = _mapFile;
#else
		_mapFile = new FileStream(fileName, FileMode::FileOpen, access);
		mapping = _mapFile->_fd;
#endif
        
        mFileSize = fileSize;
        mAccess = access;
        mPosition = 0;
        
        Views views;
        int64_t offset = 0, size = 0;
        int64_t vsize = viewSize();
        for (int64_t i = 0; i < fileSize; i += vsize)
        {
            offset = i;
            size = Math::min(vsize, (fileSize - i));
			_views.add(new View(mapping, offset, size));
        }
    }
    MappingStream::~MappingStream()
    {
        close();
    }
    
    int64_t MappingStream::write(const byte* array, int offset, int count)
    {
        if (array == NULL)
        {
            throw ArgumentNullException("array cannot be NULL.");
        }
        if (offset < 0)
        {
            throw ArgumentOutOfRangeException("offset", "Non-negative number required.");
        }
        if (count < 0)
        {
            throw ArgumentOutOfRangeException("count", "Non-negative number required.");
        }
        
        if (!isOpen())
        {
            throw Exception("Cannot access a closed file.");
        }
        if (!canWrite())
        {
            throw NotSupportException("Stream does not support writing.");
        }
        
        int64_t size = 0;
        int length = 0;
        int64_t pos = position();
        for (uint i = 0; i < _views.count(); i++)
        {
            View* view = _views[i];
            size += view->size;
            if (size > position())
            {
                length = (int)Math::min(count - length, (int)(view->size - (position() - view->offset)));
                view->write(position() - view->offset, array, offset, length);
                offset += length;
                pos += length;
                seek(pos, SeekOrigin::SeekBegin);
                if (offset >= count)
                    break;
            }
        }
        return length;
    }
    int64_t MappingStream::read(byte* array, int offset, int count)
    {
        if (array == NULL)
        {
            throw ArgumentNullException("array");
        }
        if (offset < 0)
        {
            throw ArgumentOutOfRangeException("offset", "Non-negative number required.");
        }
        if (count < 0)
        {
            throw ArgumentOutOfRangeException("count", "Non-negative number required.");
        }
        
        if (!isOpen())
        {
            throw Exception("Cannot access a closed file.");
        }
        if (!canRead())
        {
            throw NotSupportException("Stream does not support reading.");
        }
        
        int64_t size = 0;
        int length = 0;
        int64_t pos = position();
        for (uint i = 0; i < _views.count(); i++)
        {
            View* view = _views[i];
            size += view->size;
            if (size > position())
            {
                length += (int)view->read(position() - view->offset, array, offset + length, count - length);
                pos += length;
                seek(pos, SeekOrigin::SeekBegin);
                if (length >= count)
                    break;
            }
        }
        return length;
    }
    
    int64_t MappingStream::position() const
    {
        if (isOpen())
        {
            return mPosition;
        }
        return 0;
    }
    int64_t MappingStream::length() const
    {
        if (isOpen())
        {
            return mFileSize;
        }
        return 0;
    }
    bool MappingStream::seek(int64_t offset, SeekOrigin origin)
    {
        if ((origin < SeekOrigin::SeekBegin) || (origin > SeekOrigin::SeekEnd))
        {
            throw ArgumentException("Invalid seek origin.");
        }
        if (!isOpen())
        {
            throw Exception("Cannot access a closed file.");
        }
        if (!canSeek())
        {
            throw NotSupportException("Stream does not support seeking.");
        }
        
        switch (origin)
        {
            case SeekOrigin::SeekBegin:
                break;
            case SeekOrigin::SeekCurrent:
                offset += position();
                break;
            case SeekOrigin::SeekEnd:
                offset = length() - offset;
                break;
            default:
                break;
        }
        
        mPosition = offset;
        return true;
    }
    
    bool MappingStream::canWrite() const
    {
        return (mAccess & FileAccess::FileWrite) == FileAccess::FileWrite;
    }
    bool MappingStream::canRead() const
    {
        return (mAccess & FileAccess::FileRead) == FileAccess::FileRead;
    }
    bool MappingStream::canSeek() const
    {
        return true;
    }
    
    int64_t MappingStream::viewSize() const
    {
        if (canWrite())
        {
            return WriteViewSize;
        }
        return ReadViewSize;
    }
    
    bool MappingStream::isOpen() const
    {
		return _mapFile != NULL;
    }
    void MappingStream::close()
    {
		if (isOpen())
		{
			for (uint i = 0; i < _views.count(); i++)
			{
				View* view = _views[i];
				view->close();
			}
			_views.clear();

#if WIN32
			CloseHandle(_file);
			_file = NULL;
			CloseHandle(_mapFile);
#else
			delete _mapFile;
#endif
			_mapFile = NULL;
		}
    }
    
    void MappingStream::flush()
    {
        for (uint i = 0; i < _views.count(); i++)
        {
            View* view = _views[i];
            view->flush();
        }
    }
}
