#include "File.h"
#include "FileInfo.h"
#if WIN32
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <errno.h>
#elif __APPLE__
#include <sys/param.h>
#include <sys/mount.h>
#include <libgen.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#else
#include <sys/vfs.h>
#include <libgen.h>
#include <limits.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#endif

namespace Common
{
	void FileInfo::stat()
	{
		if (exists())
		{
			struct stat buffer;
			int result = ::stat(_name.c_str(), &buffer);
			if (result == 0)
			{
				_attributes = (FileInfo::FileAttributes)buffer.st_mode;
				_size = buffer.st_size;
				_modifiedTime = buffer.st_mtime;
			}
		}
	}
}
