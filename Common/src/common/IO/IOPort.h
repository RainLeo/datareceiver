#ifndef IOPORT_H
#define IOPORT_H

#if !WIN32
#include <stdio.h>
#include <termios.h>
#include <errno.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <fcntl.h>
#include <sys/poll.h>
#endif
#include "common/common_global.h"

using namespace std;

namespace Common
{
#ifdef WIN32
	typedef void* handle;
	const handle invalidHandle = ((handle)-1);
#else
	typedef int handle;
	const handle invalidHandle = -1;
#endif

	class COMMON_EXPORT IOPort
	{
	public:
		IOPort(const string& name);
		virtual ~IOPort();

		virtual bool open();
		void close();

		inline bool isOpen() const
		{
			return _handle != invalidHandle;
		}

		size_t available();
		size_t write(const char *data, uint maxlen);
		size_t read(char *data, uint maxlen);
		size_t read(char *data, uint maxlen, uint timeout);

	protected:
		handle _handle;
		string _portName;
	};
}
#endif  //IOPORT_H
