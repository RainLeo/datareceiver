#ifndef PARALLELPORT_H
#define PARALLELPORT_H

#include "common/common_global.h"
#include "IOPort.h"

namespace Common
{
	class COMMON_EXPORT ParallelPort : public IOPort
	{
	public:
		ParallelPort(const string& name);
		~ParallelPort(void);
	};
}
#endif  //PARALLELPORT_H