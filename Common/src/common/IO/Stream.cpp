#include "Stream.h"
#include "../data/BCDUtilities.h"

namespace Common
{
	Stream::Stream()
	{
	}
	Stream::~Stream()
	{
	}

	void Stream::writeStr(const string& str, int lengthCount)
	{
#ifdef DEBUG
        uint length = (uint)str.length();
		if (lengthCount == 2 && length > 0xFFFF)
		{
			assert(false);
		}
		else if (lengthCount == 1 && length > 0xFF)
		{
			assert(false);
		}
		else if (lengthCount == 4 && length > 0xFFFFFFFF)
		{
			assert(false);
		}
#endif //DEBUG
		int len = 0;
		if (lengthCount == 2)
		{
			len = (ushort)str.length();
			writeUInt16((ushort)len);
		}
		else if (lengthCount == 1)
		{
			len = (byte)str.length();
			writeByte((byte)len);
		}
		else if (lengthCount == 4)
		{
			len = (uint)str.length();
			writeUInt32((uint)len);
		}
		else
		{
			assert(false);
		}

		write((byte*)str.data(), 0, len);
	}
	string Stream::readStr(int lengthCount)
	{
		uint len = 0;
		if (lengthCount == 2)
		{
			len = readUInt16();
		}
		else if (lengthCount == 1)
		{
			len = readByte();
		}
		else if (lengthCount == 4)
		{
			len = readUInt32();
		}
		else
		{
			assert(false);
		}

		return readFixedLengthStr(len);
	}
	void Stream::writeFixedLengthStr(const string& str, int length)
	{
		int len = (int)str.length();
		if (len < length)
		{
			write((byte*)str.data(), 0, len);
			int count = length - len;
			byte* buffer = new byte[count];
			memset(buffer, 0, count);
			write(buffer, 0, count);
			delete[] buffer;
		}
		else
		{
			write((byte*)str.data(), 0, length);
		}
	}
	string Stream::readFixedLengthStr(int length)
	{
		char* str = new char[length + 1];
		memset(str, 0, length + 1);
		read((byte*)str, 0, length);
		string result = str;
		delete[] str;
		return result;
	}
	void Stream::write(const ByteArray* array)
	{
		write(array->data(), 0, array->count());
	}
	void Stream::read(ByteArray* array)
	{
		read(array->data(), 0, array->count());
	}
	void Stream::writeBoolean(bool value)
	{
		writeByte((byte)value);
	}
	bool Stream::readBoolean()
	{
		return readByte() > 0;
	}
	void Stream::writeFloat(float value, bool bigEndian)
	{
		if ((isBigEndian() && !bigEndian) ||
			(isLittleEndian() && bigEndian))
		{
			int count = sizeof(float);
			byte* buffer = new byte[count];
			byte* source = (byte*)&value;
			for (int i = 0; i < count; i++)
			{
				buffer[i] = source[count - 1 - i];
			}
			write(buffer, 0, count);
			delete[] buffer;
		}
		else
		{
			write((byte*)&value, 0, sizeof(float));
		}
	}
	float Stream::readFloat(bool bigEndian)
	{
		if ((isBigEndian() && !bigEndian) ||
			(isLittleEndian() && bigEndian))
		{
			int count = sizeof(float);
			byte* source = new byte[count];
			byte* buffer = new byte[count];
			read(source, 0, count);
			for (int i = 0; i < count; i++)
			{
				buffer[i] = source[count - 1 - i];
			}
			float result;
			memcpy(&result, buffer, count);
			delete[] source;
			delete[] buffer;
			return result;
		}
		else
		{
			int count = sizeof(float);
			byte* buffer = new byte[count];
			read(buffer, 0, count);
			float result;
			memcpy(&result, buffer, count);
			delete[] buffer;
			return result;
		}
	}
	void Stream::writeDouble(double value, bool bigEndian)
	{
		if ((isBigEndian() && !bigEndian) ||
			(isLittleEndian() && bigEndian))
		{
			int count = sizeof(double);
			byte* buffer = new byte[count];
			byte* source = (byte*)&value;
			for (int i = 0; i < count; i++)
			{
				buffer[i] = source[count - 1 - i];
			}
			write(buffer, 0, count);
			delete[] buffer;
		}
		else
		{
			write((byte*)&value, 0, sizeof(double));
		}
	}
	double Stream::readDouble(bool bigEndian)
	{
		if ((isBigEndian() && !bigEndian) ||
			(isLittleEndian() && bigEndian))
		{
			int count = sizeof(double);
			byte* source = new byte[count];
			byte* buffer = new byte[count];
			read(source, 0, count);
			for (int i = 0; i < count; i++)
			{
				buffer[i] = source[count - 1 - i];
			}
			double result;
			memcpy(&result, buffer, count);
			delete[] source;
			delete[] buffer;
			return result;
		}
		else
		{
			int count = sizeof(double);
			byte* buffer = new byte[count];
			read(buffer, 0, count);
			double result;
			memcpy(&result, buffer, count);
			delete[] buffer;
			return result;
		}
	}
	void Stream::writeInt64(int64_t value, bool bigEndian)
	{
		byte buffer[8];
		if (bigEndian)
		{
			buffer[0] = (byte)((value & 0xFF00000000000000) >> 56);
			buffer[1] = (byte)((value & 0x00FF000000000000) >> 48);
			buffer[2] = (byte)((value & 0x0000FF0000000000) >> 40);
			buffer[3] = (byte)((value & 0x000000FF00000000) >> 32);
			buffer[4] = (byte)((value & 0x00000000FF000000) >> 24);
			buffer[5] = (byte)((value & 0x0000000000FF0000) >> 16);
			buffer[6] = (byte)((value & 0x000000000000FF00) >> 8);
			buffer[7] = (byte)((value & 0x00000000000000FF) >> 0);
		}
		else
		{
			buffer[7] = (byte)((value & 0xFF00000000000000) >> 56);
			buffer[6] = (byte)((value & 0x00FF000000000000) >> 48);
			buffer[5] = (byte)((value & 0x0000FF0000000000) >> 40);
			buffer[4] = (byte)((value & 0x000000FF00000000) >> 32);
			buffer[3] = (byte)((value & 0x00000000FF000000) >> 24);
			buffer[2] = (byte)((value & 0x0000000000FF0000) >> 16);
			buffer[1] = (byte)((value & 0x000000000000FF00) >> 8);
			buffer[0] = (byte)((value & 0x00000000000000FF) >> 0);
		}
		write(buffer, 0, sizeof(buffer));
	}
    void Stream::writeInt48(int64_t value, bool bigEndian)
    {
        byte buffer[6];
        if (bigEndian)
        {
            buffer[0] = (byte)((value & 0x0000FF0000000000) >> 40);
            buffer[1] = (byte)((value & 0x000000FF00000000) >> 32);
            buffer[2] = (byte)((value & 0x00000000FF000000) >> 24);
            buffer[3] = (byte)((value & 0x0000000000FF0000) >> 16);
            buffer[4] = (byte)((value & 0x000000000000FF00) >> 8);
            buffer[5] = (byte)((value & 0x00000000000000FF) >> 0);
        }
        else
        {
            buffer[5] = (byte)((value & 0x0000FF0000000000) >> 40);
            buffer[4] = (byte)((value & 0x000000FF00000000) >> 32);
            buffer[3] = (byte)((value & 0x00000000FF000000) >> 24);
            buffer[2] = (byte)((value & 0x0000000000FF0000) >> 16);
            buffer[1] = (byte)((value & 0x000000000000FF00) >> 8);
            buffer[0] = (byte)((value & 0x00000000000000FF) >> 0);
        }
        write(buffer, 0, sizeof(buffer));
    }
	void Stream::writeUInt64(uint64_t value, bool bigEndian)
	{
		writeInt64((int64_t)value, bigEndian);
	}
    void Stream::writeUInt48(uint64_t value, bool bigEndian)
    {
        writeInt48((int64_t)value, bigEndian);
    }
	void Stream::writeInt32(int value, bool bigEndian)
	{
		byte buffer[4];
		if (bigEndian)
		{
			buffer[0] = (byte)((value & 0xFF000000) >> 24);
			buffer[1] = (byte)((value & 0x00FF0000) >> 16);
			buffer[2] = (byte)((value & 0x0000FF00) >> 8);
			buffer[3] = (byte)((value & 0x000000FF) >> 0);
		}
		else
		{
			buffer[3] = (byte)((value & 0xFF000000) >> 24);
			buffer[2] = (byte)((value & 0x00FF0000) >> 16);
			buffer[1] = (byte)((value & 0x0000FF00) >> 8);
			buffer[0] = (byte)((value & 0x000000FF) >> 0);
		}
		write(buffer, 0, sizeof(buffer));
	}
	void Stream::writeInt24(int value, bool bigEndian)
	{
		byte buffer[3];
		if (bigEndian)
		{
			buffer[0] = (byte)((value & 0x00FF0000) >> 16);
			buffer[1] = (byte)((value & 0x0000FF00) >> 8);
			buffer[2] = (byte)((value & 0x000000FF) >> 0);
		}
		else
		{
			buffer[2] = (byte)((value & 0x00FF0000) >> 16);
			buffer[1] = (byte)((value & 0x0000FF00) >> 8);
			buffer[0] = (byte)((value & 0x000000FF) >> 0);
		}
		write(buffer, 0, sizeof(buffer));
	}
	void Stream::writeInt16(short value, bool bigEndian)
	{
		byte buffer[2];
		if (bigEndian)
		{
			buffer[0] = ((value >> 8) & 0xFF);
			buffer[1] = ((value)& 0xFF);
		}
		else
		{
			buffer[1] = ((value >> 8) & 0xFF);
			buffer[0] = ((value)& 0xFF);
		}
		write(buffer, 0, sizeof(buffer));
	}
	void Stream::writeUInt16(ushort value, bool bigEndian)
	{
		writeInt16(value, bigEndian);
	}
	void Stream::writeByte(byte value)
	{
		byte buffer[1];
		buffer[0] = value;
		write(buffer, 0, sizeof(buffer));
	}
	int64_t Stream::readInt64(bool bigEndian)
	{
		int64_t value = 0;
		byte buffer[8];
		memset(buffer, 0, sizeof(buffer));
		read(buffer, 0, sizeof(buffer));
		if (bigEndian)
		{
			for (size_t i = 0; i < sizeof(buffer); i++)
			{
				value <<= 8;
				value |= buffer[i];
			}

		}
		else
		{
			for (int i = sizeof(buffer) - 1; i >= 0; i--)
			{
				value <<= 8;
				value |= buffer[i];
			}
		}
		return value;
	}
    int64_t Stream::readInt48(bool bigEndian)
    {
        int64_t value = 0;
        byte buffer[6];
        memset(buffer, 0, sizeof(buffer));
        read(buffer, 0, sizeof(buffer));
        if (bigEndian)
        {
            for (size_t i = 0; i < sizeof(buffer); i++)
            {
                value <<= 8;
                value |= buffer[i];
            }
            
        }
        else
        {
            for (int i = sizeof(buffer) - 1; i >= 0; i--)
            {
                value <<= 8;
                value |= buffer[i];
            }
        }
        return value;
    }
	uint64_t Stream::readUInt64(bool bigEndian)
	{
		return (uint64_t)readInt64(bigEndian);
	}
    uint64_t Stream::readUInt48(bool bigEndian)
    {
        return (uint64_t)readInt48(bigEndian);
    }
	int Stream::readInt32(bool bigEndian)
	{
		int value = 0;
		byte buffer[4];
		memset(buffer, 0, sizeof(buffer));
		read(buffer, 0, sizeof(buffer));
		if (bigEndian)
		{
			for (size_t i = 0; i < sizeof(buffer); i++)
			{
				value <<= 8;
				value |= buffer[i];
			}

		}
		else
		{
			for (int i = sizeof(buffer) - 1; i >= 0; i--)
			{
				value <<= 8;
				value |= buffer[i];
			}
		}
		return value;
	}
	int Stream::readInt24(bool bigEndian)
	{
		int value = 0;
		byte buffer[3];
		memset(buffer, 0, sizeof(buffer));
		read(buffer, 0, sizeof(buffer));
		if (bigEndian)
		{
			for (size_t i = 0; i < sizeof(buffer); i++)
			{
				value <<= 8;
				value |= buffer[i];
			}

		}
		else
		{
			for (int i = sizeof(buffer) - 1; i >= 0; i--)
			{
				value <<= 8;
				value |= buffer[i];
			}
		}
		return value;
	}
	short Stream::readInt16(bool bigEndian)
	{
		byte buffer[2];
		read(buffer, 0, sizeof(buffer));
		short value = 0;
		value = bigEndian ? ((buffer[0] << 8) & 0xFF00) + buffer[1] : ((buffer[1] << 8) & 0xFF00) + buffer[0];;
		return value;
	}
	ushort Stream::readUInt16(bool bigEndian)
	{
		return (ushort)readInt16(bigEndian);
	}
	byte Stream::readByte()
	{
		byte buffer[1];
		read(buffer, 0, sizeof(buffer));
		return buffer[0];
	}
	void Stream::writeBCDInt32(int value)
	{
		writeBCDValue(value, 4);
	}
	uint Stream::readBCDInt32()
	{
		return (int)readBCDValue(4);
	}
	int Stream::readUInt32(bool bigEndian)
	{
		return readInt32(bigEndian);
	}
	void Stream::writeUInt32(uint value, bool bigEndian)
	{
		writeInt32((int)value, bigEndian);
	}
	int Stream::readUInt24(bool bigEndian)
	{
		return readInt24(bigEndian);
	}
	void Stream::writeUInt24(uint value, bool bigEndian)
	{
		writeInt24((int)value, bigEndian);
	}
	void Stream::writeBCDUInt32(uint value)
	{
		writeBCDValue(value, 4);
	}
	uint Stream::readBCDUInt32()
	{
		return (uint)readBCDValue(4);
	}
	void Stream::writeBCDByte(byte value)
	{
		writeBCDValue(value, 1);
	}
	byte Stream::readBCDByte()
	{
		return (byte)readBCDValue(1);
	}
	void Stream::writeBCDUInt16(ushort value)
	{
		writeBCDValue(value, 2);
	}
	ushort Stream::readBCDUInt16()
	{
		return (ushort)readBCDValue(2);
	}
	void Stream::writeBCDInt16(short value)
	{
		writeBCDUInt16(value);
	}
	short Stream::readBCDInt16()
	{
		return (short)readBCDUInt16();
	}

	void Stream::writeBCDCurrentTime()
	{
		writeBCDDateTime(time(NULL));
	}

	void Stream::writeBCDValue(int64_t value, int length)
	{
		byte buffer[8];
		memset(buffer, 0, sizeof(buffer));
		BCDUtilities::Int64ToBCD(value, buffer, length);
		write(buffer, 0, length);
	}
	uint Stream::readBCDValue(int length)
	{
		byte buffer[8];
		memset(buffer, 0, sizeof(buffer));
		read(buffer, 0, length);
		return (uint)BCDUtilities::BCDToInt64(buffer, 0, length);
	}
	void Stream::writeBCDDateTime(const time_t timep, bool includedSec)
	{
		// such like YYYYMMDDHHmmss or YYYYMMDDHHSS
		int len = includedSec ? 7 : 6;
		byte* buffer = new byte[len];
		memset(buffer, 0, len);
		if (timep > 0)
		{
			struct tm* tmp = localtime(&timep);
			int offset = 0;
			BCDUtilities::Int64ToBCD(tmp->tm_year + 1900, buffer + offset, 2);
			offset += 2;
			buffer[offset++] = BCDUtilities::ByteToBCD((byte)tmp->tm_mon + 1);
			buffer[offset++] = BCDUtilities::ByteToBCD((byte)tmp->tm_mday);
			buffer[offset++] = BCDUtilities::ByteToBCD((byte)tmp->tm_hour);
			buffer[offset++] = BCDUtilities::ByteToBCD((byte)tmp->tm_min);
			if (includedSec)
			{
				buffer[offset++] = BCDUtilities::ByteToBCD((byte)tmp->tm_sec);
			}
		}
		write(buffer, 0, len);
		delete[] buffer;
	}
	time_t Stream::readBCDDateTime(bool includedSec)
	{
		// such like YYYYMMDDHHmmss or YYYYMMDDHHSS
		int len = includedSec ? 7 : 6;
		byte* buffer = new byte[len];
		memset(buffer, 0, len);
		read(buffer, 0, len);
		bool isNull = true;
		for (uint i = 0; i < sizeof(buffer); i++)
		{
			if (buffer[i] != 0)
			{
				isNull = false;
				break;
			}
		}

		time_t result;
		if (isNull)
		{
			result = 0;
		}
		else
		{
			int offset = 0;
			int year = (int)BCDUtilities::BCDToInt64(buffer, offset, 2);
			offset += 2;
			int month = (int)BCDUtilities::BCDToInt64(buffer, offset, 1);
			offset += 1;
			int day = (int)BCDUtilities::BCDToInt64(buffer, offset, 1);
			offset += 1;

			int hour = (int)BCDUtilities::BCDToInt64(buffer, offset, 1);
			offset += 1;
			int minute = (int)BCDUtilities::BCDToInt64(buffer, offset, 1);
			offset += 1;
			int second = 0;
			if (includedSec)
			{
				second = (int)BCDUtilities::BCDToInt64(buffer, offset, 1);
				offset += 1;
			}
			result = Convert::combineTime(year, month, day, hour, minute, second);
		}
		delete[] buffer;
		return result;
	}
	void Stream::writeBCDDate(const time_t timep)
	{
		// such like YYYYMMDD
		byte buffer[4];
		memset(buffer, 0, sizeof(buffer));
		if (timep > 0)
		{
			struct tm* tmp = localtime(&timep);
			int offset = 0;
			BCDUtilities::Int64ToBCD(tmp->tm_year + 1900, buffer + offset, 2);
			offset += 2;
			buffer[offset++] = BCDUtilities::ByteToBCD((byte)tmp->tm_mon + 1);
			buffer[offset++] = BCDUtilities::ByteToBCD((byte)tmp->tm_mday);
		}
		write(buffer, 0, sizeof(buffer));
	}
	time_t Stream::readBCDDate()
	{
		byte buffer[4];
		memset(buffer, 0, sizeof(buffer));
		read(buffer, 0, sizeof(buffer));
		bool isNull = true;
		for (uint i = 0; i < sizeof(buffer); i++)
		{
			if (buffer[i] != 0)
			{
				isNull = false;
				break;
			}
		}
		if (isNull)
		{
			return 0;
		}
		else
		{
			int offset = 0;
			int year = (int)BCDUtilities::BCDToInt64(buffer, offset, 2);
			offset += 2;
			int month = (int)BCDUtilities::BCDToInt64(buffer, offset, 1);
			offset += 1;
			int day = (int)BCDUtilities::BCDToInt64(buffer, offset, 1);
			offset += 1;

			return Convert::combineTime(year, month, day);
		}
	}
	void Stream::writeBCDTime(const time_t timep)
	{
		// such like HHmmss
		byte buffer[3];
		memset(buffer, 0, sizeof(buffer));
		if (timep > 0)
		{
			struct tm* tmp = localtime(&timep);
			int offset = 0;
			buffer[offset++] = BCDUtilities::ByteToBCD((byte)tmp->tm_hour);
			buffer[offset++] = BCDUtilities::ByteToBCD((byte)tmp->tm_min);
			buffer[offset++] = BCDUtilities::ByteToBCD((byte)tmp->tm_sec);
		}
		write(buffer, 0, sizeof(buffer));
	}
	time_t Stream::readBCDTime()
	{
		// such like HHmmss
		byte buffer[3];
		memset(buffer, 0, sizeof(buffer));
		read(buffer, 0, sizeof(buffer));
		bool isNull = true;
		for (uint i = 0; i < sizeof(buffer); i++)
		{
			if (buffer[i] != 0)
			{
				isNull = false;
				break;
			}
		}
		if (isNull)
		{
			return 0;
		}
		else
		{
			int offset = 0;
			long hour = (long)BCDUtilities::BCDToInt64(buffer, offset, 1);
			offset += 1;
			long minute = (long)BCDUtilities::BCDToInt64(buffer, offset, 1);
			offset += 1;
			long second = 0;
			second = (long)BCDUtilities::BCDToInt64(buffer, offset, 1);
			offset += 1;

			return hour * 3600 + minute * 60 + second;
		}
	}
    
    bool Stream::canWrite() const
    {
        return true;
    }
    bool Stream::canRead() const
    {
        return true;
    }
    bool Stream::canSeek() const
    {
        return true;
    }
}
