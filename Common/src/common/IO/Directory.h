#ifndef DIRECTORY_H
#define DIRECTORY_H

#include <string>
#include "../data/StringArray.h"

using namespace std;

namespace Common
{
	enum SearchOption
	{
		TopDirectoryOnly,
		AllDirectories
	};

	class Directory
	{
	public:
		static bool createDirectory(const char* path);
		static bool createDirectory(const string& path);
		static bool deleteDirectory(const char* path);
		static bool deleteDirectory(const string& path);
		static bool exists(const char* path);
		static bool exists(const string& path);
		static bool getFiles(const char* path, const char* searchPattern, SearchOption searchOption, StringArray& files);
		static bool getFiles(const string& path, const string& searchPattern, SearchOption searchOption, StringArray& files);
		static bool copy(const char* sourcePath, const char* destPath, const StringArray* excludedItems = NULL);
		static bool copy(const string& sourcePath, const string& destPath, const StringArray* excludedItems = NULL);

		static string getAppFilePath();
		static string getAppDirPath();
		static string getAppFileName();

	private:
		static bool createDirectoryInner(const char* path);
	};
}

#endif // DIRECTORY_H
