#ifndef SERIALPORT_H
#define SERIALPORT_H

#include "common/common_global.h"
#include "IOPort.h"
#include "SerialInfo.h"

namespace Common
{
	class COMMON_EXPORT SerialPort : public IOPort
	{
	public:
		SerialPort(const string& name);
		~SerialPort();

		bool open();

		inline void setBaudRate(int baudRate)
		{
			if(baudRate > 0)
			{
				if(isOpen())
				{
					setCommConfig(baudRate, parity(), dataBits(), stopBits(), handshake());
				}
				_baudRate = baudRate;
			}
		}
		inline int baudRate() const
		{
			return _baudRate;
		}

		inline void setDataBits(SerialInfo::DataBitsType dataBits)
		{
			if(isOpen())
			{
				setCommConfig(baudRate(), parity(), dataBits, stopBits(), handshake());
			}
			_dataBits = dataBits;
		}
		inline SerialInfo::DataBitsType dataBits() const
		{
			return _dataBits;
		}

		inline void setParity(SerialInfo::ParityType parity)
		{
			if(isOpen())
			{
				setCommConfig(baudRate(), parity, dataBits(), stopBits(), handshake());
			}
			_parity = parity;
		}
		inline SerialInfo::ParityType parity() const
		{
			return _parity;
		}

		inline void setStopBits(SerialInfo::StopBitsType stopBits)
		{
			if(isOpen())
			{
				setCommConfig(baudRate(), parity(), dataBits(), stopBits, handshake());
			}
			_stopBits = stopBits;
		}
		inline SerialInfo::StopBitsType stopBits() const
		{
			return _stopBits;
		}

		inline void setHandshake(SerialInfo::HandshakeType handshake)
		{
			if(isOpen())
			{
				setCommConfig(baudRate(), parity(), dataBits(), stopBits(), handshake);
			}
			_handshake = handshake;
		}
		inline SerialInfo::HandshakeType handshake() const
		{
			return _handshake;
		}

		inline bool rtsEnable() const
		{
			return _rtsEnable;
		}
		void setRtsEnable(bool enabled)
		{
			if(isOpen())
			{
				setCommConfig(baudRate(), parity(), dataBits(), stopBits(), handshake());
			}
			_rtsEnable = enabled;
		}

		inline bool dtrEnable() const
		{
			return _dtrEnable;
		}
		void setDtrEnable(bool enabled)
		{
			if(isOpen())
			{
				setCommConfig(baudRate(), parity(), dataBits(), stopBits(), handshake());
			}
			_dtrEnable = enabled;
		}

		inline bool useSignal() const
		{
			return _useSignal;
		}
		void setUseSignal(bool useSignal)
		{
			_useSignal = useSignal;
		}

	private:
		bool setCommConfig(int baudRate, SerialInfo::ParityType parity, SerialInfo::DataBitsType dataBits, SerialInfo::StopBitsType stopBits, SerialInfo::HandshakeType handshake);

	private:
		int _baudRate;
		SerialInfo::DataBitsType _dataBits;
		SerialInfo::StopBitsType _stopBits;
		SerialInfo::HandshakeType _handshake;
		SerialInfo::ParityType _parity;
		bool _rtsEnable;
		bool _dtrEnable;
		bool _useSignal;
	};
}
#endif  //SERIALPORT_H