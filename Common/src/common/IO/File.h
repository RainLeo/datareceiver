#ifndef FILE_H
#define FILE_H

#include <string>
#if !WIN32
#include <stdio.h>
#include <sys/stat.h>
#include <utime.h>
#include <fcntl.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#endif

#ifndef S_ISDIR
#define	S_ISDIR(m)	(((m) & S_IFMT) == S_IFDIR)	/* directory */
#endif

using namespace std;

namespace Common
{
	class File
	{
	public:
		static bool exists(const char* path);
		static bool exists(const string& path);
		static bool deleteFile(const char* path);
		static bool deleteFile(const string& path);
		static bool move(const char* sourceFileName, const char* destFileName);
		static bool move(const string& sourceFileName, const string& destFileName);
		static bool copy(const char* sourceFileName, const char* destFileName, bool overwrite = true);
		static bool copy(const string& sourceFileName, const string& destFileName, bool overwrite = true);
        
    private:
#if !WIN32
        static bool write_file (int src_fd, int dest_fd, struct stat *st_src);
        /**
         * CopyFile:
         * @name: a pointer to a NULL-terminated unicode string, that names
         * the file to be copied.
         * @dest_name: a pointer to a NULL-terminated unicode string, that is the
         * new name for the file.
         * @fail_if_exists: if TRUE and dest_name exists, the copy will fail.
         *
         * Copies file @name to @dest_name
         *
         * Return value: %TRUE on success, %FALSE otherwise.
         */
        static bool copy_file (const char *name, const char *dest_name, bool fail_if_exists);
        /**
         * MoveFile:
         * @name: a pointer to a NULL-terminated unicode string, that names
         * the file to be moved.
         * @dest_name: a pointer to a NULL-terminated unicode string, that is the
         * new name for the file.
         *
         * Renames file @name to @dest_name.
         * MoveFile sets ERROR_ALREADY_EXISTS if the destination exists, except
         * when it is the same file as the source.  In that case it silently succeeds.
         *
         * Return value: %TRUE on success, %FALSE otherwise.
         */
        static bool move_file (const char *name, const char *dest_name);
#endif
	};
}

#endif // FILE_H
