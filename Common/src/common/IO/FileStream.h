#ifndef FILESTREAM_H
#define FILESTREAM_H

#if WIN32
#include <io.h>
#else
#include <fcntl.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#endif
#include <sys/stat.h>
#include <fcntl.h>
#include "Stream.h"
#include "../diag/Debug.h"
#include "../data/ByteArray.h"
#include "../data/StringBuilder.h"

namespace Common
{
    enum FileMode
    {
        // Creates a new file. An exception is raised if the file already exists.
        FileCreateNew = 1,
        
        // Creates a new file. If the file already exists, it is overwritten.
        FileCreate = 2,
        
        // Opens an existing file. An exception is raised if the file does not exist.
        FileOpen = 3,
        
        // Opens the file if it exists. Otherwise, creates a new file.
        FileOpenOrCreate = 4,
        
        // Opens an existing file. Once opened, the file is truncated so that its
        // size is zero bytes. The calling process must open the file with at least
        // WRITE access. An exception is raised if the file does not exist.
        FileTruncate = 5,
        
        // Opens the file if it exists and seeks to the end.  Otherwise,
        // creates a new file.
        FileAppend = 6,
    };
    enum FileAccess
    {
        // Specifies read access to the file. Data can be read from the file and
        // the file pointer can be moved. Combine with WRITE for read-write access.
        FileRead = 1,
        
        // Specifies write access to the file. Data can be written to the file and
        // the file pointer can be moved. Combine with READ for read-write access.
        FileWrite = 2,
        
        // Specifies read and write access to the file. Data can be written to the
        // file and the file pointer can be moved. Data can also be read from the
        // file.
        FileReadWrite = 3,
    };

    class MappingStream;
	class COMMON_EXPORT FileStream : public Stream
	{
	public:
        FileStream(const char* filename, FileMode mode, FileAccess access = FileReadWrite);
        FileStream(const string& filename, FileMode mode, FileAccess access = FileReadWrite);
        ~FileStream();

		int64_t write(const byte* array, int offset, int count) override;
		int64_t read(byte* array, int offset, int count) override;

		int64_t position() const override;
		int64_t length() const override;
		bool seek(int64_t position, SeekOrigin origin = SeekOrigin::SeekBegin) override;

        bool isOpen() const;
		void close();

        void setLength(int64_t length);
        void flush();
        
        bool readToEnd(ByteArray& array, uint cacheCount = 1024);
        bool readToEnd(String& str, uint cacheCount = 1024);
        
    private:
        void open(const char* filename, int openFlag, int mode);
        
        static int openFlag(const char* filename, FileMode mode);
#ifndef mode_t
#define mode_t int
#endif
        static mode_t openMode(FileAccess access);

	private:
        friend MappingStream;
        
		int _fd;
        
        const static int InvalidHandle = -1;
	};
}

#endif // FILESTREAM_H
