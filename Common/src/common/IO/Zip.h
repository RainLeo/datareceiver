#ifndef ZIP_H
#define ZIP_H

#include <stdio.h>
#include <string>
#include "common/common_global.h"
#include "common/data/ByteArray.h"
#include <libzip/zip.h>

using namespace std;

int zipRead(void * context, char * buffer, int len);
int zipClose(void * context);

namespace Common
{
    class StringArray;
	class COMMON_EXPORT Zip
	{
	public:
		Zip(const string& filename);
		Zip(const char* filename);
		~Zip();
        
        bool isValid() const;
        
        zip_file* open(const char* filename) const;
        zip_file* open(const string& filename) const;
        void close(zip_file* zf);
        
        bool read(const char* filename, ByteArray& buffer);
        bool read(const string& filename, ByteArray& buffer);
        
    public:
		static bool extract(const char* zipfile, const char* path);
		static bool extract(const string& zipfile, const string& path);
        
        static bool compress(const char* path, const char* zipfile);
        static bool compress(const string& path, const string& zipfile);
        static bool compressFile(const char* filename, const char* zipfile);
        static bool compressFile(const string& filename, const string& zipfile);
        
    private:
        static bool compressFile(const string& cpath, const StringArray& filenames, const string& zipfile);

    private:
        zip* _zip;
        int _error;
	};
}

#endif	// ZIP_H
