//
//  FileStream.cpp
//  common
//
//  Created by baowei on 15/4/5.
//  Copyright (c) 2015 com. All rights reserved.
//

#if !WIN32
#include <termios.h>
#endif
#include "File.h"
#include "FileStream.h"
#include "../exception/Exception.h"

#ifndef O_BINARY
#define O_BINARY 0
#endif
#ifndef S_IREAD
#define S_IREAD S_IRUSR
#endif
#ifndef S_IWRITE
#define S_IWRITE S_IWUSR
#endif

namespace Common
{
    FileStream::FileStream(const char* filename, FileMode mode, FileAccess access) : _fd(InvalidHandle)
    {
        if(filename == NULL)
        {
            throw ArgumentNullException("filename");
        }
        if(mode == FileOpen)
        {
            if(!File::exists(filename))
            {
                throw FileNotFoundException(filename);
            }
        }
        
        open(filename, openFlag(filename, mode), openMode(access));
    }
    FileStream::FileStream(const string& filename, FileMode mode, FileAccess access) : FileStream(filename.c_str(), mode, access)
    {
    }
    FileStream::~FileStream()
    {
        close();
    }
    
    void FileStream::open(const char* filename, int openFlag, int mode)
    {
        _fd = ::open(filename, openFlag, mode);
        if (_fd == InvalidHandle)
        {
            Debug::writeFormatLine("can't open file: name: %s, error: %s", filename, strerror(errno));
        }
    }
    void FileStream::close()
    {
        if (_fd != InvalidHandle)
        {
            ::close(_fd);
        }
        _fd = InvalidHandle;
    }
    bool FileStream::isOpen() const
    {
        return _fd != InvalidHandle;
    }
    
    int FileStream::openFlag(const char* filename, FileMode mode)
    {
        switch (mode)
        {
            case FileCreateNew:
				return O_CREAT | O_BINARY;
            case FileCreate:
				return O_CREAT | O_RDWR | O_BINARY;
            case FileOpen:
				return O_RDWR | O_BINARY;
            case FileOpenOrCreate:
				return File::exists(filename) ? O_RDWR | O_BINARY : O_CREAT | O_TRUNC | O_RDWR | O_BINARY;
            case FileTruncate:
				return O_CREAT | O_TRUNC | O_RDWR | O_BINARY;
            case FileAppend:
				return O_APPEND | O_RDWR | O_BINARY;
            default:
				return O_RDONLY | O_BINARY;
        }
    }
    mode_t FileStream::openMode(FileAccess access)
    {
        switch (access)
        {
            case FileRead:
                return S_IREAD;
            case FileWrite:
                return S_IWRITE;
            case FileReadWrite:
                return S_IREAD | S_IWRITE;
            default:
                return S_IREAD;
        }
    }
    
    int64_t FileStream::position() const
    {
#if WIN32
        return isOpen() ? tell(_fd) : -1;
#else
        return isOpen() ? lseek(_fd, 0, SeekCurrent) : -1;
#endif
    }
    int64_t FileStream::length() const
    {
#if WIN32
        return isOpen() ? filelength(_fd) : 0;
#else
        if(isOpen())
        {
            struct stat fst;
            fstat(_fd, &fst);
            return fst.st_size;
        }
        return -1;
#endif
    }
    void FileStream::setLength(int64_t length)
    {
        if (length > 0 && isOpen())
        {
            int64_t pos = position();
            seek(length-1, SeekBegin);
            writeByte(0);
            if (pos != length)
            {
                if (pos < length)
                {
                    seek(pos, SeekBegin);
                }
                else
                {
                    seek(0, SeekEnd);
                }
            }
        }
    }
    bool FileStream::seek(int64_t position, SeekOrigin origin)
    {
        if (isOpen())
        {
            return ::lseek(_fd, (long)position, origin) >= 0;
        }
        return false;
    }
    int64_t FileStream::write(const byte* array, int offset, int count)
    {
        if (isOpen())
        {
            return ::write(_fd, array + offset, (uint)count);
        }
        return 0;
    }
    int64_t FileStream::read(byte* array, int offset, int count)
    {
        if (isOpen())
        {
            return ::read(_fd, array + offset, (uint)count);
        }
        return 0;
    }
    void FileStream::flush()
    {
        if (isOpen())
        {
#if WIN32
            _commit(_fd);
#else
            tcflush(_fd, TCIOFLUSH);
#endif
        }
    }
    
    bool FileStream::readToEnd(ByteArray& array, uint cacheCount)
    {
        if (isOpen())
        {
            byte* buffer = new byte[cacheCount];
            int64_t count = 0;
            do
            {
                count = read(buffer, 0, cacheCount);
                if(count > 0)
                {
                    array.addRange(buffer, (uint)count);
                }
            }while(count > 0);
			delete[] buffer;
            
            return true;
        }
        return false;
    }
    
    bool FileStream::readToEnd(String& str, uint cacheCount)
    {
        if (isOpen())
        {
            byte* buffer = new byte[cacheCount];
            int64_t count = 0;
            do
            {
                count = read(buffer, 0, cacheCount);
                if(count > 0)
                {
                    str.append((const char*)buffer, (uint)count);
                }
            }while(count > 0);
            delete[] buffer;
            
            return true;
        }
        return false;
    }
}
