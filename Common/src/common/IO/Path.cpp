#include "Path.h"
#include "../system/Convert.h"
#include "../exception/Exception.h"
#if WIN32
#include <Windows.h>
#include <memory.h>
#include <fcntl.h>
#include <io.h>
#include <direct.h>
#include <Shlwapi.h>
#pragma comment (lib, "Shlwapi.lib")
#elif __APPLE__
#include <sys/param.h>
#include <sys/mount.h>
#include <libgen.h>
#include <dirent.h>
#else
#include <sys/vfs.h>
#include <libgen.h>
#include <limits.h>
#include <unistd.h>
#include <dirent.h>
#include <stdlib.h>
#endif

namespace Common
{
	bool Path::isPathRooted(const char* path)
	{
#if WIN32
		return PathIsRelative(path) ? false : true;
#else
        if (path != NULL)
        {
            checkInvalidPathChars(path);
            
            size_t length = strlen(path);
            if ((length >= 1 && (path[0] == DirectorySeparatorChar || path[0] == AltDirectorySeparatorChar))
                || (length >= 2 && path[1] == VolumeSeparatorChar))
            {
                return true;
            }
        }
        return false;
#endif
	}
	bool Path::isPathRooted(const string& path)
	{
		return isPathRooted(path.c_str());
	}

	string Path::combine(const char* path1, const char* path2)
	{
		checkInvalidPathChars(path1, false);
		checkInvalidPathChars(path2, false);

		return combineNoChecks(path1, path2);
	}
	string Path::combine(const string& path1, const string& path2)
	{
		return combine(path1.c_str(), path2.c_str());
	}

	string Path::getFileName(const char* path)
	{
		if (path != NULL)
		{
			checkInvalidPathChars(path, false);
			size_t length = strlen(path);
			int num2 = (int)length;
			while (--num2 >= 0)
			{
				char ch = path[num2];
				if (((ch == DirectorySeparatorChar) || (ch == AltDirectorySeparatorChar)) || (ch == VolumeSeparatorChar))
				{
					return ((string)path).substr(num2 + 1, (length - num2) - 1);
				}
			}
		}
		return "";
	}
	string Path::getFileName(const string& path)
	{
		return getFileName(path.c_str());
	}

	string Path::getFileNameWithoutExtension(const char* path)
	{
		string pathStr = getFileName(path);
		if (pathStr.empty())
		{
			return "";
		}
		size_t length = pathStr.find_last_of('.');
        if (length == string::npos)
		{
			return pathStr;
		}
		return pathStr.substr(0, length);
	}
	string Path::getFileNameWithoutExtension(const string& path)
	{
		return getFileNameWithoutExtension(path.c_str());
	}

	string Path::getDirectoryName(const char* path)
	{
		if (path != NULL)
		{
			checkInvalidPathChars(path, false);
            
            string pathStr = path;
            size_t length = pathStr.find_last_of(DirectorySeparatorChar);
            if (length == string::npos)
            {
#if WIN32
				length = (int)pathStr.find_last_of('/');
				if (length == string::npos)
					return pathStr;
				else
					return pathStr.substr(0, length);
#else
				return pathStr;
#endif
            }
            return pathStr.substr(0, length);
//#if WIN32
//			char buffer[MAX_PATH];
//			strcpy(buffer, path);
//			PathRemoveFileSpec(buffer);
//			return buffer;
//#endif
		}
		return "";
	}
	string Path::getDirectoryName(const string& path)
	{
		return getDirectoryName(path.c_str());
	}
	string Path::getExtension(const char* path)
	{
		if (path == NULL)
			return "";

		string pathStr = getFileName(path);
		if (pathStr.empty())
		{
			pathStr = path;
		}
		size_t length = pathStr.find_last_of('.');
		if (length == string::npos)
		{
			return pathStr;
		}
		return pathStr.substr(length, pathStr.length() - 1);
	}
	string Path::getExtension(const string& path)
	{
		return getExtension(path.c_str());
	}

	bool Path::hasIllegalCharacters(const char* path, bool checkAdditional)
	{
		if (path == NULL)
			return false;

		size_t length = strlen(path);
		for (uint i = 0; i < length; i++)
		{
			byte num2 = path[i];
			if (((num2 == 0x22) || (num2 == 60)) || (((num2 == 0x3e) || (num2 == 0x7c)) || (num2 < (byte)0x20)))
			{
				return true;
			}
			if (checkAdditional && ((num2 == 0x3f) || (num2 == 0x2a)))
			{
				return true;
			}
		}
		return false;
	}
	void Path::checkInvalidPathChars(const char* path, bool checkAdditional)
	{
		if (path == NULL)
		{
			throw new ArgumentNullException("path");
		}
		if (hasIllegalCharacters(path, checkAdditional))
		{
			throw new ArgumentException("Illegal characters in path.");
		}
	}
	string Path::combineNoChecks(const char* path1, const char* path2)
	{
		if (path2 == NULL)
		{
			return path1;
		}
		if (path1 == NULL)
		{
			return path2;
		}
		if (isPathRooted(path2))
		{
			return path2;
		}

		char ch = path1[strlen(path1) - 1];
		if (((ch != DirectorySeparatorChar) && (ch != AltDirectorySeparatorChar)) && (ch != VolumeSeparatorChar))
		{
			return Convert::convertStr("%s%c%s", path1, DirectorySeparatorChar, path2);
		}
		return Convert::convertStr("%s%s", path1, path2);
	}
}
