#include "Directory.h"
#include "../system/Convert.h"
#include "../exception/Exception.h"
#include "Path.h"
#include "File.h"
#include <sys/stat.h>
#if WIN32
#include <Windows.h>
#include <memory.h>
#include <fcntl.h>
#include <io.h>
#include <direct.h>
#include <Shlwapi.h>
#pragma comment (lib, "Shlwapi.lib")
#elif __APPLE__
#include <sys/param.h>
#include <sys/mount.h>
#include <unistd.h>
#include <libgen.h>
#include <dirent.h>
#else
#include <sys/vfs.h>
#include <libgen.h>
#include <limits.h>
#include <unistd.h>
#include <dirent.h>
#endif

namespace Common
{
    bool Directory::createDirectoryInner(const char* path)
    {
        if (path == NULL)
            return false;
        
#if WIN32
        return _mkdir(path) == 0 ? true : false;
#else
        return mkdir(path, 0777) == 0 ? true : false;
#endif
    }
    bool Directory::createDirectory(const char* path)
    {
        if (path == NULL)
            return false;
        
        size_t length = strlen(path);
        string str = path[length - 1] == Path::DirectorySeparatorChar ? path : (string)path + Path::DirectorySeparatorChar;
        if (exists(str))
        {
            return false;
        }
        else
        {
            string temp;
            std::string::size_type pos = 0;
            while ((pos = str.find(Path::DirectorySeparatorChar, pos)) != string::npos)
            {
                if(pos > 0)
                {
                    temp = str.substr(0, pos);
                    if (!exists(temp))
                    {
                        if (!createDirectoryInner(temp.c_str()))
                            return false;
                    }
                }
                pos += sizeof(Path::DirectorySeparatorChar);
            }
            return true;
        }
    }
    bool Directory::createDirectory(const string& path)
    {
        return createDirectory(path.c_str());
    }
    
    bool Directory::deleteDirectory(const char* path)
    {
        if (path == NULL)
            return false;
        
#if WIN32
		return RemoveDirectory(path) != 0 ? true : false;
#else
        return remove(path) == 0 ? true : false;
#endif
    }
    bool Directory::deleteDirectory(const string& path)
    {
        return deleteDirectory(path.c_str());
    }
    
    bool Directory::exists(const char* path)
    {
#if WIN32
		return PathFileExists(path) == TRUE ? true : false;
#else
        struct stat s;
        int err = stat(path, &s);
        if(-1 == err) {
            if(ENOENT == errno) {
                /* does not exist */
            } else {
            }
        } else {
            if(S_ISDIR(s.st_mode)) {
                /* it's a dir */
                return true;
            } else {
                /* exists but is no dir */
            }
        }
        return false;
#endif
    }
    bool Directory::exists(const string& path)
    {
        return exists(path.c_str());
    }
    
    bool Directory::getFiles(const char* path, const char* searchPattern, SearchOption searchOption, StringArray& files)
    {
        if (path == NULL)
            return false;
        
        string filter = searchPattern != NULL? searchPattern : "*.*";
#if WIN32
        if (exists(path))
        {
            WIN32_FIND_DATA ffd;
            HANDLE hFind = INVALID_HANDLE_VALUE;
            // Find the first file in the directory.
            string fileStr = Path::combine(path, filter.c_str());
            hFind = FindFirstFile(fileStr.c_str(), &ffd);
            if (INVALID_HANDLE_VALUE != hFind)
            {
                // List all the files in the directory with some info about them.
                do
                {
                    if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
                    {
                        string pathStr = ffd.cFileName;
                        if (pathStr != "." && pathStr != "..")
                        {
                            if (searchOption == SearchOption::AllDirectories)
                            {
                                getFiles(Path::combine(path, ffd.cFileName).c_str(), filter.c_str(), searchOption, files);
                            }
                        }
                    }
                    else
                    {
                        files.add(Path::combine(path, ffd.cFileName));
                    }
                } while (FindNextFile(hFind, &ffd) != 0);
                FindClose(hFind);
            }
            return true;
        }
#else
        //const char* path = dirname((char*)_logPath.c_str());
        if (exists(path))
        {
            DIR *dir;
            struct dirent *ent;
            struct stat st;
            
            dir = opendir(path);
            while ((ent = readdir(dir)) != NULL)
            {
                const string file_name = ent->d_name;
                const string full_file_name = (string)path + "/" + file_name;
                
                if (file_name[0] == '.')
                    continue;
                
                if (stat(full_file_name.c_str(), &st) == -1)
                    continue;
                
                const bool is_directory = (st.st_mode & S_IFDIR) != 0;
                
                if (is_directory)
                {
                    if (searchOption == SearchOption::AllDirectories)
                    {
                        getFiles(full_file_name, filter.c_str(), searchOption, files);
                    }
                }
                else
                {
                    files.add(full_file_name);
                }
            }
            closedir(dir);
            return true;
        }
#endif
        return false;
    }
    bool Directory::getFiles(const string& path, const string& searchPattern, SearchOption searchOption, StringArray& files)
    {
        return getFiles(path.c_str(), searchPattern.empty() ? NULL : searchPattern.c_str(), searchOption, files);
    }
    
    bool Directory::copy(const char* sourcePath, const char* destPath, const StringArray* excludedItems)
    {
        if (sourcePath == NULL)
            return false;
        if (destPath == NULL)
            return false;
        
        const string filter = "*";
        if (!exists(destPath))
        {
            createDirectory(destPath);
        }
        if (exists(sourcePath))
        {
#if WIN32
            WIN32_FIND_DATA ffd;
            HANDLE hFind = INVALID_HANDLE_VALUE;
            // Find the first file in the directory.
            string fileStr = Path::combine(sourcePath, filter.c_str());
            hFind = FindFirstFile(fileStr.c_str(), &ffd);
            if (INVALID_HANDLE_VALUE != hFind)
            {
                // List all the files in the directory with some info about them.
                do
                {
                    if (excludedItems != NULL &&
                        excludedItems->contains(ffd.cFileName))
                    {
                        continue;
                    }
                    
                    if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
                    {
                        string path = ffd.cFileName;
                        if (path != "." && path != "..")
                        {
                            string sPathName = Path::combine(sourcePath, ffd.cFileName);
                            string dPathName = Path::combine(destPath, ffd.cFileName);
                            Directory::copy(sPathName.c_str(), dPathName.c_str(), excludedItems);
                        }
                    }
                    else
                    {
                        string sFileName = Path::combine(sourcePath, ffd.cFileName);
                        string dFileName = Path::combine(destPath, ffd.cFileName);
                        File::copy(sFileName.c_str(), dFileName.c_str());
                    }
                } while (FindNextFile(hFind, &ffd) != 0);
                FindClose(hFind);
            }
#else
#endif
            return true;
        }
        return false;
    }
    bool Directory::copy(const string& sourcePath, const string& destPath, const StringArray* excludedItems)
    {
        return copy(sourcePath.c_str(), destPath.c_str(), excludedItems);
    }
    
    string Directory::getAppFilePath()
    {
        string filePath;
#if WIN32
        // We do MAX_PATH + 2 here, and request with MAX_PATH + 1, so we can handle all paths
        // up to, and including MAX_PATH size perfectly fine with string termination, as well
        // as easily detect if the file path is indeed larger than MAX_PATH, in which case we
        // need to use the heap instead. This is a work-around, since contrary to what the
        // MSDN documentation states, GetModuleFileName sometimes doesn't set the
        // ERROR_INSUFFICIENT_BUFFER error number, and we thus cannot rely on this value if
        // GetModuleFileName(0, buffer, MAX_PATH) == MAX_PATH.
        // GetModuleFileName(0, buffer, MAX_PATH + 1) == MAX_PATH just means we hit the normal
        // file path limit, and we handle it normally, if the result is MAX_PATH + 1, we use
        // heap (even if the result _might_ be exactly MAX_PATH + 1, but that's ok).
        char buffer[MAX_PATH + 2];
        memset(buffer, 0, sizeof(buffer));
        DWORD v = GetModuleFileName(0, buffer, MAX_PATH + 1);
        
        if (v == 0)
        {
            filePath = "";
        }
        else if (v <= MAX_PATH)
        {
            filePath = string(buffer);
        }
        else
        {
            // MAX_PATH sized buffer wasn't large enough to contain the full path, use heap
            char *b = 0;
            int i = 1;
            size_t size;
            do {
                ++i;
                size = MAX_PATH * i;
                b = reinterpret_cast<char *>(realloc(b, (size + 1) * sizeof(char)));
                if (b)
                    v = GetModuleFileName(NULL, b, size);
            } while (b && v == size);
            
            if (b)
                *(b + size) = 0;
            filePath = string(b);
            free(b);
        }
#elif __APPLE__
        //        char path[1024];
        //        uint32_t size = sizeof(path);
        //        if (_NSGetExecutablePath(path, &size) == 0)
        //            printf("executable path is %s\n", path);
        //        else
        //            printf("buffer too small; need size %u\n", size);
        //        char result[PATH_MAX];
        //        if(getcwd(result, PATH_MAX) != NULL)
        //        {
        //            filePath = string(result);
        //        }
        throw "It must be implemented.";
#else
        char result[PATH_MAX];
        ssize_t count = readlink("/proc/self/exe", result, PATH_MAX);
        if (count > 0 && count < PATH_MAX)
        {
            result[count] = '\0';
            filePath = string(result);
        }
#endif	// WIN32
        
        return filePath;
    }
    
    string Directory::getAppDirPath()
    {
#if __APPLE__
        char result[PATH_MAX];
        if(getcwd(result, PATH_MAX) != NULL)
        {
            return string(result);
        }
        return "";
#else
        return Path::getDirectoryName(getAppFilePath());
#endif
    }
    
    string Directory::getAppFileName()
    {
        return Path::getFileName(getAppFilePath());
    }
}
