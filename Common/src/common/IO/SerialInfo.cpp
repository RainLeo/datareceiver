//
//  ChannelInfo.cpp
//  common
//
//  Created by baowei on 15/7/31.
//  Copyright (c) 2015 com. All rights reserved.
//

#include "SerialInfo.h"
#include "common/system/Convert.h"
#include "common/data/PrimitiveType.h"

namespace Common
{
    SerialInfo::SerialInfo(const string& portName)
    {
        this->portName = portName;
        this->baudRate = 9600;
        this->dataBits = DATA_8;
        this->stopBits = STOP_1;
        this->parity = PAR_NONE;
        this->handshake = FLOW_OFF;
        this->rtsEnable = false;
        this->dtrEnable = false;
        this->useSignal = false;
    }
    void SerialInfo::read(XmlTextReader& reader)
    {
        portName = reader.getAttribute("portname");
        Convert::parseInt32(reader.getAttribute("baudrate"), baudRate);
        dataBits = parseDataBits(reader.getAttribute("databits"));
        stopBits = parseStopBits(reader.getAttribute("stopbits"));
        parity = parseParity(reader.getAttribute("parity"));
        handshake = parseHandshake(reader.getAttribute("handshake"));
        Convert::parseBoolean(reader.getAttribute("rtsenable"), rtsEnable);
        Convert::parseBoolean(reader.getAttribute("dtrenable"), dtrEnable);
        Convert::parseBoolean(reader.getAttribute("usesignal"), useSignal);
    }
    void SerialInfo::write(XmlTextWriter& writer)
    {
        writer.writeAttributeString("portname", portName);
        writer.writeAttributeString("baudrate", Convert::convertStr(baudRate));
        writer.writeAttributeString("databits", convertDataBitsStr(dataBits));
        writer.writeAttributeString("stopbits", convertStopBitsStr(stopBits));
        writer.writeAttributeString("parity", convertParityStr(parity));
        writer.writeAttributeString("handshake", convertHandshakeStr(handshake));
        writer.writeAttributeString("rtsenable", Convert::convertStr(rtsEnable));
        writer.writeAttributeString("dtrenable", Convert::convertStr(dtrEnable));
        writer.writeAttributeString("usesignal", Convert::convertStr(useSignal));
    }
	string SerialInfo::dataBitsStr() const
	{
		return convertDataBitsStr(dataBits);
	}
	string SerialInfo::parityStr() const
	{
		return convertParityStr(parity);
	}
	string SerialInfo::stopBitsStr() const
	{
		return convertStopBitsStr(stopBits);
	}
	string SerialInfo::handshakeStr() const
	{
		return convertHandshakeStr(handshake);
	}
    SerialInfo::DataBitsType SerialInfo::parseDataBits(const string& str)
    {
        if (str == "8")
            return DATA_8;
        else if (str == "7")
            return DATA_7;
        else if (str == "6")
            return DATA_6;
        else if (str == "5")
            return DATA_5;
        else
        {
            return DATA_8;
        }
    }
    string SerialInfo::convertDataBitsStr(DataBitsType dataBits)
    {
        switch (dataBits)
        {
            case SerialInfo::DATA_5:
                return "5";
            case SerialInfo::DATA_6:
                return "6";
            case SerialInfo::DATA_7:
                return "7";
            case SerialInfo::DATA_8:
            default:
                return "8";
        }
    }
    
    SerialInfo::ParityType SerialInfo::parseParity(const string& str)
    {
        if (str == "N" || str == "n")
            return PAR_NONE;
        else if (str == "O" || str == "o")
            return PAR_ODD;
        else if (str == "E" || str == "e")
            return PAR_EVEN;
        else if (str == "M" || str == "m")
            return PAR_MARK;
        else if (str == "S" || str == "s")
            return PAR_SPACE;
        else
        {
            return PAR_NONE;
        }
    }
    string SerialInfo::convertParityStr(ParityType parity)
    {
        switch (parity)
        {
            case SerialInfo::PAR_NONE:
                return "N";
            case SerialInfo::PAR_ODD:
                return "O";
            case SerialInfo::PAR_EVEN:
                return "E";
            case SerialInfo::PAR_MARK:
                return "M";
            case SerialInfo::PAR_SPACE:
                return "S";
            default:
                return "N";
        }
    }
    
    SerialInfo::StopBitsType SerialInfo::parseStopBits(const string& str)
    {
        if (str == "1")
            return STOP_1;
        else if (str == "1.5")
            return STOP_1_5;
        else if (str == "2")
            return STOP_2;
        else
        {
            return STOP_1;
        }
    }
    string SerialInfo::convertStopBitsStr(StopBitsType stopBits)
    {
        switch (stopBits)
        {
            case SerialInfo::STOP_1:
                return "1";
            case SerialInfo::STOP_1_5:
                return "1.5";
            case SerialInfo::STOP_2:
                return "2";
            default:
                return "1";
        }
    }
    
    SerialInfo::HandshakeType SerialInfo::parseHandshake(const string& str)
    {
        if (String::stringEquals("OFF", str, true))
            return FLOW_OFF;
        else if (String::stringEquals("HARDWARE", str, true))
            return FLOW_HARDWARE;
        else if (String::stringEquals("XONXOFF", str, true))
            return FLOW_XONXOFF;
        else
        {
            return FLOW_OFF;
        }
    }
    string SerialInfo::convertHandshakeStr(HandshakeType handshake)
    {
        switch (handshake)
        {
            case SerialInfo::FLOW_OFF:
                return "OFF";
            case SerialInfo::FLOW_HARDWARE:
                return "HARDWARE";
            case SerialInfo::FLOW_XONXOFF:
                return "XONXOFF";
            default:
                return "OFF";
        }
    }
}
