#ifndef MEMORYSTREAM_H
#define MEMORYSTREAM_H

#include "Stream.h"

namespace Common
{
	class COMMON_EXPORT MemoryStream : public Stream
	{
	public:
        MemoryStream(uint capacity = DefaultCapacity);
        MemoryStream(const ByteArray* buffer);
        MemoryStream(ByteArray* buffer, bool autoDelete = true);
        MemoryStream(const byte* buffer, int count, uint capacity = DefaultCapacity);
        ~MemoryStream();

		int64_t write(const byte* array, int offset, int count) override;
		int64_t read(byte* array, int offset, int count) override;

		int64_t position() const override;
		int64_t length() const override;
		bool seek(int64_t position, SeekOrigin origin = SeekOrigin::SeekBegin) override;

        void clear();
        
        const ByteArray* buffer() const;
        
        void copyTo(byte* buffer) const;
        void copyTo(ByteArray& buffer) const;
        
    private:
        int minInteger(int a, int b);

	private:
		ByteArray* _buffer;
        bool _copyBuffer;
		int64_t _position;
        
        const static int DefaultCapacity = 1024;
	};
}

#endif // MEMORYSTREAM_H
