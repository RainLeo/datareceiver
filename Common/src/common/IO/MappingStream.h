#ifndef MAPPINGSTREAM_H
#define MAPPINGSTREAM_H

#include "common/IO/Stream.h"
#include "common/IO/FileStream.h"
#include "common/data/Vector.h"

namespace Common
{
    class MappingStream : public Stream
    {
    public:
#if WIN32
		typedef void* ViewMapping;
#else
		typedef int ViewMapping;
#endif
		typedef void* ViewAccessor;

        class View
        {
        public:
            int64_t offset;
            int64_t size;
            
			View(ViewMapping mapping, int64_t offset, int64_t size);
            ~View();
            
            int64_t write(int64_t position, const byte* array, int offset, int count);
            int64_t read(int64_t position, byte* array, int offset, int count);
            
            void close();
            bool flush();
            
        private:
            bool isValid() const;
            void updateViewAccessor();
            
        private:
            ViewAccessor _accessor;
			ViewMapping _mapping;
        };
        typedef Vector<View> Views;
        
        MappingStream(const string& fileName, int64_t fileSize, FileAccess access = FileReadWrite);
        ~MappingStream();
        
        int64_t write(const byte* array, int offset, int count) override;
        int64_t read(byte* array, int offset, int count) override;
        
        int64_t position() const override;
        int64_t length() const override;
        bool seek(int64_t offset, SeekOrigin origin = SeekOrigin::SeekBegin) override;
        
        bool canWrite() const override;
        bool canRead() const override;
        bool canSeek() const override;
        
        bool isOpen() const;
        void close();
        
        void flush();
        
    private:
        int64_t viewSize() const;
        
    private:
        // https://connect.microsoft.com/VisualStudio/feedback/details/552859/memorymappedviewaccessor-flush-throws-ioexception?wa=wsignin1.0
        const int64_t WriteViewSize = 20L * 1024 * 1024;
        const int64_t ReadViewSize = 256L * 1024 * 1024;
        
        int64_t mFileSize;
        Views _views;
        FileAccess mAccess;
        int64_t mPosition;
        
#if WIN32
		void* _mapFile;
		void* _file;
#else
		FileStream* _mapFile;
#endif
    };
}

#endif /* defined(MAPPINGSTREAM_H) */
