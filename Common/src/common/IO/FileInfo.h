#ifndef FILEINFO_H
#define FILEINFO_H

#include <string>
#include <stdint.h>

using namespace std;

namespace Common
{
	class FileInfo
	{
	public:
		enum FileAttributes
		{
			Unkown = 0x0,
			Read = 0x0100,
			Write = 0x0080,
			Execute = 0x0040,
			Regular = 0x8000,
		};

		FileInfo() : FileInfo("")
		{
		}
		FileInfo(const string& name) : FileInfo(name.c_str())
		{
		}
		FileInfo(const char* name)
		{
			if (name != NULL)
			{
				_name = name;
			}
			_attributes = FileAttributes::Unkown;
			_size = 0;
			_modifiedTime = 0;

			stat();
		}

		inline FileAttributes attributes() const
		{
			return _attributes;
		}

		inline bool isReadOnly() const
		{
			FileAttributes attr = attributes();
			return ((attr & Read) != 0) && ((attr & Write) == 0);
		}
		inline bool isWritable() const
		{
			FileAttributes attr = attributes();
			return ((attr & Write) != 0);
		}
		inline int64_t size() const
		{
			return _size;
		}
		inline bool exists() const
		{
			return File::exists(_name);
		}
		inline time_t modifiedTime() const
		{
			return _modifiedTime;
		}

	private:
		void stat();

	private:
		string _name;

		FileInfo::FileAttributes _attributes;
		int64_t _size;
		time_t _modifiedTime;
	};
}

#endif // FILEINFO_H
