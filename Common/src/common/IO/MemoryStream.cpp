#include "MemoryStream.h"

namespace Common
{
    MemoryStream::MemoryStream(uint capacity) : _buffer(nullptr), _copyBuffer(true), _position(0)
    {
        _buffer = new ByteArray(capacity);
    }
    MemoryStream::MemoryStream(const ByteArray* buffer) : _buffer(nullptr), _copyBuffer(true), _position(0)
    {
        _buffer = new ByteArray(buffer);
    }
    MemoryStream::MemoryStream(ByteArray* buffer, bool copyBuffer) : _buffer(nullptr), _copyBuffer(copyBuffer), _position(0)
    {
        if(_copyBuffer)
            _buffer = new ByteArray(buffer);
        else
            _buffer = buffer;
    }
    MemoryStream::MemoryStream(const byte* buffer, int count, uint capacity) : _buffer(nullptr), _copyBuffer(true), _position(0)
    {
        _buffer = new ByteArray(buffer, count, capacity);
    }
    MemoryStream::~MemoryStream()
    {
        if(_copyBuffer)
            delete _buffer;
        _buffer = nullptr;
    }
    
    int64_t MemoryStream::position() const
    {
        return _position;
    }
    int64_t MemoryStream::length() const
    {
        return _buffer->count();
    }
    bool MemoryStream::seek(int64_t position, SeekOrigin origin)
    {
        if (origin == SEEK_SET)
        {
            if (position >= 0 && position <= (int)_buffer->count())
            {
                _position = position;
                return true;
            }
        }
        else if (origin == SEEK_CUR)
        {
            if (position >= 0 && position <= (int)_buffer->count() - _position)
            {
                _position += position;
                return true;
            }
        }
        else if (origin == SEEK_END)
        {
            if (position >= 0 && position <= (int)_buffer->count() - _position)
            {
                _position = (int)_buffer->count() - position;
                return true;
            }
        }
        return false;
    }
    void MemoryStream::clear()
    {
        seek(0);
        _buffer->clear();
    }
    const ByteArray* MemoryStream::buffer() const
    {
        return _buffer;
    }
    int64_t MemoryStream::write(const byte* array, int offset, int count)
    {
        if (_position == (int64_t)_buffer->count())
        {
            _buffer->addRange((array + offset), count);
        }
        else if (_position < (int)_buffer->count())
        {
            _buffer->setRange((uint)_position, (array + offset), count);
        }
        else	// _position > _buffer->count()
        {
            int c = (int)_position - (int)_buffer->count();
            byte* zero = new byte[c];
            memset(zero, 0, c);
            _buffer->addRange(zero, c);
            _buffer->addRange((array + offset), count);
            delete[] zero;
        }
        _position += count;
        return count;
    }
    int MemoryStream::minInteger(int a, int b)
    {
        return a < b ? a : b;
    }
    int64_t MemoryStream::read(byte* array, int offset, int count)
    {
        if (_position >= 0 && _position < (int)length())
        {
            byte* temp = _buffer->data();
            int bufferCount = (int)_buffer->count();
            memcpy(array + offset, temp + (int)_position, minInteger(count, bufferCount - (int)_position));
            _position += count;
            return count;
        }
        return 0;
    }
    
    void MemoryStream::copyTo(byte* buffer) const
    {
        byte* temp = _buffer->data();
        memcpy(buffer, temp, (size_t)length());
    }
    void MemoryStream::copyTo(ByteArray& buffer) const
    {
        buffer.addRange(_buffer);
    }
}
