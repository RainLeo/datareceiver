#include "File.h"
#include "../diag/Debug.h"
#if WIN32
#include <Windows.h>
#include <memory.h>
#include <fcntl.h>
#include <io.h>
#include <direct.h>
#include <sys/stat.h>
#elif __APPLE__
#include <sys/param.h>
#include <sys/mount.h>
#include <libgen.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#else
#include <sys/vfs.h>
#include <libgen.h>
#include <limits.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#endif

namespace Common
{
    bool File::exists(const char* path)
    {
        struct stat s;
        int err = stat(path, &s);
        if(-1 == err) {
            if(ENOENT == errno) {
                /* does not exist */
            } else {
            }
        } else {
            if(S_ISDIR(s.st_mode)) {
                /* it's a dir */
            } else {
                /* exists but is no dir */
                return true;
            }
        }
        return false;
    }
    bool File::exists(const string& path)
    {
        return exists(path.c_str());
    }
    
    bool File::deleteFile(const char* path)
    {
        if (path == NULL)
            return false;
        
        if (!exists(path))
            return false;
        
        return remove(path) == 0 ? true : false;
    }
    bool File::deleteFile(const string& path)
    {
        return deleteFile(path.c_str());
    }
    
    bool File::move(const char* sourceFileName, const char* destFileName)
    {
        if (sourceFileName == NULL)
            return false;
        if (destFileName == NULL)
            return false;
        
        if (exists(sourceFileName))
        {
            if (exists(destFileName))
            {
                deleteFile(destFileName);
            }
#if WIN32
            return MoveFile(sourceFileName, destFileName) ? true : false;
#else
            return move_file(sourceFileName, destFileName);
#endif
        }
        return false;
    }
    bool File::move(const string& sourceFileName, const string& destFileName)
    {
        return move(sourceFileName.c_str(), destFileName.c_str());
    }
    
    bool File::copy(const char* sourceFileName, const char* destFileName, bool overwrite)
    {
        if (sourceFileName == NULL)
            return false;
        if (destFileName == NULL)
            return false;
        
        if (exists(sourceFileName))
        {
#if WIN32
            return CopyFile(sourceFileName, destFileName, overwrite ? FALSE : TRUE) ? true : false;
#else
            return copy_file(sourceFileName, destFileName, overwrite ? false : true);
#endif
        }
        return false;
    }
    bool File::copy(const string& sourceFileName, const string& destFileName, bool overwrite)
    {
        return copy(sourceFileName.c_str(), destFileName.c_str(), overwrite);
    }
    
#if !WIN32
    bool File::write_file (int src_fd, int dest_fd, struct stat *st_src)
    {
        int remain, n;
        char *buf, *wbuf;
        int buf_size = st_src->st_blksize;
        
        buf_size = buf_size < 8192 ? 8192 : (buf_size > 65536 ? 65536 : buf_size);
        buf = (char *) malloc (buf_size);
        
        for (;;) {
            remain = (int)read (src_fd, buf, buf_size);
            if (remain < 0) {
                if (errno == EINTR)
                    continue;
                
                free (buf);
                return false;
            }
            if (remain == 0) {
                break;
            }
            
            wbuf = buf;
            while (remain > 0) {
                if ((n = (int)write (dest_fd, wbuf, remain)) < 0) {
                    if (errno == EINTR)
                        continue;
                    
                    free (buf);
                    return false;
                }
                
                remain -= n;
                wbuf += n;
            }
        }
        
        free (buf);
        return true;
    }
    
    /**
     * CopyFile:
     * @name: a pointer to a NULL-terminated unicode string, that names
     * the file to be copied.
     * @dest_name: a pointer to a NULL-terminated unicode string, that is the
     * new name for the file.
     * @fail_if_exists: if TRUE and dest_name exists, the copy will fail.
     *
     * Copies file @name to @dest_name
     *
     * Return value: %TRUE on success, %FALSE otherwise.
     */
    bool File::copy_file (const char *name, const char *dest_name, bool fail_if_exists)
    {
        int src_fd, dest_fd;
        struct stat st, dest_st;
        struct utimbuf dest_time;
        bool ret = true;
        int ret_utime;
        
        if(name==NULL) {
            return(false);
        }
        
        if(dest_name==NULL) {
            return(false);
        }
        
        src_fd = open (name, O_RDONLY, 0);
        if (src_fd < 0) {
            return(false);
        }
        
        if (fstat (src_fd, &st) < 0) {
            return(false);
        }
        
        /* Before trying to open/create the dest, we need to report a 'file busy'
         * error if src and dest are actually the same file. We do the check here to take
         * advantage of the IOMAP capability */
        if (!stat (dest_name, &dest_st) && st.st_dev == dest_st.st_dev &&
            st.st_ino == dest_st.st_ino) {
            return (false);
        }
        
        if (fail_if_exists) {
            dest_fd = open (dest_name, O_WRONLY | O_CREAT | O_EXCL, st.st_mode);
        } else {
            /* FIXME: it kinda sucks that this code path potentially scans
             * the directory twice due to the weird SetLastError()
             * behavior. */
            dest_fd = open (dest_name, O_WRONLY | O_TRUNC, st.st_mode);
            if (dest_fd < 0) {
                /* The file does not exist, try creating it */
                dest_fd = open (dest_name, O_WRONLY | O_CREAT | O_TRUNC, st.st_mode);
            } else {
                /* Apparently this error is set if we
                 * overwrite the dest file
                 */
            }
        }
        if (dest_fd < 0) {
            close (src_fd);
            
            return(false);
        }
        
        if (!write_file (src_fd, dest_fd, &st))
            ret = false;
        
        close (src_fd);
        close (dest_fd);
        
        dest_time.modtime = st.st_mtime;
        dest_time.actime = st.st_atime;
        ret_utime = utime (dest_name, &dest_time);
        if (ret_utime == -1)
            Debug::writeFormatLine("%s: file [%s] utime failed: %s", __func__, dest_name, strerror(errno));
        
        return ret;
    }
    bool File::move_file (const char *name, const char *dest_name)
    {
        int result, errno_copy;
        struct stat stat_src, stat_dest;
        bool ret = false;
        
        if(name==NULL) {
            return(false);
        }
        
        if(dest_name==NULL) {
            return(false);
        }
        
        /*
         * In C# land we check for the existence of src, but not for dest.
         * We check it here and return the failure if dest exists and is not
         * the same file as src.
         */
        if (stat (name, &stat_src) < 0) {
            if (errno != ENOENT || lstat (name, &stat_src) < 0) {
                return false;
            }
        }
        
        if (!stat (dest_name, &stat_dest)) {
            if (stat_dest.st_dev != stat_src.st_dev ||
                stat_dest.st_ino != stat_src.st_ino) {
                return false;
            }
        }
        
        result = rename (name, dest_name);
        errno_copy = errno;
        
        if (result == -1) {
            switch(errno_copy) {
                case EEXIST:
                    break;
                    
                case EXDEV:
                    /* Ignore here, it is dealt with below */
                    break;
                    
                default:
                    break;
            }
        }
        
        if (result != 0 && errno_copy == EXDEV) {
            if (S_ISDIR (stat_src.st_mode)) {
                return false;
            }
            /* Try a copy to the new location, and delete the source */
            if (copy (name, dest_name, true)==false) {
                /* CopyFile will set the error */
                return(false);
            }
            
            return(deleteFile (name));
        }
        
        if (result == 0) {
            ret = true;
        }
        
        return(ret);
    }
#endif
}
