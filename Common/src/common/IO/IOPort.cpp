#include "../diag/Stopwatch.h"
#include "../diag/Debug.h"
#include "../data/ByteArray.h"
#include "../thread/TickTimeout.h"
#include "IOPort.h"
#if WIN32
#include <Windows.h>
#endif

using namespace Common;

namespace Common
{
	IOPort::IOPort(const string& name)
	{
		_portName = name;
		_handle = invalidHandle;
	}

	IOPort::~IOPort()
	{
		close();
	}

	bool IOPort::open()
	{
#ifdef DEBUG
		Stopwatch sw(Convert::convertStr("IOPort::Open, name: %s", _portName.c_str()), 200);
#endif
#ifdef WIN32
		string portExt = "\\\\.\\";
		if(_portName.length() > 4)
		{
			_portName = portExt + _portName;
		}
		if (isOpen())
		{
			close();
		}
		_handle = CreateFileA(_portName.c_str(), 
			GENERIC_READ|GENERIC_WRITE,
			FILE_SHARE_READ|FILE_SHARE_WRITE, 
			NULL, 
			OPEN_EXISTING, 
			0,
			NULL);
		if (_handle == INVALID_HANDLE_VALUE)
		{
			return false;
		}

		COMMTIMEOUTS ct;
		ct.ReadIntervalTimeout = 10;
		ct.ReadTotalTimeoutConstant = 10;
		ct.ReadTotalTimeoutMultiplier = 0;
		ct.WriteTotalTimeoutMultiplier = 10;
		ct.WriteTotalTimeoutConstant = 3000;
		SetCommTimeouts(_handle, &ct);
#else
		if (isOpen()) 
		{
			close();
		}
		/*open the port*/
		_handle = ::open(_portName.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);
		if(_handle < 0)
		{
			Debug::writeFormatLine("Can not find the comport, name: %s", _portName.c_str());
			return false;
		}

		//struct termios ts;
		//tcgetattr(_handle, &ts);
		//ts.c_cc[VTIME] = 150;
		//ts.c_cc[VMIN] = 1;
		//tcsetattr(_handle, TCSANOW, &ts);
#endif
		return true;
	}

	void IOPort::close()
	{
#ifdef DEBUG
		Stopwatch sw(Convert::convertStr("IOPort::Close, name: %s", _portName.c_str()), 200);
#endif
		if (isOpen()) 
		{
#if WIN32
			CloseHandle(_handle);
#else
			::close(_handle);
#endif
			_handle = invalidHandle;
		}
	}

	size_t IOPort::available()
	{
		size_t nb = 0;
		if(isOpen())
		{
#ifdef WIN32
			DWORD dwErrorFlags = 0;
			COMSTAT ComStat;
			memset(&ComStat, 0, sizeof(ComStat));
			ClearCommError(_handle, &dwErrorFlags, &ComStat); 
			nb = ComStat.cbInQue;
#else
			//Debug::writeLine("IOPort::available()");
			ioctl(_handle, FIONREAD, &nb);
#endif
		}
		return nb;
	}

	size_t IOPort::write(const char *data, uint maxlen)
	{
		size_t len = 0;
		if(isOpen())
		{
#ifdef WIN32
			LPDWORD length=0;   
			COMSTAT ComStat;
			DWORD dwErrorFlags;
			ClearCommError(_handle,&dwErrorFlags,&ComStat);
			if (!WriteFile(_handle, (void*)data, (DWORD)maxlen,(LPDWORD) &length, NULL)) 
			{
				length = (LPDWORD)-1;
			}
			len = (int)length;
#else
			len = ::write(_handle, data, maxlen);

			//struct pollfd pinfo;
			//int n;
			//int timeout = 2000;
			//int offset = 0;

			//pinfo.fd = _handle;
			//pinfo.events = POLLOUT;
			//pinfo.revents = POLLOUT;

			//n = maxlen;

			//while (n > 0)
			//{
			//	size_t t;
			//	if (timeout != 0)
			//	{
			//		int c;

			//		while ((c = ::poll (&pinfo, 1, timeout)) == -1 && errno == EINTR)
			//			;
			//		if (c == -1)
			//			return 0;
			//	}		

			//	do
			//	{
			//		t = ::write (_handle, data + offset, n);
			//	} while (t == -1 && errno == EINTR);

			//	if (t < 0)
			//		return 0;

			//	offset += t;
			//	n -= t; 
			//}

			//len = maxlen;
#endif
		}
		return len;
	}

	size_t IOPort::read(char *data, uint maxlen)
	{
		size_t len = 0;
		if(isOpen())
		{
#ifdef WIN32
			LPDWORD length=0;
			if (!ReadFile(_handle, (void*)data, (DWORD)maxlen, (LPDWORD)&length, NULL)) 
			{
				length = (LPDWORD)-1;
			}
			len = (int)length;
#else
			len = ::read(_handle, data, maxlen);
#endif
		}
		return len;
	}

	size_t IOPort::read(char *data, uint maxlen, uint timeout)
	{
		size_t len = 0;
		if(isOpen())
		{
#ifdef WIN32
			uint startTime = TickTimeout::GetCurrentTickCount();
			uint deadTime = TickTimeout::GetDeadTickCount(startTime, timeout);
			do
			{
				int count = available();
				if (count >= (int)maxlen)
				{
					len = maxlen;
					break;
				}

				if (TickTimeout::IsTimeout(startTime, deadTime, TickTimeout::GetCurrentTickCount()))
				{
					len = count;
					break;
				}

				Thread::msleep(1);
			} while (true);

			return read(data, len);
#else
			const int MAX_COUNT = 512;
			size_t red=0;
			char revda[MAX_COUNT];
			int r_num=0;
			size_t i;

			fd_set readfds;
			struct timeval tv_timeout;

            tv_timeout.tv_sec = timeout / 1000;
            tv_timeout.tv_usec = 1000 * (timeout % 1000);

			while(1)
			{
				FD_ZERO (&readfds);
				FD_SET (_handle, &readfds);

				red = select (_handle+1, &readfds, NULL, NULL, &tv_timeout);
				if(red > 0)
				{ 
					if(red > MAX_COUNT)
						red = MAX_COUNT;

					red = ::read(_handle, &revda, red);
					if(red > 0)
					{
						for(i=0;i<red;i++)
						{
							data[r_num] = revda[i];
							r_num ++;

							if(r_num >= (int)maxlen)
								return( r_num );				
						}
					}
				}
				else
				{
					return( -1 );
				}
			}//end while
			return( -1 );
#endif
		}
		return len;
	}
}
