#include <stdlib.h>
#include "../diag/Debug.h"
#include "../diag/Stopwatch.h"
#include "../thread/Locker.h"
#include "iconv.h"
#include "SqliteClient.h"

namespace Database
{
	SqliteClient::SqliteClient(void) : _sqliteDb(NULL)
	{
	}

	SqliteClient::~SqliteClient(void)
	{
		if (_sqliteDb != NULL)
		{
			close();
		}
	}

	bool SqliteClient::open(const string& connectionStr)
	{
		Locker locker(&_dbMutex);

		// connectionStr is the db file name.
		int result = sqlite3_open(connectionStr.c_str(), &_sqliteDb);
		if (result == SQLITE_OK)
		{
			// http://blog.quibb.org/2010/08/fast-bulk-inserts-into-sqlite/

			//int result;
			//DataTable table;
			//const int length = 8;
			//string names[length] = {"PRAGMA synchronous", "PRAGMA count_changes", "PRAGMA journal_mode",
			//	"PRAGMA temp_store", "PRAGMA locking_mode", "PRAGMA fullfsync", "PRAGMA read_uncommitted", "PRAGMA recursive_triggers"};
			//for (int i = 0; i < length; i++)
			//{
			//	table.clear();
			//	result = executeSqlQuery(names[i].c_str(), table);
			//	if(result == SQLITE_OK && table.getRows()->count() > 0)
			//	{
			//		Debug::writeFormatLine("PRAGMA %s = %d", table.getColumns()->at(0)->name().c_str(), table.getRows()->at(0)->getCells()->at(0)->getValue().nValue);
			//	}
			//}

			//executeSql("PRAGMA synchronous=OFF");
			//executeSql("PRAGMA count_changes=OFF");
			//executeSql("PRAGMA journal_mode=MEMORY");
			//executeSql("PRAGMA temp_store=MEMORY");
		}
		return isSuccessed(result);
	}

	bool SqliteClient::close()
	{
		Locker locker(&_dbMutex);

		int result = sqlite3_close(_sqliteDb);
		_sqliteDb = NULL;
		return isSuccessed(result);
	}

	bool SqliteClient::executeSql(const string& sql, bool transaction)
	{
		Locker locker(&_dbMutex);

		int result;
		if (transaction)
		{
			result = beginTransactionInner();
			if (!isSuccessed(result))
			{
				return false;
			}
		}
		result = executeSqlInner(sql);
		if (!isSuccessed(result))
		{
			if (transaction)
			{
				rollbackTransactionInner();
			}
			return false;
		}
		if (transaction)
		{
			result = commitTransactionInner();
			if (!isSuccessed(result))
			{
				return false;
			}
		}
		return true;
	}

	bool SqliteClient::beginTransaction()
	{
		Locker locker(&_dbMutex);

		int result = beginTransactionInner();
		return isSuccessed(result);
	}
	bool SqliteClient::commitTransaction()
	{
		Locker locker(&_dbMutex);

		int result = commitTransactionInner();
		return isSuccessed(result);
	}
	bool SqliteClient::rollbackTransaction()
	{
		Locker locker(&_dbMutex);

		int result = rollbackTransactionInner();
		return isSuccessed(result);
	}

	bool SqliteClient::executeSqlQuery(const string& sql, DataTable& table)
	{
		Locker locker(&_dbMutex);

		int result = executeSqlQueryInner(sql, table);
		return isSuccessed(result);
	}
	bool SqliteClient::executeSqlInsert(const DataTable& table, bool replace)
	{
		Locker locker(&_dbMutex);

		int result = executeSqlInsertInner(table, replace);
		return isSuccessed(result);
	}

	int SqliteClient::executeSqlInner(const string& sql)
	{
#if DEBUG
		Stopwatch sw("SqliteClient::executeSqlInner", 100);
#endif
#if WIN32
		String temp = String::GBKtoUTF8(sql);
		const char* str = temp.c_str();
#else
        const char* str = sql.c_str();
#endif
		int result = sqlite3_exec(_sqliteDb, str, NULL, NULL, NULL);
		if (result != SQLITE_OK)
		{
			printErrorInfo("sqlite3_exec", sql);
		}
		return result;
	}
	int SqliteClient::executeSqlInsertInner(const DataTable& table, bool replace)
	{
#if DEBUG
		Stopwatch sw("SqliteClient::executeSqlInsertInner", 100);
#endif
		if (table.name().empty())
			return -1;
		if (table.columnCount() == 0)
			return -2;
		if (table.rowCount() == 0)
			return -3;

		// such like '"INSERT INTO example VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7)";'
		string valuesStr = "";
		int columnCount = table.columnCount();
		for (int i = 0; i < columnCount; i++)
		{
			if (i != 0)
			{
				valuesStr += ", ";
			}
			valuesStr += Convert::convertStr("?%d", i + 1);
		}
		string sql;
		if (replace)
		{
			sql = Convert::convertStr("REPLACE INTO [%s] VALUES (%s)", table.name().c_str(), valuesStr.c_str());
		}
		else
		{
			sql = Convert::convertStr("INSERT OR IGNORE INTO [%s] VALUES (%s)", table.name().c_str(), valuesStr.c_str());
		}
		sqlite3_stmt *stmt;
		int result = sqlite3_prepare_v2(_sqliteDb, sql.c_str(), (int)sql.length(), &stmt, 0);
		if (result != SQLITE_OK)
		{
			printErrorInfo("sqlite3_prepare_v2", sql);
			return result;
		}

		result = beginTransactionInner();
		if (result != SQLITE_OK)
		{
			return result;
		}
		int rowCount = table.rowCount();
		for (int i = 0; i < rowCount; i++)
		{
			const DataRow* row = table.rows()->at(i);
			for (int j = 0; j < columnCount; j++)
			{
				const DataCell* cell = row->cells()->at(j);
				if (cell != NULL)
				{
					if (cell->isNullValue())
					{
						sqlite3_bind_null(stmt, j + 1);
					}
					else
					{
						ValueTypes type = cell->type();
						const Value& value = cell->value();
						switch (type)
						{
						case Null:
							sqlite3_bind_null(stmt, j + 1);
							break;
						case Integer32:
							sqlite3_bind_int(stmt, j + 1, value.nValue);
							break;
						case Date:
						{
							String str = DateTime(value.tValue).toString();
							result = sqlite3_bind_text(stmt, j + 1, str.c_str(), (int)str.length(), SQLITE_TRANSIENT);
							break;
						}
						case Timestamp:
						{
							String str = DateTime(value.tValue).toString(DateTime::YYYYMMDDHHMMSSfff);
							result = sqlite3_bind_text(stmt, j + 1, str.c_str(), (int)str.length(), SQLITE_TRANSIENT);
							break;
						}
						case Text:
						{
							if (value.strValue != NULL)
							{
								String str = String::GBKtoUTF8(value.strValue);
								result = sqlite3_bind_text(stmt, j + 1, str.c_str(), (int)str.length(), SQLITE_TRANSIENT);
							}
							else
							{
								sqlite3_bind_null(stmt, j + 1);
							}
							break;
						}
						case Float32:
						case Float64:
							sqlite3_bind_double(stmt, j + 1, value.dValue);
							break;
						default:
							assert(false);
							break;
						}
					}
				}
			}

			result = sqlite3_step(stmt);
			if (result != SQLITE_DONE)
			{
				printErrorInfo("sqlite3_step", sql);
			}

			result = sqlite3_reset(stmt);
			if (result != SQLITE_OK)
			{
				printErrorInfo("sqlite3_reset", sql);
			}
		}

		result = commitTransactionInner();
		if (result != SQLITE_OK)
		{
			return result;
		}
		sqlite3_finalize(stmt);
		if (result != SQLITE_OK)
		{
			printErrorInfo("sqlite3_finalize", sql);
			return result;
		}

		return SQLITE_OK;
	}
	int SqliteClient::executeSqlQueryInner(const string& sql, DataTable& table)
	{
#if DEBUG
		Stopwatch sw("SqliteClient::executeSqlQueryInner", 100);
#endif
		sqlite3_stmt *stmt;
		int result = sqlite3_prepare_v2(_sqliteDb, sql.c_str(), (int)sql.length(), &stmt, 0);
		if (result != SQLITE_OK)
		{
			printErrorInfo("sqlite3_prepare_v2", sql);
			return result;
		}

		if (!table.name().empty())
		{
			const char* name = sqlite3_column_table_name(stmt, 0);
			table.setName(name != NULL ? name : "temp");
		}

#if DEBUG
		sw.setInfo(Convert::convertStr("SqliteClient::executeSqlQueryInner, the table name is '%s'", table.name().c_str()));
#endif

		int columnCount = sqlite3_column_count(stmt);
		int table_columnCount = table.columnCount();
		if ((table_columnCount > 0 && table_columnCount == columnCount) ||
			table_columnCount == 0)
		{
			if (table_columnCount == 0)
			{
				for (int i = 0; i < columnCount; i++)
				{
					char* nameStr = (char*)sqlite3_column_name(stmt, i);
					string name;
					if (nameStr != NULL)
					{
						name = nameStr;
					}
					else
					{
						char temp[32];
						sprintf(temp, "tempCol%d", i);
						name = temp;
					}

					const char* typeStr = sqlite3_column_decltype(stmt, i);
					string type = typeStr != NULL ? typeStr : "null";
					DataColumn* column = new DataColumn(name, getColumnType(type));
					table.addColumn(column);
				}
			}

			while (sqlite3_step(stmt) == SQLITE_ROW)
			{
				DataRow* row = new DataRow();
				for (int i = 0; i < columnCount; i++)
				{
					DataColumn* column = table.columns()->at(i);
					ValueTypes type = column->type();
					char* str = (char*)sqlite3_column_text(stmt, i);
					Value value = convertValue(type, str);
					DataCell* cell = new DataCell(column, value);
					row->addCell(cell);
				}
				table.addRow(row);
			}
		}

		result = sqlite3_finalize(stmt);
		if (result != SQLITE_OK)
		{
			printErrorInfo("sqlite3_finalize", sql);
			return result;
		}

		return SQLITE_OK;
	}

	int SqliteClient::beginTransactionInner()
	{
		return executeSqlInner("BEGIN TRANSACTION");
	}
	int SqliteClient::commitTransactionInner()
	{
		return executeSqlInner("COMMIT TRANSACTION");
	}
	int SqliteClient::rollbackTransactionInner()
	{
		return executeSqlInner("ROLLBACK TRANSACTION");
	}

	ValueTypes SqliteClient::getColumnType(int type)
	{
		switch (type)
		{
		case SQLITE_TEXT:
			return ValueTypes::Text;
		case SQLITE_INTEGER:
			return ValueTypes::Integer32;
		case SQLITE_FLOAT:
			return ValueTypes::Float32;
		case SQLITE_BLOB:
			return ValueTypes::Blob;
		case SQLITE_NULL:
			return ValueTypes::Null;
		default:
			assert(false);
			return ValueTypes::Null;
		}
	}
	ValueTypes SqliteClient::getColumnType(const string& type)
	{
		ValueTypes valueType = Null;
		if (type.find("int") != string::npos)
		{
			valueType = Integer32;
		}
		else if (type.find("char") != string::npos ||
			type.find("varchar") != string::npos ||
			type.find("nvarchar") != string::npos ||
			type.find("text") != string::npos)
		{
			valueType = Text;
		}
		else if (type.find("date") != string::npos)
		{
			valueType = Date;
		}
		else if (type.find("timestamp") != string::npos)
		{
			valueType = Timestamp;
		}
		else if (type.find("float") != string::npos ||
			type.find("number") != string::npos)
		{
			valueType = Float32;
		}
		else if (type.find("null") != string::npos)
		{
			valueType = Null;
		}
		else
		{
			assert(false);
		}
		return valueType;
	}

	string SqliteClient::getErrorMsg()
	{
		return _sqliteDb != NULL ? sqlite3_errmsg(_sqliteDb) : "";
	}
}
