#include "DataTable.h"
#include "../data/StringBuilder.h"

namespace Database
{
	DataColumn::DataColumn(const string& name, const string& type, bool pkey) : DataColumn(name, convertType(type), pkey)
	{
	}
	DataColumn::DataColumn(const string& name, const ValueTypes& type, bool pkey)
	{
		_name = name;
		_type = type;
		_primaryKey = pkey;
	}
	DataColumn::~DataColumn()
	{
	}

	ValueTypes DataColumn::type() const
	{
		return _type;
	}

	int DataColumn::isInteger() const
	{
		return (_type == Integer32);
	}

	int DataColumn::isDateTime() const
	{
		return (_type == Date);
	}

	int DataColumn::isString() const
	{
		return (_type == Text);
	}

	const string& DataColumn::name() const
	{
		return _name;
	}

	bool DataColumn::primaryKey() const
	{
		return _primaryKey;
	}

	ValueTypes DataColumn::convertType(const string& type)
	{
		ValueTypes valueType = Null;
		if (type.find("int") != string::npos)
		{
			valueType = Integer32;
		}
		else if (type.find("char") != string::npos ||
			type.find("varchar") != string::npos ||
			type.find("text") != string::npos ||
			type.find("nvarchar") != string::npos)
		{
			valueType = Text;
		}
		else if (type.find("date") != string::npos)
		{
			valueType = Date;
		}
		else if (type.find("float") != string::npos)
		{
			valueType = Float32;
		}
		else
		{
		}
		return valueType;
	}

	DataColumns::DataColumns(bool autoDelete, uint capacity) : Vector<DataColumn>(autoDelete, capacity)
	{
	}

	DataColumn* DataColumns::operator[](const string& i) const
	{
		return at(i);
	}
	DataColumn* DataColumns::at(uint i) const
	{
		return Vector < DataColumn >::at(i);
	}
	DataColumn* DataColumns::at(const string& columnName) const
	{
		for (uint i = 0; i < count(); i++)
		{
			DataColumn* column = at(i);
			if (String::stringEquals(column->name(), columnName, true))
			{
				return column;
			}
		}
		return NULL;
	}

	DataCell::DataCell(const DataColumn* column)
	{
		_column = column;
		_value = DbValue::nullValue();
	}
	DataCell::DataCell(const DataColumn* column, const Value value)
	{
		_column = column;
		_value = value;
	}
	DataCell::DataCell(const DataColumn* column, const DbValue& value)
	{
		_column = column;
		if (type() == Text && !value.isNullValue())
		{
			setStringValue(value.value().strValue);
		}
		else
		{
			_value = value.value();
		}

	}
	DataCell::DataCell(const DataColumn* column, int value)
	{
		_column = column;
		switch (type())
		{
		case Database::Integer32:
			_value.nValue = value;
			break;
		case Database::Float64:
			_value.dValue = value;
			break;
		case Database::Float32:
			_value.fValue = (float)value;
			break;
		case Database::Integer64:
			_value.lValue = value;
			break;
		default:
			_value = DbValue::nullValue();
			break;
		}
	}
	DataCell::DataCell(const DataColumn* column, uint value) : DataCell(column, (int)value)
	{
	}
	DataCell::DataCell(const DataColumn* column, int64_t value)
	{
		_column = column;
		switch (type())
		{
		case Database::Integer32:
			_value.nValue = (int)value;
			break;
		case Database::Float64:
			_value.dValue = (double)value;
			break;
		case Database::Float32:
			_value.fValue = (float)value;
			break;
		case Database::Integer64:
			_value.lValue = value;
			break;
		default:
			_value = DbValue::nullValue();
			break;
		}
	}
	DataCell::DataCell(const DataColumn* column, uint64_t value) : DataCell(column, (int64_t)value)
	{
	}
	DataCell::DataCell(const DataColumn* column, double value)
	{
		_column = column;
		_value.dValue = value;
	}
	DataCell::DataCell(const DataColumn* column, const char* value)
	{
		_column = column;
		setStringValue(value);
	}
	DataCell::DataCell(const DataColumn* column, const string& value)
	{
		_column = column;
		setStringValue(value.c_str());
	}
	DataCell::DataCell(const DataColumn* column, const DateTime& value)
	{
		_column = column;
		_value.tValue = value.ticks();
	}
	DataCell::DataCell(const DataColumn* column, const TimeSpan& value)
	{
		_column = column;
		_value.tValue = value.ticks();
	}
	DataCell::~DataCell()
	{
		ValueTypes t = type();
		if (t == Text && !isNullValue())
		{
			assert(_value.strValue);
			delete[] _value.strValue;
			_value.strValue = NULL;
		}
		_column = NULL;
	}

	ValueTypes DataCell::type() const
	{
		return _column != NULL ? _column->type() : Null;
	}

	const Value& DataCell::value() const
	{
		return _value;
	}

	const string DataCell::valueStr(bool hasQuote) const
	{
		if (isNullValue())
			return "null";

		switch (type())
		{
		case Null:
			return "null";
		case Integer32:
			return Convert::convertStr(_value.nValue);
		case Integer64:
			return Convert::convertStr(_value.lValue);
		case Float64:
			return Convert::convertStr(_value.dValue);
		case Text:
			return hasQuote ?
				Convert::convertStr("'%s'", _value.strValue) :
				_value.strValue;
		case Date:
		case Timestamp:
		{
			DateTime time(_value.tValue);
			return hasQuote ?
				Convert::convertStr("'%s'", time.toString().c_str()) :
				time.toString().c_str();
		}
		case Float32:
			return Convert::convertStr(_value.fValue);
		default:
			assert(false);
			return "";
		}
	}

	bool DataCell::matchColumnName(const char* columnName) const
	{
		return (_column != NULL && columnName != NULL) ?
			String::stringEquals(_column->name(), columnName, true) : false;
	}

	const string DataCell::columnName() const
	{
		return _column != NULL ? _column->name() : "";
	}

	void DataCell::setStringValue(Value& value, const char* str)
	{
		if (str != NULL)
		{
			value.strValue = new char[strlen(str) + 1];
			strcpy(value.strValue, str);
		}
		else
		{
			value.strValue = new char[1];
			value.strValue[0] = '\0';
		}
	}
	bool DataCell::isNullValue() const
	{
		return DbValue::isNullValue(_value);
	}
	void DataCell::setNullValue()
	{
		_value = DbValue::nullValue();
	}

	void DataCell::setStringValue(const char* str)
	{
		setStringValue(_value, str);
	}

	DataCells::DataCells(bool autoDelete, uint capacity) : Vector<DataCell>(autoDelete, capacity)
	{
	}

	DataCell* DataCells::operator[](const char* i) const
	{
		return at(i);
	}
	DataCell* DataCells::at(uint i) const
	{
		return Vector < DataCell >::at(i);
	}
	DataCell* DataCells::at(const string& columnName) const
	{
		//for (size_t i = 0; i < count(); i++)
		//{
		//	DataCell* cell = at(i);
		//	if (cell->matchColumnName(columnName.c_str()))
		//	{
		//		return cell;
		//	}
		//}
		//return NULL;
		uint position;
		if (_positions.at(columnName, position))
		{
			return at(position);
		}
		return NULL;
	}

	const Value DataCells::cellValue(const string& columnName) const
	{
		const DataCell* cell = at(columnName);
		if (cell == NULL)
			return DbValue::nullValue();

		return cell->value();
	}
	bool DataCells::hasColumn(const string& columnName) const
	{
		// return at(columnName) != NULL;
		return _positions.containsKey(columnName);
	}

	void DataCells::add(const DataCell* cell)
	{
		uint position = count();
		Vector < DataCell >::add(cell);

		_positions.add(Convert::toLower(cell->columnName()), position);
	}

	DataRow::DataRow()
	{
	}
	DataRow::~DataRow()
	{
	}

	void DataRow::addCell(const DataCell* cell)
	{
		_cells.add(cell);
	}

	const DataCells* DataRow::cells() const
	{
		return &_cells;
	}

	DataTable::DataTable()
	{
		_name = "";
	}
	DataTable::DataTable(const string& name)
	{
		setName(name);
	}
	DataTable::DataTable(const char* name)
	{
		setName(name);
	}
	DataTable::~DataTable()
	{
		clear();
	}

	const string& DataTable::name() const
	{
		return _name;
	}

	void DataTable::setName(const string& name)
	{
		_name = name;
	}

	void DataTable::addRow(const DataRow* row)
	{
		_rows.add(row);
	}
	const DataRows* DataTable::rows() const
	{
		return &_rows;
	}
	uint DataTable::rowCount() const
	{
		return _rows.count();
	}

	void DataTable::addColumn(const DataColumn* column)
	{
		_columns.add(column);
	}
	const DataColumns* DataTable::columns() const
	{
		return &_columns;
	}

	uint DataTable::columnCount() const
	{
		return _columns.count();
	}

	void DataTable::clear()
	{
		_name = "";
		_rows.clear();
		_columns.clear();
	}

	const string DataTable::anyColumnNameStr(bool hasTableName) const
	{
		StringBuilder sb;
		if (hasTableName)
		{
			sb.append(name());
			sb.append(".");
		}
		sb.append("*");
		return sb.toString();
	}
	const string DataTable::columnNameStr(bool hasTableName, const StringArray* excludedNames, const char* splitStr) const
	{
		StringBuilder sb;
		for (uint i = 0; i < columnCount(); i++)
		{
			string cname = _columns.at(i)->name();
			if (excludedNames == NULL ||
				(excludedNames != NULL && !excludedNames->contains(cname, true)))
			{
				if (i > 0)
				{
					sb.append(splitStr);
				}
				if (hasTableName)
				{
					sb.append(name());
					sb.append(".");
				}
				sb.append(cname);
			}
		}
		return sb.toString();
	}
}