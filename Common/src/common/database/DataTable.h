#ifndef DATATABLE_H
#define DATATABLE_H

#include <assert.h>
#include <string>
#include "../data/Vector.h"
#include "../data/Dictionary.h"
#include "../system/DateTime.h"
#include "../system/Convert.h"
#include "../IO/Stream.h"
#include "DbValue.h"

using namespace std;
using namespace Common;

namespace Database
{
	class DataColumn
	{
	public:
		DataColumn(const string& name, const string& type, bool pkey = false);
		DataColumn(const string& name, const ValueTypes& type, bool pkey = false);
		~DataColumn();

		ValueTypes type() const;
		int isInteger() const;
		int isDateTime() const;
		int isString() const;
		const string& name() const;
		bool primaryKey() const;

	private:
		ValueTypes convertType(const string& type);

	protected:
		string _name;
		ValueTypes _type;
		bool _primaryKey;
	};

	class DataColumns : public Vector<DataColumn>
	{
	public:
		DataColumns(bool autoDelete = true, uint capacity = Vector<DataColumn>::DefaultCapacity);

		DataColumn* operator[](const string& i) const;
		DataColumn* at(uint i) const;
		DataColumn* at(const string& columnName) const;
	};

	class DataCell
	{
	public:
		DataCell(const DataColumn* column);
		DataCell(const DataColumn* column, const DbValue& value);
		DataCell(const DataColumn* column, int value);
		DataCell(const DataColumn* column, uint value);
		DataCell(const DataColumn* column, int64_t value);
		DataCell(const DataColumn* column, uint64_t value);
		DataCell(const DataColumn* column, double value);
		DataCell(const DataColumn* column, const char* value);
		DataCell(const DataColumn* column, const string& value);
		DataCell(const DataColumn* column, const DateTime& value);
		DataCell(const DataColumn* column, const TimeSpan& value);
		~DataCell();

		ValueTypes type() const;
		const Value& value() const;
		const string valueStr(bool hasQuote = false) const;
		bool matchColumnName(const char* columnName) const;
		const string columnName() const;

		static void setStringValue(Value& value, const char* str);

		bool isNullValue() const;
		void setNullValue();

	private:
		DataCell(const DataColumn* column, const Value value);

		void setStringValue(const char* str);

	private:
		friend class SqliteClient;
		friend class OracleClient;

		Value _value;
		const DataColumn* _column;
	};
	class DataCells : public Vector<DataCell>
	{
	public:
		DataCells(bool autoDelete = true, uint capacity = Vector<DataCell>::DefaultCapacity);

		DataCell* operator[](const char* i) const;
		DataCell* at(uint i) const;
		DataCell* at(const string& columnName) const;

		const Value cellValue(const string& columnName) const;
		bool hasColumn(const string& columnName) const;

		void add(const DataCell* cell);

	private:
		Dictionary<string, uint> _positions;
	};

	class DataRow
	{
	public:
		DataRow();
		~DataRow();

		void addCell(const DataCell* cell);
		const DataCells* cells() const;

	private:
		DataCells _cells;
	};

	typedef Vector<DataRow> DataRows;

	class DataTable
	{
	public:
		DataTable();
		DataTable(const string& name);
		DataTable(const char* name);
		~DataTable();

		const string& name() const;
		void setName(const string& name);
		void addRow(const DataRow* row);
		const DataRows* rows() const;
		uint rowCount() const;
		void addColumn(const DataColumn* column);
		const DataColumns* columns() const;
		uint columnCount() const;
		void clear();

		const string anyColumnNameStr(bool hasTableName = false) const;
		const string columnNameStr(bool hasTableName = false, const StringArray* excludedNames = NULL, const char* splitStr = ",") const;

	private:
		string _name;
		DataRows _rows;
		DataColumns _columns;
	};
}
#endif // DATATABLE_H
