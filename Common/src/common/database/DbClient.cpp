#include "DbClient.h"
#include "../diag/Debug.h"
#include "../diag/Stopwatch.h"
#include "../thread/Locker.h"
#include "../data/Vector.h"

namespace Database
{
	DbClient::DbClient()
	{

	}
	DbClient::~DbClient()
	{
	}

	void DbClient::printErrorInfo(const string& methodName, const string& sql, const string& error)
	{
		const int maxCount = 512;
		const char* sqlStr;
		string temp;
		if (sql.length() > maxCount)
		{
			temp = sql.substr(0, maxCount);
			sqlStr = temp.c_str();
		}
		else
		{
			sqlStr = sql.c_str();
		}

		string errorStr = error.empty() ? getErrorMsg() : error;

		if (sql.length() > 0)
		{
			Trace::writeFormatLine("Method'%s' error, msg: %s, sql: %s", methodName.c_str(), errorStr.c_str(), sqlStr);
		}
		else
		{
			Trace::writeFormatLine("Method'%s' error, msg: %s", methodName.c_str(), errorStr.c_str());
		}
	}

	Value DbClient::convertValue(const ValueTypes type, const char* str)
	{
		Value value = DbValue::nullValue();
		if (str == NULL)
			return value;

		switch (type)
		{
		case Null:
			break;
		case Integer32:
			Convert::parseInt32(str, value.nValue);
			break;
		case Integer64:
			Convert::parseInt64(str, value.lValue);
			break;
		case Float32:
		case Float64:
			Convert::parseDouble(str, value.dValue);
			break;
		case Text:
			if (strlen(str) > 0)
			{
				DataCell::setStringValue(value, str);
			}
			break;
		case Date:
		case Timestamp:
		{
			DateTime time;
			if (DateTime::parse(str, time))
			{
				value.tValue = time.ticks();
			}
			break;
		}
		default:
			assert(false);
			break;
		}
		return value;
	}
}
