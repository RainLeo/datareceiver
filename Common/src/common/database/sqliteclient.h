#ifndef SQLITECLIENT_H
#define SQLITECLIENT_H

#include <stdio.h>
#include <mutex>
#include "../../sqlite/sqlite3.h"
#include "../system/Convert.h"
#include "../data/Vector.h"
#include "../database/DbClient.h"

using namespace std;

namespace Database
{
	class COMMON_EXPORT SqliteClient : public DbClient
	{
	public:
		SqliteClient(void);
		~SqliteClient(void);

		bool open(const string& connectionStr);
		bool close();

		bool executeSql(const string& sql, bool transaction = true);
		bool executeSqlQuery(const string& sql, DataTable& table);
		bool executeSqlInsert(const DataTable& table, bool replace = false);

		string getErrorMsg();

		bool beginTransaction();
		bool commitTransaction();
		bool rollbackTransaction();

	protected:
		ValueTypes getColumnType(int type);
		ValueTypes getColumnType(const string& type);

	private:
		int executeSqlInner(const string& sql);
		int executeSqlInsertInner(const DataTable& table, bool replace = false);
		int executeSqlQueryInner(const string& sql, DataTable& table);

		int beginTransactionInner();
		int commitTransactionInner();
		int rollbackTransactionInner();

		inline bool isSuccessed(int result) const
		{
			return result == SQLITE_OK;
		}

	private:
		sqlite3* _sqliteDb;
	};
}
#endif // SQLITECLIENT_H