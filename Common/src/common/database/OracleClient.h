#ifndef ORACLECLIENT_H
#define ORACLECLIENT_H

#include <stdio.h>
#include "../system/Convert.h"
#include "../data/Vector.h"
#include "../database/DbClient.h"
#include "../../oci/oci.h"

namespace Database
{
	class COMMON_EXPORT OracleClient : public DbClient
	{
	public:
		OracleClient();
		~OracleClient();

		bool open(const string& database, const string& username, const string& password);

		bool open(const string& connectionStr);
		bool close();

		bool executeSql(const string& sql, bool transaction = true);
		bool executeSqlQuery(const string& sql, DataTable& table);
		bool executeSqlInsert(const DataTable& table, bool replace = false);

		string getErrorMsg();

		bool beginTransaction();
		bool commitTransaction();
		bool rollbackTransaction();

	protected:
		ValueTypes getColumnType(int type);

	private:
		sword executeSqlInner(const string& sql);
		sword executeSqlQueryInner(const string& sql, DataTable& table);
		sword executeSqlInsertInner(const DataTable& table);
		sword executeSqlMergeInner(const DataTable& table);

		string toInsertStr(const DataTable& table);
		string toInsertStr(const DataTable& table, const DataRow* row);
		string toMergeStr(const DataTable& table, const DataRow* row);

		inline bool isSuccessed(sword result) const
		{
			return result == OCI_SUCCESS;
		}
		inline void printErrorInfo(const string& methodName, sword error, const string& sql = "")
		{
			DbClient::printErrorInfo(methodName, sql, getErrorMsg(error));
		}
		string getErrorMsg(sword error);

	private:
		OCIEnv* _env;
		OCIError* _error;
		OCISvcCtx* _context;

		sword _lastError;
	};
}

#endif // ORACLECLIENT_H
