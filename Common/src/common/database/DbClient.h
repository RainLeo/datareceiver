#ifndef DBCLIENT_H
#define DBCLIENT_H

#include <stdio.h>
#include <mutex>
#include "DataTable.h"

namespace Database
{
	class COMMON_EXPORT DbClient
	{
	public:
		DbClient();
		virtual ~DbClient();

		virtual bool open(const string& connectionStr) = 0;
		virtual bool close() = 0;

		virtual bool executeSql(const string& sql, bool transaction = true) = 0;
		virtual bool executeSqlQuery(const string& sql, DataTable& table) = 0;
		virtual bool executeSqlInsert(const DataTable& table, bool replace = false) = 0;

		virtual string getErrorMsg() = 0;

		virtual bool beginTransaction() = 0;
		virtual bool commitTransaction() = 0;
		virtual bool rollbackTransaction() = 0;

	protected:
		virtual ValueTypes getColumnType(int type) = 0;

		Value convertValue(const ValueTypes type, const char* str);

	protected:
		void printErrorInfo(const string& methodName, const string& sql = "", const string& error = "");

	protected:
		mutex _dbMutex;
	};
}

#endif // DBCLIENT_H