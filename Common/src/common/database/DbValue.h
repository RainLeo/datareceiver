#ifndef DBVALUE_H
#define DBVALUE_H

#include <assert.h>
#include <string>
#include "../data/Vector.h"
#include "../data/Dictionary.h"
#include "../system/DateTime.h"
#include "../system/Convert.h"
#include "../IO/Stream.h"

using namespace std;
using namespace Common;

namespace Database
{
	union Value
	{
		char* strValue;
		int nValue;
		int64_t lValue;
		uint64_t ulValue;
		double dValue;
		float fValue;
		bool bValue;
		void* blobValue;
		uint64_t tValue;	// 100-nanosecond ticks
	};

	enum ValueTypes : byte
	{
		Null = 0,
        Digital,
		Integer32,
		Integer64,
		Float32,
        Float64,
		Text,
		Date,
		Timestamp,
        Blob,
	};

	struct DbValue
	{
	public:
		DbValue(ValueTypes type);
		DbValue(ValueTypes type, const Value& value);
		~DbValue();

		ValueTypes type() const;
		const Value& value() const;

		static bool isNullValue(const Value& value);
		bool isNullValue() const;
		static Value nullValue();
		void setNullValue();

		void copyFrom(const DbValue* value);

		bool operator==(const Value& value);
		bool operator!=(const Value& value);
		bool operator==(const DbValue& value);
		bool operator!=(const DbValue& value);

		void operator=(const Value& value);
		void operator=(const DbValue& value);
		void operator=(const string& value);
		void operator=(const int& value);
		void operator=(const DateTime& value);

		void write(Stream* stream, bool bigEndian = true) const;
		void read(Stream* stream, bool bigEndian = true);

		void writeStr(Stream* stream, int lengthCount = 2) const;
		void readStr(Stream* stream, int lengthCount = 2);
		void writeFixedLengthStr(Stream* stream, int length) const;
		void readFixedLengthStr(Stream* stream, int length);

		void writeByte(Stream* stream) const;
		void readByte(Stream* stream);

		void writeBCDDateTime(Stream* stream, bool includedSec = true, bool includedMs = false) const;
		void readBCDDateTime(Stream* stream, bool includedSec = true, bool includedMs = false);

	private:
		void setStringValue(const string& str);
		void setStringValue(const char* str);

	private:
		enum ValueFlag : byte
		{
			NullFlag = 0xF7,
			NormalFlag = 0x02
		};

		ValueTypes _type;
		Value _value;
	};
}
#endif // DBVALUE_H
