#include "OracleClient.h"
#include "../diag/Debug.h"
#include "../diag/Stopwatch.h"
#include "../thread/Locker.h"
#include "../data/Vector.h"

namespace Database
{
	class InnerColumn
	{
	public:
		Value datavalue;    //output variable
		ub4 datasize;
		ub2 datatype;

		InnerColumn()
		{
			datavalue.strValue = NULL;
			datasize = 0;
			datatype = 0;
		}
		InnerColumn(ub2 type, ub4 size)
		{
			datatype = type;
			datasize = size;
			datavalue.strValue = new char[datasize];
			memset(datavalue.strValue, 0, datasize);
		}
		~InnerColumn()
		{
			if (datavalue.strValue != NULL)
			{
				delete[] datavalue.strValue;
				datavalue.strValue = NULL;
			}
		}
	};
	typedef Vector<InnerColumn> InnerColumns;

	OracleClient::OracleClient()
	{
		_context = NULL;
		_lastError = OCI_SUCCESS;

		sword result;
		result = OCIEnvCreate(&_env, OCI_THREADED, NULL, NULL, NULL, NULL, 0, NULL);
		if (result)
		{
			_lastError = result;
			printErrorInfo("OCIEnvCreate", result);
		}
		else
		{
			result = OCIHandleAlloc(_env, (void**)&_error, OCI_HTYPE_ERROR, 0, NULL);
			if (result)
			{
				_lastError = result;
				printErrorInfo("OCIHandleAlloc", result);
			}
		}
	}

	OracleClient::~OracleClient()
	{
		close();
	}

	bool OracleClient::open(const string& database, const string& username, const string& password)
	{
		Locker locker(&_dbMutex);

		sword result = OCILogon(_env, _error, &_context, (const OraText*)username.c_str(), (ub4)username.length(),
			(const OraText*)password.c_str(), (ub4)password.length(), (const OraText*)database.c_str(), (ub4)database.length());
		if (result)
		{
			_lastError = result;
			printErrorInfo("OCILogon", result);
		}
		else
		{
			result = executeSqlInner("ALTER SESSION SET NLS_DATE_FORMAT = 'yyyy-mm-dd hh24:mi:ss'");
			if (result)
			{
				_lastError = result;
				printErrorInfo("ALTER SESSION SET NLS_DATE_FORMAT", result);
			}
			result = executeSqlInner("ALTER SESSION SET NLS_TIMESTAMP_FORMAT = 'yyyy-mm-dd hh24:mi:ss.ff3'");
			if (result)
			{
				_lastError = result;
				printErrorInfo("ALTER SESSION SET NLS_TIMESTAMP_FORMAT", result);
			}
		}
		return isSuccessed(result);
	}

	bool OracleClient::open(const string& connectionStr)
	{
		StringArray texts;
		Convert::splitStr(connectionStr, ';', texts);
		if (texts.count() == 3)
		{
			return open(texts[0], texts[1], texts[2]);
		}
		return false;
	}
	bool OracleClient::close()
	{
		Locker locker(&_dbMutex);

		sword result = 0;
		if (_context != NULL)
		{
			result = OCILogoff(_context, _error);
			OCIHandleFree(_context, OCI_HTYPE_SVCCTX);
			_context = NULL;
		}
		OCIHandleFree(_error, OCI_HTYPE_ERROR);
		OCIHandleFree(_env, OCI_HTYPE_ENV);
		_lastError = result;
		return isSuccessed(result);
	}

	bool OracleClient::executeSql(const string& sql, bool transaction)
	{
		Locker locker(&_dbMutex);

		sword result = executeSqlInner(sql);
		_lastError = result;
		return isSuccessed(result);
	}
	bool OracleClient::executeSqlQuery(const string& sql, DataTable& table)
	{
		Locker locker(&_dbMutex);

		sword result = executeSqlQueryInner(sql, table);
		_lastError = result;
		return isSuccessed(result);
	}
	bool OracleClient::executeSqlInsert(const DataTable& table, bool replace)
	{
		Locker locker(&_dbMutex);

		sword result = replace ? executeSqlMergeInner(table) : executeSqlInsertInner(table);
		_lastError = result;
		return isSuccessed(result);
	}

	bool OracleClient::beginTransaction()
	{
		return false;
	}
	bool OracleClient::commitTransaction()
	{
		return false;
	}
	bool OracleClient::rollbackTransaction()
	{
		return false;
	}

	ValueTypes OracleClient::getColumnType(int type)
	{
		switch (type)
		{
		case SQLT_CHR:
		case SQLT_AFC:
		case SQLT_STR:
			return ValueTypes::Text;
		case SQLT_DAT:
			return ValueTypes::Date;
		case SQLT_INT:
			return ValueTypes::Integer32;
		case SQLT_LNG:
			return ValueTypes::Integer64;
		case SQLT_NUM:
			return ValueTypes::Float64;
		case SQLT_FLT:
			return ValueTypes::Float32;
		case SQLT_TIMESTAMP:
		case SQLT_TIMESTAMP_TZ:
		case SQLT_TIMESTAMP_LTZ:
			return ValueTypes::Timestamp;
		case SQLT_BOL:
			return ValueTypes::Digital;
		default:
			assert(false);
			return ValueTypes::Null;
		}
	}

	string OracleClient::getErrorMsg()
	{
		return getErrorMsg(_lastError);
	}

	string OracleClient::getErrorMsg(sword error)
	{
		sword result = error;
		sword origin_error = result;
		char error_buffer[1024];
		result = OCIErrorGet(_error, 1, NULL, &result, (OraText*)error_buffer, sizeof(error_buffer), OCI_HTYPE_ERROR);
		if (!result)
		{
			return error_buffer;
		}
		else
		{
			return Convert::convertStr("cannot get error text, original error is %d", origin_error);
		}
	}
	sword OracleClient::executeSqlInner(const string& sql)
	{
#if DEBUG
		Stopwatch sw("OracleClient::executeSqlInner", 100);
#endif
		OCIStmt* statement;
		sword result = OCIHandleAlloc(_env, (void**)&statement, OCI_HTYPE_STMT, 0, NULL);
		if (result)
		{
			printErrorInfo("OCIHandleAlloc", result, sql);
			return result;
		}
		result = OCIStmtPrepare(statement, _error, (const OraText*)sql.c_str(), (ub4)sql.length(), OCI_NTV_SYNTAX, OCI_DEFAULT);
		if (result)
		{
			OCIHandleFree(statement, OCI_HTYPE_STMT);
			printErrorInfo("OCIStmtPrepare", result, sql);
			return result;
		}
		result = OCIStmtExecute(_context, statement, _error, 1, 0, NULL, NULL, OCI_DEFAULT);
		if (result)
		{
			OCIHandleFree(statement, OCI_HTYPE_STMT);
			printErrorInfo("OCIStmtExecute", result, sql);
			return result;
		}
		result = OCITransCommit(_context, _error, 0);
		if (result)
		{
			OCIHandleFree(statement, OCI_HTYPE_STMT);
			printErrorInfo("OCITransCommit", result, sql);
			return result;
		}
		OCIHandleFree(statement, OCI_HTYPE_STMT);
		return OCI_SUCCESS;
	}
	sword OracleClient::executeSqlQueryInner(const string& sql, DataTable& table)
	{
#if DEBUG
		Stopwatch sw("OracleClient::executeSqlQueryInner", 100);

		if (table.name().empty())
			table.setName("temp");
		sw.setInfo(Convert::convertStr("executeSqlQueryInner, the table name is '%s'", table.name().c_str()));
#endif

		sword result;
		OCIStmt* statement;
		result = OCIHandleAlloc(_env, (void**)&statement, OCI_HTYPE_STMT, 0, NULL);
		if (result)
		{
			printErrorInfo("OCIHandleAlloc", result, sql);
			return result;
		}

		result = OCIStmtPrepare(statement, _error, (const OraText*)sql.c_str(), (ub4)sql.length(), OCI_NTV_SYNTAX, OCI_DEFAULT);
		if (result)
		{
			OCIHandleFree(statement, OCI_HTYPE_STMT);
			printErrorInfo("OCIStmtPrepare", result, sql);
			return result;
		}

		result = OCIStmtExecute(_context, statement, _error, 0, 0, NULL, NULL, OCI_DEFAULT);
		if (result)
		{
			OCIHandleFree(statement, OCI_HTYPE_STMT);
			printErrorInfo("OCIStmtExecute", result, sql);
			return result;
		}

		int numcols = 0;
		result = OCIAttrGet(statement, OCI_HTYPE_STMT, &numcols, 0, OCI_ATTR_PARAM_COUNT, _error);
		if (result)
		{
			OCIHandleFree(statement, OCI_HTYPE_STMT);
			printErrorInfo("OCIAttrGet", result, sql);
			return result;
		}

		InnerColumns icolumns;
		int table_columnCount = table.columnCount();
		if ((table_columnCount > 0 && table_columnCount == numcols) ||
			table_columnCount == 0)
		{
			OCIParam *paramhp;
			const ub4 DEFAULT_LONG_SIZE = 32768;
			ub2 datatype = 0;
			ub4 datasize = 0;
			ub4 dispsize = 0;
			text* col_name;
			ub4 col_name_len;
			ub4 charSemantics;
			char name[512];
			for (int i = 0; i < numcols; i++)
			{
				/* get parameter for column col*/
				result = OCIParamGet(statement, OCI_HTYPE_STMT, _error, (void **)&paramhp, i + 1);
				/* Retrieve the column name attribute */
				result = OCIAttrGet((dvoid*)paramhp, (ub4)OCI_DTYPE_PARAM,
					(dvoid**)&col_name, (ub4 *)&col_name_len, (ub4)OCI_ATTR_NAME, (OCIError *)_error);
				result = OCIAttrGet((dvoid*)paramhp, (ub4)OCI_DTYPE_PARAM,
					(dvoid**)&datatype, 0, (ub4)OCI_ATTR_DATA_TYPE, (OCIError *)_error);

				/* Retrieve the length semantics for the column */
				charSemantics = 0;
				OCIAttrGet((dvoid*)paramhp, OCI_DTYPE_PARAM,
					(dvoid*)&charSemantics, 0, OCI_ATTR_CHAR_USED, (OCIError *)_error);
				if (charSemantics)
				{
					/* Retrieve the column width in characters */
					OCIAttrGet((dvoid*)paramhp, OCI_DTYPE_PARAM,
						(dvoid*)&datasize, 0, OCI_ATTR_CHAR_SIZE, (OCIError *)_error);
				}
				else
				{
					result = OCIAttrGet((dvoid*)paramhp, (ub4)OCI_DTYPE_PARAM,
						(dvoid**)&datasize, 0, (ub4)OCI_ATTR_DATA_SIZE, (OCIError *)_error);
				}
				if (datasize > DEFAULT_LONG_SIZE || datasize == 0)
					datasize = DEFAULT_LONG_SIZE;
				/* add one more byte to store the ternimal char of string */
				datasize = datasize + 1;
				result = OCIAttrGet((dvoid*)paramhp, (ub4)OCI_DTYPE_PARAM,
					(dvoid**)&dispsize, 0, (ub4)OCI_ATTR_DISP_SIZE, (OCIError *)_error);
				if (dispsize > DEFAULT_LONG_SIZE || dispsize == 0)
					dispsize = DEFAULT_LONG_SIZE;
				/* add one more byte to store the ternimal char of string */
				dispsize = dispsize + 1;

				bool useDispSize = datatype == SQLT_DAT ||
					datatype == SQLT_TIMESTAMP ||
					datatype == SQLT_TIMESTAMP_TZ ||
					datatype == SQLT_TIMESTAMP_LTZ;
				InnerColumn* icolumn = new InnerColumn(datatype, useDispSize ? dispsize : datasize);
				icolumns.add(icolumn);

				strncpy(name, (const char*)col_name, col_name_len);
				name[col_name_len] = '\0';
				if (table_columnCount == 0)
				{
					DataColumn* column = new DataColumn(name, getColumnType(datatype));
					table.addColumn(column);
				}
			}

			OCIDefine* define;
			for (int i = 0; i < numcols; i++)
			{
				const InnerColumn* icolumn = icolumns[i];
				if (!(icolumn->datatype == SQLT_BLOB || icolumn->datatype == SQLT_CLOB))
				{
					result = OCIDefineByPos(statement, &define, _error, i + 1,
						(dvoid *)icolumn->datavalue.strValue, icolumn->datasize, SQLT_STR, NULL, NULL, NULL, OCI_DEFAULT);
					if (result)
					{
						OCIHandleFree(statement, OCI_HTYPE_STMT);
						printErrorInfo("OCIDefineByPos", result, sql);
						return result;
					}
				}
			}
		}

		while (true)
		{
			result = OCIStmtFetch2(statement, _error, 1, OCI_DEFAULT, 0, OCI_DEFAULT);
			//result = OCIStmtFetch(statement, _error, 1, 0, OCI_DEFAULT);
			if (result == OCI_NO_DATA)
			{
				break;
			}
			if (result)
			{
				//printErrorInfo("OCIStmtFetch2", result, sql);
			}

			DataRow* row = new DataRow();
			for (uint i = 0; i < table.columnCount(); i++)
			{
				DataColumn* column = table.columns()->at(i);
				const InnerColumn* icolumn = icolumns[i];
				ValueTypes type = column->type();
				Value value = convertValue(type, icolumn->datavalue.strValue);
				DataCell* cell = new DataCell(column, value);
				row->addCell(cell);
			}
			table.addRow(row);
		}

		OCIHandleFree(statement, OCI_HTYPE_STMT);

		return OCI_SUCCESS;
	}
	string OracleClient::toInsertStr(const DataTable& table)
	{
		const char* insertStr = "INSERT INTO %s(%s) VALUES (%s)";
		uint columnCount = table.columnCount();

		StringBuilder columsStr;
		StringBuilder valuesStr;
		for (uint j = 0; j < columnCount; j++)
		{
			if (!columsStr.isEmpty())
			{
				columsStr.append(", ");
			}
			columsStr.append(table.columns()->at(j)->name());

			if (!valuesStr.isEmpty())
			{
				valuesStr.append(", ");
			}
			//valuesStr.append(Convert::convertStr(":%s", table.columns()->at(j)->name().c_str()));
			valuesStr.append(Convert::convertStr(":%d", j + 1));
		}
		string sql = Convert::convertStr(insertStr, table.name().c_str(),
			columsStr.toString(), valuesStr.toString());
		return sql;
	}
	string OracleClient::toInsertStr(const DataTable& table, const DataRow* row)
	{
		const char* insertStr = "INSERT INTO %s(%s) VALUES (%s)";
		uint columnCount = table.columnCount();

		StringBuilder columsStr;
		StringBuilder valuesStr;
		for (uint j = 0; j < columnCount; j++)
		{
			const DataCell* cell = row->cells()->at(j);
			if (cell != NULL)
			{
				if (!columsStr.isEmpty())
				{
					columsStr.append(", ");
				}
				columsStr.append(table.columns()->at(j)->name());

				if (!valuesStr.isEmpty())
				{
					valuesStr.append(", ");
				}
				valuesStr.append(cell->valueStr(true));
			}
		}
		string sql = Convert::convertStr(insertStr, table.name().c_str(),
			columsStr.toString(), valuesStr.toString());
		return sql;
	}
	string OracleClient::toMergeStr(const DataTable& table, const DataRow* row)
	{
		uint columnCount = table.columnCount();
		string primaryKey = "";
		for (uint i = 0; i < columnCount; i++)
		{
			if (table.columns()->at(i)->primaryKey())
			{
				primaryKey = table.columns()->at(i)->name();
				break;
			}
		}
		if (primaryKey.empty())
			return "";

		// MERGE INTO T T1
		//	USING(SELECT '1001' AS a, 2 AS b FROM dual) T2
		//	ON(T1.a = T2.a)
		//	WHEN MATCHED THEN
		//	UPDATE SET T1.b = T2.b
		//	WHEN NOT MATCHED THEN
		//	INSERT(a, b) VALUES(T2.a, T2.b);
		const char* mergeStr = "MERGE INTO %s T1\
								USING(SELECT %s FROM dual) T2\
								ON(T1.%s = T2.%s)\
								WHEN MATCHED THEN\
								UPDATE SET %s\
								WHEN NOT MATCHED THEN\
								INSERT(%s) VALUES(%s)";

		StringBuilder svaluesStr;
		StringBuilder uvaluesStr;
		StringBuilder icolumsStr;
		StringBuilder ivaluesStr;
		for (uint j = 0; j < columnCount; j++)
		{
			const DataCell* cell = row->cells()->at(j);
			if (cell != NULL)
			{
				string name = table.columns()->at(j)->name();
				string value = cell->valueStr(true);

				if (!svaluesStr.isEmpty())
				{
					svaluesStr.append(", ");
				}
				string svalue = Convert::convertStr("%s AS %s", value.c_str(), name.c_str());
				svaluesStr.append(svalue);

				if (name != primaryKey)
				{
					if (!uvaluesStr.isEmpty())
					{
						uvaluesStr.append(", ");
					}
					string uvalue = Convert::convertStr("T1.%s = T2.%s", name.c_str(), name.c_str());
					uvaluesStr.append(uvalue);
				}

				if (!icolumsStr.isEmpty())
				{
					icolumsStr.append(", ");
				}
				icolumsStr.append(name.c_str());

				if (!ivaluesStr.isEmpty())
				{
					ivaluesStr.append(", ");
				}
				string ivalue = Convert::convertStr("T2.%s", name.c_str());
				ivaluesStr.append(ivalue);
			}
		}
		string sql = Convert::convertStr(mergeStr, table.name().c_str(), svaluesStr.toString(),
			primaryKey.c_str(), primaryKey.c_str(),
			uvaluesStr.toString(), icolumsStr.toString(), ivaluesStr.toString());
		return sql;
	}
	sword OracleClient::executeSqlInsertInner(const DataTable& table)
	{
#if DEBUG
		Stopwatch sw("OracleClient::executeSqlInsertInner", 100);
#endif
		if (table.name().empty())
			return -1;
		if (table.columnCount() == 0)
			return -2;
		if (table.rowCount() == 0)
			return -3;

		OCIStmt* statement;
		sword result = OCIHandleAlloc(_env, (void**)&statement, OCI_HTYPE_STMT, 0, NULL);
		if (result)
		{
			printErrorInfo("OCIHandleAlloc", result);
			return result;
		}

		uint columnCount = table.columnCount();
		uint rowCount = table.rowCount();
		string sql = toInsertStr(table);
		const char* sqlStr = sql.c_str();
		result = OCIStmtPrepare(statement, _error, (const OraText*)sqlStr, (ub4)sql.length(), OCI_NTV_SYNTAX, OCI_DEFAULT);
		if (result)
		{
			OCIHandleFree(statement, OCI_HTYPE_STMT);
			printErrorInfo("OCIStmtPrepare", result, sqlStr);
			return result;
		}

		const int StrLength = 65535;
		void** buffer = new void*[columnCount];
		void** alenps = new void*[columnCount];
		OCIBind** binds = new OCIBind*[columnCount];
		for (uint j = 0; j < columnCount; j++)
		{
			char(*values)[StrLength] = new char[rowCount][StrLength];
			memset(values, 0, rowCount * StrLength);
			buffer[j] = values;

			ub2 *alenp = new ub2[rowCount];
			alenps[j] = alenp;
			for (uint i = 0; i < rowCount; i++)
			{
				const DataRow* row = table.rows()->at(i);
				const DataCell* cell = row->cells()->at(j);
				string value;
				if (cell != NULL)
				{
					value = !cell->isNullValue() ? cell->valueStr() : "";
				}
				else
				{
					value = "";
				}
				strcpy(values[i], value.c_str());
				alenp[i] = value.length();
			}

			result = OCIBindByPos(statement, &binds[j], _error, j + 1, (dvoid *)values[0], StrLength, SQLT_CHR, 0, alenp, 0, 0, 0, OCI_DEFAULT);
			if (result)
			{
				printErrorInfo("OCIBindByPos", result, sqlStr);
			}
		}

		result = OCIStmtExecute(_context, statement, _error, rowCount, 0, NULL, NULL, OCI_BATCH_ERRORS);
		if (!(result == OCI_SUCCESS || result == OCI_SUCCESS_WITH_INFO))
		{
			printErrorInfo("OCIStmtExecute", result, sqlStr);
			goto destroy;
		}
		result = OCITransCommit(_context, _error, 0);
		if (result)
		{
			printErrorInfo("OCITransCommit", result, sqlStr);
			goto destroy;
		}

	destroy:
		delete[] binds;
		for (size_t j = 0; j < columnCount; j++)
		{
			delete[] alenps[j];
			delete[] buffer[j];
		}
		delete[] alenps;
		delete[] buffer;
		OCIHandleFree(statement, OCI_HTYPE_STMT);
		return result;
	}
	sword OracleClient::executeSqlMergeInner(const DataTable& table)
	{
#if DEBUG
		Stopwatch sw("OracleClient::executeSqlMergeInner", 100);
#endif
		if (table.name().empty())
			return -1;
		if (table.columnCount() == 0)
			return -2;
		if (table.rowCount() == 0)
			return -3;
		bool primaryKey = false;
		for (uint i = 0; i < table.columnCount(); i++)
		{
			if (table.columns()->at(i)->primaryKey())
			{
				primaryKey = true;
				break;
			}
		}
		if (!primaryKey)
			return -4;

		OCIStmt* statement;
		sword result = OCIHandleAlloc(_env, (void**)&statement, OCI_HTYPE_STMT, 0, NULL);
		if (result)
		{
			printErrorInfo("OCIHandleAlloc", result);
			return result;
		}

		const int patchCount = 500;
		uint rowCount = table.rowCount();
		string sql;
		for (uint i = 0; i < rowCount; i++)
		{
			const DataRow* row = table.rows()->at(i);
			sql = toMergeStr(table, row);

			const char* sqlStr = sql.c_str();
			result = OCIStmtPrepare(statement, _error, (const OraText*)sqlStr, (ub4)sql.length(), OCI_NTV_SYNTAX, OCI_DEFAULT);
			if (result)
			{
				OCIHandleFree(statement, OCI_HTYPE_STMT);
				printErrorInfo("OCIStmtPrepare", result, sqlStr);
				return result;
			}
			result = OCIStmtExecute(_context, statement, _error, 1, 0, NULL, NULL, OCI_DEFAULT);
			if (result)
			{
				OCIHandleFree(statement, OCI_HTYPE_STMT);
				printErrorInfo("OCIStmtExecute", result, sqlStr);
				return result;
			}
			if ((i == rowCount - 1) ||
				(i > 0 && i % patchCount == 0))
			{
				result = OCITransCommit(_context, _error, OCI_DEFAULT);
				if (result)
				{
					OCIHandleFree(statement, OCI_HTYPE_STMT);
					printErrorInfo("OCITransCommit", result, sqlStr);
					return result;
				}
			}
		}

		OCIHandleFree(statement, OCI_HTYPE_STMT);
		return OCI_SUCCESS;
	}
}
