#ifndef COMMON_GLOBAL_H
#define COMMON_GLOBAL_H

#include <stdio.h>
#include <string>
#include <stdint.h>

using namespace std;

#if COMMON_LIB
#ifdef WIN32
# define COMMON_EXPORT __declspec(dllexport)
# define COMMON_TEMPLATE
#else		// WIN32
# define COMMON_EXPORT __attribute__((visibility("default")))
#endif		// WIN32
#elif COMMON_STATIC
# define COMMON_EXPORT
# define COMMON_TEMPLATE
#else
#ifdef WIN32
# define COMMON_EXPORT __declspec(dllimport)
# define COMMON_TEMPLATE extern
#else		// WIN32
# define COMMON_EXPORT
# define COMMON_TEMPLATE
#endif		// WIN32
#endif

#ifndef byte
typedef unsigned char byte;
#endif

#ifndef uint
typedef unsigned int uint;
#endif

#ifndef ushort
typedef unsigned short ushort;
#endif

#ifndef UINT64_MAX 
#define UINT64_MAX        18446744073709551615ULL
#endif
#ifndef UINT32_MAX
#define UINT32_MAX        4294967295U
#endif

#if WIN32
#pragma warning(disable : 4996)
#pragma warning(disable : 4251)

#ifndef NOMINMAX
#define NOMINMAX
#endif
#endif

#if !defined(COMMON_ATTRIBUTE)
#  if defined(__clang__) || defined(__GNUC__)
#    define COMMON_ATTRIBUTE(attr) __attribute__((attr))
#  else
#    define COMMON_ATTRIBUTE(attr)
#  endif
#endif

#if !defined(COMMON_UNUSED)
#  define COMMON_UNUSED COMMON_ATTRIBUTE(unused)
#endif

#if !defined(COMMON_ATTR_DEPRECATED)
// FIXME: we ignore msg for now...
#  define COMMON_ATTR_DEPRECATED(msg) COMMON_ATTRIBUTE(deprecated)
#endif

#ifdef __APPLE__
# include <TargetConditionals.h>
#endif
#if TARGET_OS_IPHONE || __ANDROID__
#define PHONE_OS
#endif

#endif // COMMON_GLOBAL_H
