#include "Dns.h"
#include "../diag/Debug.h"
#include "../exception/Exception.h"
#include <errno.h>
#if WIN32
#include<Winsock2.h>
#include <ws2tcpip.h>
#pragma comment (lib, "Ws2_32.lib")
#include <iphlpapi.h>
// Link with Iphlpapi.lib
#pragma comment(lib, "IPHLPAPI.lib")
#else
#include <unistd.h>  
#include <sys/types.h>  
#include <sys/socket.h>  
#include <sys/ioctl.h>  
#include <netinet/in.h>  
#include <arpa/inet.h>  
#include <netdb.h>
#include <net/if.h>
#endif
#include "TcpClient.h"

#if WIN32
#undef errno
#define errno WSAGetLastError()
#else
#endif

namespace Common
{
	string Dns::getHostName()
	{
#if WIN32
		TcpClient::initializeSocket();
#endif
		char hostName[1024];
		if (gethostname(hostName, sizeof(hostName)) == 0)
		{
			return hostName;
		}
		else
		{
			Debug::writeFormatLine("can't get host name: %s", strerror(errno));
		}
		return "";
	}

	bool Dns::getHostNames(StringArray& hostNames)
	{
#if WIN32
		TcpClient::initializeSocket();
#endif
		//struct addrinfo *result = NULL;
		//struct addrinfo *ptr = NULL;
		//struct addrinfo hints;
		//memset(&hints, 0, sizeof(hints));
		//hints.ai_family = AF_UNSPEC;
		//hints.ai_socktype = SOCK_STREAM;
		//hints.ai_protocol = IPPROTO_TCP;

		//int retval = getaddrinfo(getHostName().c_str(), NULL, &hints, &result);
		//if(retval == 0)
		//{
		//	// Retrieve each address and print out the hex bytes
		//	for(ptr=result; ptr != NULL ;ptr=ptr->ai_next)
		//	{
		//		if(ptr->ai_family == AF_INET)
		//		{
		//			struct sockaddr_in  *sockaddr_ipv4 = (struct sockaddr_in *) ptr->ai_addr;
		//			hostNames.add(new string(inet_ntoa(sockaddr_ipv4->sin_addr)));
		//		}
		//	}
		//	return true;
		//}
		//return false;

#if WIN32
		struct hostent *remoteHost = gethostbyname(getHostName().c_str());
		if (remoteHost != NULL && remoteHost->h_addrtype == AF_INET)
		{
			int i = 0;
			struct in_addr addr;
			while (remoteHost->h_addr_list[i] != 0)
			{
				addr.s_addr = *(u_long *)remoteHost->h_addr_list[i++];
				hostNames.add(inet_ntoa(addr));
			}
			return true;
		}
		else
		{
			Debug::writeFormatLine("can't get host names: %s", strerror(errno));
			return false;
		}
#else
		int s;
		struct ifconf ifconf;
		struct ifreq ifr[50];
		int ifs;
		int i;

		s = socket(AF_INET, SOCK_STREAM, 0);
		if (s < 0)
		{
			perror("socket");
			return false;
		}

		ifconf.ifc_buf = (char *) ifr;
		ifconf.ifc_len = sizeof ifr;

		if (ioctl(s, SIOCGIFCONF, &ifconf) == -1)
		{
			perror("ioctl");
			return false;
		}

		ifs = ifconf.ifc_len / sizeof(ifr[0]);
		//printf("interfaces = %d:\n", ifs);
		for (i = 0; i < ifs; i++)
		{
			char ip[INET_ADDRSTRLEN];
			struct sockaddr_in *s_in = (struct sockaddr_in *) &ifr[i].ifr_addr;

			if (!inet_ntop(AF_INET, &s_in->sin_addr, ip, sizeof(ip)))
			{
				perror("inet_ntop");
				return false;
			}
			hostNames.add(ip);
		}

		close(s);
		return true;
#endif
	}

	bool Dns::getMacAddresses(MacAddresses& addresses)
	{
#if WIN32

#define MAX_TRIES 3
#define MALLOC(x) HeapAlloc(GetProcessHeap(), 0, (x))
#define FREE(x) HeapFree(GetProcessHeap(), 0, (x))

		int i = 0;
		// default to unspecified address family (both)
		ULONG family = AF_INET;

		PIP_ADAPTER_ADDRESSES pAddresses = NULL;
		ULONG outBufLen = 0;
		ULONG Iterations = 0;

		// Set the flags to pass to GetAdaptersAddresses
		ULONG flags = GAA_FLAG_INCLUDE_PREFIX;
		PIP_ADAPTER_ADDRESSES pCurrAddresses = NULL;
		DWORD dwRetVal = 0;
		do
		{
			pAddresses = (IP_ADAPTER_ADDRESSES *)MALLOC(outBufLen);
			if (pAddresses == NULL)
			{
				printf
					("Memory allocation failed for IP_ADAPTER_ADDRESSES struct\n");
				exit(1);
			}

			dwRetVal = GetAdaptersAddresses(family, flags, NULL, pAddresses, &outBufLen);

			if (dwRetVal == ERROR_BUFFER_OVERFLOW)
			{
				FREE(pAddresses);
				pAddresses = NULL;
			}
			else {
				break;
			}

			Iterations++;

		} while ((dwRetVal == ERROR_BUFFER_OVERFLOW) && (Iterations < MAX_TRIES));

		if (dwRetVal == NO_ERROR)
		{
			// If successful, output some information from the data we received
			pCurrAddresses = pAddresses;
			while (pCurrAddresses)
			{
				if (pCurrAddresses->PhysicalAddressLength == (ULONG)MacAddress::Count)
				{
					MacAddress addr(pCurrAddresses->PhysicalAddress);
					addresses.add(addr);
				}

				pCurrAddresses = pCurrAddresses->Next;
			}
		}

		if (pAddresses)
		{
			FREE(pAddresses);
		}
		return addresses.count() > 0;
#else
		// todo: Dns::getMacAddresses
		throw NotImplementedException("Dns::getMacAddresses");
		return false;
#endif
		}
	}
