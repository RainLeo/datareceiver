/*
 * UdpClient.h
 *
 *  Created on: May 15, 2014
 *      Author: baowei
 */

#ifndef UDPCLIENT_H_
#define UDPCLIENT_H_

#include "common/common_global.h"

using namespace std;

namespace Common
{
	class COMMON_EXPORT UdpClient
	{
	public:
		UdpClient();
		virtual ~UdpClient();

		bool open(const char* host, const ushort port);
		bool open(const ushort port);
		void close();

		int write(const char* host, const ushort port, const byte *data, uint len);
		int write(const ushort port, const byte *data, uint len);

		int sendBufferSize() const;
		void setSendBufferSize(int bufferSize);

	private:
		int _socket;
	};

} /* namespace Driver */

#endif /* UDPCLIENT_H_ */
