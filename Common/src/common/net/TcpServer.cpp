#if WIN32
#include <winsock2.h>
#else
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <signal.h>
#include <netinet/tcp.h>
#endif
#include "TcpServer.h"
#include "TcpClient.h"
#include <errno.h>
#include "common/diag/Debug.h"
#include "common/system/Convert.h"
#include "common/thread/TickTimeout.h"
#include "common/system/Math.h"

#if WIN32
#undef errno
#define errno WSAGetLastError()
#define ioctl(s, cmd, argp) ioctlsocket(s, cmd, argp)
#define SHUT_RDWR SD_BOTH
#define socklen_t int
#else
#define closesocket(a) ::close(a)
#endif

using namespace Common;

namespace Common
{
    TcpServer::TcpServer()
    {
        _socket = -1;
        _isListening = false;
        TcpClient::initializeSocket();
    }
    
    TcpServer::~TcpServer()
    {
        close();
        
        _socket = -1;
    }
    
    bool TcpServer::bind(const string& address, int port)
    {
        struct hostent  *phe;		/* pointer to host information entry    */
        struct sockaddr_in sin;     /* an Internet endpoint address  */
        
        memset(&sin, 0, sizeof(sin));
        sin.sin_family = AF_INET;
        if ((sin.sin_port = htons(port)) == 0)
            Debug::writeFormatLine("invalid port \"%d\"", port);
        
        /* Map host name to IP address, allowing for dotted decimal */
        bool anyAddr = false;
        if(!address.empty())
        {
            const char* anyStr = "any";
#if WIN32
            anyAddr = stricmp(address.c_str(), anyStr) == 0;
#else
            anyAddr = strcasecmp(address.c_str(), anyStr) == 0;
#endif
        }
        else
        {
            anyAddr = true;
        }
        if(!anyAddr)
        {
            if ( (phe = gethostbyname(address.c_str())) != NULL )
                memcpy(&sin.sin_addr, phe->h_addr, phe->h_length);
            else if ( (sin.sin_addr.s_addr = inet_addr(address.c_str())) == INADDR_NONE )
                Debug::writeFormatLine("can't get \"%s\" host entry", address.c_str());
        }
        else
        {
            sin.sin_addr.s_addr = htonl(INADDR_ANY);
        }
        
        /* Allocate a socket */
        _socket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
        if (_socket < 0)
            Debug::writeFormatLine("can't create tcp server socket: %s", strerror(errno));
        
        setBlocking(true);
        
        return ::bind(_socket, (struct sockaddr *) & sin, sizeof (sin)) != -1;
    }
    
    bool TcpServer::listen(int maxConnections)
    {
        if(_socket != -1)
        {
            bool result = ::listen(_socket, maxConnections) != -1;
            _isListening = true;
            return result;
        }
        return false;
    }
    
    int TcpServer::accept()
    {
        if(_socket != -1)
        {
            struct sockaddr_in sin;
            socklen_t clen = sizeof(sin);
            memset(&sin, 0, clen);
            sin.sin_family = AF_INET;
            int result = ::accept(_socket, (struct sockaddr *)&sin, &clen);
            //if(result != -1)
            //{
            //	string addr = inet_ntoa(sin.sin_addr);
            //	_clientAddrs.add(addr);
            //}
            return result;
        }
        return false;
    }
    
    void TcpServer::close()
    {
        if (_socket != -1)
        {
            int result;
            if(_isListening)
            {
                result = shutdown(_socket, SHUT_RDWR);
                if (result == -1)
                {
                    Debug::writeFormatLine("shutdown failed with error = %s", strerror(errno) );
                }
            }
            result = closesocket(_socket);
            if (result == -1)
            {
                Debug::writeFormatLine("closesocket failed with error = %s", strerror(errno) );
            }
            else
            {
                _socket = -1;
                _isListening = false;
            }
        }
    }
    
    bool TcpServer::setBlocking(bool blocking)
    {
        if (_socket != -1)
        {
            //-------------------------
            // Set the socket I/O mode: In this case FIONBIO
            // enables or disables the blocking mode for the
            // socket based on the numerical value of mode.
            // If mode = 0, blocking is enabled;
            // If mode != 0, non-blocking mode is enabled.
            u_long mode = blocking ? 0 : 1;
            int result = ::ioctl(_socket, FIONBIO, &mode);
            if (result != 0)
            {
                Debug::writeFormatLine("ioctlsocket failed with error: %d\n", result);
            }
            else
            {
                return true;
            }
        }
        return false;
    }
    
    int TcpServer::sendBufferSize() const
    {
        socklen_t bufferSize = 0;
        if (_socket != -1)
        {
#if WIN32
            int len = sizeof(bufferSize);
#else
            socklen_t len = sizeof(bufferSize);
#endif
            int result = getsockopt(_socket, SOL_SOCKET, SO_SNDBUF, (char*)&bufferSize, &len);
            if (result == -1)
            {
                Debug::writeFormatLine("getsockopt'SO_SNDBUF' failed with error = %s", strerror(errno));
            }
        }
        return bufferSize;
    }
    void TcpServer::setSendBufferSize(int bufferSize)
    {
        if (_socket != -1)
        {
            int result = setsockopt(_socket, SOL_SOCKET, SO_SNDBUF, (const char*)&bufferSize, sizeof(int));
            if (result == -1)
            {
                Debug::writeFormatLine("setsockopt'SO_SNDBUF' failed with error = %s", strerror(errno));
            }
        }
    }
    int TcpServer::receiveBufferSize() const
    {
        socklen_t bufferSize = 0;
        if (_socket != -1)
        {
#if WIN32
            int len = sizeof(bufferSize);
#else
            socklen_t len = sizeof(bufferSize);
#endif
            int result = getsockopt(_socket, SOL_SOCKET, SO_RCVBUF, (char*)&bufferSize, &len);
            if (result == -1)
            {
                Debug::writeFormatLine("getsockopt'SO_RCVBUF' failed with error = %s", strerror(errno));
            }
        }
        return bufferSize;
    }
    void TcpServer::setReceiveBufferSize(int bufferSize)
    {
        if (_socket != -1)
        {
            int result = setsockopt(_socket, SOL_SOCKET, SO_RCVBUF, (const char*)&bufferSize, sizeof(int));
            if (result == -1)
            {
                Debug::writeFormatLine("setsockopt'SO_RCVBUF' failed with error = %s", strerror(errno));
            }
        }
    }
    
    bool TcpServer::isNoDelay() const
    {
        socklen_t yes = 0;
        if (_socket != -1)
        {
#if WIN32
            int len = sizeof(yes);
#else
            socklen_t len = sizeof(yes);
#endif
            int result = getsockopt(_socket, IPPROTO_TCP, TCP_NODELAY, (char*)&yes, &len);
            if (result == -1)
            {
                Debug::writeFormatLine("getsockopt'TCP_NODELAY' failed with error = %s", strerror(errno));
            }
        }
        return yes == 0 ? false : false;
    }
    void TcpServer::setNoDelay(bool noDelay)
    {
        if (_socket != -1)
        {
            int yes = noDelay ? 1 : 0;
            int result = setsockopt(_socket, IPPROTO_TCP, TCP_NODELAY, (char*)&yes, sizeof(yes));
            if (result == -1)
            {
                Debug::writeFormatLine("setsockopt'TCP_NODELAY' failed with error = %s", strerror(errno));
            }
        }
    }
}
