/*
 * UdpClient.cpp
 *
 *  Created on: May 15, 2014
 *      Author: baowei
 */

#if WIN32
#include <winsock2.h>
#include <Ws2tcpip.h>
#else
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <signal.h>
#endif
#include <errno.h>
#include "UdpClient.h"
#include "common/diag/Debug.h"
#include "common/system/Convert.h"
#include "common/system/Math.h"
#include "common/thread/TickTimeout.h"
#include "TcpClient.h"

#if WIN32
#undef errno
#define errno WSAGetLastError()
#define ioctl(s, cmd, argp) ioctlsocket(s, cmd, argp)
#define SHUT_RDWR SD_BOTH
#else
#define closesocket(a) ::close(a)
#endif

using namespace Common;

namespace Common
{
	UdpClient::UdpClient()
	{
		_socket = -1;
		TcpClient::initializeSocket();
	}

	UdpClient::~UdpClient()
	{
		close();

		_socket = -1;
	}

	bool UdpClient::open(const char *host, const ushort port)
	{
		/* Allocate a socket */
		_socket = socket(AF_INET, SOCK_DGRAM, 0);
		if (_socket < 0)
		{
			Debug::writeFormatLine("can't create udp client socket: %s", strerror(errno));
			return false;
		}

		bool allowBroadcast = host == NULL || strlen(host) == 0;

        struct sockaddr_in sin;

        /* Construct the server sockaddr_in structure */
        memset(&sin, 0, sizeof(sin));

        /* Clear struct */
        sin.sin_family = AF_INET;

        /* Internet/IP */
        sin.sin_addr.s_addr = allowBroadcast ? htonl(INADDR_BROADCAST) : inet_addr(host);

        /* IP address */
        sin.sin_port = htons(port);

        if(allowBroadcast)
        {
			int broadcast = 1;
			// if that doesn't work, try this
			//char broadcast = '1';

			/* server port */
			setsockopt(_socket,
					   IPPROTO_IP,
					   IP_MULTICAST_IF,
					   (const char*)&sin,
					   sizeof(sin));
			// this call is what allows broadcast packets to be sent:
			if (setsockopt(_socket,
						   SOL_SOCKET,
						   SO_BROADCAST,
						   (const char*)&broadcast,
						   sizeof(broadcast)) == -1)
			{
				Debug::writeFormatLine("setsockopt error: %s", strerror(errno));
				return false;
			}
        }

		return _socket != -1;
	}
	bool UdpClient::open(const ushort port)
	{
		return open(NULL, port);
	}
	void UdpClient::close()
	{
		if (_socket != -1)
		{
			closesocket(_socket);
			_socket = -1;
		}
	}

	int UdpClient::write(const char* host, const ushort port, const byte *data, uint len)
	{
		if(_socket != -1)
		{
			struct sockaddr_in sin;     /* an Internet endpoint address  */

			memset(&sin, 0, sizeof(sin));
			sin.sin_family = AF_INET;
			if ((sin.sin_port = htons(port)) == 0)
			{
				Debug::writeFormatLine("invalid port \"%d\"", port);
				return -1;
			}
			bool allowBroadcast = host == NULL || strlen(host) == 0;
			sin.sin_addr.s_addr = allowBroadcast ? htonl(INADDR_BROADCAST) : inet_addr(host);

			return (int)::sendto(_socket, (char*)data, len, 0, (struct sockaddr *)&sin, sizeof(sin));
		}
		return -1;
	}
	int UdpClient::write(const ushort port, const byte *data, uint len)
	{
		return write(NULL, port, data, len);
	}

	int UdpClient::sendBufferSize() const
	{
		socklen_t bufferSize = 0;
		if (_socket != -1)
		{
#if WIN32
			int len = sizeof(bufferSize);
#else
			socklen_t len = sizeof(bufferSize);
#endif
			int result = getsockopt(_socket, SOL_SOCKET, SO_SNDBUF, (char*)&bufferSize, &len);
			if (result == -1)
			{
				Debug::writeFormatLine("getsockopt'SO_SNDBUF' failed with error = %s", strerror(errno));
			}
		}
		return bufferSize;
	}
	void UdpClient::setSendBufferSize(int bufferSize)
	{
		if (_socket != -1)
		{
			int result = setsockopt(_socket, SOL_SOCKET, SO_SNDBUF, (const char*)&bufferSize, sizeof(int));
			if (result == -1)
			{
				Debug::writeFormatLine("setsockopt'SO_SNDBUF' failed with error = %s", strerror(errno));
			}
		}
	}

} /* namespace Driver */
