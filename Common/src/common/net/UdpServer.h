/*
 * UdpServer.h
 *
 *  Created on: May 15, 2014
 *      Author: baowei
 */

#ifndef UDPSERVER_H_
#define UDPSERVER_H_

#include <string>
#include "common/common_global.h"
#include "common/data/StringArray.h"
#if WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#else
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <signal.h>
#endif

using namespace std;
using namespace Common;

namespace Common
{
	class COMMON_EXPORT UdpServer
	{
	public:
		UdpServer();
		virtual ~UdpServer();

		bool bind(const string& addr, int port);

		void close();

		int read(byte *data, uint maxlen, bool peek = false);
        int write(byte *data, uint len, struct sockaddr_in * pSockAddr = NULL);
        int write(char *data, uint len, struct sockaddr_in * pSockAddr = NULL);
        int receive(byte *data, uint maxlen, uint timeout);

		inline bool isValid() const
		{
			return _socket != -1;
		}
        
        // local ip address.
        inline const string& address() const
        {
            return _address;
        }
        // local ip port.
        inline int port() const
        {
            return _port;
        }
        // peer ip address.
        inline const string& peerAddr() const
        {
            return _peerAddr;
        }
        // peer ip port.
        inline int peerPort() const
        {
            return _peerPort;
        }

        inline const struct sockaddr_in& sockAddr() const
        {
            return _sockaddr;
        }

		int available() const;
        
        bool setBlocking(bool blocking = false);

		int receiveBufferSize() const;
		void setReceiveBufferSize(int bufferSize);

	private:
		int _socket;
        
        string _peerAddr;
        int _peerPort;
        
        string _address;
        int _port;
        struct sockaddr_in _sockaddr;
    };

} /* namespace Driver */

#endif /* UDPSERVER_H_ */
