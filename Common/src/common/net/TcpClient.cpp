#if WIN32
#include <winsock2.h>
#include "../exception/Exception.h"
#pragma comment (lib, "Ws2_32.lib")
#else
#include <unistd.h>  
#include <sys/types.h>  
#include <sys/socket.h>  
#include <sys/ioctl.h>  
#include <netinet/in.h>  
#include <arpa/inet.h>  
#include <netdb.h>
#include <signal.h>
#include <net/if.h>
#include <netinet/tcp.h>
#endif
#include <errno.h>
#include "TcpClient.h"
#include "common/diag/Debug.h"
#include "common/system/Math.h"
#include "common/system/Convert.h"
#include "common/thread/TickTimeout.h"

#if WIN32
#undef errno
#define errno WSAGetLastError()
#define ioctl(s, cmd, argp) ioctlsocket(s, cmd, argp)
#define socklen_t int
#define SHUT_RDWR SD_BOTH
#else
#define closesocket(a) ::close(a)
#endif

using namespace Common;

namespace Common
{
	bool TcpClient::_initSocket = false;

	TcpClient::TcpClient(int socketId)
	{
		_socket = -1;
		_connected = false;
		_peerAddr = "";

		initializeSocket();

		if(socketId != -1)
		{
			_socket = socketId;
			_connected = true;

			struct sockaddr_in sin;
			socklen_t clen = sizeof(sin);
			memset(&sin, 0, clen);
			int result = ::getpeername(_socket, (struct sockaddr *)&sin, &clen);
			if(result != -1)
			{
				_peerAddr = inet_ntoa(sin.sin_addr);
				_peerPort = ntohs(sin.sin_port);
			}
			memset(&sin, 0, clen);
            result = ::getsockname(_socket, (struct sockaddr *)&sin, &clen);
			if (result != -1)
			{
				_address = inet_ntoa(sin.sin_addr);
				_port = ntohs(sin.sin_port);
			}
		}
	}

	TcpClient::~TcpClient(void)
	{
		close();

		_socket = -1;
		//#if WIN32
		//		WSACleanup();
		//#endif
	}

	bool TcpClient::connectToHost(const char *host, const ushort port, uint timeout)
	{
		unsigned long non_blocking = 1;  
		struct hostent  *phe;		/* pointer to host information entry    */
		struct sockaddr_in sin;     /* an Internet endpoint address  */  
		int result;

		memset(&sin, 0, sizeof(sin));  
		sin.sin_family = AF_INET;  
		if ((sin.sin_port = htons(port)) == 0)  
			Debug::writeFormatLine("invalid port \"%d\"", port);  

		/* Map host name to IP address, allowing for dotted decimal */  
		if ( (phe = gethostbyname(host)) != NULL )
			memcpy(&sin.sin_addr, phe->h_addr, phe->h_length);  
		else if ( (sin.sin_addr.s_addr = inet_addr(host)) == INADDR_NONE )  
			Debug::writeFormatLine("can't get \"%s\" host entry", host);  

		/* Allocate a socket */  
		_socket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);  
		if (_socket < 0)  
			Debug::writeFormatLine("can't create tcp client socket: %s", strerror(errno));  

		/* Connect the socket with timeout */  
		result = ioctl(_socket,FIONBIO,&non_blocking);  
        if (result != 0)
			Debug::writeFormatLine("ioctl failed with error: %s", strerror(errno));

		//fcntl(_socket,F_SETFL, O_NONBLOCK);  
		if (connect(_socket, (struct sockaddr *)&sin, sizeof(sin)) == -1)
		{
			struct timeval tv; 
            tv.tv_sec = (long)timeout / 1000;
            tv.tv_usec = 1000 * (timeout % 1000);
			fd_set myset; 
			FD_ZERO(&myset); 
			FD_SET(_socket, &myset); 
			if(select(_socket+1, NULL, &myset, NULL, &tv) != 0)
			{
				if(FD_ISSET(_socket, &myset))
				{
					socklen_t error = 0;
#if WIN32
					int len = sizeof(error);
#else
					socklen_t len = sizeof(error);
#endif
					if(getsockopt(_socket, SOL_SOCKET, SO_ERROR,  (char *)&error, &len) < 0)
					{
//						Debug::writeFormatLine("getsockopt1 function failed with error: %s", strerror(errno));
					}
					else if(error != 0)
					{
//						Debug::writeFormatLine("getsockopt2 function failed with error: %s", strerror(errno));
					}
					else
					{
						_connected = true;
                        
                        struct sockaddr_in sin;
                        socklen_t clen = sizeof(sin);
                        memset(&sin, 0, clen);
                        int result = ::getpeername(_socket, (struct sockaddr *)&sin, &clen);
                        if(result != -1)
                        {
                            _peerAddr = inet_ntoa(sin.sin_addr);
                            _peerPort = ntohs(sin.sin_port);
                        }
						memset(&sin, 0, clen);
                        result = ::getsockname(_socket, (struct sockaddr *)&sin, &clen);
						if (result != -1)
						{
							_address = inet_ntoa(sin.sin_addr);
							_port = ntohs(sin.sin_port);
						}
					}
				}  
				else
				{
					Debug::writeFormatLine("connect function failed with error: %s", strerror(errno)); //timeout or error happen   
				}
			}
		}

		return connected();
	}

	void TcpClient::close()
	{
		if (_socket != -1) 
		{
			int result;
			if(_connected)
			{
				result = shutdown(_socket, SHUT_RDWR);
				if (result == -1)
				{
					Debug::writeFormatLine("shutdown failed with error = %s", strerror(errno) );
				}
			}
			result = closesocket(_socket);
			if (result == -1)
			{
				Debug::writeFormatLine("closesocket failed with error = %s", strerror(errno) );
			}
			else
			{
				_socket = -1;
				_connected = false;
			}
		}
		//_connected = false;
	}

	int TcpClient::available() const
	{
		if(_socket != -1)
		{
			u_long argp = 0;
			if(ioctl(_socket, FIONREAD, &argp) == 0)
			{
				return (int)argp;
			}
		}
		return 0;
	}

	int TcpClient::write( const byte *data, uint len )
	{
		return _socket != -1 ? (int)::send(_socket, (char*)data, len, 0) : 0;
	}

	int TcpClient::read( byte *data, uint maxlen )
	{
		return _socket != -1 ? (int)::recv(_socket, (char*)data, maxlen, 0) : 0;
	}

	bool TcpClient::connected() const
	{
//		if(_connected)
//		{
//			fd_set myset; 
//			FD_ZERO(&myset); 
//			FD_SET(_socket, &myset); 
//			_connected = select(_socket+1, NULL, &myset, NULL, NULL) != 0 && FD_ISSET(_socket, &myset);
//#ifdef DEBUG
//			if(!_connected)
//				Debug::writeLine("TcpClient::connected = false.");
//#endif
//		}
		return _connected;
	}

	int TcpClient::receiveBySize(TcpClient* client, byte* buffer, int bufferLength, int offset, int count, uint timeout)
	{
		uint startTime = TickTimeout::GetCurrentTickCount();
		uint deadTime = TickTimeout::GetDeadTickCount(startTime, timeout);

		int available = client->available();
		bool dataReady = false;
		do
		{
			if (client->available() >= count)
			{
				dataReady = true;
				break;
			}
			uint now = TickTimeout::GetCurrentTickCount();
			if (client->available() == available)
			{
				Thread::msleep(1);
				if (TickTimeout::IsTimeout(startTime, deadTime, now))
					break;
			}
			else
			{
				available = client->available();
				deadTime = TickTimeout::GetDeadTickCount(now, timeout);
			}
		} while (true);

		if (client->available() > 0)
		{
			return dataReady
				? client->read(buffer + offset, count)
				: client->read(buffer + offset, Math::min(client->available(), bufferLength));
		}
		return 0;
	}

	void TcpClient::initializeSocket()
	{
		if(!_initSocket)
		{
#if WIN32
			// Declare and initialize variables
			WSADATA wsaData = {0};
			// Initialize Winsock
			int result = WSAStartup(MAKEWORD(2, 2), &wsaData);
			if (result != 0)
			{
				Debug::writeFormatLine("WSAStartup failed: %d", result);
				return;
			}
#else
			signal(SIGPIPE, SIG_IGN);
#endif
			_initSocket = true;
		}
	}

	string TcpClient::getIpAddress(const char* ifaceName)
	{
#if WIN32
		throw NotImplementedException("Can not get ip address by the face name.");
#else
		int fd;
		struct ifreq ifr;

		fd = socket(AF_INET, SOCK_DGRAM, 0);

		/* I want to get an IPv4 IP address */
		ifr.ifr_addr.sa_family = AF_INET;

		/* I want IP address attached to "eth0" */
		strncpy(ifr.ifr_name, ifaceName, IFNAMSIZ-1);

		ioctl(fd, SIOCGIFADDR, &ifr);

		closesocket(fd);

		return inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr);
#endif
	}

	int TcpClient::sendBufferSize() const
	{
		socklen_t bufferSize = 0;
		if (_socket != -1)
		{
#if WIN32
			int len = sizeof(bufferSize);
#else
			socklen_t len = sizeof(bufferSize);
#endif
			int result = getsockopt(_socket, SOL_SOCKET, SO_SNDBUF, (char*)&bufferSize, &len);
			if (result == -1)
			{
				Debug::writeFormatLine("getsockopt'SO_SNDBUF' failed with error = %s", strerror(errno));
			}
		}
		return bufferSize;
	}
	void TcpClient::setSendBufferSize(int bufferSize)
	{
		if (_socket != -1)
		{
			int result = setsockopt(_socket, SOL_SOCKET, SO_SNDBUF, (const char*)&bufferSize, sizeof(int));
			if (result == -1)
			{
				Debug::writeFormatLine("setsockopt'SO_SNDBUF' failed with error = %s", strerror(errno));
			}
		}
	}
	int TcpClient::receiveBufferSize() const
	{
		socklen_t bufferSize = 0;
		if (_socket != -1)
		{
#if WIN32
			int len = sizeof(bufferSize);
#else
			socklen_t len = sizeof(bufferSize);
#endif
			int result = getsockopt(_socket, SOL_SOCKET, SO_RCVBUF, (char*)&bufferSize, &len);
			if (result == -1)
			{
				Debug::writeFormatLine("getsockopt'SO_RCVBUF' failed with error = %s", strerror(errno));
			}
		}
		return bufferSize;
	}
	void TcpClient::setReceiveBufferSize(int bufferSize)
	{
		if (_socket != -1)
		{
			int result = setsockopt(_socket, SOL_SOCKET, SO_RCVBUF, (const char*)&bufferSize, sizeof(int));
			if (result == -1)
			{
				Debug::writeFormatLine("setsockopt'SO_RCVBUF' failed with error = %s", strerror(errno));
			}
		}
	}

	bool TcpClient::isNoDelay() const
	{
		socklen_t yes = 0;
		if (_socket != -1)
		{
#if WIN32
			int len = sizeof(yes);
#else
			socklen_t len = sizeof(yes);
#endif
			int result = getsockopt(_socket, IPPROTO_TCP, TCP_NODELAY, (char*)&yes, &len);
			if (result == -1)
			{
				Debug::writeFormatLine("getsockopt'TCP_NODELAY' failed with error = %s", strerror(errno));
			}
		}
		return yes == 0 ? false : false;
	}
	void TcpClient::setNoDelay(bool noDelay)
	{
		if (_socket != -1)
		{
			int yes = noDelay ? 1 : 0;
			int result = setsockopt(_socket, IPPROTO_TCP, TCP_NODELAY, (char*)&yes, sizeof(yes));
			if (result == -1)
			{
				Debug::writeFormatLine("setsockopt'TCP_NODELAY' failed with error = %s", strerror(errno));
			}
		}
	}
    bool TcpClient::setBlocking(bool blocking)
    {
        if (_socket != -1)
        {
            //-------------------------
            // Set the socket I/O mode: In this case FIONBIO
            // enables or disables the blocking mode for the
            // socket based on the numerical value of mode.
            // If mode = 0, blocking is enabled;
            // If mode != 0, non-blocking mode is enabled.
            u_long mode = blocking ? 0 : 1;
            int result = ::ioctl(_socket, FIONBIO, &mode);
            if (result != 0)
            {
                Debug::writeFormatLine("ioctlsocket failed with error: %d\n", result);
            }
            else
            {
                return true;
            }
        }
        return false;
    }
}
