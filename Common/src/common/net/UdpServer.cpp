/*
 * UdpServer.cpp
 *
 *  Created on: May 15, 2014
 *      Author: baowei
 */

#include "UdpServer.h"
#include <errno.h>
#include "common/diag/Debug.h"
#include "common/system/Convert.h"
#include "common/thread/TickTimeout.h"
#include "common/system/Math.h"
#include "TcpClient.h"

#if WIN32
#undef errno
#define errno WSAGetLastError()
#define ioctl(s, cmd, argp) ioctlsocket(s, cmd, argp)
#define SHUT_RDWR SD_BOTH
#else
#define closesocket(a) ::close(a)
#endif

namespace Common
{
    UdpServer::UdpServer()
    {
        _socket = -1;
        _peerAddr = "";
        _peerPort = 0;
		TcpClient::initializeSocket();
    }
    
    UdpServer::~UdpServer()
    {
        close();
        
        _socket = -1;
    }
    
    bool UdpServer::bind(const string& addr, int port)
    {
        struct hostent  *phe;		/* pointer to host information entry    */
        struct sockaddr_in sin;     /* an Internet endpoint address  */
        
        memset(&sin, 0, sizeof(sin));
        sin.sin_family = AF_INET;
        if ((sin.sin_port = htons(port)) == 0)
            Debug::writeFormatLine("invalid port \"%d\"", port);
        bool anyAddr = false;
        if(!addr.empty())
        {
            const char* anyStr = "any";
#if WIN32
            anyAddr = stricmp(addr.c_str(), anyStr) == 0;
#else
            anyAddr = strcasecmp(addr.c_str(), anyStr) == 0;
#endif
        }
        else
        {
            anyAddr = true;
        }
        if(!anyAddr)
        {
            if ( (phe = gethostbyname(addr.c_str())) != NULL )
                memcpy(&sin.sin_addr, phe->h_addr, phe->h_length);
            else if ( (sin.sin_addr.s_addr = inet_addr(addr.c_str())) == INADDR_NONE )
                Debug::writeFormatLine("can't get \"%s\" host entry", addr.c_str());
        }
        else
        {
            sin.sin_addr.s_addr = htonl(INADDR_ANY);
        }
        
        /* Allocate a socket */
		_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        if (_socket < 0)
            Debug::writeFormatLine("can't create udp server socket: %s", strerror(errno));
        
        setBlocking(true);

        return ::bind(_socket, (struct sockaddr *) & sin, sizeof (sin)) != -1;
    }
    
    void UdpServer::close()
    {
        if (_socket != -1)
        {
            closesocket(_socket);
            _socket = -1;
        }
    }
    
    bool UdpServer::setBlocking(bool blocking)
    {
        if (_socket != -1)
        {
            //-------------------------
            // Set the socket I/O mode: In this case FIONBIO
            // enables or disables the blocking mode for the
            // socket based on the numerical value of mode.
            // If mode = 0, blocking is enabled;
            // If mode != 0, non-blocking mode is enabled.
            u_long mode = blocking ? 0 : 1;
            int result = ::ioctl(_socket, FIONBIO, &mode);
            if (result != 0)
            {
                Debug::writeFormatLine("ioctlsocket failed with error: %d\n", result);
            }
            else
            {
                return true;
            }
        }
        return false;
    }

    int UdpServer::write(byte *data, uint len, struct sockaddr_in * pSockAddr)
    {
        return write((char *)data, len, pSockAddr);
    }
    
    int UdpServer::write(char *data, uint len, struct sockaddr_in * pSockAddr)
    {
        if (_socket == -1) return 0;
        if (pSockAddr == NULL)
        {
            pSockAddr = &_sockaddr;
        }

        return sendto(_socket, data, len, 0, (struct sockaddr *)pSockAddr, sizeof(*pSockAddr));
    }

    int UdpServer::read(byte *data, uint maxlen, bool peek)
    {
        if(_socket != -1)
        {
            memset(&_sockaddr, 0, sizeof(_sockaddr));
            socklen_t len = sizeof(_sockaddr);
            
            //            return (int)::recv(_socket, data, maxlen, 0);
            int result = (int)::recvfrom(_socket, (char*)data, maxlen, peek ? MSG_PEEK : 0, (struct sockaddr *)&_sockaddr, &len);
#if WIN32
			// If the datagram or message is larger than the buffer specified, the buffer is filled with the first part of the datagram,
			// and recvfrom generates the error WSAEMSGSIZE. For unreliable protocols (for example, udp) the excess data is lost.
            if(result > 0 || (result == -1 && WSAGetLastError() == WSAEMSGSIZE))
#else
			if (result > 0)
#endif
            {
#if WIN32
				if (result == -1)
					result = maxlen;
#endif

                _peerAddr = inet_ntoa(_sockaddr.sin_addr);
                _peerPort = ntohs(_sockaddr.sin_port);
                
                struct sockaddr_in sin;     /* an Internet endpoint address  */
                len = sizeof(sin);
                memset(&sin, 0, len);
                int result2 = ::getsockname(_socket, (struct sockaddr *)&sin, &len);
                if (result2 != -1)
                {
                    _address = inet_ntoa(sin.sin_addr);
                    _port = ntohs(sin.sin_port);
                }
            }
            return result;
        }
        return 0;
    }
    int UdpServer::receive(byte *data, uint maxlen, uint timeout)
    {
        int len = 0;
        if(_socket != -1)
        {
            uint startTime = (uint)TickTimeout::GetCurrentTickCount();
            uint deadTime = (uint)TickTimeout::GetDeadTickCount(startTime, timeout);
            do
            {
                int count = available();
                if (count >= (int)maxlen)
                {
                    len = maxlen;
                    break;
                }
                
                if (TickTimeout::IsTimeout(startTime, deadTime, TickTimeout::GetCurrentTickCount()))
                {
                    len = count;
                    break;
                }
                
                Thread::msleep(1);
            } while (true);
            
            return read(data, len);
        }
        return len;
    }
    
    int UdpServer::available() const
    {
        if(_socket != -1)
        {
            u_long argp = 0;
#if __APPLE__
            // ioctl (fd, FIONREAD, XXX) returns the size of
            // the UDP header as well on
            // Darwin.
            //
            // Use getsockopt SO_NREAD instead to get the
            // right values for TCP and UDP.
            //
            // ai_canonname can be null in some cases on darwin, where the runtime assumes it will
            // be the value of the ip buffer.
            
            socklen_t optlen = sizeof (int);
            if(getsockopt (_socket, SOL_SOCKET, SO_NREAD, &argp, &optlen) == 0)
            {
                return (int)argp;
            }
#else
            if(ioctl(_socket, FIONREAD, &argp) == 0)
            {
                return (int)argp;
            }
#endif
        }
        return 0;
    }

	int UdpServer::receiveBufferSize() const
	{
		socklen_t bufferSize = 0;
		if (_socket != -1)
		{
#if WIN32
			int len = sizeof(bufferSize);
#else
			socklen_t len = sizeof(bufferSize);
#endif
			int result = getsockopt(_socket, SOL_SOCKET, SO_RCVBUF, (char*)&bufferSize, &len);
			if (result == -1)
			{
				Debug::writeFormatLine("getsockopt'SO_RCVBUF' failed with error = %s", strerror(errno));
			}
		}
		return bufferSize;
	}
	void UdpServer::setReceiveBufferSize(int bufferSize)
	{
		if (_socket != -1)
		{
			int result = setsockopt(_socket, SOL_SOCKET, SO_RCVBUF, (const char*)&bufferSize, sizeof(int));
			if (result == -1)
			{
				Debug::writeFormatLine("setsockopt'SO_RCVBUF' failed with error = %s", strerror(errno));
			}
		}
	}
    
} /* namespace Driver */
