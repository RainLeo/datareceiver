#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <string>
#include "common/common_global.h"
#include "common/data/StringArray.h"

using namespace std;
using namespace Common;

namespace Common
{
    class COMMON_EXPORT TcpServer
    {
    public:
        TcpServer();
        ~TcpServer();
        
        bool bind(const string& address, int port);
        bool listen(int maxConnections = 30);
        int accept();
        void close();
        bool setBlocking(bool blocking = false);
        
        inline bool isValid() const
        {
            return _socket != -1;
        }
        
        int sendBufferSize() const;
        void setSendBufferSize(int bufferSize);
        int receiveBufferSize() const;
        void setReceiveBufferSize(int bufferSize);
        
        bool isNoDelay() const;
        void setNoDelay(bool noDelay = true);
        
    private:
        int _socket;
        bool _isListening;
    };
}

#endif // TCPSERVER_H
