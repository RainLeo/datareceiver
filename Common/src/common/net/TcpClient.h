#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include <time.h>
#include <string>
#include "common/common_global.h"

using namespace std;

namespace Common
{
	class COMMON_EXPORT TcpClient
	{
	public:
		TcpClient(int socketId = -1);
		~TcpClient(void);

		bool connectToHost(const char *host, const ushort port, uint timeout = 3000 );
		void close();

		int available() const;
		int write( const byte *data, uint len );
		int read( byte *data, uint maxlen );
		int receiveBySize(TcpClient* client, byte* buffer, int bufferLength, int offset, int count, uint timeout = 3000);

		bool connected() const;

        // local ip address.
		inline const string& address() const
		{
			return _address;
		}
        // local ip port.
		inline int port() const
		{
			return _port;
		}
        // peer ip address.
		inline const string& peerAddr() const
		{
			return _peerAddr;
		}
        // peer ip port.
		inline int peerPort() const
		{
			return _peerPort;
		}
		inline int socketId() const
		{
			return _socket;
		}

		int sendBufferSize() const;
		void setSendBufferSize(int bufferSize);
		int receiveBufferSize() const;
		void setReceiveBufferSize(int bufferSize);

		static void initializeSocket();

		static string getIpAddress(const char* ifaceName);

		bool isNoDelay() const;
		void setNoDelay(bool noDelay = true);
        bool setBlocking(bool blocking);

	private:
		int _socket;
		bool _connected;
		static bool _initSocket;
        
		string _peerAddr;
		int _peerPort;
        
		string _address;
		int _port;
	};
};
#endif //TCPCLIENT_H
