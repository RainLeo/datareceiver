#ifndef DNS_H
#define DNS_H

#include <string>
#include "common/common_global.h"
#include "../data/Array.h"
#include "../data/StringArray.h"

using namespace std;

namespace Common
{
	struct MacAddress
	{
	public:
		const static int Count = 6;
		byte Address[Count];

		MacAddress()
		{
			memset(Address, 0, sizeof(Address));
		}
		MacAddress(const byte addr[Count])
		{
			memcpy(Address, addr, sizeof(Address));
		}

		inline string toString() const
		{
			char temp[64];
			sprintf(temp, "%02X:%02X:%02X:%02X:%02X:%02X",
				Address[0], Address[1], Address[2], Address[3], Address[4], Address[5]);
			return temp;
		}
		static bool parse(const char* str, MacAddress& address)
		{
			if (str == NULL)
				return false;

			return parse((string)str, address);
		}
		static bool parse(const string& str, MacAddress& address)
		{
			int addr[Count];
			int result = sscanf(str.c_str(), "%02X:%02X:%02X:%02X:%02X:%02X",
				&addr[0], &addr[1], &addr[2], &addr[3], &addr[4], &addr[5]);
			if (result == Count &&
				addr[0] <= 255 && addr[1] <= 255 && addr[2] <= 255 &&
				addr[3] <= 255 && addr[4] <= 255 && addr[5] <= 255)
			{
				for (size_t i = 0; i < Count; i++)
				{
					address.Address[i] = (byte)addr[i];
				}
				return true;
			}
			return false;
		}
	};
	typedef Array<MacAddress> MacAddresses;

	struct IPAddress
	{
	public:
		const static int Count = 4;
		byte Address[Count];

		IPAddress()
		{
			memset(Address, 0, sizeof(Address));
		}
		IPAddress(byte addr[Count])
		{
			memcpy(Address, addr, sizeof(Address));
		}

		inline string toString() const
		{
			char temp[64];
			sprintf(temp, "%d.%d.%d.%d",
				Address[0], Address[1], Address[2], Address[3]);
			return temp;
		}
		static bool parse(const char* str, IPAddress& address)
		{
			if (str == NULL)
				return false;

			return parse((string)str, address);
		}
		static bool parse(const string& str, IPAddress& address)
		{
			int addr[Count];
			int result = sscanf(str.c_str(), "%d.%d.%d.%d",
				&addr[0], &addr[1], &addr[2], &addr[3]);
			if (result == Count &&
				addr[0] <= 255 && addr[1] <= 255 && addr[2] <= 255 && addr[3] <= 255)
			{
				for (size_t i = 0; i < Count; i++)
				{
					address.Address[i] = (byte)addr[i];
				}
				return true;
			}
			return false;
		}
	};
	typedef Array<IPAddress> IPAddresses;

	class Dns
	{
	public:
		static string getHostName();
		static bool getHostNames(StringArray& hostNames);

		static bool getMacAddresses(MacAddresses& addresses);

	private:
		Dns(){}
		~Dns(){}
	};
}

#endif	// DNS_H
