#ifndef PROCESS_H
#define PROCESS_H

#include <stdio.h>
#include <string>
#include "common/common_global.h"
#include "common/data/Vector.h"

using namespace std;

namespace Common
{
	class Process;
	typedef Vector<Process> Processes;

	class Process
	{
	public:
		Process();
		~Process();

		static bool start(const char* fileName, const char* arguments = NULL, Process* process = NULL);
		static bool start(const string& fileName, const string& arguments = "", Process* process = NULL);
		static bool getProcessById(int processId, Process& process);
		static bool getProcessByName(const char* processName, Processes& processes);
		static bool getProcessByName(const string& processName, Processes& processes);

		static bool waitForProcessExit(int processId, int milliseconds = -1);
		bool waitForExit(int milliseconds = -1) const;

		static bool getCurrentProcess(Process& process);

		static int getCurrentProcessId()
		{
			Process process;
			getCurrentProcess(process);
			return process._processId;
		}
		inline int getId() const
		{
			return _processId;
		}

		bool kill() const;
		bool closeMainWindow() const;
		int mainWindow() const;

	private:
		Process(int processId);

#if WIN32
		void enableDebugPriv() const;
#endif

	private:
		int _processId;
	};
}

#endif // PROCESS_H

