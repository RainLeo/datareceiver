#ifndef TIMEER_H
#define TIMEER_H

#include <stdio.h>
#include <stdint.h>
#include "Thread.h"
#include "common/system/TimeSpan.h"
#include "common/common_global.h"

namespace Common
{
	typedef void (*timer_callback)(void*);

	class COMMON_EXPORT Timer
	{
	public:
        // callback: A delegate representing a method to be executed.
        // state: An object containing information to be used by the callback method, or NULL.
        // dueTime: The amount of time to delay before the callback parameter invokes its methods.
        //          Specify Infinite to prevent the timer from starting.
        //          Specify Zero to start the timer immediately.
        // period: The time interval between invocations of callback, in milliseconds.
        //          Specify Infinite to disable periodic signaling.
        Timer(timer_callback callback, void* state, int dueTime, int period);
        Timer(timer_callback callback, void* state, int period);
        Timer(timer_callback callback, int period);
        
        Timer(timer_callback callback, void* state, TimeSpan dueTime, TimeSpan period);
        Timer(timer_callback callback, void* state, TimeSpan period);
        Timer(timer_callback callback, TimeSpan period);
        
        ~Timer();
        
//        bool change(int dueTime, int period);
//        bool change(TimeSpan dueTime, TimeSpan period);
        
    private:
        friend void timerProc(void* parameter);
        void timerProcInner();
        
        void fire(int period = 0);

    public:
        static const int Zero = 0;
        static const int Infinite = 0xFFFFFFFF;
        
	private:
        Thread* _thread;
        
        timer_callback _callback;
        void* _state;
        int _dueTime;
        int _period;
        
        bool _firstInvoke;
        uint _start;
	};
}
#endif // TIMEER_H
