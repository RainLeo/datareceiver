#ifndef THREAD_H
#define THREAD_H

#include <memory>
#include <thread>
#include "../common_global.h"
#include "../data/PrimitiveType.h"

#if !WIN32
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include <sched.h>
#endif	// WIN32

using namespace std;

namespace Common
{
    typedef void (*action_callback)();
    typedef void (*action_callback2)(void*);
    
    class Timer;
    class COMMON_EXPORT Thread
    {
    public:
        Thread(bool autoDelete = false);
        ~Thread();
        
        bool isAlive() const;
        void setSleepPeriod(uint msec);
        
        void start(action_callback startAction, action_callback stopAction = NULL);
        void startProc(action_callback procAction, uint msec = 1, action_callback startAction = NULL, action_callback stopAction = NULL);
        
        void start(action_callback2 startAction, void* startParam, action_callback2 stopAction = NULL, void* stopParam = NULL);
        void startProc(action_callback2 procAction, void* procParam, uint msec = 1, action_callback2 startAction = NULL,
                       void* startParam = NULL, action_callback2 stopAction = NULL, void* stopParam = NULL);
        
        void stop(uint delaySeconds = 10);
        void stopmsec(uint delaymsec = 10*1000);
        
        void setName(const string& name);
        
    public:
        static void msleep(uint msecs);
        
    private:
        friend void runAThread(void* parameter);
        
        void run();
        void start();
        
        void msleepWithBreak(uint start, uint msec);
        
        void loop();
        void loop2();
        
    private:
        friend Timer;
        
        action_callback _startAction;
        action_callback _procAction;
        action_callback _stopAction;
        
        action_callback2 _startAction2;
        action_callback2 _procAction2;
        action_callback2 _stopAction2;
        void* _procParam;
        void* _startParam;
        void* _stopParam;
        
        bool _break;
        uint _msec;
        bool _autoDelete;
        
        static auto_ptr<Thread> _tcs;
        
        thread* _thread;
        bool _running;
        
        String _name;
        bool _alreadySetName;
        
        uint _start;
        bool _firstInvoke;
    };
}

#endif // THREAD_H
