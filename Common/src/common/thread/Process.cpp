#include <string.h>
#include "Process.h"
#include "../system/Convert.h"
#include "../data/StringArray.h"
#include "../diag/Debug.h"
#include "../exception/Exception.h"
#if WIN32
#include <Windows.h>
#include <Psapi.h>
#include <TlHelp32.h>
#pragma comment (lib, "User32.lib")
#pragma comment (lib, "Advapi32.lib")
#else
#include <limits.h>
#include <unistd.h>
#endif

namespace Common
{
	Process::Process() : Process(-1)
	{
	}
	Process::Process(int processId)
	{
		_processId = processId;
	}

	Process::~Process()
	{
	}

#if WIN32
	void Process::enableDebugPriv() const
	{
		HANDLE hToken;
		LUID luid;
		TOKEN_PRIVILEGES tkp;

		OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken);

		LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &luid);

		tkp.PrivilegeCount = 1;
		tkp.Privileges[0].Luid = luid;
		tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

		AdjustTokenPrivileges(hToken, false, &tkp, sizeof(tkp), NULL, NULL);

		CloseHandle(hToken);
	}
#endif

	bool Process::start(const char* fileName, const char* arguments, Process* process)
	{
		if (fileName == NULL)
			throw ArgumentNullException("fileName");
#if WIN32
		STARTUPINFO si;
		PROCESS_INFORMATION pi;
		STARTUPINFO sj;
		PROCESS_INFORMATION pj;
		memset(&si, 0, sizeof(si));
		si.cb = sizeof(si);
		memset(&pi, 0, sizeof(pi));
		memset(&sj, 0, sizeof(sj));
		sj.cb = sizeof(sj);
		memset(&pj, 0, sizeof(pj));
		string clineStr = arguments != NULL ? Convert::convertStr("%s %s", fileName, arguments) : fileName;
		if (!CreateProcess(NULL, (char*)clineStr.c_str(), NULL, NULL, FALSE, 0, NULL, NULL, &sj, &pj))
		{
			Debug::writeFormatLine("CreateProcess failed (%d)", GetLastError());
			return false;
		}
		if (process != NULL)
		{
			process->_processId = pj.dwProcessId;
		}
#else
		char** argv = NULL;
		uint len = 0;
		if(arguments != NULL)
		{
			string temp = arguments;
			StringArray as;
			Convert::splitStr(temp, ' ', as);
			len = as.count();
			argv = new char*[len];
			for(uint i = 0;i<len;i++)
			{
				const string valueStr = as[i];
				size_t tempLen = valueStr.length() + 1;
				argv[i] = new char[tempLen];
				memset(argv[i], 0, tempLen);
				strcpy(argv[i], valueStr.c_str());
			}
		}
		int result = execv(fileName, argv);
		if(argv != NULL)
		{
			assert(len > 0);
			for(size_t i = 0;i<len;i++)
				delete[] argv[i];
			delete[] argv;
		}
		if(result == -1)
		{
			Debug::writeFormatLine("CreateProcess failed (%d)", result);
		}
#endif
		return true;
	}
	bool Process::start(const string& fileName, const string& arguments, Process* process)
	{
		return start(fileName.c_str(), arguments.empty() ? NULL : arguments.c_str(), process);
	}

	bool Process::getProcessById(int processId, Process& process)
	{
#if WIN32
		DWORD pid = 0;

		// Create toolhelp snapshot.
		HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
		PROCESSENTRY32 entry;
		ZeroMemory(&entry, sizeof(entry));
		entry.dwSize = sizeof(entry);

		// Walkthrough all processes.
		if (Process32First(snapshot, &entry))
		{
			do
			{
				// Compare process.szExeFile based on format of name, i.e., trim file path
				// trim .exe if necessary, etc.
				//if (MatchProcessName(process.szExeFile, name))
				if (processId == entry.th32ProcessID)
				{
					pid = entry.th32ProcessID;
					break;
				}
			} while (Process32Next(snapshot, &entry));
		}

		CloseHandle(snapshot);

		if (pid > 0)
		{
			process._processId = pid;
			return true;
		}
#else
#endif
		return false;
	}

	bool Process::getProcessByName(const char* processName, Processes& processes)
	{
#if WIN32
		// Create toolhelp snapshot.
		HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
		PROCESSENTRY32 entry;
		ZeroMemory(&entry, sizeof(entry));
		entry.dwSize = sizeof(entry);

		// Walkthrough all processes.
		if (Process32First(snapshot, &entry))
		{
			do
			{
				// Compare process.szExeFile based on format of name, i.e., trim file path
				// trim .exe if necessary, etc.
				if (strcmp(entry.szExeFile, processName) == 0)
				{
					Process* process = new Process(entry.th32ProcessID);
					processes.add(process);
				}
			} while (Process32Next(snapshot, &entry));
		}

		CloseHandle(snapshot);

		return processes.count() > 0;
#else
#endif
		return false;
	}
	bool Process::getProcessByName(const string& processName, Processes& processes)
	{
		return getProcessByName(processName.c_str(), processes);
	}

	bool Process::waitForExit(int milliseconds) const
	{
#if WIN32
		if (_processId != -1)
		{
			enableDebugPriv();
			HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, _processId);
			if (hProcess != NULL)
			{
				WaitForSingleObject(hProcess, milliseconds);
				CloseHandle(hProcess);

				return true;
			}
		}
#else
#endif
		return false;
	}
	bool Process::waitForProcessExit(int processId, int milliseconds)
	{
#if WIN32
		Process process;
		if (getProcessById(processId, process))
		{
			return process.waitForExit(milliseconds);
		}
#else
#endif
		return false;
	}

	bool Process::getCurrentProcess(Process& process)
	{
#if WIN32
		DWORD processId = ::GetCurrentProcessId();
		if (processId != -1)
		{
			process._processId = processId;
		}
#else
#endif
		return false;
	}

	bool Process::kill() const
	{
#if WIN32
		enableDebugPriv();
		HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, _processId);
		if (hProcess != NULL)
		{
			bool result = TerminateProcess(hProcess, -1) != 0;
			if (!result)
			{
				DWORD error = GetLastError();
				Debug::writeFormatLine("Failed to TerminateProcess, error: %d", error);
			}
			CloseHandle(hProcess);
			return result;
		}
#else
#endif
		return false;
	}

	bool Process::closeMainWindow() const
	{
#if WIN32
		int main = mainWindow();
		if (main != NULL)
		{
			return PostMessage((HWND)main, WM_CLOSE, NULL, NULL) != 0;
		}
#else
#endif
		return false;
	}

#if WIN32
	struct ProcessWindows
	{
		DWORD dwCurrentProcessId;
		HWND hCurrentProcessWin;
	};
	BOOL IsMainWindow(HWND hWnd)
	{
		return (!(GetWindow(hWnd, GW_OWNER) != NULL) && IsWindowVisible(hWnd));
	}
	BOOL CALLBACK EnumWindowsProc(HWND hWnd, LPARAM lParam)
	{
		DWORD dwProcessId = 0;
		GetWindowThreadProcessId(hWnd, &dwProcessId);

		ProcessWindows* ppw = (ProcessWindows*)lParam;
		if (ppw != NULL)
		{
			if (ppw->dwCurrentProcessId == dwProcessId && IsMainWindow(hWnd))
			{
				ppw->hCurrentProcessWin = hWnd;
				return FALSE;
			}
		}
		return TRUE;
	}
#endif
	int Process::mainWindow() const
	{
#if WIN32
		ProcessWindows pw;
		memset(&pw, 0, sizeof(pw));
		pw.dwCurrentProcessId = _processId;
		EnumWindows(EnumWindowsProc, (LPARAM)&pw);
		return (int)pw.hCurrentProcessWin;
#else
        // todo: return the main window.
        return -1;
#endif
	}
}
