#ifndef MUTEX_H
#define MUTEX_H

#if WIN32
#include <Windows.h>
#else
#endif

namespace Common
{
	class Mutex
	{
	public:
		Mutex(bool initiallyOwned, const char* name, bool& createdNew)
		{
			_mutex = CreateMutex(NULL, initiallyOwned, name); //do early
			DWORD error = GetLastError();
			createdNew = ERROR_ALREADY_EXISTS != error;
		}
		~Mutex()
		{
			if (_mutex != NULL)
			{
				CloseHandle(_mutex);
				_mutex = NULL;
			}
		}

	private:
#if WIN32
		HANDLE _mutex;
#else
#endif
	};
}

#endif // MUTEX_H
