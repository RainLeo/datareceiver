#include "Timer.h"
#include "common/system/Convert.h"
#include "TickTimeout.h"
#include "common/diag/Debug.h"

namespace Common
{
    void timerProc(void* parameter)
    {
        Timer* timer = (Timer*)parameter;
        timer->timerProcInner();
    }
    Timer::Timer(timer_callback callback, void* state, int dueTime, int period)
    {
        if(callback == NULL)
            throw ArgumentNullException("callback");
        
        _callback = callback;
        _state = state;
        _dueTime = dueTime;
        _period = period;
        
        _firstInvoke = true;
        _start = 0;
        
        _thread = new Thread();
        static int no = 0;
        no++;
        _thread->setName(String::convert("timerProc%d", no).c_str());
		_thread->startProc(timerProc, this, 1);
    }
    Timer::Timer(timer_callback callback, void* state, int period) : Timer(callback, state, Zero, period)
    {
    }
    Timer::Timer(timer_callback callback, int period) : Timer(callback, NULL, period)
    {
    }
    
    Timer::Timer(timer_callback callback, void* state, TimeSpan dueTime, TimeSpan period)
    : Timer(callback, state, (int)dueTime.totalMilliseconds(), (int)period.totalMilliseconds())
    {
    }
    Timer::Timer(timer_callback callback, void* state, TimeSpan period) : Timer(callback, state, TimeSpan::Zero, period)
    {
    }
    Timer::Timer(timer_callback callback, TimeSpan period) : Timer(callback, NULL, period)
    {
    }
    
    Timer::~Timer()
    {
        if(_thread != NULL)
        {
            _thread->stop();
            delete _thread;
            _thread = NULL;
        }
    }
    
    //    bool Timer::change(int dueTime, int period);
    //    bool Timer::change(TimeSpan dueTime, TimeSpan period);
    
    void Timer::fire(int period)
    {
        if(period > 0)
        {
            _start = TickTimeout::GetNextTickCount(_start, period);
        }
        _callback(_state);
    }
    void Timer::timerProcInner()
    {
        if(_firstInvoke)
        {
            Debug::writeLine("Timer fire start.");
            
            _firstInvoke = false;
            _start = TickTimeout::GetCurrentTickCount();
            
            if(_dueTime == Zero)
            {
                fire();
            }
            else if(_dueTime == Infinite)
            {
                
            }
            else
            {
                _thread->msleepWithBreak(_start, _dueTime);
                fire(_dueTime);
            }
        }
        
        _thread->msleepWithBreak(_start, _period);
        
        if(_dueTime != Infinite)
        {
            fire(_period);
        }
    }
};
