#include "TickTimeout.h"
#if WIN32
#include <Windows.h>
#else
#include <time.h>
#include <sys/time.h>
#endif

namespace Common
{
	uint TickTimeout::GetCurrentTickCount()
	{
#if WIN32
		return GetTickCount();
#else
		return mono_msec_ticks();
		//struct timespec ts;
		//clock_gettime(CLOCK_MONOTONIC, &ts);
		//return (ts.tv_sec * 1000 + ts.tv_nsec / 1000000);
#endif
	}

#if !WIN32
#define MTICKS_PER_SEC 10000000

		/* Returns the number of 100ns ticks from unspecified time: this should be monotonic */
		int64_t TickTimeout::mono_100ns_ticks ()
		{
			struct timeval tv;
			if (gettimeofday (&tv, NULL) == 0)
				return ((int64_t)tv.tv_sec * 1000000 + tv.tv_usec) * 10;
			return 0;
		}

		int64_t TickTimeout::get_boot_time ()
		{
			FILE *uptime = fopen ("/proc/uptime", "r");
			if (uptime) {
				double upt;
				if (fscanf (uptime, "%lf", &upt) == 1) {
					int64_t now = mono_100ns_ticks ();
					fclose (uptime);
					return now - (int64_t)(upt * MTICKS_PER_SEC);
				}
				fclose (uptime);
			}
			/* a made up uptime of 300 seconds */
			return (int64_t)300 * MTICKS_PER_SEC;
		}

		/* Returns the number of milliseconds from boot time: this should be monotonic */
		uint TickTimeout::mono_msec_ticks ()
		{
			static int64_t boot_time = 0;
			int64_t now;
			if (!boot_time)
				boot_time = get_boot_time ();
			now = mono_100ns_ticks ();
			//printf ("now: %llu (boot: %llu) ticks: %llu\n", (int64_t)now, (int64_t)boot_time, (int64_t)(now - boot_time));
			return (uint)((now - boot_time)/10000);
		}
#endif
    
    bool TickTimeout::IsTimeout(uint start, uint end, uint now)
    {
        if (end == start) return true;
        if (end > start)
        {
            return now > end /*|| now < start*/; // Modified by HYC on 2016/3/19 to fix bug when 'now < start' really happens in some cases and timer does not function well
        }
        return now > end && now < start;
    }
    
    bool TickTimeout::IsTimeout(uint start, uint end)
    {
        return IsTimeout(start, end, GetCurrentTickCount());
    }
    
    uint TickTimeout::GetDeadTickCount(uint timeout)
    {
        return GetDeadTickCount(GetCurrentTickCount(), timeout);
    }
    
    uint TickTimeout::GetDeadTickCount(uint start, uint timeout)
    {
        return (uint)(start + timeout);
    }
    
    uint TickTimeout::Elapsed(uint start, uint end)
    {
        return (end >= start) ? end - start : MaxTickCount - end + start;
    }
    uint TickTimeout::Elapsed(uint start)
    {
        return Elapsed(start, GetCurrentTickCount());
    }
    
    uint TickTimeout::GetPrevTickCount(uint start, uint elapsed)
    {
        uint curTick = start == MaxTickCount ? GetCurrentTickCount() : start;
        if (curTick >= elapsed)
        {
            return curTick - elapsed;
        }
        else
        {
            return MaxTickCount - (elapsed - curTick);
        }
    }
    uint TickTimeout::GetNextTickCount(uint start, uint elapsed)
    {
        uint curTick = start == MaxTickCount ? GetCurrentTickCount() : start;
        if ((uint64_t)(curTick + elapsed) <= MaxTickCount)
        {
            return curTick + elapsed;
        }
        else
        {
            return elapsed - (MaxTickCount - curTick);
        }
    }
    
    void TickTimeout::sdelay(uint sec, delay_callback condition, void* parameter, uint sleepms)
    {
        msdelay(sec * 1000, condition, parameter, sleepms);
    }
    void TickTimeout::msdelay(uint msec, delay_callback condition, void* parameter, uint sleepms)
    {
        uint startTime = GetCurrentTickCount();
        uint deadTime = GetDeadTickCount(startTime, msec);
        
        do
        {
            if (condition != NULL && condition(parameter))
            {
                break;
            }
            
            if (IsTimeout(startTime, deadTime, GetCurrentTickCount()))
            {
                break;
            }
            
            Thread::msleep(sleepms);
        } while (true);
    }
};
