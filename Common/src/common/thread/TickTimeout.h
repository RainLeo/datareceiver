#ifndef TICKTIMEOUT_H
#define TICKTIMEOUT_H

#include <stdio.h>
#include <stdint.h>
#include "Thread.h"
#include "common/common_global.h"

namespace Common
{
	typedef bool (*delay_callback)(void*);

	class COMMON_EXPORT TickTimeout
	{
	public:
        static bool IsTimeout(uint start, uint end, uint now);
        static bool IsTimeout(uint start, uint end);

        static uint GetDeadTickCount(uint timeout);
        static uint GetDeadTickCount(uint start, uint timeout);

        static uint Elapsed(uint start, uint end);
        static uint Elapsed(uint start);

		static uint GetCurrentTickCount();

        static uint GetPrevTickCount(uint start, uint elapsed);
        static uint GetNextTickCount(uint start, uint elapsed);

        static void sdelay(uint sec = 10, delay_callback condition = NULL, void* parameter = NULL, uint sleepms = 1);
        static void msdelay(uint msec = 3000, delay_callback condition = NULL, void* parameter = NULL, uint sleepms = 1);

	private:
        const static uint MaxTickCount = UINT32_MAX;
        
#if !WIN32
		/* Returns the number of 100ns ticks from unspecified time: this should be monotonic */
		static int64_t mono_100ns_ticks ();

		static int64_t get_boot_time ();

		/* Returns the number of milliseconds from boot time: this should be monotonic */
		static uint mono_msec_ticks ();
#endif
	};
}
#endif // TICKTIMEOUT_H
