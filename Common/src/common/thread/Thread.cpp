#include <thread>
#include "Thread.h"
#include "TickTimeout.h"
#include "../diag/Stopwatch.h"
#include "TickTimeout.h"
#include "../system/Convert.h"
#include "../diag/Trace.h"

#if WIN32
#include <windows.h>
#endif	// WIN32

namespace Common
{
    auto_ptr<Thread> Thread::_tcs;
    
#if WIN32
    const DWORD MS_VC_EXCEPTION = 0x406D1388;
    
#pragma pack(push,8)
    typedef struct tagTHREADNAME_INFO
    {
        DWORD dwType; // Must be 0x1000.
        LPCSTR szName; // Pointer to name (in user addr space).
        DWORD dwThreadID; // Thread ID (-1=caller thread).
        DWORD dwFlags; // Reserved for future use, must be zero.
    } THREADNAME_INFO;
#pragma pack(pop)
    
    //
    // Usage: SetThreadName (-1, "MainThread");
    //
    void SetThreadName(DWORD dwThreadID, const char* threadName)
    {
        THREADNAME_INFO info;
        info.dwType = 0x1000;
        info.szName = threadName;
        info.dwThreadID = dwThreadID;
        info.dwFlags = 0;
        
        __try
        {
            RaiseException( MS_VC_EXCEPTION, 0, sizeof(info)/sizeof(ULONG_PTR), (ULONG_PTR*)&info );
        }
        __except(EXCEPTION_EXECUTE_HANDLER)
        {
        }
    }
#endif	// WIN32
    
    void SetThreadName(const char* threadName)
    {
#if WIN32
        SetThreadName(GetCurrentThreadId(), threadName);
#else
//        pthread_setname_np(threadName);
#endif
    }
    
    bool isThreadDead(void* parameter)
    {
        return parameter != NULL ? !((Thread*)parameter)->isAlive() : false;
    }
    void runAThread(void* parameter)
    {
        if(parameter != NULL)
        {
            ((Thread*)parameter)->run();
        }
    }
    
    Thread::Thread(bool autoDelete)
    {
        _startAction = NULL;
        _procAction = NULL;
        _stopAction = NULL;
        
        _startAction2 = NULL;
        _procAction2 = NULL;
        _stopAction2 = NULL;
        _procParam = NULL;
        _startParam = NULL;
        _stopParam = NULL;
        
        _break = false;
        _msec = 1;
        _autoDelete = autoDelete;
        
        _thread = NULL;
        _running = false;
        
        _name = "";
        _alreadySetName = false;
        
        _start = 0;
        _firstInvoke = true;
    }
    Thread::~Thread()
    {
    }
    
    void Thread::start(action_callback startAction, action_callback stopAction)
    {
        startProc(NULL, 1, startAction, stopAction);
    }
    void Thread::startProc(action_callback procAction, uint msec, action_callback startAction, action_callback stopAction)
    {
        if(_autoDelete)
        {
            if(_tcs.get() == NULL)
            {
                _tcs.reset(this);
            }
        }
        
        if(isAlive())
            return;
        
        _procAction = procAction;
        _msec = msec;
        _startAction = startAction;
        _stopAction = stopAction;
        
        start();
    }
    void Thread::start(action_callback2 startAction, void* startParam, action_callback2 stopAction, void* stopParam)
    {
        startProc(NULL, NULL, 1, startAction, startParam, stopAction, stopParam);
    }
    void Thread::startProc(action_callback2 procAction, void* procParam, uint msec, action_callback2 startAction,
                                  void* startParam, action_callback2 stopAction, void* stopParam)
    {
        if(_autoDelete)
        {
            if(_tcs.get() == NULL)
            {
                _tcs.reset(this);
            }
        }
        
        if(isAlive())
            return;
        
        _procAction2 = procAction;
        _procParam = procParam;
        _msec = msec;
        _startAction2 = startAction;
        _startParam = startParam;
        _stopAction2 = stopAction;
        _stopParam = stopParam;
        
        start();
    }
    
    void Thread::stopmsec(uint delaymsec)
    {
        if(isAlive())
        {
            _break = true;
            
#ifdef DEBUG
            String msg = String::convert("Thread::stopmsec, name: %s", _name.c_str());
            Stopwatch sw(msg);
            
            Debug::writeFormatLine("Start Thread::stopmsec, name: %s", _name.c_str());
#endif
            
            thread::id this_id = this_thread::get_id();
            thread::id thread_id = _thread->get_id();
            if (this_id != thread_id)
            {
                TickTimeout::msdelay(delaymsec, isThreadDead, this);
                
                if (isAlive())
                {
                    _thread->detach();
                    //terminate();
                }
            }
            
            _thread = NULL;
            _running = false;
        }
    }
    void Thread::stop(uint delaySeconds)
    {
        stopmsec(delaySeconds * 1000);
    }
    
    void Thread::start()
    {
        if(!_running)
        {
            _thread = new thread(runAThread, this);
            _running = true;
        }
    }
    
    bool Thread::isAlive() const
    {
        return _running;
    }
    void Thread::setSleepPeriod(uint msec)
    {
        uint temp = _msec;
        _msec = msec;
        Thread::msleep(temp);
    }
    
    void Thread::setName(const string& name)
    {
        _name = name;
    }
    
    void Thread::run()
    {
        try
        {
            if(!_alreadySetName && !_name.isNullOrEmpty())
            {
                _alreadySetName = true;
                SetThreadName(_name);
            }
            
            if(_startAction != NULL)
            {
                _startAction();
            }
            else if(_startAction2 != NULL)
            {
                _startAction2(_startParam);
            }
            
            if (_procAction != NULL)
            {
                loop();
            }
            else if (_procAction2 != NULL)
            {
                loop2();
            }
            
            if(_stopAction != NULL)
            {
                _stopAction();
            }
            else if(_stopAction2 != NULL)
            {
                _stopAction2(_stopParam);
            }
        }
        catch(const exception& e)
        {
            String str = String::convert("Generate error'%s' in thread'%s' running", e.what(), _name.c_str());
            Trace::writeLine(str, Trace::Error);
        }
        
        _running = false;
    }
    
    void Thread::loop()
    {
        while (!_break)
        {
            if(_firstInvoke)
            {
                _firstInvoke = false;
                _start = TickTimeout::GetCurrentTickCount();
            }
            else
            {
                _start = TickTimeout::GetNextTickCount(_start, _msec);
            }
            
            _procAction();
            
            msleepWithBreak(_start, _msec);
        }
    }
    void Thread::loop2()
    {
        while (!_break)
        {
            if(_firstInvoke)
            {
                _firstInvoke = false;
                _start = TickTimeout::GetCurrentTickCount();
            }
            else
            {
                _start = TickTimeout::GetNextTickCount(_start, _msec);
            }
            
            _procAction2(_procParam);
            msleepWithBreak(_start, _msec);
        }
    }

    void Thread::msleepWithBreak(uint start, uint msec)
    {
        const uint unitmsec = 1;		// 1 ms
        if(msec <= unitmsec)
        {
            Thread::msleep(msec);
        }
        else
        {
            uint end = TickTimeout::GetDeadTickCount(start, msec);
            while (!_break)
            {
                if(TickTimeout::IsTimeout(start, end))
                {
                    break;
                }
                Thread::msleep(unitmsec);
            }
        }
    }
    
    void Thread::msleep(uint msecs)
    {
#if WIN32
        Sleep((DWORD)msecs);
#else
        usleep(msecs * 1000);
        
        //        struct timeval delay;
        //        delay.tv_sec = msecs / 1000;
        //        delay.tv_usec = 1000 * (msecs % 1000);
        //        select(0, NULL, NULL, NULL, &delay);
        
        //        int ret;
        //        struct timespec req, rem;
        //
        //        req.tv_sec = msecs / 1000;
        //        req.tv_nsec = (msecs % 1000) * 1000000;
        //
        //        do {
        //            memset (&rem, 0, sizeof (rem));
        //            ret = nanosleep (&req, &rem);
        //        } while (ret != 0);
#endif
    }
}
