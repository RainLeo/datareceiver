﻿#include "Resources.h"
#include "system/Application.h"

namespace Common
{
	const char* Resources::EmptyStr = "";
	const char* Resources::NullStr = "-";
	const char* Resources::ErrorStr = "错误";
	const char* Resources::WarningStr = "警告";
	const char* Resources::OkStr =  "确定";
	const char* Resources::CancelStr = "取消";
	const char* Resources::YesStr = "是";
	const char* Resources::NoStr = "否";
	const char* Resources::PromptStr = "提示";

	const char* Resources::RemoveLogFilesSuccessfullyStr = "移除日志文件'%s'成功！";
	const char* Resources::FailedtoRemoveLogFilesStr = "移除日志文件'%s'失败！";

	const char* Resources::SocketListenFailedStr = "监听IP地址'%s'，端口'%d'失败！端口可能被占用。";
	const char* Resources::SocketBindingFailedStr =  "绑定IP地址'%s'，端口'%d'失败！端口可能被占用。";
	const char* Resources::BluetoothListenFailedStr = "监听蓝牙失败！";
	const char* Resources::BluetoothBindingFailedStr = "绑定蓝牙失败！";

	const char* Resources::UnabletoConnectDeviceStr = "无法连接设备, 设备名称: %s";
	const char* Resources::DeviceFailureStr = "设备失效, 设备名称: %s";
	const char* Resources::RetryingConnectDeviceStr = "重新尝试连接设备, 设备名称: %s";

	const char* Resources::SocketBindingFailedStr2 =  "绑定 UDP 端口'%d'失败！端口可能被占用。";
    
    const char* Resources::NetSendInfoStr = "send: %s";
    const char* Resources::NetReceivedInfoStr = "recv: %s";
    
    const char* Resources::NetStateChangedStr = "服务端'%s'状态从'%s'变为'%s'.";
    
    const char* Resources::CreateTcpChannelInfoStr = "创建以太网通道: 名称: %s, IP 地址: %s, 端口: %d";
    const char* Resources::CreateTcpServerChannelInfoStr = "创建以太网服务通道: 名称: %s, IP 地址: %s, 端口: %d";
    
    const char* Resources::FailedtoSendInstructionStr = "发送网络指令失败，名称: '%s', 异常: %s";
    const char* Resources::CommunicationExceptionStr = "通讯异常";
    
    const char* Resources::en_EmptyStr = "";
    const char* Resources::en_NullStr = "-";
    const char* Resources::en_ErrorStr = "Error";
    const char* Resources::en_WarningStr = "Warning";
    const char* Resources::en_OkStr =  "Ok";
    const char* Resources::en_CancelStr = "Cancel";
    const char* Resources::en_YesStr = "Yes";
    const char* Resources::en_NoStr = "No";
    const char* Resources::en_PromptStr = "Prompt";
    
    const char* Resources::en_RemoveLogFilesSuccessfullyStr = "Remove log file'%s' successfully!";
    const char* Resources::en_FailedtoRemoveLogFilesStr = "Failed to remove log file'%s'!";
    
    const char* Resources::en_SocketListenFailedStr = "Failed to listen IP address'%s', port'%d'! The port may be used.";
    const char* Resources::en_SocketBindingFailedStr =  "Failed to bind IP address'%s'，port'%d'! The port may be used.";
    const char* Resources::en_BluetoothListenFailedStr = "Failed to listen bluetooth!";
    const char* Resources::en_BluetoothBindingFailedStr = "Failed to bind bluetooth!";
    
    const char* Resources::en_UnabletoConnectDeviceStr = "Unable to connect the device, name: %s";
    const char* Resources::en_DeviceFailureStr = "The device is failure, name: %s";
    const char* Resources::en_RetryingConnectDeviceStr = "Retry to connect the device, name: %s";
    
    const char* Resources::en_SocketBindingFailedStr2 =  "Failed to bind UDP, port'%d'! The port may be used.";
    
    const char* Resources::en_NetSendInfoStr = "send: %s";
    const char* Resources::en_NetReceivedInfoStr = "recv: %s";
    
    const char* Resources::en_NetStateChangedStr = "The server'%s' state changed, from'%s'to'%s'.";
    
    const char* Resources::en_CreateTcpChannelInfoStr = "Create tcp channel: name: %s, IP address: %s, port: %d";
    const char* Resources::en_CreateTcpServerChannelInfoStr = "Create tcp server channel: name: %s, IP address: %s, port: %d";
    
    const char* Resources::en_FailedtoSendInstructionStr = "Failed to send the instruction, name: '%s', exception: %s";
    const char* Resources::en_CommunicationExceptionStr = "Communication exception";

    string Resources::getString(const string& name)
    {
        return getStaticString(name);
    }
    string Resources::getStaticString(const string& name)
    {
        const Culture& culture = Application::instance()->culture();
        bool isChinese = culture.id() == 0x0004 || culture.id() == 0x0804;
        if(name == "Empty")
        {
            return isChinese ? EmptyStr : en_EmptyStr;
        }
        else if(name == "Null")
        {
            return isChinese ? NullStr : en_NullStr;
        }
        else if(name == "Error")
        {
            return isChinese ? ErrorStr : en_ErrorStr;
        }
        else if(name == "Warning")
        {
            return isChinese ? WarningStr : en_WarningStr;
        }
        else if(name == "Ok")
        {
            return isChinese ? OkStr : en_OkStr;
        }
        else if(name == "Yes")
        {
            return isChinese ? YesStr : en_YesStr;
        }
        else if(name == "No")
        {
            return isChinese ? NoStr : en_NoStr;
        }
        else if(name == "Prompt")
        {
            return isChinese ? PromptStr : en_PromptStr;
        }
        else if(name == "RemoveLogFilesSuccessfully")
        {
            return isChinese ? RemoveLogFilesSuccessfullyStr : en_RemoveLogFilesSuccessfullyStr;
        }
        else if(name == "FailedtoRemoveLogFiles")
        {
            return isChinese ? FailedtoRemoveLogFilesStr : en_FailedtoRemoveLogFilesStr;
        }
        else if(name == "SocketListenFailed")
        {
            return isChinese ? SocketListenFailedStr : en_SocketListenFailedStr;
        }
        else if(name == "SocketBindingFailed")
        {
            return isChinese ? SocketBindingFailedStr : en_SocketBindingFailedStr;
        }
        else if(name == "BluetoothListenFailed")
        {
            return isChinese ? BluetoothListenFailedStr : en_BluetoothListenFailedStr;
        }
        else if(name == "BluetoothBindingFailed")
        {
            return isChinese ? BluetoothBindingFailedStr : en_BluetoothBindingFailedStr;
        }
        else if(name == "UnabletoConnectDevice")
        {
            return isChinese ? UnabletoConnectDeviceStr : en_UnabletoConnectDeviceStr;
        }
        else if(name == "DeviceFailure")
        {
            return isChinese ? DeviceFailureStr : en_DeviceFailureStr;
        }
        else if(name == "RetryingConnectDevice")
        {
            return isChinese ? RetryingConnectDeviceStr : en_RetryingConnectDeviceStr;
        }
        else if(name == "SocketBindingFailed2")
        {
            return isChinese ? SocketBindingFailedStr2 : en_SocketBindingFailedStr2;
        }
        else if(name == "NetSendInfo")
        {
            return isChinese ? NetSendInfoStr : en_NetSendInfoStr;
        }
        else if(name == "NetReceivedInfo")
        {
            return isChinese ? NetReceivedInfoStr : en_NetReceivedInfoStr;
        }
        else if(name == "NetStateChanged")
        {
            return isChinese ? NetStateChangedStr : en_NetStateChangedStr;
        }
        else if(name == "CreateTcpChannelInfo")
        {
            return isChinese ? CreateTcpChannelInfoStr : en_CreateTcpChannelInfoStr;
        }
        else if(name == "CreateTcpServerChannelInfo")
        {
            return isChinese ? CreateTcpServerChannelInfoStr : en_CreateTcpServerChannelInfoStr;
        }
        else if(name == "FailedtoSendInstruction")
        {
            return isChinese ? FailedtoSendInstructionStr : en_FailedtoSendInstructionStr;
        }
        else if(name == "CommunicationException")
        {
            return isChinese ? CommunicationExceptionStr : en_CommunicationExceptionStr;
        }
        return "";
    }
}
