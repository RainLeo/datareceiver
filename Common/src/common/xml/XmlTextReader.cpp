#include "XmlTextReader.h"
#include "../exception/Exception.h"
#include "../system/Convert.h"
#include "../IO/Path.h"
#include "../data/PrimitiveType.h"

namespace Common
{
	XmlTextReader::XmlTextReader(const char *filename)
	{
        clear();
		/*
		* this initialize the library and check potential ABI mismatches
		* between the version it was compiled for and the actual shared
		* library used.
		*/
		LIBXML_TEST_VERSION

        _reader = xmlReaderForFile(filename, NULL, 0);
        
        if(_reader != NULL)
        {
            string rootPath = Path::getDirectoryName(filename);
            string fileName = Path::getFileName(filename);
            _configFile.rootPath = rootPath;
            _configFile.fileName = fileName;
        }
        _zipFile = NULL;
	}
    XmlTextReader::XmlTextReader(Zip* zip, const char *filename)
    {
        if(zip == NULL)
            throw ArgumentException("zip");
        if(filename == NULL)
            throw ArgumentException("filename");
        
        clear();
        /*
         * this initialize the library and check potential ABI mismatches
         * between the version it was compiled for and the actual shared
         * library used.
         */
        LIBXML_TEST_VERSION
        
        _deleteZip = false;
        _configFile.zip = zip;
#ifdef WIN32
        // fixbug: '/' must be used instead of '\' in windows.
        _configFile.fileName = String::replace(filename, "\\", "/");
#else
        _configFile.fileName = filename;
#endif
        _zipFile = zip->open(_configFile.fileName);
        _reader = xmlReaderForIO(zipRead, NULL, _zipFile, NULL, NULL, 0);
    }
    XmlTextReader::XmlTextReader(Zip* zip, const string& filename) : XmlTextReader(zip, filename.c_str())
    {
    }
    XmlTextReader::XmlTextReader(const char* zipFilename, const char *filename)
    {
        if(zipFilename == NULL)
            throw ArgumentException("zipFilename");
        if(filename == NULL)
            throw ArgumentException("filename");
        
        clear();
        /*
         * this initialize the library and check potential ABI mismatches
         * between the version it was compiled for and the actual shared
         * library used.
         */
        LIBXML_TEST_VERSION
        
        Zip* zip = new Zip(zipFilename);
        if(zip->isValid())
        {
            _deleteZip = true;
            _configFile.zip = zip;
#ifdef WIN32
            // fixbug: '/' must be used instead of '\' in windows.
            _configFile.fileName = String::replace(filename, "\\", "/");
#else
            _configFile.fileName = filename;
#endif
            _zipFile = zip->open(_configFile.fileName);
            _reader = xmlReaderForIO(zipRead, NULL, _zipFile, NULL, NULL, 0);
        }
        else
        {
            delete zip;
        }
    }
    XmlTextReader::XmlTextReader(const string& filename) : XmlTextReader(filename.c_str())
    {
    }

	XmlTextReader::~XmlTextReader()
	{
		if (isValid())
		{
			xmlFreeTextReader(_reader);
		}

		/*
		* Cleanup function for the XML library.
		*/
		xmlCleanupParser();
		/*
		* this is to debug memory for regression tests
		*/
		xmlMemoryDump();
        
        if(_configFile.isZip() && _zipFile != NULL)
        {
            _configFile.zip->close(_zipFile);
            if(_deleteZip)
            {
                delete _configFile.zip;
            }
            _configFile.zip = NULL;
            _zipFile = NULL;
        }
	}

	bool XmlTextReader::isValid() const
	{
		return _reader != NULL && xmlTextReaderIsValid(_reader) != XmlFailed;
	}

	void XmlTextReader::clear()
	{
		_name = "";
		_localName = "";
		_namespaceUri = "";
		_value = "";
		_nodeType = NodeNone;
	}

	bool XmlTextReader::read()
	{
		if (isValid())
		{
			clear();
			return xmlTextReaderRead(_reader) == XmlSuccess;
			//int ret = xmlTextReaderRead(_reader);
			//while (ret == 1)
			//{
			//	const xmlChar *name, *value;

			//	name = xmlTextReaderConstName(_reader);
			//	value = xmlTextReaderConstValue(_reader);
			//	ret = xmlTextReaderRead(_reader);
			//}
			///*
			//* Once the document has been fully parsed check the validation results
			//*/
			//if (xmlTextReaderIsValid(_reader) != 1) {
			//	fprintf(stderr, "Document %s does not validate\n", filename);
			//}
			//xmlFreeTextReader(_reader);
			//if (ret != 0) {
			//	fprintf(stderr, "%s : failed to parse\n", filename);
			//}
		}
		return false;
	}

	XmlNodeType XmlTextReader::nodeType()
	{
		if (_nodeType == NodeNone)
		{
			if (isValid())
			{
				_nodeType = (XmlNodeType)xmlTextReaderNodeType(_reader);
			}
		}
		return _nodeType;
	}

	string XmlTextReader::localName()
	{
		if (_localName.empty())
		{
			if (isValid())
			{
				const xmlChar* result = xmlTextReaderConstLocalName(_reader);
				_localName = result != NULL ? Convert::UTF8toGBKStr((char*)result) : "";
			}
		}
		return _localName;
	}

	string XmlTextReader::namespaceUri()
	{
		if (_namespaceUri.empty())
		{
			if (isValid())
			{
				const xmlChar* result = xmlTextReaderConstNamespaceUri(_reader);
				_namespaceUri = result != NULL ? Convert::UTF8toGBKStr((char*)result) : "";
			}
		}
		return _namespaceUri;
	}

	string XmlTextReader::name()
	{
		if (_name.empty())
		{
			if (isValid())
			{
				const xmlChar* result = xmlTextReaderConstName(_reader);
				_name = result != NULL ? Convert::UTF8toGBKStr((char*)result) : "";
			}
		}
		return _name;
	}

	string XmlTextReader::value()
	{
		if (_value.empty())
		{
			if (isValid())
			{
				const xmlChar* result = xmlTextReaderConstValue(_reader);
				_value = result != NULL ? Convert::UTF8toGBKStr((char*)result) : "";
			}
		}
		return _value;
	}
    bool XmlTextReader::isEmptyElement() const
    {
        if (isValid())
        {
            return xmlTextReaderIsEmptyElement(_reader) == 1;
        }
        return false;
    }

	XmlNodeType XmlTextReader::moveToContent()
	{
		if (isValid())
		{
			XmlNodeType type;

		LNext:
			type = nodeType();
			switch (type)
			{
			case Element:
			case Text:
			case CDATA:
			case EntityRef:
			case EntityDeclaration:
			case EndElement:
			case EndEntity:
				break;

			case Attribute:
				moveToElement();
				break;

			default:
				if (read())
				{
					goto LNext;
				}
				break;
			}
			return type;
		}
		return XmlNodeType::NodeNone;
	}

	bool XmlTextReader::moveToElement()
	{
		if (isValid())
		{
			clear();
			return xmlTextReaderMoveToElement(_reader) == XmlSuccess;
		}
		return false;
	}

	string XmlTextReader::getAttribute(const string& name)
	{
		if (isValid())
		{
			clear();
			xmlChar* result = xmlTextReaderGetAttribute(_reader, (const xmlChar*)name.c_str());
            if (result == NULL) return "";
            string attribute = (char*)result;//Convert::UTF8toGBKStr((char*)result);
            xmlFree(result); // Note: added by HYC to free memory by malloc in xmlTextReaderGetAttribute
            return attribute;
		}
		return "";
	}
    
    const ConfigFile& XmlTextReader::configFile() const
    {
        return _configFile;
    }
}
