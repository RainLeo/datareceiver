//
//  Configuration.cpp
//  common
//
//  Created by baowei on 15/3/31.
//  Copyright (c) 2015 com. All rights reserved.
//

#include "Configuration.h"
#include <assert.h>
#include "common/exception/Exception.h"
#include "common/IO/File.h"
#include "common/IO/Path.h"
#include "common/IO/Directory.h"

namespace Common
{
    Configuration::Configuration(const ConfigFile& file)
    {
        if (file.isEmpty())
        {
            throw new ArgumentNullException("file");
        }
        _file = file;
    }
    Configuration::~Configuration()
    {
    }
    
    bool Configuration::load(void* sender)
    {
        if(_file.isEmpty())
            return false;
        
        if(_file.isZip())
        {
            XmlTextReader reader(_file.zip, _file.fileName);
            if (reader.isValid())
            {
                reader.moveToContent();
                if(sender != nullptr)
                {
                    return load(reader, sender);
                }
                else
                {
                    return load(reader);
                }
            }
        }
        else
        {
            string fullFileName = Path::combine(_file.rootPath, _file.fileName);
            if (File::exists(fullFileName))
            {
                XmlTextReader reader(fullFileName);
                if (reader.isValid())
                {
                    reader.moveToContent();
                    if(sender != nullptr)
                    {
                        return load(reader, sender);
                    }
                    else
                    {
                        return load(reader);
                    }
                }
            }
        }
        return false;
    }
    
    bool Configuration::save(void* sender)
    {
        if(_file.isEmpty())
            return false;
        
        if(_file.isZip())
        {
            // todo: save file in zip.
        }
        else
        {
            // save a temp file at first.
            if(!Directory::exists(_file.rootPath))
            {
                Directory::createDirectory(_file.rootPath);
            }
            string tempFileName = Path::combine(_file.rootPath, (string)"~" + _file.fileName);
            XmlTextWriter writer(tempFileName);
            if (writer.isValid())
            {
                writer.enableIndent();
                writer.writeStartDocument();
                
                if(sender != nullptr)
                {
                    if(!save(writer, sender))
                        return false;
                }
                else
                {
                    if(!save(writer))
                        return false;
                }
                
                writer.writeEndDocument();
            }
            writer.close();
            
            // copy temp file to real file
            if (File::exists(tempFileName))
            {
                string fullFileName = Path::combine(_file.rootPath, _file.fileName);
                return File::move(tempFileName, fullFileName);
            }
        }
        return false;
    }
    
    bool Configuration::load(XmlTextReader& reader)
    {
        return false;
    }
    bool Configuration::load(XmlTextReader& reader, void* sender)
    {
        return false;
    }
    bool Configuration::save(XmlTextWriter& writer)
    {
        return false;
    }
    bool Configuration::save(XmlTextWriter& writer, void* sender)
    {
        return false;
    }
}
