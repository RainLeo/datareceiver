#include "XmlDocument.h"
#include "../exception/Exception.h"

namespace Common
{
	XmlDocument::XmlDocument(const string& fileName)
	{
		_xmlDoc = NULL;
        
        if(!fileName.empty())
        {
            load(fileName);
        }
	}

	XmlDocument::~XmlDocument()
	{
	}

	void XmlDocument::load(const char* fileName)
	{
		if (fileName == NULL)
			throw ArgumentNullException("fileName");

		_xmlDoc = xmlParseFile(fileName);
	}
	void XmlDocument::load(const string& fileName)
	{
		load(fileName.c_str());
	}

	void XmlDocument::save(const char* fileName)
	{
		if (fileName == NULL)
			throw ArgumentNullException("fileName");

		if (_xmlDoc != NULL)
		{
			//Save the document back out to disk.
			xmlSaveFileEnc(fileName, _xmlDoc, (const char*)_xmlDoc->encoding);
		}
	}
	void XmlDocument::save(const string& fileName)
	{
		save(fileName.c_str());
	}

	bool XmlDocument::documentElement(XmlNode& node) const
	{
		if (_xmlDoc != NULL)
		{
			node.setXmlNodeInner(_xmlDoc->children);
		}
		return NULL;
	}
}
