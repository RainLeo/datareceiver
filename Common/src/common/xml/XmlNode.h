#ifndef XMLNODE_H
#define XMLNODE_H

#include <stdio.h>
#include <string>
#include "common/common_global.h"
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include "XmlAttribute.h"

using namespace std;

namespace Common
{
	class COMMON_EXPORT XmlNode
	{
	public:
		XmlNode();
		~XmlNode();

		void selectSingleNode(const char* xpath, XmlNode& node);

		inline bool isValid() const
		{
			return _xmlNode != NULL;
		}
		inline const XmlAttributes& attributes() const
		{
			return _attributes;
		}

	private:
		void setXmlNodeInner(const xmlNodePtr node);

	private:
		friend class XmlDocument;

		xmlNodePtr _xmlNode;

		XmlAttributes _attributes;
	};
}

#endif	// XMLNODE_H
