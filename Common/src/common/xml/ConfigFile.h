#ifndef CONFIGFILE_H
#define CONFIGFILE_H

#include <string>
#include "common/common_global.h"
#include "common/IO/Zip.h"

using namespace std;

namespace Common
{
    struct ConfigSource
    {
        string rootPath;
        Zip* zip;
        
        ConfigSource() : ConfigSource(NULL, "")
        {
        }
        ConfigSource(Zip* zip) : ConfigSource(zip, "")
        {
        }
        ConfigSource(const string& pathName) : ConfigSource(NULL, pathName)
        {
        }
        ConfigSource(const ConfigSource& cs)
        {
            this->operator=(cs);
        }
        
        inline bool isZip() const
        {
            return this->zip != NULL;
        }
        inline bool isEmpty() const
        {
            return this->zip == NULL && this->rootPath.empty();
        }
        
        void operator=(const ConfigSource& value)
        {
            this->rootPath = value.rootPath;
            this->zip = value.zip;
        }
        bool operator==(const ConfigSource& value) const
        {
            return this->rootPath == value.rootPath &&
                this->zip == value.zip;
        }
        bool operator!=(const ConfigSource& value) const
        {
            return !operator==(value);
        }
        
    protected:
        ConfigSource(Zip* zip, const string& rootPath)
        {
            this->zip = zip;
            this->rootPath = rootPath;
        }
    };
    struct ConfigFile : public ConfigSource
    {
        string fileName;
        
        ConfigFile() : ConfigFile(NULL, "", "")
        {
        }
        ConfigFile(Zip* zip) : ConfigFile(zip, "", "")
        {
        }
        ConfigFile(Zip* zip, const string& fileName) : ConfigFile(zip, "", fileName)
        {
        }
        ConfigFile(const string& pathName, const string& fileName) : ConfigFile(NULL, pathName, fileName)
        {
        }
        ConfigFile(const ConfigFile& cf)
        {
            this->operator=(cf);
        }
        
        void operator=(const ConfigSource& value)
        {
            this->rootPath = value.rootPath;
            this->zip = value.zip;
        }
        void operator=(const ConfigFile& value)
        {
            this->rootPath = value.rootPath;
            this->zip = value.zip;
            this->fileName = value.fileName;
        }
        bool operator==(const ConfigFile& value) const
        {
            return this->rootPath == value.rootPath &&
                this->zip == value.zip &&
                this->fileName == value.fileName;
        }
        bool operator!=(const ConfigFile& value) const
        {
            return !operator==(value);
        }
        
    private:
        ConfigFile(Zip* zip, const string& rootPath, const string& fileName) : ConfigSource(zip, rootPath)
        {
            this->fileName = fileName;
        }
    };
}

#endif	// CONFIGFILE_H
