#ifndef XMLATTRIBUTE_H
#define XMLATTRIBUTE_H

#include <stdio.h>
#include <string>
#include "common/common_global.h"
#include "common/data/Vector.h"
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>

using namespace std;

namespace Common
{
	class COMMON_EXPORT XmlAttribute
	{
	public:
		XmlAttribute();
		~XmlAttribute();

		inline bool isValid() const
		{
			return _xmlNode != NULL && _xmlAttr != NULL;
		}

		string name() const;
		string value() const;
		void setValue(const char* value);
        void setValue(const string& value);

	private:
		void setXmlAttributeInner(const xmlNodePtr node, const xmlAttrPtr attr);

	private:
		friend class XmlNode;

		xmlNodePtr _xmlNode;
		xmlAttrPtr _xmlAttr;
	};
	//typedef Vector<XmlAttribute> XmlAttributes;
	class XmlAttributes : public Vector<XmlAttribute>
	{
	public:
		XmlAttributes(bool autoDelete = true, uint capacity = Vector<XmlAttribute>::DefaultCapacity) : Vector<XmlAttribute>(autoDelete, capacity)
		{
		}

		inline XmlAttribute* at(const char* name) const
		{
			for (uint i = 0; i < count(); i++)
			{
				XmlAttribute* attr = Vector<XmlAttribute>::at(i);
				if (attr->name() == name)
				{
					return attr;
				}
			}
			return NULL;
		}
		inline XmlAttribute* operator[](const char* name) const
		{
			return this->at(name);
		}
	};
}

#endif	// XMLATTRIBUTE_H
