#include "XmlNode.h"
#include "../exception/Exception.h"

namespace Common
{
	XmlNode::XmlNode()
	{
		_xmlNode = NULL;
	}

	XmlNode::~XmlNode()
	{
	}

	void XmlNode::setXmlNodeInner(const xmlNodePtr node)
	{
		_xmlNode = node;

		_attributes.clear();
		xmlAttrPtr attrPtr = _xmlNode->properties;
		while (attrPtr != NULL)
		{
			XmlAttribute* attr = new XmlAttribute();
			attr->setXmlAttributeInner(_xmlNode, attrPtr);
			_attributes.add(attr);

			attrPtr = attrPtr->next;
		};
	}

	void XmlNode::selectSingleNode(const char* xpath, XmlNode& node)
	{
		if (_xmlNode != NULL)
		{
			xmlNodePtr nodePtr = _xmlNode->children;
			while (nodePtr != NULL)
			{
				if (nodePtr->type == XML_ELEMENT_NODE &&
					strcmp((const char*)nodePtr->name, xpath) == 0)
				{
					node.setXmlNodeInner(nodePtr);
					break;
				}
				nodePtr = nodePtr->next;
			};
		}
	}
}
