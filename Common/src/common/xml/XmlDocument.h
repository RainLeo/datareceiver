#ifndef XMLDOCUMENT_H
#define XMLDOCUMENT_H

#include <stdio.h>
#include <string>
#include "common/common_global.h"
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include "XmlNode.h"

using namespace std;

namespace Common
{
	class COMMON_EXPORT XmlDocument
	{
	public:
		XmlDocument(const string& fileName = "");
		~XmlDocument();

		void load(const char* fileName);
		void load(const string& fileName);
		void save(const char* fileName);
		void save(const string& fileName);

		bool documentElement(XmlNode& node) const;

		inline bool isValid() const
		{
			return _xmlDoc != NULL;
		}

	private:
		xmlDocPtr _xmlDoc;
	};
}

#endif	// XMLDOCUMENT_H
