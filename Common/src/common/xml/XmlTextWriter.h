#ifndef XMLTEXTWRITER_H
#define XMLTEXTWRITER_H

#include <stdio.h>
#include <string>
#include "common/common_global.h"
#include "libxml/xmlwriter.h"

using namespace std;

namespace Common
{
	class COMMON_EXPORT XmlTextWriter
	{
	public:
		XmlTextWriter(const char *filename);
        XmlTextWriter(const string& filename);
		~XmlTextWriter();

		bool isValid() const;

		bool close();
        
        void enableIndent(bool indent = true);
        
        void writeStartDocument();
        void writeStartDocument(const string& version, const string& encoding, const string& standalone);
        void writeEndDocument();
        
        void writeStartElement(const string& localName);
        void writeStartElement(const string& prefix, const string& localName, const string& ns);
        void writeEndElement();
        
        void writeAttributeString(const string& localName, const string& value);

	private:
		xmlTextWriterPtr _writer;
	};
}

#endif	// XMLTEXTWRITER_H
