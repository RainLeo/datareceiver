#include "XmlAttribute.h"
#include "../exception/Exception.h"

namespace Common
{
	XmlAttribute::XmlAttribute()
	{
		_xmlNode = NULL;
		_xmlAttr = NULL;
	}

	XmlAttribute::~XmlAttribute()
	{
	}

	string XmlAttribute::name() const
	{
		string result = "";
		if (isValid())
		{
			result = (const char*)_xmlAttr->name;
		}
		return result;
	}

	string XmlAttribute::value() const
	{
		string result = "";
		if (isValid())
		{
			result = (const char*)xmlGetProp(_xmlNode, (const xmlChar*)name().c_str());
		}
		return result;
	}
	void XmlAttribute::setValue(const char* value)
	{
		if (isValid())
		{
			xmlSetProp(_xmlNode, (const xmlChar*)name().c_str(), (const xmlChar*)value);
		}
	}
    void XmlAttribute::setValue(const string& value)
    {
        setValue(value.c_str());
    }

	void XmlAttribute::setXmlAttributeInner(const xmlNodePtr node, const xmlAttrPtr attr)
	{
		_xmlNode = node;
		_xmlAttr = attr;
	}
}
