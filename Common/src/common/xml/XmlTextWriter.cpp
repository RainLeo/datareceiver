#include "XmlTextWriter.h"
#include "../data/PrimitiveType.h"

namespace Common
{
	XmlTextWriter::XmlTextWriter(const char *filename)
	{
		/*
		* this initialize the library and check potential ABI mismatches
		* between the version it was compiled for and the actual shared
		* library used.
		*/
		LIBXML_TEST_VERSION

        _writer = xmlNewTextWriterFilename(filename, 0);
	}
    XmlTextWriter::XmlTextWriter(const string& filename) : XmlTextWriter(filename.c_str())
    {
    }

	XmlTextWriter::~XmlTextWriter()
	{
        close();

		/*
		* Cleanup function for the XML library.
		*/
		xmlCleanupParser();
		/*
		* this is to debug memory for regression tests
		*/
		xmlMemoryDump();
	}

	bool XmlTextWriter::isValid() const
	{
        return _writer != NULL;
    }

	bool XmlTextWriter::close()
	{
        if (isValid())
        {
            xmlTextWriterFlush(_writer);
            xmlFreeTextWriter(_writer);
            _writer = NULL;
            return true;
        }
        return false;
	}
    
    void XmlTextWriter::enableIndent(bool indent)
    {
        if (isValid())
        {
            xmlTextWriterSetIndent(_writer, indent ? 1 : 0);
        }
    }
    
    void XmlTextWriter::writeStartDocument()
    {
        writeStartDocument("1.0", "utf-8", "");
    }
    void XmlTextWriter::writeStartDocument(const string& version, const string& encoding, const string& standalone)
    {
        if (isValid())
        {
            xmlTextWriterStartDocument(_writer,
                                       version.empty() ? NULL : version.c_str(),
                                       encoding.empty() ? NULL : encoding.c_str(),
                                       standalone.empty() ? NULL : standalone.c_str());
        }
    }
    void XmlTextWriter::writeEndDocument()
    {
        if (isValid())
        {
            xmlTextWriterEndDocument(_writer);
        }
    }
    
    void XmlTextWriter::writeStartElement(const string& localName)
    {
        if (isValid())
        {
            xmlTextWriterStartElement(_writer, (const byte*)(localName).c_str());
        }
    }
    void XmlTextWriter::writeStartElement(const string& prefix, const string& localName, const string& ns)
    {
        if (isValid())
        {
            xmlTextWriterStartElementNS(_writer,
                                        (const byte*)(prefix.empty() ? NULL : (prefix).c_str()),
                                        (const byte*)((localName).c_str()),
                                        (const byte*)(ns.empty() ? NULL : (ns).c_str()));
        }
    }
    void XmlTextWriter::writeEndElement()
    {
        if (isValid())
        {
            xmlTextWriterEndElement(_writer);
        }
    }
    
    void XmlTextWriter::writeAttributeString(const string& localName, const string& value)
    {
        if (isValid())
        {
            xmlTextWriterWriteAttribute(_writer, (const byte*)(localName).c_str(), (const byte*)(value).c_str());
        }
    }
}
