#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include "common/common_global.h"
#include "common/xml/XmlTextReader.h"
#include "common/xml/XmlTextWriter.h"
#include "ConfigFile.h"

namespace Common
{
    class COMMON_EXPORT Configuration
    {
    public:
        Configuration(const ConfigFile& file);
        virtual ~Configuration();
        
        bool load(void* sender = nullptr);
        bool save(void* sender = nullptr);
        
    protected:
        virtual bool load(XmlTextReader& reader);
        virtual bool load(XmlTextReader& reader, void* sender);
        virtual bool save(XmlTextWriter& writer);
        virtual bool save(XmlTextWriter& writer, void* sender);
        
    protected:
        ConfigFile _file;
    };
}

#endif	// CONFIGURATION_H
