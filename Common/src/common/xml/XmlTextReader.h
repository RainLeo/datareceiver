#ifndef XMLTEXTREADER_H
#define XMLTEXTREADER_H

#include <stdio.h>
#include <string>
#include "common/common_global.h"
#include "../IO/Zip.h"
#include "libxml/xmlreader.h"
#include "ConfigFile.h"

using namespace std;

namespace Common
{
	enum XmlNodeType
	{
		NodeNone = 0,
		Element = 1,
		Attribute = 2,
		Text = 3,
		CDATA = 4,
		EntityRef = 5,
		EntityDeclaration = 6,
		ProcessingInstruction = 7,
		Comments = 8,
		Document = 9,
		DocumentType = 10,
		DocumentFragment = 11,
		Notation = 12,
		Whitespace = 13,
		SignificantWhitespace = 14,
		EndElement = 15,
		EndEntity = 16,
		mlDeclaration = 17
	};

	class COMMON_EXPORT XmlTextReader
	{
	public:
		XmlTextReader(const char *filename);
        XmlTextReader(const string& filename);
        XmlTextReader(Zip* zip, const char *filename);
        XmlTextReader(Zip* zip, const string& filename);
        XmlTextReader(const char* zipFilename, const char *filename);
		~XmlTextReader();

		bool isValid() const;

		void clear();

		bool read();

		string name();
		string localName();
		string namespaceUri();
		XmlNodeType nodeType();
		string value();
        bool isEmptyElement() const;

		XmlNodeType moveToContent();
		bool moveToElement();

		string getAttribute(const string& name);
        
        const ConfigFile& configFile() const;

	private:
		xmlTextReaderPtr _reader;

		string _name;
		string _localName;
		string _namespaceUri;
		XmlNodeType _nodeType;
		string _value;
        
        bool _deleteZip;
        zip_file* _zipFile;
        ConfigFile _configFile;

		const int XmlSuccess = 1;
		const int XmlFailed = -1;
	};
}

#endif	// XMLTEXTREADER_H
