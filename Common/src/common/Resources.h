﻿#ifndef RESOURCES_H
#define RESOURCES_H

#include "common/common_global.h"

namespace Common
{
	class COMMON_EXPORT Resources
	{
	public:
        // Chinese string
		const static char* EmptyStr;						// ""
		const static char* NullStr;							// "-"
		const static char* ErrorStr;						// "错误"
		const static char* WarningStr;						// "警告"
		const static char* OkStr;							// "确定"
		const static char* CancelStr;						// "取消"
		const static char* YesStr;							// "是"
		const static char* NoStr;							// "否"
		const static char* PromptStr;						// "提示"

		const static char* RemoveLogFilesSuccessfullyStr;	// "移除日志文件'%s'成功！"
		const static char* FailedtoRemoveLogFilesStr;		// "移除日志文件'%s'失败！"

		const static char* SocketListenFailedStr;			// "监听IP地址'%s'，端口'%d'失败！端口可能被占用。"
		const static char* SocketBindingFailedStr;			//  "绑定IP地址'%s'，端口'%d'失败！端口可能被占用。"
		const static char* BluetoothListenFailedStr;		// "监听蓝牙失败！"
		const static char* BluetoothBindingFailedStr;		//  "绑定蓝牙失败！"

		const static char* UnabletoConnectDeviceStr;		// "无法连接设备, 设备名称: %s"
		const static char* DeviceFailureStr;				// "设备失效, 设备名称: %s"
		const static char* RetryingConnectDeviceStr;		// "重新尝试连接设备, 设备名称: %s"

		const static char* SocketBindingFailedStr2;			//  "绑定 UDP 端口'%d'失败！端口可能被占用。"
        
        const static char* NetSendInfoStr;					// "send: %s"
        const static char* NetReceivedInfoStr;				// "recv: %s"
        
        const static char* NetStateChangedStr;				// "服务端'%s'状态从'%s'变为'%s'."
        
        const static char* CreateTcpChannelInfoStr;			// "创建以太网通道: 名称: %s, IP 地址: %s, 端口: %d"
        const static char* CreateTcpServerChannelInfoStr;	// "创建以太网服务通道: 名称: %s, IP 地址: %s, 端口: %d"
        
        const static char* FailedtoSendInstructionStr;		// "发送网络指令失败，名称: '%s', 异常: %s"
        const static char* CommunicationExceptionStr;		// "通讯异常"
        
        // English string
        const static char* en_EmptyStr;                     // "";
        const static char* en_NullStr;                      // "-";
        const static char* en_ErrorStr;                     // "Error";
        const static char* en_WarningStr;                   // "Warning";
        const static char* en_OkStr;                        // "Ok";
        const static char* en_CancelStr;                    // "Cancel";
        const static char* en_YesStr;                       // "Yes";
        const static char* en_NoStr;                        // "No";
        const static char* en_PromptStr;                    // "Prompt";
        
        const static char* en_RemoveLogFilesSuccessfullyStr;// "Remove log file'%s' successfully!";
        const static char* en_FailedtoRemoveLogFilesStr;    // "Failed to remove log file'%s'!";
        
        const static char* en_SocketListenFailedStr;        // "Failed to listen IP address'%s', port'%d'! The port may be used.";
        const static char* en_SocketBindingFailedStr;       //  "Failed to bind IP address'%s'，port'%d'! The port may be used.";
        const static char* en_BluetoothListenFailedStr;     // "Failed to listen bluetooth!";
        const static char* en_BluetoothBindingFailedStr;    // "Failed to bind bluetooth!";
        
        const static char* en_UnabletoConnectDeviceStr;     // "Unable to connect the device, name: %s";
        const static char* en_DeviceFailureStr;             // "The device is failure, name: %s";
        const static char* en_RetryingConnectDeviceStr;     // "Retry to connect the device, name: %s";
        
        const static char* en_SocketBindingFailedStr2;      //  "Failed to bind UDP, port'%d'! The port may be used.";
        
        const static char* en_NetSendInfoStr;               // "send: %s";
        const static char* en_NetReceivedInfoStr;           // "recv: %s";
        
        const static char* en_NetStateChangedStr;           // "The server'%s' state changed, from'%s'to'%s'.";
        
        const static char* en_CreateTcpChannelInfoStr;      // "Create tcp channel: name: %s, IP address: %s, port: %d";
        const static char* en_CreateTcpServerChannelInfoStr;// "Create tcp server channel: name: %s, IP address: %s, port: %d";
        
        const static char* en_FailedtoSendInstructionStr;   // "Failed to send the instruction, name: '%s', exception: %s";
        const static char* en_CommunicationExceptionStr;    // "Communication exception";
        
    public:
        static string getString(const string& name);
        
    private:
        static string getStaticString(const string& name);
	};
}

#endif // RESOURCES_H
