#ifndef TRACE_H
#define TRACE_H

#include <stdarg.h>
#include "common/common_global.h"
#include "common/data/PrimitiveType.h"

namespace Common
{
	class LogTraceListener;
	class COMMON_EXPORT Trace
	{
	public:
		static void writeFormat(const char* format, ...);
		static void writeFormatLine(const char* format, ...);

        static void write(const String& message, const String& category = String::Empty, bool showTime = true)
		{
			writeInner(message.c_str(), false, category.c_str(), showTime);
		}

		static void writeLine(const String& message, const String& category = String::Empty, bool showTime = true)
		{
			writeInner(message.c_str(), true, category.c_str(), showTime);
		}

		static void enableLog(LogTraceListener* tl = NULL);
		static void disableLog();

		static void enableConsoleOutput();
		static void disableConsoleOutput();

	private:
		static void writeInner(const char* message, bool newLine = false, const char* category = NULL, bool showTime = true);

	public:
		const static char* System;			// "System"
		const static char* Information;		// "Information"
		const static char* Info;			// "Info"
		const static char* Error;			// "Error"
		const static char* Warning;			// "Warning"
		const static char* Verbose;			// "Verbose"
		const static char* Debug;			// "Debug"
		const static char* Fatal;			// "Fatal"

		const static int MaxMessageLength = 1024 * 2;	// 2 K

	private:
		friend class Debug;

		static bool _enableLog;
		static LogTraceListener* _logListener;
		static bool _ownerListener;
		static bool _enableConsoleOutput;
	};
}

#endif // TRACE_H
