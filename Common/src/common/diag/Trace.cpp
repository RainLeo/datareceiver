#include "LogTraceListener.h"
#if WIN32
#include <Windows.h>
#elif __ANDROID__
#include <android/log.h>
#else
#endif
#include "../system/Convert.h"
#include "Trace.h"

namespace Common
{
	const char* Trace::System = "System";
	const char* Trace::Information = "Information";
	const char* Trace::Info = "Info";
	const char* Trace::Error = "Error";
	const char* Trace::Warning = "Warning";
	const char* Trace::Verbose = "Verbose";
	const char* Trace::Debug = "Debug";
	const char* Trace::Fatal = "Fatal";

	bool Trace::_enableLog = false;
	LogTraceListener* Trace::_logListener = NULL;
	bool Trace::_ownerListener = false;
	bool Trace::_enableConsoleOutput = false;

	void Trace::writeFormat(const char* format, ...)
	{
		char* message = new char[MaxMessageLength];
		memset(message, 0, MaxMessageLength);
		va_list ap;
		va_start(ap, format);
		vsprintf(message, format, ap);
		va_end(ap);

		writeInner(message, false);
		delete[] message;
	}

	void Trace::writeFormatLine(const char* format, ...)
	{
		char* message = new char[MaxMessageLength];
		memset(message, 0, MaxMessageLength);
		va_list ap;
		va_start(ap, format);
		vsprintf(message, format, ap);
		va_end(ap);

		writeInner(message, true);
		delete[] message;
	}

	void Trace::writeInner(const char* message, bool newLine, const char* category, bool showTime)
	{
		string str = "";
		if (showTime)
		{
			str.append(Convert::getCurrentTimeStr(true));
			str.append(" ");
		}
		if (category != NULL && strlen(category) > 0)
		{
			str.append(category);
			str.append(": ");
		}
		str.append(message);
		if (newLine)
		{
			str.append(Convert::NewLine);
		}

		const char* dstr = str.c_str();
#if WIN32
		OutputDebugString(dstr);
		if(_enableConsoleOutput)
		{
			printf("%s", dstr);
		}
#elif __ANDROID__
		int prio = ANDROID_LOG_DEFAULT;
		if (category != NULL)
		{
			if(String::stringEquals(Trace::System, category))
			{
				prio = ANDROID_LOG_INFO;
			}
			else if(String::stringEquals(Trace::Error, category))
			{
				prio = ANDROID_LOG_ERROR;
			}
			else if(String::stringEquals(Trace::Information, category) ||
					String::stringEquals(Trace::Info, category))
			{
				prio = ANDROID_LOG_INFO;
			}
			else if(String::stringEquals(Trace::Verbose, category))
			{
				prio = ANDROID_LOG_VERBOSE;
			}
			else if(String::stringEquals(Trace::Debug, category))
			{
				prio = ANDROID_LOG_DEBUG;
			}
			else if(String::stringEquals(Trace::Warning, category))
			{
				prio = ANDROID_LOG_WARN;
			}
			else if(String::stringEquals(Trace::Fatal, category))
			{
				prio = ANDROID_LOG_FATAL;
			}
		}
		__android_log_print(prio, NULL, message);
#else
		printf("%s", dstr);
#endif
		// Log to files.
		if (_enableLog)
		{
			if (_logListener != NULL)
			{
				_logListener->writeLine(dstr, category);
			}
		}
	}

	void Trace::enableLog(LogTraceListener* tl)
	{
		_enableLog = true;
		if (tl != NULL)
		{
			_ownerListener = false;
			_logListener = tl;
		}
		else
		{
			_ownerListener = true;
			Singleton<LogTraceListener>::initialize();
			_logListener = Singleton<LogTraceListener>::instance();
		}
	}
	void Trace::disableLog()
	{
		_enableLog = false;
		if (_ownerListener)
		{
			Singleton<LogTraceListener>::unInitialize();
		}
		_logListener = NULL;
	}

	void Trace::enableConsoleOutput()
	{
		_enableConsoleOutput = true;
	}
	void Trace::disableConsoleOutput()
	{
		_enableConsoleOutput = false;
	}
}
