#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>
#include <fcntl.h>

#if WIN32
#include <Windows.h>
#include <memory.h>
#include <fcntl.h>
#include <io.h>
#include <direct.h>
#include <Shlwapi.h>
#pragma comment (lib, "Shlwapi.lib")
#elif __APPLE__
#include <sys/param.h>
#include <sys/mount.h>
#include <unistd.h>
#include <libgen.h>
#include <dirent.h>
#else
#include <sys/vfs.h>
#include <libgen.h>
#include <limits.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#endif

#include "LogTraceListener.h"
#include "Debug.h"
#include "../system/Convert.h"
#include "../IO/DiskChecker.h"
#include "../Resources.h"
#include "../IO/Path.h"
#include "../IO/Directory.h"

namespace Common
{
    LogTraceConfig::LogTraceConfig() : LogTraceConfig("logs")
    {
    }
    LogTraceConfig::LogTraceConfig(const LogTraceConfig& config)
    {
        this->operator=(config);
    }
    LogTraceConfig::LogTraceConfig(const string& path, const char* prefix, const char* suffix)
    {
        this->enabled = true;
        
        this->path = path;
        this->reservationDays = 30;       // 30 days
        this->diskFullSize = 10;          // 10 M
        this->prefix = prefix != NULL ? prefix : "";
        this->suffix = suffix != NULL ? suffix : "";
        this->extName = ".log";
        this->deleteUnuseFilesHour = 1;   // 1 AM
    }
    void LogTraceConfig::operator=(const LogTraceConfig& config)
    {
        this->enabled = config.enabled;
        
        this->path = config.path;
        this->reservationDays = config.reservationDays;
        this->diskFullSize = config.diskFullSize;
        this->prefix = config.prefix;
        this->suffix = config.suffix;
        this->extName = config.extName;
        this->deleteUnuseFilesHour = config.deleteUnuseFilesHour;
    }
    void LogTraceConfig::read(XmlTextReader& reader, const string& localName)
    {
        if (reader.nodeType() == XmlNodeType::Element &&
            reader.localName() == localName)
        {
            Convert::parseBoolean(reader.getAttribute("enabled"), enabled);
            string ph = reader.getAttribute("path");
            if(!ph.empty())
            {
                path = ph;
            }
            int rd;
            if(Convert::parseInt32(reader.getAttribute("reservationDays"), rd) &&
               rd >= 1 && rd <= 3650)
            {
                reservationDays = rd;
            }
            int dfs;
            if(Convert::parseInt32(reader.getAttribute("diskFullSize"), dfs) &&
               dfs >= 5 && dfs <= 100)
            {
                diskFullSize = dfs;
            }
            string p = reader.getAttribute("prefix");
            if(!p.empty())
            {
                prefix = p;
            }
            string s = reader.getAttribute("suffix");
            if(!s.empty())
            {
                suffix = s;
            }
            string e = reader.getAttribute("extName");
            if(!e.empty() && e[0] == '.')
            {
                extName = e;
            }
            int dufh;
            if(Convert::parseInt32(reader.getAttribute("deleteUnuseFilesHour"), dufh) &&
               dufh >= 0 && dufh <=23)
            {
                deleteUnuseFilesHour = dufh;
            }
        }
    }
    
    void processProc(void* parameter)
    {
        LogTraceListener* listener = (LogTraceListener*)parameter;
        assert(listener);
        listener->processProcInner();
    }
    void deleteUnuseFilesAction(void* parameter)
    {
        LogTraceListener* listener = (LogTraceListener*)parameter;
        assert(listener);
        listener->deleteUnuseFiles();
    }
    
    LogTraceListener::LogTraceListener(const LogTraceConfig& config)
    {
        _messageCount = 0;
        _diskIsFull = false;
        _fullFileName = "";
        _message = "";
        _processThread = nullptr;
        
        _config = config;
        
        if(!isRealPath(_config.path.c_str()))
        {
            string appPath = getAppPath();
#ifdef WIN32
            const char* fmt = "%s\\%s";
#else
            const char* fmt = "%s/%s";
#endif
            _config.path = Convert::convertStr(fmt, appPath.c_str(), _config.path.c_str());
        }
        
        if(_config.enabled)
        {
            createFile(_config.path.c_str());

            _processThread = new Thread();
            _processThread->setName("LogTraceListenerProc");
            uint interval = 60 * 1000;		// 60 seconds.
            _processThread->startProc(processProc, this, interval, deleteUnuseFilesAction, this);
        }
    }
    LogTraceListener::LogTraceListener(const char* logPath, const char* prefix, const char* suffix) : LogTraceListener(LogTraceConfig(logPath, prefix, suffix))
    {
    }
    LogTraceListener::LogTraceListener(const string& logPath, const char* prefix, const char* suffix) : LogTraceListener(logPath.c_str(), prefix, suffix)
    {
    }
    
    LogTraceListener::~LogTraceListener()
    {
        if(_processThread != NULL)
        {
            _processThread->stop();
            delete _processThread;
            _processThread = NULL;
        }
        
        if (_file != -1)
        {
            flushInner();
            close(_file);
            _file = -1;
        }
    }
    
    string LogTraceListener::getAppPath()
    {
        return Directory::getAppDirPath();
    }
    
    bool LogTraceListener::isRealPath(const char* path)
    {
        return Path::isPathRooted(path);
    }
    
    void LogTraceListener::writeLine(const char* message, const char* category)
    {
        if(!_config.enabled)
            return;
        
        Locker locker(&_messageMutex);
        
        if(message == NULL || strlen(message) == 0)
            return;
        
        if(_diskIsFull)
            return;
        
        _message.append(message);
        
        updateMessageCount(category);
    }
    
    void LogTraceListener::updateMessageCount(const char* category)
    {
        _messageCount++;
        
        if (category != NULL &&
            (strcmp(category, Trace::Error) == 0 ||
             strcmp(category, Trace::System) == 0))
        {
            flushInner(false);
        }
        else
        {
            if (_messageCount >= MaxMessageCount)
            {
                flushInner(false);
            }
        }
    }
    
    void LogTraceListener::flushInner(bool locked)
    {
        if (!fileOpened())
            return;
        
        if(locked)
        {
            _messageMutex.lock();
        }
        
        if (_messageCount > 0)
        {
            _messageCount = 0;
            
            size_t length = _message.length();
            write(_file, _message.c_str(), length);
            _message = "";
        }
        
        if(locked)
        {
            _messageMutex.unlock();
        }
    }
    
    bool LogTraceListener::isDiskFull()
    {
        const int MaxSize = _config.diskFullSize * 1024 * 1024;    // M
        return DiskChecker::isDiskFull(_config.path, MaxSize);
    }
    
    void LogTraceListener::processProcInner()
    {
        flushInner();
        
        time_t now = time(NULL);
        // Try to create new log file.
        createFile(now);
        
        // Try to delete some log files at 1 AM.
        deleteFiles(now);
        
        _diskIsFull = isDiskFull();
    }
    
    void LogTraceListener::deleteUnuseFiles()
    {
        deleteFiles(_config.reservationDays);
    }
    
    void LogTraceListener::deleteFiles(const time_t& timep)
    {
        struct tm* tmp = localtime(&timep);
        if (tmp->tm_hour == _config.deleteUnuseFilesHour && tmp->tm_min == 0)
        {
            deleteUnuseFiles();
        }
    }
    
    void LogTraceListener::deleteFiles(int days)
    {
        string filter = Convert::convertStr("*%s", _config.extName.c_str());
#if WIN32
        if (access(_config.path.c_str(), 0) != -1)	// file exists?
        {
            WIN32_FIND_DATA ffd;
            HANDLE hFind = INVALID_HANDLE_VALUE;
            // Find the first file in the directory.
            hFind = FindFirstFile((_config.path + "/" + filter).c_str(), &ffd);
            if (INVALID_HANDLE_VALUE != hFind)
            {
                // List all the files in the directory with some info about them.
                do
                {
                    if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
                    {
                    }
                    else
                    {
                        removeFile(_config.path, ffd.cFileName, days);
                    }
                } while (FindNextFile(hFind, &ffd) != 0);
                FindClose(hFind);
            }
        }
#else
        const char* path = _config.path.c_str();
        if(access( path, F_OK ) != -1) // file exists?
        {
            DIR *dir;
            struct dirent *ent;
            struct stat st;
            
            dir = opendir(path);
            while ((ent = readdir(dir)) != NULL)
            {
                const string file_name = ent->d_name;
                const string full_file_name = (string)path + "/" + file_name;
                
                if (file_name[0] == '.')
                    continue;
                
                if (stat(full_file_name.c_str(), &st) == -1)
                    continue;
                
                const bool is_directory = (st.st_mode & S_IFDIR) != 0;
                
                if (!is_directory)
                {
                    removeFile(_config.path, full_file_name.c_str(), days);
                }
            }
            closedir(dir);
        }
#endif
    }
    
    void LogTraceListener::removeFile(const string& dir, const string& fileName, int days)
    {
        string name = fileName;
        if (strcmp(_fullFileName.c_str(), name.c_str()) != 0)
        {
            const time_t now = time(NULL);
            
            if (!_config.prefix.empty())
            {
                Convert::replaceStr(name, _config.prefix, "");
            }
            if (!_config.suffix.empty())
            {
                Convert::replaceStr(name, _config.suffix, "");
            }
            Convert::replaceStr(name, _config.extName, "");
            time_t date = Convert::parseDate(name);
            time_t sec = now - date;
            if (sec < 0 && (-sec > days * 24 * 3600))
            {
                bool result = remove(fileName.c_str()) == 0;
                if (result)
                {
                    Trace::writeFormatLine(Resources::getString("RemoveLogFilesSuccessfully").c_str(), fileName.c_str());
                }
                else
                {
                    Trace::writeFormatLine(Resources::getString("FailedtoRemoveLogFiles").c_str(), fileName.c_str());
                }
            }
        }
    }
    
    bool LogTraceListener::createFile(const time_t& timep)
    {
        struct tm* tmp = localtime(&timep);
        if (tmp->tm_hour == 0 && tmp->tm_min == 0)
        {
            close(_file);
            _file = -1;
            return createFile(_config.path.c_str());
        }
        return true;
    }
    
    bool LogTraceListener::createFile(const char* logPath)
    {
        return createFile(time(NULL), logPath);
    }
    
    bool LogTraceListener::createFile(const time_t& timep, const char* logPath)
    {
#if WIN32
        char fileName[MAX_PATH];
#else
        char fileName[PATH_MAX];
#endif
        sprintf(fileName, "%s%s%s%s", _config.prefix.c_str(), Convert::getCurrentDateStr().c_str(), _config.suffix.c_str(), _config.extName.c_str());
        
        if (access(logPath, 0) == -1)	// file exists?
        {
#if WIN32
            mkdir(logPath);
#else
            mkdir(logPath, 0777);
#endif
        }
        _fullFileName = string(logPath) + "/" + fileName;
#ifndef O_BINARY
#define O_BINARY 0
#endif
#ifndef S_IREAD
#define S_IREAD S_IRUSR
#endif
#ifndef S_IWRITE
#define S_IWRITE S_IWUSR
#endif
        _file = open(_fullFileName.c_str(), O_APPEND | O_CREAT | O_RDWR | O_BINARY, S_IREAD | S_IWRITE);
        return fileOpened();
    }
    
    bool LogTraceListener::fileOpened() const
    {
        return _file != -1;
    }
}
