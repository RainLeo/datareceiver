#ifndef DEBUG_H
#define DEBUG_H

#include <assert.h>
#include "../system/Convert.h"
#include "Trace.h"

namespace Common
{
	class Debug
	{
	public:
		static void Assert(bool condition)
		{
#ifdef DEBUG
			assert(condition);
#endif
		}
		static void writeFormat(const char* format, ...)
		{
#ifdef DEBUG
			char* message = new char[Trace::MaxMessageLength];
			memset(message, 0, Trace::MaxMessageLength);
			va_list ap;
			va_start(ap, format);
			vsprintf(message, format, ap);
			va_end(ap);

			writeInner(message, false);
			delete[] message;
#endif
		}

		static void writeFormatLine(const char* format, ...)
		{
#ifdef DEBUG
			char* message = new char[Trace::MaxMessageLength];
			memset(message, 0, Trace::MaxMessageLength);
			va_list ap;
			va_start(ap, format);
			vsprintf(message, format, ap);
			va_end(ap);

			writeInner(message, true);
			delete[] message;
#endif
		}

		static void write(const String& message, const String& category = String::Empty, bool showTime = true)
		{
#ifdef DEBUG
			writeInner(message.c_str(), false, category.c_str(), showTime);
#endif
		}

		static void writeLine(const String& message, const String& category = String::Empty, bool showTime = true)
		{
#ifdef DEBUG
			writeInner(message.c_str(), true, category.c_str(), showTime);
#endif
		}

	private:
		static void writeInner(const char* message, bool newLine = false, const char* category = NULL, bool showTime = true)
		{
#ifdef DEBUG
#ifdef __ANDROID__
			Trace::writeInner(message, newLine, Trace::Debug, showTime);
#else
			Trace::writeInner(message, newLine, category, showTime);
#endif	// __ANDROID__
#endif	// DEBUG
		}
	};
}

#endif // DEBUG_H
