#ifndef LOGTRACELISTENER_H
#define LOGTRACELISTENER_H

#include <string>
#include "../system/Singleton.h"
#include "../thread/Thread.h"
#include "../xml/XmlTextReader.h"

using namespace std;

namespace Common
{
	enum LogLevel
	{
		/// <summary>
		/// Output error-handling messages.
		/// </summary>
		LogError,
		/// <summary>
		/// Output informational messages, warnings, and error-handling messages.
		/// </summary>
		LogInfo,
		/// <summary>
		/// Output no tracing and debugging messages.
		/// </summary>
		LogOff,
		/// <summary>
		/// Output all debugging and tracing messages.
		/// </summary>
		LogVerbose,
		/// <summary>
		/// Output warnings and error-handling messages.
		/// </summary>
		LogWarning,
		/// <summary>
		/// Output system message.
		/// </summary>
		LogSystem,
	};
    
    struct LogTraceConfig
    {
    public:
        bool enabled;
        
        string path;
        int reservationDays;        // unit: days
        int diskFullSize;           // unit: M
        string prefix;
        string suffix;
        string extName;             // with .
        int deleteUnuseFilesHour;   // 0-23
        
        LogTraceConfig();
        LogTraceConfig(const LogTraceConfig& config);
        LogTraceConfig(const string& path, const char* prefix = NULL, const char* suffix = NULL);
        
        void operator=(const LogTraceConfig& config);
        
        void read(XmlTextReader& reader, const string& localName="log");
    };

    class Trace;
	class COMMON_EXPORT LogTraceListener
	{
	public:
        LogTraceListener(const LogTraceConfig& config);
		LogTraceListener(const char* logPath = "logs", const char* prefix = NULL, const char* suffix = NULL);
        LogTraceListener(const string& logPath, const char* prefix = NULL, const char* suffix = NULL);
		~LogTraceListener();

	private:
		void writeLine(const char* message, const char* category);
        
		bool createFile(const char* logPath);
		bool createFile(const time_t& timep);
		bool createFile(const time_t& timep, const char* logPath);

		void deleteUnuseFiles();
		void deleteFiles(int days = 30);
		void deleteFiles(const time_t& timep);

		void updateMessageCount(const char* category = NULL);
		void flushInner(bool locked = true);

		bool isDiskFull();
		void removeFile(const string& dir, const string& fileName, int days);

		string getAppPath();
		bool isRealPath(const char* path);

		bool fileOpened() const;

		DECLARE_SINGLETON_CLASS(LogTraceListener);

	protected:
		friend void processProc(void* parameter);
		void processProcInner();
		friend void deleteUnuseFilesAction(void* parameter);

	private:
        friend class Trace;
        
		string _fullFileName;

		int _file;

		int _messageCount;
		const static int MaxMessageCount = 10;

		string _message;
		mutex _messageMutex;

		bool _diskIsFull;

		Thread* _processThread;
        
        LogTraceConfig _config;
	};
}

#endif // LOGTRACELISTENER_H
