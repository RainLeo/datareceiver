#ifndef STOPWATCH_H
#define STOPWATCH_H

#ifdef DEBUG
#include "common/common_global.h"
#include "../system/Convert.h"
#include "../thread/TickTimeout.h"
#include "../system/DateTime.h"
#include "../data/PrimitiveType.h"
#include "Debug.h"
#include "Trace.h"

namespace Common
{
	class COMMON_EXPORT Stopwatch
	{
	public:
		Stopwatch(const String& info, uint deadTime = 0) : _info(info)
		{
			start(deadTime);
		}
		~Stopwatch()
		{
			stop();
		}

		inline void reStart()
		{
			start(_deadTime);
		}
		inline void start(uint deadTime = 0)
		{
			_deadTime = deadTime;
			_startTime = TickTimeout::GetCurrentTickCount();
			_endTime = 0;
		}
		inline void stop(bool showInfo = true)
		{
			if (_endTime == 0)
			{
				_endTime = TickTimeout::GetCurrentTickCount();
				if (showInfo)
				{
					uint e = elapsedInner();
					if (e >= _deadTime)
					{
						if (e <= 10 * 1000)	// less than 10 seconds
						{
							Debug::writeFormatLine("%s: Elapsed: %d ms", _info.c_str(), e);
						}
						else
						{
							Debug::writeFormatLine("%s: Elapsed: %.3f s", _info.c_str(), e / 1000.0f);
						}
					}
				}
			}
		}
		inline void setInfo(const string& info)
		{
			_info = info;
		}
		inline void setInfo(const char* info)
		{
			_info = info;
		}
		inline uint elapsed() const
		{
			return elapsedInner(true);
		}
        inline TimeSpan elapsed2() const
        {
            uint e = elapsed();
            return TimeSpan::fromMilliseconds((double)e);
        }

	private:
		inline uint elapsedInner(bool currentTime = false) const
		{
			uint endTime = currentTime ? TickTimeout::GetCurrentTickCount() : _endTime;
			uint elapsed = TickTimeout::Elapsed(_startTime, endTime);
			return elapsed;
		}

	private:
		String _info;
		uint _deadTime;
		uint _startTime;
		uint _endTime;
	};
}
#endif // DEBUG
#endif // STOPWATCH_H
