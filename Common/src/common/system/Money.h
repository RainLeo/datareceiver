#ifndef MONEY_H
#define MONEY_H

#include <stdio.h>
#include <string>
#include <string.h>
#include "common/common_global.h"
#include "Convert.h"
#include "Math.h"
#include "Regex.h"

using namespace std;

namespace Common
{
	typedef int cent;

	class Money
	{
	public:
		inline static string getYuanString(cent centValue, int pointSize = 2)
		{
			float value = centValue/100.0f;
			return Convert::convertStr(value, pointSize);
		}

		inline static bool getYuanString(cent centValue, char* str, int pointSize = 2)
		{
			float value = centValue/100.0f;
			return Convert::convertStr(value, str, pointSize);
		}

		inline static cent getCentInteger(const string& yuanStr)
		{
			static Regex r("^[0-9]+(\\.[0-9]{0,2})?$");
			if(r.match(yuanStr))
			{
				float value = 0.0f;
				int len = 0;
				int result = sscanf(yuanStr.c_str(), "%f%n", &value, &len);
				if(result == 1 && yuanStr.length() == len)
				{
					return (cent)Math::round(value * 100.0f);
				}
			}
			return -1;
		}
	};
}
#endif // MONEY_H
