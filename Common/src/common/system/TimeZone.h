#ifndef TIMEZONE_H
#define TIMEZONE_H

#include <stdio.h>
#include <string>
#include <limits.h>
#include "common/common_global.h"
#include "../IO/Stream.h"
#include "DateTime.h"

using namespace std;

namespace Common
{
	struct COMMON_EXPORT TimeZone
	{
	public:
        DateTime toUtcTime(DateTime time);
        DateTime toLocalTime(DateTime time);
        
        void operator=(const TimeZone& tz);
        bool operator==(const TimeZone& tz) const;
        bool operator!=(const TimeZone& tz) const;
        
    public:
        static TimeZone Local;
        static TimeZone Zero;
        
    private:
        TimeZone(int64_t offset = 0);
        
    private:
        int64_t _ticksOffset;
        
        static const int64_t TicksPerMillisecond = 10000;
        static const int64_t TicksPerSecond = TicksPerMillisecond * 1000;   // 10,000,000
        static const int64_t TicksPerMinute = TicksPerSecond * 60;         // 600,000,000
	};
}

#endif // TIMEZONE_H

