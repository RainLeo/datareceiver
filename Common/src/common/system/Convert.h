#ifndef CONVERT_H
#define CONVERT_H

#include <stdio.h>
#include <string>
#include <chrono>
#include "common/common_global.h"
#include "../data/StringArray.h"
#include "../data/StringBuilder.h"

using namespace std;
using namespace std::chrono;

namespace Common
{
	class COMMON_EXPORT Convert
	{
	public:
		enum Base64FormattingOptions
		{
			Base64None = 0,
			InsertLineBreaks = 1
		};

        COMMON_ATTR_DEPRECATED("use String::NewLine")
		const static char* NewLine;
        COMMON_ATTR_DEPRECATED("use String::Empty")
		const static char* Empty;

        COMMON_ATTR_DEPRECATED("use DateTime::now")
		static string getCurrentTimeStr(bool includems = false);
        COMMON_ATTR_DEPRECATED("use DateTime::now")
		static string getCurrentDateStr();
        COMMON_ATTR_DEPRECATED("use DateTime::toString")
		static string getDateTimeStr(const time_t timep);
        COMMON_ATTR_DEPRECATED("use DateTime::toString")
		static bool getDateTimeStr(char* buffer, const time_t timep);
        COMMON_ATTR_DEPRECATED("use DateTime::toString")
		static string getDateTimeStr(const system_clock::time_point& timep, bool includems = false);
        COMMON_ATTR_DEPRECATED("use DateTime::toString")
		static bool getDateTimeStr(char* buffer, const system_clock::time_point& timep, bool includems = false);
        COMMON_ATTR_DEPRECATED("use DateTime::parse")
		static time_t parseDateTime(const string& timeStr, bool fullFormat = true);
        COMMON_ATTR_DEPRECATED("use DateTime::parse")
		static bool isDateTime(const string& timeStr);
        COMMON_ATTR_DEPRECATED("use DateTime::parse")
		static bool isDateTime(const char* timeStr);
        COMMON_ATTR_DEPRECATED("use DateTime::parse")
		static time_t parseDateTime(const char* timeStr, bool fullFormat = true);
        COMMON_ATTR_DEPRECATED("use DateTime::toString")
		static string getTimeStr(const time_t timep, bool includeSec = false);
        COMMON_ATTR_DEPRECATED("use DateTime::toString")
		static string getTimeStr(const system_clock::time_point& timep, bool includeSec = false);
        COMMON_ATTR_DEPRECATED("use DateTime::toString")
		static string getTimeHMSStr(const time_t timep);
        COMMON_ATTR_DEPRECATED("use DateTime::toString")
		static string getTimeHMStr(const time_t timep);
        COMMON_ATTR_DEPRECATED("use DateTime::toString")
		static string getTimeMSStr(const time_t timep);

        COMMON_ATTR_DEPRECATED("use DateTime::parse")
		static time_t parseDate(const string& dateStr);
        COMMON_ATTR_DEPRECATED("use DateTime::toString")
		static string getDateStr(const time_t timep);
        COMMON_ATTR_DEPRECATED("use DateTime::toString")
		static string getDateStr(const system_clock::time_point& timep);

        COMMON_ATTR_DEPRECATED("use String::replace")
		static void replaceStr(string& strBig, const string& strsrc, const string& strdst);
        COMMON_ATTR_DEPRECATED("use String::convert")
		static string convertStr(const char *format, ...);

		static string convertStr(int64_t value);
		static string convertStr(uint64_t value);
		static string convertStr(int value);
		static string convertStr(uint value);
		static string convertStr(bool value);

		static string convertStr(float value, int pointSize = -1);
		static bool convertStr(float value, char* str, int pointSize = -1);

		static string convertStr(double value, int pointSize = -1);

		static bool parseInt64(const string& text, int64_t& value, bool decimal = true);
		static bool parseUInt64(const string& text, uint64_t& value, bool decimal = true);

		static bool parseInt32(const string& text, int& value, bool decimal = true);
		static bool parseUInt32(const string& text, uint& value, bool decimal = true);

		static bool parseInt16(const string& text, short& value, bool decimal = true);
		static bool parseUInt16(const string& text, ushort& value, bool decimal = true);

		static bool parseByte(const string& text, byte& value, bool decimal = true);
		static bool parseBoolean(const string& text, bool& value);

		static bool parseSingle(const string& text, float& value);
		static bool parseDouble(const string& text, double& value);
        
        static bool parseStr(const string& text, float& value);
        static bool parseStr(const string& text, double& value);
        
        static bool parseStr(const string& text, int64_t& value, bool decimal = true);
        static bool parseStr(const string& text, uint64_t& value, bool decimal = true);
        
        static bool parseStr(const string& text, int& value, bool decimal = true);
        static bool parseStr(const string& text, uint& value, bool decimal = true);
        
        static bool parseStr(const string& text, short& value, bool decimal = true);
        static bool parseStr(const string& text, ushort& value, bool decimal = true);
        
        static bool parseStr(const string& text, byte& value, bool decimal = true);
        static bool parseStr(const string& text, bool& value);

		static string convertIPStr(byte ip1, byte ip2, byte ip3, byte ip4);
        static string convertIPStr(const string& ip1, const string& ip2, const string& ip3, const string& ip4);
        static string convertIPStr(uint ip);
        
		static bool parseIPStr(const string& text, string& ip1, string& ip2, string& ip3, string& ip4);
		static bool parseIPStr(const string& text, byte& ip1, byte& ip2, byte& ip3, byte& ip4);
		static bool parseIPStr(const string& text, uint& ip);

		static time_t combineTime(int year, int month, int day, int hour = 0, int minute = 0, int second = 0);

		static void splitStr(const string& str, const char splitSymbol, StringArray& texts);
        static void splitStr(const string& str, StringArray& texts, const char splitSymbol=';');
        static void splitItems(const string& str, StringArray& texts, const char splitSymbol=';', const char escape='\\', const char startRange='{', const char endRange='}');
        class KeyPair
        {
        public:
            string name;
            string value;
            
            KeyPair(const string& name="", const string& value="")
            {
                this->name = name;
                this->value = value;
            }
        };
        typedef Vector<KeyPair> KeyPairs;
        static bool splitItems(const string& str, KeyPairs& pairs, const char splitSymbol=';', const char escape='\\', const char startRange='{', const char endRange='}');
        COMMON_ATTR_DEPRECATED("use String::equals")
		static bool stringEqual(const string& a, const string& b, bool ignoreCase = false);
		COMMON_ATTR_DEPRECATED("use String::toLower")
        static const string toLower(const string& str);
        COMMON_ATTR_DEPRECATED("use String::toUpper")
		static const string toUpper(const string& str);
        COMMON_ATTR_DEPRECATED("use String::trim")
		static const string trimStr(const string& str, const char symbol1=' ', const char symbol2='\0', const char symbol3='\0', const char symbol4='\0');
        COMMON_ATTR_DEPRECATED("use String::trimStart")
        static const string trimStartStr(const string& str, const char symbol1=' ', const char symbol2='\0', const char symbol3='\0', const char symbol4='\0');
        COMMON_ATTR_DEPRECATED("use String::trimEnd")
        static const string trimEndStr(const string& str, const char symbol1=' ', const char symbol2='\0', const char symbol3='\0', const char symbol4='\0');

        COMMON_ATTR_DEPRECATED("use String::GBKtoUTF8")
		static const string GBKtoUTF8Str(const char* str);
        COMMON_ATTR_DEPRECATED("use String::GBKtoUTF8")
		static const string GBKtoUTF8Str(const string& str);

        COMMON_ATTR_DEPRECATED("use String::UTF8toGBK")
		static const string UTF8toGBKStr(const char* str);
        COMMON_ATTR_DEPRECATED("use String::UTF8toGBK")
		static const string UTF8toGBKStr(const string& str);

        COMMON_ATTR_DEPRECATED("use String::encoding")
		static const string encodingToStr(const char* fromCode, const char* toCode, const char* str);
        COMMON_ATTR_DEPRECATED("use String::encoding")
		static bool encodingToStr(const char* fromCode, const char* toCode, const char* str, char*& buffer, int& length);

        COMMON_ATTR_DEPRECATED("use String::toBase64")
		static string toBase64String(const string& str);
        COMMON_ATTR_DEPRECATED("use String::toBase64")
		static string toBase64String(const byte* inArray, int offset, int length, Base64FormattingOptions options = Convert::Base64None);
        COMMON_ATTR_DEPRECATED("use String::toBase64")
		static bool toBase64String(const byte* inArray, int offset, int length, StringBuilder& array, Base64FormattingOptions options = Convert::Base64None);

        COMMON_ATTR_DEPRECATED("use String::fromBase64")
		static string fromBase64String(const string& str);
        COMMON_ATTR_DEPRECATED("use String::fromBase64")
		static bool fromBase64String(const string& str, ByteArray& array);
        COMMON_ATTR_DEPRECATED("use String::fromBase64")
		static bool fromBase64String(const char* inputPtr, int inputLength, ByteArray& array);
        
        static byte ascToHex(byte aHex);
        static byte hexToAsc(byte aChar);

	private:
		static int convertToBase64Array(char* outChars, const byte* inData, int offset, int length, bool insertLineBreaks);
		static int toBase64_CalculateAndValidateOutputLength(int inputLength, bool insertLineBreaks);

		static int fromBase64_ComputeResultLength(const char* inputPtr, int inputLength);
		static int fromBase64_Decode(const char* startInputPtr, int inputLength, byte* startDestPtr, int destLength);
        
        static bool isHexInteger(const string& text);

	public:
		static const int MaxFormatStrLength = 1024 * 10;	// 10 K

	private:
		const static char base64Table[65];

		const static int base64LineBreakPosition = 76;
	};
}

#endif // CONVERT_H
