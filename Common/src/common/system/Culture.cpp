//
//  Culture.cpp
//  common
//
//  Created by baowei on 15/8/29.
//  Copyright (c) 2015 com. All rights reserved.
//

#include "Culture.h"

namespace Common
{
    const Culture::Data Culture::AllCultures[AllCulturesCount] =
    {
        Culture::Data(0x0009, "en"),
        Culture::Data(0x0409, "en-US"),
        Culture::Data(0x0804, "zh-CN"),
        Culture::Data(0x0004, "zh-CHS")
    };
    
    Culture::Culture()
    {
    }
    Culture::Culture(const string& name)
    {
        for (uint i=0; i<AllCulturesCount; i++)
        {
            const Culture::Data& data = AllCultures[i];
            if(data.name == name)
            {
                _data = data;
            }
        }
    }
    Culture::Culture(uint id)
    {
        for (uint i=0; i<AllCulturesCount; i++)
        {
            const Culture::Data& data = AllCultures[i];
            if(data.id == id)
            {
                _data = data;
            }
        }
    }
    Culture::~Culture()
    {
    }
    
    const string& Culture::name() const
    {
        return _data.name;
    }
    uint Culture::id() const
    {
        return _data.id;
    }
}
