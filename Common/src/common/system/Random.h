#ifndef RANDOM_H
#define RANDOM_H

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <time.h>
#include "common/common_global.h"

using namespace std;

namespace Common
{
	class Random
	{
	public:
		inline static int getRandValue(int min, int max)
		{
			static time_t curtime = 0;
			if(curtime == 0)
			{
				curtime = time(NULL);
				srand((unsigned)curtime);
			}
			//srand((unsigned)time(NULL));

			// range_min <= return value <= range_max
			if (min>max) 
			{ 
				return (int)(rand()/(RAND_MAX+1.0)*(min-max+1))+max;
			} 
			else 
			{
				return (int)(rand()/(RAND_MAX+1.0)*(max-min+1))+min;
			}
		}
	};
}

#endif // RANDOM_H
