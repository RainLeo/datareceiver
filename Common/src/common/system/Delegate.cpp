//
//  Delegate.cpp
//  common
//
//  Created by baowei on 15/4/18.
//  Copyright (c) 2015 com. All rights reserved.
//

#include "Delegate.h"
#include "common/thread/Locker.h"
#include "common/exception/Exception.h"

namespace Common
{
    void Delegate::invoke(void* sender, EventArgs* args)
    {
        if(handler != NULL)
        {
            handler(owner, sender, args);
        }
    }
    
    void Delegates::add(void* owner, EventHandler handler)
    {
        if(handler == NULL)
            throw ArgumentException("handler");
        
        Locker locker(&_mutex);
        
        Vector<Delegate>::add(new Delegate(owner, handler));
    }
    void Delegates::remove(void* owner, EventHandler handler)
    {
        Locker locker(&_mutex);
        
        for (uint i=0; i<count(); i++)
        {
            Delegate* delegate = at(i);
            if(delegate != NULL)
            {
                if(delegate->owner == owner &&
                   delegate->handler == handler)
                {
                    Vector<Delegate>::removeAt(i);
                    break;
                }
            }
        }
    }
    void Delegates::add(const Delegate& delegate)
    {
        add(delegate.owner, delegate.handler);
    }
    void Delegates::remove(const Delegate& delegate)
    {
        remove(delegate.owner, delegate.handler);
    }
    void Delegates::invoke(void* sender, EventArgs* args)
    {
        Locker locker(&_mutex);
        
        for (uint i=0; i<count(); i++)
        {
            Delegate* delegate = at(i);
            if(delegate != NULL)
            {
                delegate->invoke(sender, args);
                HandledEventArgs* hargs = dynamic_cast<HandledEventArgs*>(args);
                if(hargs != NULL && hargs->handled)
                {
                    break;
                }
            }
        }
    }
}
