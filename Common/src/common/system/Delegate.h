//
//  Delegate.h
//  common
//
//  Created by baowei on 15/4/18.
//  Copyright (c) 2015 com. All rights reserved.
//

#ifndef __common__Delegate__
#define __common__Delegate__

#include <mutex>
#include "common/common_global.h"
#include "common/data/Vector.h"

namespace Common
{
    class EventArgs
    {
    public:
        EventArgs()
        {
        }
        virtual ~EventArgs()
        {
        }
    };
    class HandledEventArgs : public EventArgs
    {
    public:
        HandledEventArgs(bool handled = false)
        {
            this->handled = handled;
        }
        
    public:
        bool handled;
    };
    
    typedef void (*EventHandler)(void*, void*, EventArgs*);
//    typedef void (*HandledEventHandler)(void*, HandledEventArgs*);
    
    class Delegate
    {
    public:
        Delegate(const Delegate& delegate) : Delegate(delegate.owner, delegate.handler)
        {
        }
        Delegate(void* owner, EventHandler handler)
        {
            this->owner = owner;
            this->handler = handler;
        }
        
        void invoke(void* sender = NULL, EventArgs* args = NULL);
        
    public:
        EventHandler handler;
        void* owner;
    };
//    typedef Vector<Delegate> Delegates;
    class Delegates : public Vector<Delegate>
    {
    public:
        Delegates(bool autoDelete = true, uint capacity = 1) : Vector<Delegate>(autoDelete, capacity)
        {
        }
        
        void add(const Delegate& delegate);
        void remove(const Delegate& delegate);
        void add(void* owner, EventHandler hander);
        void remove(void* owner, EventHandler hander);
        void invoke(void* sender = NULL, EventArgs* args = NULL);
        
    private:
        mutex _mutex;
    };
}

#endif /* defined(__common__Delegate__) */
