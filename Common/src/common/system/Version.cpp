#include "Version.h"
#include "Convert.h"
#include "../data/StringArray.h"

namespace Common
{
    const Version Version::Empty = Version();
	Version::Version(const char* version)
	{
		if (version != NULL)
		{
			parse(version, *this);
		}
	}
	Version::Version(const string& version)
	{
		if (!version.empty())
		{
			parse(version, *this);
		}
	}
	Version::Version(int major, int minor, int build, int revision)
	{
		_major = major;
		_minor = minor;
		_build = build;
		_revision = revision;
	}
    Version::Version(const Version& version)
    {
        this->operator=(version);
    }

	Version::~Version()
	{
	}

	bool Version::parse(const string& str, Version& version)
	{
		StringArray componnets;
		Convert::splitStr(str, '.', componnets);
		uint length = componnets.count();
		if ((length < 2) || (length > 4))
		{
			return false;
		}

		string str1 = componnets[0];
		string str2 = componnets[1];
		int v1, v2;
		if (!Convert::parseInt32(str1, v1))
		{
			return false;
		}
		if (!Convert::parseInt32(str2, v2))
		{
			return false;
		}

		length -= 2;
		if (length > 0)
		{
			int v3, v4;
			string str3 = componnets[2];
			if (!Convert::parseInt32(str3, v3))
			{
				return false;
			}
			length--;
			if (length > 0)
			{
				string str4 = componnets[3];
				if (!Convert::parseInt32(str4, v4))
				{
					return false;
				}
				version._major = v1;
				version._minor = v2;
				version._build = v3;
				version._revision = v4;
			}
			else
			{
				version._major = v1;
				version._minor = v2;
				version._build = v3;
			}
		}
		else
		{
			version._major = v1;
			version._minor = v2;
		}
		return true;
	}

	String Version::toString() const
	{
		if (_build == -1)
		{
			return toString(2);
		}
		if (_revision == -1)
		{
			return toString(3);
		}
		return toString(4);

	}

	String Version::toString(int fieldCount) const
	{
		switch (fieldCount)
		{
		case 0:
			return "";

		case 1:
			return Convert::convertStr(_major);

		case 2:
			return String::convert("%d.%d", _major, _minor);

		case 3:
			return String::convert("%d.%d.%d", _major, _minor, _build);

		case 4:
			return String::convert("%d.%d.%d.%d", _major, _minor, _build, _revision);

		default:
			return "";
		}
	}

    void Version::operator=(const Version& version)
    {
        this->_major = version._major;
        this->_minor = version._minor;
        this->_build = version._build;
        this->_revision = version._revision;
    }
	bool Version::operator==(const Version& version) const
	{
		return (_major == version._major &&
			_minor == version._minor &&
			_build == version._build &&
			_revision == version._revision);
	}
	bool Version::operator!=(const Version& version) const
	{
		return !operator==(version);
	}
	bool Version::operator>=(const Version& version) const
	{
		return version.compareTo(*this) <= 0;
	}
	bool Version::operator>(const Version& version) const
	{
		return version.compareTo(*this) < 0;
	}
	bool Version::operator<=(const Version& version) const
	{
		return compareTo(version) <= 0;
	}
	bool Version::operator<(const Version& version) const
	{
		return compareTo(version) < 0;
	}
    bool Version::IsCompatible(const Version& version) const
    {
        return (_major == version._major && _minor == version._minor && _build == version._build);
    }

	int Version::compareTo(const Version& version) const
	{
		if (_major != version._major)
		{
			if (_major > version._major)
			{
				return 1;
			}
			return -1;
		}
		if (_minor != version._minor)
		{
			if (_minor > version._minor)
			{
				return 1;
			}
			return -1;
		}
		if (_build != version._build)
		{
			if (_build > version._build)
			{
				return 1;
			}
			return -1;
		}
		if (_revision == version._revision)
		{
			return 0;
		}
		if (_revision > version._revision)
		{
			return 1;
		}
		return -1;
	}

	void Version::writeInt32(Stream* stream, bool bigEndian) const
	{
		stream->writeInt32(_major, bigEndian);
		stream->writeInt32(_minor, bigEndian);
		stream->writeInt32(_build, bigEndian);
		stream->writeInt32(_revision, bigEndian);
	}
	void Version::readInt32(Stream* stream, bool bigEndian)
	{
		_major = stream->readInt32(bigEndian);
		_minor = stream->readInt32(bigEndian);
		_build = stream->readInt32(bigEndian);
		_revision = stream->readInt32(bigEndian);
	}
	void Version::writeInt16(Stream* stream, bool bigEndian) const
	{
		stream->writeInt16(_major, bigEndian);
		stream->writeInt16(_minor, bigEndian);
		stream->writeInt16(_build, bigEndian);
		stream->writeInt16(_revision, bigEndian);
	}
	void Version::readInt16(Stream* stream, bool bigEndian)
	{
		_major = stream->readInt16(bigEndian);
		_minor = stream->readInt16(bigEndian);
		_build = stream->readInt16(bigEndian);
		_revision = stream->readInt16(bigEndian);
	}
	void Version::writeBCDInt16(Stream* stream) const
	{
		stream->writeBCDInt16(_major);
		stream->writeBCDInt16(_minor);
		stream->writeBCDInt16(_build);
		stream->writeBCDInt16(_revision);
	}
	void Version::readBCDInt16(Stream* stream)
	{
		_major = stream->readBCDInt16();
		_minor = stream->readBCDInt16();
		_build = stream->readBCDInt16();
		_revision = stream->readBCDInt16();
	}
	void Version::writeBCDByte(Stream* stream) const
	{
		stream->writeBCDByte(_major);
		stream->writeBCDByte(_minor);
		stream->writeBCDByte(_build);
		stream->writeBCDByte(_revision);
	}
	void Version::readBCDByte(Stream* stream)
	{
		_major = stream->readBCDByte();
		_minor = stream->readBCDByte();
		_build = stream->readBCDByte();
		_revision = stream->readBCDByte();
	}
    void Version::writeByte(Stream* stream) const
    {
        stream->writeByte(_major);
        stream->writeByte(_minor);
        stream->writeByte(_build);
        stream->writeByte(_revision);
    }
    void Version::readByte(Stream* stream)
    {
        _major = stream->readByte();
        _minor = stream->readByte();
        _build = stream->readByte();
        _revision = stream->readByte();
    }
    void Version::write(Stream* stream) const
    {
        writeInt32(stream);
    }
    void Version::read(Stream* stream)
    {
        readInt32(stream);
    }
}
