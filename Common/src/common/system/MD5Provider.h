#ifndef MD5PROVIDER_H
#define MD5PROVIDER_H

#include "common/common_global.h"
#include "md5.h"

namespace Common
{
	class MD5Provider
	{
	public:
		static bool computeHash(const char* path, byte output[16])
		{
			return md5_file(path, output) == 0;
		}
	};
}

#endif // MD5PROVIDER_H
