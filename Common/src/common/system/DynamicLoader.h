#ifndef DYNAMICLOADER_H
#define DYNAMICLOADER_H

#include "common/common_global.h"

namespace Common
{
    class COMMON_EXPORT DynamicLoader
    {
    public:
        DynamicLoader(const char* libName);
        DynamicLoader(const string& libName);
        ~DynamicLoader();
        
        void* getSymbol(const char* symbol);
        void* getSymbol(const string& symbol);
        
        bool hasError() const;
        string error() const;

	private:
		void retriveError();
        
    private:
        void* _handle;
        string _error;
    };
}

#endif	// DYNAMICLOADER_H
