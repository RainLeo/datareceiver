#include "LocalTime.h"
#include "Convert.h"
#include "DateTime.h"
#include "../diag/Debug.h"
#ifdef WIN32
#include <windows.h>
#pragma comment (lib, "Advapi32.lib")
#else
#include <limits.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/time.h>
#endif

namespace Common
{
#ifdef WIN32
	void EnableSystemTimePriv()
	{
		HANDLE hToken;
		LUID luid;
		TOKEN_PRIVILEGES tkp;

		OpenProcessToken( GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken );

		LookupPrivilegeValue( NULL, SE_SYSTEMTIME_NAME, &luid );

		tkp.PrivilegeCount = 1;
		tkp.Privileges[0].Luid = luid;
		tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

		AdjustTokenPrivileges( hToken, false, &tkp, sizeof( tkp ), NULL, NULL );

		CloseHandle( hToken ); 
	}
#endif

	bool LocalTime::setTime(const time_t& timep)
	{
#ifdef WIN32
		EnableSystemTimePriv();

		struct tm* t = localtime(&timep);

		SYSTEMTIME st;
		st.wYear = t->tm_year;
		st.wMonth = t->tm_mon;
		st.wDay = t->tm_mday;
		st.wHour = t->tm_hour;
		st.wMinute = t->tm_min;
		st.wSecond = t->tm_sec;
		st.wMilliseconds = 0;
		return SetLocalTime(&st) ? true : false;
#else
		struct tm* t = localtime(&timep);

		struct timeval tv;
		tv.tv_sec = mktime(t);
		tv.tv_usec = 0;
		return settimeofday(&tv, NULL) == 0;
#endif
	}
	bool LocalTime::setTime(const DateTime& time)
	{
#ifdef WIN32
		EnableSystemTimePriv();

		SYSTEMTIME st;
		st.wYear = time.year();
		st.wMonth = time.month();
		st.wDay = time.day();
		st.wHour = time.hour();
		st.wMinute = time.minute();
		st.wSecond = time.second();
		st.wMilliseconds = time.millisecond();
		return SetLocalTime(&st) ? true : false;
#else
        DateTime start = DateTime(1970, 1, 1).toLocalTime();
		struct timeval tv;
		TimeSpan diff = (DateTime)time - start;
		tv.tv_sec = (time_t)diff.totalSeconds();
		tv.tv_usec = 0;
		return settimeofday(&tv, NULL) == 0;
#endif
	}
}
