#ifndef CRC16UTILITIES_H
#define CRC16UTILITIES_H

#include "common/common_global.h"
#include "Convert.h"

namespace Common
{
    // Block Check Character
    class BccUtilities
    {
   	public:
        static byte checkByBit(const byte* buffer, int offset, int count)
        {
            byte result = 0;
            for (int i = offset; i < count + offset; i++)
            {
                result ^= buffer[i];
            }
            return result;
        }
    };
    
    class SumUtilities
    {
   	public:
        static byte checkByBit(const byte* buffer, int offset, int count)
        {
            byte result = 0;
            for (int i = offset; i < count + offset; i++)
            {
                result += buffer[i];
            }
            return result;
        }
    };
    
    // Longitudinal Redundancy Check
    class LrcUtilities
    {
   	public:
        static byte checkByBit(const byte* buffer, int offset, int count)
        {
            byte result = 0;
            for (int i = offset; i < count + offset; i++)
            {
                result += buffer[i];
            }
            result = -result;
//            result = ~result;
//            result++;
            return result;
        }
    };
    
    // Cyclic Redundancy Check 16
	class Crc16Utilities
	{
	public:
		static unsigned short checkByBit(const byte* buffer, int offset, int count, unsigned short initValue = 0x0, unsigned short polynomial = 0xA001)
		{
			for (int i = offset; i < count + offset; i++)
			{
				initValue = (unsigned short)(initValue ^ buffer[i]);
				for (int j = 0; j < 8; j++)
				{
					int msbInfo = initValue & 0x0001;
					initValue = (unsigned short)(initValue >> 1);
					if (msbInfo != 0)
					{
						initValue = (unsigned short)(initValue ^ polynomial);
					}
				}
			}
			return initValue;
		}
	};
}

#endif // CRC16UTILITIES_H