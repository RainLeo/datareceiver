#include <math.h>
#include "TimeZone.h"
#include "Convert.h"
#include "Math.h"
#include "../exception/Exception.h"
#include "../data/StringArray.h"
#include "../data/BCDUtilities.h"

namespace Common
{
    TimeZone TimeZone::Local = TimeZone();
    TimeZone TimeZone::Zero = TimeZone();
    TimeZone::TimeZone(int64_t offset)
    {
        if(this == &Local)
        {
#if WIN32
			_ticksOffset = TicksPerSecond * timezone;
#else
            struct timeval tv;
            struct timezone tz;
            gettimeofday(&tv, &tz);
            _ticksOffset = TicksPerMinute * tz.tz_minuteswest;
#endif
        }
        else
        {
            _ticksOffset = offset;
        }
    }
    
    DateTime TimeZone::toUtcTime(DateTime time)
    {
        return DateTime(time.ticks() + _ticksOffset, DateTime::Utc);
    }
    DateTime TimeZone::toLocalTime(DateTime time)
    {
        return DateTime(time.ticks() - _ticksOffset, DateTime::Local);
    }
    
    void TimeZone::operator=(const TimeZone& tz)
    {
        this->_ticksOffset = tz._ticksOffset;
    }
    bool TimeZone::operator==(const TimeZone& tz) const
    {
        return this->_ticksOffset == tz._ticksOffset;
    }
    bool TimeZone::operator!=(const TimeZone& tz) const
    {
        return !operator==(tz);
    }
}
