#ifndef VERSION_H
#define VERSION_H

#include <stdio.h>
#include "common/common_global.h"
#include "../IO/Stream.h"
#include "../data/PrimitiveType.h"

namespace Common
{
	struct COMMON_EXPORT Version
	{
	public:
		Version(const char* version);
		Version(const string& version);
        Version(const Version& version);
		Version(int major = 0, int minor = 0, int build = -1, int revision = -1);
		virtual ~Version();

		static bool parse(const string& str, Version& version);
		String toString() const;

        void operator=(const Version& version);
		bool operator==(const Version& version) const;
		bool operator!=(const Version& version) const;
		bool operator>=(const Version& version) const;
		bool operator>(const Version& version) const;
		bool operator<=(const Version& version) const;
		bool operator<(const Version& version) const;
        bool IsCompatible(const Version& version) const;

		void writeInt32(Stream* stream, bool bigEndian = true) const;
		void readInt32(Stream* stream, bool bigEndian = true);
		void writeInt16(Stream* stream, bool bigEndian = true) const;
		void readInt16(Stream* stream, bool bigEndian = true);
		void writeBCDInt16(Stream* stream)  const;
		void readBCDInt16(Stream* stream);
		void writeBCDByte(Stream* stream)  const;
		void readBCDByte(Stream* stream);
        void writeByte(Stream* stream)  const;
        void readByte(Stream* stream);
        void write(Stream* stream)  const;
        void read(Stream* stream);
        
    public:
        static const Version Empty;

	private:
		String toString(int fieldCount) const;

		int compareTo(const Version& version) const;

	private:
		int _major;
		int _minor;
		int _build;
		int _revision;
	};
}

#endif // VERSION_H

