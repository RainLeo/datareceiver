#ifndef MATH_H
#define MATH_H

#include <stdlib.h>
#include <math.h>
#include <stdint.h>
#include "common/common_global.h"

namespace Common
{
#if WIN32
#ifndef NOMINMAX
#define NOMINMAX
#endif
#undef min
#undef max
#endif
	class Math
	{
	public:
		inline static int min(int a, int b)
		{
			return a < b ? a : b;
		}
		inline static int max(int a, int b)
		{
			return a < b ? b : a;
		}
        inline static uint min(uint a, uint b)
        {
            return a < b ? a : b;
        }
        inline static uint max(uint a, uint b)
        {
            return a < b ? b : a;
        }
        inline static int64_t min(int64_t a, int64_t b)
        {
            return a < b ? a : b;
        }
        inline static int64_t max(int64_t a, int64_t b)
        {
            return a < b ? b : a;
        }
        inline static float min(float a, float b)
        {
            return a < b ? a : b;
        }
        inline static float max(float a, float b)
        {
            return a < b ? b : a;
        }
        
        inline static float round(float value)
        {
            return ::roundf(value);
        }
        inline static double round(double value)
        {
            return ::round(value);
        }
        inline static long double round(long double value)
        {
            return ::roundl(value);
        }
        
        inline static double sin(double value)
        {
            return ::sin(value);
        }
        inline static float sin(float value)
        {
            return ::sinf(value);
        }
        inline static double cos(double value)
        {
            return ::cos(value);
        }
        inline static float cos(float value)
        {
            return ::cosf(value);
        }
        inline static double tan(double value)
        {
            return ::tan(value);
        }
        inline static float tan(float value)
        {
            return ::tanf(value);
        }
        inline static double asin(double value)
        {
            return ::asin(value);
        }
        inline static float asin(float value)
        {
            return ::asinf(value);
        }
        inline static double acos(double value)
        {
            return ::acos(value);
        }
        inline static float acos(float value)
        {
            return ::acosf(value);
        }
        inline static double atan(double value)
        {
            return ::atan(value);
        }
        inline static float atan(float value)
        {
            return ::atanf(value);
        }
        
        inline static double pow(double a, double b)
        {
            return ::pow(a, b);
        }
        inline static float pow(float a, float b)
        {
            return ::powf(a, b);
        }
        inline static float sqrt(float value)
        {
            return ::sqrtf(value);
        }
        inline static double sqrt(double value)
        {
            return ::sqrt(value);
        }

        inline static int abs(int value)
        {
            return ::abs(value);
        }
        inline static float abs(float value)
        {
            return ::fabsf(value);
        }
        inline static double abs(double value)
        {
            return ::fabs(value);
        }
        inline static long double abs(long double value)
        {
            return ::fabsl(value);
        }
        
        inline static float toRadian(float angle)
        {
            return (float)(angle * RadianFactor);
        }
        inline static float toAngle(float radian)
        {
            return (float)(radian * AngleFactor);
        }
        static void calcCoordinate(float angle, float l, float& x, float& y);
        static bool calcPolarCoordinate(float x, float y, float& angle, float& l);
        
    public:
        static const double PI;
        static const double RadianFactor;
        static const double AngleFactor;
        static const double E;
	};
}

#endif // MATH_H
