#ifndef APPLICATION_H
#define APPLICATION_H

#include "common/common_global.h"
#include "common/diag/LogTraceListener.h"
#include "Culture.h"
#include "Delegate.h"

namespace Common
{
    class COMMON_EXPORT Application
    {
    public:
        Application();
        Application(int argc, const char * argv[]);
        ~Application();
        
        string rootPath() const;
        
        const Culture& culture() const;
        void setCulture(const Culture& culture);
        
        static Application* instance();
        
    protected:
        void initLog(const string& rootPath, const string& logPath = "logs");
        void unInitLog();
        
    protected:
        string _rootPath;
        static bool _phoneApplication;
        
    private:
        LogTraceListener* _logListener;
        Culture _culture;
        
        static Application* _instance;
    };
    
    typedef void (*action_callback)();
    class COMMON_EXPORT PhoneApplication : public Application
    {
    public:
        ~PhoneApplication();
        
        static PhoneApplication* instance();
        
        void onCreate(const string& rootPath);
        void onDestroy();
        
        void onActived();
        void onInactived();
        void onActiving();
        void onInactiving();
        
        void addEventCreate(const Delegate& delegate);
        void removeEventCreate(const Delegate& delegate);
        void addEventActived(const Delegate& delegate);
        void removeEventActived(const Delegate& delegate);
        void addEventInactived(const Delegate& delegate);
        void removeEventInactived(const Delegate& delegate);
        
    private:
        enum EventType
        {
            EventCreate = 0,
            EventDestroy,
            EventActived,
            EventActiving,
            EventInactived,
            EventInactiving
        };
        
    private:
        PhoneApplication();
        
        void addEvent(EventType type, const Delegate& delegate);
        void removeEvent(EventType type, const Delegate& delegate);
        void invokeEvent(EventType type);
        
    private:
        static PhoneApplication* _instance;
        
        const static int EventCount = 6;
        Delegates _delegates[EventCount];
        mutex _delegatesMutex[EventCount];
    };
}

#endif // APPLICATION_H
