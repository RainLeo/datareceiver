#include "DateTime.h"
#include "Convert.h"
#include "Math.h"
#include "TimeSpan.h"
#include "../exception/Exception.h"
#include "../data/StringArray.h"
#include "../data/BCDUtilities.h"

namespace Common
{
	const int DateTime::DaysToMonth365[13] = {
		0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365 };
	const int DateTime::DaysToMonth366[13] = {
		0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366 };

	const DateTime DateTime::MinValue(MinTicks);
	const DateTime DateTime::MaxValue(MaxTicks);

	DateTime::DateTime(uint64_t ticks, Kind kind)
	{
		_dateData = ((uint64_t)ticks | ((uint64_t)kind << KindShift));
	}
	DateTime::DateTime(int year, int month, int day, int hour, int minute, int second, int millisecond, Kind kind)
	{
		_dateData = toTicks(year, month, day, hour, minute, second, millisecond, kind);
	}

	uint64_t DateTime::toTicks(int year, int month, int day, int hour, int minute, int second, int millisecond, Kind kind)
	{
		uint64_t ticks = dateToTicks(year, month, day) + timeToTicks(hour, minute, second);
		ticks += millisecond * TicksPerMillisecond;
		return ((uint64_t)ticks | ((uint64_t)kind << KindShift));
	}
	DateTime DateTime::now()
	{
		system_clock::time_point tp = system_clock::now();
		time_t tt = system_clock::to_time_t(tp);
		struct tm* tmp = localtime(&tt);
		auto currentTimeRounded = system_clock::from_time_t(tt);
		if (currentTimeRounded > tp) {
			currentTimeRounded -= std::chrono::seconds(1);
		}
		int milliseconds = duration_cast<duration<int, std::milli>>(tp - currentTimeRounded).count();
		DateTime dateTime(tmp->tm_year + 1900, tmp->tm_mon + 1, tmp->tm_mday, tmp->tm_hour, tmp->tm_min, tmp->tm_sec, milliseconds);
		return dateTime;
	}
	DateTime DateTime::utcNow()
	{
		system_clock::time_point tp = system_clock::now();
		time_t tt = system_clock::to_time_t(tp);
		struct tm* tmp = gmtime(&tt);
		auto currentTimeRounded = system_clock::from_time_t(tt);
		if (currentTimeRounded > tp) {
			currentTimeRounded -= std::chrono::seconds(1);
		}
		int milliseconds = duration_cast<duration<int, std::milli>>(tp - currentTimeRounded).count();
		DateTime dateTime(tmp->tm_year + 1900, tmp->tm_mon + 1, tmp->tm_mday, tmp->tm_hour, tmp->tm_min, tmp->tm_sec, milliseconds, Utc);
		return dateTime;
	}
	DateTime DateTime::toLocalTime() const
	{
		DateTime time = *this;
		if (kind() == Utc)
		{
			return time.addSeconds(-timezone);
		}
		return time;
	}
	DateTime DateTime::toUtcTime() const
	{
		DateTime time = *this;
		if (kind() != Utc)
		{
			return time.addSeconds(timezone);
		}
		return time;
	}

	uint64_t DateTime::dateToTicks(int year, int month, int day)
	{
		if (year >= 1 && year <= 9999 && month >= 1 && month <= 12)
		{
			const int* days = isLeapYear(year) ? DaysToMonth366 : DaysToMonth365;
			if (day >= 1 && day <= days[month] - days[month - 1]) {
				int y = year - 1;
				int n = y * 365 + y / 4 - y / 100 + y / 400 + days[month - 1] + day - 1;
				return n * TicksPerDay;
			}
		}
		throw ArgumentOutOfRangeException(NULL, "Year, Month, and Day parameters describe an un - representable DateTime.");
	}
	uint64_t DateTime::timeToTicks(int hour, int minute, int second)
	{
		//TimeSpan.TimeToTicks is a family access function which does no error checking, so
		//we need to put some error checking out here.
		if (hour >= 0 && hour < 24 && minute >= 0 && minute < 60 && second >= 0 && second < 60)
		{
			return TimeSpan::timeToTicks(hour, minute, second);
		}
		throw ArgumentOutOfRangeException(NULL, "Hour, Minute, and Second parameters describe an un-representable DateTime.");
	}
	bool DateTime::isLeapYear(int year)
	{
		if (year < 1 || year > 9999)
		{
			throw ArgumentOutOfRangeException("year", "Year must be between 1 and 9999.");
		}
		return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
	}

	// Returns a given date part of this DateTime. This method is used
	// to compute the year, day-of-year, month, or day part.
	int DateTime::getDatePart(int part) const
	{
		uint64_t ticks = internalTicks();
		// n = number of days since 1/1/0001
		int n = (int)(ticks / TicksPerDay);
		// y400 = number of whole 400-year periods since 1/1/0001
		int y400 = n / DaysPer400Years;
		// n = day number within 400-year period
		n -= y400 * DaysPer400Years;
		// y100 = number of whole 100-year periods within 400-year period
		int y100 = n / DaysPer100Years;
		// Last 100-year period has an extra day, so decrement result if 4
		if (y100 == 4) y100 = 3;
		// n = day number within 100-year period
		n -= y100 * DaysPer100Years;
		// y4 = number of whole 4-year periods within 100-year period
		int y4 = n / DaysPer4Years;
		// n = day number within 4-year period
		n -= y4 * DaysPer4Years;
		// y1 = number of whole years within 4-year period
		int y1 = n / DaysPerYear;
		// Last year has an extra day, so decrement result if 4
		if (y1 == 4) y1 = 3;
		// If year was requested, compute and return it
		if (part == DatePartYear) {
			return y400 * 400 + y100 * 100 + y4 * 4 + y1 + 1;
		}
		// n = day number within year
		n -= y1 * DaysPerYear;
		// If day-of-year was requested, return it
		if (part == DatePartDayOfYear) return n + 1;
		// Leap year calculation looks different from IsLeapYear since y1, y4,
		// and y100 are relative to year 1, not year 0
		bool leapYear = y1 == 3 && (y4 != 24 || y100 == 3);
		const int* days = leapYear ? DaysToMonth366 : DaysToMonth365;
		// All months have less than 32 days, so n >> 5 is a good conservative
		// estimate for the month
		int m = (n >> 5) + 1;
		// m = 1-based month number
		while (n >= days[m]) m++;
		// If month was requested, return it
		if (part == DatePartMonth) return m;
		// Return 1-based day-of-month
		return n - days[m - 1] + 1;
	}

	bool DateTime::parse(const String& str, DateTime& dateTime)
	{
		if (str.isNullOrEmpty())
			return false;

		const int MaxCount = 16;
		char temp[MaxCount];

		int year = 0, month = 0, day = 0, hour = 0, minute = 0, second = 0, millisecond = 0;
		const char* buffer = str.c_str();
		int yearIndex = (int)((string)buffer).find('-');
		if (yearIndex > 0)
		{
			// include year.
			memset(temp, 0, sizeof(temp));
			strncpy(temp, buffer, Math::min(yearIndex, MaxCount));
			Convert::parseInt32(temp, year);

			buffer += yearIndex + 1;

			int monthIndex = (int)((string)buffer).find('-');
			if (monthIndex > 0)
			{
				// include month.
				memset(temp, 0, sizeof(temp));
				strncpy(temp, buffer, Math::min(monthIndex, MaxCount));
				Convert::parseInt32(temp, month);

				buffer += monthIndex + 1;

				int dayIndex = (int)((string)buffer).find(' ');
				if (dayIndex > 0)
				{
					// include day.
					memset(temp, 0, sizeof(temp));
					strncpy(temp, buffer, Math::min(dayIndex, MaxCount));
					Convert::parseInt32(temp, day);

					buffer += dayIndex + 1;
				}
				else
				{
					// can not find space.
					Convert::parseInt32(buffer, day);

					buffer += strlen(buffer);
				}
			}
		}
		if (buffer == NULL)
			return false;

		if (strlen(buffer) > 0)
		{
			TimeSpan timeSpan;
			if (!TimeSpan::parse(buffer, timeSpan))
				return false;

			hour = timeSpan.hours();
			minute = timeSpan.minutes();
			second = timeSpan.seconds();
			millisecond = timeSpan.milliseconds();
		}

		dateTime._dateData = toTicks(year, month, day, hour, minute, second, millisecond);

		return true;
	}
	String DateTime::toString(const Format format) const
	{
		String result = "";
		char temp[64];
		memset(temp, 0, sizeof(temp));
		switch (format)
		{
		case YYYYMMDDHHMMSSfff:
		{
			sprintf(temp, "%04d-%02d-%02d %02d:%02d:%02d.%03d", year(), month(), day(), hour(), minute(), second(), millisecond());
			result = temp;
			break;
		}
		case YYYYMMDDHHMMSS:
		{
			sprintf(temp, "%04d-%02d-%02d %02d:%02d:%02d", year(), month(), day(), hour(), minute(), second());
			result = temp;
			break;
		}
		case YYYYMMDDHHMM:
		{
			sprintf(temp, "%04d-%02d-%02d %02d:%02d", year(), month(), day(), hour(), minute());
			result = temp;
			break;
		}
		case YYYYMMDD:
		{
			sprintf(temp, "%04d-%02d-%02d", year(), month(), day());
			result = temp;
			break;
		}
		case HHMMSS:
		{
			sprintf(temp, "%02d:%02d:%02d", hour(), minute(), second());
			result = temp;
			break;
		}
		case HHMM:
		{
			sprintf(temp, "%02d:%02d", hour(), minute());
			result = temp;
			break;
		}
		case HHMMSSfff:
		{
			sprintf(temp, "%02d:%02d:%02d.%03d", hour(), minute(), second(), millisecond());
			result = temp;
			break;
		}
		default:
			break;
		}
		return result;
	}

	void DateTime::writeBCDDateTime(Stream* stream, bool includedSec, bool includedMs) const
	{
		// such like YYYYMMDDHHmmss or YYYYMMDDHHSS
		int len = includedSec ? 7 : 6;
		if (includedMs)
		{
			len += 2;
		}
		byte* buffer = new byte[len];
		memset(buffer, 0, len);

		int offset = 0;
		BCDUtilities::Int64ToBCD(year(), buffer + offset, 2);
		offset += 2;
		buffer[offset++] = BCDUtilities::ByteToBCD((byte)month());
		buffer[offset++] = BCDUtilities::ByteToBCD((byte)day());
		buffer[offset++] = BCDUtilities::ByteToBCD((byte)hour());
		buffer[offset++] = BCDUtilities::ByteToBCD((byte)minute());
		if (includedSec)
		{
			buffer[offset++] = BCDUtilities::ByteToBCD((byte)second());
		}
		if (includedMs)
		{
			BCDUtilities::Int64ToBCD(millisecond(), buffer + offset, 2);
			offset += 2;
		}

		stream->write(buffer, 0, len);
		delete[] buffer;
	}
	void DateTime::readBCDDateTime(Stream* stream, bool includedSec, bool includedMs)
	{
		// such like YYYYMMDDHHmmss or YYYYMMDDHHSS
		int len = includedSec ? 7 : 6;
		if (includedMs)
		{
			len += 2;
		}
		byte* buffer = new byte[len];
		memset(buffer, 0, len);
		stream->read(buffer, 0, len);
		bool isNull = true;
		for (uint i = 0; i < sizeof(buffer); i++)
		{
			if (buffer[i] != 0)
			{
				isNull = false;
				break;
			}
		}

		if (isNull)
		{
			_dateData = 0;
		}
		else
		{
			int offset = 0;
			int year = (int)BCDUtilities::BCDToInt64(buffer, offset, 2);
			offset += 2;
			int month = (int)BCDUtilities::BCDToInt64(buffer, offset, 1);
			offset += 1;
			int day = (int)BCDUtilities::BCDToInt64(buffer, offset, 1);
			offset += 1;

			int hour = (int)BCDUtilities::BCDToInt64(buffer, offset, 1);
			offset += 1;
			int minute = (int)BCDUtilities::BCDToInt64(buffer, offset, 1);
			offset += 1;
			int second = 0;
			if (includedSec)
			{
				second = (int)BCDUtilities::BCDToInt64(buffer, offset, 1);
				offset += 1;
			}
			int millisecond = 0;
			if (includedMs)
			{
				millisecond = (int)BCDUtilities::BCDToInt64(buffer, offset, 2);
				offset += 2;
			}
			_dateData = toTicks(year, month, day, hour, minute, second, millisecond);
		}
		delete[] buffer;
	}
	void DateTime::writeBCDDate(Stream* stream) const
	{
		// such like YYYYMMDD
		byte buffer[4];
		memset(buffer, 0, sizeof(buffer));

		int offset = 0;
		BCDUtilities::Int64ToBCD(year(), buffer + offset, 2);
		offset += 2;
		buffer[offset++] = BCDUtilities::ByteToBCD((byte)month());
		buffer[offset++] = BCDUtilities::ByteToBCD((byte)day());

		stream->write(buffer, 0, sizeof(buffer));
	}
	void DateTime::readBCDDate(Stream* stream)
	{
		byte buffer[4];
		memset(buffer, 0, sizeof(buffer));
		stream->read(buffer, 0, sizeof(buffer));
		bool isNull = true;
		for (uint i = 0; i < sizeof(buffer); i++)
		{
			if (buffer[i] != 0)
			{
				isNull = false;
				break;
			}
		}
		if (isNull)
		{
			_dateData = 0;
		}
		else
		{
			int offset = 0;
			int year = (int)BCDUtilities::BCDToInt64(buffer, offset, 2);
			offset += 2;
			int month = (int)BCDUtilities::BCDToInt64(buffer, offset, 1);
			offset += 1;
			int day = (int)BCDUtilities::BCDToInt64(buffer, offset, 1);
			offset += 1;

			_dateData = toTicks(year, month, day);
		}
	}
	void DateTime::writeBCDTime(Stream* stream) const
	{
		// such like HHmmss
		byte buffer[3];
		memset(buffer, 0, sizeof(buffer));

		int offset = 0;
		buffer[offset++] = BCDUtilities::ByteToBCD((byte)hour());
		buffer[offset++] = BCDUtilities::ByteToBCD((byte)minute());
		buffer[offset++] = BCDUtilities::ByteToBCD((byte)second());

		stream->write(buffer, 0, sizeof(buffer));
	}
	void DateTime::readBCDTime(Stream* stream)
	{
		// such like HHmmss
		byte buffer[3];
		memset(buffer, 0, sizeof(buffer));
		stream->read(buffer, 0, sizeof(buffer));
		bool isNull = true;
		for (uint i = 0; i < sizeof(buffer); i++)
		{
			if (buffer[i] != 0)
			{
				isNull = false;
				break;
			}
		}
		if (isNull)
		{
			_dateData = 0;
		}
		else
		{
			int offset = 0;
			int hour = (int)BCDUtilities::BCDToInt64(buffer, offset, 1);
			offset += 1;
			int minute = (int)BCDUtilities::BCDToInt64(buffer, offset, 1);
			offset += 1;
			int second = 0;
			second = (int)BCDUtilities::BCDToInt64(buffer, offset, 1);
			offset += 1;

			_dateData = toTicks(1, 1, 1, hour, minute, second);
		}
	}
	void DateTime::write(Stream* stream, bool bigEndian) const
	{
		stream->writeUInt64(_dateData, bigEndian);
	}
	void DateTime::read(Stream* stream, bool bigEndian)
	{
		_dateData = stream->readUInt64(bigEndian);
	}

	void DateTime::operator=(const DateTime& value)
	{
		_dateData = value._dateData;
	}
	bool DateTime::operator==(const DateTime& value) const
	{
		return (internalTicks() == value.internalTicks());
	}
	bool DateTime::operator!=(const DateTime& value) const
	{
		return !operator==(value);
	}
	bool DateTime::operator>=(const DateTime& value) const
	{
		return (internalTicks() >= value.internalTicks());
	}
	bool DateTime::operator>(const DateTime& value) const
	{
		return (internalTicks() > value.internalTicks());
	}
	bool DateTime::operator<=(const DateTime& value) const
	{
		return (internalTicks() <= value.internalTicks());
	}
	bool DateTime::operator<(const DateTime& dateTime) const
	{
		return (internalTicks() < dateTime.internalTicks());
	}

	DateTime DateTime::operator+(const TimeSpan& value) const
	{
		int64_t ticks = (int64_t)internalTicks();
		int64_t valueTicks = (int64_t)value.ticks();
		if (valueTicks > MaxTicks - ticks || valueTicks < MinTicks - ticks)
		{
			throw ArgumentOutOfRangeException("timeSpan", "ArgumentOutOfRange_DateArithmetic");
		}
		return DateTime((ticks + valueTicks) | internalKind());
	}
	DateTime DateTime::operator-(const TimeSpan& value) const
	{
		int64_t ticks = (int64_t)internalTicks();
		int64_t valueTicks = (int64_t)value.ticks();
		if (ticks - MinTicks < valueTicks || ticks - MaxTicks > valueTicks)
		{
			throw ArgumentOutOfRangeException("timeSpan", "ArgumentOutOfRange_DateArithmetic");
		}
		return DateTime((ticks - valueTicks) | internalKind());
	}
	TimeSpan DateTime::operator-(const DateTime& dateTime) const
	{
		return TimeSpan(internalTicks() - dateTime.internalTicks());
	}

	// Returns the number of days in the month given by the year and
	// month arguments.
	//
	int DateTime::daysInMonth(int year, int month) {
		if (month < 1 || month > 12) throw ArgumentOutOfRangeException("month", "ArgumentOutOfRange_Month");
		// IsLeapYear checks the year argument
		const int* days = isLeapYear(year) ? DaysToMonth366 : DaysToMonth365;
		return days[month] - days[month - 1];
	}

	// Returns the DateTime resulting from adding the given
	// TimeSpan to this DateTime.
	//
	DateTime DateTime::add(const TimeSpan& value) const
    {
		return addTicks(value._ticks);
	}

	// Returns the DateTime resulting from adding a fractional number of
	// time units to this DateTime.
	DateTime DateTime::add(double value, int scale) const
    {
		int64_t millis = (int64_t)(value * scale + (value >= 0 ? 0.5 : -0.5));
		if (millis <= -MaxMillis || millis >= MaxMillis)
			throw ArgumentOutOfRangeException("value", "ArgumentOutOfRange_addValue");
		return addTicks(millis * TicksPerMillisecond);
	}

	// Returns the DateTime resulting from adding a fractional number of
	// days to this DateTime. The result is computed by rounding the
	// fractional number of days given by value to the nearest
	// millisecond, and adding that interval to this DateTime. The
	// value argument is permitted to be negative.
	//
	DateTime DateTime::addDays(double value) {
		return add(value, MillisPerDay);
	}

	// Returns the DateTime resulting from adding a fractional number of
	// hours to this DateTime. The result is computed by rounding the
	// fractional number of hours given by value to the nearest
	// millisecond, and adding that interval to this DateTime. The
	// value argument is permitted to be negative.
	//
	DateTime DateTime::addHours(double value) {
		return add(value, MillisPerHour);
	}

	// Returns the DateTime resulting from the given number of
	// milliseconds to this DateTime. The result is computed by rounding
	// the number of milliseconds given by value to the nearest integer,
	// and adding that interval to this DateTime. The value
	// argument is permitted to be negative.
	//
	DateTime DateTime::addMilliseconds(double value) {
		return add(value, 1);
	}

	// Returns the DateTime resulting from adding a fractional number of
	// minutes to this DateTime. The result is computed by rounding the
	// fractional number of minutes given by value to the nearest
	// millisecond, and adding that interval to this DateTime. The
	// value argument is permitted to be negative.
	//
	DateTime DateTime::addMinutes(double value) {
		return add(value, MillisPerMinute);
	}

	// Returns the DateTime resulting from adding the given number of
	// months to this DateTime. The result is computed by incrementing
	// (or decrementing) the year and month parts of this DateTime by
	// months months, and, if required, adjusting the day part of the
	// resulting date downwards to the last day of the resulting month in the
	// resulting year. The time-of-day part of the result is the same as the
	// time-of-day part of this DateTime.
	//
	// In more precise terms, considering this DateTime to be of the
	// form y / m / d + t, where y is the
	// year, m is the month, d is the day, and t is the
	// time-of-day, the result is y1 / m1 / d1 + t,
	// where y1 and m1 are computed by adding months months
	// to y and m, and d1 is the largest value less than
	// or equal to d that denotes a valid day in month m1 of year
	// y1.
	//
	DateTime DateTime::addMonths(int months) {
		if (months < -120000 || months > 120000) throw ArgumentOutOfRangeException("months", "ArgumentOutOfRange_DateTimeBadMonths");
		int y = getDatePart(DatePartYear);
		int m = getDatePart(DatePartMonth);
		int d = getDatePart(DatePartDay);
		int i = m - 1 + months;
		if (i >= 0) {
			m = i % 12 + 1;
			y = y + i / 12;
		}
		else {
			m = 12 + (i + 1) % 12;
			y = y + (i - 11) / 12;
		}
		if (y < 1 || y > 9999) {
			throw ArgumentOutOfRangeException("months", "ArgumentOutOfRange_DateArithmetic");
		}
		int days = daysInMonth(y, m);
		if (d > days) d = days;
		return DateTime((uint64_t)(dateToTicks(y, m, d) + internalTicks() % TicksPerDay) | internalKind());
	}

	// Returns the DateTime resulting from adding a fractional number of
	// seconds to this DateTime. The result is computed by rounding the
	// fractional number of seconds given by value to the nearest
	// millisecond, and adding that interval to this DateTime. The
	// value argument is permitted to be negative.
	//
	DateTime DateTime::addSeconds(double value) {
		return add(value, MillisPerSecond);
	}

	// Returns the DateTime resulting from adding the given number of
	// 100-nanosecond ticks to this DateTime. The value argument
	// is permitted to be negative.
	//
	DateTime DateTime::addTicks(int64_t value) const
    {
		int64_t ticks = (int64_t)internalTicks();
		if (value > MaxTicks - ticks || value < MinTicks - ticks) {
			throw ArgumentOutOfRangeException("value", "ArgumentOutOfRange_DateArithmetic");
		}
		return DateTime((uint64_t)(ticks + value) | internalKind());
	}

	// Returns the DateTime resulting from adding the given number of
	// years to this DateTime. The result is computed by incrementing
	// (or decrementing) the year part of this DateTime by value
	// years. If the month and day of this DateTime is 2/29, and if the
	// resulting year is not a leap year, the month and day of the resulting
	// DateTime becomes 2/28. Otherwise, the month, day, and time-of-day
	// parts of the result are the same as those of this DateTime.
	//
	DateTime DateTime::addYears(int value) {
		if (value < -10000 || value > 10000) throw ArgumentOutOfRangeException("years", "ArgumentOutOfRange_DateTimeBadYears");
		return addMonths(value * 12);
	}
    
    uint DateTime::subtract(DateTime prev, Resolutions tr) const
    {
        DateTime value = *this;
        if (value < prev)
        {
            throw ArgumentException("value must be greater than prev", "prevTime");
        }
        
        if (tr == Resolutions::ResNone)
        {
            throw ArgumentException("Resolutions must not be equal to None", "tr");
        }
        
        uint time = 0;
        switch (tr)
        {
            case Resolutions::ResMillisecond:
                time = (uint)(value - prev).totalMilliseconds();
                break;
            case Resolutions::ResSecond:
                time = (uint)(value - prev).totalSeconds();
                break;
            case Resolutions::ResMinute:
                time = (uint)(value - prev).totalMinutes();
                break;
            case Resolutions::ResHour:
                time = (uint)(value - prev).totalHours();
                break;
            default:
                assert(false);
                break;
        }
        return time;
    }
    
    DateTime DateTime::add(uint time, Resolutions tr) const
    {
        DateTime value = *this;
        if (tr == Resolutions::ResNone)
        {
            throw ArgumentException("Resolutions must not be equal to None", "tr");
        }
        
        DateTime result = MinValue;
        switch (tr)
        {
            case Resolutions::ResMillisecond:
                result = value.addMilliseconds(time);
                break;
            case Resolutions::ResSecond:
                result = value.addSeconds(time);
                break;
            case Resolutions::ResMinute:
                result = value.addMinutes(time);
                break;
            case Resolutions::ResHour:
                result = value.addHours(time);
                break;
            default:
                assert(false);
                break;
        }
        return result;
    }
    
    DateTime::Resolutions DateTime::fromResolutionStr(const String& str)
    {
        Resolutions tr = Resolutions::ResSecond;
        if(str.equals("Second", true))
        {
            tr = Resolutions::ResSecond;
        }
        else if(str.equals("Millisecond", true))
        {
            tr = Resolutions::ResMillisecond;
        }
        else if(str.equals("Minute", true))
        {
            tr = Resolutions::ResMinute;
        }
        else if(str.equals("Hour", true))
        {
            tr = Resolutions::ResHour;
        }
        return tr;
    }
    String DateTime::toResolutionStr(Resolutions tr)
    {
        switch (tr)
        {
            case Resolutions::ResMillisecond:
                return "Millisecond";
            case Resolutions::ResSecond:
                return "Second";
            case Resolutions::ResMinute:
                return "Minute";
            case Resolutions::ResHour:
                return "Hour";
            default:
				return "None";
                break;
        }
    }
}
