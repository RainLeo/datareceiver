//
//  Application.cpp
//  common
//
//  Created by baowei on 15/4/18.
//  Copyright (c) 2015 com. All rights reserved.
//

#include "Application.h"
#include "common/diag/Trace.h"
#include "common/IO/Path.h"
#include "common/IO/Directory.h"

namespace Common
{
    Application* Application::_instance = NULL;
    bool Application::_phoneApplication = false;
    Application::Application() : Application(0, NULL)
    {
    }
    Application::Application(int argc, const char * argv[])
    {
        if(!_phoneApplication)
        {
            string rootPath = Directory::getAppDirPath();
            if(!rootPath.empty())
            {
                initLog(rootPath);
            }
            _rootPath = rootPath;
        }
        _instance = this;
        _phoneApplication = false;
    }
    Application::~Application()
    {
        unInitLog();
    }
    
    Application* Application::instance()
    {
        return _instance;
    }
    
    string Application::rootPath() const
    {
        return _rootPath;
    }
    
    const Culture& Application::culture() const
    {
        return _culture;
    }
    void Application::setCulture(const Culture& culture)
    {
        _culture = culture;
    }
    
    void Application::initLog(const string& rootPath, const string& logPath)
    {
        _logListener = new LogTraceListener(Path::combine(rootPath, logPath));
        Trace::enableLog(_logListener);
        Trace::enableConsoleOutput();
    }
    void Application::unInitLog()
    {
        if(_logListener != NULL)
        {
            Trace::disableLog();
            Trace::disableConsoleOutput();
            
            delete _logListener;
            _logListener = NULL;
        }
    }
    
    PhoneApplication* PhoneApplication::_instance = NULL;
    PhoneApplication::PhoneApplication()
    {
    }
    PhoneApplication::~PhoneApplication()
    {
        unInitLog();
    }
    
    PhoneApplication* PhoneApplication::instance()
    {
        _phoneApplication = true;
        
        if(_instance == NULL)
        {
            _instance = new PhoneApplication();
        }
        return _instance;
    }
    
    void PhoneApplication::onCreate(const string& rootPath)
    {
        assert(!rootPath.empty());
        _rootPath = rootPath;
        
        initLog(_rootPath);
        
        invokeEvent(EventCreate);
    }
    void PhoneApplication::onDestroy()
    {
        unInitLog();
        
        invokeEvent(EventDestroy);
    }
    void PhoneApplication::onActived()
    {
        invokeEvent(EventActived);
    }
    void PhoneApplication::onInactived()
    {
        invokeEvent(EventInactived);
    }
    void PhoneApplication::onActiving()
    {
        invokeEvent(EventActiving);
    }
    void PhoneApplication::onInactiving()
    {
        invokeEvent(EventInactiving);
    }
    
    void PhoneApplication::addEventCreate(const Delegate& delegate)
    {
        addEvent(EventCreate, delegate);
    }
    void PhoneApplication::removeEventCreate(const Delegate& delegate)
    {
        removeEvent(EventCreate, delegate);
    }
    void PhoneApplication::addEventActived(const Delegate& delegate)
    {
        addEvent(EventActived, delegate);
    }
    void PhoneApplication::removeEventActived(const Delegate& delegate)
    {
        removeEvent(EventActived, delegate);
    }
    void PhoneApplication::addEventInactived(const Delegate& delegate)
    {
        addEvent(EventInactived, delegate);
    }
    void PhoneApplication::removeEventInactived(const Delegate& delegate)
    {
        removeEvent(EventInactived, delegate);
    }
    
    void PhoneApplication::addEvent(EventType type, const Delegate& delegate)
    {
        _delegates[type].add(delegate);
    }
    void PhoneApplication::removeEvent(EventType type, const Delegate& delegate)
    {
        _delegates[type].remove(delegate);
    }
    void PhoneApplication::invokeEvent(EventType type)
    {
        _delegates[type].invoke(this);
    }
}
