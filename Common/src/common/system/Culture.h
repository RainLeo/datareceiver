//
//  Culture.h
//  common
//
//  Created by baowei on 15/8/29.
//  Copyright (c) 2015 com. All rights reserved.
//

#ifndef __common__Culture__
#define __common__Culture__

#include "common/common_global.h"

namespace Common
{
    class COMMON_EXPORT Culture
    {
    public:
        struct Data
        {
            uint id;
            string name;
            
            Data(uint id=0x0009, const string& name="en")
            {
                this->id = id;
                this->name = name;
            }
        };
        
        Culture();
        Culture(const string& name);
        Culture(uint id);
        ~Culture();
        
        const string& name() const;
        uint id() const;

    private:
        Data _data;
        
        const static int AllCulturesCount = 4;
        const static Culture::Data AllCultures[AllCulturesCount];
    };
}

#endif /* defined(__common__Culture__) */
