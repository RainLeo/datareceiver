#include "Convert.h"
#include <stdarg.h>
#include <iconv.h>
#if !WIN32
#include <sys/time.h>
#endif
#include <time.h>

namespace Common
{
#if WIN32
	const char* Convert::NewLine = "\r\n";
#else
	const char* Convert::NewLine = "\n";
#endif
	const char* Convert::Empty = "";

	const char Convert::base64Table[65] = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
		'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd',
		'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
		't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7',
		'8', '9', '+', '/', '=' };

	bool Convert::getDateTimeStr(char* buffer, const system_clock::time_point& timep, bool includems)
	{
		time_t timep_c = system_clock::to_time_t(timep);
		if (timep_c > 0)
		{
			char temp[64];
			struct tm* tmp = localtime(&timep_c);
			if (!includems)
			{
				sprintf(temp, "%04d-%02d-%02d %02d:%02d:%02d", tmp->tm_year + 1900, tmp->tm_mon + 1, tmp->tm_mday,
					tmp->tm_hour, tmp->tm_min, tmp->tm_sec);
			}
			else
			{
				auto currentTimeRounded = system_clock::from_time_t(timep_c);
				if (currentTimeRounded > timep) {
					currentTimeRounded -= std::chrono::seconds(1);
				}
				int milliseconds = duration_cast<duration<int, std::milli> >(timep - currentTimeRounded).count();

				sprintf(temp, "%04d-%02d-%02d %02d:%02d:%02d.%03d", tmp->tm_year + 1900, tmp->tm_mon + 1, tmp->tm_mday,
					tmp->tm_hour, tmp->tm_min, tmp->tm_sec, milliseconds);
			}

			strcpy(buffer, temp);
			return true;
		}
		return false;
	}
	string Convert::getDateTimeStr(const system_clock::time_point& timep, bool includems)
	{
		char temp[64];
		if (getDateTimeStr(temp, timep, includems))
		{
			return temp;
		}
		return "";
	}
	bool Convert::getDateTimeStr(char* buffer, const time_t timep)
	{
		return getDateTimeStr(buffer, system_clock::from_time_t(timep), false);
	}
	string Convert::getDateTimeStr(const time_t timep)
	{
		return getDateTimeStr(system_clock::from_time_t(timep), false);
	}
	string Convert::getTimeStr(const time_t timep, bool includeSec)
	{
		if(timep > 0)
		{
			char temp[16];
            struct tm* tmp = localtime(&timep);
			if(!includeSec)
			{
				sprintf(temp, "%02d:%02d", tmp->tm_hour, tmp->tm_min);
			}
			else
			{
				sprintf(temp, "%02d:%02d:%02d", tmp->tm_hour, tmp->tm_min, tmp->tm_sec);
			}
			return temp;
		}
		return "";
	}
	string Convert::getTimeStr(const system_clock::time_point& timep, bool includeSec)
	{
		time_t timep_c = system_clock::to_time_t(timep);
		return getTimeStr(timep_c);
	}
	string Convert::getTimeHMSStr(const time_t timep)
	{
		if(timep > 0)
		{
			char temp[16];
            struct tm* tmp = localtime(&timep);
			sprintf(temp, "%02d:%02d:%02d", tmp->tm_hour, tmp->tm_min, tmp->tm_sec);
			return temp;
		}
		return "00:00";
	}
	string Convert::getTimeHMStr(const time_t timep)
	{
		if(timep > 0)
		{
			char temp[16];
            struct tm* tmp = localtime(&timep);
			sprintf(temp, "%02d:%02d", tmp->tm_hour, tmp->tm_min);
			return temp;
		}
		return "00:00";
	}
	string Convert::getTimeMSStr(const time_t timep)
	{
		if(timep > 0)
		{
			char temp[16];
            struct tm* tmp = localtime(&timep);
			sprintf(temp, "%02d:%02d", tmp->tm_min, tmp->tm_sec);
			return temp;
		}
		return "00:00";
	}

	time_t Convert::combineTime(int year, int month, int day, int hour, int minute, int second)
	{
		struct tm tmp;
		memset(&tmp, 0, sizeof(tmp));
		tmp.tm_year = year - 1900;
		tmp.tm_mon = month - 1;
		tmp.tm_mday = day;
		tmp.tm_hour = hour;
		tmp.tm_min = minute;
		tmp.tm_sec = second;
		return mktime(&tmp);
	}

	string Convert::getCurrentTimeStr(bool includems)
	{
		return getDateTimeStr(system_clock::now(), includems);
	}
	string Convert::getCurrentDateStr()
	{
		time_t timep = system_clock::to_time_t(system_clock::now());
		return getDateStr(timep);
	}
	time_t Convert::parseDateTime(const string& timeStr, bool fullFormat)
	{
        bool success = false;
		int year, month, day, hour, minute, second;
		if(fullFormat)
		{
			int result = sscanf(timeStr.c_str(), "%04d-%02d-%02d %02d:%02d:%02d", &year, &month, &day, &hour, &minute, &second);
			if(result == 6)
			{
				success = true;
			}
		}
		else
		{
			int result = sscanf(timeStr.c_str(), "%d-%d-%d %d:%d:%d", &year, &month, &day, &hour, &minute, &second);
			if(result == 6)
			{
				success = true;
			}
		}
        if(success)
        {
			return combineTime(year, month, day, hour, minute, second);
        }
		return 0;
	}
	time_t Convert::parseDateTime(const char* timeStr, bool fullFormat)
	{
		return parseDateTime(string(timeStr), fullFormat);
	}
	bool Convert::isDateTime(const string& timeStr)
	{
		int year, month, day, hour, minute, second;
		int result = sscanf(timeStr.c_str(), "%d-%d-%d %d:%d:%d", &year, &month, &day, &hour, &minute, &second);
		if(result == 6)
		{
			if(!(year >= 0 && year <= 9999))
				return false;
            
			return combineTime(year, month, day, hour, minute, second) > 0;
		}
		return false;
	}
	bool Convert::isDateTime(const char* timeStr)
	{
		return isDateTime(string(timeStr));
	}

	time_t Convert::parseDate(const string& dateStr)
	{
		int year, month, day;
		int result = sscanf(dateStr.c_str(), "%04d-%02d-%02d", &year, &month, &day);
		if(result == 3)
		{
			return combineTime(year, month, day);
		}
		return 0;
	}

	string Convert::getDateStr(const time_t timep)
	{
		if(timep > 0)
		{
			char temp[255];
            struct tm* tmp = localtime(&timep);
			sprintf(temp, "%04d-%02d-%02d", tmp->tm_year + 1900, tmp->tm_mon + 1, tmp->tm_mday);
			return temp;
		}
		return "";
	}
	string Convert::getDateStr(const system_clock::time_point& timep)
	{
		time_t timep_c = system_clock::to_time_t(timep);
		return getDateStr(timep_c);
	}

	void Convert::replaceStr(string& strBig, const string& strsrc, const string& strdst)
	{
		std::string::size_type pos = 0;
		while( (pos = strBig.find(strsrc, pos)) != string::npos)
		{
			strBig.replace(pos, strsrc.length(), strdst);
			pos += strdst.length();
		}
	}

	string Convert::convertStr(const char *format, ...)
	{
		char* message = new char[MaxFormatStrLength];
		memset(message, 0, MaxFormatStrLength);
		va_list ap;
		va_start(ap, format);
		vsprintf(message, format, ap);
		va_end(ap);
		string result = message;
		delete[] message;
		return result;
	}
	string Convert::convertStr(int64_t value)
	{
		char temp[64];
		sprintf(temp, "%lld", value);
		return temp;
	}
	string Convert::convertStr(uint64_t value)
	{
		char temp[64];
		sprintf(temp, "%lld", value);
		return temp;
	}
	string Convert::convertStr(int value)
	{
		char temp[255];
		sprintf(temp, "%d", value);
		return temp;
	}
	string Convert::convertStr(bool value)
	{
		return value ? "true" : "false";
	}
	string Convert::convertStr(uint value)
	{
		char temp[255];
		sprintf(temp, "%d", value);
		return temp;
	}
	string Convert::convertStr(float value, int pointSize)
	{
		char temp[255];
		if(pointSize >=0 && pointSize <= 6)
		{
			sprintf(temp, "%.*f", pointSize, value);
		}
		else
		{
			sprintf(temp, "%f", value);
		}
		return temp;
	}
	bool Convert::convertStr(float value, char* str, int pointSize)
	{
		char temp[255];
		if(pointSize >=0 && pointSize <= 6)
		{
			sprintf(temp, "%.*f", pointSize, value);
		}
		else
		{
			sprintf(temp, "%f", value);
		}
		strcpy(str, temp);
		return true;
	}
	string Convert::convertStr(double value, int pointSize)
	{
		char temp[255];
		if(pointSize >=0 && pointSize <= 15)
		{
			sprintf(temp, "%.*lf", pointSize, value);
		}
		else
		{
			sprintf(temp, "%lf", value);
		}
		return temp;
	}

	bool Convert::parseInt64(const string& text, int64_t& value, bool decimal)
	{
		uint len = 0;
        if (sscanf(text.c_str(), decimal && !isHexInteger(text) ? "%lld%n" : "%llx%n", &value, &len) == 1 && text.length() == len)
		{
			return true;
		}
		return false;
	}
	bool Convert::parseUInt64(const string& text, uint64_t& value, bool decimal)
	{
		uint len = 0;
        if (sscanf(text.c_str(), decimal && !isHexInteger(text) ? "%lld%n" : "%llx%n", &value, &len) == 1 && text.length() == len)
		{
			return true;
		}
		return false;
	}

	bool Convert::parseInt32(const string& text, int& value, bool decimal)
	{
		uint len = 0;
        if(sscanf(text.c_str(), decimal && !isHexInteger(text) ? "%d%n" : "%x%n", &value, &len) == 1 && text.length() == len)
		{
			return true;
		}
		return false;
	}
	bool Convert::parseUInt32(const string& text, uint& value, bool decimal)
	{
		uint len = 0;
		if(sscanf(text.c_str(), decimal && !isHexInteger(text) ? "%d%n" : "%x%n", &value, &len) == 1 && text.length() == len)
		{
			return true;
		}
		return false;
	}

	bool Convert::parseInt16(const string& text, short& value, bool decimal)
	{
		uint len = 0;
        if(sscanf(text.c_str(), decimal && !isHexInteger(text) ? "%hd%n" : "%hx%n", &value, &len) == 1 && text.length() == len)
		{
			return true;
		}
		return false;
	}
	bool Convert::parseUInt16(const string& text, ushort& value, bool decimal)
	{
		uint len = 0;
        if(sscanf(text.c_str(), decimal && !isHexInteger(text) ? "%hd%n" : "%hx%n", &value, &len) == 1 && text.length() == len)
		{
			return true;
		}
		return false;
	}
	bool Convert::parseByte(const string& text, byte& value, bool decimal)
	{
		ushort uValue;
		if(parseUInt16(text, uValue, decimal) && uValue <= 255)
		{
			value = (byte)uValue;
			return true;
		}
		return false;
	}
	bool Convert::parseBoolean(const string& text, bool& value)
	{
		if (stringEqual(text, "true", true))
		{
			value = true;
			return true;
		}
		else if (stringEqual(text, "false", true))
		{
			value = false;
			return true;
		}
		return false;
	}
	bool Convert::parseSingle(const string& text, float& value)
	{
		uint len = 0;
        // fixed bug, removed 'f' or 'F'
        string str = trimStr(text, 'f', 'F');
		if(sscanf(str.c_str(), "%f%n", &value, &len) == 1 && str.length() == len)
		{
			return true;
		}
		return false;
	}
	bool Convert::parseDouble(const string& text, double& value)
	{
		uint len = 0;
		if(sscanf(text.c_str(), "%lf%n", &value, &len) == 1 && text.length() == len)
		{
			return true;
		}
		return false;
	}
    
    bool Convert::parseStr(const string& text, float& value)
    {
        return parseSingle(text, value);
    }
    bool Convert::parseStr(const string& text, double& value)
    {
        return parseDouble(text, value);
    }
    
    bool Convert::parseStr(const string& text, int64_t& value, bool decimal)
    {
        return parseInt64(text, value, decimal);
    }
    bool Convert::parseStr(const string& text, uint64_t& value, bool decimal)
    {
        return parseUInt64(text, value, decimal);
    }
    
    bool Convert::parseStr(const string& text, int& value, bool decimal)
    {
        return parseInt32(text, value, decimal);
    }
    bool Convert::parseStr(const string& text, uint& value, bool decimal)
    {
        return parseUInt32(text, value, decimal);
    }
    
    bool Convert::parseStr(const string& text, short& value, bool decimal)
    {
        return parseInt16(text, value, decimal);
    }
    bool Convert::parseStr(const string& text, ushort& value, bool decimal)
    {
        return parseUInt16(text, value, decimal);
    }
    
    bool Convert::parseStr(const string& text, byte& value, bool decimal)
    {
        return parseByte(text, value, decimal);
    }
    bool Convert::parseStr(const string& text, bool& value)
    {
        return parseBoolean(text, value);
    }

	string Convert::convertIPStr(byte ip1, byte ip2, byte ip3, byte ip4)
	{
		char temp[255];
		sprintf(temp, "%d.%d.%d.%d", ip1, ip2, ip3, ip4);
		return temp;
	}
	string Convert::convertIPStr(const string& ip1, const string& ip2, const string& ip3, const string& ip4)
	{
		int i1, i2, i3, i4;
		if(Convert::parseInt32(ip1, i1) && (i1>=0 && i1<=255) &&
			Convert::parseInt32(ip2, i2) && (i2>=0 && i2<=255) &&
			Convert::parseInt32(ip3, i3) && (i3>=0 && i3<=255) &&
			Convert::parseInt32(ip4, i4) && (i4>=0 && i4<=255))
		{
			return convertIPStr(i1, i2, i3, i4);
		}
		return "";
	}
	string Convert::convertIPStr(uint ip)
	{
		byte ip1 = (ip >> 24) & 0xFF;
		byte ip2 = (ip >> 16) & 0xFF;
		byte ip3 = (ip >> 8) & 0xFF;
		byte ip4 = ip & 0xFF;
		return convertIPStr(ip1, ip2, ip3, ip4);
	}

	bool Convert::parseIPStr(const string& text, string& ip1, string& ip2, string& ip3, string& ip4)
	{
		uint nip1 = 0;
		uint nip2 = 0;
		uint nip3 = 0;
		uint nip4 = 0;
		if(sscanf(text.c_str(), "%d.%d.%d.%d", &nip1, &nip2, &nip3, &nip4) == 4 &&
				(nip1 <= 255 && nip2 <= 255 && nip3 <= 255 && nip4 <= 255))
		{
			ip1 = Convert::convertStr(nip1).c_str();
			ip2 = Convert::convertStr(nip2).c_str();
			ip3 = Convert::convertStr(nip3).c_str();
			ip4 = Convert::convertStr(nip4).c_str();
			return true;
		}
		return false;
	}
	bool Convert::parseIPStr(const string& text, byte& ip1, byte& ip2, byte& ip3, byte& ip4)
	{
		uint nip1 = 0;
		uint nip2 = 0;
		uint nip3 = 0;
		uint nip4 = 0;
		if(sscanf(text.c_str(), "%d.%d.%d.%d", &nip1, &nip2, &nip3, &nip4) == 4 &&
				(nip1 <= 255 && nip2 <= 255 && nip3 <= 255 && nip4 <= 255))
		{
			ip1 = (byte)nip1;
			ip2 = (byte)nip2;
			ip3 = (byte)nip3;
			ip4 = (byte)nip4;
			return true;
		}
		return false;
	}
	bool Convert::parseIPStr(const string& text, uint& ip)
	{
		byte ip1, ip2, ip3, ip4;
		if(Convert::parseIPStr(text, ip1, ip2, ip3, ip4))
		{
			ip = (ip1<<24)
				|(ip2<<16)
				|(ip3<<8)
				|ip4;

			return true;
		}
		return false;
	}

	void Convert::splitStr(const string& str, const char splitSymbol, StringArray& texts)
	{
		int size;
		string splitStr;
		string text = str;

		while(!text.empty())
		{
			size = (int)text.find(splitSymbol);
			if(size < 0)
			{
				splitStr = text;
				texts.add(text);
				return;
			}
			splitStr = text.substr(0, size);
			texts.add(splitStr);
			if(splitStr != text)
			{
				text = text.substr(size + 1, text.length() - size);
			}
		}
	}
    void Convert::splitStr(const string& str, StringArray& texts, const char splitSymbol)
    {
		splitStr(str, splitSymbol, texts);
    }
    void Convert::splitItems(const string& str, StringArray& texts, const char splitSymbol, const char escape, const char startRange, const char endRange)
    {
        int offset = 0;
        int range = 0;
        char prevCh = '\0';
        for (uint i = 0; i < str.length(); i++)
        {
            char ch = str[i];
            if (ch == splitSymbol && prevCh != escape)
            {
                if (range == 0)
                {
                    texts.add(str.substr(offset, i - offset));
                    offset = i + 1;     // skip ;
                }
            }
            else if (ch == startRange && prevCh != escape)
            {
                range++;
            }
            else if (ch == endRange && prevCh != escape)
            {
                range--;
            }
            prevCh = ch;
        }
        
        // process the last item.
        texts.add(str.substr(offset, str.length() - offset));
    }
    bool Convert::splitItems(const string& str, KeyPairs& pairs, const char splitSymbol, const char escape, const char startRange, const char endRange)
    {
        StringArray texts;
        Convert::splitItems(str, texts, splitSymbol, escape, startRange, endRange);
        if(texts.count() > 0)
        {
            for (uint i=0; i<texts.count(); i++)
            {
                StringArray values;
                Convert::splitItems(texts[i], values, ':');
                if(values.count() == 2)
                {
                    string name = Convert::trimStr(values[0], ' ', startRange, endRange);
                    string value = Convert::trimStr(values[1], ' ', startRange, endRange);
                    
                    if(!name.empty())
                    {
                        KeyPair* kp = new KeyPair(name, value);
                        pairs.add(kp);
                    }
                }
            }
            
            return pairs.count() > 0;
        }
        return false;
    }

	bool Convert::stringEqual(const string& a, const string& b, bool ignoreCase)
	{
		if (!ignoreCase)
		{
			return a.compare(b) == 0;
		}
		else
		{
#if WIN32
			return stricmp(a.c_str(), b.c_str()) == 0;
#else
			return strcasecmp(a.c_str(), b.c_str()) == 0;
#endif
			//unsigned int sz = a.size();
			//if (b.size() != sz)
			//	return false;
			//for (unsigned int i = 0; i < sz; ++i)
			//	if (tolower(a[i]) != tolower(b[i]))
			//		return false;
			//return true;
		}
	}

	const string Convert::toLower(const string& str)
	{
		if (str.empty())
			return str;

		uint len = (uint)str.length();
		char* result = new char[len + 1];
		memset(result, 0, len + 1);
		for (size_t i = 0; i < len; i++)
		{
			result[i] = (char)tolower(str[i]);
		}
		string resultStr = result;
		delete[] result;
		return resultStr;
	}
	const string Convert::toUpper(const string& str)
	{
		if (str.empty())
			return str;

		uint len = (uint)str.length();
		char* result = new char[len + 1];
		memset(result, 0, len + 1);
		for (size_t i = 0; i < len; i++)
		{
			result[i] = (char)toupper(str[i]);
		}
		string resultStr = result;
		delete[] result;
		return resultStr;
	}
	const string Convert::trimStr(const string& str, const char symbol1, const char symbol2, const char symbol3, const char symbol4)
	{
		StringBuilder sb;
		for (size_t i = 0; i < str.length(); i++)
		{
			char ch = str[i];
			if (!(ch == symbol1 || ch == symbol2 || ch == symbol3 || ch == symbol4))
				sb.append(ch);
		}
		return sb.toString();
	}
    const string Convert::trimStartStr(const string& str, const char symbol1, const char symbol2, const char symbol3, const char symbol4)
    {
        if(str.empty())
            return str;
        
        StringBuilder sb;
        for (size_t i = 0; i < str.length(); i++)
        {
            char ch = str[i];
            if (!((ch == symbol1 || ch == symbol2 || ch == symbol3 || ch == symbol4) && i == 0))
                sb.append(ch);
        }
        return sb.toString();
    }
    const string Convert::trimEndStr(const string& str, const char symbol1, const char symbol2, const char symbol3, const char symbol4)
    {
        if(str.empty())
            return str;
        
        StringBuilder sb;
        for (size_t i = 0; i < str.length(); i++)
        {
            char ch = str[i];
            if (!((ch == symbol1 || ch == symbol2 || ch == symbol3 || ch == symbol4) && i == str.length()-1))
                sb.append(ch);
        }
        return sb.toString();
    }

	const string Convert::GBKtoUTF8Str(const string& str)
	{
		return GBKtoUTF8Str(str.c_str());
	}
	const string Convert::GBKtoUTF8Str(const char* str)
	{
		return encodingToStr("GBK", "UTF-8", str);
	}

	const string Convert::UTF8toGBKStr(const string& str)
	{
		return UTF8toGBKStr(str.c_str());
	}
	const string Convert::UTF8toGBKStr(const char* str)
	{
		return encodingToStr("UTF-8", "GBK", str);
	}

	const string Convert::encodingToStr(const char* fromCode, const char* toCode, const char* str)
	{
		if (str == NULL || strlen(str) == 0)
			return "";

		string result;
		int length = 0;
		char* buffer = new char[strlen(str) * 4];
		if (encodingToStr(fromCode, toCode, str, buffer, length))
		{
			result = buffer;
		}
		else
		{
			result = str;
		}
		delete[] buffer;
		return result;
	}
	bool Convert::encodingToStr(const char* fromCode, const char* toCode, const char* str, char*& buffer, int& length)
	{
		if (str == NULL || strlen(str) == 0)
			return false;

		length = 0;
		iconv_t cd = iconv_open(toCode, fromCode);
		if (cd != (iconv_t)-1)
		{
			const char* inbuf = str;
			int inlen = (int)strlen(inbuf);
			char *outbuf = (char *)malloc(inlen * 4);
			memset(outbuf, 0, inlen * 4);
#if _LIBICONV_VERSION > 0x010B
			const char *in = inbuf;
#else
			char *in = (char*)inbuf;
#endif
			char *out = outbuf;
			size_t outlen = inlen * 4;
			iconv(cd, &in, (size_t *)&inlen, &out, &outlen);
			outlen = strlen(outbuf);

			strcpy(buffer, outbuf);
			length = (int)outlen;

			free(outbuf);
			iconv_close(cd);
		}
		return length > 0;
	}

	string Convert::toBase64String(const string& str)
	{
		int length = (int)str.length();
		if (length == 0)
			return "";

		return toBase64String((const byte*)str.c_str(), 0, length);
	}
	int Convert::toBase64_CalculateAndValidateOutputLength(int inputLength, bool insertLineBreaks)
	{
		long outlen = ((long)inputLength) / 3 * 4;          // the base length - we want integer division here. 
		outlen += ((inputLength % 3) != 0) ? 4 : 0;         // at most 4 more chars for the remainder

		if (outlen == 0)
			return 0;

		if (insertLineBreaks) {
			long newLines = outlen / base64LineBreakPosition;
			if ((outlen % base64LineBreakPosition) == 0) {
				--newLines;
			}
			outlen += newLines * 2;              // the number of line break chars we'll add, "\r\n"
		}

		return (int)outlen;
	}
	int Convert::convertToBase64Array(char* outChars, const byte* inData, int offset, int length, bool insertLineBreaks)
	{
		int lengthmod3 = length % 3;
		int calcLength = offset + (length - lengthmod3);
		int j = 0;
		int charcount = 0;
		//Convert three bytes at a time to base64 notation.  This will consume 4 chars.
		int i;

		// get a pointer to the base64Table to avoid unnecessary range checking
		const char* base64 = base64Table;
		for (i = offset; i < calcLength; i += 3)
		{
			if (insertLineBreaks) {
				if (charcount == base64LineBreakPosition) {
					outChars[j++] = '\r';
					outChars[j++] = '\n';
					charcount = 0;
				}
				charcount += 4;
			}
			outChars[j] = base64[(inData[i] & 0xfc) >> 2];
			outChars[j + 1] = base64[((inData[i] & 0x03) << 4) | ((inData[i + 1] & 0xf0) >> 4)];
			outChars[j + 2] = base64[((inData[i + 1] & 0x0f) << 2) | ((inData[i + 2] & 0xc0) >> 6)];
			outChars[j + 3] = base64[(inData[i + 2] & 0x3f)];
			j += 4;
		}

		//Where we left off before
		i = calcLength;

		if (insertLineBreaks && (lengthmod3 != 0) && (charcount == base64LineBreakPosition)) {
			outChars[j++] = '\r';
			outChars[j++] = '\n';
		}

		switch (lengthmod3)
		{
		case 2: //One character padding needed
			outChars[j] = base64[(inData[i] & 0xfc) >> 2];
			outChars[j + 1] = base64[((inData[i] & 0x03) << 4) | ((inData[i + 1] & 0xf0) >> 4)];
			outChars[j + 2] = base64[(inData[i + 1] & 0x0f) << 2];
			outChars[j + 3] = base64[64]; //Pad
			j += 4;
			break;
		case 1: // Two character padding needed
			outChars[j] = base64[(inData[i] & 0xfc) >> 2];
			outChars[j + 1] = base64[(inData[i] & 0x03) << 4];
			outChars[j + 2] = base64[64]; //Pad
			outChars[j + 3] = base64[64]; //Pad
			j += 4;
			break;
		}

		return j;
	}
	bool Convert::toBase64String(const byte* inArray, int offset, int length, StringBuilder& array, Base64FormattingOptions options)
	{
//		int inA-rrayLength;
		int stringLength;

//		inArrayLength = length;

		bool insertLineBreaks = (options == Convert::InsertLineBreaks);
		//Create the new string.  This is the maximally required length.
		stringLength = toBase64_CalculateAndValidateOutputLength(length, insertLineBreaks);
		if (stringLength > 0)
		{
			char* outChars = new char[stringLength];
			int j = convertToBase64Array(outChars, inArray, offset, length, insertLineBreaks);
			array.append(outChars, stringLength);
			delete[] outChars;
			return j > 0;
		}
		return false;
	}
	string Convert::toBase64String(const byte* inArray, int offset, int length, Convert::Base64FormattingOptions options)
	{
		StringBuilder array;
		if (toBase64String(inArray, offset, length, array, options))
			return array.toString();
		return "";
	}

	string Convert::fromBase64String(const string& str)
	{
		ByteArray array;
		if (fromBase64String(str, array))
		{
			return string((const char*)array.data());
		}
		return "";
	}
	bool Convert::fromBase64String(const string& str, ByteArray& array)
	{
		return fromBase64String(str.c_str(), (int)str.length(), array);
	}
	bool Convert::fromBase64String(const char* inputPtr, int inputLength, ByteArray& array)
	{
		// The validity of parameters much be checked by callers, thus we are Critical here.

		assert(0 <= inputLength);

		// We need to get rid of any trailing white spaces.
		// Otherwise we would be rejecting input such as "abc= ":
		while (inputLength > 0)
		{
			int lastChar = inputPtr[inputLength - 1];
			if (lastChar != (int) ' ' && lastChar != (int) '\n' && lastChar != (int) '\r' && lastChar != (int) '\t')
				break;
			inputLength--;
		}

		// Compute the output length:
		int resultLength = fromBase64_ComputeResultLength(inputPtr, inputLength);

		if (0 <= resultLength)
		{
			// resultLength can be zero. We will still enter FromBase64_Decode and process the input.
			// It may either simply write no bytes (e.g. input = " ") or throw (e.g. input = "ab").

			// Create result byte blob:
			byte* decodedBytes = new byte[resultLength];

			// Convert Base64 chars into bytes:
			int actualResultLength = fromBase64_Decode(inputPtr, inputLength, decodedBytes, resultLength);
			array.addRange(decodedBytes, resultLength);
			delete[] decodedBytes;

			return actualResultLength > 0;
		}
		// Note that actualResultLength can differ from resultLength if the caller is modifying the array
		// as it is being converted. Silently ignore the failure.
		// Consider throwing exception in an non in-place release.

		// We are done:
		return false;
	}
	/// <summary>
	/// Compute the number of bytes encoded in the specified Base 64 char array:
	/// Walk the entire input counting white spaces and padding chars, then compute result length
	/// based on 3 bytes per 4 chars.
	/// </summary>
	int Convert::fromBase64_ComputeResultLength(const char* inputPtr, int inputLength)
	{
		const uint intEq = (uint) '=';
		const uint intSpace = (uint) ' ';

		assert(0 <= inputLength);

		const char* inputEndPtr = inputPtr + inputLength;
		int usefulInputLength = inputLength;
		int padding = 0;

		while (inputPtr < inputEndPtr) {

			uint c = (uint)(*inputPtr);
			inputPtr++;

			// We want to be as fast as possible and filter out spaces with as few comparisons as possible.
			// We end up accepting a number of illegal chars as legal white-space chars.
			// This is ok: as soon as we hit them during actual decode we will recognise them as illegal and throw.
			if (c <= intSpace)
				usefulInputLength--;

			else if (c == intEq) {
				usefulInputLength--;
				padding++;
			}
		}

		assert(0 <= usefulInputLength);

		// For legal input, we can assume that 0 <= padding < 3. But it may be more for illegal input.
		// We will notice it at decode when we see a '=' at the wrong place.
		assert(0 <= padding);

		// Perf: reuse the variable that stored the number of '=' to store the number of bytes encoded by the
		// last group that contains the '=':
		if (padding != 0) {

			if (padding == 1)
				padding = 2;
			else if (padding == 2)
				padding = 1;
			else
				return 0;// throw Exception("Format_BadBase64Char");
		}

		// Done:
		return (usefulInputLength / 4) * 3 + padding;
	}
	/// <summary>
	/// Decode characters representing a Base64 encoding into bytes:
	/// Walk the input. Every time 4 chars are read, convert them to the 3 corresponding output bytes.
	/// This method is a bit lengthy on purpose. We are trying to avoid jumps to helpers in the loop
	/// to aid performance.
	/// </summary>
	/// <param name="inputPtr">Pointer to first input char</param>
	/// <param name="inputLength">Number of input chars</param>
	/// <param name="destPtr">Pointer to location for teh first result byte</param>
	/// <param name="destLength">Max length of the preallocated result buffer</param>
	/// <returns>If the result buffer was not large enough to write all result bytes, return -1;
	/// Otherwise return the number of result bytes actually produced.</returns>
	int Convert::fromBase64_Decode(const char* startInputPtr, int inputLength, byte* startDestPtr, int destLength)
	{
		// You may find this method weird to look at. It������s written for performance, not aesthetics.
		// You will find unrolled loops label jumps and bit manipulations.

		const uint intA = (uint) 'A';
		const uint inta = (uint) 'a';
		const uint int0 = (uint) '0';
		const uint intEq = (uint) '=';
		const uint intPlus = (uint) '+';
		const uint intSlash = (uint) '/';
		const uint intSpace = (uint) ' ';
		const uint intTab = (uint) '\t';
		const uint intNLn = (uint) '\n';
		const uint intCRt = (uint) '\r';
		const uint intAtoZ = (uint)('Z' - 'A');  // = ('z' - 'a')
		const uint int0to9 = (uint)('9' - '0');

		const char* inputPtr = startInputPtr;
		byte* destPtr = startDestPtr;

		// Pointers to the end of input and output:
		const char* endInputPtr = inputPtr + inputLength;
		byte* endDestPtr = destPtr + destLength;

		// Current char code/value:
		uint currCode;

		// This 4-byte integer will contain the 4 codes of the current 4-char group.
		// Eeach char codes for 6 bits = 24 bits.
		// The remaining byte will be FF, we use it as a marker when 4 chars have been processed.            
		uint currBlockCodes = 0x000000FFu;

		while (true)
		{
			// break when done:
			if (inputPtr >= endInputPtr)
				goto _AllInputConsumed;

			// Get current char:
			currCode = (uint)(*inputPtr);
			inputPtr++;

			// Determine current char code:

			if (currCode - intA <= intAtoZ)
				currCode -= intA;

			else if (currCode - inta <= intAtoZ)
				currCode -= (inta - 26u);

			else if (currCode - int0 <= int0to9)
				currCode -= (int0 - 52u);

			else {
				// Use the slower switch for less common cases:
				switch (currCode) {

					// Significant chars:
				case intPlus:  currCode = 62u;
					break;

				case intSlash: currCode = 63u;
					break;

					// Legal no-value chars (we ignore these):
				case intCRt:  case intNLn:  case intSpace:  case intTab:
					continue;

					// The equality char is only legal at the end of the input.
					// Jump after the loop to make it easier for the JIT register predictor to do a good job for the loop itself:
				case intEq:
					goto _EqualityCharEncountered;

					// Other chars are illegal:
				default:
					return 0;	// throw Exception("Format_BadBase64Char");
				}
			}

			// Ok, we got the code. Save it:
			currBlockCodes = (currBlockCodes << 6) | currCode;

			// Last bit in currBlockCodes will be on after in shifted right 4 times:
			if ((currBlockCodes & 0x80000000u) != 0u) {

				if ((int)(endDestPtr - destPtr) < 3)
					return -1;

				*(destPtr) = (byte)(currBlockCodes >> 16);
				*(destPtr + 1) = (byte)(currBlockCodes >> 8);
				*(destPtr + 2) = (byte)(currBlockCodes);
				destPtr += 3;

				currBlockCodes = 0x000000FFu;
			}

		}  // while

			// 'd be nice to have an assert that we never get here, but CS0162: Unreachable code detected.
			// Contract.Assert(false, "We only leave the above loop by jumping; should never get here.");

			// We jump here out of the loop if we hit an '=':
		_EqualityCharEncountered:

		// Recall that inputPtr is now one position past where '=' was read.
		// '=' can only be at the last input pos:
		if (inputPtr == endInputPtr) {

			// Code is zero for trailing '=':
			currBlockCodes <<= 6;

			// The '=' did not complete a 4-group. The input must be bad:
			if ((currBlockCodes & 0x80000000u) == 0u)
				return 0;	// throw Exception("Format_BadBase64CharArrayLength");

			if ((int)(endDestPtr - destPtr) < 2)  // Autch! We underestimated the output length!
				return -1;

			// We are good, store bytes form this past group. We had a single "=", so we take two bytes:
			*(destPtr++) = (byte)(currBlockCodes >> 16);
			*(destPtr++) = (byte)(currBlockCodes >> 8);

			currBlockCodes = 0x000000FFu;

		}
		else { // '=' can also be at the pre-last position iff the last is also a '=' excluding the white spaces:

			// We need to get rid of any intermediate white spaces.
			// Otherwise we would be rejecting input such as "abc= =":
			while (inputPtr < (endInputPtr - 1))
			{
				int lastChar = *(inputPtr);
				if (lastChar != (int) ' ' && lastChar != (int) '\n' && lastChar != (int) '\r' && lastChar != (int) '\t')
					break;
				inputPtr++;
			}

			if (inputPtr == (endInputPtr - 1) && *(inputPtr) == '=') {

				// Code is zero for each of the two '=':
				currBlockCodes <<= 12;

				// The '=' did not complete a 4-group. The input must be bad:
				if ((currBlockCodes & 0x80000000u) == 0u)
					return 0;	// throw Exception("Format_BadBase64CharArrayLength");

				if ((int)(endDestPtr - destPtr) < 1)  // Autch! We underestimated the output length!
					return -1;

				// We are good, store bytes form this past group. We had a "==", so we take only one byte:
				*(destPtr++) = (byte)(currBlockCodes >> 16);

				currBlockCodes = 0x000000FFu;

			}
			else  // '=' is not ok at places other than the end:
				return 0;	// throw Exception("Format_BadBase64Char");

		}

		// We get here either from above or by jumping out of the loop:
	_AllInputConsumed:

		// The last block of chars has less than 4 items
		if (currBlockCodes != 0x000000FFu)
			return 0;	// throw Exception("Format_BadBase64CharArrayLength");

		// Return how many bytes were actually recovered:
		return (int)(destPtr - startDestPtr);

	} // int FromBase64_Decode(...)
    
    byte Convert::ascToHex(byte aHex)
    {
        if((aHex>=0)&&(aHex<=9))
            aHex += 0x30;
        else if((aHex>=10)&&(aHex<=15))//A-F
            aHex += 0x37;
        else
            aHex = 0xff;
        return aHex;
    }
    byte Convert::hexToAsc(byte aChar)
    {
        if((aChar>=0x30)&&(aChar<=0x39))
            aChar -= 0x30;
        else if((aChar>=0x41)&&(aChar<=0x46))//A-Z
            aChar -= 0x37;
        else if((aChar>=0x61)&&(aChar<=0x66))//a-z
            aChar -= 0x57;
        else aChar = 0xff;
        return aChar;
    }
    
    bool Convert::isHexInteger(const string& text)
    {
        if(text.length() <= 2)
            return false;
        
        return stringEqual(text.substr(0, 2), "0x");
    }
}
