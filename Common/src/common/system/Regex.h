#ifndef REGEX_H
#define REGEX_H

#include <stdio.h>
#include <string>
#if WIN32
#include <regex>
#else
#include <sys/types.h>
#include <regex.h>
#endif
#include "common/common_global.h"

using namespace std;

namespace Common
{
	class COMMON_EXPORT Regex
	{
	public:
		Regex(const string& pattern)
		{
#if WIN32
			_regex = new regex(pattern);
#else
			int result = regcomp(&_regex, pattern.c_str(), REG_EXTENDED);
#ifdef DEBUG
			if (result != REG_NOERROR)
			{
				char ebuf[128];
				regerror(result, &_regex, ebuf, sizeof(ebuf));
				char str[256];
				sprintf(str, "regcomp failed! result: %s\n", ebuf);
				fprintf(stderr, "%s\n", str);
				fflush(stderr);
			}
#endif
#endif
		}
		~Regex()
		{
#if WIN32
			delete _regex;
			_regex = NULL;
#else
			regfree(&_regex);
#endif
		}

		inline bool match(const string& input)
		{
#if WIN32
			if(!input.empty())
			{
				smatch m;
				return regex_search(input, m, *_regex, regex_constants::match_not_null);
			}
#else
			const size_t nmatch = 10;
			regmatch_t pmatch[nmatch];
			int result = regexec(&_regex, input.c_str(), nmatch, pmatch, 0);
			if(result == REG_NOERROR)
			{
				return (pmatch[0].rm_so != pmatch[0].rm_eo);
			}
#endif
			return false;
		}
	private:
#if WIN32
		regex* _regex;
#else
		regex_t _regex;
#endif
	};
}
#endif // REGEX_H
