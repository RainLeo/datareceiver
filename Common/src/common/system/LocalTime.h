#ifndef LOCALTIME_H
#define LOCALTIME_H

#include <time.h>
#include "common/common_global.h"
#include "DateTime.h"

namespace Common
{
	class LocalTime
	{
	public:
		static bool setTime(const time_t& time);
		static bool setTime(const DateTime& time);
	};
}

#endif // LOCALTIME_H
