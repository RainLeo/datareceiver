﻿#include "DynamicLoader.h"
#include "common/system/Convert.h"
#include "common/exception/Exception.h"
#if WIN32
#include <Windows.h>
#else
#include <dlfcn.h>
#endif

using namespace Common;

namespace Common
{
    DynamicLoader::DynamicLoader(const char* libName)
    {
        if (libName == NULL || strlen(libName) == 0)
            throw ArgumentException("libName");
        
#if WIN32
		_handle = LoadLibrary(libName);
		if(_handle == NULL)
		{
			retriveError();
		}
		else
		{
			_error = "";
		}
#else
        _handle = dlopen(libName, RTLD_NOW);
        if(_handle == NULL)
        {
			retriveError();
        }
        else
        {
            _error = "";
            // reset errors
            dlerror();
        }
#endif
	}
    DynamicLoader::DynamicLoader(const string& libName) : DynamicLoader(libName.c_str())
    {
    }
    DynamicLoader::~DynamicLoader()
    {
        if(_handle != NULL)
        {
#if WIN32
			FreeLibrary((HINSTANCE)_handle);
#else
            dlclose(_handle);
#endif
            _handle = NULL;
        }
    }
    
    void* DynamicLoader::getSymbol(const char* symbol)
    {
        if (symbol == NULL || strlen(symbol) == 0)
            throw ArgumentException("symbol");
        
        if(_handle != NULL)
        {
#if WIN32
			void* result = GetProcAddress((HINSTANCE)_handle, symbol);
#else
            void* result = dlsym(_handle, symbol);
#endif
			if(result == NULL)
			{
				retriveError();
			}
        }
        return NULL;
    }
    void* DynamicLoader::getSymbol(const string& symbol)
    {
        return getSymbol(symbol.c_str());
    }
    
    bool DynamicLoader::hasError() const
    {
        return !_error.empty();
    }
    string DynamicLoader::error() const
    {
        return _error;
    }

	void DynamicLoader::retriveError()
	{
#if WIN32
		// Retrieve the system error message for the last-error code

		LPVOID lpMsgBuf;
		DWORD dw = GetLastError();

		FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			dw,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR)&lpMsgBuf,
			0, NULL);

		_error = (LPTSTR)lpMsgBuf;
		LocalFree(lpMsgBuf);
#else
		_error = dlerror();
#endif
	}
}
