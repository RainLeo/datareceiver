#ifndef STREAMACCESSOR_H
#define STREAMACCESSOR_H

#include "common/common_global.h"
#include "../IO/Stream.h"
#include "CopyVector.h"

namespace Common
{
	class StreamAccessor
	{
	public:
		StreamAccessor(void)
		{
		}
		virtual ~StreamAccessor(void)
		{
		}

		virtual void write(Stream* stream) const = 0;
		virtual void read(Stream* stream) = 0;
	};

	template <class type>
	class StreamVector : public CopyVector<type>, public StreamAccessor
	{
	public:
		StreamVector(bool autoDelete = true, uint capacity = CopyVector<type>::DefaultCapacity) : CopyVector<type>(autoDelete, capacity)
		{
		}
		virtual ~StreamVector(void)
		{
		}

		virtual void write(Stream* stream) const
		{
			int cl = countLength();
			if(!(cl == 0 || cl == 1 || cl == 2 || cl == 4))
			{
				throw ArgumentException("countLength must be equal to 0, 1, 2, 4.");
			}

			uint c = fixedCount();
			switch (cl)
			{
			case 0:
				break;
			case 1:
				stream->writeByte((byte)c);
				break;
			case 2:
				stream->writeUInt16((ushort)c);
				break;
			case 4:
				stream->writeUInt32(c);
				break;
			default:
				break;
			}
			for (uint i = 0; i < c; i++)
			{
				type* value = this->at(i);
				value->write(stream);
			}
		}
		virtual void read(Stream* stream)
		{
			int cl = countLength();
			if(!(cl == 0 || cl == 1 || cl == 2 || cl == 4))
			{
				throw ArgumentException("countLength must be equal to 0, 1, 2, 4.");
			}

			uint c = fixedCount();
			switch (cl)
			{
			case 0:
				break;
			case 1:
				c = stream->readByte();
				break;
			case 2:
				c = stream->readUInt16();
				break;
			case 4:
				c = stream->readUInt32();
				break;
			default:
				break;
			}
			for (uint i = 0; i < c; i++)
			{
				type* value = new type();
				assert(value);
				value->read(stream);
				this->add(value);
			}
		}

	protected:
		virtual int countLength() const
		{
			return 2;
		}

		virtual uint fixedCount() const
		{
			return Vector<type>::count();
		}
	};
}

#endif // STREAMACCESSOR_H
