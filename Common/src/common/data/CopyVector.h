#ifndef COPYVECTOR_H
#define COPYVECTOR_H

#include "Vector.h"

namespace Common
{
	template <class type>
	class CopyVector : public Vector<type>
	{
	public:
		CopyVector(bool autoDelete = true, uint capacity = Vector<type>::DefaultCapacity) : Vector<type>(autoDelete, capacity)
		{
		}
		virtual ~CopyVector(void)
		{
		}

		virtual void copyFrom(const CopyVector* values, bool append = false)
		{
			if(!append)
			{
				this->clear();
			}

			for (uint i = 0; i < values->count(); i++)
			{
				type* value = new type();
				value->copyFrom(values->at(i));
				this->add(value);
			}
		}
	};
}

#endif // COPYVECTOR_H
