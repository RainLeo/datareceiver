#ifndef STRINGBUILDER_H
#define STRINGBUILDER_H

#include <string>
#include "Array.h"

using namespace std;

namespace Common
{
	class StringBuilder
	{
	public:
#if WIN32
        const char* NewLine = "\r\n";
#else
        const char* NewLine = "\n";
#endif
        
        COMMON_ATTR_DEPRECATED("use String::String")
		StringBuilder(uint capacity = DefaultCapacity)
		{
			_array.setCapacity(capacity);
		}

        COMMON_ATTR_DEPRECATED("use String::append")
		inline void append(const char ch)
		{
			_array.add(ch);
		}
        COMMON_ATTR_DEPRECATED("use String::append")
		inline void append(const char* str, uint count)
		{
			_array.addRange(str, count);
		}
        COMMON_ATTR_DEPRECATED("use String::append")
		inline void append(const string& str)
		{
			_array.addRange(str.c_str(), (uint)str.length());
		}
        COMMON_ATTR_DEPRECATED("use String::appendLine")
        inline void appendLine(const char ch)
        {
            append(ch);
            append(NewLine);
        }
        COMMON_ATTR_DEPRECATED("use String::appendLine")
        inline void appendLine(const char* str, uint count)
        {
            append(str, count);
            append(NewLine);
        }
        COMMON_ATTR_DEPRECATED("use String::appendLine")
        inline void appendLine(const string& str)
        {
            append(str);
            append(NewLine);
        }
        
        COMMON_ATTR_DEPRECATED("use String::length")
		inline uint length() const
		{
			return _array.count();
		}
        COMMON_ATTR_DEPRECATED("use String::clear")
		inline void empty()
		{
			_array.clear();
		}
        COMMON_ATTR_DEPRECATED("use String::c_str")
		inline const char* toString() const
		{
			return _array.data();
		}

        COMMON_ATTR_DEPRECATED("use String::clear")
		inline void clear()
		{
			_array.clear();
		}
        COMMON_ATTR_DEPRECATED("use String::isNullOrEmpty")
		inline bool isEmpty() const
		{
			return _array.count() == 0;
		}

	private:
		Array<char> _array;
        
        const static int DefaultCapacity = 1024;
	};
}

#endif	// STRINGBUILDER_H
