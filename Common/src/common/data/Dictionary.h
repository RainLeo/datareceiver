#ifndef DICTIONARY_H
#define DICTIONARY_H

#include <stdio.h>
#include <map>
#include "Vector.h"

#ifndef uint
typedef unsigned uint;
#endif

using namespace std;

namespace Common
{
    template< class TKey, class TValue >
    class Dictionary
    {
    public:
        Dictionary(bool autoDeleteValue = false, bool autoDeleteKey = false)
        {
            _autoDeleteValue = autoDeleteValue;
            _autoDeleteKey = autoDeleteKey;
        }
        ~Dictionary()
        {
            clear();
        }
        
        inline void add(TKey key, TValue value)
        {
            _map.insert(make_pair(key, value));
        }
        inline bool containsKey(TKey key) const
        {
            auto result = _map.find(key);
            return result != _map.end();
        }
        inline void clear()
        {
            if(_autoDeleteKey)
            {
                Vector<TKey> k;
                keys(k, _autoDeleteKey);
            }
            if(_autoDeleteValue)
            {
                Vector<TValue> v;
                values(v, _autoDeleteValue);
            }
            
            _map.clear();
        }
        inline uint count() const
        {
            return (uint)_map.size();
        }
        inline bool at(TKey key, TValue& value) const
        {
            if (!containsKey(key))
                return false;
            
            value = _map.at(key);
            return true;
        }
        inline TValue operator[](TKey key) const
        {
            TValue value;
            if (this->at(key, value))
                return value;
            
            return NULL;
        }
        inline bool remove(TKey key)
        {
            return _map.erase(key) > 0;
        }
        
        inline void keys(Vector<TKey>& k, bool autoDelete = false) const
        {
            k.setAutoDelete(autoDelete);
            
            auto it = _map.begin();
            for(;it!=_map.end();++it)
            {
                k.add(&it->first);
            }
        }
        inline void values(Vector<TValue>& v, bool autoDelete = false) const
        {
            v.setAutoDelete(autoDelete);
            
            auto it = _map.begin();
            for(;it!=_map.end();++it)
            {
                v.add(&it->second);
            }
        }
        
    private:
        map<TKey, TValue> _map;
        bool _autoDeleteKey;
        bool _autoDeleteValue;
    };
}

#endif // DICTIONARY_H
