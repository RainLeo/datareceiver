#ifndef LOOPARRAY_H
#define LOOPARRAY_H

#include <stdio.h>
#include <string.h>
#include "common/common_global.h"

namespace Common
{
	template <class type>
	class LoopArray
	{
	public:
		LoopArray(int maxLength = DefaultMaxLength)
		{
			_maxLength = maxLength <= 0 ? 1 : maxLength;
			_array = new type[_maxLength];

            makeNull();
		}
		~LoopArray()
		{
			delete[] _array;
			_array = NULL;
		}
        void makeNull()
        {
            memset(_array, 0, sizeof(type)*(_maxLength));
            _front = 0;
            _rear = 0;
        }
		void enqueue(type value)
		{
			bool f = full();
			int index = _rear >= _maxLength ? 0 : _rear;
			_array[index] = value;
			_rear = index + 1;
			if (f)
			{
				_front++;
				if (_front >= _maxLength)
					_front = 0;
			}
		}
		bool dequeue()
		{
			if (empty())
			{
				return false;
			}

			_front++;
			if (_front >= _maxLength)
				_front = 0;

			// _front = _rear = 0
			if (_front == _rear || (_front == 0 && _rear == _maxLength))
				makeNull();

			return true;
		}
		type front()
		{
			if (empty())
			{
				return type();
			}
			return _array[_front];
		}
		inline bool empty() const
		{
			return (_front == 0 && _rear == 0);
		}
		inline bool full() const
		{
			return count() >= (uint)_maxLength;
		}
		inline uint count() const
		{
			if (empty())
				return 0;

			return (_rear > _front) ? _rear - _front : _rear + _maxLength - _front;
		}
		void copyTo(type* value) const
		{
			if (!empty())
			{
				if (_rear > _front)
				{
					memcpy(value, _array+_front, count()*sizeof(type));
				}
				else
				{
					int n = 0;
					if (_front < _maxLength)
					{
						n = _maxLength - _front;
						memcpy(value, _array+_front, n*sizeof(type));
					}
					memcpy(value+n, _array, (count() - n)*sizeof(type));
				}
			}
		}
        type at(uint i) const
        {
            if (!empty() && i < count())
            {
                if (_rear > _front)
                {
                    return _array[_front+i];
                }
                else
                {
                    if (_front+i < _maxLength)
                    {
                        return _array[_front+i];
                    }
                    return _array[_rear-(count()-i)];
                }
            }
            return type();
        }
        inline int maxLength() const
        {
            return _maxLength;
        }
        inline void setMaxLength(int maxLength)
        {
            _maxLength = maxLength;
        }
        inline void clear()
        {
            makeNull();
        }
        
    protected:
        const static int DefaultMaxLength = 1024;

	private:
		int _maxLength;
		int _front;
		int _rear;
		type* _array;
	};
}
#endif // LOOPARRAY_H
