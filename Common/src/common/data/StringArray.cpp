#include <stdarg.h>
#include "StringArray.h"
#include "../system/Convert.h"
#include "PrimitiveType.h"

namespace Common
{
	StringArray::StringArray(bool autoDelete, uint capacity) : Vector<string>(autoDelete, capacity)
	{
	}
	StringArray::StringArray(const char* item, ...) : StringArray()
	{
		const char* str = item;
		va_list ap;
		va_start(ap, item);
		while (str != NULL && str[0] != '\0')
		{
			if (str != NULL)
			{
				add(str);
			}
			str = va_arg(ap, const char*);
		}
		va_end(ap);
	}

	void StringArray::add(const string& str)
	{
		string* nstr = new string(str);
		Vector<string>::add(nstr);
}

	string StringArray::at(uint i) const
	{
		string* str = Vector<string>::at(i);
		return string(*str);
	}

	string StringArray::operator[](uint i) const
	{
		return this->at(i);
	}

	void StringArray::addRange(const string* strs, uint count)
	{
		for (uint i = 0; i < count; i++)
		{
			add(strs[i]);
		}
	}
    void StringArray::addRange(const StringArray* strs)
    {
        for (uint i = 0; i < strs->count(); i++)
        {
            add(strs->at(i));
        }
    }

	bool StringArray::insert(uint i, const string& str)
	{
		string* nstr = new string(str);
		return Vector<string>::insert(i, nstr);
	}

	bool StringArray::set(uint i, const string& str)
	{
		string* nstr = new string(str);
		return Vector<string>::set(i, nstr);
	}

	bool StringArray::contains(const string& str, bool ignoreCase) const
	{
		string** strings = data();
		for (uint i = 0; i < count(); i++)
		{
			if (String::stringEquals(*strings[i], str, ignoreCase))
			{
				return true;
			}
		}
		return false;
	}

	void StringArray::write(Stream* stream, int countLength, int strLength) const
	{
		int cl = countLength;
		if (!(cl == 0 || cl == 1 || cl == 2 || cl == 4))
		{
			throw ArgumentException("countLength must be equal to 0, 1, 2, 4.");
		}

		uint c = count();
		switch (cl)
		{
		case 0:
			break;
		case 1:
			stream->writeByte((byte)c);
			break;
		case 2:
			stream->writeUInt16((ushort)c);
			break;
		case 4:
			stream->writeUInt32(c);
			break;
		default:
			break;
		}
		for (uint i = 0; i < c; i++)
		{
			string value = this->at(i);
			stream->writeStr(value, strLength);
		}
	}
	void StringArray::read(Stream* stream, int countLength, int strLength)
	{
		int cl = countLength;
		if (!(cl == 0 || cl == 1 || cl == 2 || cl == 4))
		{
			throw ArgumentException("countLength must be equal to 0, 1, 2, 4.");
		}

		uint c = count();
		switch (cl)
		{
		case 0:
			break;
		case 1:
			c = stream->readByte();
			break;
		case 2:
			c = stream->readUInt16();
			break;
		case 4:
			c = stream->readUInt32();
			break;
		default:
			break;
		}
		for (uint i = 0; i < c; i++)
		{
			string value = stream->readStr(strLength);
			add(value);
		}
	}

	void StringArray::copyFrom(const StringArray* values, bool append)
	{
		if (!append)
		{
			this->clear();
		}

		for (uint i = 0; i < values->count(); i++)
		{
			add(values->at(i));
		}
	}
};
