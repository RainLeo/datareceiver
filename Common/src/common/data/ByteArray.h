#ifndef BYTEARRAY_H
#define BYTEARRAY_H

#include <stdio.h>
#include "common/common_global.h"
#include "Array.h"
#include "../exception/Exception.h"

namespace Common
{
	class ByteArray : public Array<byte>
	{
	public:
        ByteArray(uint capacity = Array<byte>::DefaultCapacity) : Array<byte>(capacity)
		{
		}
		ByteArray(const Array<byte>* array) : Array<byte>(array)
		{
		}
		ByteArray(const byte* array, uint count, uint capacity = DefaultCapacity) : Array<byte>(array, count, capacity)
		{
		}

		inline string toString(const char* format = "%02X", const char* splitStr = "-") const
		{
			string result = "";
			char temp[32];
			memset(temp, 0, sizeof(temp));
			for (uint i = 0; i < count(); i++)
			{
				if(i > 0)
				{
					result.append(splitStr);
				}
				sprintf(temp, format, at(i));
				result.append(temp);
			}
			return result;
		}

		static bool parse(const string& str, ByteArray& array, const char* splitStr = "-")
		{
			const char* format = "%02X%n";
			const int formatLen = 2;
			size_t splitStrLen = splitStr != NULL ? strlen(splitStr) : 0;

			const char* buffer = str.c_str();
			size_t length = str.length();
			uint index = 0;
			ByteArray temp;
			char valueStr[8];
			memset(valueStr, 0, sizeof(valueStr));
			char splitValueStr[8];
			memset(splitValueStr, 0, sizeof(splitValueStr));
			while (index < length)
			{
				strncpy(valueStr, &buffer[index], formatLen);
				int value;
				uint len = 0;
				if (sscanf(valueStr, format, &value, &len) == 1 && formatLen == len && value <= 255)
				{
					temp.add((byte)value);
				}
				else
				{
					return false;
				}
				index += formatLen;

				if (splitStrLen > 0)
				{
					strncpy(splitValueStr, &buffer[index], splitStrLen);
					if (strcmp(splitValueStr, splitStr) != 0)
					{
						return false;
					}
					index += splitStrLen;
				}
			}

			array.addRange(&temp);
			return true;
		}

		void replace(const ByteArray* buffer, int offset, int count, const ByteArray* src, const ByteArray* dst)
		{
			if (buffer == NULL)
			{
				throw ArgumentNullException("buffer");
			}
			if (src == NULL)
			{
				throw ArgumentNullException("src");
			}
			if (offset < 0)
			{
				throw ArgumentOutOfRangeException("offset");
			}
			if (count < 0)
			{
				throw ArgumentOutOfRangeException("count");
			}
			if (((int)buffer->count() - offset) < count)
			{
				throw ArgumentException("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection.");
			}

			for (int i = 0; i < offset; i++)
			{
				add(buffer->at(i));
			}
			int firstIndex = 0;
			for (int i = offset; i < count + offset; i++)
			{
				if (buffer->at(i) == src->at(firstIndex))
				{
					firstIndex++;
					if (firstIndex >= (int)src->count())
					{
						if (dst != NULL)
							addRange(dst);
						firstIndex = 0;
					}
				}
				else
				{
					if (firstIndex > 0)
					{
						for (int j = 0; j < firstIndex; j++)
						{
							add(src->at(j));
						}
						firstIndex = 0;
					}
					else
					{
						add(buffer->at(i));
					}
				}
			}
			for (int i = 0; i < (int)buffer->count() - offset - count; i++)
			{
				add(buffer->at(offset + count + i));
			}
		}
	};
}
#endif // BYTEARRAY_H
