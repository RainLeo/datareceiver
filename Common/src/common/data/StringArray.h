#ifndef STRINGARRAY_H
#define STRINGARRAY_H

#include <string>
#include "Vector.h"
#include "../IO/Stream.h"

using namespace std;

namespace Common
{
	class StringArray : public Vector<string>
	{
	public:
		StringArray(bool autoDelete = true, uint capacity = Vector<string>::DefaultCapacity);
		StringArray(const char* item, ...);

		void add(const string& str);
		string at(uint i) const;
		string operator[](uint i) const;
		void addRange(const string* strs, uint count);
        void addRange(const StringArray* strs);
		bool insert(uint i, const string& str);
		bool set(uint i, const string& str);
		bool contains(const string& str, bool ignoreCase = false) const;

		void write(Stream* stream, int countLength = 2, int strLength = 2) const;
		void read(Stream* stream, int countLength = 2, int strLength = 2);

		void copyFrom(const StringArray* values, bool append = false);
	};
}

#endif	// STRINGARRAY_H
