#ifndef common_PrimitiveType_h
#define common_PrimitiveType_h

#include <stdio.h>
#if WIN32
#ifndef GUID_DEFINED
#include <guiddef.h>
#endif /* GUID_DEFINED */

#ifndef UUID_DEFINED
#define UUID_DEFINED
typedef GUID UUID;
#ifndef uuid_t
#define uuid_t UUID
#endif
#endif
#else
//#include <uuid/uuid.h>
#include "libuuid/uuid.h"
#endif
#include "../IO/Stream.h"

namespace Common
{
    class String
    {
    public:
        enum Base64FormattingOptions
        {
            Base64None = 0,
            InsertLineBreaks = 1
        };
        
        const static char* NewLine;
        const static char* Empty;
        
        String(uint capacity = 1024);
        String(const String& value);
        String(const string& value);
        String(const char* value);
        ~String();
        
        bool isNullOrEmpty() const;
        uint length() const;
        void empty();
        
        operator const char*() const;
        
        String operator+=(const String& value);
        String operator+=(const string& value);
        String operator+=(const char* value);
        String operator+(const String& value);
        String operator+(const string& value);
        String operator+(const char* value);
        
        void operator=(const String& value);
        void operator=(const string& value);
        void operator=(const char* value);
        bool operator==(const String& value) const;
        bool operator!=(const String& value) const;
        
        bool operator>(const String& value) const;
        bool operator>=(const String& value) const;
        bool operator<(const String& value) const;
        bool operator<=(const String& value) const;
        
        void write(Stream* stream, int lengthCount = 2) const;
        void read(Stream* stream, int lengthCount = 2);
        void writeFixedLengthStr(Stream* stream, uint length);
        void readFixedLengthStr(Stream* stream, uint length);
        
        const char* c_str() const;
        
        bool equals(const String& value, bool ignoreCase = false) const;
        
        const String toLower();
        const String toUpper();
        
        const String trim(const char symbol1=' ', const char symbol2='\0', const char symbol3='\0', const char symbol4='\0');
        const String trimStart(const char symbol1=' ', const char symbol2='\0', const char symbol3='\0', const char symbol4='\0');
        const String trimEnd(const char symbol1=' ', const char symbol2='\0', const char symbol3='\0', const char symbol4='\0');

        const String GBKtoUTF8() const;
        const String UTF8toGBK() const;
        
        const String toBase64() const;
        const String fromBase64() const;

        int find(const char* substring) const;
        bool contains(const char* substring) const;
        bool contains(const char subchar) const;
        
//        void splitStr(const char splitSymbol, StringArray& texts);
//        void splitStr(StringArray& texts, const char splitSymbol=';');
        
        void append(const char ch);
        void append(const char* str, uint count);
        void append(const string& str);
        void appendLine(const char ch);
        void appendLine(const char* str, uint count);
        void appendLine(const string& str);
        
        const String replace(const String& src, const String& dst);
        const String substr(int offset, uint count);
        
    public:
        static bool equals(const String& value1, const String& value2, bool ignoreCase = false);
        static bool stringEquals(const string& value1, const string& value2, bool ignoreCase = false);
        static int compare(const String& value1, const String& value2, bool ignoreCase = false);
        static const String GBKtoUTF8(const String& value);
        static const String UTF8toGBK(const String& value);
        
        static const String toBase64(const String& value);
        static const String toBase64(const byte* inArray, int offset, int length, Base64FormattingOptions options = Base64FormattingOptions::Base64None);
        static bool toBase64(const byte* inArray, int offset, int length, String& str, Base64FormattingOptions options = Base64FormattingOptions::Base64None);

        static const String fromBase64(const String& value);
        static bool fromBase64(const String& value, ByteArray& array);
        static bool fromBase64(const char* inputPtr, int inputLength, ByteArray& array);
        
        static const String convert(const char *format, ...);
        static bool isNullOrEmpty(const String& value);
        
        static const String replace(const String& str, const String& src, const String& dst);
        static const String substr(const String& str, int offset, uint count);
        
    private:
        void setString(const char* value);
        void setString(const string& value);
        void setString(char value);
        void addString(const char* value);
        void addString(const string& value);
        void addString(char value);
        const char* getString() const;
        
        static int convertToBase64Array(char* outChars, const byte* inData, int offset, int length, bool insertLineBreaks);
        static int toBase64_CalculateAndValidateOutputLength(int inputLength, bool insertLineBreaks);
        static int fromBase64_ComputeResultLength(const char* inputPtr, int inputLength);
        static int fromBase64_Decode(const char* startInputPtr, int inputLength, byte* startDestPtr, int destLength);
        
    private:
        static const String encoding(const char* fromCode, const char* toCode, const char* str);
        static bool encoding(const char* fromCode, const char* toCode, const char* str, char*& buffer, int& length);
        
    private:
        Array<char> _buffer;
        
        const static char base64Table[65];
        const static int base64LineBreakPosition = 76;
        const static int MaxFormatStrLength = 1024 * 10;	// 10 K
    };
    
    struct Uuid
    {
    public:
        Uuid();
        Uuid(const Uuid& value);
        Uuid(const string& value);
        ~Uuid();
        
        void operator=(const Uuid& value);
        bool operator==(const Uuid& value) const;
        bool operator!=(const Uuid& value) const;
        
        bool operator>(const Uuid& value) const;
        bool operator>=(const Uuid& value) const;
        bool operator<(const Uuid& value) const;
        bool operator<=(const Uuid& value) const;
        
        void write(Stream* stream) const;
        void read(Stream* stream);
        
        String toString() const;
        bool isEmpty() const;
        
        void clear();

	public:
        static bool parse(const String& str, Uuid& value);
		static Uuid generate();
        
    private:
        uuid_t _value;
        
        const static int Size = sizeof(uuid_t);
    };
}

#endif /* common_PrimitiveType_h */
