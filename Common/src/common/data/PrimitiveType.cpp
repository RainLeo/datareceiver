#include <ctype.h>
#include <iconv.h>
#include <stdarg.h>
#include "PrimitiveType.h"
#if WIN32
#include <Rpc.h>
#pragma comment(lib, "Rpcrt4.lib")
#endif

namespace Common
{
#if WIN32
    const char* String::NewLine = "\r\n";
#else
    const char* String::NewLine = "\n";
#endif
    const char* String::Empty = "";
    
    const char String::base64Table[65] = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
        'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd',
        'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
        't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7',
        '8', '9', '+', '/', '=' };
    
    String::String(uint capacity) : String(nullptr)
    {
        _buffer.setCapacity(capacity);
    }
    String::String(const String& value)
    {
        this->operator=(value);
    }
    String::String(const string& value) : String(value.c_str())
    {
    }
    String::String(const char* value)
    {
        setString(value);
    }
    String::~String()
    {
    }
    
    void String::setString(const char* value)
    {
        _buffer.clear();
        addString(value);
    }
    void String::setString(const string& value)
    {
        setString(value.c_str());
    }
    void String::addString(const char* value)
    {
        if(value != nullptr)
        {
            uint length = (uint)strlen(value);
            if(length > 0)
            {
                if(isNullOrEmpty())
                {
                    _buffer.addRange(value, length);
                }
                else
                {
                    // replace '\0' position.
                    _buffer.set(_buffer.count()-1, value[0]);
                    _buffer.addRange(value + 1, length - 1);
                }
            }
            _buffer.add('\0');
        }
    }
    void String::addString(const string& value)
    {
        addString(value.c_str());
    }
    void String::setString(char value)
    {
        char buffer[2] = {value, '\0'};
        setString(buffer);
    }
    void String::addString(char value)
    {
        char buffer[2] = {value, '\0'};
        addString(buffer);
    }
    const char* String::getString() const
    {
        return _buffer.data();
    }
    
    String String::operator+=(const String& value)
    {
        append(value.getString());
        return *this;
    }
    String String::operator+=(const string& value)
    {
        append(value);
        return *this;
    }
    String String::operator+=(const char* value)
    {
        append(value);
        return *this;
    }
    String String::operator+(const String& value)
    {
        String result = getString();
        result.append(value.getString());
        return result;
    }
    String String::operator+(const string& value)
    {
        String result = getString();
        result.append(value);
        return result;
    }
    String String::operator+(const char* value)
    {
        String result = getString();
        result.append(value);
        return result;
    }
    
    void String::operator=(const String& value)
    {
        setString(value.getString());
    }
    void String::operator=(const string& value)
    {
        setString(value.c_str());
    }
    void String::operator=(const char* value)
    {
        setString(value);
    }
    bool String::operator==(const String& value) const
    {
        return equals(value);
    }
    bool String::operator!=(const String& value) const
    {
        return !operator==(value);
    }
    
    bool String::operator>(const String& value) const
    {
        return compare(*this, value) > 0;
    }
    bool String::operator>=(const String& value) const
    {
        return compare(*this, value) >= 0;
    }
    bool String::operator<(const String& value) const
    {
        return compare(*this, value) < 0;
    }
    bool String::operator<=(const String& value) const
    {
        return compare(*this, value) <= 0;
    }
    
    const char* String::c_str() const
    {
        return getString();
    }
    
    bool String::equals(const String& value, bool ignoreCase) const
    {
        return equals(*this, value, ignoreCase);
    }
    bool String::equals(const String& value1, const String& value2, bool ignoreCase)
    {
        return compare(value1, value2, ignoreCase) == 0;
    }
    bool String::stringEquals(const string& value1, const string& value2, bool ignoreCase)
    {
        return compare(value1, value2, ignoreCase) == 0;
    }
    int String::compare(const String& value1, const String& value2, bool ignoreCase)
    {
        if (!ignoreCase)
        {
            return strcmp(value1.getString(), value2.getString());
        }
        else
        {
#if WIN32
#define strcasecmp stricmp
#endif
            return strcasecmp(value1.getString(), value2.getString());
        }
    }
    
    void String::write(Stream* stream, int lengthCount) const
    {
        size_t length = this->length();
#ifdef DEBUG
        if (lengthCount == 2 && length > 0xFFFF)
        {
            assert(false);
        }
        else if (lengthCount == 1 && length > 0xFF)
        {
            assert(false);
        }
        else if (lengthCount == 4 && length > 0xFFFFFFFF)
        {
            assert(false);
        }
#endif //DEBUG
        int len = 0;
        if (lengthCount == 2)
        {
            len = (ushort)length;
            stream->writeUInt16((ushort)len);
        }
        else if (lengthCount == 1)
        {
            len = (byte)length;
            stream->writeByte((byte)len);
        }
        else if (lengthCount == 4)
        {
            len = (uint)length;
            stream->writeUInt32((uint)len);
        }
        else
        {
            assert(false);
        }
        
        stream->write((byte*)getString(), 0, len);
    }
    void String::read(Stream* stream, int lengthCount)
    {
        uint len = 0;
        if (lengthCount == 2)
        {
            len = stream->readUInt16();
        }
        else if (lengthCount == 1)
        {
            len = stream->readByte();
        }
        else if (lengthCount == 4)
        {
            len = stream->readUInt32();
        }
        else
        {
            assert(false);
        }
        
        readFixedLengthStr(stream, len);
    }
    void String::writeFixedLengthStr(Stream* stream, uint length)
    {
        size_t len = this->length();
        if (len < length)
        {
            stream->write((byte*)getString(), 0, (int)len);
            int count = length - (int)len;
            byte* buffer = new byte[count];
            memset(buffer, 0, count);
            stream->write(buffer, 0, count);
            delete[] buffer;
        }
        else
        {
            stream->write((byte*)getString(), 0, length);
        }
    }
    void String::readFixedLengthStr(Stream* stream, uint length)
    {
        char* str = new char[length + 1];
        memset(str, 0, length + 1);
        stream->read((byte*)str, 0, length);
        setString(str);
        delete[] str;
    }
    
    const String String::toLower()
    {
        if (isNullOrEmpty())
            return *this;
        
        const char* str = this->getString();
        uint len = length();
        char* result = new char[len + 1];
        memset(result, 0, len + 1);
        for (size_t i = 0; i < len; i++)
        {
            result[i] = (char)tolower(str[i]);
        }
        string resultStr = result;
        delete[] result;
        return resultStr;
    }
    const String String::toUpper()
    {
        if (isNullOrEmpty())
            return *this;
        
        const char* str = this->getString();
        uint len = length();
        char* result = new char[len + 1];
        memset(result, 0, len + 1);
        for (size_t i = 0; i < len; i++)
        {
            result[i] = (char)toupper(str[i]);
        }
        string resultStr = result;
        delete[] result;
        return resultStr;
    }
    bool String::isNullOrEmpty() const
    {
        return String::isNullOrEmpty(*this);
    }
    bool String::isNullOrEmpty(const String& value)
    {
        // count == 0: nullptr; count == 1: ""
        return value._buffer.count() <= 1;
//        const char* str = value.getString();
//        if(str == nullptr || strlen(str) == 0)
//            return true;
//        return false;
    }
    uint String::length() const
    {
        if(isNullOrEmpty())
            return 0;
        return _buffer.count() - 1;
//        const char* str = this->getString();
//        if(str == nullptr)
//            return 0;
//        return (uint)strlen(str);
    }
    void String::empty()
    {
        _buffer.clear();
    }
    
    String::operator const char*() const
    {
        return getString();
    }
    
    const String String::trim(const char symbol1, const char symbol2, const char symbol3, const char symbol4)
    {
        String str;
        for (uint i = 0; i < length(); i++)
        {
            char ch = _buffer[i];
            if (!(ch == symbol1 || ch == symbol2 || ch == symbol3 || ch == symbol4))
                str.append(ch);
        }
        return str;
    }
    const String String::trimStart(const char symbol1, const char symbol2, const char symbol3, const char symbol4)
    {
        if (isNullOrEmpty())
            return *this;
        
        String str;
        for (uint i = 0; i < length(); i++)
        {
            char ch = _buffer[i];
            if (!((ch == symbol1 || ch == symbol2 || ch == symbol3 || ch == symbol4) && i == 0))
                str.append(ch);
        }
        return str;
    }
    const String String::trimEnd(const char symbol1, const char symbol2, const char symbol3, const char symbol4)
    {
        if (isNullOrEmpty())
            return *this;
        
        String str;
        for (uint i = 0; i < length(); i++)
        {
            char ch = _buffer[i];
            if (!((ch == symbol1 || ch == symbol2 || ch == symbol3 || ch == symbol4) && i == length()-1))
                str.append(ch);
        }
        return str;
    }
    
    const String String::GBKtoUTF8() const
    {
        return GBKtoUTF8(*this);
    }
    const String String::UTF8toGBK() const
    {
        return UTF8toGBK(*this);
    }
    
    const String String::GBKtoUTF8(const String& value)
    {
        if (value.isNullOrEmpty())
            return value;
        
        return encoding("GBK", "UTF-8", value.getString());
    }
    const String String::UTF8toGBK(const String& value)
    {
        if (value.isNullOrEmpty())
            return value;
        
        return encoding("UTF-8", "GBK", value.getString());
    }
    
    const String String::encoding(const char* fromCode, const char* toCode, const char* str)
    {
        if (str == NULL || strlen(str) == 0)
            return Empty;
        
        string result;
        int length = 0;
        char* buffer = new char[strlen(str) * 4];
        if (encoding(fromCode, toCode, str, buffer, length))
        {
            result = buffer;
        }
        else
        {
            result = str;
        }
        delete[] buffer;
        return result;
    }
    bool String::encoding(const char* fromCode, const char* toCode, const char* str, char*& buffer, int& length)
    {
        if (str == NULL || strlen(str) == 0)
            return false;
        
        length = 0;
        iconv_t cd = iconv_open(toCode, fromCode);
        if (cd != (iconv_t)-1)
        {
            const char* inbuf = str;
            int inlen = (int)strlen(inbuf);
            char *outbuf = (char *)malloc(inlen * 4);
            memset(outbuf, 0, inlen * 4);
#if _LIBICONV_VERSION > 0x010B
            const char *in = inbuf;
#else
            char *in = (char*)inbuf;
#endif
            char *out = outbuf;
            size_t outlen = inlen * 4;
            iconv(cd, &in, (size_t *)&inlen, &out, &outlen);
            outlen = strlen(outbuf);
            
            strcpy(buffer, outbuf);
            length = (int)outlen;
            
            free(outbuf);
            iconv_close(cd);
        }
        return length > 0;
    }
    
    const String String::toBase64() const
    {
        return String::toBase64(*this);
    }
    const String String::toBase64(const String& value)
    {
        if (value.isNullOrEmpty())
            return value;
        
        return toBase64((const byte*)value.getString(), 0, value.length());
    }
    int String::toBase64_CalculateAndValidateOutputLength(int inputLength, bool insertLineBreaks)
    {
        long outlen = ((long)inputLength) / 3 * 4;          // the base length - we want integer division here.
        outlen += ((inputLength % 3) != 0) ? 4 : 0;         // at most 4 more chars for the remainder
        
        if (outlen == 0)
            return 0;
        
        if (insertLineBreaks) {
            long newLines = outlen / base64LineBreakPosition;
            if ((outlen % base64LineBreakPosition) == 0) {
                --newLines;
            }
            outlen += newLines * 2;              // the number of line break chars we'll add, "\r\n"
        }
        
        return (int)outlen;
    }
    int String::convertToBase64Array(char* outChars, const byte* inData, int offset, int length, bool insertLineBreaks)
    {
        int lengthmod3 = length % 3;
        int calcLength = offset + (length - lengthmod3);
        int j = 0;
        int charcount = 0;
        //Convert three bytes at a time to base64 notation.  This will consume 4 chars.
        int i;
        
        // get a pointer to the base64Table to avoid unnecessary range checking
        const char* base64 = base64Table;
        for (i = offset; i < calcLength; i += 3)
        {
            if (insertLineBreaks) {
                if (charcount == base64LineBreakPosition) {
                    outChars[j++] = '\r';
                    outChars[j++] = '\n';
                    charcount = 0;
                }
                charcount += 4;
            }
            outChars[j] = base64[(inData[i] & 0xfc) >> 2];
            outChars[j + 1] = base64[((inData[i] & 0x03) << 4) | ((inData[i + 1] & 0xf0) >> 4)];
            outChars[j + 2] = base64[((inData[i + 1] & 0x0f) << 2) | ((inData[i + 2] & 0xc0) >> 6)];
            outChars[j + 3] = base64[(inData[i + 2] & 0x3f)];
            j += 4;
        }
        
        //Where we left off before
        i = calcLength;
        
        if (insertLineBreaks && (lengthmod3 != 0) && (charcount == base64LineBreakPosition)) {
            outChars[j++] = '\r';
            outChars[j++] = '\n';
        }
        
        switch (lengthmod3)
        {
            case 2: //One character padding needed
                outChars[j] = base64[(inData[i] & 0xfc) >> 2];
                outChars[j + 1] = base64[((inData[i] & 0x03) << 4) | ((inData[i + 1] & 0xf0) >> 4)];
                outChars[j + 2] = base64[(inData[i + 1] & 0x0f) << 2];
                outChars[j + 3] = base64[64]; //Pad
                j += 4;
                break;
            case 1: // Two character padding needed
                outChars[j] = base64[(inData[i] & 0xfc) >> 2];
                outChars[j + 1] = base64[(inData[i] & 0x03) << 4];
                outChars[j + 2] = base64[64]; //Pad
                outChars[j + 3] = base64[64]; //Pad
                j += 4;
                break;
        }
        
        return j;
    }
    bool String::toBase64(const byte* inArray, int offset, int length, String& str, Base64FormattingOptions options)
    {
        //		int inA-rrayLength;
        int stringLength;
        
        //		inArrayLength = length;
        
        bool insertLineBreaks = (options == String::InsertLineBreaks);
        //Create the new string.  This is the maximally required length.
        stringLength = toBase64_CalculateAndValidateOutputLength(length, insertLineBreaks);
        if (stringLength > 0)
        {
            char* outChars = new char[stringLength + 1];
            memset(outChars, 0, stringLength + 1);
            int j = convertToBase64Array(outChars, inArray, offset, length, insertLineBreaks);
            str.append(outChars, stringLength);
            delete[] outChars;
            return j > 0;
        }
        return false;
    }
    const String String::toBase64(const byte* inArray, int offset, int length, Base64FormattingOptions options)
    {
        String str;
        if (toBase64(inArray, offset, length, str, options))
            return str;
        return Empty;
    }
    
    const String String::fromBase64() const
    {
        return String::fromBase64(*this);
    }
    const String String::fromBase64(const String& value)
    {
        if (value.isNullOrEmpty())
            return value;
        
        ByteArray array;
        if (fromBase64(value, array))
        {
            return String((const char*)array.data());
        }
        return Empty;
    }
    bool String::fromBase64(const String& value, ByteArray& array)
    {
        if (value.isNullOrEmpty())
            return false;
        
        return fromBase64(value.getString(), value.length(), array);
    }
    bool String::fromBase64(const char* inputPtr, int inputLength, ByteArray& array)
    {
        // The validity of parameters much be checked by callers, thus we are Critical here.
        
        assert(0 <= inputLength);
        
        // We need to get rid of any trailing white spaces.
        // Otherwise we would be rejecting input such as "abc= ":
        while (inputLength > 0)
        {
            int lastChar = inputPtr[inputLength - 1];
            if (lastChar != (int) ' ' && lastChar != (int) '\n' && lastChar != (int) '\r' && lastChar != (int) '\t')
                break;
            inputLength--;
        }
        
        // Compute the output length:
        int resultLength = fromBase64_ComputeResultLength(inputPtr, inputLength);
        
        if (0 <= resultLength)
        {
            // resultLength can be zero. We will still enter FromBase64_Decode and process the input.
            // It may either simply write no bytes (e.g. input = " ") or throw (e.g. input = "ab").
            
            // Create result byte blob:
            byte* decodedBytes = new byte[resultLength];
            
            // Convert Base64 chars into bytes:
            int actualResultLength = fromBase64_Decode(inputPtr, inputLength, decodedBytes, resultLength);
            array.addRange(decodedBytes, resultLength);
            delete[] decodedBytes;
            
            return actualResultLength > 0;
        }
        // Note that actualResultLength can differ from resultLength if the caller is modifying the array
        // as it is being converted. Silently ignore the failure.
        // Consider throwing exception in an non in-place release.
        
        // We are done:
        return false;
    }
    /// <summary>
    /// Compute the number of bytes encoded in the specified Base 64 char array:
    /// Walk the entire input counting white spaces and padding chars, then compute result length
    /// based on 3 bytes per 4 chars.
    /// </summary>
    int String::fromBase64_ComputeResultLength(const char* inputPtr, int inputLength)
    {
        const uint intEq = (uint) '=';
        const uint intSpace = (uint) ' ';
        
        assert(0 <= inputLength);
        
        const char* inputEndPtr = inputPtr + inputLength;
        int usefulInputLength = inputLength;
        int padding = 0;
        
        while (inputPtr < inputEndPtr) {
            
            uint c = (uint)(*inputPtr);
            inputPtr++;
            
            // We want to be as fast as possible and filter out spaces with as few comparisons as possible.
            // We end up accepting a number of illegal chars as legal white-space chars.
            // This is ok: as soon as we hit them during actual decode we will recognise them as illegal and throw.
            if (c <= intSpace)
                usefulInputLength--;
            
            else if (c == intEq) {
                usefulInputLength--;
                padding++;
            }
        }
        
        assert(0 <= usefulInputLength);
        
        // For legal input, we can assume that 0 <= padding < 3. But it may be more for illegal input.
        // We will notice it at decode when we see a '=' at the wrong place.
        assert(0 <= padding);
        
        // Perf: reuse the variable that stored the number of '=' to store the number of bytes encoded by the
        // last group that contains the '=':
        if (padding != 0) {
            
            if (padding == 1)
                padding = 2;
            else if (padding == 2)
                padding = 1;
            else
                return 0;// throw Exception("Format_BadBase64Char");
        }
        
        // Done:
        return (usefulInputLength / 4) * 3 + padding;
    }
    /// <summary>
    /// Decode characters representing a Base64 encoding into bytes:
    /// Walk the input. Every time 4 chars are read, convert them to the 3 corresponding output bytes.
    /// This method is a bit lengthy on purpose. We are trying to avoid jumps to helpers in the loop
    /// to aid performance.
    /// </summary>
    /// <param name="inputPtr">Pointer to first input char</param>
    /// <param name="inputLength">Number of input chars</param>
    /// <param name="destPtr">Pointer to location for teh first result byte</param>
    /// <param name="destLength">Max length of the preallocated result buffer</param>
    /// <returns>If the result buffer was not large enough to write all result bytes, return -1;
    /// Otherwise return the number of result bytes actually produced.</returns>
    int String::fromBase64_Decode(const char* startInputPtr, int inputLength, byte* startDestPtr, int destLength)
    {
        // You may find this method weird to look at. It's written for performance, not aesthetics.
        // You will find unrolled loops label jumps and bit manipulations.
        
        const uint intA = (uint) 'A';
        const uint inta = (uint) 'a';
        const uint int0 = (uint) '0';
        const uint intEq = (uint) '=';
        const uint intPlus = (uint) '+';
        const uint intSlash = (uint) '/';
        const uint intSpace = (uint) ' ';
        const uint intTab = (uint) '\t';
        const uint intNLn = (uint) '\n';
        const uint intCRt = (uint) '\r';
        const uint intAtoZ = (uint)('Z' - 'A');  // = ('z' - 'a')
        const uint int0to9 = (uint)('9' - '0');
        
        const char* inputPtr = startInputPtr;
        byte* destPtr = startDestPtr;
        
        // Pointers to the end of input and output:
        const char* endInputPtr = inputPtr + inputLength;
        byte* endDestPtr = destPtr + destLength;
        
        // Current char code/value:
        uint currCode;
        
        // This 4-byte integer will contain the 4 codes of the current 4-char group.
        // Eeach char codes for 6 bits = 24 bits.
        // The remaining byte will be FF, we use it as a marker when 4 chars have been processed.
        uint currBlockCodes = 0x000000FFu;
        
        while (true)
        {
            // break when done:
            if (inputPtr >= endInputPtr)
                goto _AllInputConsumed;
            
            // Get current char:
            currCode = (uint)(*inputPtr);
            inputPtr++;
            
            // Determine current char code:
            
            if (currCode - intA <= intAtoZ)
                currCode -= intA;
            
            else if (currCode - inta <= intAtoZ)
                currCode -= (inta - 26u);
            
            else if (currCode - int0 <= int0to9)
                currCode -= (int0 - 52u);
            
            else {
                // Use the slower switch for less common cases:
                switch (currCode) {
                        
                        // Significant chars:
                    case intPlus:  currCode = 62u;
                        break;
                        
                    case intSlash: currCode = 63u;
                        break;
                        
                        // Legal no-value chars (we ignore these):
                    case intCRt:  case intNLn:  case intSpace:  case intTab:
                        continue;
                        
                        // The equality char is only legal at the end of the input.
                        // Jump after the loop to make it easier for the JIT register predictor to do a good job for the loop itself:
                    case intEq:
                        goto _EqualityCharEncountered;
                        
                        // Other chars are illegal:
                    default:
                        return 0;	// throw Exception("Format_BadBase64Char");
                }
            }
            
            // Ok, we got the code. Save it:
            currBlockCodes = (currBlockCodes << 6) | currCode;
            
            // Last bit in currBlockCodes will be on after in shifted right 4 times:
            if ((currBlockCodes & 0x80000000u) != 0u) {
                
                if ((int)(endDestPtr - destPtr) < 3)
                    return -1;
                
                *(destPtr) = (byte)(currBlockCodes >> 16);
                *(destPtr + 1) = (byte)(currBlockCodes >> 8);
                *(destPtr + 2) = (byte)(currBlockCodes);
                destPtr += 3;
                
                currBlockCodes = 0x000000FFu;
            }
            
        }  // while
        
        // 'd be nice to have an assert that we never get here, but CS0162: Unreachable code detected.
        // Contract.Assert(false, "We only leave the above loop by jumping; should never get here.");
        
        // We jump here out of the loop if we hit an '=':
    _EqualityCharEncountered:
        
        // Recall that inputPtr is now one position past where '=' was read.
        // '=' can only be at the last input pos:
        if (inputPtr == endInputPtr) {
            
            // Code is zero for trailing '=':
            currBlockCodes <<= 6;
            
            // The '=' did not complete a 4-group. The input must be bad:
            if ((currBlockCodes & 0x80000000u) == 0u)
                return 0;	// throw Exception("Format_BadBase64CharArrayLength");
            
            if ((int)(endDestPtr - destPtr) < 2)  // Autch! We underestimated the output length!
                return -1;
            
            // We are good, store bytes form this past group. We had a single "=", so we take two bytes:
            *(destPtr++) = (byte)(currBlockCodes >> 16);
            *(destPtr++) = (byte)(currBlockCodes >> 8);
            
            currBlockCodes = 0x000000FFu;
            
        }
        else { // '=' can also be at the pre-last position iff the last is also a '=' excluding the white spaces:
            
            // We need to get rid of any intermediate white spaces.
            // Otherwise we would be rejecting input such as "abc= =":
            while (inputPtr < (endInputPtr - 1))
            {
                int lastChar = *(inputPtr);
                if (lastChar != (int) ' ' && lastChar != (int) '\n' && lastChar != (int) '\r' && lastChar != (int) '\t')
                    break;
                inputPtr++;
            }
            
            if (inputPtr == (endInputPtr - 1) && *(inputPtr) == '=') {
                
                // Code is zero for each of the two '=':
                currBlockCodes <<= 12;
                
                // The '=' did not complete a 4-group. The input must be bad:
                if ((currBlockCodes & 0x80000000u) == 0u)
                    return 0;	// throw Exception("Format_BadBase64CharArrayLength");
                
                if ((int)(endDestPtr - destPtr) < 1)  // Autch! We underestimated the output length!
                    return -1;
                
                // We are good, store bytes form this past group. We had a "==", so we take only one byte:
                *(destPtr++) = (byte)(currBlockCodes >> 16);
                
                currBlockCodes = 0x000000FFu;
                
            }
            else  // '=' is not ok at places other than the end:
                return 0;	// throw Exception("Format_BadBase64Char");
            
        }
        
        // We get here either from above or by jumping out of the loop:
    _AllInputConsumed:
        
        // The last block of chars has less than 4 items
        if (currBlockCodes != 0x000000FFu)
            return 0;	// throw Exception("Format_BadBase64CharArrayLength");
        
        // Return how many bytes were actually recovered:
        return (int)(destPtr - startDestPtr);
        
    } // int FromBase64_Decode(...)
    
    int String::find(const char* substring) const
    {
        if(isNullOrEmpty())
            return -1;
        
        const char *first = strstr(getString(), substring);
        if (NULL == first) return -1;
        return (int)(first - &getString()[0]);
    }
    bool String::contains(const char* substring) const
    {
        return find(substring) != -1;
    }
    bool String::contains(const char subchar) const
    {
        char temp[2];
        temp[0] = subchar;
        temp[1] = '\0';
        return contains(temp);
    }
    
    void String::append(const char ch)
    {
        addString(ch);
    }
    void String::append(const char* str, uint count)
    {
//        addString(str);
        _buffer.addRange(str, count);
        _buffer.add('\0');
    }
    void String::append(const string& str)
    {
        addString(str);
    }
    void String::appendLine(const char ch)
    {
        append(ch);
        append(NewLine);
    }
    void String::appendLine(const char* str, uint count)
    {
        append(str, count);
        append(NewLine);
    }
    void String::appendLine(const string& str)
    {
        append(str);
        append(NewLine);
    }
    
    const String String::replace(const String& src, const String& dst)
    {
        String str = replace(*this, src, dst);
        setString(str);
        return str;
    }
    const String String::replace(const String& str, const String& src, const String& dst)
    {
        if(str.isNullOrEmpty())
            return str;
        
        // todo: use char to replace string.
        string strBig = str.getString();
        std::string::size_type pos = 0;
        while( (pos = strBig.find(src, pos)) != string::npos)
        {
            strBig.replace(pos, src.length(), dst);
            pos += dst.length();
        }
        return strBig;
    }
    const String String::substr(int offset, uint count)
    {
        return substr(*this, offset, count);
    }
    const String String::substr(const String& str, int offset, uint count)
    {
        if(str.isNullOrEmpty())
            return Empty;
        uint strLength = str.length();
        if(offset < 0 || offset >= (int)strLength)
            return Empty;
        if(count >= strLength)
            return Empty;
        
        String result;
        result._buffer.addRange(str.c_str(), offset, count);
        result._buffer.add('\0');
        return result;
    }
    
    const String String::convert(const char *format, ...)
    {
        char* message = new char[MaxFormatStrLength];
        memset(message, 0, MaxFormatStrLength);
        va_list ap;
        va_start(ap, format);
        vsprintf(message, format, ap);
        va_end(ap);
        string result = message;
        delete[] message;
        return result;
    }
    
    Uuid::Uuid()
    {
    	clear();
    }
    Uuid::Uuid(const Uuid& value)
    {
        this->operator=(value);
    }
    Uuid::Uuid(const string& value)
    {
        parse(value, *this);
    }
    Uuid::~Uuid()
    {
	}
    
    void Uuid::operator=(const Uuid& value)
    {
#if WIN32
		memcpy(&this->_value, &value._value, Size);
#else
        uuid_copy(this->_value, value._value);
#endif
        assert(this->operator==(value));
	}
    bool Uuid::operator==(const Uuid& value) const
    {
#if WIN32
		RPC_STATUS status;
		return UuidCompare((uuid_t*)&this->_value, (uuid_t*)&value._value, &status) == 0;
#else
        return uuid_compare(this->_value, value._value) == 0;
#endif
	}
    bool Uuid::operator!=(const Uuid& value) const
    {
        return !operator==(value);
    }
    
    bool Uuid::operator>(const Uuid& value) const
    {
#if WIN32
        RPC_STATUS status;
        return UuidCompare((uuid_t*)&this->_value, (uuid_t*)&value._value, &status) > 0;
#else
        return uuid_compare(this->_value, value._value) > 0;
#endif
    }
    bool Uuid::operator>=(const Uuid& value) const
    {
#if WIN32
        RPC_STATUS status;
        return UuidCompare((uuid_t*)&this->_value, (uuid_t*)&value._value, &status) >= 0;
#else
        return uuid_compare(this->_value, value._value) >= 0;
#endif
    }
    bool Uuid::operator<(const Uuid& value) const
    {
#if WIN32
        RPC_STATUS status;
        return UuidCompare((uuid_t*)&this->_value, (uuid_t*)&value._value, &status) < 0;
#else
        return uuid_compare(this->_value, value._value) <= 0;
#endif
    }
    bool Uuid::operator<=(const Uuid& value) const
    {
#if WIN32
        RPC_STATUS status;
        return UuidCompare((uuid_t*)&this->_value, (uuid_t*)&value._value, &status) <= 0;
#else
        return uuid_compare(this->_value, value._value) <= 0;
#endif
    }
    
    void Uuid::write(Stream* stream) const
    {
        assert(!isEmpty());
#if WIN32
		stream->writeUInt32(_value.Data1, true);
		stream->writeUInt16(_value.Data2, true);
		stream->writeUInt16(_value.Data3, true);
		stream->write((byte*)&_value.Data4, 0, sizeof(_value.Data4));
#else
        stream->write(_value, 0, Size);
#endif
	}
    void Uuid::read(Stream* stream)
    {
#if WIN32
		_value.Data1 = stream->readUInt32(true);
		_value.Data2 = stream->readUInt16(true);
		_value.Data3 = stream->readUInt16(true);
		stream->read((byte*)&_value.Data4, 0, sizeof(_value.Data4));
#else
		stream->read(_value, 0, Size);
#endif
		assert(!isEmpty());
    }
    
    bool Uuid::isEmpty() const
    {
#if WIN32
		RPC_STATUS status;
		return UuidIsNil((uuid_t*)&_value, &status) != 0;
#else
        return uuid_is_null(_value) == 1;
#endif
    }
    void Uuid::clear()
    {
#if WIN32
        UuidCreateNil(&_value);
#else
        uuid_clear(_value);
#endif
    }
    
    String Uuid::toString() const
    {
#if WIN32
		char* buffer;
		if (UuidToString(&_value, (RPC_CSTR*)&buffer) == RPC_S_OK)
		{
			string str = (string)buffer;
			RpcStringFree((RPC_CSTR*)&buffer);
			return str;
		}
        return String::Empty;
#else
        char buffer[64];
        uuid_unparse_upper(_value, buffer);
        return (String)buffer;
//        uuid_string_t str;
//        uuid_unparse(_value, str);
//        return (string)str;
#endif
	}
    bool Uuid::parse(const String& str, Uuid& value)
    {
        if(str.isNullOrEmpty())
            return false;
#if WIN32
		return (UuidFromString((RPC_CSTR)str.c_str(), &value._value) == RPC_S_OK);
#else
        return uuid_parse(str, value._value) == 0;
#endif
	}

	Uuid Uuid::generate()
	{
		Uuid uuid;
#if WIN32
		UuidCreate(&uuid._value);
#else
		uuid_generate(uuid._value);
#endif
		assert(!uuid.isEmpty());
		return uuid;
	}
}
