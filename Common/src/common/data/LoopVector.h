#ifndef LOOPVECTOR_H
#define LOOPVECTOR_H

#include <stdio.h>
#include <string.h>
#include "common/common_global.h"

namespace Common
{
	template <class type>
	class LoopVector
	{
	public:
		LoopVector(int maxLength = DefaultMaxLength, bool autoDelete = true)
		{
			_maxLength = maxLength <= 0 ? 1 : maxLength;
			_array = new type*[_maxLength];
			setAutoDelete(autoDelete);

			makeNullInner(false);
		}
		~LoopVector()
		{
			makeNull();
			delete[] _array;
			_array = NULL;
		}
		void makeNull(bool autoDelete = true)
		{
			makeNullInner(autoDelete && _autoDelete);
		}
		void enqueue(type* value)
		{
			bool f = full();
			int index = _rear >= _maxLength ? 0 : _rear;
			if(f)
			{
				if(_autoDelete)
				{
					delete _array[index];
				}
			}
			_array[index] = value;
			_rear = index + 1;
			if (f)
			{
				_front++;
				if (_front >= _maxLength)
					_front = 0;
			}
		}
		bool dequeue()
		{
			if (empty())
			{
				return false;
			}

			if(_autoDelete)
			{
				delete _array[_front];
				_array[_front] = NULL;
			}
			_front++;
			if (_front >= _maxLength)
				_front = 0;

			// _front = _rear = 0
			if (_front == _rear || (_front == 0 && _rear == _maxLength))
				makeNull();

			return true;
		}
		type* front()
		{
			if (empty())
			{
				return NULL;
			}
			return _array[_front];
		}
		inline bool empty() const
		{
			return (_front == 0 && _rear == 0);
		}
		inline bool full() const
		{
			return count() >= (uint)_maxLength;
		}
		inline uint count() const
		{
			if (empty())
				return 0;

			return (_rear > _front) ? _rear - _front : _rear + _maxLength - _front;
		}
		void copyTo(type** value)
		{
			if (!empty())
			{
				if (_rear > _front)
				{
					memcpy(value, _array+_front, count()*sizeof(type*));
				}
				else
				{
					int n = 0;
					if (_front < _maxLength)
					{
						n = _maxLength - _front;
						memcpy(value, _array+_front, n*sizeof(type*));
					}
					memcpy(value+n, _array, (count() - n)*sizeof(type*));
				}
			}
		}
        type* at(uint i) const
        {
            if (!empty() && i < count())
            {
                if (_rear > _front)
                {
                    return _array[_front+i];
                }
                else
                {
                    if (_front+i < _maxLength)
                    {
                        return _array[_front+i];
                    }
                    return _array[_rear-(count()-i)];
                }
            }
            return NULL;
        }
		inline void setAutoDelete(bool autoDelete)
		{
			_autoDelete = autoDelete;
		}
		inline bool autoDelete() const
		{
			return _autoDelete;
		}
        inline int maxLength() const
        {
            return _maxLength;
        }
        inline void setMaxLength(int maxLength)
        {
            _maxLength = maxLength;
        }
        inline void clear()
        {
            makeNull();
        }

	private:
		void makeNullInner(bool autoDelete = true)
		{
			if(autoDelete)
			{
				deleteAllItems();
			}
			else
			{
				memset(_array, 0, sizeof(type*)*(_maxLength));
			}
			_front = 0;
			_rear = 0;
		}
		void deleteAllItems()
		{
			if (!empty())
			{
				if (_rear > _front)
				{
					deleteItem(_front, count());
				}
				else
				{
					int n = 0;
					if (_front < _maxLength)
					{
						n = _maxLength - _front;
						deleteItem(_front, n);
					}
					deleteItem(0, (count() - n));
				}
			}
		}
		void deleteItem(int start, int end)
		{
			for (int i = start; i < end; i++)
			{
				delete _array[i];
				_array[i] = NULL;
			}
		}
        
    protected:
        const static int DefaultMaxLength = 1024;

	private:
		int _maxLength;
		int _front;
		int _rear;
		type** _array;
		bool _autoDelete;
	};
}
#endif // LOOPVECTOR_H
