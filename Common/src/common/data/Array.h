#ifndef ARRAY_H
#define ARRAY_H

#include <stdio.h>
#include <memory.h>

#ifndef uint
typedef unsigned uint;
#endif

namespace Common
{
    template <class type>
    class Array
    {
    public:
        Array(uint capacity = DefaultCapacity) : _array(nullptr), _capacity(0), _count(0)
        {
            setCapacity(capacity > 0 ? capacity : DefaultCapacity);
        }
        Array(const Array* array) : Array(array->capacity())
        {
            addRange(array);
        }
        Array(const type* array, uint count, uint capacity = DefaultCapacity) : Array(capacity)
        {
            addRange(array, count);
        }
        virtual ~Array()
        {
            clearInner();
        }
        inline uint count() const
        {
            return _count;
        }
        inline uint capacity() const
        {
            return _capacity;
        }
        inline void setCapacity(uint capacity = DefaultCapacity)
        {
            if(capacity != _capacity)
            {
                if(count() == 0)
                {
                    _capacity = capacity;
                    makeNull();
                }
                else
                {
                    // have data.
                    uint size = this->size(_count, capacity);
                    type* temp = new type[size];
                    memcpy(temp, data(), sizeof(type)*_count);
                    
                    _capacity = capacity;
                    clearInner();
                    
                    _array = temp;
                }
            }
        }
        inline type at(uint i) const
        {
            if (i < _count)
            {
                return _array[i];
            }
            return type();
        }
        inline type operator[](uint i) const
        {
            return this->at(i);
        }
        inline void add(const type d)
        {
            if (canResize())
            {
                autoResize();
            }
            _array[_count++] = (type)d;
        }
        inline void addRange(const Array* array)
        {
            addRange(array->_array, array->count());
        }
        inline void addRange(const Array* array, int offset, uint count)
        {
            if(offset + count > array->count())
                return;
            
            addRange(array->_array, offset, count);
        }
        inline void addRange(const type* array, uint count)
        {
            if(count > 0)
            {
                uint c = count;
                if(_count + c > this->size())
                {
                    type* temp = _array;
                    uint size = this->size(_count + c);
                    _array = new type[size];
                    memcpy(_array, temp, sizeof(type)*(_count));
                    memcpy(_array + _count, array, sizeof(type)*(c));
                    memset(_array + (_count + c), 0, sizeof(type)*(size - _count - c));
                    delete[] temp;
                }
                else
                {
                    if (canResize())
                    {
                        autoResize();
                    }
                    memcpy(_array + _count, array, sizeof(type)*(c));
                }
                _count += c;
            }
        }
        inline void addRange(const type* array, int offset, uint count)
        {
            addRange(array + offset, count);
        }
        inline bool insertRange(uint i, const Array* array)
        {
            return insertRange(i, array->_array, array->count());
        }
        inline bool insertRange(uint i, const type* array, uint count)
        {
            if (count > 0 && i <= _count)
            {
                uint c = count;
                uint size = this->size(_count + c);
                type* temp = _array;
                _array = new type[size];
                memcpy(_array, temp, sizeof(type)*(i));
                memcpy(_array + i, array, sizeof(type)*(c));
                memcpy(_array + (i + c), temp + i, sizeof(type)*(_count - i));
                memset(_array + (_count + i + c), 0, sizeof(type)*(size - _count - i - c));
                delete[] temp;
                _count += c;
                
                return true;
            }
            return false;
        }
        inline bool insertRange(uint i, const type* array, int offset, uint count)
        {
            return insertRange(i, array + offset, count);
        }
        inline bool insert(uint i, const type d)
        {
            if (i <= _count)
            {
                if (canResize())
                {
                    autoResize();
                }
                
                type* temp = _array;
                _array = new type[size()];
                memcpy(_array, temp, sizeof(type)*(i));
                _array[i] = (type)d;
                memcpy(_array + (i + 1), temp + i, sizeof(type)*(_count - i));
                delete[] temp;
                _count++;
                return true;
            }
            return false;
        }
        inline bool setRange(uint i, const type* array, uint count)
        {
            if (i < _count && i + count < _count)
            {
                memcpy(_array + i, array, count);
                return true;
            }
            return false;
        }
        inline bool set(uint i, const type d, bool insertEmpty = false)
        {
            if (i < _count)
            {
                _array[i] = (type)d;
                return true;
            }
            
            if(insertEmpty)
            {
                uint c = (i / capacity() - count() / capacity()) * capacity();
                if(c > 0)
                {
                    autoResize(c);
                }
                _array[i] = (type)d;
                _count = i + 1;
                return true;
            }
            else
            {
                return false;
            }
        }
        inline bool remove(const type d)
        {
            for (uint i = 0; i < count(); i++)
            {
                if (_array[i] == d)
                {
                    return removeAt(i);
                }
            }
            return false;
        }
        inline bool removeAt(uint i)
        {
            if (i < _count)
            {
                if (i != _count - 1)	// except the last one.
                {
                    memmove(_array + i, _array + i + 1, sizeof(type)*(_count - i - 1));
                    memset(_array + _count - 1, 0, sizeof(type)* 1);
                }
                _count--;
                return true;
            }
            return false;
        }
        inline bool removeAt(uint from, uint to)
        {
            if (from >= 0 && to < _count && to >= from)
            {
                uint rcount = to - from + 1;
                if (to != _count - 1)	// except the last one.
                {
                    memmove(_array + from, _array + to + 1, sizeof(type)*(_count - to - 1));
                    memset(_array + _count - rcount, 0, sizeof(type)* rcount);
                }
                _count -= rcount;
                return true;
            }
            return false;
        }
        inline void clear()
        {
            clearInner();
            
            makeNull();
        }
        inline bool contains(const type d)
        {
            for (uint i = 0; i < count(); i++)
            {
                if (_array[i] == d)
                {
                    return true;
                }
            }
            return false;
        }
        inline type* data() const
        {
            return _array;
        }
        void operator=(const Array& value)
        {
            _capacity = value.capacity();
            clear();
            addRange(&value);
        }
        bool operator==(const Array& value) const
        {
            if(count() != value.count())
                return false;
            
            for (uint i=0; i<count(); i++)
            {
                if(at(i) != value.at(i))
                    return false;
            }
            return false;
        }
        bool operator!=(const Array& value) const
        {
            return !operator==(value);
        }
        
        // type must be overide operator<= and operator>=
        void quicksort()
        {
            quicksort(_array, 0, (int)count()-1);
        }
        // type must be overide operator<= and operator>=
        void quicksort(int low, int high)
        {
            if(count() > 1 && low >= 0 && high < count() && low < high)
                quicksort(_array, low, high);
        }
        // type must be overide operator<= and operator>=
        static void quicksort(type array[], int low, int high)
        {
            if(low >= high)
            {
                return;
            }
            int first = low;
            int last = high;
            type key = array[first];
            
            while(first < last)
            {
                while(first < last && array[last] >= key)
                {
                    --last;
                }
                
                array[first] = array[last];
                
                while(first < last && array[first] <= key)
                {
                    ++first;
                }
                
                array[last] = array[first];
            }
            array[first] = key;
            quicksort(array, low, first-1);
            quicksort(array, first+1, high);
        }
        // type must be overide operator<= and operator>=
        static void quicksort(type array[], uint length)
        {
            quicksort(array, 0, (int)length-1);
        }
    private:
        inline void autoResize()
        {
            if (canResize())
            {
                autoResize(_capacity);
            }
        }
        inline void autoResize(uint capacity)
        {
            type* temp = _array;
            _array = new type[_count + capacity];
            memcpy((void*)_array, (void*)temp, sizeof(type)*(_count));
            memset((void*)(_array + _count), 0, sizeof(type)*(capacity));
            delete[] temp;
        }
        inline bool canResize() const
        {
            return (_count > 0 && (_count % _capacity) == 0);
        }
        inline void makeNull()
        {
            if(_array != nullptr)
            {
                clearInner();
            }
            _count = 0;
            _array = new type[_capacity];
            memset((void*)_array, 0, sizeof(type)*(_capacity));
        }
        inline void clearInner()
        {
            delete[] _array;
            _array = nullptr;
        }
        inline uint size() const
        {
            return size(count(), capacity());
        }
        inline uint size(uint count) const
        {
            return size(count, capacity());
        }
        static uint size(uint count, uint capacity)
        {
            uint size = (count / capacity) + 1;
            return size * capacity;
        }
        
    protected:
        const static int DefaultCapacity = 1024;
        
    private:
        type* _array;
        uint _capacity;
        uint _count;
    };
}

#endif // ARRAY_H
