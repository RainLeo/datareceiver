#include "Device.h"
#include "../../diag/Debug.h"
#include "../../exception/Exception.h"
#include "../instructions/Instruction.h"
#include "../instructions/InstructionSet.h"
#include "../channels/Channel.h"
#include "DeviceDescription.h"

namespace Driver
{
    Device::StatusSnap::StatusSnap()
    {
        OldStatus = Unknown;
        NewStatus = Unknown;
    }
    Device::StatusSnap::StatusSnap(Status status)
    {
        OldStatus = Unknown;
        NewStatus = status;
    }
    Device::StatusSnap::StatusSnap(Status oldStatus, Status newStatus)
    {
        OldStatus = oldStatus;
        NewStatus = newStatus;
    }
    
    void Device::StatusSnap::write(Common::Stream* stream) const
    {
        stream->writeByte(OldStatus);
        stream->writeByte(NewStatus);
    }
    void Device::StatusSnap::read(Common::Stream* stream)
    {
        OldStatus = (Status)stream->readByte();
        NewStatus = (Status)stream->readByte();
    }
    void Device::StatusSnap::copyFrom(const StatusSnap* status)
    {
        OldStatus = status->OldStatus;
        NewStatus = status->NewStatus;
    }
    
	Device::Device(DeviceDescription* dd, Channel* channel)
	{
		_description = dd;
		InstructionSet* is = instructionSet();
		if(is == NULL)
		{
			throw ArgumentException("Is must not be null.", "dd.InstructionSet");
		}
		_instructions = new Instructions();
		is->generateInstructions(_instructions);

		_channel = channel;
        
        Locker locker(&_receiveDevicesMutex);
        _receiveDevices.setAutoDelete(false);
	}
	Device::~Device(void)
	{
		_description = NULL;
		delete _instructions;

		_channel = NULL;
	}

	const string& Device::name() const
	{
		static string Empty = "";
		return _description != NULL ? _description->name() : Empty;
	}

	DeviceDescription* Device::description() const
	{
		return _description;
	}

	Channel* Device::getChannel() const
	{
		return _channel;
	}

	Instruction* Device::getInstruction(const string& instructionName) const
	{
		for (uint i = 0; i < _instructions->count(); i++)
		{
			Instruction* instruction = _instructions->at(i);
			if(instruction->description()->name() == instructionName)
			{
				return instruction;
			}
		}
		return NULL;
	}
    Instruction* Device::getInstruction(const InstructionDescription* id) const
    {
        return id != nullptr ? getInstruction(id->name()) : nullptr;
    }

	InstructionSet* Device::instructionSet() const
	{
		return _description != NULL ? _description->instructionSet() : NULL;
	}

	bool Device::receive(ByteArray* buffer)
	{
		return instructionSet()->receive(this, _channel, buffer);
	}
	Instruction* Device::matchInstruction(const ByteArray& buffer)
	{
		for (uint i = 0; i < _instructions->count(); i++)
		{
			Instruction* instruction = _instructions->at(i);
			if(instruction->match(&buffer))
			{
				return instruction;
			}
		}
		return NULL;
	}
    void Device::executeInstruction()
    {
        ByteArray buffer(128 * 1024);
        if (receive(&buffer))
        {
            Instruction* instruction = matchInstruction(buffer);
            if (instruction != NULL)
            {
                executeInstruction(instruction, instruction->context(), &buffer);
            }
        }
    }
	InstructionContext* Device::executeInstruction(InstructionDescription* id)
	{
        return executeInstruction(getInstruction(id), id->context());
	}

	InstructionContext* Device::executeInstruction(Instruction* instruction, InstructionContext* ic, const ByteArray* buffer)
	{
		if(instruction != NULL)
		{
			Channel* channel = getChannel();
			if(channel != NULL && channel->connected())
			{
//#ifdef DEBUG
//				Debug::writeFormatLine("Execute a instruction, device name: %s, instruction name: %s.",
//					name().c_str(), instruction->description()->name().c_str());
//#endif
				InstructionContext* oldic = instruction->context();
                instruction->setContext(ic);
				InstructionContext* result = instruction->execute(channel->interactive(), this, ic, buffer);
				instruction->setContext(oldic);
				return result;
			}
		}
		return NULL;
	}

    void Device::addReceiveDevice(Device* device)
    {
        Locker locker(&_receiveDevicesMutex);
        
        assert(device);
        assert(!_receiveDevices.contains(device));
        _receiveDevices.add(device);
        
        for (uint i=0; i<_instructions->count(); i++)
        {
            device->setReceiveInstruction(_instructions->at(i));
        }
    }
    void Device::setReceiveInstruction(Instruction* instruction)
    {
        for (uint i=0; i<_instructions->count(); i++)
        {
            Instruction* ri = _instructions->at(i);
            if(instruction->description()->name() == ri->description()->name())
            {
                instruction->setReceiveInstruction(ri);
                break;
            }
        }
    }
    void Device::removeReceiveDevice(Device* device)
    {
        Locker locker(&_receiveDevicesMutex);
        
        assert(device);
        _receiveDevices.remove(device);
    }
    
    void Device::setAllowLog(bool enable)
    {
        for (uint i = 0; i < _instructions->count(); i++)
        {
            Instruction* instruction = _instructions->at(i);
            instruction->setAllowLog(enable);
        }
    }
    
    string Device::getStatusStr(const Status status)
    {
        switch (status)
        {
            case Unknown:
                return "Unknown";
            case Offline:
                return "Offline";
            case Online:
                return "Online";
            default:
                break;
        }
        return "Unknown";
    }
    bool Device::isStatusChanged(Status oldStatus, Status newStatus)
    {
        return ((newStatus == Device::Online && (oldStatus == Device::Offline || oldStatus == Device::Unknown)) ||
                (newStatus == Device::Offline && oldStatus == Device::Online));
    }
}
