#include <assert.h>
#include "Sampler.h"
#include "../../thread/Locker.h"
#include "../../thread/TickTimeout.h"
#include "../../diag/Stopwatch.h"
#include "../../diag/Debug.h"
#include "common/Resources.h"
#include "../channels/ChannelDescription.h"
#include "DeviceDescription.h"

namespace Driver
{
    Sampler::Sampler(DriverManager* dm, ChannelDescription* cd, DeviceDescription* dd, bool skipSampler) : InstructionPool(dm, cd, dd)
    {
        _connected = Device::Unknown;
        _sampleStart = (uint)-1;    // the first sample value.
        
        _showConnectedError = true; // modified by HYC on 2015/10/22
        _isInvalidStatus = false;
        _checkOnlineFailedCount = 0;
        _connetedFailedCount = 0;
        _sampleInterval = detetionInterval();
        
        _skipSampler = skipSampler;
    }
    
    Sampler::~Sampler()
    {
        stop();

        _connected = Device::Unknown;
    }    
   
    void Sampler::addSampleInstruction()
    {
        // add the sample instruction into the looparray.
        if(_sampleStart == 0)
        {
            _sampleStart = TickTimeout::GetCurrentTickCount();
        }
        uint interval = _sampleInterval;
        if(_sampleStart == (uint)-1 ||
           TickTimeout::IsTimeout(_sampleStart, _sampleStart + interval))
        {
            if (_isInvalidStatus)
            {
                _isInvalidStatus = false;
                _sampleInterval = detetionInterval();
                
//#ifdef DEBUG
                if(_showConnectedError)
                    Trace::writeFormatLine(Resources::getString("RetryingConnectDevice").c_str(), _dd->name().c_str());
//#endif
            }
            _sampleStart = 0;
            addInstructionInner(sampleInstruction());
        }
    }

	void Sampler::instructionProcInner()
	{
		if (!_pause)
		{
			addSampleInstruction();
		}
		InstructionPool::instructionProcInner();
	}
    
    void Sampler::errorHandle(Channel* channel, bool error)
    {
        if (error)
        {
//#if DEBUG
            if(_showConnectedError)
                Trace::writeFormatLine(Resources::getString("UnabletoConnectDevice").c_str(), _dd->name().c_str());
//#endif
            
            _checkOnlineFailedCount++;
            
            if (_checkOnlineFailedCount >= detetionCount())
            {
                if(channel != NULL && channel->context()->reopened())
                {
                    channel->close();
                }
                setConnectStatus(Device::Offline);
//#if DEBUG
                if(_showConnectedError)
                    Trace::writeFormatLine(Resources::getString("DeviceFailure").c_str(), _dd->name().c_str());
//#endif
                
                _sampleStart = 0;
                _sampleInterval = resumeInterval();
                
                _isInvalidStatus = true;
                
                _checkOnlineFailedCount = 0;
                _connetedFailedCount++;
            }
        }
        else
        {
            setConnectStatus(Device::Online);
            
            _checkOnlineFailedCount = 0;
            _connetedFailedCount = 0;
            
            _sampleInterval = detetionInterval();
        }
    }
    
    void Sampler::setConnectedError(bool showError)
    {
        _showConnectedError = showError;
    }
}
