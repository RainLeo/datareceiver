#ifndef INSTRUCTIONPOOL_H
#define INSTRUCTIONPOOL_H

#include <mutex>
#include "common/common_global.h"
#include "../../data/LoopVector.h"
#include "../../thread/Thread.h"
#include "../channels/Channel.h"
#include "../channels/ChannelDescription.h"
#include "../instructions/InstructionDescription.h"
#include "Device.h"
#include "DeviceDescription.h"

using namespace std;
using namespace Common;

namespace Driver
{
    class DriverManager;
	class COMMON_EXPORT InstructionPool
	{
	public:
		class Packet
		{
		public:
            enum AutoDelete
            {
                None = 0,
                PacketAndInstruction = 1,
                PacketOnly = 2
            };
            
            Packet(InstructionDescription* id, AutoDelete autoDelete = PacketAndInstruction, byte priority = 0);
            Packet(DeviceDescription* dd, InstructionDescription* id, AutoDelete autoDelete = PacketAndInstruction, byte priority = 0);
            ~Packet();
            DeviceDescription* deviceDescription() const;
            InstructionDescription* getInstruction() const;
            void process(InstructionContext* ic = NULL);
            bool isProcessed() const;
            InstructionContext* getInstructionContext() const;
            bool needDeleted() const;
            
            bool operator<=(const Packet& packet) const;
            bool operator>=(const Packet& packet) const;

		private:
            DeviceDescription* _dd;
			InstructionDescription* _id;
			AutoDelete _autoDelete;
            // 0 is the highest priority.
            byte _priority;
            
			bool _processed;
			InstructionContext* _ic;
		};
//		typedef LoopVector<Packet> LoopInstructions;
        class LoopInstructions : public LoopVector<Packet>
        {
        public:
            LoopInstructions(int maxLength = 512, bool autoDelete = true, bool hasPriority = false);
            
            void copyTo(Packet** value);
            
        private:
            bool _hasPriority;
        };

		InstructionPool(DriverManager* dm, ChannelDescription* cd, DeviceDescription* dd, bool hasPriority = false);
		virtual ~InstructionPool();

		virtual void start();
		virtual void stop();

        virtual bool channelConnected() const;
        
        Packet* addInstruction(InstructionDescription* id, Packet::AutoDelete autoDelete = Packet::AutoDelete::PacketAndInstruction, byte priority = 0);
        void addInstruction(const InstructionDescriptions& ids, Packet::AutoDelete autoDelete = Packet::AutoDelete::PacketAndInstruction, byte priority = 0);
        
        Packet* addInstruction(DeviceDescription* dd, InstructionDescription* id, Packet::AutoDelete autoDelete = Packet::AutoDelete::PacketAndInstruction, byte priority = 0);
        void addInstruction(DeviceDescription* dd, const InstructionDescriptions& ids, Packet::AutoDelete autoDelete = Packet::AutoDelete::PacketAndInstruction, byte priority = 0);
        
        string deviceName() const;
        string channelName() const;

		InstructionContext* executeInstructionSync(InstructionDescription* id);
        InstructionContext* executeInstructionSync(DeviceDescription* dd, InstructionDescription* id);

        ChannelContext* context() const;
        void pause();
        void resume();
        
        void addDevice(DeviceDescription* dd);

	protected:
		virtual InstructionContext* executeInstruction(InstructionDescription* id);
        virtual InstructionContext* executeInstruction(DeviceDescription* dd, InstructionDescription* id);
        
        virtual void errorHandle(Channel* channel, bool error);
        virtual void resultInstruction(InstructionDescription* id);

		void processInstructions();

		friend void instructionProc(void* parameter);
		virtual void instructionProcInner();

		friend bool isPacketFinished(void* parameter);

		Packet* addInstructionInner(InstructionDescription* id, Packet::AutoDelete autoDelete = Packet::AutoDelete::PacketAndInstruction, byte priority = 0);
        void addInstructionInner(const InstructionDescriptions& ids, Packet::AutoDelete autoDelete = Packet::AutoDelete::PacketAndInstruction, byte priority = 0);
        
        Packet* addInstructionInner(DeviceDescription* dd, InstructionDescription* id, Packet::AutoDelete autoDelete = Packet::AutoDelete::PacketAndInstruction, byte priority = 0);
        void addInstructionInner(DeviceDescription* dd, const InstructionDescriptions& ids, Packet::AutoDelete autoDelete = Packet::AutoDelete::PacketAndInstruction, byte priority = 0);
        
        
        DriverManager* manager();
        
        bool hadAnotherDevices() const;
        
    protected:
        Channel* _channel;
        Device* _device;

		LoopInstructions* _instructions;
		mutex _mutexInstructions;

		DeviceDescription* _dd;
		ChannelDescription* _cd;
        DeviceDescriptions _dds;    // another devices.

		Thread* _instructionThread;

		bool _pause;
        
        DriverManager* _manager;
	};
    typedef Vector<InstructionPool> InstructionPools;
}
#endif // INSTRUCTIONPOOL_H
