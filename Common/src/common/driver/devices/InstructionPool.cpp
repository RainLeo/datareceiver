#include <assert.h>
#include "InstructionPool.h"
#include "../../thread/Locker.h"
#include "../../thread/TickTimeout.h"
#include "../../diag/Stopwatch.h"
#include "../../diag/Debug.h"
#include "common/Resources.h"
#include "../DriverManager.h"
#include "../channels/ChannelDescription.h"
#include "DeviceDescription.h"

namespace Driver
{
    InstructionPool::Packet::Packet(InstructionDescription* id, AutoDelete autoDelete, byte priority)
				: _dd(nullptr), _id(id), _autoDelete(autoDelete), _priority(priority), _processed(false), _ic(NULL)
    {
    }
    InstructionPool::Packet::Packet(DeviceDescription* dd, InstructionDescription* id, AutoDelete autoDelete, byte priority)
                : _dd(dd), _id(id), _autoDelete(autoDelete), _priority(priority), _processed(false), _ic(NULL)
    {
    }
    InstructionPool::Packet::~Packet()
    {
        _dd = nullptr;
        if(_autoDelete == PacketAndInstruction && _id != NULL)
        {
            delete _id;
        }
        _id = NULL;
        _ic = NULL;
    }
    DeviceDescription* InstructionPool::Packet::deviceDescription() const
    {
        return _dd;
    }
    InstructionDescription* InstructionPool::Packet::getInstruction() const
    {
        return _id;
    }
    void InstructionPool::Packet::process(InstructionContext* ic)
    {
        _ic = ic;
        _processed = true;
    }
    bool InstructionPool::Packet::isProcessed() const
    {
        return _processed;
    }
    InstructionContext* InstructionPool::Packet::getInstructionContext() const
    {
        return _ic;
    }
    bool InstructionPool::Packet::needDeleted() const
    {
        return _autoDelete == AutoDelete::PacketAndInstruction || _autoDelete == AutoDelete::PacketOnly;
    }
    
    bool InstructionPool::Packet::operator<=(const Packet& packet) const
    {
        return this->_priority <= packet._priority;
    }
    bool InstructionPool::Packet::operator>=(const Packet& packet) const
    {
        return this->_priority >= packet._priority;
    }
    
    InstructionPool::LoopInstructions::LoopInstructions(int maxLength, bool autoDelete, bool hasPriority) : LoopVector<Packet>(maxLength, autoDelete)
    {
        _hasPriority = hasPriority;
    }
    void InstructionPool::LoopInstructions::copyTo(Packet** value)
    {
        LoopVector<Packet>::copyTo(value);
        if(_hasPriority)    // sort by priority
        {
            uint c = count();
            if(c > 1)
            {
                Vector<Packet>::quicksort(value, c);
            }
        }
    }
    
	void instructionProc(void* parameter)
	{
		InstructionPool* ip = (InstructionPool*)parameter;
		ip->instructionProcInner();
	}

	bool isPacketFinished(void* parameter)
	{
		if(parameter != NULL)
		{
			InstructionPool::Packet* packet = (InstructionPool::Packet*)parameter;
			return packet->isProcessed();
		}
		return false;
	}

	InstructionPool::InstructionPool(DriverManager* dm, ChannelDescription* cd, DeviceDescription* dd, bool hasPriority)
	{
        if(dm == NULL)
            throw ArgumentNullException("dm");
		if(cd == NULL)
			throw ArgumentNullException("cd");
		if(dd == NULL)
			throw ArgumentNullException("dd");

		_manager = dm;
		_cd = cd;
		_dd = dd;

		_device = NULL;
		_channel = NULL;

		_instructionThread = NULL;

		_mutexInstructions.lock();
		_instructions = new LoopInstructions(512, true, hasPriority);
		_mutexInstructions.unlock();

		_pause = false;
        
        _dds.setAutoDelete(false);
	}

	InstructionPool::~InstructionPool()
	{
		stop();

		_cd = NULL;
		_dd = NULL;

		_mutexInstructions.lock();
		delete _instructions;
		_instructions = NULL;
		_mutexInstructions.unlock();
	}

	void InstructionPool::start()
	{
		DriverManager* dm = manager();
		assert(dm);
		_channel = dm->getChannel(_cd);
        assert(_channel);
		_device = dm->getDevice(_dd);
        assert(_device);

		if(_instructionThread == NULL)
		{
			_instructionThread = new Thread();
			char name[256];
			sprintf(name, "instructionProc_%s", _dd->name().c_str());
			_instructionThread->setName(name);
			_instructionThread->startProc(instructionProc, this, 1);
		}
	}
	void InstructionPool::stop()
	{
		_channel = NULL;
		_device = NULL;

		if(_instructionThread != NULL)
		{
			_instructionThread->stop();
			delete _instructionThread;
			_instructionThread = NULL;
		}
	}
    
    bool InstructionPool::channelConnected() const
    {
        return _channel != NULL ? _channel->connected() : false;
    }

	InstructionPool::Packet* InstructionPool::addInstruction(InstructionDescription* id, Packet::AutoDelete autoDelete, byte priority)
	{
		if(_pause || channelConnected())
		{
			return addInstructionInner(id, autoDelete, priority);
		}
		else
		{
            if(autoDelete == Packet::AutoDelete::PacketAndInstruction)
			{
				delete id;
			}
		}
		return NULL;
	}
    void InstructionPool::addInstruction(const InstructionDescriptions& ids, Packet::AutoDelete autoDelete, byte priority)
    {
        for (uint i=0; i<ids.count(); i++)
        {
            InstructionDescription* id = ids.at(i);
            addInstruction(id, autoDelete, priority);
        }
    }
    
    InstructionPool::Packet* InstructionPool::addInstruction(DeviceDescription* dd, InstructionDescription* id, Packet::AutoDelete autoDelete, byte priority)
    {
        if(_pause || channelConnected())
        {
            return addInstructionInner(dd, id, autoDelete, priority);
        }
        else
        {
            if(autoDelete == Packet::AutoDelete::PacketAndInstruction)
            {
                delete id;
            }
        }
        return NULL;
    }
    void InstructionPool::addInstruction(DeviceDescription* dd, const InstructionDescriptions& ids, Packet::AutoDelete autoDelete, byte priority)
    {
        for (uint i=0; i<ids.count(); i++)
        {
            InstructionDescription* id = ids.at(i);
            addInstruction(dd, id, autoDelete, priority);
        }
    }

	InstructionPool::Packet* InstructionPool::addInstructionInner(InstructionDescription* id, Packet::AutoDelete autoDelete, byte priority)
	{
		if(id != NULL && _instructions != NULL)
		{
			Packet* packet = new Packet(id, autoDelete, priority);
			Locker locker(&_mutexInstructions);
			_instructions->enqueue(packet);
			return packet;
		}
		return NULL;
	}
    void InstructionPool::addInstructionInner(const InstructionDescriptions& ids, Packet::AutoDelete autoDelete, byte priority)
    {
        for (uint i=0; i<ids.count(); i++)
        {
            InstructionDescription* id = ids.at(i);
            addInstructionInner(id, autoDelete, priority);
        }
    }
    
    InstructionPool::Packet* InstructionPool::addInstructionInner(DeviceDescription* dd, InstructionDescription* id, Packet::AutoDelete autoDelete, byte priority)
    {
        if(id != NULL && _instructions != NULL)
        {
            Packet* packet = new Packet(dd, id, autoDelete, priority);
            Locker locker(&_mutexInstructions);
            _instructions->enqueue(packet);
            return packet;
        }
        return NULL;
    }
    void InstructionPool::addInstructionInner(DeviceDescription* dd, const InstructionDescriptions& ids, Packet::AutoDelete autoDelete, byte priority)
    {
        for (uint i=0; i<ids.count(); i++)
        {
            InstructionDescription* id = ids.at(i);
            addInstructionInner(dd, id, autoDelete, priority);
        }
    }

	void InstructionPool::processInstructions()
	{
		// process the instructions, all of...
		if(_channel != NULL)
		{
			_mutexInstructions.lock();
			uint count = _instructions->count();
			if(count > 0)
			{
				bool result = true;

				Packet** packets = new Packet*[count];
				_instructions->copyTo(packets);
				_instructions->makeNull(false);
				_mutexInstructions.unlock();

				if(channelConnected())
				{
					for (uint i = 0; i < count; i++)
					{
						Packet* packet = packets[i];
//                        Debug::writeFormatLine("execute an instruction, device name: %s",
//                                               packet->deviceDescription() == nullptr ?
//                                               _dd->name().c_str() : packet->deviceDescription()->name().c_str());
						InstructionDescription* id = packet->getInstruction();
                        InstructionContext* ic = packet->deviceDescription() == nullptr ?
                            executeInstruction(id) :
                            executeInstruction(packet->deviceDescription(), id);
						result = (ic != NULL && !ic->hasException());
						errorHandle(_channel, !result);

						resultInstruction(id);

                        if(packet->needDeleted())
						{
							delete packet;
						}
						else
						{
							packet->process(ic);
						}
					}
				}
				else
				{
					for (uint i = 0; i < count; i++)
					{
						Packet* packet = packets[i];
						if(packet->needDeleted())
						{
							delete packet;
						}
						else
						{
							packet->process();
						}
					}

					if(_channel != NULL && _channel->context()->reopened())
					{
						result = _channel->open();
					}

					if(!result)
					{
						errorHandle(_channel, !result);
					}
				}

				delete[] packets;
			}
			else
			{
				_mutexInstructions.unlock();
			}
		}
	}
	InstructionContext* InstructionPool::executeInstruction(InstructionDescription* id)
	{
		return _device != NULL ? _device->executeInstruction(id) : NULL;
	}
    InstructionContext* InstructionPool::executeInstruction(DeviceDescription *dd, InstructionDescription* id)
    {
        Device* device = nullptr;
        if(_dd == dd)
            device = _device;
        else
        {
            DriverManager* dm = manager();
            for (uint i=0; i<_dds.count(); i++)
            {
                DeviceDescription* temp = _dds[i];
                if(temp == dd)
                {
                    device = dm->getDevice(dd);
                    break;
                }
            }
        }
        return device != nullptr ? device->executeInstruction(id) : NULL;
    }
    
	InstructionContext* InstructionPool::executeInstructionSync(DeviceDescription *dd, InstructionDescription* id)
	{
        Packet* packet = addInstruction(dd, id, Packet::AutoDelete::None);
		if(packet != NULL)
		{
			TickTimeout::msdelay(dd->receiveDelay(id), isPacketFinished, packet);
			if(packet->isProcessed())
			{
				InstructionContext* ic = packet->getInstructionContext();
				delete packet;
				return ic;
			}
		}
		return NULL;
	}
    InstructionContext* InstructionPool::executeInstructionSync(InstructionDescription* id)
    {
        Packet* packet = addInstruction(id, Packet::AutoDelete::None);
        if(packet != NULL)
        {
            TickTimeout::msdelay(_dd->receiveDelay(id), isPacketFinished, packet);
            if(packet->isProcessed())
            {
                InstructionContext* ic = packet->getInstructionContext();
                delete packet;
                return ic;
            }
        }
        return NULL;
    }
    
	void InstructionPool::instructionProcInner()
	{
		if(!_pause)
		{
			processInstructions();
		}
	}
    
    void InstructionPool::errorHandle(Channel* channel, bool error)
    {
    }
    void InstructionPool::resultInstruction(InstructionDescription* id)
    {
    }

	string InstructionPool::deviceName() const
	{
		return _dd != NULL ? _dd->name() : "";
	}
    string InstructionPool::channelName() const
    {
        return _cd != NULL ? _cd->name() : "";
    }
    
    DriverManager* InstructionPool::manager()
    {
        return _manager;
    }
    
    ChannelContext* InstructionPool::context() const
    {
        return _cd != NULL ? _cd->context() : NULL;
    }
    void InstructionPool::pause()
    {
        _pause = true;
    }
    void InstructionPool::resume()
    {
        _pause = false;
    }
    
    bool InstructionPool::hadAnotherDevices() const
    {
        return _dds.count() > 0;
    }
    
    void InstructionPool::addDevice(DeviceDescription* dd)
    {
        _dds.add(dd);
    }
}
