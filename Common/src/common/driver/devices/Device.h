#ifndef DEVICE_H
#define DEVICE_H

#include "common/common_global.h"
#include "../../data/StreamVector.h"
#include "../../thread/Locker.h"
#include "../instructions/Instruction.h"
#include "DeviceDescription.h"

using namespace Common;

namespace Driver
{
	class Channel;
	class Instruction;
	class InstructionSet;
	class DeviceDescription;
	class COMMON_EXPORT Device
	{
	public:
		enum Status
		{
			Unknown = 0,
			Offline = 1,
			Online = 2,
		};

		struct StatusSnap : public StreamAccessor
		{
		public:
			Status OldStatus;
			Status NewStatus;

            StatusSnap();
            StatusSnap(Status status);
            StatusSnap(Status oldStatus, Status newStatus);

            void write(Common::Stream* stream) const;
            void read(Common::Stream* stream);
            void copyFrom(const StatusSnap* status);
		};

		Device(DeviceDescription* dd, Channel* channel);
		~Device(void);

		DeviceDescription* description() const;
		Channel* getChannel() const;
		Instruction* getInstruction(const string& instructionName) const;
        Instruction* getInstruction(const InstructionDescription* id) const;

		bool receive(ByteArray* buffer);

		const string& name() const;

        void executeInstruction();
		InstructionContext* executeInstruction(InstructionDescription* id);

        static string getStatusStr(const Status status);
        static bool isStatusChanged(Status oldStatus, Status newStatus);

		InstructionSet* instructionSet() const;
        
        void addReceiveDevice(Device* device);
        void removeReceiveDevice(Device* device);
        
        void setAllowLog(bool enable);
        
    private:
        Instruction* matchInstruction(const ByteArray& buffer);
		InstructionContext* executeInstruction(Instruction* instruction, InstructionContext* ic, const ByteArray* buffer = NULL);
        
        void setReceiveInstruction(Instruction* instruction);

	private:
		DeviceDescription* _description;
		Instructions* _instructions;
		Channel* _channel;
        
        mutex _receiveDevicesMutex;
        Vector<Device> _receiveDevices;
	};
    typedef Vector<Device> Devices;
}
#endif // DEVICE_H