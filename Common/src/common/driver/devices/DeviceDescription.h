#ifndef DEVICEDESCRIPTION_H
#define DEVICEDESCRIPTION_H

#include "common/common_global.h"
#include "../IContextProperty.h"
#include "../instructions/InstructionSet.h"
#include "../channels/ChannelDescription.h"

namespace Driver
{
    class COMMON_EXPORT DeviceContext : public Context
    {
    public:
        DeviceContext()
        {
            _sendTimeout = 3000;
            _receiveTimeout = 3000;
            
            _address = 0;
        }
        
        inline const int address() const
        {
            return _address;
        }
        inline void setAddress(int address)
        {
            _address = address;
        }
        
        inline uint sendTimeout() const
        {
            return _sendTimeout;
        }
        inline void setSendTimeout(uint timeout)
        {
            _sendTimeout = timeout;
        }
        inline void setSendTimeout(TimeSpan timeout)
        {
            _sendTimeout = (uint)timeout.totalMilliseconds();
        }
        inline uint receiveTimeout(const InstructionDescription* id) const
        {
            if(id != NULL)
            {
                uint timeout = id->getReceiveTimeout();
                if(timeout > 0)
                {
                    return timeout;
                }
            }
            return _receiveTimeout;
        }
        inline uint receiveDelay(const InstructionDescription* id) const
        {
            return receiveTimeout(id) * 3;
        }
        inline uint receiveTimeout() const
        {
            return _receiveTimeout;
        }
        inline void setReceiveTimeout(uint timeout) 
        {
            _receiveTimeout = timeout;
        }
        inline void setReceiveTimeout(TimeSpan timeout)
        {
            _receiveTimeout = (uint)timeout.totalMilliseconds();
        }
        
        virtual DeviceContext* clone() const
        {
            DeviceContext* context = new DeviceContext();
            context->copyFrom(this);
            return context;
        }
        
    protected:
        void copyFrom(const DeviceContext* context)
        {
            this->_address = context->_address;
            this->_sendTimeout = context->_sendTimeout;
            this->_receiveTimeout = context->_receiveTimeout;
        }
        
    protected:
        uint  _sendTimeout;
        uint  _receiveTimeout;
        
        int _address;
    };
    
	class COMMON_EXPORT DeviceDescription : public IContextProperty
	{
	public:
		DeviceDescription(const string& name, ChannelDescription* cd, InstructionSet* is, DeviceContext* context = NULL)
		{
			_name = name;
			_channel = cd;
			_instructionSet = is;

            _context = context == NULL ? new DeviceContext() : context;
		}
		~DeviceDescription()
		{
			if(_channel != NULL)
			{
				delete _channel;
				_channel = NULL;
			}
			if(_instructionSet != NULL)
			{
				delete _instructionSet;
				_instructionSet = NULL;
			}
            if(_context != NULL)
            {
                delete _context;
                _context = NULL;
            }
		}

		inline const string& name() const
		{
			return _name;
		}

		inline const int address() const
		{
			return _context->address();
		}
		inline void setAddress(int address)
		{
			_context->setAddress(address);
		}

		inline DeviceContext* context() const
		{
			return _context;
		}

		inline ChannelDescription* getChannel() const
		{
			return _channel;
		}

		inline InstructionSet* instructionSet() const
		{
			return _instructionSet;
		}

		inline uint sendTimeout() const
		{
            return _context->sendTimeout();
		}
		inline void setSendTimeout(uint timeout) 
		{
			_context->setSendTimeout(timeout);
		}
		inline void setSendTimeout(TimeSpan timeout)
		{
            _context->setSendTimeout(timeout);
		}
		inline uint receiveTimeout(const InstructionDescription* id) const
		{
			return _context->receiveTimeout(id);
		}
		inline uint receiveDelay(const InstructionDescription* id) const
		{
			return _context->receiveDelay(id);
		}
		inline uint receiveTimeout() const
		{
			return _context->receiveTimeout();
		}
		inline void setReceiveTimeout(uint timeout) 
		{
            _context->setReceiveTimeout(timeout);
		}
		inline void setReceiveTimeout(TimeSpan timeout)
		{
			_context->setReceiveTimeout(timeout);
		}

	private:
		string _name;
		DeviceContext* _context;
		ChannelDescription* _channel;
		InstructionSet* _instructionSet;
	};
    typedef Vector<DeviceDescription> DeviceDescriptions;
}
#endif // DEVICEDESCRIPTION_H
