#ifndef SAMPLER_H
#define SAMPLER_H

#include <mutex>
#include "common/common_global.h"
#include "../../data/LoopArray.h"
#include "../../thread/Thread.h"
#include "../channels/Channel.h"
#include "../channels/ChannelDescription.h"
#include "../instructions/InstructionDescription.h"
#include "Device.h"
#include "DeviceDescription.h"
#include "InstructionPool.h"

using namespace std;
using namespace Common;

namespace Driver
{
    class COMMON_EXPORT Sampler : public InstructionPool
    {
    public:
        Sampler(DriverManager* dm, ChannelDescription* cd, DeviceDescription* dd, bool skipSampler = true);
        virtual ~Sampler(void);
        
        virtual Device::Status getConnectStatus() const
        {
            return  _connected;
        }
        inline bool isOnline() const
        {
            return getConnectStatus() == Device::Online;
        }
        void setConnectedError(bool showError = false);
        
    protected:
        // return the sample interval, unit: ms
        virtual uint detetionInterval() const
        {
            return 3000;
        }
        // return the resume interval, unit: ms
        virtual uint resumeInterval() const
        {
            return 10 * 1000;
        }
        // return the sample interval, unit: ms
        virtual int detetionCount() const
        {
            return 3;
        }
        virtual void setConnectStatus(Device::Status status)
        {
            if(_connected != status)
            {
                _connected = status;
            }
        }
        // return the sample instruction context, do not warry about deleting.
        virtual InstructionDescription* sampleInstruction() = 0;
		void instructionProcInner();
        
    protected:
        virtual void addSampleInstruction();
        void errorHandle(Channel* channel, bool error);
        
    private:
        uint _sampleStart;
        Device::Status _connected;
        
        int _checkOnlineFailedCount;
        bool _isInvalidStatus;
        int _connetedFailedCount;
        uint _sampleInterval;
        
        bool _skipSampler;
        
        bool _showConnectedError;
    };
}
#endif // SAMPLER_H
