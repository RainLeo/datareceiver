#ifndef INSTRUCTIONSET_H
#define INSTRUCTIONSET_H

#include "common/common_global.h"
#include "../../data/ByteArray.h"
#include "../../exception/Exception.h"
#include "Instruction.h"

using namespace Common;

namespace Driver
{
	class Channel;
	class Device;

	class COMMON_EXPORT InstructionSet
	{
	public:
		InstructionSet(void){}
		virtual ~InstructionSet(void){}

		virtual void generateInstructions(Instructions* instructions) = 0;

		virtual bool receive(Device* device, Channel* channel, ByteArray* buffer)
		{
			return false;
		}

		virtual InstructionSet* clone() const
		{
			throw NotImplementedException("It must be overide.");
		}
	};
}

#endif // INSTRUCTIONSET_H
