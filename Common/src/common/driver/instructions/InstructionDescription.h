#ifndef INSTRUCTIONDESCRIPTION_H
#define INSTRUCTIONDESCRIPTION_H

#include "common/common_global.h"
#include "common/data/Vector.h"
#include "../../exception/Exception.h"
#include "../../system/Convert.h"
#include "../IContextProperty.h"
#include "InstructionContext.h"

using namespace Common;

namespace Driver
{
	class COMMON_EXPORT InstructionDescription : IContextProperty
	{
	public:
		InstructionDescription(const string& name, InstructionContext* context = NULL, bool autoDeleteContext = true) : _receiveTimeout(0)
		{
			if(name.empty())
				throw ArgumentNullException("name");

			_name = name;
			_context = context;
			_autoDeleteContext = autoDeleteContext;
		}
		~InstructionDescription()
		{
			if (_autoDeleteContext)
			{
				if (_context != NULL)
				{
					delete _context;
					_context = NULL;
				}
			}
		}

		inline const string& name() const
		{
			return _name;
		}

		inline InstructionContext* context() const
		{
			return _context;
		}
		inline void setContext(InstructionContext* context)
		{
			_context = context;
		}

		inline uint getReceiveTimeout() const
		{
			return _receiveTimeout;
		}
		inline void setReceiveTimeout(uint timeout) 
		{
			_receiveTimeout = timeout;
		}

	private:
		string _name;
		InstructionContext* _context;
		uint  _receiveTimeout;
		bool _autoDeleteContext;
	};
    typedef Vector<InstructionDescription> InstructionDescriptions;
}

#endif // INSTRUCTIONDESCRIPTION_H