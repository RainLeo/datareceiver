#include "Instruction.h"
#include "../../diag/Debug.h"
#include "../../system/Singleton.h"
#include "../../system/Math.h"
#include "../DriverManager.h"

using namespace Common;

namespace Driver
{
	Instruction::Instruction(InstructionDescription* id)
	{
		_description = id;
        _receiveInstruction = nullptr;
        
        _logLength = DefaultLogLength;
        _allowLog = true;
	}
	Instruction::~Instruction(void)
	{
		delete _description;
		_description = NULL;
        _receiveInstruction = nullptr;
	}
    
    InstructionDescription* Instruction::description() const
    {
        return _description;
    }
    InstructionContext* Instruction::context() const
    {
        return _description != NULL ? _description->context() : NULL;
    }
    void Instruction::setContext(InstructionContext* context)
    {
        if(_description != NULL)
        {
            _description->setContext(context);
        }
    }
    
    bool Instruction::match(const ByteArray* buffer)
    {
        return false;
    }
    
    void Instruction::setReceiveInstruction(Instruction* instruction)
    {
        assert(instruction);
        _receiveInstruction = instruction;
    }
    
    uint Instruction::logLength() const
    {
        return _logLength;
    }
    void Instruction::setLogLength(int length)
    {
        if(length >= MinLogLength && length <= MaxLogLength)
        {
            _logLength = length;
        }
    }
    bool Instruction::allowLog() const
    {
        return _allowLog;
    }
    void Instruction::setAllowLog(bool enable)
    {
        _allowLog = enable;
    }
    void Instruction::log(const ByteArray* buffer, bool send)
    {
        if(buffer != nullptr)
            log(buffer, buffer->count(), send);
    }
    void Instruction::log(const ByteArray* buffer, uint count, bool send)
    {
        if (allowLog() && buffer != nullptr && buffer->count() > 0 && count > 0)
        {
            if(count > buffer->count())
                count = buffer->count();
            
            String str;
            if(count > logLength())
            {
                ByteArray array(buffer->data(), Math::min(logLength(), count));
                str = array.toString();
            }
            else
            {
                str = buffer->toString();
            }
            if (send)
            {
                Trace::writeLine(String::convert("send: %s", str.c_str()), Trace::System);
            }
            else
            {
                Trace::writeLine(String::convert("recv: %s", str.c_str()), Trace::System);
            }
        }
    }
}