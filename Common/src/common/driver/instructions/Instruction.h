#ifndef INSTRUCTION_H
#define INSTRUCTION_H

#include "common/common_global.h"
#include "../../data/ByteArray.h"
#include "../../system/Convert.h"
#include "InstructionDescription.h"
#include "InstructionContext.h"

using namespace Common;

namespace Driver
{
	class Interactive;
	class Device;
    class DriverManager;
	class COMMON_EXPORT Instruction
	{
	public:
		Instruction(InstructionDescription* id);
		virtual ~Instruction(void);
        InstructionDescription* description() const;
        InstructionContext* context() const;
        void setContext(InstructionContext* context);
        
		virtual InstructionContext* execute(Interactive* interactive, Device* device, InstructionContext* context, const ByteArray* buffer = NULL) = 0;
        virtual bool match(const ByteArray* buffer);
        
    protected:
        void setReceiveInstruction(Instruction* instruction);
        
        uint logLength() const;
        void setLogLength(int length);
        virtual bool allowLog() const;
        void setAllowLog(bool enable);
        void log(const ByteArray* buffer, bool send);
        void log(const ByteArray* buffer, uint count, bool send);

    protected:
        Instruction* _receiveInstruction;
        
	private:
        friend Device;
        
		InstructionDescription* _description;
        
        uint _logLength;
        bool _allowLog;
        
        static const uint DefaultLogLength = 128;
        static const uint MinLogLength = 1;
        static const uint MaxLogLength = 128 * 1024;
	};

	typedef Vector<Instruction> Instructions;
}

#endif // INSTRUCTION_H