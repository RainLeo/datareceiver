#ifndef INSTRUCTIONCONTEXT_H
#define INSTRUCTIONCONTEXT_H

#include "common/common_global.h"
#include "../../exception/Exception.h"
#include "../Context.h"

using namespace Common;

namespace Driver
{
	class COMMON_EXPORT InstructionContext : public Context
	{
	public:
		InstructionContext()
		{
			_exception = NULL;
			_qualitystamp = Bad;
			_timestamp = 0;
		}
		virtual ~InstructionContext()
		{
			if(_exception != NULL)
			{
				delete _exception;
				_exception = NULL;
			}
		}
		void setExcetion(int type,const char* message)
		{
			clearException();
			_exception = new Exception(type,message);
		}
		void clearException()
		{
			if(_exception!=NULL)
			{
				delete _exception;
				_exception = NULL;
			}
		}
		inline Exception* getException() const
		{
			return _exception;
		}
		inline bool hasException() const
		{
			return _exception != NULL;
		}
		inline void setTimeStamp(uint stamp)
		{
			_timestamp = stamp;
		}
		inline uint getTimeStamp() const
		{
			return _timestamp;
		}
		inline void setQualityStamp(ushort stamp)
		{
			_qualitystamp = stamp;
		}
		inline ushort getQualityStamp() const
		{
			return _qualitystamp;
		}

	private:
		Exception* _exception;
		uint _timestamp;
		ushort _qualitystamp;

		const static int Good = 192;
		const static int Bad = 0;
	};
}

#endif // INSTRUCTIONCONTEXT_H
