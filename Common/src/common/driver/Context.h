#ifndef CONTEXT_H
#define CONTEXT_H

#include "common/common_global.h"
#include "../system/Convert.h"
#include "../data/Vector.h"

using namespace Common;

namespace Driver
{
	enum ValueType
	{
		ValueEmpty = 0,
		ValueInt = 1,
		ValueShort	= 2,
		ValueChar	= 3,
		ValueStr = 4,

	};

	struct ValueSet
	{
		ValueType type;
		union 
		{
			int	iValue;	
			char strValue[256];	
			unsigned short wValue;	
			char cValue;
		}Value;

	public:
		ValueSet() : type(ValueEmpty)
		{
			Value.iValue = 0;
		}
		ValueSet(const string& value)
		{
			type = ValueStr;
			strcpy(Value.strValue, value.c_str());
		}
		ValueSet(int value)
		{
			type = ValueInt;
			Value.iValue = value;
		}
		inline bool isEmpty() const
		{
			return type == ValueEmpty;
		}
	};

	struct Data
	{
	public:
		Data()
		{
			_name = "";
		}
		Data(const string& name, ValueSet value)
		{
			_name = name;
			_value = value;
		}

	public:
		const string& name() const
		{
			return _name;
		}

		ValueSet getValue() const
		{
			return _value;
		}
	private:
		string _name;
		ValueSet _value;
	};

	typedef Vector<Data> Datas;

	class COMMON_EXPORT Context
	{
	public:
		Context();
		virtual ~Context();

		void setValue(const string& name, ValueSet value);
		ValueSet getValue(const string& name);

	private:
		Data* find(const string& name);

	private:
		Datas* _data;
	};
}
#endif // CONTEXT_H
