#ifndef ICONTEXTPROPERTY_H
#define ICONTEXTPROPERTY_H

#include "common/common_global.h"
#include "Context.h"

namespace Driver
{
	class COMMON_EXPORT IContextProperty
	{
	public:
		virtual ~IContextProperty(){}
		virtual Context* context() const = 0;
	};
}

#endif // ICONTEXTPROPERTY_H
