#include "../exception/Exception.h"
#include "../diag/Trace.h"
#include "../thread/Thread.h"
#include "../thread/Locker.h"
#include "devices/DeviceDescription.h"
#include "channels/ChannelDescription.h"
#include "DriverManager.h"

using namespace Common;

namespace Driver
{
    DriverManager::DriverManager()
    {
        _description = new DriverDescription();
        _devices = new Devices();
        _channels = new Channels();
        _instructionPools = new InstructionPools();
        
        _opened = false;
    }
    DriverManager::~DriverManager()
    {
        close();
        
        delete _description;
        _description = NULL;
        delete _devices;
        _devices = NULL;
        delete _channels;
        _channels = NULL;
        delete _instructionPools;
        _instructionPools = NULL;
    }
    
    DriverDescription* DriverManager::description() const
    {
        return _description;
    }
    
	void DriverManager::open(bool onlyIncludeServerChannel)
	{
		createDevices();
        
        for(uint i=0;i<_channels->count();i++)
        {
            Channel* channel = _channels->at(i);
            if((onlyIncludeServerChannel && channel->isServerChannel()) ||
               !onlyIncludeServerChannel)
            {
                channel->open();
            }
        }
        
        // It is not server channel if it has a sampler.
		for (uint i = 0; i < _instructionPools->count(); i++)
		{
			_instructionPools->at(i)->start();
		}

		_opened = true;
	}
    void DriverManager::reopen(const string& channelName, bool allowConnected)
    {
        if(channelName.empty())
        {
            for(uint i=0;i<_channels->count();i++)
            {
                Channel* channel = _channels->at(i);
                reopen(channel, allowConnected);
            }
        }
        else
        {
            Channel* channel = getChannel(channelName);
            reopen(channel, allowConnected);
        }
    }
    void DriverManager::reopenAll(bool allowConnected)
    {
        reopen("", allowConnected);
    }
    void DriverManager::reopen(Channel* channel, bool allowConnected)
    {
        if(channel != NULL && channel->context()->reopened() &&
           (!channel->connected() || allowConnected))
        {
            channel->close();
            channel->open();
        }
    }
	void DriverManager::close()
	{
        if(!opened())
            return;
        
		_opened = false;

		Locker locker(&_mutex);
		DeviceDescriptions* dds = _description->getDevices();
		for (uint i = 0; i < dds->count(); i++)
		{
			DeviceDescription* dd = dds->at(i);

			ChannelDescription* cd = dd->getChannel();
			Channel* channel = getChannel(cd);
			if(channel != NULL)
			{
				channel->close();
			}
		}

        for (uint i = 0; i < _instructionPools->count(); i++)
        {
            _instructionPools->at(i)->stop();
        }
    }

	bool DriverManager::hasDevice(const string& deviceName)
	{
		bool result = false;

		Locker locker(&_mutex);
		DeviceDescriptions* dds = _description->getDevices();
		if (!(dds == NULL || dds->count() == 0))
		{
			for (uint i = 0; i < dds->count(); i++)
			{
				DeviceDescription* dd = dds->at(i);
				if(dd->name() == deviceName)
				{
					result = true;
					break;
				}
			}
		}

		return result;
	}
    
    Device* DriverManager::getDevice(const string& deviceName) const
    {
        for (uint i = 0; i < _devices->count(); i++)
        {
            Device* device = _devices->at(i);
            if(device != nullptr &&
               device->description()->name() == deviceName)
            {
                return device;
            }
        }
        return NULL;
    }
    Device* DriverManager::getDevice(const Channel* channel) const
    {
        for (uint i = 0; i < _devices->count(); i++)
        {
            Device* device = _devices->at(i);
            if(device != nullptr &&
               device->getChannel() == channel)
            {
                return device;
            }
        }
        return NULL;
    }
    Device* DriverManager::getDevice(DeviceDescription* dd) const
    {
        return dd != NULL ? getDevice(dd->name()) : NULL;
    }
    bool DriverManager::containsDevice(DeviceDescription* dd) const
    {
        return getDevice(dd) != NULL;
    }
    
    bool DriverManager::hasChannel(ChannelDescription* cd)
    {
        for (uint i = 0; i < _channels->count(); i++)
        {
            const Channel* channel = _channels->at(i);
            if(channel != nullptr &&
               channel->description() == cd)
            {
                return true;
            }
        }
        return false;
    }
    Channel* DriverManager::getChannel(const string& channelName) const
    {
        for (uint i = 0; i < _channels->count(); i++)
        {
            Channel* channel = _channels->at(i);
            if(channel->description()->name() == channelName)
            {
                return channel;
            }
        }
        return NULL;
    }
    Channel* DriverManager::getChannel(ChannelDescription* cd) const
    {
        return cd != NULL ? getChannel(cd->name()) : NULL;
    }
    
    void DriverManager::addPool(const InstructionPool* ip)
    {
        if(ip != NULL)
        {
            _instructionPools->add(ip);
        }
    }
    InstructionPool* DriverManager::getPool(const string& deviceName) const
    {
        for (uint i = 0; i < _instructionPools->count(); i++)
        {
            InstructionPool* ip = _instructionPools->at(i);
            if(ip->deviceName() == deviceName)
            {
                return ip;
            }
        }
        return NULL;
    }
    InstructionPool* DriverManager::getPoolByChannelName(const string& channelName) const
    {
        for (uint i = 0; i < _instructionPools->count(); i++)
        {
            InstructionPool* ip = _instructionPools->at(i);
            if(ip->channelName() == channelName)
            {
                return ip;
            }
        }
        return NULL;
    }
    const InstructionPools* DriverManager::getPools() const
    {
        return _instructionPools;
    }
    
    bool DriverManager::opened() const
    {
        return _opened;
    }

	void DriverManager::createDevices()
	{
		DeviceDescriptions* dds = _description->getDevices();
		if (dds == NULL || dds->count() == 0)
		{
			return;
		}
		Locker locker(&_mutex);
		for (uint i = 0; i < dds->count(); i++)
		{
			DeviceDescription* dd = dds->at(i);
            if(!containsDevice(dd))
            {
                ChannelDescription* cd = dd->getChannel();
                Channel* channel = getChannel(cd);
                if(channel == NULL)
                {
                    channel = new Channel(this, cd);
                    _channels->add(channel);
                }

                Device* device = new Device(dd, channel);
                _devices->add(device);
            }
		}
	}

	InstructionContext* DriverManager::executeInstruction(const string& deviceName, InstructionDescription* id)
	{
		InstructionPool* ip = getPool(deviceName);
		if(ip != NULL)
		{
			return ip->executeInstructionSync(id);
		}
		else
		{
			Device* device = getDevice(deviceName);
			if(device != NULL)
			{
				return device->executeInstruction(id);
			}
		}
		return NULL;
	}
}
