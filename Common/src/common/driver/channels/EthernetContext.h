//
//  EthernetContext.h
//  common
//
//  Created by baowei on 15/7/27.
//  Copyright (c) 2015 com. All rights reserved.
//

#ifndef common_EthernetContext_h
#define common_EthernetContext_h

namespace Driver
{
    class EthernetContext
    {
    public:
        EthernetContext(const string& address = "", int port = 0)
        {
            _address = address;
            _port = port;
        }
        inline const string& address() const
        {
            return _address;
        }
        inline void setAddress(const string& address)
        {
            _address = address;
        }
        
        inline int port() const
        {
            return _port;
        }
        inline void setPort(int port)
        {
            _port = port;
        }
        
    protected:
        int _port;
        string _address;
    };
}

#endif
