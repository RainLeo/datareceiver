#include <assert.h>
#include "../../diag/Stopwatch.h"
#include "../../thread/Locker.h"
#include "../../thread/Thread.h"
#include "../../diag/Debug.h"
#include "../DriverManager.h"
#include "../devices/Device.h"
#include "common/Resources.h"
#include "BluetoothServerInteractive.h"

using namespace Common;

namespace Driver
{
	void bluetooth_acceptProc(void* parameter)
	{
		BluetoothServerInteractive* ti = (BluetoothServerInteractive*)parameter;
		assert(ti);
		ti->acceptProcInner();
	}
	void bluetooth_closeProc(void* parameter)
	{
		BluetoothServerInteractive* ti = (BluetoothServerInteractive*)parameter;
		assert(ti);
		ti->closeProcInner();
	}

	BluetoothServerInteractive::BluetoothServerInteractive(DriverManager* dm, Channel* channel) : Interactive(dm, channel),
		_bluetoothServer(NULL), _acceptThread(NULL)
	{
		if(channel == NULL)
			throw ArgumentNullException("channel");
		if((BluetoothServerChannelContext*)channel->description()->context() == NULL)
			throw ArgumentNullException("channel");

		_acceptAction = NULL;
		_closeAction = NULL;
	}

	BluetoothServerInteractive::~BluetoothServerInteractive(void)
	{
		close();
	}

	bool BluetoothServerInteractive::open()
	{
		return rebind();
	}

	bool BluetoothServerInteractive::rebind()
	{
		static int bindingCount = 0;
		const static int MaxBindingCount = 5;
		const static uint MaxDelayTime = 2000;	// 2s

		_bluetoothServerMutex.lock();
		if (_bluetoothServer != NULL)
		{
			_bluetoothServer->close();
			delete _bluetoothServer;
			_bluetoothServer = NULL;
		}

		_bluetoothServer = new BluetoothServer();

		BluetoothServerChannelContext* tcc = getChannelContext();
#ifdef DEBUG
		string message = "listen a bluetooth socket";
		Stopwatch sw(message);
#endif

		bindingCount++;

		bool result = _bluetoothServer->bind();
		if(result)
		{
			result = _bluetoothServer->listen(tcc->maxConnections());
			_bluetoothServerMutex.unlock();
			if(result)
			{
				if (result)
				{
					if (tcc->sendBufferSize() > 0)
					{
						_bluetoothServer->setSendBufferSize(tcc->sendBufferSize());
					}
					if (tcc->receiveBufferSize() > 0)
					{
						_bluetoothServer->setReceiveBufferSize(tcc->receiveBufferSize());
					}
				}

				_acceptThread = new Thread();
				_acceptThread->setName("bluetooth_acceptProc");
				_acceptThread->startProc(bluetooth_acceptProc, this, 1);

				_closeThread = new Thread();
				_closeThread->setName("bluetooth_closeProc");
				_closeThread->startProc(bluetooth_closeProc, this, 1000);

				return true;
			}
			else
			{
				Debug::writeLine("Failed to listen the BluetoothServerInteractive socket.");

				if(bindingCount < MaxBindingCount)
				{
					Thread::msleep(MaxDelayTime);
					return rebind();
				}
				else
				{
					string message = Resources::BluetoothListenFailedStr;
					Trace::writeLine(message.c_str(), Trace::Error);
					throw Exception(message.c_str());
				}
			}
		}
		else
		{
			_bluetoothServerMutex.unlock();
			Debug::writeLine("Failed to bind the BluetoothServerInteractive socket.");

			if(bindingCount < MaxBindingCount)
			{
				Thread::msleep(MaxDelayTime);
				return rebind();
			}
			else
			{
				string message = Resources::BluetoothBindingFailedStr;
				Trace::writeLine(message.c_str(), Trace::Error);
				throw Exception(message.c_str());
			}
		}
		return false;
	}

	void BluetoothServerInteractive::close()
	{
		_bluetoothServerMutex.lock();
		if(_bluetoothServer != NULL)
		{
			_bluetoothServer->close();
			delete _bluetoothServer;
			_bluetoothServer = NULL;
		}
		_bluetoothServerMutex.unlock();

		if(_acceptThread != NULL)
		{
			_acceptThread->stop();
			delete _acceptThread;
			_acceptThread = NULL;
		}
		if (_closeThread != NULL)
		{
			_closeThread->stop();
			delete _closeThread;
			_closeThread = NULL;
		}
	}

	bool BluetoothServerInteractive::connected()
    {
        Locker locker(&_mutexClients);
        for (uint i = 0; i < _clients.count(); i++)
        {
            BluetoothServerClient* client = _clients.at(i);
            if (client != NULL)
            {
                if (client->connected())
                {
                    return true;
                }
            }
        }
        return false;
        //		throw NotSupportException("Cannot support this method, use 'BluetoothServerClient' instead.");
    }
	int BluetoothServerInteractive::available()
	{
        return 0;
//		throw NotSupportException("Cannot support this method, use 'BluetoothServerClient' instead.");
	}

	int BluetoothServerInteractive::send(byte* buffer, int offset, int count)
	{
        Locker locker(&_mutexClients);
        for (uint i = 0; i < _clients.count(); i++)
        {
            BluetoothServerClient* client = _clients.at(i);
            if (client != NULL)
            {
                // todo: add a pool that it contains buffer.
                client->send(buffer, offset, count);
            }
        }
        return count;
//		throw NotSupportException("Cannot support this method, use 'BluetoothServerClient' instead.");
	}

	int BluetoothServerInteractive::receive(byte* buffer, int offset, int count)
	{
		throw NotSupportException("Cannot support this method, use 'BluetoothServerClient' instead.");
	}

	void BluetoothServerInteractive::acceptProcInner()
	{
		if(_bluetoothServer != NULL)
		{
			int socketId = _bluetoothServer->accept();
			//Debug::writeLine(Convert::convertStr("client connected, socketId: %d", socketId), Trace::System);

			Locker locker(&_bluetoothServerMutex);
			bool valid = _bluetoothServer != NULL ? _bluetoothServer->isValid() : false;
			if(valid && socketId != -1)
			{
				BluetoothClient* client = new BluetoothClient(socketId);
				Locker locker(&_mutexClients);
				_clients.add(new BluetoothServerClient(manager(), client, _channel));

				if(_acceptAction != NULL)
				{
					_acceptAction(client->peerAddr(), client->peerPort());
				}
                
                Trace::writeLine(Convert::convertStr("client connected, peerAddr: %s, socketId: %d",
					client->peerAddr().c_str(), socketId), Trace::System);
			}
		}
	}

	void BluetoothServerInteractive::closeProcInner()
	{
		_mutexClients.lock();
		for (uint i = 0; i < _clients.count(); i++)
		{
			BluetoothServerClient* client = _clients.at(i);
			if (client != NULL)
			{
				if (!client->connected())
				{
					Trace::writeLine(Convert::convertStr("Remove an unused client, peerAddr: %s, socketId: %d",
						client->peerAddr().c_str(), client->socketId()), Trace::System);
					_clients.remove(client);

					if (_closeAction != NULL)
					{
						string addr = client->getBluetoothClient()->peerAddr();
						int port = client->getBluetoothClient()->peerPort();
						_closeAction(addr, port);
					}
				}
			}
		}
		_mutexClients.unlock();
	}
}
