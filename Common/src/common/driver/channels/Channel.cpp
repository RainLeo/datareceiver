﻿#include "../../thread/TickTimeout.h"
#include "../../system/Convert.h"
#include "../../thread/Thread.h"
#include "../../data/Array.h"
#include "../../diag/Trace.h"
#include "../../diag/Debug.h"
#include "Channel.h"
#include "ChannelDescription.h"
#include "TcpInteractive.h"
#include "SerialInteractive.h"
#include "SerialServerInteractive.h"
#include "ParallelInteractive.h"
#include "TcpServerInteractive.h"
#include "UdpServerInteractive.h"
#include "UdpInteractive.h"
#include "MockInteractive.h"
#include "../../system/Math.h"
#include "BluetoothInteractive.h"
#include "BluetoothServerInteractive.h"
#include "DtuInteractive.h"

using namespace Common;

namespace Driver
{
    Channel::Channel(DriverManager* dm, ChannelDescription* description) : _interactive(NULL), _mock(NULL)
    {
        if(dm == NULL)
            throw ArgumentNullException("dm");
        
        _description = description;
        _manager = dm;
        
        if(_description->interactiveName()=="TcpInteractive")
        {
            _interactive = new TcpInteractive(dm, this);
        }
        else if(_description->interactiveName()=="SerialInteractive")
        {
            _interactive = new SerialInteractive(dm, this);
        }
        else if (_description->interactiveName() == "SerialServerInteractive")
        {
            _interactive = new SerialServerInteractive(dm, this);
        }
        else if(_description->interactiveName()=="ParallelInteractive")
        {
            _interactive = new ParallelInteractive(dm, this);
        }
        else if(_description->interactiveName()=="TcpServerInteractive")
        {
            _interactive = new TcpServerInteractive(dm, this);
        }
        else if(_description->interactiveName()=="UdpInteractive")
        {
            _interactive = new UdpInteractive(dm, this);
        }
        else if(_description->interactiveName()=="UdpServerInteractive")
        {
            _interactive = new UdpServerInteractive(dm, this);
        }
        else if (_description->interactiveName() == "BluetoothInteractive")
        {
            _interactive = new BluetoothInteractive(dm, this);
        }
        else if (_description->interactiveName() == "BluetoothServerInteractive")
        {
            _interactive = new BluetoothServerInteractive(dm, this);
        }
        else if (_description->interactiveName() == "DtuInteractive")
        {
            _interactive = new DtuInteractive(dm, this);
        }
        else
        {
            Interactive* interactive = _description->interactive();
            if(interactive != NULL)
            {
                interactive->setChannel(this);
                _interactive = interactive;
            }
            else
            {
                throw NotImplementedException("Can not create a interactive.");
            }
        }
    }
    
    Channel::~Channel()
    {
        _description = NULL;
        if(_interactive != NULL)
        {
            delete _interactive;
            _interactive = NULL;
        }
        if(_mock != NULL)
        {
            delete _mock;
            _mock = NULL;
        }
    }
    
    bool Channel::open()
    {
        if(_description->enabled())
        {
            if(_interactive != NULL)
            {
                return _interactive->open();
            }
        }
        return false;
    }
    void Channel::close()
    {
        if(_interactive != NULL)
        {
            _interactive->close();
        }
    }
    
    Interactive* Channel::interactive() const
    {
        return _mock == NULL ? _interactive : _mock;
    }
    void Channel::updateMockRecvBuffer(const ByteArray& buffer)
    {
        if(_mock == NULL)
        {
            _mock = new MockInteractive(_manager, this);
        }
        _mock->updateRecvBuffer(buffer);
    }
    
    void Channel::getEscapeBuffer(const byte* buffer, int offset, int count, ByteArray& dst, const EscapeOption* escape)
    {
        ByteArray src((byte*)buffer, count);
        ByteArray toEscapeBuffer(escape->ToEscapeBuffer, escape->ToEscapeLength);
        ByteArray escapeBuffer(escape->EscapeBuffer, escape->EscapeLength);
        int length = escape->Length > 0 ? escape->Length : count + escape->Length;
        dst.replace(&src, offset + escape->Offset, length,  &escapeBuffer,&toEscapeBuffer);
    }
    
    int Channel::send(byte* buffer, int offset, int count, const EscapeOption* escape)
    {
        int sendLen = 0;
        
        if (buffer == NULL)
        {
            throw ArgumentNullException("buffer");
        }
        if (offset < 0)
        {
            throw ArgumentOutOfRangeException("offset", "Non-negative number required.");
        }
        if (count < 0)
        {
            throw ArgumentOutOfRangeException("count", "Non-negative number required.");
        }
        
        if (connected())
        {
            // deleted by HYC, 2015/7/19 not clear buffer before sending message
            //clearReceiveBuffer();
            
            if (escape == NULL)
            {
                sendLen = interactive()->send(buffer, offset, count);
            }
            else
            {
                ByteArray dst;
                getEscapeBuffer(buffer, offset, count, dst, escape);
                sendLen = send(dst.data(), offset, dst.count());
            }
        }
        else
        {
            throw Exception("no_connect");
        }
        return sendLen;
    }
    int Channel::send(const ByteArray& buffer, const EscapeOption* escape)
    {
        return send(buffer.data(), 0, buffer.count(), escape);
    }
    
    int Channel::receiveBySize(byte* buffer, int bufferLength, int offset, int count, uint timeout, const EscapeOption* escape)
    {
        if (buffer == NULL)
        {
            throw ArgumentNullException("buffer");
        }
        if (offset < 0)
        {
            throw ArgumentOutOfRangeException("offset", "Non-negative number required.");
        }
        if (count < 0)
        {
            throw ArgumentOutOfRangeException("count", "Non-negative number required.");
        }
        
        if (connected())
        {
            if (escape != NULL)
            {
                return receiveBySizeWithEscape(buffer, bufferLength, offset, count, timeout, escape);
            }
            return receiveBySizeWithoutEscape(buffer, bufferLength, offset, count, timeout);
        }
        return 0;
    }
    int Channel::receiveBySize(byte* buffer, int bufferLength, int size, uint timeout)
    {
        return receiveBySize(buffer, bufferLength, 0, size, timeout);
    }
    int Channel::receiveBySize(ByteArray* buffer, int count, uint timeout, const EscapeOption* escape)
    {
        if (count < 0)
        {
            throw ArgumentOutOfRangeException("count", "Non-negative number required.");
        }
        
        if (connected())
        {
            if (escape != nullptr)
            {
                throw NotImplementedException("receiveBySizeWithEscape with ByteArray.");
                // return receiveBySizeWithEscape(buffer, count, timeout, escape);
            }
            return receiveBySizeWithoutEscape(buffer, count, timeout);
        }
        return 0;
    }
    
    int Channel::receiveByEndBytes(byte* buffer, int bufferLength, const byte* endBuffer, int ebLength, int suffix, uint timeout)
    {
        if (buffer == NULL)
        {
            throw ArgumentNullException("buffer");
        }
        if (endBuffer == NULL)
        {
            throw ArgumentNullException("endBuffer");
        }
        
        if(!description()->context()->useReceiveTimeout())
        {
            uint received = 0; //    已经读到的字节数
            if (connected())
            {
                if (buffer == NULL || endBuffer == NULL)
                    return received;
                
                bool bReceiveEndBytes = false; //    是否接收到结束符
                int nSuffix = 0, nStartByte = 0;
                uint startTime = TickTimeout::GetCurrentTickCount();
                uint deadTime = TickTimeout::GetDeadTickCount(startTime, timeout);
                do
                {
                    if (bReceiveEndBytes && nSuffix >= suffix) //  接收到的数据满足（结束符+结束符后长度）
                        break;
                    int nBytesToRead = interactive()->available();
                    if (nBytesToRead <= 0) //没有新数据，检测超时
                    {
                        Thread::msleep(1);
                        if (TickTimeout::IsTimeout(startTime, deadTime))
                            break;
                        continue;
                    }
                    if (received + 1 <= (uint)bufferLength)
                    {
                        received += interactive()->receive(buffer, received, 1);
                    }
                    else
                    {
                        //  缓冲区满，停止接收
                        return received;
                    }
                    
                    if (!bReceiveEndBytes)
                    {
                        if (buffer[received - 1] == endBuffer[nStartByte]) //   第n个结束符匹配(接收到的最后一个数据是否和结束符匹配)
                        {
                            nStartByte++; // 结束符匹配，检测下一个结束符
                            if (nStartByte == ebLength)
                            {
                                bReceiveEndBytes = true;
                            }
                        }
                        else
                        {
                            if (nStartByte == 0)
                            {
                                continue;
                            }
                            
                            nStartByte = 0;
                            if (buffer[received - 1] == endBuffer[nStartByte])
                            {
                                nStartByte++;
                            }
                        }
                    }
                    else //  结束符全部接收到，继续接收suffix
                    {
                        nSuffix++;
                    }
                    
                    deadTime = TickTimeout::GetDeadTickCount(timeout);
                } while (true);
                return received;
            }
            return received;
        }
        else
        {
            uint received = 0; //    已经读到的字节数
            if (connected())
            {
                if (buffer == NULL || endBuffer == NULL)
                    return received;
                
                bool bReceiveEndBytes = false; //    是否接收到结束符
                int nSuffix = 0, nStartByte = 0;
                do
                {
                    if (bReceiveEndBytes && nSuffix >= suffix) //  接收到的数据满足（结束符+结束符后长度）
                        break;
                    
                    //  每次接收一个字节，并判断。
                    int readlen = interactive()->receive(buffer, received, 1, timeout);
                    if(readlen <= 0)
                    {
                        return 0;
                    }
                    if (received + 1 <= (uint)bufferLength)
                    {
                        received += readlen;
                    }
                    else
                    {
                        return received; //  缓冲区满，停止接收
                    }
                    
                    if (!bReceiveEndBytes)
                    {
                        if (buffer[received - 1] == endBuffer[nStartByte]) //   第n个结束符匹配(接收到的最后一个数据是否和结束符匹配)
                        {
                            nStartByte++; // 结束符匹配，检测下一个结束符
                            if (nStartByte == ebLength)
                            {
                                bReceiveEndBytes = true;
                            }
                        }
                        else
                        {
                            if (nStartByte == 0)
                            {
                                continue;
                            }
                            
                            nStartByte = 0;
                            if (buffer[received - 1] == endBuffer[nStartByte])
                            {
                                nStartByte++;
                            }
                        }
                    }
                    else //  结束符全部接收到，继续接收suffix
                    {
                        nSuffix++;
                    }
                } while (true);
                return received;
            }
            return received;
        }
    }
    
    int Channel::receiveByEndBytes(byte* buffer, int bufferLength, const byte* startBuffer, int sbLength, const byte* endBuffer, int ebLength, int suffix, uint timeout)
    {
        if (buffer == NULL)
        {
            throw ArgumentNullException("buffer");
        }
        if (startBuffer == NULL)
        {
            throw ArgumentNullException("startBuffer");
        }
        if (endBuffer == NULL)
        {
            throw ArgumentNullException("endBuffer");
        }
        
        if (receiveStartBytes(startBuffer, sbLength, timeout))
        {
            //  接收到开始字节
            uint startCount = sbLength; //   要接收的开始字节长度
            for (uint i = 0; i < startCount && i < (uint)bufferLength; i++)
            {
                buffer[i] = startBuffer[i];
            }
            if ((uint)bufferLength > startCount)
            {
                byte* buffer2 = new byte[bufferLength - startCount];
                int count = receiveByEndBytes(buffer2, bufferLength - startCount, endBuffer, ebLength, suffix, timeout);
                memcpy(buffer + startCount, buffer2, count);
                delete buffer2;
                
                return count + startCount;
            }
            return bufferLength;
        }
        return 0;
    }
    
    bool Channel::receiveByte(byte& data, uint timeout)
    {
        byte buffer[8];
        bool result = (receiveBySize(buffer, sizeof(buffer), 1, timeout) == 1);
        data = buffer[0];
        return result;
    }
    
    bool Channel::receiveStartBytes(const byte* startBuffer, int sbLength, uint timeout)
    {
        if (startBuffer == NULL)
        {
            throw ArgumentNullException("startBuffer");
        }
        
        byte buffer[1024];
        return receiveStartBytes(buffer, sizeof(buffer), startBuffer, sbLength, timeout);
    }
    
    bool Channel::receiveStartBytes(byte* buffer, int bufferLength, const byte* startBuffer, int sbLength, uint timeout)
    {
        if (buffer == NULL)
        {
            throw ArgumentNullException("buffer");
        }
        if (startBuffer == NULL)
        {
            throw ArgumentNullException("startBuffer");
        }
        
        uint nReadStartCount = (uint)receiveByEndBytes(buffer, bufferLength, startBuffer, sbLength, 0, timeout);
        return nReadStartCount >= (uint)sbLength;
    }
    
    int Channel::receiveBySizeWithEscape(byte* buffer, int bufferLength, int offset, int count, uint timeout, const EscapeOption* escape)
    {
        if(!description()->context()->useReceiveTimeout())
        {
            uint startTime = TickTimeout::GetCurrentTickCount();
            uint deadTime = TickTimeout::GetDeadTickCount(startTime, timeout);
            
            int targetLength = escape->EscapeBuffer[0] == 0 ? 0 : escape->EscapeLength;
            int adjustCount = targetLength - (escape->ToEscapeBuffer[0] == 0 ? 0 : escape->ToEscapeLength);
            uint received = 0; //    已经读到的字节数
            uint startByte = 0;
            do
            {
                //  每次接收一个字节，并判断。
                int available = interactive()->available();
                if (available <= 0) //没有新数据，检测超时
                {
                    Thread::msleep(1);
                    if (TickTimeout::IsTimeout(startTime, deadTime))
                        break;
                    continue; // 没有数据，继续等待
                }
                if (received + 1 <= (uint)bufferLength)
                {
                    received += interactive()->receive(buffer, received + offset, 1);
                }
                else
                {
                    return received; //  缓冲区满，停止接收
                }
                
                //  判断是否收到匹配字符
                if (buffer[received + offset - 1] == escape->ToEscapeBuffer[startByte])
                {
                    startByte++;
                    if (startByte >= (uint)escape->ToEscapeLength)
                    {
                        if (escape->EscapeBuffer[0] != 0)
                        {
                            for (int i = 0; i < targetLength; i++)
                            {
                                buffer[received - escape->ToEscapeLength + i + offset] = escape->EscapeBuffer[i];
                            }
                        }
                        received += adjustCount;
                        
                        startByte = 0;
                    }
                }
                else
                {
                    startByte = 0;
                }
                if (received >= (uint)count && startByte <= 0)
                {
                    return received;
                }
                deadTime = TickTimeout::GetDeadTickCount(timeout);
            } while (true);
            return received;
        }
        else
        {
            int targetLength = escape->EscapeBuffer[0] == 0 ? 0 : escape->EscapeLength;
            int adjustCount = targetLength - (escape->ToEscapeBuffer[0] == 0 ? 0 : escape->ToEscapeLength);
            uint received = 0; //    已经读到的字节数
            uint startByte = 0;
            do
            {
                //  每次接收一个字节，并判断。
                int readlen = interactive()->receive(buffer, received + offset, 1, timeout);
                if(readlen <= 0)
                {
                    return 0;
                }
                if (received + 1 <= (uint)bufferLength)
                {
                    received += readlen;
                }
                else
                {
                    return received; //  缓冲区满，停止接收
                }
                
                //  判断是否收到匹配字符
                if (buffer[received + offset - 1] == escape->ToEscapeBuffer[startByte])
                {
                    startByte++;
                    if (startByte >= (uint)escape->ToEscapeLength)
                    {
                        if (escape->EscapeBuffer[0] != 0)
                        {
                            for (int i = 0; i < targetLength; i++)
                            {
                                buffer[received - escape->ToEscapeLength + i + offset] = escape->EscapeBuffer[i];
                            }
                        }
                        received += adjustCount;
                        
                        startByte = 0;
                    }
                }
                else
                {
                    startByte = 0;
                }
                if (received >= (uint)count && startByte <= 0)
                {
                    return received;
                }
            } while (true);
            return received;
        }
    }
    
    int Channel::receiveBySizeWithoutEscape(byte* buffer, int bufferLength, int offset, int count, uint timeout)
    {
        if(!description()->context()->useReceiveTimeout())
        {
            uint startTime = TickTimeout::GetCurrentTickCount();
            uint deadTime = TickTimeout::GetDeadTickCount(startTime, timeout);
            
            int available = interactive()->available();
            bool dataReady = false;
            do
            {
                // Added to fix crash issue if timeout is large for DTU service
                if (!interactive()->connected())
                {
                    return 0;
                }
                if (interactive()->available() >= count) //缓冲区已有足够的数据
                {
                    dataReady = true;
                    break;
                }
                uint now = TickTimeout::GetCurrentTickCount();
                if (interactive()->available() == available) //没有新数据，检测超时
                {
                    Thread::msleep(1);
                    if (TickTimeout::IsTimeout(startTime, deadTime, now))
                        break;
                }
                else
                {
                    available = interactive()->available();
                    deadTime = TickTimeout::GetDeadTickCount(now, timeout);
                }
            } while (true);
            
            if (interactive()->available() > 0)
            {
                return dataReady
                ? interactive()->receive(buffer, offset, count)
                : interactive()->receive(buffer, offset, Math::min(interactive()->available(), bufferLength));
            }
            return 0;
        }
        else
        {
            return interactive()->receive(buffer, offset, count, timeout);
        }
    }
    
    int Channel::receiveBySizeWithoutEscape(ByteArray* buffer, int count, uint timeout)
    {
        if(buffer == nullptr)
            return 0;
        
        if(!description()->context()->useReceiveTimeout())
        {
            uint startTime = TickTimeout::GetCurrentTickCount();
            uint deadTime = TickTimeout::GetDeadTickCount(startTime, timeout);
            
            byte temp[ReceiveBufferLength];
            memset(temp, 0, sizeof(temp));
            int readCount = 0;
            int totalCount = 0;
            int singleReceiveCount = 0;
            do
            {
                int available = interactive()->available();
                if(available > 0)
                {
                    singleReceiveCount = Math::min(count-totalCount, ReceiveBufferLength);
                    singleReceiveCount = Math::min(singleReceiveCount, available);
                    readCount = interactive()->receive(temp, 0, singleReceiveCount);
                    if(readCount > 0)
                    {
                        buffer->addRange(temp, readCount);
                        totalCount += readCount;
                    }
                }
               
                if(TickTimeout::IsTimeout(startTime, deadTime))
                    break;
                
                Thread::msleep(1);
            }while(readCount >= 0 && totalCount < count);
            return totalCount == count ? totalCount : 0;
        }
        else
        {
            return interactive()->receive(buffer, count, timeout);
        }
        return 0;
    }
    int Channel::receiveDirectly(ByteArray* buffer, int count)
    {
        if(buffer == nullptr)
            return 0;
        
        byte temp[ReceiveBufferLength];
        memset(temp, 0, sizeof(temp));
        int readCount = 0;
        int totalCount = 0;
        do
        {
            readCount = interactive()->receive(temp, 0, Math::min(count-totalCount, ReceiveBufferLength));
            if(readCount > 0)
            {
                buffer->addRange(temp, readCount);
                totalCount += readCount;
            }
        }while(readCount > 0 && totalCount < count);
        return totalCount;
    }
    
    void Channel::clearReceiveBuffer()
    {
        if(!description()->context()->useReceiveTimeout())
        {
            int available = interactive()->available();
            if (available > 0)
            {
                byte* buffer = new byte[available];
                receiveBySize(buffer, available, available, 1000);
                delete[] buffer;
            }
        }
        else
        {
            byte buffer[128];
            receiveBySizeWithoutEscape(buffer, sizeof(buffer), 0, sizeof(buffer), 2);	// ms
        }
    }
}
