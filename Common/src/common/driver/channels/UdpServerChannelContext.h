#ifndef UDPSERVERCHANNELCONTEXT_H
#define UDPSERVERCHANNELCONTEXT_H

#include "common/common_global.h"
#include "ChannelContext.h"
#include "EthernetContext.h"

namespace Driver
{
	class COMMON_EXPORT UdpServerChannelContext : public ChannelContext, public EthernetContext
	{
	public:
		UdpServerChannelContext() : UdpServerChannelContext("", 4096)
		{
		}
		UdpServerChannelContext(const string& address, int port) : EthernetContext(address, port), _peek(false)
		{
		}
		~UdpServerChannelContext(void)
		{
		}
        
		inline const bool getPeek() const
		{
			return _peek;
		}
		inline void setPeek(bool peek)
		{
			_peek = peek;
		}

	private:
        bool _peek;
	};
}

#endif // UDPSERVERCHANNELCONTEXT_H
