#include "../../exception/Exception.h"
#include "../../diag/Stopwatch.h"
#include "SerialInteractive.h"

using namespace Common;

namespace Driver
{
	SerialInteractive::SerialInteractive(DriverManager* dm, Channel* channel) : Interactive(dm, channel)
	{
		if(channel == NULL)
			throw ArgumentException("channel");
		if((SerialChannelContext*)channel->description()->context() == NULL)
			throw ArgumentException("channel");

		_port = NULL;
	}
	SerialInteractive::~SerialInteractive(void)
	{
		close();
	}
	bool SerialInteractive::open()
	{
		close();

		SerialChannelContext* scc = getChannelContext();

#ifdef DEBUG
		string message = Convert::convertStr("open a serial port, name = %s", scc->portName().c_str());
		Stopwatch sw(message);
#endif

		_port = new SerialPort(scc->portName());
		_port->setBaudRate(scc->baudRate());
		_port->setStopBits(scc->stopBits());
		_port->setDataBits(scc->dataBits());
		_port->setParity(scc->parity());
		_port->setHandshake(scc->handshake());
		_port->setUseSignal(scc->useSignal());
		_port->setRtsEnable(scc->rtsEnable());
		_port->setDtrEnable(scc->dtrEnable());

		return _port->open();
	}
	void SerialInteractive::close()
	{
		if(_port != NULL)
		{
			_port->close();
			delete _port;
			_port = NULL;
		}
	}

	bool SerialInteractive::connected()
	{
		return _port != NULL ? _port->isOpen() : false;
	}
	int SerialInteractive::available()
	{
		return (int)_port->available();
	}

	int SerialInteractive::send(byte* buffer, int offset, int count)
	{
#ifdef DEBUG
		Stopwatch sw("serial send", 1000);
#endif
		int len = 0;
		if(connected())
		{
			len = (int)_port->write((const char*)buffer, count);
		}
		return len;
	}
	int SerialInteractive::receive(byte* buffer, int offset, int count)
	{
#ifdef DEBUG
		Stopwatch sw("serial receive", 1000);
#endif
		int len = 0;
		if(connected())
		{
			len = (int)_port->read((char*)(buffer+offset), count);
		}
		return len;
	}

	int SerialInteractive::receive(byte* buffer, int offset, int count, uint timeout)
	{
#ifdef DEBUG
		Stopwatch sw("serial receive2", 1000);
#endif
		int len = 0;
		if(connected())
		{
			len = (int)_port->read((char*)(buffer+offset), count, timeout);
		}
		return len;
	}
}
