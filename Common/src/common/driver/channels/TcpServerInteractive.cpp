#include <assert.h>
#include "../../diag/Stopwatch.h"
#include "../../thread/Locker.h"
#include "../../thread/Thread.h"
#include "../../diag/Debug.h"
#include "../DriverManager.h"
#include "../devices/Device.h"
#include "common/Resources.h"
#include "TcpServerInteractive.h"

using namespace Common;

namespace Driver
{
	void tcp_acceptProc(void* parameter)
	{
		TcpServerInteractive* ti = (TcpServerInteractive*)parameter;
		assert(ti);
		ti->acceptProcInner();
	}
	void tcp_closeProc(void* parameter)
	{
		TcpServerInteractive* ti = (TcpServerInteractive*)parameter;
		assert(ti);
		ti->closeProcInner();
	}

	TcpServerInteractive::TcpServerInteractive(DriverManager* dm, Channel* channel) : Interactive(dm, channel),
		_tcpServer(NULL), _acceptThread(NULL), _closeThread(NULL)
	{
		if(channel == NULL)
			throw ArgumentNullException("channel");
		if((TcpServerChannelContext*)channel->description()->context() == NULL)
			throw ArgumentNullException("channel");

		_acceptAction = NULL;
		_closeAction = NULL;
        _sentAddress = "";
	}

	TcpServerInteractive::~TcpServerInteractive(void)
	{
		close();
	}

	bool TcpServerInteractive::open()
	{
		return rebind();
	}

	bool TcpServerInteractive::rebind()
	{
		static int bindingCount = 0;
		const static int MaxBindingCount = 5;
		const static uint MaxDelayTime = 2000;	// 2s

		_tcpServerMutex.lock();
		if(_tcpServer != NULL)
		{
			_tcpServer->close();
			delete _tcpServer;
			_tcpServer = NULL;
		}

		_tcpServer = new TcpServer();
#if !WIN32
		_tcpServer->setBlocking(false);
#endif
		TcpServerChannelContext* tcc = getChannelContext();
#ifdef DEBUG
		string message = Convert::convertStr("listen a socket, address = %s, port = %d",
                                             !tcc->address().empty() ? tcc->address().c_str() : "any", tcc->port());
		Stopwatch sw(message);
#endif

		bindingCount++;

		bool result = _tcpServer->bind(tcc->address(), tcc->port());
		if(result)
		{
			result = _tcpServer->listen(tcc->maxConnections());
			_tcpServerMutex.unlock();
			if(result)
			{
				if (result)
				{
					if (tcc->sendBufferSize() > 0)
					{
						_tcpServer->setSendBufferSize(tcc->sendBufferSize());
					}
					if (tcc->receiveBufferSize() > 0)
					{
						_tcpServer->setReceiveBufferSize(tcc->receiveBufferSize());
					}
					if (tcc->isNoDelay())
					{
						_tcpServer->setNoDelay(true);
					}
				}

				_acceptThread = new Thread();
				_acceptThread->setName("tcp_acceptProc");
				_acceptThread->startProc(tcp_acceptProc, this, 1);

				_closeThread = new Thread();
				_closeThread->setName("tcp_closeProc");
				_closeThread->startProc(tcp_closeProc, this, 1000);

				return true;
			}
			else
			{
				Debug::writeFormatLine("Failed to listen the TcpServerInteractive socket. address = %s, port = %d", tcc->address().c_str(), tcc->port());

				if(bindingCount < MaxBindingCount)
				{
					Thread::msleep(MaxDelayTime);
					return rebind();
				}
				else
				{
					string message = Convert::convertStr(Resources::getString("SocketListenFailed").c_str(), tcc->address().c_str(), tcc->port());
					Trace::writeLine(message.c_str(), Trace::Error);
					throw Exception(message.c_str());
				}
			}
		}
		else
		{
			_tcpServerMutex.unlock();
			Debug::writeFormatLine("Failed to bind the TcpServerInteractive socket. address = %s, port = %d", tcc->address().c_str(), tcc->port());

			if(bindingCount < MaxBindingCount)
			{
				Thread::msleep(MaxDelayTime);
				return rebind();
			}
			else
			{
				string message = Convert::convertStr(Resources::getString("SocketBindingFailed").c_str(), tcc->address().c_str(), tcc->port());
				Trace::writeLine(message.c_str(), Trace::Error);
				throw Exception(message.c_str());
			}
		}
		return false;
	}

	void TcpServerInteractive::close()
	{
		_tcpServerMutex.lock();
		if(_tcpServer != NULL)
		{
			_tcpServer->close();
			delete _tcpServer;
			_tcpServer = NULL;
		}
		_tcpServerMutex.unlock();

		if(_acceptThread != NULL)
		{
			_acceptThread->stop();
			delete _acceptThread;
			_acceptThread = NULL;
		}
		if (_closeThread != NULL)
		{
			_closeThread->stop();
			delete _closeThread;
			_closeThread = NULL;
		}
	}

	bool TcpServerInteractive::connected()
    {
        Locker locker(&_mutexClients);
        for (uint i = 0; i < _clients.count(); i++)
        {
            TcpBackgroudReceiver* client = _clients.at(i);
            if (client != NULL)
            {
                if (client->connected())
                {
                    return true;
                }
            }
        }
        return false;
        //		throw NotSupportException("Cannot support this method, use 'TcpBackgroudReceiver' instead.");
    }
	int TcpServerInteractive::available()
	{
        return 0;
//		throw NotSupportException("Cannot support this method, use 'TcpBackgroudReceiver' instead.");
	}

	int TcpServerInteractive::send(byte* buffer, int offset, int count)
	{
        Locker locker(&_mutexClients);
        for (uint i = 0; i < _clients.count(); i++)
        {
            TcpBackgroudReceiver* client = _clients.at(i);
            if (client != NULL)
            {
                bool sent = false;
                if(_sentAddress.empty())
                {
                    sent = true;
                }
                else
                {
                    string peerAddr = client->peerAddr();
                    if(peerAddr == _sentAddress)
                    {
                        sent = true;
                    }
                }
                
                if(sent)
                {
                    // todo: add a pool that it contains buffer.
                    client->send(buffer, offset, count);
                }
            }
        }
        return count;
//		throw NotSupportException("Cannot support this method, use 'TcpBackgroudReceiver' instead.");
	}

	int TcpServerInteractive::receive(byte* buffer, int offset, int count)
	{
		throw NotSupportException("Cannot support this method, use 'TcpBackgroudReceiver' instead.");
	}

	void TcpServerInteractive::acceptProcInner()
	{
		if(_tcpServer != NULL)
		{
			int socketId = _tcpServer->accept();

			Locker locker(&_tcpServerMutex);
			bool valid = _tcpServer != NULL ? _tcpServer->isValid() : false;
			if(valid && socketId != -1)
			{
				TcpClient* client = new TcpClient(socketId);
                client->setBlocking(false);
                Locker locker(&_mutexClients);
				_clients.add(new TcpBackgroudReceiver(manager(), _channel, client));

				if(_acceptAction != NULL)
				{
					_acceptAction(client->peerAddr(), client->peerPort());
				}
                
				Trace::writeLine(Convert::convertStr("client connected, peerAddr: %s, socketId: %d",
					client->peerAddr().c_str(), socketId), Trace::System);
			}
		}
	}

	void TcpServerInteractive::closeProcInner()
	{
		_mutexClients.lock();
		for (uint i = 0; i < _clients.count(); i++)
		{
			TcpBackgroudReceiver* client = _clients.at(i);
			if (client != NULL)
			{
				if (!client->connected())
				{
					Trace::writeLine(Convert::convertStr("Remove an unused client, peerAddr: %s, socketId: %d",
						client->peerAddr().c_str(), client->socketId()), Trace::System);
					_clients.remove(client);

					if (_closeAction != NULL)
					{
						string addr = client->tcpClient()->peerAddr();
						int port = client->tcpClient()->peerPort();
						_closeAction(addr, port);
					}
				}
			}
		}
		_mutexClients.unlock();
	}
}
