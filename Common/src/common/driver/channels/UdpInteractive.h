#ifndef UDPINTERACTIVE_H
#define UDPINTERACTIVE_H

#include "common/common_global.h"
#include "Interactive.h"
#include "UdpChannelContext.h"
#include "ChannelDescription.h"
#include "Channel.h"
#include "common/net/UdpClient.h"

namespace Driver
{
	class COMMON_EXPORT UdpInteractive : public Interactive
	{
	public:
		UdpInteractive(DriverManager* dm, Channel* channel);
		~UdpInteractive(void);

		bool open() override;
		void close() override;

		bool connected() override;
		int available() override;

		int send(byte* buffer, int offset, int count) override;
		int receive(byte* buffer, int offset, int count) override;

	private:
		inline UdpChannelContext* getChannelContext()
		{
			return (UdpChannelContext*)(_channel->description()->context());
		}

	private:
		UdpClient* _udpClient;
	};
}
#endif // UDPINTERACTIVE_H
