
#include "DtuChannelContext.h"

namespace Driver
{
    DtuChannelContext::DtuChannelContext(const string& id, const string& vendor) : _id(id), _vendor(vendor)
	{
	}
	DtuChannelContext::~DtuChannelContext()
	{
	}

    void DtuChannelContext::setId(const string& id)
    {
        _id = id;
    }
    const string& DtuChannelContext::id() const
    {
        return _id;
    }
    void DtuChannelContext::setVendor(const string& vendor)
    {
        _vendor = vendor;
    }
    const string& DtuChannelContext::vendor() const
    {
        return _vendor;
    }
}
