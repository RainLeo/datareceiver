#pragma once
#ifndef DTUCAIMAOSERVICE_H
#define DTUCAIMAOSERVICE_H

#include "DtuService.h"
#include "common/common_global.h"
#include "common/system/Singleton.h"
#include <list>
#ifdef WIN32
typedef unsigned long ULONG;
typedef unsigned short USHORT;
typedef unsigned char UCHAR;
typedef unsigned int UINT;
typedef int                 BOOL;
#ifndef FALSE
#define FALSE               0
#endif

#ifndef TRUE
#define TRUE                1
#endif
#include "gprsdll.h"
#endif

using namespace std;
using namespace Common;

namespace Driver
{
	class COMMON_EXPORT DtuCaiMaoService : public DtuService
	{
	public:
        DtuCaiMaoService();
        ~DtuCaiMaoService(void);
        static DtuCaiMaoService * Instance() { return Singleton<DtuCaiMaoService>::instance(); }

        bool initialize(int port) override;
        bool unInitialize() override;

        int send(const string &id, byte* buffer, int offset, int count) override;
        bool receive(string &id, uint timeout, string &data) override;
	};
}

#endif //DTUCAIMAOSERVICE_H
