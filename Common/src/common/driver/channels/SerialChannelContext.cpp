
#include "SerialChannelContext.h"

namespace Driver
{
    SerialChannelContext::SerialChannelContext(const string& portName) : _serial(portName)
	{
	}
	SerialChannelContext::~SerialChannelContext()
	{
	}

    void SerialChannelContext::setPortName(const string& portName)
    {
        _serial.portName = portName;
    }
    const string& SerialChannelContext::portName() const
    {
        return _serial.portName;
    }
	void SerialChannelContext::setBaudRate(int baudRate)
	{
		_serial.baudRate = baudRate;
	}
	void SerialChannelContext::setBaudRate(const string& baudRate)
	{
		int value;
		if (Convert::parseInt32(baudRate, value))
		{
			_serial.baudRate = value;
		}
	}
	int SerialChannelContext::baudRate() const
	{
		return _serial.baudRate;
	}
	void SerialChannelContext::setDataBits(SerialInfo::DataBitsType dataBits)
	{
		_serial.dataBits = dataBits;
	}
	void SerialChannelContext::setDataBits(const string& dataBits)
	{
        _serial.dataBits = SerialInfo::parseDataBits(dataBits);
	}
	SerialInfo::DataBitsType SerialChannelContext::dataBits() const
	{
		return _serial.dataBits;
	}
	void SerialChannelContext::setParity(SerialInfo::ParityType parity)
	{	
		_serial.parity = parity;
	}
	void SerialChannelContext::setParity(const string& parity)
	{
		_serial.parity = SerialInfo::parseParity(parity);
	}
	SerialInfo::ParityType SerialChannelContext::parity() const
	{	
		return _serial.parity;
	}
	void SerialChannelContext::setStopBits(SerialInfo::StopBitsType stopBits)
	{
		_serial.stopBits = stopBits;
	}
	void SerialChannelContext::setStopBits(const string& stopBits)
	{
		_serial.stopBits = SerialInfo::parseStopBits(stopBits);
	}
	SerialInfo::StopBitsType SerialChannelContext::stopBits() const
	{
		return _serial.stopBits;
	}
	void SerialChannelContext::setHandshake(SerialInfo::HandshakeType handshake)
	{
		_serial.handshake = handshake;
	}
	void SerialChannelContext::setHandshake(const string& handshake)
	{
		_serial.handshake = SerialInfo::parseHandshake(handshake);
	}
	SerialInfo::HandshakeType SerialChannelContext::handshake() const
	{
		return _serial.handshake;
	}
    bool SerialChannelContext::rtsEnable() const
    {
        return _serial.rtsEnable;
    }
    void SerialChannelContext::setRtsEnable(bool enabled)
    {
        _serial.rtsEnable = enabled;
    }
    bool SerialChannelContext::dtrEnable() const
    {
        return _serial.dtrEnable;
    }
    void SerialChannelContext::setDtrEnable(bool enabled)
    {
        _serial.dtrEnable = enabled;
    }
    bool SerialChannelContext::useSignal() const
    {
        return _serial.useSignal;
    }
    void SerialChannelContext::setUseSignal(bool useSignal)
    {
        _serial.useSignal = useSignal;
    }
    
    const SerialInfo& SerialChannelContext::serial() const
    {
        return _serial;
    }
    void SerialChannelContext::setSerial(const SerialInfo& serial)
    {
        _serial = serial;
    }
}
