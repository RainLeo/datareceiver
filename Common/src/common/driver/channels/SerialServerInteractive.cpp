#include <assert.h>
#include "SerialServerInteractive.h"
#include "../devices/Device.h"
#include "../DriverManager.h"
#include "common/Resources.h"
#include "../../diag/Stopwatch.h"
#include "../../thread/Locker.h"
#include "../../thread/Thread.h"
#include "../../diag/Debug.h"

using namespace Common;

namespace Driver
{
	void serial_receiveProc(void* parameter)
	{
		SerialServerInteractive* ui = (SerialServerInteractive*)parameter;
		assert(ui);
		ui->receiveProcInner();
	}

	SerialServerInteractive::SerialServerInteractive(DriverManager* dm, Channel* channel) : Interactive(dm, channel),
		_port(NULL), _receiveThread(NULL), _device(NULL)
	{
		if(channel == NULL)
			throw ArgumentException("channel");
		if((TcpServerChannelContext*)channel->description()->context() == NULL)
			throw ArgumentException("channel");
	}

	SerialServerInteractive::~SerialServerInteractive(void)
	{
		close();
	}

	bool SerialServerInteractive::open()
	{
		_receiveThread = new Thread();
		_receiveThread->setName("serial_receiveProc");
		_receiveThread->startProc(serial_receiveProc, this, 1);

        SerialChannelContext* scc = getChannelContext();
        
#ifdef DEBUG
        string message = Convert::convertStr("open a serial port, name = %s", scc->portName().c_str());
        Stopwatch sw(message);
#endif
        
        _port = new SerialPort(scc->portName());
        _port->setBaudRate(scc->baudRate());
        _port->setStopBits(scc->stopBits());
        _port->setDataBits(scc->dataBits());
        _port->setParity(scc->parity());
        _port->setHandshake(scc->handshake());
        _port->setUseSignal(scc->useSignal());
        _port->setRtsEnable(scc->rtsEnable());
        _port->setDtrEnable(scc->dtrEnable());
        
        return _port->open();
	}

	void SerialServerInteractive::close()
	{
		if(_receiveThread != NULL)
		{
			_receiveThread->stop();
			delete _receiveThread;
			_receiveThread = NULL;
		}
        
        if(_port != NULL)
        {
            _port->close();
            delete _port;
            _port = NULL;
        }
        
		_device = NULL;
	}

    bool SerialServerInteractive::connectdInner() const
    {
        return _port != NULL ? _port->isOpen() : false;
    }
	bool SerialServerInteractive::connected()
	{
		return connectdInner();
	}
	int SerialServerInteractive::available()
	{
        return connected() ? (int)_port->available() : 0;
	}

	int SerialServerInteractive::send(byte* buffer, int offset, int count)
	{
#ifdef DEBUG
        Stopwatch sw("serial send", 1000);
#endif
        int len = 0;
        if(connected())
        {
            len = (int)_port->write((const char*)buffer, count);
        }
        return len;
	}

	int SerialServerInteractive::receive(byte* buffer, int offset, int count)
	{
#ifdef DEBUG
        Stopwatch sw("serial receive", 1000);
#endif
        int len = 0;
        if(connected())
        {
            len = (int)_port->read((char*)(buffer+offset), count);
        }
        return len;
	}
	int SerialServerInteractive::receive(byte* buffer, int offset, int count, uint timeout)
	{
#ifdef DEBUG
        Stopwatch sw("serial receive2", 1000);
#endif
        int len = 0;
        if(connected())
        {
            len = (int)_port->read((char*)(buffer+offset), count, timeout);
        }
        return len;
	}

	void SerialServerInteractive::receiveProcInner()
	{
#ifdef DEBUG
		Stopwatch sw("receiveProc", 1000);
#endif
		if(_device == NULL)
		{
			DriverManager* dm = manager();
			assert(dm);
			_device = dm->getDevice(_channel);
		}
        
        if (connected() && available() > 0)
        {
			receiveFromBuffer(_device);
        }
	}
}
