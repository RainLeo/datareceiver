#ifndef BLUETOOTHRECEIVECLIENT_H
#define BLUETOOTHRECEIVECLIENT_H

#include "common/common_global.h"
#include "Interactive.h"
#include "Channel.h"
#include "BluetoothServerChannelContext.h"
#include "BluetoothClient.h"
#include "BluetoothServer.h"
#include "../../data/LoopArray.h"
#include "../../thread/Thread.h"
#include "../../thread/TickTimeout.h"
#include "../../thread/Locker.h"
#include "../../system/TimeSpan.h"

using namespace std;
using namespace Common;

namespace Driver
{
	class Device;
	class Channel;
	class DriverManager;
    class DeviceDescription;
    class COMMON_EXPORT BluetoothServerClient : public BackgroudReceiver
	{
	public:
		BluetoothServerClient(DriverManager* dm, BluetoothClient* client, Channel* channel, bool autoDelete = true);
		~BluetoothServerClient();

		int available();
		bool connected() const;

		inline BluetoothClient* getBluetoothClient() const
		{
			return _client;
		}
		inline string peerAddr() const
		{
			return _client->peerAddr();
		}
		inline int socketId() const
		{
			return _client->socketId();
		}
        
        int send(byte* buffer, int offset, int count);

	private:
		friend void bluetooth_receiveProc(void* parameter);
		void receiveProcInner();

		void createDevice(const Channel* channel);
        void updateDevice(Channel* channel);

		DriverManager* manager()
		{
			return _manager;
		}

	private:
		BluetoothClient* _client;
		uint _startTime;
        bool _autoDelete;

		BluetoothChannelBaseContext* _context;

		Thread* _receiveThread;

		Channel* _channel;
		Device* _device;
        DeviceDescription* _dd;

		DriverManager* _manager;
	};

	typedef Vector<BluetoothServerClient> BluetoothServerClients;
}
#endif // BLUETOOTHRECEIVECLIENT_H
