#pragma once
#ifndef DTULICHUANGSERVICE_H
#define DTULICHUANGSERVICE_H

#include "DtuService.h"
#include "common/common_global.h"
#include "common/system/Singleton.h"
#include "DtuChannelContext.h"
#include "common/net/UdpServer.h"
#include <map>
#include <list>

using namespace std;
using namespace Common;

namespace Driver
{
	class COMMON_EXPORT DtuLiChuangService : public DtuService
	{
	public:
        DtuLiChuangService();
        ~DtuLiChuangService(void);
        static DtuLiChuangService * Instance() { return Singleton<DtuLiChuangService>::instance(); }

        bool initialize(int port) override;
        bool unInitialize() override;

        bool connected()  override;
        int available(const string &id) override;

        int send(const string &id, byte* buffer, int offset, int count) override;
        bool receive(string &id, uint timeout, string &data) override;

    private:
        UdpServer* m_pUdpServer;
        typedef map<string, struct sockaddr_in> MapDtu2Addr;
        MapDtu2Addr m_mapDtu2Addr;
        typedef map<string, string> MapAddr2Dtu;
        MapAddr2Dtu m_mapAddr2Dtu;

        bool rebind(int port);
	};
}

#endif //DTULICHUANGSERVICE_H
