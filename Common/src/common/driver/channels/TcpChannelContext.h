#ifndef TCPCHANNELCONTEXT_H
#define TCPCHANNELCONTEXT_H

#include <stdio.h>
#include "common/common_global.h"
#include "ChannelContext.h"
#include "common/system/Convert.h"
#include "common/system/TimeSpan.h"
#include "EthernetContext.h"

using namespace Common;

namespace Driver
{
	class TcpChannelBaseContext : public ChannelContext, public EthernetContext
	{
	public:
        TcpChannelBaseContext() : TcpChannelBaseContext("", 0)
		{
		}
        TcpChannelBaseContext(const string& address, int port) : EthernetContext(address, port)
        {
            _receiveBufferSize = 0;
            _sendBufferSize = 0;
            _noDelay = false;
            _closeTimeout = TimeSpan(0, 1, 0);
            _syncSendReceive = true;
        }
		~TcpChannelBaseContext()
        {
        }

		inline int sendBufferSize() const
		{
			return _sendBufferSize;
		}
		inline void setSendBufferSize(int bufferSize)
		{
			_sendBufferSize = bufferSize;
		}
		inline int receiveBufferSize() const
		{
			return _receiveBufferSize;
		}
		inline void setReceiveBufferSize(int bufferSize)
		{
			_receiveBufferSize = bufferSize;
		}

		inline bool isNoDelay() const
		{
			return _noDelay;
		}
		inline void setNoDelay(bool noDelay = true)
		{
			_noDelay = noDelay;
		}
        
        inline const TimeSpan& closeTimout() const
        {
            return _closeTimeout;
        }
        inline void setCloseTimeout(const TimeSpan& timeout)
        {
            _closeTimeout = timeout;
        }
		inline void setCloseTimeout(uint milliSeconds)
        {
            _closeTimeout = TimeSpan::fromMilliseconds((double)milliSeconds);
        }
        
        virtual void copyFrom(TcpChannelBaseContext* context)
        {
            _address = context->_address;
            _port = context->_port;
            
            _receiveBufferSize = context->_receiveBufferSize;
            _sendBufferSize = context->_sendBufferSize;
            
            _noDelay = context->_noDelay;
            
            _closeTimeout = context->_closeTimeout;
            _syncSendReceive = context->_syncSendReceive;
        }
        
        inline bool syncSendReceive() const
        {
            return _syncSendReceive;
        }
        inline void setSyncSendReceive(bool sync = true)
        {
            _syncSendReceive = sync;
        }

	protected:
        int _receiveBufferSize;
		int _sendBufferSize;

		bool _noDelay;
        
   		TimeSpan _closeTimeout;
        bool _syncSendReceive;
	};
    
    class TcpChannelContext : public TcpChannelBaseContext
    {
    public:
        TcpChannelContext() : TcpChannelBaseContext()
        {
            _openTimeout = 3000;
        }
        TcpChannelContext(const string& address, int port) : TcpChannelBaseContext(address, port)
        {
        	_openTimeout = 3000;
        }
        ~TcpChannelContext()
        {
        }
        
        inline uint getOpenTimeout() const
        {
            return _openTimeout;
        }
        inline void setOpenTimeout(uint timeout)
        {
            if(timeout > 0)
            {
                _openTimeout = timeout;
            }
        }
        inline void setOpenTimeout(TimeSpan timeout)
        {
            if(timeout != TimeSpan::Zero)
            {
                _openTimeout = (uint)timeout.totalMilliseconds();
            }
        }
        
        void copyFrom(TcpChannelBaseContext* context) override
        {
            TcpChannelBaseContext::copyFrom(context);
            
            TcpChannelContext* tc = (TcpChannelContext*)context;
            _openTimeout = tc->_openTimeout;
        }
        
    private:
        uint _openTimeout;
    };
}

#endif // TCPCHANNELCONTEXT_H
