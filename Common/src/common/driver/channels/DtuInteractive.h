#ifndef DTUINTERACTIVE_H
#define DTUINTERACTIVE_H

#include "common/common_global.h"
#include "Interactive.h"
#include "DtuChannelContext.h"
#include "ChannelDescription.h"
#include "Channel.h"
#include "DtuService.h"

using namespace std;

namespace Driver
{
    extern string string_empty;
    class COMMON_EXPORT DtuInteractive : public Interactive
	{
	public:
		DtuInteractive(DriverManager* dm, Channel* channel);
		~DtuInteractive(void);

		bool open() override;
		void close() override;

		bool connected() override;
		int available() override;

		int send(byte* buffer, int offset, int count) override;
		int receive(byte* buffer, int offset, int count) override;
		int receive(byte* buffer, int offset, int count, uint timeout) override;

        const string & getDtuId();

	private:
		inline DtuChannelContext* getChannelContext()  
		{
            if (_channel == NULL || _channel->description() == NULL) return NULL;

			return (DtuChannelContext*)(_channel->description()->context());
		}

        DtuService * getDtuService();
	};
}

#endif //DtuInteractive_H
