#ifndef TCPRECEIVECLIENT_H
#define TCPRECEIVECLIENT_H

#include "common/common_global.h"
#include "Interactive.h"
#include "Channel.h"
#include "TcpServerChannelContext.h"
#include "common/net/TcpClient.h"
#include "common/net/TcpServer.h"
#include "common/data/LoopArray.h"
#include "common/thread/Thread.h"
#include "common/thread/TickTimeout.h"
#include "common/thread/Locker.h"
#include "common/system/TimeSpan.h"

using namespace std;
using namespace Common;

namespace Driver
{
	class Device;
	class Channel;
    class DriverManager;
    class DeviceDescription;
    class COMMON_EXPORT TcpBackgroudReceiver : public BackgroudReceiver
	{
	public:
		TcpBackgroudReceiver(DriverManager* dm, Channel* channel, TcpClient* client, bool isServer = true);
		~TcpBackgroudReceiver();

		int available();
		bool connected() const;

		inline TcpClient* tcpClient() const
		{
			return _client;
		}
		inline const string& peerAddr() const
		{
			return _client->peerAddr();
		}
        inline int peerPort() const
        {
            return _client->peerPort();
        }
		inline int socketId() const
		{
			return _client->socketId();
		}
        
        int send(byte* buffer, int offset, int count);

	private:
		friend void tcp_receiveProc(void* parameter);
		void receiveProcInner();

		void createDevice(const Channel* channel);
        void updateDevice(Channel* channel);
        
        DriverManager* manager()
        {
            return _manager;
        }

	private:
		TcpClient* _client;
		uint _startTime;
        bool _isServer;

		TcpChannelBaseContext* _context;

		Thread* _receiveThread;

		Channel* _channel;
		Device* _device;
        DeviceDescription* _dd;
        
        Device* _originalDevice;
        
        DriverManager* _manager;
	};
    
	typedef Vector<TcpBackgroudReceiver> TcpBackgroudReceivers;
}
#endif // TCPRECEIVECLIENT_H
