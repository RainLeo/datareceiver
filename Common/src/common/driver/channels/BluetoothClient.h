#ifndef BLUETOOTHCLIENT_H
#define BLUETOOTHCLIENT_H

#include <time.h>
#include <string>
#include "common/common_global.h"

using namespace std;

namespace Driver
{
	class COMMON_EXPORT BluetoothClient
	{
	public:
		BluetoothClient(int socketId = -1);
		~BluetoothClient(void);

		void close();

		int available() const;
		int write(const byte *data, uint len);
		int read(byte *data, uint maxlen);
		int receiveBySize(BluetoothClient* client, byte* buffer, int bufferLength, int offset, int count, uint timeout = 3000);

		bool connected();

		inline string peerAddr() const
		{
			return _peerAddr;
		}
		inline int peerPort() const
		{
			return _peerPort;
		}
		inline int socketId() const
		{
			return _socket;
		}

		int sendBufferSize() const;
		void setSendBufferSize(int bufferSize);
		int receiveBufferSize() const;
		void setReceiveBufferSize(int bufferSize);

	private:
		int _socket;
		bool _connected;
		string _peerAddr;
		int _peerPort;
	};
};
#endif //BLUETOOTHCLIENT_H
