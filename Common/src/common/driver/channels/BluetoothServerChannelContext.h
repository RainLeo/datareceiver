#ifndef BLUETOOTHSERVERCHANNELCONTEXT_H
#define BLUETOOTHSERVERCHANNELCONTEXT_H

#include "common/common_global.h"
#include "ChannelContext.h"
#include "../../system/TimeSpan.h"
#include "BluetoothChannelContext.h"

using namespace Common;

namespace Driver
{
	class COMMON_EXPORT BluetoothServerChannelContext : public BluetoothChannelBaseContext
	{
	public:
		BluetoothServerChannelContext(int maxConnections = 65535) : BluetoothChannelBaseContext()
		{
			_maxConnections = maxConnections;
		}
		~BluetoothServerChannelContext(void)
		{
		}

		inline int maxConnections() const
		{
			return _maxConnections;
		}
		inline void setMaxConnections(int maxConnections)
		{
			_maxConnections = maxConnections;
		}
        
		void copyFrom(BluetoothChannelBaseContext* context)
        {
			BluetoothChannelBaseContext::copyFrom(context);
            
            BluetoothServerChannelContext* tc = dynamic_cast<BluetoothServerChannelContext*>(context);
            _maxConnections = tc->_maxConnections;
        }

	private:
		int _maxConnections;
	};
}

#endif // BLUETOOTHSERVERCHANNELCONTEXT_H
