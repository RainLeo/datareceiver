#include <assert.h>
#include "UdpServerInteractive.h"
#include "../devices/Device.h"
#include "../DriverManager.h"
#include "common/Resources.h"
#include "../../diag/Stopwatch.h"
#include "../../thread/Locker.h"
#include "../../thread/Thread.h"
#include "../../diag/Debug.h"

using namespace Common;

namespace Driver
{
	void udp_receiveProc(void* parameter)
	{
		UdpServerInteractive* ui = (UdpServerInteractive*)parameter;
		assert(ui);
		ui->receiveProcInner();
	}

	UdpServerInteractive::UdpServerInteractive(DriverManager* dm, Channel* channel) : Interactive(dm, channel),
		_udpServer(NULL), _receiveThread(NULL), _device(NULL)
	{
		if(channel == NULL)
			throw ArgumentException("channel");
		if((TcpServerChannelContext*)channel->description()->context() == NULL)
			throw ArgumentException("channel");
	}

	UdpServerInteractive::~UdpServerInteractive(void)
	{
		close();
	}

	bool UdpServerInteractive::open()
	{
		_receiveThread = new Thread();
		_receiveThread->setName("udp_receiveProc");
		_receiveThread->startProc(udp_receiveProc, this, 1);

		return rebind();
	}

	bool UdpServerInteractive::rebind()
	{
		static int bindingCount = 0;
		const static int MaxBindingCount = 5;
		const static uint MaxDelayTime = 2000;	// 2s

		if(_udpServer != NULL)
		{
			_udpServer->close();
			delete _udpServer;
			_udpServer = NULL;
		}
		_udpServer = new UdpServer();

		UdpServerChannelContext* context = getChannelContext();
#ifdef DEBUG
		string message = Convert::convertStr("bind a udp socket, port = %d", context->port());
		Stopwatch sw(message);
#endif

		bindingCount++;

		bool result = _udpServer->bind(context->address(), context->port());
		if(!result)
		{
			Debug::writeFormatLine("Failed to bind the UdpServerInteractive socket. port = %d", context->port());

			if(bindingCount < MaxBindingCount)
			{
				Thread::msleep(MaxDelayTime);
				return rebind();
			}
			else
			{
				string message = Convert::convertStr(Resources::getString("SocketBindingFailed2").c_str(), context->port());
				Trace::writeLine(message.c_str(), Trace::Error);
				throw Exception(message.c_str());
			}
			return false;
		}
		else
		{
			return true;
		}
	}

	void UdpServerInteractive::close()
	{
		if(_receiveThread != NULL)
		{
			_receiveThread->stop();
			delete _receiveThread;
			_receiveThread = NULL;
		}
        
        if(_udpServer != NULL)
        {
            _udpServer->close();
            delete _udpServer;
            _udpServer = NULL;
        }
        
		_device = NULL;
	}

    bool UdpServerInteractive::connectdInner() const
    {
        return _udpServer != NULL;
    }
	bool UdpServerInteractive::connected()
	{
		return connectdInner();
	}
	int UdpServerInteractive::available()
	{
		return _udpServer->available();
	}

	int UdpServerInteractive::send(byte* buffer, int offset, int count)
	{
		throw new NotSupportException("UdpServer can not send anything.");
	}

	int UdpServerInteractive::receive(byte* buffer, int offset, int count)
	{
#ifdef DEBUG
		Stopwatch sw("socket recv", 1000);
#endif

		int len = 0;
		if(connected())
		{
            UdpServerChannelContext* context = getChannelContext();
            len = _udpServer->read(buffer + offset, count, context->getPeek());
        }
        return len;
	}
	int UdpServerInteractive::receive(byte* buffer, int offset, int count, uint timeout)
	{
#ifdef DEBUG
		Stopwatch sw("socket recv2", 1000);
#endif
		int len = 0;
		if(connected())
		{
			len = _udpServer->receive(buffer+offset, count, timeout);
		}
		return len;
	}

	void UdpServerInteractive::receiveProcInner()
	{
#ifdef DEBUG
		Stopwatch sw("receiveProc", 1000);
#endif
		if(_device == NULL)
		{
			DriverManager* dm = manager();
			assert(dm);
			_device = dm->getDevice(_channel);
		}
        
        if (connected() && available() > 0)
        {
			receiveFromBuffer(_device);
        }
	}
}
