#ifndef BLUETOOTHINTERACTIVE_H
#define BLUETOOTHINTERACTIVE_H

#include "common/common_global.h"
#include "Interactive.h"
#include "BluetoothChannelContext.h"
#include "ChannelDescription.h"
#include "Channel.h"
#include "BluetoothClient.h"
#include "BluetoothServerClient.h"

namespace Driver
{
	class COMMON_EXPORT BluetoothInteractive : public Interactive
	{
	public:
		BluetoothInteractive(DriverManager* dm, Channel* channel);
		~BluetoothInteractive(void);

		bool open();
		void close();

		bool connected();
		int available();

		int send(byte* buffer, int offset, int count);
		int receive(byte* buffer, int offset, int count);

	private:
		inline BluetoothChannelContext* getChannelContext()  
		{
			return (BluetoothChannelContext*)(_channel->description()->context());
		}

		friend class BluetoothServerClient;

		void updateBluetoothClient(BluetoothClient* tcpClient)
		{
			_bluetoothClient = tcpClient;
			_autoDelete = false;
		}

	private:
		BluetoothClient* _bluetoothClient;
		bool _autoDelete;
        
        BluetoothServerClient* _receiveClient;
	};
}
#endif // BLUETOOTHINTERACTIVE_H
