#include "../../exception/Exception.h"
#include "../../diag/Stopwatch.h"
#include "../../thread/Thread.h"
#include "Channel.h"
#include "BluetoothChannelContext.h"
#include "BluetoothInteractive.h"
#include "ChannelDescription.h"

using namespace Common;

namespace Driver
{
	BluetoothInteractive::BluetoothInteractive(DriverManager* dm, Channel* channel) : Interactive(dm, channel)
	{
		if(channel == NULL)
			throw ArgumentException("channel");
		if((BluetoothChannelContext*)channel->description()->context() == NULL)
			throw ArgumentException("channel");

		_bluetoothClient = NULL;
		_autoDelete = true;
        _receiveClient = NULL;
	}

	BluetoothInteractive::~BluetoothInteractive(void)
	{
		close();
	}

	bool BluetoothInteractive::open()
	{
		close();

		BluetoothChannelContext* tcc = getChannelContext();

//#ifdef DEBUG
//		string message = Convert::convertStr("connect a host, address = %s, port = %d", tcc->address().c_str(), tcc->port());
//		Stopwatch sw(message, 100);
//#endif

		_bluetoothClient = new BluetoothClient();
		//bool result = _bluetoothClient->connectToHost(tcc->address().c_str(), tcc->port(), tcc->getOpenTimeout());
		//if (result)
		{
			if (tcc->sendBufferSize() > 0)
			{
				_bluetoothClient->setSendBufferSize(tcc->sendBufferSize());
			}
			if (tcc->receiveBufferSize() > 0)
			{
				_bluetoothClient->setReceiveBufferSize(tcc->receiveBufferSize());
			}
            
            if(!tcc->syncSendReceive())
            {
                _receiveClient = new BluetoothServerClient(manager(), _bluetoothClient, _channel, false);
            }
		}
		return true;
	}

	void BluetoothInteractive::close()
	{
		if (_autoDelete)
		{
            if(_receiveClient != NULL)
            {
                delete _receiveClient;
                _receiveClient = NULL;
            }
			if (_bluetoothClient != NULL)
			{
				_bluetoothClient->close();
				delete _bluetoothClient;
				_bluetoothClient = NULL;
			}
		}
	}

	bool BluetoothInteractive::connected()
	{
		return _bluetoothClient != NULL ? _bluetoothClient->connected() : false;
	}
	int BluetoothInteractive::available()
	{
		return _bluetoothClient != NULL ? _bluetoothClient->available() : 0;
	}

	int BluetoothInteractive::send(byte* buffer, int offset, int count)
	{
#ifdef DEBUG
		Stopwatch sw("socket send", 1000);
#endif
		int len = 0;
		if(connected())
		{
			len =  _bluetoothClient->write(buffer + offset, count);
		}
		return len;
	}

	int BluetoothInteractive::receive(byte* buffer, int offset, int count)
	{
#ifdef DEBUG
		Stopwatch sw("socket recv", 1000);
#endif
		int len = 0;
		if(connected())
		{
			len =  _bluetoothClient->read(buffer + offset, count);
		}
		return len;
	}
}
