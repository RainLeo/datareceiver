#include "DtuService.h"
#include "common/diag/Trace.h"
#include "common/IO/MemoryStream.h"
#include "DtuCaiMaoService.h"
#include "DtuHongDianService.h"
#include "DtuLiChuangService.h"
#include "DtuInteractive.h"

DtuService::DtuService() :
    m_bInitialized(false),
    m_pChannelContext(NULL),
    m_pInteractive(NULL)
{
}

DtuService::~DtuService()
{
    close();
}

DtuService * DtuService::getDtuService(string& vendor)
{
    if (String::stringEquals(vendor, "caimao", true))
    {
        return DtuCaiMaoService::Instance();
    }
    else if (String::stringEquals(vendor, "hongdian", true))
    {
        return DtuHongDianService::Instance();
    }
    else if (String::stringEquals(vendor, "lichuang", true))
    {
        return DtuLiChuangService::Instance();
        //return NULL;
    }

    return NULL;
}

bool DtuService::initialize(int port, string& vendor)
{
    DtuService *pDtu = getDtuService(vendor);
    if (pDtu == NULL)
    {
        return false;
    }

    bool ret = pDtu->initialize(port);
    if (ret)
    {
        m_dtuServices.push_back(pDtu);
    }

    return ret;
}

bool DtuService::initialize(int port)
{
    m_bInitialized = true;

    return m_bInitialized;
}

bool DtuService::unInitialize()
{
    for (vector<DtuService *>::iterator it = m_dtuServices.begin(); it != m_dtuServices.end(); it++)
    {
        DtuService * pDtu = *it;
        if (pDtu == NULL) continue;
        pDtu->unInitialize();
    }

    return true;
}

bool DtuService::open(DtuChannelContext* pcc, DtuInteractive *pInteractive)
{
    if (pcc == NULL) return false;

    m_pChannelContext = pcc;
    m_pInteractive = pInteractive;

    close();

    m_mapDtuData[pcc->id()].clear(); // init DTU data list
    m_mapDtuDataPos[pcc->id()] = 0; // init DTU data position
    m_bConnected = true;

    return true;
}

int DtuService::available(const string &id)
{
    int len = 0;

    if (!receiveDtuData(id, 0)) return len;

    string& data = m_mapDtuData[id].front();
    return data.size() - m_mapDtuDataPos[id];
}

int DtuService::send(const string &id, byte* buffer, int offset, int count)
{
    // reset receiver buffer
    m_mapDtuData[id].clear();
    m_mapDtuDataPos[id] = 0;

    return 0;
}

int DtuService::receive(const string &id, byte* buffer, int offset, int count)
{
    return receive(id, buffer, offset, count, 0);
}

int DtuService::receive(const string &id, byte* buffer, int offset, int count, uint timeout)
{
    int len = 0;

    if (!receiveDtuData(id, timeout)) return len;

    string& data = m_mapDtuData[id].front();
    int pos = m_mapDtuDataPos[id];
    len = count < (int)data.size() - pos ? count : (int)data.size() - pos;
    memcpy(buffer + offset, data.c_str() + pos, len);
    pos += len;
    m_mapDtuDataPos[id] = pos;
    if (pos >= (int)data.size())
    {
        m_mapDtuData[id].pop_front();
        m_mapDtuDataPos[id] = 0;
    }

    return len;
}

bool DtuService::receiveDtuData(const string &id, uint timeout)
{
    Locker lock(&m_mutexReceive);

    if (!id.empty())
    {
        if (m_mapDtuData.find(id) == m_mapDtuData.end())
        {
            Trace::writeFormatLine("DTU: ID unmatched (%s)", id.c_str());
            return false;
        }
        list<string> &lstDtuData = m_mapDtuData[id];
        bool ret = lstDtuData.size() > 0;
        if (ret) return ret;
    }

    string newId;
    string data;
    while (receive(newId, timeout, data))
    {
        if (m_mapDtuData.find(newId) == m_mapDtuData.end())
        {
            Trace::writeFormatLine("DTU: ID unmatched (%s)", newId.c_str());
        }
        m_mapDtuData[newId].push_back(data);
    }

    if (id.empty()) return false;

    return m_mapDtuData[id].size() > 0;
}
