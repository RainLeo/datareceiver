#ifndef PARALLELINTERACTIVE_H
#define PARALLELINTERACTIVE_H

#include "common/common_global.h"
#include "Interactive.h"
#include "ParallelChannelContext.h"
#include "ChannelDescription.h"
#include "Channel.h"
#include "common/IO/ParallelPort.h"

namespace Driver
{
	class COMMON_EXPORT ParallelInteractive : public Interactive
	{
	public:
		ParallelInteractive(DriverManager* dm, Channel* channel);
		~ParallelInteractive(void);

		bool open() override;
		void close() override;

		bool connected() override;
		int available() override;

		int send(byte* buffer, int offset, int count) override;
		int receive(byte* buffer, int offset, int count) override;

	private:
		inline ParallelChannelContext* getChannelContext()  
		{
			return (ParallelChannelContext*)(_channel->description()->context());
		}

	private:
		ParallelPort* _port;	
	};
}

#endif //PARALLELINTERACTIVE_H
