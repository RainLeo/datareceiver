#ifndef PARALLELCHANNELCONTEXT_H
#define PARALLELCHANNELCONTEXT_H

#include "common/common_global.h"
#include "ChannelContext.h"

namespace Driver
{
	class COMMON_EXPORT ParallelChannelContext : public ChannelContext
	{
	public:
		ParallelChannelContext(const string& name);
		~ParallelChannelContext();

		void setPortName(const string& portName)
		{
			_portName = portName;
		}
		const string portName() const
		{
			return _portName;
		}

	private:
		string _portName;
	};
}

#endif //PARALLELCHANNELCONTEXT_H