#include "../../exception/Exception.h"
#include "../../diag/Stopwatch.h"
#include "ParallelInteractive.h"

using namespace Common;

namespace Driver
{
	ParallelInteractive::ParallelInteractive(DriverManager* dm, Channel* channel) : Interactive(dm, channel)
	{
		if(channel == NULL)
			throw ArgumentException("channel");
		if((ParallelChannelContext*)channel->description()->context() == NULL)
			throw ArgumentException("channel");

		_port = NULL;
	}
	ParallelInteractive::~ParallelInteractive(void)
	{
		close();
	}
	bool ParallelInteractive::open()
	{
		close();

		ParallelChannelContext* scc = getChannelContext();

#ifdef DEBUG
		string message = Convert::convertStr("open a parallel port, name = %s", scc->portName().c_str());
		Stopwatch sw(message);
#endif

		_port = new ParallelPort(scc->portName());
		return _port->open();
	}
	void ParallelInteractive::close()
	{
		if(_port != NULL && _port->isOpen())
		{
			_port->close();
			delete _port;
			_port = NULL;
		}
	}

	bool ParallelInteractive::connected()
	{
		return _port != NULL ? _port->isOpen() : false;
	}
	int ParallelInteractive::available()
	{
		return (int)_port->available();
	}

	int ParallelInteractive::send(byte* buffer, int offset, int count)
	{
#ifdef DEBUG
		Stopwatch sw("parallel send", 1000);
#endif
		int len = 0;
		if(connected())
		{
			len = (int)_port->write((const char*)buffer, count);
		}
		return len;
	}
	int ParallelInteractive::receive(byte* buffer, int offset, int count)
	{
#ifdef DEBUG
		Stopwatch sw("parallel receive", 1000);
#endif
		int len = 0;
		if(connected())
		{
			len = (int)_port->read((char*)(buffer+offset),count);
		}
		return len;
	}
}
