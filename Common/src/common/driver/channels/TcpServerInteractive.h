#ifndef TCPSERVERINTERACTIVE_H
#define TCPSERVERINTERACTIVE_H

#include "common/common_global.h"
#include "common/data/LoopArray.h"
#include "common/thread/Thread.h"
#include "common/thread/TickTimeout.h"
#include "common/thread/Locker.h"
#include "common/net/TcpClient.h"
#include "common/net/TcpServer.h"
#include "Interactive.h"
#include "Channel.h"
#include "TcpServerChannelContext.h"
#include "TcpBackgroudReceiver.h"

using namespace Common;

namespace Driver
{
	typedef void (*client_accpet_callback)(const string&, int);
	typedef void(*client_close_callback)(const string&, int);

	class COMMON_EXPORT TcpServerInteractive : public Interactive
	{
	public:
		TcpServerInteractive(DriverManager* dm, Channel* channel);
		~TcpServerInteractive(void);

		bool open() override;
		void close() override;

		bool connected() override;
		int available() override;

		int send(byte* buffer, int offset, int count) override;
		int receive(byte* buffer, int offset, int count) override;

		inline void setAcceptAction(client_accpet_callback acceptAction)
		{
			_acceptAction = acceptAction;
		}
		inline void setCloseAction(client_close_callback closeAction)
		{
			_closeAction = closeAction;
		}
        
        inline const string& address() const
        {
            static string empty = "";
            TcpServerChannelContext* tsc = getChannelContext();
            return tsc != NULL ? tsc->address() : empty;
        }
        inline int port() const
        {
            TcpServerChannelContext* tsc = getChannelContext();
            return tsc != NULL ? tsc->port() : 0;
        }
        
        inline void setSentAddress(const string& address)
        {
            _sentAddress = address;
        }
        inline string sentAddress() const
        {
            return _sentAddress;
        }

	private:
		friend void tcp_acceptProc(void* parameter);
		void acceptProcInner();

		friend void tcp_closeProc(void* parameter);
		void closeProcInner();

		inline TcpServerChannelContext* getChannelContext() const
		{
			return (TcpServerChannelContext*)(_channel->description()->context());
		}

		bool rebind();

	private:
		TcpServer* _tcpServer;
		mutex _tcpServerMutex;

		mutex _mutexClients;
		TcpBackgroudReceivers _clients;

		Thread* _acceptThread;
		Thread* _closeThread;

		client_accpet_callback _acceptAction;
		client_close_callback _closeAction;
        
        string _sentAddress;
	};
}

#endif // TCPSERVERINTERACTIVE_H
