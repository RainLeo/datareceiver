#ifndef SERIALSERVERINTERACTIVE_H
#define SERIALSERVERINTERACTIVE_H

#include "common/common_global.h"
#include "Interactive.h"
#include "Channel.h"
#include "UdpServerChannelContext.h"
#include "../../data/LoopArray.h"
#include "../../thread/Thread.h"
#include "../../thread/TickTimeout.h"
#include "../../thread/Locker.h"
#include "common/IO/SerialPort.h"

using namespace Common;

namespace Driver
{
	class Device;
	class COMMON_EXPORT SerialServerInteractive : public Interactive, public BackgroudReceiver
	{
	public:
		SerialServerInteractive(DriverManager* dm, Channel* channel);
		~SerialServerInteractive(void);

		bool open() override;
		void close() override;

		bool connected() override;
		int available() override;

        int send(byte* buffer, int offset, int count) override;
		int receive(byte* buffer, int offset, int count) override;
        int receive(byte* buffer, int offset, int count, uint timeout) override;

	private:
		friend void serial_receiveProc(void* parameter);
		void receiveProcInner();
        
        bool connectdInner() const;

		inline SerialChannelContext* getChannelContext()
		{
			return (SerialChannelContext*)(_channel->description()->context());
		}

	private:
		SerialPort*	_port;

		Thread* _receiveThread;

		Device* _device;
	};
}

#endif // SERIALSERVERINTERACTIVE_H
