#pragma once
#include "common/system/Singleton.h"
#include "DtuChannelContext.h"
#include <vector>
#include <list>
#include <map>

using namespace Common;
using namespace Driver;
using namespace std;
namespace Driver
{
    class DtuInteractive;
}

class DtuService
{
public:
    DtuService();
    ~DtuService();
    static DtuService * Instance() { return Singleton<DtuService>::instance(); }
    DtuService * getDtuService(string& vendor);

    bool initialize(int port, string& vendor);

    virtual bool initialize(int port);
    virtual bool unInitialize();

    virtual bool open(DtuChannelContext* pcc, DtuInteractive * pInteractive);
    virtual void close() { m_bConnected = false; }

    virtual bool connected() { return m_bConnected; }
    virtual int available(const string &id);

    virtual int send(const string &id, byte* buffer, int offset, int count);
    virtual int receive(const string &id, byte* buffer, int offset, int count);
    virtual int receive(const string &id, byte* buffer, int offset, int count, uint timeout);
    virtual bool receiveDtuData(const string &id, uint timeout);
    virtual bool receive(string &id, uint timeout, string &data) { return false; }

protected:
    DtuChannelContext* m_pChannelContext;
    bool m_bInitialized;
    vector<DtuService *> m_dtuServices;
    typedef map<string, list<string> > MapDtuData;
    MapDtuData m_mapDtuData;
    typedef map<string, int> MapDtuDataPos;
    MapDtuDataPos m_mapDtuDataPos;
    bool m_bConnected;
    DtuInteractive *m_pInteractive;
    mutex m_mutexReceive;
};

