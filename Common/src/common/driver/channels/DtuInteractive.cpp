#include "../../exception/Exception.h"
#include "../../diag/Stopwatch.h"
#include "DtuInteractive.h"
#include "../../diag/Trace.h"
#include "DtuCaiMaoService.h"
#include "DtuHongDianService.h"
#include "DtuLiChuangService.h"

using namespace Common;

namespace Driver
{
	DtuInteractive::DtuInteractive(DriverManager* dm, Channel* channel) : Interactive(dm, channel)
	{
		if(channel == NULL)
			throw ArgumentException("channel");
		if((DtuChannelContext*)channel->description()->context() == NULL)
			throw ArgumentException("channel");
	}

	DtuInteractive::~DtuInteractive(void)
	{
		close();
	}

    string string_empty = "";
    const string & DtuInteractive::getDtuId()
    {
        DtuChannelContext *cc = getChannelContext();
        if (cc == NULL) return string_empty;

        return cc->id();
    }
    DtuService * DtuInteractive::getDtuService()
    {
        DtuChannelContext *cc = getChannelContext();
        if (cc == NULL) return NULL;

        if (String::stringEquals(cc->vendor(), "caimao", true))
        {
            return DtuCaiMaoService::Instance();
        }
        else if (String::stringEquals(cc->vendor(), "hongdian", true))
        {
            return DtuHongDianService::Instance();
        }
        else if (String::stringEquals(cc->vendor(), "lichuang", true))
        {
            return DtuLiChuangService::Instance();
        }

        return NULL;
    }

	bool DtuInteractive::open()
	{
		close();

		DtuChannelContext* scc = getChannelContext();
        DtuService *pDtu = getDtuService();
        if (pDtu == NULL)
        {
            return false;
        }

        return pDtu->open(scc, this);
	}
	void DtuInteractive::close()
	{
        DtuService *pDtu = getDtuService();
        if (pDtu == NULL)
        {
            return;
        }
        pDtu->close();
	}

	bool DtuInteractive::connected()
	{
        DtuService *pDtu = getDtuService();
        if (pDtu == NULL)
        {
            return false;
        }

        return pDtu->connected();
    }

	int DtuInteractive::available()
	{
        int len = 0;

        DtuService *pDtu = getDtuService();
        if (pDtu == NULL)
        {
            return len;
        }

        return pDtu->available(getDtuId());
    }

	int DtuInteractive::send(byte* buffer, int offset, int count)
	{
        DtuService *pDtu = getDtuService();
        if (pDtu == NULL)
        {
            return 0;
        }

        return pDtu->send(getDtuId(), buffer, offset, count);
	}

	int DtuInteractive::receive(byte* buffer, int offset, int count)
	{
        return receive(buffer, offset, count, 0);
	}

    int DtuInteractive::receive(byte* buffer, int offset, int count, uint timeout)
    {
        int len = 0;

        DtuService *pDtu = getDtuService();
        if (pDtu == NULL)
        {
            return len;
        }

        return pDtu->receive(getDtuId(), buffer, offset, count, timeout);
    }
}
