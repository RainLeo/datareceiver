#include "../../exception/Exception.h"
#include "../../diag/Stopwatch.h"
#include "../../thread/Thread.h"
#include "Channel.h"
#include "TcpChannelContext.h"
#include "TcpInteractive.h"
#include "ChannelDescription.h"

using namespace Common;

namespace Driver
{
	TcpInteractive::TcpInteractive(DriverManager* dm, Channel* channel) : Interactive(dm, channel)
	{
		if(channel == NULL)
			throw ArgumentException("channel");
		if((TcpChannelContext*)channel->description()->context() == NULL)
			throw ArgumentException("channel");

		_tcpClient = NULL;
		_autoDelete = true;
        _receiveClient = NULL;
	}

	TcpInteractive::~TcpInteractive(void)
	{
		close();
	}

	bool TcpInteractive::open()
	{
		close();

		TcpChannelContext* tcc = getChannelContext();

//#ifdef DEBUG
//		string message = Convert::convertStr("connect a host, address = %s, port = %d", tcc->address().c_str(), tcc->port());
//		Stopwatch sw(message, 100);
//#endif

		_tcpClient = new TcpClient();
		bool result = _tcpClient->connectToHost(tcc->address().c_str(), tcc->port(), tcc->getOpenTimeout());
		if (result)
		{
			if (tcc->sendBufferSize() > 0)
			{
				_tcpClient->setSendBufferSize(tcc->sendBufferSize());
			}
			if (tcc->receiveBufferSize() > 0)
			{
				_tcpClient->setReceiveBufferSize(tcc->receiveBufferSize());
			}
			if (tcc->isNoDelay())
			{
				_tcpClient->setNoDelay(true);
			}
            
            if(!tcc->syncSendReceive())
            {
                _receiveClient = new TcpBackgroudReceiver(manager(), _channel, _tcpClient, false);
            }
		}
		return result;
	}

	void TcpInteractive::close()
	{
		if (_autoDelete)
		{
            if(_receiveClient != NULL)
            {
                delete _receiveClient;
                _receiveClient = NULL;
            }
			if (_tcpClient != NULL)
			{
				_tcpClient->close();
				delete _tcpClient;
				_tcpClient = NULL;
			}
		}
	}

    bool TcpInteractive::connectdInner() const
    {
        return _tcpClient != NULL ? _tcpClient->connected() : false;
    }
	bool TcpInteractive::connected()
	{
		return connectdInner();
	}
	int TcpInteractive::available()
	{
		return _tcpClient != NULL ? _tcpClient->available() : 0;
	}

	int TcpInteractive::send(byte* buffer, int offset, int count)
	{
#ifdef DEBUG
		Stopwatch sw("socket send", 1000);
#endif
		int len = 0;
		if(connected())
		{
			len =  _tcpClient->write(buffer + offset, count);
		}
		return len;
	}

	int TcpInteractive::receive(byte* buffer, int offset, int count)
	{
#ifdef DEBUG
		Stopwatch sw("socket recv", 1000);
#endif
		int len = 0;
		if(connected())
		{
			len =  _tcpClient->read(buffer + offset, count);
		}
		return len;
	}
}
