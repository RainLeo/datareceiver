#ifndef UDPCHANNELCONTEXT_H
#define UDPCHANNELCONTEXT_H

#include <stdio.h>
#include "common/common_global.h"
#include "common/system/Convert.h"
#include "ChannelContext.h"
#include "EthernetContext.h"

using namespace Common;

namespace Driver
{
	class UdpChannelContext : public ChannelContext, public EthernetContext
	{
	public:
		UdpChannelContext() : UdpChannelContext("", 0)
		{
		}
		UdpChannelContext(int port) : UdpChannelContext("", port)
		{
		}
		UdpChannelContext(const string& address, int port) : EthernetContext(address, port)
		{
		}
		~UdpChannelContext()
        {
        }
	};
}

#endif // UDPCHANNELCONTEXT_H
