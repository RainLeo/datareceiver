#ifndef TCPINTERACTIVE_H
#define TCPINTERACTIVE_H

#include "common/common_global.h"
#include "Interactive.h"
#include "TcpChannelContext.h"
#include "ChannelDescription.h"
#include "Channel.h"
#include "common/net/TcpClient.h"
#include "TcpBackgroudReceiver.h"

namespace Driver
{
	class COMMON_EXPORT TcpInteractive : public Interactive, public EthernetAddress
	{
	public:
		TcpInteractive(DriverManager* dm, Channel* channel);
		~TcpInteractive(void);

		bool open() override;
		void close() override;

		bool connected() override;
		int available() override;

		int send(byte* buffer, int offset, int count) override;
		int receive(byte* buffer, int offset, int count) override;
        
		inline const string& address() const
		{
            static string empty = "";
			return connectdInner() ? _tcpClient->address() : empty;
		}
		inline int port() const
		{
			return connectdInner() ? _tcpClient->port() : 0;
		}
		inline const string& peerAddr() const override
        {
            static string empty = "";
            return connectdInner() ? _tcpClient->peerAddr() : empty;
        }
        inline int peerPort() const override
        {
            return connectdInner() ? _tcpClient->peerPort() : 0;
        }

	private:
        bool connectdInner() const;
		inline TcpChannelContext* getChannelContext() const
		{
			return (TcpChannelContext*)(_channel->description()->context());
		}

		friend class TcpBackgroudReceiver;

		void updateTcpClient(TcpClient* tcpClient)
		{
			_tcpClient = tcpClient;
			_autoDelete = false;
		}

	private:
		TcpClient* _tcpClient;
		bool _autoDelete;
        
        TcpBackgroudReceiver* _receiveClient;
	};
}
#endif // TCPINTERACTIVE_H
