#include <assert.h>
#include "../../thread/Locker.h"
#include "../../thread/TickTimeout.h"
#include "../../diag/Stopwatch.h"
#include "../../diag/Debug.h"
#include "common/Resources.h"
#include "../DriverManager.h"
#include "../devices/DeviceDescription.h"
#include "ChannelDescription.h"
#include "BluetoothInteractive.h"
#include "BluetoothServerClient.h"

namespace Driver
{
	void bluetooth_receiveProc(void* parameter)
	{
		BluetoothServerClient* ti = (BluetoothServerClient*)parameter;
		assert(ti);
		ti->receiveProcInner();
	}

	BluetoothServerClient::BluetoothServerClient(DriverManager* dm, BluetoothClient* client, Channel* channel, bool autoDelete)
	{
		if (dm == NULL)
			throw ArgumentNullException("dm");
		if (client == NULL)
			throw ArgumentNullException("client");
		if (channel == NULL)
			throw ArgumentNullException("channel");

		_client = client;
        _autoDelete = autoDelete;
		_manager = dm;

		_context = dynamic_cast<BluetoothChannelBaseContext*>(channel->description()->context());
		assert(_context);

        if(autoDelete)
        {
            createDevice(channel);
        }
        else
        {
            updateDevice(channel);
        }

		_receiveThread = new Thread();
		_receiveThread->setName("bluetooth_receiveProc");
		_receiveThread->startProc(bluetooth_receiveProc, this, 1);

		_startTime = TickTimeout::GetCurrentTickCount();
	}
	BluetoothServerClient::~BluetoothServerClient()
	{
		if (_receiveThread != NULL)
		{
			_receiveThread->stop();
			delete _receiveThread;
			_receiveThread = NULL;
		}

		if (_channel != NULL)
		{
            if(_autoDelete)
                delete _channel;
			_channel = NULL;
		}
		if (_device != NULL)
		{
            if(_autoDelete)
                delete _device;
			_device = NULL;
		}
        if (_dd != NULL)
        {
            if(_autoDelete)
                delete _dd;
            _dd = NULL;
        }

        if(_client != NULL)
        {
            if(_autoDelete)
                delete _client;
            _client = NULL;
        }
	}

	int BluetoothServerClient::available()
	{
		int result = _client != NULL ? _client->available() : 0;
		if (result > 0)
		{
			_startTime = TickTimeout::GetCurrentTickCount();
		}
		return result;
	}
	bool BluetoothServerClient::connected() const
	{
		TimeSpan clientTimeout = _context->closeTimout();
		uint deadTime = TickTimeout::GetDeadTickCount(_startTime, (uint)clientTimeout.totalMilliseconds());
		if (TickTimeout::IsTimeout(_startTime, deadTime))
			return false;

		return _client != NULL ? _client->connected() : false;
	}

	void BluetoothServerClient::receiveProcInner()
	{
#ifdef DEBUG
		Stopwatch sw("receiveProc", 1000);
#endif

		if (connected() && available() > 0)
		{
            receiveFromBuffer(_device);
		}
	}

	void BluetoothServerClient::createDevice(const Channel* channel)
	{
		DriverManager* dm = manager();
		assert(dm);
		Device* device = dm->getDevice(channel);
		assert(device);

		string channelName = "BluetoothClient";
		ChannelDescription* cd = new ChannelDescription(channelName, "BluetoothInteractive");
        cd->context()->setReopened(false);
		BluetoothChannelBaseContext* tc = dynamic_cast<BluetoothChannelBaseContext*>(cd->context());
        tc->copyFrom(_context);
		_channel = new Channel(dm, cd);
		BluetoothInteractive* ti = dynamic_cast<BluetoothInteractive*>(_channel->interactive());
		assert(ti);
		ti->updateBluetoothClient(_client);

		string deviceName = "BluetoothClientDevice";
        DeviceDescription* dd = new DeviceDescription(deviceName, cd,
                                                      device->instructionSet()->clone(),
                                                      device->description()->context()->clone());
        _dd = dd;
		_device = new Device(dd, _channel);
	}
    void BluetoothServerClient::updateDevice(Channel* channel)
    {
        DriverManager* dm = manager();
        assert(dm);
        Device* device = dm->getDevice(channel);
        assert(device);
        
        _channel = channel;
        _device = device;
    }
    
    int BluetoothServerClient::send(byte* buffer, int offset, int count)
    {
#ifdef DEBUG
        Stopwatch sw("socket send", 1000);
#endif
        int len = 0;
        if(connected())
        {
            len =  _client->write(buffer + offset, count);
        }
        return len;
    }
}
