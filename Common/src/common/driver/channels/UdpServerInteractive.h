#ifndef UDPSERVERINTERACTIVE_H
#define UDPSERVERINTERACTIVE_H

#include "common/common_global.h"
#include "Interactive.h"
#include "Channel.h"
#include "UdpServerChannelContext.h"
#include "common/net/UdpClient.h"
#include "common/net/UdpServer.h"
#include "../../data/LoopArray.h"
#include "../../thread/Thread.h"
#include "../../thread/TickTimeout.h"
#include "../../thread/Locker.h"

using namespace Common;

namespace Driver
{
	class Device;
	class COMMON_EXPORT UdpServerInteractive : public Interactive, public EthernetAddress, public BackgroudReceiver
	{
	public:
		UdpServerInteractive(DriverManager* dm, Channel* channel);
		~UdpServerInteractive(void);

		bool open() override;
		void close() override;

		bool connected() override;
		int available() override;

        int send(byte* buffer, int offset, int count) override;
		int receive(byte* buffer, int offset, int count) override;
        int receive(byte* buffer, int offset, int count, uint timeout) override;
        
        inline const string& peerAddr() const override
        {
            static string empty = "";
            return connectdInner() ? _udpServer->peerAddr() : empty;
        }
        inline int peerPort() const override
        {
            return connectdInner() ? _udpServer->peerPort() : 0;
        }

	private:
		friend void udp_receiveProc(void* parameter);
		void receiveProcInner();
        
        bool connectdInner() const;

		inline UdpServerChannelContext* getChannelContext()
		{
			return (UdpServerChannelContext*)(_channel->description()->context());
		}

		bool rebind();

	private:
		UdpServer* _udpServer;

		Thread* _receiveThread;

		Device* _device;
	};
}

#endif // UDPSERVERINTERACTIVE_H
