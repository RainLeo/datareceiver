#ifndef SERIALCHANNELCONTEXT_H
#define SERIALCHANNELCONTEXT_H

#include "common/common_global.h"
#include "ChannelContext.h"
#include "common/IO/SerialInfo.h"

using namespace Common;

namespace Driver
{
	class COMMON_EXPORT SerialChannelContext : public ChannelContext
	{
	public:
		SerialChannelContext(const string& portName = "");
		~SerialChannelContext();

        void setPortName(const string& portName);
        const string& portName() const;
		void setBaudRate(int);
		void setBaudRate(const string&);
		int baudRate() const;
		void setDataBits(SerialInfo::DataBitsType);
		void setDataBits(const string&);
		SerialInfo::DataBitsType dataBits() const;
		void setParity(SerialInfo::ParityType);
		void setParity(const string&);
		SerialInfo::ParityType parity() const;
		void setStopBits(SerialInfo::StopBitsType);
		void setStopBits(const string&);
		SerialInfo::StopBitsType stopBits() const;
		void setHandshake(SerialInfo::HandshakeType);
		void setHandshake(const string&);
		SerialInfo::HandshakeType handshake() const;
        bool rtsEnable() const;
        void setRtsEnable(bool enabled);
        bool dtrEnable() const;
        void setDtrEnable(bool enabled);
        bool useSignal() const;
        void setUseSignal(bool useSignal);
        
        const SerialInfo& serial() const;
        void setSerial(const SerialInfo& serial);

	private:
        SerialInfo _serial;
	};
}

#endif //SERIALCHANNELCONTEXT_H
