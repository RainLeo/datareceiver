#ifndef BLUETOOTHCHANNELCONTEXT_H
#define BLUETOOTHCHANNELCONTEXT_H

#include <stdio.h>
#include "common/common_global.h"
#include "ChannelContext.h"
#include "../../system/Convert.h"
#include "../../system/TimeSpan.h"

using namespace Common;

namespace Driver
{
	class BluetoothChannelBaseContext : public ChannelContext
	{
	public:
		BluetoothChannelBaseContext()
		{
			_receiveBufferSize = 0;
			_sendBufferSize = 0;
            _closeTimeout = TimeSpan(0, 1, 0);
            _syncSendReceive = true;
		}
		~BluetoothChannelBaseContext()
        {
        }

		inline int sendBufferSize() const
		{
			return _sendBufferSize;
		}
		inline void setSendBufferSize(int bufferSize)
		{
			_sendBufferSize = bufferSize;
		}
		inline int receiveBufferSize() const
		{
			return _receiveBufferSize;
		}
		inline void setReceiveBufferSize(int bufferSize)
		{
			_receiveBufferSize = bufferSize;
		}
        
        inline const TimeSpan& closeTimout() const
        {
            return _closeTimeout;
        }
        inline void setCloseTimeout(const TimeSpan& timeout)
        {
            _closeTimeout = timeout;
        }
		inline void setCloseTimeout(uint milliSeconds)
        {
            _closeTimeout = TimeSpan::fromMilliseconds((double)milliSeconds);
        }
        
        virtual void copyFrom(BluetoothChannelBaseContext* context)
        {
            _receiveBufferSize = context->_receiveBufferSize;
            _sendBufferSize = context->_sendBufferSize;
            
            _closeTimeout = context->_closeTimeout;
            _syncSendReceive = context->_syncSendReceive;
        }
        
        inline bool syncSendReceive() const
        {
            return _syncSendReceive;
        }
        inline void setSyncSendReceive(bool sync = true)
        {
            _syncSendReceive = sync;
        }

	protected:
        int _receiveBufferSize;
		int _sendBufferSize;

   		TimeSpan _closeTimeout;
        bool _syncSendReceive;
	};
    
    class BluetoothChannelContext : public BluetoothChannelBaseContext
    {
    public:
        BluetoothChannelContext() : BluetoothChannelBaseContext()
        {
            _openTimeout = 3000;
        }
        ~BluetoothChannelContext()
        {
        }
        
        inline uint getOpenTimeout() const
        {
            return _openTimeout;
        }
        inline void setOpenTimeout(uint timeout)
        {
            _openTimeout = timeout;
        }
        inline void setOpenTimeout(TimeSpan timeout)
        {
            _openTimeout = (uint)timeout.totalMilliseconds();
        }
        
        void copyFrom(BluetoothChannelBaseContext* context)
        {
            BluetoothChannelBaseContext::copyFrom(context);
            
            BluetoothChannelContext* tc = dynamic_cast<BluetoothChannelContext*>(context);
            if(tc != NULL)
            {
                _openTimeout = tc->_openTimeout;
            }
        }
        
    private:
        uint _openTimeout;
    };
}

#endif // BLUETOOTHCHANNELCONTEXT_H
