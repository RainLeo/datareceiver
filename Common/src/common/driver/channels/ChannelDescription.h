#ifndef CHANNELDESCRIPTION_H
#define CHANNELDESCRIPTION_H

#include "common/common_global.h"
#include "../../exception/Exception.h"
#include "../IContextProperty.h"
#include "TcpChannelContext.h"
#include "TcpServerChannelContext.h"
#include "SerialChannelContext.h"
#include "ParallelChannelContext.h"
#include "UdpServerChannelContext.h"
#include "UdpChannelContext.h"
#include "BluetoothServerChannelContext.h"
#include "DtuChannelContext.h"

using namespace Common;

namespace Driver
{
    class Interactive;
    class COMMON_EXPORT ChannelDescription : public IContextProperty
    {
    public:
        ChannelDescription(const string& name, const string& interactiveName)
        : _name(name), _interactiveName(interactiveName), _interactive(NULL)
        {
        	_name = name;
       	 	_interactiveName = interactiveName;
       	 	_context = NULL;
       	 	_interactive = NULL;
       	 	_isServerChannel = false;
            _enabled = true;
            
            if(_interactiveName == "TcpInteractive")
            {
                _context = new TcpChannelContext();
            }
            else if(_interactiveName == "SerialInteractive")
            {
                _context = new SerialChannelContext(name);
            }
            else if (_interactiveName == "SerialServerInteractive")
            {
                _context = new SerialChannelContext(name);
            }
            else if(_interactiveName == "ParallelInteractive")
            {
                _context = new ParallelChannelContext(name);
            }
            else if(_interactiveName == "TcpServerInteractive")
            {
                _context = new TcpServerChannelContext();
                _isServerChannel = true;
            }
            else if(_interactiveName == "UdpInteractive")
            {
                _context = new UdpChannelContext();
            }
            else if(_interactiveName == "UdpServerInteractive")
            {
                _context = new UdpServerChannelContext();
                _isServerChannel = true;
            }
            else if (_interactiveName == "BluetoothInteractive")
            {
                _context = new BluetoothChannelContext();
            }
            else if (_interactiveName == "BluetoothServerInteractive")
            {
                _context = new BluetoothServerChannelContext();
                _isServerChannel = true;
            }
            else if (_interactiveName == "DtuInteractive")
            {
                _context = new DtuChannelContext();
            }
            else
            {
                throw NotImplementedException("Can not create a channel context.");
            }
        }
        ChannelDescription(const string& name, ChannelContext* context, Interactive* interactive, bool isServerChannel = false)
        {
        	 _name = name;
        	 _interactiveName = "";
        	 _context = context;
        	 _interactive = interactive;
        	 _isServerChannel = isServerChannel;
            _enabled = true;
        }
        
        virtual ~ChannelDescription()
        {
            delete _context;
            _context = NULL;
            _interactive = NULL;    // deleted by Channel
        }
        
        inline void setName(const string& name)
        {
            _name = name;
        }
        
        inline const string& name() const
        {
            return _name;
        }
        
        inline ChannelContext* context() const
        {
            return _context;
        }
        inline const string& interactiveName() const
        {
            return _interactiveName;
        }
        
        inline bool isServerChannel() const
        {
            return _isServerChannel;
        }
        
        inline bool enabled() const
        {
            return _enabled;
        }
        inline void setEnabled(bool enabled)
        {
            _enabled = enabled;
        }
        
        Interactive* interactive() const
        {
            return _interactive;
        }
        
    private:
        string _name;
        string _interactiveName;
        ChannelContext* _context;
        bool _isServerChannel;
        bool _enabled;
        Interactive* _interactive;
    };
}
#endif // CHANNELDESCRIPTION_H
