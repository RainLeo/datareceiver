#ifndef DTUCHANNELCONTEXT_H
#define DTUCHANNELCONTEXT_H

#include "common/common_global.h"
#include "ChannelContext.h"
#include "common/IO/SerialInfo.h"

using namespace Common;

namespace Driver
{
	class COMMON_EXPORT DtuChannelContext : public ChannelContext
	{
	public:
		DtuChannelContext(const string& id = "", const string& vendor = "");
		~DtuChannelContext();

        void setId(const string& id);
        const string& id() const;
        void setVendor(const string& vendor);
        const string& vendor() const;

	private:
        string _id;
        string _vendor;
	};
}

#endif //DtuChannelContext_H
