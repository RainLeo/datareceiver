#include "../../exception/Exception.h"
#include "../../diag/Stopwatch.h"
#include "DtuHongDianService.h"
#include "../../diag/Trace.h"
#ifdef WIN32
#include "Hongdian_dtu.h"
#endif
#include "DtuInteractive.h"
#include "common/thread/Thread.h"

using namespace Common;

namespace Driver
{
    DtuHongDianService::DtuHongDianService()
	{
	}

	DtuHongDianService::~DtuHongDianService(void)
	{
	}

    const string & DtuHongDianService::getDtuId()
    {
        if (m_pInteractive == NULL) return string_empty;
        return m_pInteractive->getDtuId();
    }

    Thread *m_pThread = NULL;
    void staticReceiveProc(void* parameter)
    {
        DtuHongDianService* pThis = (DtuHongDianService*)parameter;
        while (true)
        {
            pThis->receiveDtuData(string_empty, 0);
            Thread::msleep(3000);
        }
    }

    bool DtuHongDianService::initialize(int port)
    {
        if (m_bInitialized) return true;

#ifdef WIN32
        HMODULE module = LoadLibrary("wcomm_dll.dll");
        start_net_service = (Tstart_net_service)GetProcAddress(module, "start_net_service");
        SetWorkMode = (TSetWorkMode)GetProcAddress(module, "SetWorkMode");
        do_send_user_data = (Tdo_send_user_data)GetProcAddress(module, "do_send_user_data");
        do_read_proc = (Tdo_read_proc)GetProcAddress(module, "do_read_proc");

        int i = SetWorkMode(1);
        i = start_net_service(NULL, 0, port, NULL);
        if (i)
        {
            Trace::writeFormatLine("Error! HongDian DTU service started failed at port %u. Error code: %d", port, i);
        }
        else
        {
            Trace::writeFormatLine("HongDian DTU service started successfully at port %u.", port);
            m_bInitialized = true;

            if (m_pThread != NULL)
            {
                if (m_pThread->isAlive())
                {
                    return true;
                }
                delete m_pThread;
                m_pThread = NULL;
            }
            m_pThread = new Thread();
            m_pThread->setName("HongDianReceiveThread");
            m_pThread->start(staticReceiveProc, this);

            return true;
        }
#endif
        return false;
    }

    bool DtuHongDianService::unInitialize()
    {
        return true;
    }

    int DtuHongDianService::send(const string &id, byte* buffer, int offset, int count)
	{
        DtuService::send(id, buffer, offset, count);

        int len = 0;
		if(connected())
		{
#ifdef WIN32
            int i = do_send_user_data((uint8*)id.c_str(), (uint8*)buffer, count, NULL);
            if (i)
            {
                Trace::writeFormatLine("DTU: Failed to send data to HongDian dtu, id %s. Error code: %d", id.c_str(), i);
                return 0;
            }
            Trace::writeFormatLine("DTU: Sending data to HongDian dtu %s", id.c_str());

            len = count;
#endif
        }

		return len;
	}

    bool DtuHongDianService::receive(string &id, uint timeout, string &data)
    {
        bool ret = false;

#ifdef WIN32
        data_record dr;
        int i = do_read_proc(&dr, NULL, FALSE);
        if (i)
        {
            return ret; // No data received
        }
        switch (dr.m_data_type)    {
        case 0x01: //DTU注册包、心跳包 
            //界面刷新DTU信息 
            break;
        case 0x04: //DTU上一次收到DSC的错误包，目前对于用户来说，收到这个
            //只是意味着参数配置出错 
            //请调试参数配置 
            break;
        case 0x06: //DTU收到do_disconnect_ppp_link的请求 
            break;
        case 0x07: //DTU收到do_stop_send_data的请求 
            break;
        case 0x08: //DTU收到do_start_send_data的请求 
            break;
        case 0x09: //DTU数据包 
        {
            //处理数据 
            const char* userid = dr.m_userid;
            Trace::writeFormatLine("DTU: Data received from HongDian DTU %s, length:%u", userid, dr.m_data_len);
            id = userid;
            data = string(dr.m_data_buf, dr.m_data_len);
            ret = true;
            break;
        }
        case 0x0b: //DTU返回参数查询结果 
            //调用GetParam读取参数 
            break;
        case 0x0d: //DTU参数配置成功 
            break;
        default:
            break;
        }
#endif
        return ret;
    }
}
