#ifndef CHANNEL_H
#define CHANNEL_H

#include "common/common_global.h"
#include "../../data/ByteArray.h"
#include "../../system/Convert.h"
#include "../../exception/Exception.h"
#include "../IContextProperty.h"
#include "../EscapeOption.h"
#include "Interactive.h"
#include "ChannelDescription.h"

namespace Driver
{
	class Interactive;
    class DriverManager;
    class MockInteractive;
	class COMMON_EXPORT Channel
	{
	public:
		Channel(DriverManager* dm, ChannelDescription* description);
		~Channel();

		int send(byte* buffer, int offset, int count, const EscapeOption* escape = NULL);
        int send(const ByteArray& buffer, const EscapeOption* escape = NULL);

		int receiveByEndBytes(byte* buffer, int bufferLength, const byte* endBuffer, int ebLength, int suffix, uint timeout);
		int receiveByEndBytes(byte* buffer, int bufferLength, const byte* startBuffer, int sbLength, const byte* endBuffer, int ebLength, int suffix, uint timeout);

		int receiveBySize(byte* buffer, int bufferLength, int offset, int count, uint timeout, const EscapeOption* escape = NULL);
		int receiveBySize(byte* buffer, int bufferLength, int size, uint timeout);
        
        int receiveBySize(ByteArray* buffer, int count, uint timeout, const EscapeOption* escape = nullptr);

		bool receiveByte(byte& data, uint timeout);

		inline bool connected() const
		{
			return interactive() != NULL ? interactive()->connected() : false;
		}

		inline const string& name() const
		{
			static string Empty = "";
			return _description != NULL ? _description->name() : Empty;
		}
		inline ChannelDescription* description() const
		{
			return _description;
		}
		inline ChannelContext* context() const
		{
			return _description != NULL ? _description->context() : NULL;
		}

        Interactive* interactive() const;
        
        inline bool isServerChannel() const
        {
            return _description->isServerChannel();
        }
        inline bool enabled() const
        {
            return _description->enabled();
        }

		void getEscapeBuffer(const byte* buffer, int offset, int count, ByteArray& dst, const EscapeOption* escape);
			
		bool open();
		void close();

		void clearReceiveBuffer();
        
        void updateMockRecvBuffer(const ByteArray& buffer);

	private:
		bool receiveStartBytes(const byte* startBuffer, int sbLength, uint timeout);
		bool receiveStartBytes(byte* buffer, int bufferLength, const byte* startBuffer, int sbLength, uint timeout);
		int receiveBySizeWithEscape(byte* buffer, int bufferLength, int offset, int count, uint timeout, const EscapeOption* escape = NULL);
		int receiveBySizeWithoutEscape(byte* buffer, int bufferLength, int offset, int count, uint timeout);
        
        int receiveBySizeWithoutEscape(ByteArray* buffer, int count, uint timeout);
        int receiveDirectly(ByteArray* buffer, int count);
        
        DriverManager* manager()
        {
            return _manager;
        }

	private:
		ChannelDescription* _description;
		Interactive* _interactive;
        
        DriverManager* _manager;
        
        MockInteractive* _mock;
        
        static const int ReceiveBufferLength = 64 * 1024;
	};
    typedef Vector<Channel> Channels;
}

#endif // CHANNEL_H
