#pragma once
#ifndef DTUHONGDIANSERVICE_H
#define DTUHONGDIANSERVICE_H

#include "DtuService.h"
#include "common/common_global.h"
#include "common/system/Singleton.h"

using namespace std;
using namespace Common;

namespace Driver
{
	class COMMON_EXPORT DtuHongDianService : public DtuService
	{
	public:
        DtuHongDianService();
        ~DtuHongDianService(void);
        static DtuHongDianService * Instance() { return Singleton<DtuHongDianService>::instance(); }

        bool initialize(int port) override;
        bool unInitialize() override;

        int send(const string &id, byte* buffer, int offset, int count) override;
        bool receive(string &id, uint timeout, string &data) override;

        const string &getDtuId();

    private:
	};
}

#endif //DTUHONGDIANSERVICE_H
