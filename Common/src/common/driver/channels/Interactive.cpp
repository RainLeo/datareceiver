#include "common/diag/Debug.h"
#include "common/data/ByteArray.h"
#include "Interactive.h"
#include "../devices/Device.h"

namespace Driver
{
    Interactive::Interactive(DriverManager* dm, Channel* channel)
    {
        if(dm == NULL)
            throw ArgumentNullException("dm");
        
        _manager = dm;
        _channel = channel;
        _useReceiveTimeout = false;
    }
    Interactive::~Interactive()
    {
        _channel = NULL;
    }
    int Interactive::receive(byte* buffer, int offset, int count, uint timeout)
    {
        if(timeout == 0)
        {
            return receive(buffer, offset, count);
        }
        // use it if useReceiveTimeout() return true;
        throw NotImplementedException("Can not implement this method.");
    }
    int Interactive::receive(ByteArray* buffer, int count, uint timeout)
    {
        if(timeout == 0)
        {
            byte* temp = new byte[count];
            int readCount = receive(temp, 0, count);
            if(readCount > 0)
            {
                buffer->addRange(temp, readCount);
            }
            delete[] temp;
            return readCount;
        }
        // use it if useReceiveTimeout() return true;
        throw NotImplementedException("Can not implement this method.");
    }
    
    Channel* Interactive::channel() const
    {
        return _channel;
    }
    void Interactive::setChannel(Channel* channel)
    {
        _channel = channel;
    }
    DriverManager* Interactive::manager() const
    {
        return _manager;
    }
    
    EthernetAddress::~EthernetAddress()
    {
    }
    
    BackgroudReceiver::~BackgroudReceiver()
    {
    }
    void BackgroudReceiver::receiveFromBuffer(Device* device)
    {
        if(!device)
            return;
        
        // receive & match & execute
        device->executeInstruction();
    }
}
