#ifndef CHANNELCONTEXT_H
#define CHANNELCONTEXT_H

#include <stdio.h>
#include "common/common_global.h"
#include "../Context.h"

namespace Driver
{
	class COMMON_EXPORT ChannelContext : public Context
	{
	public:
		ChannelContext() : _useReceiveTimeout(false), _reopened(true)
		{
		}
		~ChannelContext(){}

		inline bool useReceiveTimeout() const
		{
			return _useReceiveTimeout;
		}
		void setUseReceiveTimeout(bool useReceiveTimeout)
		{
			_useReceiveTimeout = useReceiveTimeout;
		}
		inline bool reopened() const
		{
			return _reopened;
		}
		void setReopened(bool reopened)
		{
			_reopened = reopened;
		}

	protected:
		bool _useReceiveTimeout;
		bool _reopened;
	};
}

#endif // CHANNELCONTEXT_H