#include "../../exception/Exception.h"
#include "../../diag/Stopwatch.h"
#include "MockInteractive.h"
#include "Channel.h"

namespace Driver
{
    MockChannelContext::MockChannelContext()
    {
    }
    MockChannelContext::~MockChannelContext()
    {
    }
    
    MockInteractive::MockInteractive(DriverManager* dm, Channel* channel) : Interactive(dm, channel)
    {
        if(channel == NULL)
            throw ArgumentException("channel");
        if((MockChannelContext*)channel->description()->context() == NULL)
            throw ArgumentException("channel");
    }
    MockInteractive::~MockInteractive(void)
    {
        close();
    }
    bool MockInteractive::open()
    {
        return true;
    }
    void MockInteractive::close()
    {
        _stream.clear();
    }
    
    bool MockInteractive::connected()
    {
        return true;
    }
    int MockInteractive::available()
    {
        return (int)(_stream.length() - _stream.position());
    }
    
    int MockInteractive::send(byte* buffer, int offset, int count)
    {
#ifdef DEBUG
        Stopwatch sw("serial send", 1000);
#endif
        int len = 0;
        if(connected())
        {
            len = (int)_stream.write(buffer, offset, count);
        }
        return len;
    }
    int MockInteractive::receive(byte* buffer, int offset, int count)
    {
#ifdef DEBUG
        Stopwatch sw("serial receive", 1000);
#endif
        int len = 0;
        if(connected())
        {
            len = (int)_stream.read(buffer, offset, count);
        }
        return len;
    }
    
    int MockInteractive::receive(byte* buffer, int offset, int count, uint timeout)
    {
#ifdef DEBUG
        Stopwatch sw("serial receive2", 1000);
#endif
        int len = 0;
        if(connected())
        {
            len = receive(buffer, offset, count);
        }
        return len;
    }
    
    MockChannelContext* MockInteractive::getChannelContext()
    {
        return (MockChannelContext*)(_channel->description()->context());
    }
    
    void MockInteractive::updateRecvBuffer(const ByteArray& buffer)
    {
        _stream.clear();
        _stream.write(buffer.data(), 0, buffer.count());
        _stream.seek(0, SeekOrigin::SeekBegin);
    }
}
