#include "../../exception/Exception.h"
#include "../../diag/Stopwatch.h"
#include "DtuLiChuangService.h"
#include "../../diag/Trace.h"
#include "../../Resources.h"
#include "common/thread/TickTimeout.h"

using namespace Common;

namespace Driver
{
    DtuLiChuangService::DtuLiChuangService() :
        m_pUdpServer(NULL)
	{
	}

	DtuLiChuangService::~DtuLiChuangService(void)
	{
		unInitialize();
        if (m_pUdpServer != NULL)
        {
            delete m_pUdpServer;
            m_pUdpServer = NULL;
        }
	}

    bool DtuLiChuangService::rebind(int port)
    {
        static int bindingCount = 0;
        const static int MaxBindingCount = 5;
        const static uint MaxDelayTime = 2000;	// 2s

        if (m_pUdpServer != NULL)
        {
            m_pUdpServer->close();
            delete m_pUdpServer;
            m_pUdpServer = NULL;
        }
        m_pUdpServer = new UdpServer();

        string message = Convert::convertStr("DTU: bind UDP socket, port = %d", port);

        bindingCount++;

        bool result = m_pUdpServer->bind("", port); //Any address
        if (!result)
        {
            Trace::writeFormatLine("Failed to bind UDP socket. port = %d", port);

            if (bindingCount < MaxBindingCount)
            {
                Thread::msleep(MaxDelayTime);
                return rebind(port);
            }
            else
            {
                string message = Convert::convertStr(Resources::getString("SocketBindingFailed2").c_str(), port);
                Trace::writeLine(message.c_str(), Trace::Error);
            }
            return false;
        }
        else
        {
            return true;
        }
    }

    bool DtuLiChuangService::initialize(int port)
    {
        if (m_bInitialized) return true;

        bool ret = rebind(port);
        if (ret)
        {
            Trace::writeFormatLine("LiChuang DTU service started successfully at port %u.", port);
            m_bInitialized = true;
            return true;
        }
        else
        {
            Trace::writeFormatLine("Error! LiChuang DTU service started failed at port %u.", port);
            return false;
        }
    }

    bool DtuLiChuangService::unInitialize()
    {
        if (connected())
        {
            m_pUdpServer->close();
        }

        return true;
    }

	bool DtuLiChuangService::connected()
	{
        return m_pUdpServer != NULL;
	}

	int DtuLiChuangService::available(const string &id)
	{
        if (m_pUdpServer == NULL) return 0;

        return m_pUdpServer->available();
    }

	int DtuLiChuangService::send(const string &id, byte* buffer, int offset, int count)
	{
        int len = 0;
		if(connected())
		{
            if (m_mapDtu2Addr.find(id) == m_mapDtu2Addr.end())
            {
                Trace::writeFormatLine("DTU: id %s not found.", id.c_str());
                return len;
            }

            len = m_pUdpServer->write(buffer, count, &m_mapDtu2Addr[id]);
        }

		return len;
	}


    bool DtuLiChuangService::receive(string &id, uint timeout, string &data)
    {
        bool ret = false;
        char buffer[1024];
        int len = m_pUdpServer->receive((byte *)buffer, sizeof(buffer), timeout);
        if (len <= 0) return ret;

        string addr = m_pUdpServer->peerAddr() + ":" + Convert::convertStr(m_pUdpServer->peerPort());

        switch (data[0])
        {
        case 0x7B:
        {
                     string id;
                     if (data[4] == 0x01)
                     {
                         id = string(buffer + 5, 10);
                     }
                     else
                     {
                         id = string(buffer + 4, 11);
                     }
                     m_mapDtu2Addr[id] = m_pUdpServer->sockAddr();
                     m_mapAddr2Dtu[addr] = id;
                     m_pUdpServer->write("\x00\x00\x00", 3);
                     Trace::writeFormatLine("Lichuang DTU %s connected.", id.c_str());
                     break;
        }
        case 0x00:
        {
                     Trace::writeFormatLine("Lichuang DTU heart beat received.");
                     m_pUdpServer->write("\x00\x00\x00", 3);
                     break;
        }
        default:
        {
                   Trace::writeFormatLine("DTU: Data length:%u received.", len);
                   MapAddr2Dtu::const_iterator ci = m_mapAddr2Dtu.find(addr);
                   if (ci == m_mapAddr2Dtu.end())
                   {
                       Trace::writeFormatLine("DTU: Unknown address:%s", addr.c_str());
                       break;
                   }
                   data = string(buffer, len);
                   ret = true;
                   break;
        }
        }

        return ret;
    }
}
