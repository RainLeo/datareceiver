#ifndef BLUETOOTHSERVERINTERACTIVE_H
#define BLUETOOTHSERVERINTERACTIVE_H

#include "common/common_global.h"
#include "Interactive.h"
#include "Channel.h"
#include "BluetoothServerChannelContext.h"
#include "BluetoothClient.h"
#include "BluetoothServer.h"
#include "BluetoothServerClient.h"
#include "TcpServerInteractive.h"
#include "../../data/LoopArray.h"
#include "../../thread/Thread.h"
#include "../../thread/TickTimeout.h"
#include "../../thread/Locker.h"

using namespace Common;

namespace Driver
{
	class COMMON_EXPORT BluetoothServerInteractive : public Interactive
	{
	public:
		BluetoothServerInteractive(DriverManager* dm, Channel* channel);
		~BluetoothServerInteractive(void);

		bool open();
		void close();

		bool connected();
		int available();

		int send(byte* buffer, int offset, int count);
		int receive(byte* buffer, int offset, int count);

		inline void setAcceptAction(client_accpet_callback acceptAction)
		{
			_acceptAction = acceptAction;
		}
		inline void setCloseAction(client_close_callback closeAction)
		{
			_closeAction = closeAction;
		}

	private:
		friend void bluetooth_acceptProc(void* parameter);
		void acceptProcInner();

		friend void bluetooth_closeProc(void* parameter);
		void closeProcInner();

		inline BluetoothServerChannelContext* getChannelContext()  
		{
			return (BluetoothServerChannelContext*)(_channel->description()->context());
		}

		bool rebind();

	private:
		BluetoothServer* _bluetoothServer;
		mutex _bluetoothServerMutex;

		mutex _mutexClients;
		BluetoothServerClients _clients;

		Thread* _acceptThread;
		Thread* _closeThread;

		client_accpet_callback _acceptAction;
		client_close_callback _closeAction;
	};
}

#endif // BLUETOOTHSERVERINTERACTIVE_H
