#ifndef TCPSERVERCHANNELCONTEXT_H
#define TCPSERVERCHANNELCONTEXT_H

#include "common/common_global.h"
#include "ChannelContext.h"
#include "../../system/TimeSpan.h"
#include "TcpChannelContext.h"

using namespace Common;

namespace Driver
{
	class COMMON_EXPORT TcpServerChannelContext : public TcpChannelBaseContext
	{
	public:
        TcpServerChannelContext() : TcpServerChannelContext("", 0)
		{
		}
		TcpServerChannelContext(const string& address, int port, int maxConnections = 65535) : TcpChannelBaseContext(address, port)
		{
			_maxConnections = maxConnections;
		}
		~TcpServerChannelContext(void)
		{
		}

		inline int maxConnections() const
		{
			return _maxConnections;
		}
		inline void setMaxConnections(int maxConnections)
		{
			_maxConnections = maxConnections;
		}
        
        void copyFrom(TcpChannelBaseContext* context)
        {
            TcpChannelBaseContext::copyFrom(context);
            
            TcpServerChannelContext* tc = (TcpServerChannelContext*)context;
            _maxConnections = tc->_maxConnections;
        }

	private:
		int _maxConnections;
	};
}

#endif // TCPSERVERCHANNELCONTEXT_H
