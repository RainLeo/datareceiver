#include <assert.h>
#include "../../thread/Locker.h"
#include "../../thread/TickTimeout.h"
#include "../../diag/Stopwatch.h"
#include "../../diag/Debug.h"
#include "common/Resources.h"
#include "../DriverManager.h"
#include "../devices/DeviceDescription.h"
#include "ChannelDescription.h"
#include "TcpInteractive.h"
#include "TcpBackgroudReceiver.h"

namespace Driver
{
    void tcp_receiveProc(void* parameter)
    {
        TcpBackgroudReceiver* ti = (TcpBackgroudReceiver*)parameter;
        assert(ti);
        ti->receiveProcInner();
    }
    
    TcpBackgroudReceiver::TcpBackgroudReceiver(DriverManager* dm, Channel* channel, TcpClient* client, bool isServer)
    {
        if(dm == NULL)
            throw ArgumentNullException("dm");
        if (client == NULL)
            throw ArgumentNullException("client");
        if (channel == NULL)
            throw ArgumentNullException("channel");
        
        _client = client;
        _isServer = isServer;
        _manager = dm;
        _originalDevice = nullptr;
        
        _context = dynamic_cast<TcpChannelBaseContext*>(channel->description()->context());
        assert(_context);
        
        createDevice(channel);
        
        _receiveThread = new Thread();
        _receiveThread->setName("tcp_receiveProc");
        _receiveThread->startProc(tcp_receiveProc, this, 1);
        
        _startTime = TickTimeout::GetCurrentTickCount();
    }
    TcpBackgroudReceiver::~TcpBackgroudReceiver()
    {
        if (_receiveThread != NULL)
        {
            _receiveThread->stop();
            delete _receiveThread;
            _receiveThread = NULL;
        }
        
        assert(_originalDevice);
        _originalDevice->removeReceiveDevice(_device);
        _originalDevice = nullptr;
        
        if (_channel != NULL)
        {
            delete _channel;
            _channel = NULL;
        }
        if (_device != NULL)
        {
            delete _device;
            _device = NULL;
        }
        if (_dd != NULL)
        {
            delete _dd;
            _dd = NULL;
        }
        if(_client != NULL)
        {
            if(_isServer)
                delete _client;
            _client = NULL;
        }
    }
    
    int TcpBackgroudReceiver::available()
    {
        int result = _client != NULL ? _client->available() : 0;
        if (result > 0)
        {
            _startTime = TickTimeout::GetCurrentTickCount();
        }
        return result;
    }
    bool TcpBackgroudReceiver::connected() const
    {
        if(_isServer)
        {
            TimeSpan clientTimeout = _context->closeTimout();
            uint deadTime = TickTimeout::GetDeadTickCount(_startTime, (uint)clientTimeout.totalMilliseconds());
            if (TickTimeout::IsTimeout(_startTime, deadTime))
                return false;
        }
        return _client != NULL ? _client->connected() : false;
    }
    
    void TcpBackgroudReceiver::receiveProcInner()
    {
#ifdef DEBUG
        Stopwatch sw("receiveProc", 1000);
#endif
        
        if (/*connected() &&*/ available() > 0) // remove connected by HYC on 2016/3/15 to fix the logical error: always return false
        {
            receiveFromBuffer(_device);
        }
    }
    
    void TcpBackgroudReceiver::createDevice(const Channel* channel)
    {
        DriverManager* dm = manager();
        assert(dm);
        Device* device = dm->getDevice(channel);
        assert(device);
        _originalDevice = device;
        
        string channelName = Convert::convertStr("TcpClient_%s", _context->address().c_str());
        ChannelDescription* cd = new ChannelDescription(channelName, "TcpInteractive");
        cd->context()->setReopened(false);
        TcpChannelBaseContext* tc = dynamic_cast<TcpChannelBaseContext*>(cd->context());
        tc->copyFrom(_context);
        _channel = new Channel(dm, cd);
        TcpInteractive* ti = dynamic_cast<TcpInteractive*>(_channel->interactive());
        assert(ti);
        ti->updateTcpClient(_client);
        
        string deviceName = Convert::convertStr("ClientDevice_%s", _context->address().c_str());
        DeviceDescription* dd = new DeviceDescription(deviceName, cd,
                                                      device->instructionSet()->clone(),
                                                      device->description()->context()->clone());
        _dd = dd;
        _device = new Device(dd, _channel);
        device->addReceiveDevice(_device);
    }
    void TcpBackgroudReceiver::updateDevice(Channel* channel)
    {
        DriverManager* dm = manager();
        assert(dm);
        Device* device = dm->getDevice(channel);
        assert(device);
        
        _channel = channel;
        _dd = device->description();
        _device = device;
    }
    
    int TcpBackgroudReceiver::send(byte* buffer, int offset, int count)
    {
#ifdef DEBUG
        Stopwatch sw("socket send", 1000);
#endif
        int len = 0;
        if(connected())
        {
            len =  _client->write(buffer + offset, count);
            if(len > 0)
            {
                _startTime = TickTimeout::GetCurrentTickCount();
            }
        }
        return len;
    }
}
