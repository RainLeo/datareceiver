#ifndef INTERACTIVE_H
#define INTERACTIVE_H

#include "common/common_global.h"
#include "../../system/Convert.h"
#include "../../exception/Exception.h"

using namespace Common;

namespace Driver
{
    class Device;
	class Channel;
    class DriverManager;
	class COMMON_EXPORT Interactive
	{
	public:
        virtual ~Interactive();
        
	protected:
        Interactive(DriverManager* dm, Channel* channel = NULL);

	public:
		virtual bool open() = 0;
		virtual void close() = 0;

		virtual bool connected() = 0;
		virtual int available() = 0;

		virtual int send(byte* buffer, int offset, int count) = 0;
		virtual int receive(byte* buffer, int offset, int count) = 0;
        virtual int receive(byte* buffer, int offset, int count, uint timeout);
        virtual int receive(ByteArray* buffer, int count, uint timeout);

        Channel* channel() const;
        void setChannel(Channel* channel);
        
    protected:
        DriverManager* manager() const;

	protected:
		Channel* _channel;
		bool _useReceiveTimeout;
        
        DriverManager* _manager;
	};
    
    class EthernetAddress
    {
    public:
        virtual ~EthernetAddress();
        virtual const string& peerAddr() const = 0;
        virtual int peerPort() const = 0;
    };
    
    class BackgroudReceiver
    {
    public:
        virtual ~BackgroudReceiver();
        
    protected:
        void receiveFromBuffer(Device* device);
    };
}

#endif // INTERACTIVE_H
