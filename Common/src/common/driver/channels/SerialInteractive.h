#ifndef SERIALINTERACTIVE_H
#define SERIALINTERACTIVE_H

#include "common/common_global.h"
#include "Interactive.h"
#include "SerialChannelContext.h"
#include "ChannelDescription.h"
#include "Channel.h"
#include "common/IO/SerialPort.h"

namespace Driver
{
	class COMMON_EXPORT SerialInteractive : public Interactive
	{
	public:
		SerialInteractive(DriverManager* dm, Channel* channel);
		~SerialInteractive(void);

		bool open() override;
		void close() override;

		bool connected() override;
		int available() override;

		int send(byte* buffer, int offset, int count) override;
		int receive(byte* buffer, int offset, int count) override;
		int receive(byte* buffer, int offset, int count, uint timeout) override;

	private:
		inline SerialChannelContext* getChannelContext()  
		{
			return (SerialChannelContext*)(_channel->description()->context());
		}

	private:
		SerialPort*	_port;	
	};
}

#endif //SERIALINTERACTIVE_H
