#ifndef MOCKINTERACTIVE_H
#define MOCKINTERACTIVE_H

#include "common/common_global.h"
#include "common/IO/MemoryStream.h"
#include "Interactive.h"
#include "ChannelDescription.h"

using namespace Common;

namespace Driver
{
    class COMMON_EXPORT MockChannelContext : public ChannelContext
    {
    public:
        MockChannelContext();
        ~MockChannelContext();
    };
    
    class COMMON_EXPORT MockInteractive : public Interactive
    {
    public:
        MockInteractive(DriverManager* dm, Channel* channel);
        ~MockInteractive(void);
        
        bool open() override;
        void close() override;
        
        bool connected() override;
        int available() override;
        
        int send(byte* buffer, int offset, int count) override;
        int receive(byte* buffer, int offset, int count) override;
        int receive(byte* buffer, int offset, int count, uint timeout) override;
        
        void updateRecvBuffer(const ByteArray& buffer);
        
    private:
        MockChannelContext* getChannelContext();
        
    private:
        MemoryStream _stream;
    };
}

#endif //MOCKINTERACTIVE_H
