#include "../../exception/Exception.h"
#include "../../diag/Stopwatch.h"
#include "DtuCaiMaoService.h"
#include "../../diag/Trace.h"

using namespace Common;

namespace Driver
{
    DtuCaiMaoService::DtuCaiMaoService()
	{
	}

	DtuCaiMaoService::~DtuCaiMaoService(void)
	{
	}

    bool DtuCaiMaoService::initialize(int port)
    {
        if (m_bInitialized) return true;

#ifdef WIN32
        if (DSStartService((u16t)port))
        {
            Trace::writeFormatLine("CaiMao DTU service started successfully at port %u.", port);
            m_bInitialized = true;
            return true;
        }
        else
        {
            Trace::writeFormatLine("Error! CaiMao DTU service failed to start at port %u.", port);
        }
#endif
        return false;
    }

    bool DtuCaiMaoService::unInitialize()
    {
#ifdef WIN32
        return DSStopService() == TRUE;
#endif

        return true;
    }

	int DtuCaiMaoService::send(const string &id, byte* buffer, int offset, int count)
	{
        // reset receiver buffer
        DtuService::send(id, buffer, offset, count);

        int len = 0;
		if(connected())
		{
            uint uid = 0;
            Convert::parseUInt32(id, uid, false);
#ifdef WIN32
            ModemInfoStruct mi;
            u32t iMaxDTUAmount = DSGetModemCount();
            for (int i = 0; i<iMaxDTUAmount; i++)
            {
                DSGetModemByPosition(i, &mi);
                //对 DTU 信息的处理 
            }
            Trace::writeFormatLine("DTU: Sending data to CaiMao dtu %X", uid);
            BOOL b = DSSendData(uid, count, (u8t*)buffer);
            if (!b)
            {
                char msg[1024] = { '\0' };
                DSGetLastError(msg, sizeof(msg));
                Trace::writeFormatLine("DTU: Failed to send data, id %X. Additional information: %s", uid, msg);
                return 0;
            }
            len = count;
#endif
        }

		return len;
	}

    bool DtuCaiMaoService::receive(string &strId, uint timeout, string &strData)
    {
        bool ret = false;
#ifdef WIN32
        ModemDataStruct data;
        BOOL b = DSGetNextData(&data, (u16t)timeout);
        if (!b)
        {
            return ret;
        }
        unsigned int id = data.m_modemId;
        Trace::writeFormatLine("DTU: Data received from modem %X, length:%u, type:%u", id, data.m_data_len, data.m_data_type);
        if (data.m_data_type != 0x01)
        {
            return ret;
        }
        strId = Convert::convertStr("%X", id);
        strData = string((char *)data.m_data_buf, data.m_data_len);
        ret = true;
#endif
        return ret;
    }
}
