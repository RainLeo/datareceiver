#include "../../exception/Exception.h"
#include "../../diag/Stopwatch.h"
#include "../../thread/Thread.h"
#include "Channel.h"
#include "UdpChannelContext.h"
#include "UdpInteractive.h"
#include "ChannelDescription.h"

using namespace Common;

namespace Driver
{
	UdpInteractive::UdpInteractive(DriverManager* dm, Channel* channel) : Interactive(dm, channel)
	{
		if(channel == NULL)
			throw ArgumentException("channel");
		if((UdpChannelContext*)channel->description()->context() == NULL)
			throw ArgumentException("channel");

		_udpClient = NULL;
	}

	UdpInteractive::~UdpInteractive(void)
	{
		close();
	}

	bool UdpInteractive::open()
	{
		close();

		_udpClient = new UdpClient();
		UdpChannelContext* context = getChannelContext();
		return _udpClient->open(context->address().c_str(), context->port());
	}

	void UdpInteractive::close()
	{
		if(_udpClient != NULL)
		{
			_udpClient->close();
			delete _udpClient;
			_udpClient = NULL;
		}
	}

	bool UdpInteractive::connected()
	{
		return _udpClient != NULL;
	}
	int UdpInteractive::available()
	{
		return 0;
	}

	int UdpInteractive::send(byte* buffer, int offset, int count)
	{
#ifdef DEBUG
		Stopwatch sw("socket send", 1000);
#endif
		int len = 0;
		if(connected())
		{
			UdpChannelContext* context = getChannelContext();
			len =  _udpClient->write(context->port(), buffer + offset, count);
		}
		return len;
	}

	int UdpInteractive::receive(byte* buffer, int offset, int count)
	{
		throw new NotSupportException("UdpClient can not receive anything.");
	}
}
