#ifndef DRIVERMANAGER_H
#define DRIVERMANAGER_H

#include <stdio.h>
#include "common/common_global.h"
#include "../data/Vector.h"
#include "../thread/Locker.h"
#include "DriverDescription.h"
#include "channels/ChannelDescription.h"
#include "channels/Channel.h"
#include "devices/Device.h"
#include "devices/InstructionPool.h"

using namespace Common;

namespace Driver
{
	class COMMON_EXPORT DriverManager
	{
	public:
        DriverManager();
        ~DriverManager();

		void open(bool onlyIncludeServerChannel = false);
		void close();
        void reopen(const string& channelName = "", bool allowConnected = false);
        void reopenAll(bool allowConnected = false);

		InstructionContext* executeInstruction(const string& deviceName, InstructionDescription* id);
        
        DriverDescription* description() const;        

		bool hasDevice(const string& deviceName);
        Device* getDevice(const string& deviceName) const;
        Device* getDevice(const Channel* channel) const;
        Device* getDevice(DeviceDescription* dd) const;
        bool containsDevice(DeviceDescription* dd) const;

        bool hasChannel(ChannelDescription* cd);
        Channel* getChannel(const string& channelName) const;
        Channel* getChannel(ChannelDescription* cd) const;
        
        void addPool(const InstructionPool* ip);
        InstructionPool* getPool(const string& deviceName) const;
        InstructionPool* getPoolByChannelName(const string& channelName) const;
        const InstructionPools* getPools() const;

        bool opened() const;

	private:
		void createDevices();
        
        void reopen(Channel* channel, bool allowConnected = false);

	private:
		DriverDescription* _description;
		mutex _mutex;
		Devices* _devices;
		Channels* _channels;
		InstructionPools* _instructionPools;

		bool _opened;
	};
}
#endif // DRIVERMANAGER_H
