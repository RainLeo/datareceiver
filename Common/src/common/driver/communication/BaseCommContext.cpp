#include "common/IO/Path.h"
#include "common/IO/File.h"
#include "common/IO/FileStream.h"
#include "common/system/MD5Provider.h"
#include "BaseCommContext.h"

namespace Driver
{
    ClientContext::ClientContext()
    {
        _peerAddr = "";
        _sentAddr = "";
    }
    ClientContext::~ClientContext()
    {
    }
    
    const string& ClientContext::peerAddr() const
    {
        return _peerAddr;
    }
    void ClientContext::setPeerAddr(const string& peerAddr)
    {
        _peerAddr = peerAddr;
    }
    
    void ClientContext::setSentAddr(const string& sentAddr)
    {
        _sentAddr = sentAddr;
    }
    const string& ClientContext::sentAddr() const
    {
        return _sentAddr;
    }
    
    StatusContext::StatusContext(byte status)
    {
        this->status = status;
    }
    StatusContext::~StatusContext()
    {        
    }
    void StatusContext::write(Stream* stream) const
    {
        stream->writeByte(status);
    }
    void StatusContext::read(Stream* stream)
    {
        status = stream->readByte();
    }
    void StatusContext::copyFrom(const StatusContext* value)
    {
        this->status = value->status;
    }
    
    bool StatusContext::isSuccessful() const
    {
        return status == Succeed;
    }
    
    void StatusContext::operator=(const byte& value)
    {
        this->status = value;
    }
    void StatusContext::operator=(const StatusContext& value)
    {
        this->status = value.status;
    }
    bool StatusContext::operator==(const StatusContext& value) const
    {
        return this->status == value.status;
    }
    bool StatusContext::operator!=(const StatusContext& value) const
    {
        return !operator==(value);
    }
    
    uint BasePacketContext::_packetLength = DefaultPacketLength;
    BasePacketContext::BasePacketContext() : _packetCount(0), _packetNo(0), _transfer(0)
    {
    }
    BasePacketContext::~BasePacketContext()
    {
    }
    
    uint BasePacketContext::packetCount() const
    {
        return _packetCount;
    }
    void BasePacketContext::setPacketCount(uint count)
    {
        _packetCount = count;
    }
    uint BasePacketContext::getPacketNo() const
    {
        return _packetNo;
    }
    void BasePacketContext::setPacketNo(uint no)
    {
        _packetNo = no;
    }
    
    void BasePacketContext::transferHeader()
    {
        _transfer = TransferHeader;
    }
    void BasePacketContext::setTransfer(byte transfer)
    {
        assert(transfer == TransferHeader ||
               transfer == TransferData);
        _transfer = transfer;
    }
    void BasePacketContext::transferData()
    {
        _transfer = TransferData;
    }
    byte BasePacketContext::transfer() const
    {
        return _transfer;
    }
    bool BasePacketContext::isTransferHeader() const
    {
        return _transfer == TransferHeader;
    }
    bool BasePacketContext::isTransferData() const
    {
        return _transfer == TransferData;
    }
    
    uint BasePacketContext::packetLength()
    {
        return _packetLength < MaxPacketLength ? _packetLength : MaxPacketLength;
    }
    void BasePacketContext::setPacketLength(uint packetLength)
    {
        if (packetLength >= MinPacketLength && packetLength <= MaxPacketLength)
        {
            _packetLength = packetLength;
        }
    }
    
    FileHeader::FileHeader()
    {
        fileLength = 0;
        memset(filemd5, 0, MD5_COUNT);
        file_name = "";
        packetCount = 0;
        path = "";
    }
    FileHeader::~FileHeader()
    {
    }
    
    void FileHeader::write(Stream* stream) const
    {
        stream->writeUInt32(fileLength);
        stream->write(filemd5, 0, MD5_COUNT);
        stream->writeStr(file_name, 1);
        stream->writeUInt32(packetCount);
    }
    void FileHeader::read(Stream* stream)
    {
        fileLength = stream->readUInt32();
        stream->read(filemd5, 0, MD5_COUNT);
        file_name = stream->readStr(1);
        packetCount = stream->readUInt32();
    }
    void FileHeader::copyFrom(const FileHeader* obj)
    {
        fileLength = obj->fileLength;
        memcpy(filemd5, obj->filemd5, MD5_COUNT);
        file_name = obj->file_name;
        packetCount = obj->packetCount;
        path = obj->path;
    }
    bool FileHeader::update()
    {
        string fileName = fullFileName();
        if (File::exists(fileName))
        {
            if (MD5Provider::computeHash(fileName.c_str(), filemd5))
            {
                FileStream fs(fileName.c_str(), FileMode::FileOpen, FileAccess::FileRead);
                if(!fs.isOpen())
                    return false;
                fileLength = (uint)fs.length();
            }
            return true;
        }
        return false;
    }
    bool FileHeader::checkmd5() const
    {
        return checkmd5(fullFileName());
    }
    bool FileHeader::checkmd5(const string& filename) const
    {
        byte buffer[MD5_COUNT];
        memset(buffer, 0, sizeof(buffer));
        bool result = MD5Provider::computeHash(filename.c_str(), buffer);
        if (result)
        {
            for (size_t i = 0; i < MD5_COUNT; i++)
            {
                if (buffer[i] != filemd5[i])
                {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    string FileHeader::md5Str() const
    {
        // a23ffa1f42606a4f55754648881465e9
        ByteArray array(filemd5, MD5_COUNT);
        return array.toString("%02X", "");
    }
    bool FileHeader::parseMd5Str(const string& file_md5, byte md5[16])
    {
        // a23ffa1f42606a4f55754648881465e9
        ByteArray array;
        if (ByteArray::parse(file_md5, array, "") &&
            array.count() == MD5_COUNT)
        {
            memcpy(md5, array.data(), MD5_COUNT);
            return true;
        }
        return false;
    }
    bool FileHeader::parseMd5Str(const string& file_md5)
    {
        return parseMd5Str(file_md5, filemd5);
    }
    string FileHeader::fullFileName() const
    {
        if(path.empty())
            return "";
        return Path::combine(path, file_name);
    }
    string FileHeader::tempFullFileName() const
    {
        if(path.empty())
            return "";
        return Path::combine(path, "~" + file_name);
    }
    
    FileData::FileData() : packetNo(0)
    {
    }
    FileData::~FileData()
    {
    }
    
    void FileData::write(Stream* stream) const
    {
        stream->writeUInt32(packetNo);
        stream->writeUInt32(data.count());
        stream->write(data.data(), 0, data.count());
    }
    void FileData::read(Stream* stream)
    {
        packetNo = stream->readUInt32();
        uint length = stream->readUInt32();
        byte* buffer = new byte[length];
        stream->read(buffer, 0, length);
        data.addRange(buffer, length);
        delete[] buffer;
    }
    void FileData::copyFrom(const FileData* obj, bool append)
    {
        packetNo = obj->packetNo;
        if (!append)
        {
            data.clear();
        }
        data.addRange(&obj->data);
    }
}
