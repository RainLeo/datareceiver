#ifndef COMMON_CLIENTINSTRUCTION_H
#define COMMON_CLIENTINSTRUCTION_H

#include <typeinfo>
#include <mutex>
#include "common/IO/Path.h"
#include "common/IO/File.h"
#include "common/IO/FileInfo.h"
#include "common/IO/Directory.h"
#include "common/IO/FileStream.h"
#include "common/IO/MemoryStream.h"
#include "common/IO/MappingStream.h"
#include "common/IO/FileStream.h"
#include "common/data/PrimitiveType.h"
#include "common/diag/Stopwatch.h"
#include "common/driver/instructions/Instruction.h"
#include "common/system/Math.h"
#include "BaseCommContext.h"

using namespace Common;

namespace Driver
{
    class ClientInstruction : public Instruction
    {
    public:
        ClientInstruction(InstructionDescription* id);
        ~ClientInstruction(void);
        
        InstructionContext* execute(Interactive* interactive, Device* device, InstructionContext* context, const ByteArray* buffer = NULL) override;
        
        bool match(const ByteArray* buffer) override;
        
    protected:
        virtual bool getSendBuffer(MemoryStream& ms, ClientContext* context, byte frameId = 0xFF);
        virtual bool getCommandBuffer(MemoryStream& ms, ClientContext* context) = 0;
        
        virtual ClientContext* setReceiveBuffer(MemoryStream& ms, ClientContext* context, bool checkFrameId = true);
        virtual bool setCommandBuffer(MemoryStream& ms, ClientContext* context) = 0;
        
        virtual byte command() const = 0;
        virtual bool autoResponsed() const;
        
        virtual bool syncSendReceive(Interactive* interactive) const;
        
        virtual InstructionContext* executeSync(Interactive* interactive, Device* device, InstructionContext* context, const ByteArray* buffer = NULL);
        virtual InstructionContext* executeAsync(Interactive* interactive, Device* device, InstructionContext* context, const ByteArray* buffer = NULL);
        
        InstructionContext* send(Interactive* interactive, Device* device, InstructionContext* context, byte frameId = -1);
        InstructionContext* receiveFromBuffer(Interactive* interactive, Device* device, InstructionContext* context);
        InstructionContext* receive(Interactive* interactive, Device* device, InstructionContext* context, const ByteArray* buffer);
        
    private:
        inline byte getFrameId()
        {
            _frameId++;
            _frameId &= 0x3F;
            _currentFrameId = _frameId;
            
            return (byte)_currentFrameId;
        }
        inline bool isSameFrameId(byte frameId)
        {
            return _currentFrameId == frameId;
        }
        
        bool hasBackgroudReceiver(Interactive* interactive) const;
        
        friend bool isReceiveFinished(void* parameter);
        bool isReceiveFinishedInner();
        void copyReceiveBuffer(ByteArray& buffer);
        
    private:
        static char _frameId;
        char _currentFrameId;
        
        ByteArray _backgroundBuffer;
        mutex _backgroundBufferMutex;
    };
    
    // T is input data, K is output data.
    template <class T, class K>
    class ElementInstruction : public ClientInstruction
    {
    public:
        ElementInstruction(InstructionDescription* id) : ClientInstruction(id)
        {
        }
        ~ElementInstruction()
        {
        }
        
        bool getCommandBuffer(MemoryStream& ms, ClientContext* context) override
        {
            ElementContext<T, K>* rcontext = dynamic_cast<ElementContext<T, K>*>(context);
            if (rcontext != NULL)
            {
                if(getValue(context) != NULL)
                {
                    ms.writeByte(command());
                    rcontext->inputData()->write(&ms);
                    
                    return true;
                }
            }
            return false;
        }
        bool setCommandBuffer(MemoryStream& ms, ClientContext* context) override
        {
            ElementContext<T, K>* rcontext = dynamic_cast<ElementContext<T, K>*>(context);
            if (rcontext != NULL)
            {
                ms.readByte();	// skip command
                
                rcontext->outputData()->read(&ms);
                
                return setValue(rcontext) != NULL;
            }
            return false;
        }
        
        virtual ClientContext* getValue(ClientContext* context)
        {
            ElementContext<T, K>* rcontext = dynamic_cast<ElementContext<T, K>*>(context);
            if (rcontext != NULL)
            {
                return rcontext;
            }
            return NULL;
        }
        virtual ClientContext* setValue(ClientContext* context)
        {
            ElementContext<T, K>* rcontext = dynamic_cast<ElementContext<T, K>*>(context);
            if (rcontext != NULL)
            {
                return rcontext;
            }
            return NULL;
        }
    };
    // T is input data, K is EmptyContext.
    // Set channel context syncSendReceive=false before use this instruction.
    template <class T>
    class ElementAInstruction : public ElementInstruction<T, EmptyContext>
    {
    public:
        ElementAInstruction(InstructionDescription* id) : ElementInstruction<T, EmptyContext>(id)
        {
        }
        ~ElementAInstruction()
        {
        }
        
    protected:
        bool syncSendReceive(Interactive*) const override
        {
            return false;
        }
    };
    // T is input data, K is StatusContext.
    template <class T>
    class ElementSInstruction : public ElementInstruction<T, StatusContext>
    {
    public:
        ElementSInstruction(InstructionDescription* id) : ElementInstruction<T, StatusContext>(id)
        {
        }
        ~ElementSInstruction()
        {
        }
        
    protected:
        bool syncSendReceive(Interactive*) const override
        {
            return true;
        }
    };
    
    // T is input data, K is output data.
    template <class T, class K>
    class DownloadPacketInstruction : public ClientInstruction
    {
    public:
        DownloadPacketInstruction(InstructionDescription* id) : ClientInstruction(id)
        {
        }
        ~DownloadPacketInstruction()
        {
        }
        
        bool getCommandBuffer(MemoryStream& ms, ClientContext* context) override
        {
            PacketContext<T, K>* rcontext = dynamic_cast<PacketContext<T, K>*>(context);
            if (rcontext != NULL)
            {
                ms.writeByte(command());
                
                byte transfer = rcontext->transfer();
                assert(transfer == BasePacketContext::TransferHeader ||
                       transfer == BasePacketContext::TransferData);
                ms.writeByte(rcontext->transfer());
                if(transfer == BasePacketContext::TransferHeader)
                {
                    rcontext->inputData()->write(&ms);
                    ms.writeUInt32(rcontext->packetLength());
                }
                else if(transfer == BasePacketContext::TransferData)
                {
                    ms.writeUInt32(rcontext->getPacketNo());
                }
                else
                {
                    return false;
                }
                
                return true;
            }
            return false;
        }
        bool setCommandBuffer(MemoryStream& ms, ClientContext* context) override
        {
            PacketContext<T, K>* rcontext = dynamic_cast<PacketContext<T, K>*>(context);
            if (rcontext != NULL)
            {
                ms.readByte();	// skip command
                
                byte transfer = ms.readByte();
                assert(transfer == BasePacketContext::TransferHeader ||
                       transfer == BasePacketContext::TransferData);
                if(transfer == BasePacketContext::TransferHeader)
                {
                    rcontext->status = ms.readByte();
                    if(rcontext->status == StatusContext::Succeed)
                    {
                        rcontext->setPacketCount(ms.readUInt32());
                    }
                }
                else if(transfer == BasePacketContext::TransferData)
                {
                    if(ms.readBoolean())
                        rcontext->outputData()->read(&ms);
                }
                else
                {
                    return false;
                }
                
                return setValue(rcontext) != NULL;
            }
            return false;
        }
        
    protected:
        virtual ClientContext* setValue(ClientContext* context)
        {
            return context;
        }
        
        bool syncSendReceive(Interactive*) const override
        {
            return true;
        }
    };

    // T is input data, K is output data, P is item of T.
    template <class T, class K, class P>
    class UploadPacketInstruction : public ClientInstruction
    {
    public:
        UploadPacketInstruction(InstructionDescription* id) : ClientInstruction(id)
        {
        }
        ~UploadPacketInstruction()
        {
        }
        
        bool getCommandBuffer(MemoryStream& ms, ClientContext* context) override
        {
            PacketContext<T, K>* rcontext = dynamic_cast<PacketContext<T, K>*>(context);
            if (rcontext != NULL)
            {
                setValue(rcontext);
                
                ms.writeByte(command());
                
                byte transfer = rcontext->transfer();
                assert(transfer == BasePacketContext::TransferHeader ||
                       transfer == BasePacketContext::TransferData);
                ms.writeByte(rcontext->transfer());
                if(transfer == BasePacketContext::TransferHeader)
                {
                    uint packetCount = calcPacketCount(rcontext);
                    rcontext->setPacketCount(packetCount);
                    ms.writeUInt32(packetCount);
                    ms.writeUInt32(rcontext->inputData()->count());
                }
                else if(transfer == BasePacketContext::TransferData)
                {
                    // send the elements.
                    uint packetNo = rcontext->getPacketNo();
                    assert(packetNo >= 0 && packetNo < (int)this->_ranges.count());
                    T* t = this->_ranges.at(packetNo);
                    t->write(&ms);
                }
                else
                {
                    return false;
                }
                
                return true;
            }
            return false;
        }
        bool setCommandBuffer(MemoryStream& ms, ClientContext* context) override
        {
            PacketContext<T, K>* rcontext = dynamic_cast<PacketContext<T, K>*>(context);
            if (rcontext != NULL)
            {
                ms.readByte();	// skip command
                
                byte transfer = ms.readByte();
                assert(transfer == BasePacketContext::TransferHeader ||
                       transfer == BasePacketContext::TransferData);
                if(transfer == BasePacketContext::TransferHeader)
                {
                    rcontext->status = ms.readByte();
                    if(rcontext->status == StatusContext::Succeed)
                    {
                        rcontext->outputData()->read(&ms);
                    }
                }
                else if(transfer == BasePacketContext::TransferData)
                {
                    rcontext->status = ms.readByte();
                }
                else
                {
                    return false;
                }
                return true;
            }
            return false;
        }
        
    protected:
        virtual ClientContext* setValue(ClientContext* context)
        {
            return context;
        }
        
        bool syncSendReceive(Interactive*) const override
        {
            return true;
        }
        
    private:
        uint calcPacketCount(PacketContext<T, K>* context)
        {
            const T* inputData = context->inputData();
            assert(inputData);
            MemoryStream ms;
            int count = 0;
            _ranges.clear();
            for (uint i = 0; i < inputData->count(); i++)
            {
                P* p = inputData->at(i);
                p->write(&ms);
                if (ms.length() > context->packetLength())
                {
                    count++;
                    // full, so seek the zero position, and write the last one.
                    ms.clear();
                    p->write(&ms);
                }
                T* t;
                if (_ranges.count() == count)
                {
                    t = new T(false);
                    _ranges.add(t);
                }
                else
                {
                    t = _ranges.at(count);
                }
                t->add(p);
            }
            return _ranges.count();
        }
        
        typedef Vector<T> Ranges;
        
        Ranges _ranges;
    };
    // T is input data, K is EmptyContext, P is item of T.
    template <class T, class P>
    class UploadPacketEInstruction : public UploadPacketInstruction<T, EmptyContext, P>
    {
    public:
        UploadPacketEInstruction(InstructionDescription* id) : UploadPacketInstruction<T, EmptyContext, P>(id)
        {
        }
        ~UploadPacketEInstruction()
        {
        }
    };
    
    // T is input data.
    template <class T>
    class FileInstruction
    {
    public:
        virtual ~FileInstruction(){}
        
    protected:
        uint calcPacketCount(FileContext<T>* context) const
        {
            FileHeader* header = context->header();
            string fileName = Path::combine(header->path, header->file_name);
            if(File::exists(fileName))
            {
                if(!header->update())
                    return 0;
                
                FileStream fs(fileName.c_str(), FileMode::FileOpen, FileAccess::FileRead);
                uint packetLength = context->packetLength();
                assert(packetLength > 0);
                int64_t fileLength = fs.length();
                return (fileLength % packetLength == 0) ? (uint)(fileLength / packetLength) : (uint)(fileLength / packetLength + 1);
            }
            else
            {
                Trace::writeFormatLine("Can not find file in file communication, name: %s", fileName.c_str());
            }
            return 0;
        }
        
        bool saveFile(const FileContext<T>* context)
        {
            return saveFile(context->header(), context->outputData());
        }
        bool saveFile(const FileHeader* header, const FileDatas* fds)
        {
            for (uint i=0; i<fds->count(); i++)
            {
                if(saveFile(header, fds->at(i)))
                    return true;
            }
            return false;
        }
        bool saveFile(const FileHeader* header, const FileData* fd)
        {
            string path = header->path;
            if (!Directory::exists(path))
            {
                if(!Directory::createDirectory(path))
                    return false;
            }
            
            string filename = header->tempFullFileName();
            bool fileExists = File::exists(filename);
            uint dataCount = fd->data.count();
            bool isLastPart = fd->packetNo == header->packetCount - 1;
            int64_t position;
            if (!isLastPart)
            {
                position = fd->packetNo * dataCount;
            }
            else
            {
                // the last part.
                position = header->fileLength - dataCount;
            }
            
#if DEBUG
            const char* formatStr = "saveFile: packetNo: %d, position: %d, exists: %s, isLastPart: %s, dataCount: %d, fileLength: %d, packetCount: %d, filename: %s";
            String str = String::convert(formatStr,
                                             fd->packetNo, (uint)position,
                                             Convert::convertStr(fileExists).c_str(),
                                             Convert::convertStr(isLastPart).c_str(), dataCount,
                                             header->fileLength, header->packetCount, header->file_name.c_str());
            Debug::writeLine(str, Trace::System);
#endif
            
            FileStream fs(filename.c_str(), FileMode::FileCreate);
            if(!fs.isOpen())
                return false;
            fs.seek(position);
            fs.write(fd->data.data(), 0, dataCount);
            fs.close();
            
            //            MappingStream ms(filename.c_str(), header->fileLength);
            //            ms.seek(position);
            //            ms.write(fd->data.data(), 0, dataCount);
            //            ms.close();
            
#if DEBUG
            FileInfo fi(filename.c_str());
            assert(!fi.isReadOnly());
#endif
            //            // check md5
            //            if (!header->checkmd5(filename))
            //                return false;
            return true;
        }
        
        bool readFile(FileContext<T>* rcontext, Stream* stream) const
        {
            const FileHeader* header = rcontext->header();
            string fileName = Path::combine(header->path, header->file_name);
            if(File::exists(fileName))
            {
                FileStream fs(fileName.c_str(), FileMode::FileOpen, FileAccess::FileRead);
                if(!fs.isOpen())
                    return false;
                uint packetNo = rcontext->getPacketNo();
                uint position = packetNo * rcontext->packetLength();
                fs.seek(position);
                uint fileLength = header->fileLength;
                uint lastLength = fileLength - position;
                uint dataLength = rcontext->packetLength() <= lastLength ? rcontext->packetLength() : lastLength;
                byte* buffer = new byte[dataLength];
                memset(buffer, 0, dataLength);
                uint rlen = (uint)fs.read(buffer, 0, dataLength);
                assert(rlen == dataLength);
                
                FileDatas data;
                FileData* item = new FileData();
                item->packetNo = packetNo;
                item->data.addRange(buffer, dataLength);
                delete[] buffer;
                data.add(item);
                data.write(stream);
                
                return true;
            }
            return false;
        }
    };
    // T is input data.
    template <class T>
    class DownloadFileInstruction : public ClientInstruction, public FileInstruction<T>
    {
    public:
        DownloadFileInstruction(InstructionDescription* id) : ClientInstruction(id)
        {
        }
        ~DownloadFileInstruction()
        {
        }
        
        bool getCommandBuffer(MemoryStream& ms, ClientContext* context) override
        {
            FileContext<T>* rcontext = dynamic_cast<FileContext<T>*>(context);
            if (rcontext != NULL)
            {
                ms.writeByte(command());
                
                byte transfer = rcontext->transfer();
                assert(transfer == BasePacketContext::TransferHeader ||
                       transfer == BasePacketContext::TransferData);
                ms.writeByte(rcontext->transfer());
                if(transfer == BasePacketContext::TransferHeader)
                {
                    rcontext->inputData()->write(&ms);
                    ms.writeUInt32(rcontext->packetLength());
                }
                else if(transfer == BasePacketContext::TransferData)
                {
                    rcontext->header()->write(&ms);
                    ms.writeUInt32(rcontext->getPacketNo());
                }
                else
                {
                    return false;
                }
                
                return true;
            }
            return false;
        }
        bool setCommandBuffer(MemoryStream& ms, ClientContext* context) override
        {
            FileContext<T>* rcontext = dynamic_cast<FileContext<T>*>(context);
            if (rcontext != NULL)
            {
                ms.readByte();	// skip command
                
                byte transfer = ms.readByte();
                assert(transfer == BasePacketContext::TransferHeader ||
                       transfer == BasePacketContext::TransferData);
                if(transfer == BasePacketContext::TransferHeader)
                {
                    rcontext->status = ms.readByte();
                    if(rcontext->status == StatusContext::Succeed)
                    {
                        rcontext->setPacketCount(ms.readUInt32());
                        rcontext->header()->read(&ms);
                    }
                }
                else if(transfer == BasePacketContext::TransferData)
                {
                    rcontext->outputData()->read(&ms);
                    this->saveFile(rcontext);
                }
                else
                {
                    return false;
                }
                
                return setValue(rcontext) != NULL;
            }
            return false;
        }
        
        virtual ClientContext* setValue(ClientContext* context)
        {
            return context;
        }
        
    protected:
        bool syncSendReceive(Interactive*) const override
        {
            return true;
        }
    };
    
    // T is input data.
    template <class T>
    class UploadFileInstruction : public ClientInstruction, public FileInstruction<T>
    {
    public:
        UploadFileInstruction(InstructionDescription* id) : ClientInstruction(id)
        {
        }
        ~UploadFileInstruction()
        {
        }
        
        bool getCommandBuffer(MemoryStream& ms, ClientContext* context) override
        {
            FileContext<T>* rcontext = dynamic_cast<FileContext<T>*>(context);
            if (rcontext != NULL)
            {
                setValue(rcontext);
                
                ms.writeByte(command());

                byte transfer = rcontext->transfer();
                assert(transfer == BasePacketContext::TransferHeader ||
                       transfer == BasePacketContext::TransferData);
                ms.writeByte(rcontext->transfer());
                if(transfer == BasePacketContext::TransferHeader)
                {
                    rcontext->inputData()->write(&ms);
                    uint packetCount = this->calcPacketCount(rcontext);
                    rcontext->setPacketCount(packetCount);
                    rcontext->header()->write(&ms);

                    return true;
                }
                else if(transfer == BasePacketContext::TransferData)
                {
                    // send the elements.
                    if(this->readFile(rcontext, &ms))
                    {
                        return true;
                    }
                }
                return false;
            }
            return false;
        }
        bool setCommandBuffer(MemoryStream& ms, ClientContext* context) override
        {
            FileContext<T>* rcontext = dynamic_cast<FileContext<T>*>(context);
            if (rcontext != NULL)
            {
                ms.readByte();	// skip command
                
                byte transfer = ms.readByte();
                assert(transfer == BasePacketContext::TransferHeader ||
                       transfer == BasePacketContext::TransferData);
                if(transfer == BasePacketContext::TransferHeader)
                {
                    rcontext->status = ms.readByte();
                }
                else if(transfer == BasePacketContext::TransferData)
                {
                    rcontext->status = ms.readByte();
                }
                else
                {
                    return false;
                }                
                return true;
            }
            return false;
        }
        
        virtual ClientContext* setValue(ClientContext* context)
        {
            return context;
        }
        
    protected:
        bool syncSendReceive(Interactive*) const override
        {
            return true;
        }
    };
}

#endif // COMMON_CLIENTINSTRUCTION_H
