#ifndef COMMON_COMMINSTRUCTIONSET_H
#define COMMON_COMMINSTRUCTIONSET_H

#include "common/driver/instructions/InstructionSet.h"
#include "ServerInstruction.h"

using namespace Common;

namespace Driver
{
    typedef void(*instructions_callback)(void*, Instructions*);
    
    class COMMON_EXPORT TcpInstructionSet : public InstructionSet
    {
    public:
        TcpInstructionSet(void* owner, instructions_callback action);
        ~TcpInstructionSet();

        void generateInstructions(Instructions* instructions);
        
        bool receive(Device* device, Channel* channel, ByteArray* buffer);
        
        InstructionSet* clone() const
        {
            return new TcpInstructionSet(_owner, _instructionsCallback);
        }
        
    private:
        instructions_callback _instructionsCallback;
        void* _owner;
    };

	class COMMON_EXPORT SerialInstructionSet : public InstructionSet
	{
	public:
		SerialInstructionSet(void* owner, instructions_callback action);
		~SerialInstructionSet();

		void generateInstructions(Instructions* instructions);

		bool receive(Device* device, Channel* channel, ByteArray* buffer);

		InstructionSet* clone() const
		{
			return new SerialInstructionSet(_owner, _instructionsCallback);
		}

	private:
		instructions_callback _instructionsCallback;
		void* _owner;
	};
    
    class COMMON_EXPORT UdpInstructionSet : public InstructionSet
    {
    public:
        UdpInstructionSet(void* owner, instructions_callback action);
        ~UdpInstructionSet();
        
        void generateInstructions(Instructions* instructions);
        
        bool receive(Device* device, Channel* channel, ByteArray* buffer);
        
        InstructionSet* clone() const
        {
            return new UdpInstructionSet(_owner, _instructionsCallback);
        }
        
    private:
        instructions_callback _instructionsCallback;
        void* _owner;
    };
}
#endif // COMMON_COMMINSTRUCTIONSET_H
