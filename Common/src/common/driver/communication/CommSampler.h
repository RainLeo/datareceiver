#ifndef COMMON_COMMSAMPLER_H
#define COMMON_COMMSAMPLER_H

#include "common/driver/channels/ChannelDescription.h"
#include "common/driver/devices/DeviceDescription.h"
#include "common/driver/devices/Sampler.h"
#include "BaseCommConfig.h"

using namespace Common;

namespace Driver
{
    typedef InstructionDescription* (*sampler_callback)();
    
    class COMMON_EXPORT TcpSampler : public Sampler
    {
    public:
        TcpSampler(DriverManager* dm, ChannelDescription* cd, DeviceDescription* dd, const Client::Connection& connection, sampler_callback action = NULL);
        ~TcpSampler();
        
    protected:
        // return the sample interval, unit: ms
        uint detetionInterval() const
        {
            return _detetionInterval;
        }
        // return the resume interval, unit: ms
        uint resumeInterval() const
        {
            return _resumeInterval;
        }
        // return the sample interval, unit: ms
        int detetionCount() const
        {
            return _detetionCount;
        }
        
        InstructionDescription* sampleInstruction();
        void resultInstruction(InstructionDescription* id);
        void setConnectStatus(Device::Status status);
        
    private:
        uint _detetionInterval;
        uint _resumeInterval;
        int _detetionCount;

        sampler_callback _sampleCallback;
    };
    
    class COMMON_EXPORT UdpSampler : public Sampler
    {
    public:
        UdpSampler(DriverManager* dm, ChannelDescription* cd, DeviceDescription* dd, const Broadcast& broadcast, sampler_callback action = NULL);
        ~UdpSampler();
        
    protected:
        // return the sample interval, unit: ms
        uint detetionInterval() const
        {
            return _detetionInterval;
        }
        // return the resume interval, unit: ms
        uint resumeInterval() const
        {
            return _resumeInterval;
        }
        // return the sample interval, unit: ms
        int detetionCount() const
        {
            return _detetionCount;
        }
        
        InstructionDescription* sampleInstruction();
        void resultInstruction(InstructionDescription* id);
        
    private:
        uint _detetionInterval;
        uint _resumeInterval;
        int _detetionCount;
        
        Broadcast _broadcast;
        sampler_callback _sampleCallback;
    };
    
    class COMMON_EXPORT ServerSampler : public InstructionPool
    {
    public:
        ServerSampler(DriverManager* dm, ChannelDescription* cd, DeviceDescription* dd);
        ~ServerSampler();
    };
}

#endif // COMMON_COMMSAMPLER_H
