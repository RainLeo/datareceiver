#include "common/thread/TickTimeout.h"
#include "common/diag/Debug.h"
#include "common/Resources.h"
#include "CommSampler.h"
#include "BaseCommContext.h"
#include "BaseCommConfig.h"

using namespace Common;

namespace Driver
{
    TcpSampler::TcpSampler(DriverManager* dm, ChannelDescription* cd, DeviceDescription* dd, const Client::Connection& connection, sampler_callback action)
        : Sampler(dm, cd, dd)
    {
        _detetionInterval = (uint)connection.detectionInterval.totalMilliseconds();
        _resumeInterval = (uint)connection.resumeInterval.totalMilliseconds();
        _detetionCount = connection.detectionCount;

        _sampleCallback = action;
    }
    TcpSampler::~TcpSampler()
    {
    }
    
    void TcpSampler::setConnectStatus(Device::Status status)
    {
        Device::Status oldStatus = getConnectStatus();
        Sampler::setConnectStatus(status);
        if (oldStatus != status)
        {
            string addrStr = ((TcpChannelContext*)context())->address();
            if (Device::isStatusChanged(oldStatus, status))
            {
                Trace::writeFormatLine(Resources::getString("NetStateChanged").c_str(), addrStr.c_str(),
                                       Device::getStatusStr(oldStatus).c_str(), Device::getStatusStr(status).c_str());
            }
        }
    }
    InstructionDescription* TcpSampler::sampleInstruction()
    {
        if(_sampleCallback != NULL)
            return _sampleCallback();
        
        return NULL;
    }
    void TcpSampler::resultInstruction(InstructionDescription* id)
    {
        // nothing to do.
    }
    
    UdpSampler::UdpSampler(DriverManager* dm, ChannelDescription* cd, DeviceDescription* dd, const Broadcast& broadcast, sampler_callback action)
    : Sampler(dm, cd, dd)
    {
        _detetionInterval = (uint)broadcast.interval.totalMilliseconds();
        _resumeInterval = _detetionInterval;
        _detetionCount = broadcast.count;
        
        _broadcast = broadcast;
        _sampleCallback = action;
    }
    UdpSampler::~UdpSampler()
    {
    }
    
    InstructionDescription* UdpSampler::sampleInstruction()
    {
        if(_sampleCallback != NULL)
            return _sampleCallback();
        
        return NULL;
    }
    void UdpSampler::resultInstruction(InstructionDescription* id)
    {
        // nothing to do.
    }
    
    ServerSampler::ServerSampler(DriverManager* dm, ChannelDescription* cd, DeviceDescription* dd) : InstructionPool(dm, cd, dd)
    {
    }
    ServerSampler::~ServerSampler()
    {
    }
}
