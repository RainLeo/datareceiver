#ifndef COMMON_SERVERSERVICE_H
#define COMMON_SERVERSERVICE_H

#include "common/common_global.h"
#include "common/data/PrimitiveType.h"
#include "BaseCommService.h"
#include "BaseCommConfig.h"

using namespace Common;

namespace Driver
{
    class COMMON_EXPORT ServerService : public BaseCommService
    {
    public:
        ServerService();
        ServerService(const Server& server);
        ~ServerService();
        
        const Server& server() const
        {
            return _server;
        }
        
        // T is input data, C is context.
        // Sent all of clients if address is empty.
        // Use , or ; to split address.
        template<class T, class C>
        bool sendAsync(const string& address, const T& inputData, const string& name)
        {
#if DEBUG
            Stopwatch sw(String::convert("ServerService::sendAsync, name: %s", name.c_str()), 200);
#endif
            
            if (!_instructionPool)
                return false;
            if (!_instructionPool->channelConnected())
                return false;
            
            C* context = new C();
            context->setInputData(&inputData);
            context->setSentAddr(address);
            InstructionDescription* id = new InstructionDescription(name, context);
            return _instructionPool->addInstruction(id) != NULL;
        }
        // T is input data, K is output data, C is context.
        // Sent all of clients if address is empty.
        // Use , or ; to split address.
        template<class T, class K, class C>
        bool sendSync(const string& address, const T& inputData, K& outputData, const string& name)
        {
#if DEBUG
            Stopwatch sw(String::convert("ServerService::sendSync, name: %s", name.c_str()), 200);
#endif
            
            if (!_instructionPool)
                return false;
            if (!_instructionPool->channelConnected())
                return false;
            
            C* context = new C();
            context->setInputData(&inputData);
            context->setSentAddr(address);
            InstructionDescription* id = new InstructionDescription(name, context);
            C* rcontext = dynamic_cast<C*>(_instructionPool->executeInstructionSync(id));
            if (isSendSuccessfully(id->name(), rcontext))
            {
                outputData.copyFrom(rcontext->outputData());
                return true;
            }
            return false;
        }
        
    protected:
        void createDevice(const InstructionCallback& callback) override;

    private:
        void createTcpDevice(const InstructionCallback& callback);
        void createUdpServerDevice(const InstructionCallback& callback);
        void createUdpClientDevice(const InstructionCallback& callback);
        
        bool isSendSuccessfully(const string& instructionName, const InstructionContext* ic) const;
        bool isSendSuccessfully(const string& instructionName, const InstructionContext* ic, bool condition) const;
        
    protected:
        Server _server;
    };
}
#endif // COMMON_SERVERSERVICE_H
