#include "common/diag/Trace.h"
#include "common/diag/Stopwatch.h"
#include "common/driver/DriverManager.h"
#include "common/driver/devices/DeviceDescription.h"
#include "common/driver/channels/ChannelDescription.h"
#include "common/Resources.h"
#include "ClientService.h"
#include "BaseCommConfig.h"

using namespace Common;
using namespace Driver;

namespace Driver
{
    ClientService::ClientService() : BaseCommService()
    {
    }
    ClientService::ClientService(const Client& client) : ClientService()
    {
        _client = client;
    }
    
    ClientService::~ClientService()
    {
    }
    
    void ClientService::createTcpDevice(const InstructionCallback& callback, bool enabled)
    {
        if(_instructionPool != NULL)
            return;
        
        DriverManager* dm = manager();
        assert(dm);
        
        TcpInstructionSet* set = new TcpInstructionSet(this, callback.tcpInstructions);
        ChannelDescription* cd = new ChannelDescription("TcpClient", "TcpInteractive");
        cd->setEnabled(enabled);
        DeviceDescription* dd = new DeviceDescription("TcpClientDevice", cd, set);
        TcpChannelContext* tc = (TcpChannelContext*)cd->context();
        tc->setAddress(_client.address);
        tc->setPort(_client.port);
        tc->setSendBufferSize(_client.sendBufferSize);
        tc->setReceiveBufferSize(_client.receiveBufferSize);
        tc->setNoDelay(_client.noDelay);
        tc->setSyncSendReceive(false);
        
        tc->setOpenTimeout(_client.timeout.open);
        dd->setReceiveTimeout(_client.timeout.receive);
        dd->setSendTimeout(_client.timeout.send);
        
        BasePacketContext::setPacketLength(_client.packetLength);
        
        dm->description()->addDevice(dd);
        if(callback.tcpSampler != NULL)
        {
            dm->addPool(_instructionPool = new TcpSampler(dm, cd, dd, _client.connection, callback.tcpSampler));
        }
        
        Trace::writeFormatLine(Resources::getString("CreateTcpChannelInfo").c_str(), cd->name().c_str(), tc->address().c_str(), tc->port());
    }
    void ClientService::createUdpClientDevice(const InstructionCallback& callback)
    {
        DriverManager* dm = manager();
        assert(dm);
        
        const Client& client = _client;
        
        UdpInstructionSet* set = new UdpInstructionSet(this, callback.udpInstructions);
        ChannelDescription* cd = new ChannelDescription("UdpClient", "UdpInteractive");
        DeviceDescription* dd = new DeviceDescription("UdpClientDevice", cd, set);
        UdpChannelContext* uc = (UdpChannelContext*)cd->context();
        
        uc->setPort(client.broadcast.receiveport);
        dm->description()->addDevice(dd);
        if(callback.udpSampler != NULL)
        {
            UdpSampler* sampler = new UdpSampler(dm, cd, dd, client.broadcast, callback.udpSampler);
            _udpSampler = sampler;
            dm->addPool(sampler);
        }
        
        Trace::writeFormatLine("Create the UDP client ethernet channel: name: %s, port: %d",
                               cd->name().c_str(), uc->port());
    }
    void ClientService::createUdpServerDevice(const InstructionCallback& callback)
    {
        DriverManager* dm = manager();
        assert(dm);
        
        const Client& client = _client;
        
        UdpInstructionSet* set = new UdpInstructionSet(this, callback.udpInstructions);
        ChannelDescription* cd = new ChannelDescription("UdpServer", "UdpServerInteractive");
        DeviceDescription* dd = new DeviceDescription("UdpServerDevice", cd, set);
        UdpServerChannelContext* uc = (UdpServerChannelContext*)cd->context();
        
        uc->setPort(client.broadcast.sendport);
        dm->description()->addDevice(dd);

		Trace::writeFormatLine("Create the UDP server ethernet channel: name: %s, port: %d",
			cd->name().c_str(), uc->port());
    }
    void ClientService::createDevice(const InstructionCallback& callback)
    {
        if (!_client.enabled)
            return;
        
        const Client& client = _client;
        
        bool createUdpDevice = false;
        if(client.broadcast.enabled && callback.udpInstructions != NULL)
        {
            // create udp device.
            createUdpDevice = true;
            createUdpClientDevice(callback);
            createUdpServerDevice(callback);
        }
        createTcpDevice(callback, !createUdpDevice);
    }
    
    bool ClientService::isSendSuccessfully(const string& instructionName, const InstructionContext* ic) const
    {
        return isSendSuccessfully(instructionName, ic, ic != NULL && !ic->hasException());
    }
    bool ClientService::isSendSuccessfully(const string& instructionName, const InstructionContext* ic, bool condition) const
    {
        if (condition)
        {
#if DEBUG
            Debug::writeFormatLine("Send an instraction '%s' successfully.", instructionName.c_str());
#endif
            return true;
        }
        else
        {
            string mess = Convert::convertStr(Resources::getString("FailedtoSendInstruction").c_str(),
                                              instructionName.c_str(), (ic != NULL && ic->hasException() ? ic->getException()->getMessage() : Resources::getString("CommunicationException").c_str()));
            
            Trace::writeLine(mess.c_str(), Trace::Error);
            return false;
        }
    }

    Sampler* ClientService::clientSampler() const
    {
        return dynamic_cast<Sampler*>(_instructionPool);
    }
}
