#ifndef COMMON_CLIENTSERVICE_H
#define COMMON_CLIENTSERVICE_H

#include "common/thread/Thread.h"
#include "common/thread/TickTimeout.h"
#include "common/driver/instructions/InstructionContext.h"
#include "common/driver/DriverManager.h"
#include "CommSampler.h"
#include "BaseCommService.h"
#include "BaseCommConfig.h"

using namespace Common;

namespace Driver
{
    class ClientService : public BaseCommService
    {
    public:
        ClientService();
        ClientService(const Client& client);
        ~ClientService();
        
        const Client& client() const
        {
            return _client;
        }
        
        // T is input data, K is output data, C is context.
        template<class T, class K, class C>
        bool sendSync(const T& inputData, K& outputData, const string& name) const
        {
#if DEBUG
            Stopwatch sw(Convert::convertStr("ClientService::sendSync, name: %s", name.c_str()), 200);
#endif
            if (!_instructionPool)
                return false;
            if (!clientSampler()->isOnline())
                return false;
            
            bool result = false;
            C* context = new C();
            context->setInputData(&inputData);
            InstructionDescription* id = new InstructionDescription(name, context);
            C* rcontext = dynamic_cast<C*>(_instructionPool->executeInstructionSync(id));
            if (isSendSuccessfully(id->name(), rcontext))
            {
                result = true;
                outputData.copyFrom(rcontext->outputData());
            }
            delete id;
            return result;
        }
        // T is input data, C is context.
        // sync send input data.
        template<class T, class C>
        StatusContext sendSync(const T& inputData, const string& name) const
        {
            StatusContext status;
            if(sendSync<T, StatusContext, ElementSContext<T>>(inputData, status, name))
            {
                return status;
            }
            return StatusContext(StatusContext::CommunicationError);
        }
        // T is input data, C is context.
        // sync download.
        template<class T, class C>
        bool sendAsync(const T& inputData, const string& name) const
        {
#if DEBUG
            Stopwatch sw(Convert::convertStr("ClientService::sendAsync, name: %s", name.c_str()), 200);
#endif
            
            if (!_instructionPool)
                return false;
            if (!clientSampler()->isOnline())
                return false;
            
            C* context = new C();
            context->setInputData(&inputData);
            InstructionDescription* id = new InstructionDescription(name, context);
            return _instructionPool->addInstruction(id) != NULL;
        }
        // T is input data, K is output data(Vector), C is context.
        template<class T, class K, class C>
        bool downloadPacketSync(const T& inputData, K& outputData, const string& name) const
        {
#if DEBUG
            Stopwatch sw(Convert::convertStr("ClientService::sendPacket, name: %s", name.c_str()), 200);
#endif
            if (!_instructionPool)
                return false;
            if (!clientSampler()->isOnline())
                return false;
            
            bool result = false;
            C* context = new C();
            context->setInputData(&inputData);
            context->transferHeader();
            InstructionDescription* ids = new InstructionDescription(name, context);
            C* rcontext = dynamic_cast<C*>(_instructionPool->executeInstructionSync(ids));
            if (isSendSuccessfully(ids->name(), rcontext))
            {
                result = true;
                uint packetCount = rcontext->packetCount();
                for (uint i = 0; i < packetCount; i++)
                {
                    context->setPacketNo(i);
                    context->transferData();
                    InstructionDescription* id = new InstructionDescription(name, context, false);
                    C* rcontext = dynamic_cast<C*>(_instructionPool->executeInstructionSync(id));
                    if (isSendSuccessfully(id->name(), rcontext))
                    {
                        outputData.copyFrom(rcontext->outputData(), true);
                        delete id;
                    }
                    else
                    {
                        result = false;
                        delete id;
                        break;
                    }
                }
            }
            delete ids;
            return result;
        }
        
        // T is input data, C is context.
        template<class T, class C>
        bool downloadFileSync(const string& path, const T& inputData, const string& name) const
        {
#if DEBUG
            Stopwatch sw(Convert::convertStr("ClientService::downloadFile, name: %s", name.c_str()), 200);
#endif
            if (!_instructionPool)
                return false;
            if (!clientSampler()->isOnline())
                return false;

#ifdef DEBUG
            uint start = TickTimeout::GetCurrentTickCount();
#endif
            bool result = false;
            C* context = new C();
            context->setInputData(&inputData);
            context->transferHeader();
            InstructionDescription* ids = new InstructionDescription(name, context);
            C* rcontext = dynamic_cast<C*>(_instructionPool->executeInstructionSync(ids));
            if (isSendSuccessfully(ids->name(), rcontext))
            {
                FileHeader* header = rcontext->header();
                header->path = path;
                
                string fileName = header->file_name;
#ifdef DEBUG
                uint length = header->fileLength;
#endif
                result = true;
                uint packetCount = rcontext->packetCount();
                for (uint i = 0; i < packetCount; i++)
                {
                    C* context = new C();
                    context->setInputData(&inputData);
                    context->setHeader(header);
                    context->transferData();
                    InstructionDescription* id = new InstructionDescription(name, context);
                    context->setPacketNo(i);
                    C* rcontext = dynamic_cast<C*>(_instructionPool->executeInstructionSync(id));
                    if (isSendSuccessfully(id->name(), rcontext))
                    {
                        delete id;
                    }
                    else
                    {
                        result = false;
                        delete id;
                        break;
                    }
                }
                
                // check MD5.
                if(result)
                {
                    result = header->checkmd5(header->tempFullFileName());
                    if(result)
                    {
                        File::move(header->tempFullFileName(), header->fullFileName());
                    }
#ifdef DEBUG
                    string lengthStr;
                    if(length < 1024)
                    {
                        lengthStr = Convert::convertStr(length);
                    }
                    else if(length >= 1024 && length < 1024 * 1024)     // 1K - 1M
                    {
                        lengthStr = Convert::convertStr("%dK", length / 1024);
                    }
                    else    // > 1M
                    {
                        lengthStr = Convert::convertStr("%.1fM", (double)length / 1024.0 / 1024.0);
                    }
                    uint elapsed = TickTimeout::Elapsed(start);
                    const char* elapsedStr = elapsed <= 10 * 1000 ? "elapsed: %.0f ms" : "elapsed: %.3f s";
                    string info = (string)"download a file %s, file name: %s, file length: %s, speed: %.1f K/s, " + elapsedStr;
                    Trace::writeFormatLine(info.c_str(),
                                           result ? "successfully" : "unsuccessfully",
                                           fileName.c_str(),
                                           lengthStr.c_str(),
                                           ((double)length / 1024.0) / ((double)elapsed / 1000.0),
                                           elapsed <= 10 * 1000 ? elapsed : elapsed / 1000.0);
#endif
                }
            }
            delete ids;
            return result;
        }

        // T is input data(Vector), K is EmptyContext, C is context.
        template<class T, class C>
        bool uploadPacketSync(const T& inputData, const string& name) const
        {
            EmptyContext empty;
            return uploadPacketSync<T, EmptyContext, C>(inputData, empty, name);
        }
        // T is input data(Vector), K is output data, C is context.
        template<class T, class K, class C>
        bool uploadPacketSync(const T& inputData, K& outputData, const string& name) const
        {
#if DEBUG
            Stopwatch sw(Convert::convertStr("ClientService::upload, name: %s", name.c_str()), 200);
#endif
            if (!_instructionPool)
                return false;
            if (!clientSampler()->isOnline())
                return false;
            
            bool result = false;
            C* context = new C();
            context->setInputData(&inputData);
            context->transferHeader();
            InstructionDescription* ids = new InstructionDescription(name, context);
            C* rcontext = dynamic_cast<C*>(_instructionPool->executeInstructionSync(ids));
            if (isSendSuccessfully(ids->name(), rcontext))
            {
                result = true;
                uint packetCount = rcontext->packetCount();
                for (uint i = 0; i < packetCount; i++)
                {
                    context->setPacketNo(i);
                    context->transferData();
                    InstructionDescription* id = new InstructionDescription(name, context, false);
                    C* rcontext = dynamic_cast<C*>(_instructionPool->executeInstructionSync(id));
                    if (isSendSuccessfully(id->name(), rcontext))
                    {
                        outputData.copyFrom(rcontext->outputData());
                        delete id;
                    }
                    else
                    {
                        result = false;
                        delete id;
                        break;
                    }
                }
            }
            delete ids;
            return result;
        }
        // T is input data, C is context.
        template<class T, class C>
        bool uploadFileSync(const FileHeader& header, const T& inputData, const string& name) const
        {
#if DEBUG
            Stopwatch sw(Convert::convertStr("ClientService::uploadFile, name: %s", name.c_str()), 200);
#endif
            
            if (!_instructionPool)
                return false;
            if (!clientSampler()->isOnline())
                return false;
            
            FileHeader temp;
            temp.copyFrom(&header);
            if (!temp.update())
            {
                Trace::writeLine(Convert::convertStr("can not find upload file'%s'.", header.file_name.c_str()), Trace::Error);
                return false;
            }
            
#ifdef DEBUG
            uint start = TickTimeout::GetCurrentTickCount();
            uint length = temp.fileLength;
            string fileName = temp.file_name;
#endif
            bool result = false;
            C* context = new C();
            context->setHeader(&temp);
            context->transferHeader();
            InstructionDescription* ids = new InstructionDescription(name, context);
            C* rcontext = dynamic_cast<C*>(_instructionPool->executeInstructionSync(ids));
            if (isSendSuccessfully(ids->name(), rcontext))
            {
                result = true;
                uint packetCount = rcontext->packetCount();
                for (uint i = 0; i < packetCount; i++)
                {
                    InstructionDescription* id = new InstructionDescription(name, context, false);
                    context->setPacketNo(i);
                    context->transferData();
                    C* rcontext = dynamic_cast<C*>(_instructionPool->executeInstructionSync(id));
                    if (!isSendSuccessfully(id->name(), rcontext))
                    {
                        delete id;
                        result = false;
                        break;
                    }
                    delete id;
                }
            }
            delete ids;
            
#ifdef DEBUG
            string lengthStr;
            if(length < 1024)
            {
                lengthStr = Convert::convertStr(length);
            }
            else if(length >= 1024 && length < 1024 * 1024)     // 1K - 1M
            {
                lengthStr = Convert::convertStr("%dK", length / 1024);
            }
            else    // > 1M
            {
                lengthStr = Convert::convertStr("%.1fM", (double)length / 1024.0 / 1024.0);
            }
            uint elapsed = TickTimeout::Elapsed(start);
            const char* elapsedStr = elapsed <= 10 * 1000 ? "elapsed: %.0f ms" : "elapsed: %.3f s";
            string info = (string)"upload a file %s, file name: %s, file length: %s, speed: %.1f K/s, " + elapsedStr;
            Trace::writeFormatLine(info.c_str(),
                                   result ? "successfully" : "unsuccessfully",
                                   fileName.c_str(),
                                   lengthStr.c_str(),
                                   ((double)length / 1024.0) / ((double)elapsed / 1000.0),
                                   elapsed <= 10 * 1000 ? elapsed : elapsed / 1000.0);
#endif
            return result;
        }
        
    protected:
        void createDevice(const InstructionCallback& callback) override;
        
        bool isSendSuccessfully(const string& instructionName, const InstructionContext* ic) const;
        bool isSendSuccessfully(const string& instructionName, const InstructionContext* ic, bool condition) const;
        
        Sampler* clientSampler() const;
        
    private:
        void createTcpDevice(const InstructionCallback& callback, bool enabled = true);
        void createUdpClientDevice(const InstructionCallback& callback);
        void createUdpServerDevice(const InstructionCallback& callback);
        
    protected:
        Client _client;
    };
}
#endif // COMMON_CLIENTSERVICE_H
