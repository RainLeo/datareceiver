#include "common/diag/Trace.h"
#include "common/diag/Debug.h"
#include "common/IO/Path.h"
#include "common/IO/Directory.h"
#include "common/system/MD5Provider.h"
#include "common/driver/channels/Channel.h"
#include "common/driver/devices/Device.h"
#include "ServerInstruction.h"

namespace Driver
{
    ServerInstruction::ServerInstruction(InstructionDescription* id) : ClientInstruction(id)
    {
    }
    ServerInstruction::~ServerInstruction()
    {
    }
    InstructionContext* ServerInstruction::executeSync(Interactive* interactive, Device* device, InstructionContext* context, const ByteArray* buffer)
    {
        return receive(interactive, device, context, buffer);
    }
    ClientContext* ServerInstruction::setReceiveBuffer(MemoryStream& ms, ClientContext* context, bool checkFrameId)
    {
        ClientContext* result = ClientInstruction::setReceiveBuffer(ms, context, checkFrameId);
        return setValue(result);
    }
}
