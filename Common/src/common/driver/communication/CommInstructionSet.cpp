#include "common/data/BCDUtilities.h"
#include "common/driver/channels/Channel.h"
#include "common/driver/devices/Device.h"
#include "CommInstructionSet.h"

namespace Driver
{
    TcpInstructionSet::TcpInstructionSet(void* owner, instructions_callback action)
    {
        _owner = owner;
        if(action == NULL)
            throw ArgumentException("action");
        _instructionsCallback = action;
    }
    TcpInstructionSet::~TcpInstructionSet()
    {
    }
    
    void TcpInstructionSet::generateInstructions(Instructions* instructions)
    {
        if(_instructionsCallback != NULL)
        {
            _instructionsCallback(_owner, instructions);
        }
    }
    
    bool TcpInstructionSet::receive(Device* device, Channel* channel, ByteArray* buffer)
    {
        if (NULL == channel)
        {
            return false;
        }
        if (!channel->connected())
        {
            return false;
        }
        
        uint timeout = device->description()->receiveTimeout();
        
        int headerLength = channel->receiveBySize(buffer, ClientContext::HeaderLength, timeout);
        if (ClientContext::HeaderLength != headerLength ||
            (*buffer)[ClientContext::HeaderPosition] != ClientContext::Header)
        {
            return false;
        }
        
        int validLength = (int)BCDUtilities::BCDToInt64(buffer->data(), 2, ClientContext::BufferBCDLength);
        int dataLen = channel->receiveBySize(buffer, validLength, timeout);
        if (validLength != dataLen)
        {
            ByteArray messages(buffer->data(), Math::min((int)buffer->count(), 128));
            Debug::writeFormatLine("error: tcp receive error! recv: %s", messages.toString().c_str());
            return false;
        }
        return true;
    }

	SerialInstructionSet::SerialInstructionSet(void* owner, instructions_callback action)
	{
		_owner = owner;
		if (action == NULL)
			throw ArgumentException("action");
		_instructionsCallback = action;
	}
	SerialInstructionSet::~SerialInstructionSet()
	{
	}

	void SerialInstructionSet::generateInstructions(Instructions* instructions)
	{
		if (_instructionsCallback != NULL)
		{
			_instructionsCallback(_owner, instructions);
		}
	}

	bool SerialInstructionSet::receive(Device* device, Channel* channel, ByteArray* buffer)
	{
		if (NULL == channel)
		{
			return false;
		}
		if (!channel->connected())
		{
			return false;
		}

		uint timeout = device->description()->receiveTimeout();

		bool matchHeader = false;
		do
		{
			int rLength = channel->receiveBySize(buffer, 1, timeout);
			if (rLength == 1 && (*buffer)[ClientContext::HeaderPosition] == ClientContext::Header)
			{
				matchHeader = true;
				break;
			}
			else if (rLength != 1)
			{
				matchHeader = false;
				break;
			}
		} while (!matchHeader);
		if (!matchHeader)
		{
			return false;
		}

		int headerLength = channel->receiveBySize(buffer, ClientContext::HeaderLength - 1, timeout);
		if (ClientContext::HeaderLength - 1 != headerLength ||
            (*buffer)[ClientContext::HeaderPosition] != ClientContext::Header)
		{
			return false;
		}
        
		int validLength = (int)BCDUtilities::BCDToInt64(buffer->data(), 2, ClientContext::BufferBCDLength);
		int dataLen = channel->receiveBySize(buffer, validLength, timeout);
        if (validLength != dataLen)
        {
            ByteArray messages(buffer->data(), Math::min((int)buffer->count(), 128));
            Debug::writeFormatLine("error: serial receive error! recv: %s", messages.toString().c_str());
            return false;
        }
        
        return true;
	}
    
    UdpInstructionSet::UdpInstructionSet(void* owner, instructions_callback action)
    {
        _owner = owner;
        if(action == NULL)
            throw ArgumentException("action");
        _instructionsCallback = action;
    }
    UdpInstructionSet::~UdpInstructionSet()
    {
    }
    
    void UdpInstructionSet::generateInstructions(Instructions* instructions)
    {
        if(_instructionsCallback != NULL)
        {
            _instructionsCallback(_owner, instructions);
        }
    }
    
    bool UdpInstructionSet::receive(Device* device, Channel* channel, ByteArray* buffer)
    {
        if (NULL == channel)
        {
            return false;
        }
        if (!channel->connected())
        {
            return false;
        }
        
        UdpServerChannelContext* context = static_cast<UdpServerChannelContext*>(channel->context());
        assert(context != NULL);
        context->setPeek(true);
        
        uint timeout = device->description()->receiveTimeout();
        
        int headerLength = channel->receiveBySize(buffer, ClientContext::HeaderLength, timeout);
        if (ClientContext::HeaderLength != headerLength ||
            (*buffer)[ClientContext::HeaderPosition] != ClientContext::Header)
        {
            return false;
        }
        
        context->setPeek(false);
        
        int validLength = (int)BCDUtilities::BCDToInt64(buffer->data(), 2, ClientContext::BufferBCDLength);
        validLength += ClientContext::HeaderLength;
        int dataLen = channel->receiveBySize(buffer, validLength, timeout);
        if (validLength != dataLen)
        {
            ByteArray messages(buffer->data(), Math::min((int)buffer->count(), 128));
            Debug::writeFormatLine("error: udp receive error! recv: %s", messages.toString().c_str());
            return false;
        }
        
        return true;
    }
}
