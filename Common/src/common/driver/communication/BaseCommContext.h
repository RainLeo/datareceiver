#ifndef COMMON_BASECOMMCONTEXT_H
#define COMMON_BASECOMMCONTEXT_H

#include "common/data/StringArray.h"
#include "common/system/Version.h"
#include "common/system/DateTime.h"
#include "common/data/StreamVector.h"
#include "common/driver/instructions/InstructionContext.h"

using namespace Common;

namespace Driver
{
    class COMMON_EXPORT ClientContext : public InstructionContext
    {
    public:
        ClientContext();
        ~ClientContext();
        
        const string& peerAddr() const;
        void setPeerAddr(const string& peerAddr);
        
        void setSentAddr(const string& sentAddr);
        const string& sentAddr() const;
        
    public:
        static const int BufferBCDLength = 4;
        
        static const int Header = 0xEE;
        static const int HeaderLength = 2 + BufferBCDLength;
        static const int MinFrameLen = HeaderLength + 1 + 2;
        
        static const int HeaderPosition = 0;
        static const int FrameIdPosition = 1;
        static const int LengthPosition = 2;
        static const int AddressPosition = HeaderLength;
        static const int CommandPosition = HeaderLength;

    private:
        string _sentAddr;
        string _peerAddr;
    };
    
    // element instruction's context.
    // T is input data, K is output data.
    template <class T, class K>
    class ElementContext : public ClientContext
    {
    public:
        ElementContext() : ClientContext()
        {
        }
        ~ElementContext()
        {
        }
        
        inline void setInputData(const T* inputData)
        {
            if (inputData != NULL)
            {
                _inputData.copyFrom(inputData);
            }
        }
        inline T* inputData() const
        {
            return (T*)&_inputData;
        }
        inline void setOutputData(const K* outputData)
        {
            if (outputData != NULL)
            {
                _outputData.copyFrom(outputData);
            }
        }
        inline K* outputData() const
        {
            return (K*)&_outputData;
        }
        
    private:
        T _inputData;
        K _outputData;
    };
    
    class StatusContext
    {
    public:
        byte status;
        
        StatusContext(byte status = Succeed);
        virtual ~StatusContext();
        
        void write(Stream* stream) const;
        void read(Stream* stream);
        void copyFrom(const StatusContext* value);
        
        inline bool isSuccessful() const;

        void operator=(const byte& value);
        void operator=(const StatusContext& value);
        bool operator==(const StatusContext& value) const;
        bool operator!=(const StatusContext& value) const;
        
    public:
        static const byte Succeed = 0x00;
        static const byte CommunicationError = 0xFF;
    };
    struct EmptyContext
    {
        void write(Stream* stream) const
        {
        }
        void read(Stream* stream)
        {
        }
        void copyFrom(const EmptyContext* value)
        {
        }
    };
    
    // async element instruction's context.
    // T is input data, K is EmptyContext.
    template <class T>
    class ElementAContext : public ElementContext<T, EmptyContext>
    {
    };
    // sync element instruction's context.
    // T is input data, K is StatusContext.
    template <class T>
    class ElementSContext : public ElementContext<T, StatusContext>
    {
    };
    
    // base packet context.
    class BasePacketContext
    {
    public:
        BasePacketContext();
        virtual ~BasePacketContext();
        
        uint packetCount() const;
        void setPacketCount(uint count);
        uint getPacketNo() const;
        void setPacketNo(uint no);
        
        void transferHeader();
        void setTransfer(byte transfer);
        void transferData();
        byte transfer() const;
        bool isTransferHeader() const;
        bool isTransferData() const;

        static uint packetLength();
        static void setPacketLength(uint packetLength);
        
    public:
        static const byte TransferHeader = 0x01;
        static const byte TransferData = 0x02;
        
        static const int MinPacketLength = 1024 * 1;
        static const int DefaultPacketLength = 1024 * 1024 * 1;
        static const int MaxPacketLength = 1024 * 1024 * 10;
        
    protected:
        static uint _packetLength;
        
        uint _packetCount;
        uint _packetNo;
        
        byte _transfer;
    };

    class FileHeader
    {
    protected:
        static const int MD5_COUNT = 16;
        
    public:
        uint fileLength;
        byte filemd5[MD5_COUNT];
        string file_name;
        uint packetCount;
        string path;	// use by local db, it is not necessary to use in communications.
        
        FileHeader();
        virtual ~FileHeader();
        
        virtual void write(Stream* stream) const;
        virtual void read(Stream* stream);
        virtual void copyFrom(const FileHeader* obj);
        
        bool update();
        bool checkmd5() const;
        bool checkmd5(const string& filename) const;
        string md5Str() const;
        static bool parseMd5Str(const string& file_md5, byte md5[MD5_COUNT]);
        bool parseMd5Str(const string& file_md5);
        
        string fullFileName() const;
        string tempFullFileName() const;
    };
    class FileData
    {
    public:
        uint packetNo;
        ByteArray data;
        
        FileData();
        virtual ~FileData();
        
        virtual void write(Stream* stream) const;
        virtual void read(Stream* stream);
        virtual void copyFrom(const FileData* obj, bool append = false);
    };
    typedef StreamVector<FileData> FileDatas;
    
    // download & upload packet instruction's context.
    // T is input data, K is output data.
    template <class T, class K>
    class PacketContext : public ElementContext<T, K>, public BasePacketContext, public StatusContext
    {
    public:
        PacketContext(bool autoDelete = false) : ElementContext<T, K>(), BasePacketContext(), StatusContext(), _count(0)
        {
            _autoDelete = autoDelete;
        }
        ~PacketContext()
        {
        }
        
        inline void setCount(uint count)
        {
            _count = count;
        }
        inline uint count() const
        {
            return _count;
        }
        
        inline bool autoDelete() const
        {
            return _autoDelete;
        }
        
    public:
        static const byte PacketNotFound = 1;
        
    private:
        uint _count;
        bool _autoDelete;
    };
    // upload packet instruction's context.
    // T is input data, K is EmptyContext.
    template <class T>
    class PacketEContext : public PacketContext<T, EmptyContext>
    {
    public:
        PacketEContext() : PacketContext<T, EmptyContext>()
        {
        }
        ~PacketEContext()
        {
        }
        
    public:
        static const byte PacketNotFound = 1;
    };
    
    // download & upload file instruction's context.
    // T is input data
    template <class T>
    class FileContext : public ElementContext<T, FileDatas>, public BasePacketContext, public StatusContext
    {
    public:
        FileContext() : ElementContext<T, FileDatas>(), BasePacketContext(), StatusContext()
        {
        }
        ~FileContext()
        {
        }
        
        inline void setHeader(const FileHeader* header)
        {
            _header.copyFrom(header);
        }
        inline FileHeader* header() const
        {
            return (FileHeader*)&_header;
        }
        
        inline void setPacketCount(uint count)
        {
            _header.packetCount = count;
            BasePacketContext::setPacketCount(count);
        }
        
    public:
        static const byte FileNotFound = 1;
        
    private:
        FileHeader _header;
    };
}
#endif // COMMON_BASECOMMCONTEXT_H
