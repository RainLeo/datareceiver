#include "common/system/TimeSpan.h"
#include "common/system/Convert.h"
#include "common/IO/Directory.h"
#include "common/IO/File.h"
#include "BaseCommConfig.h"

using namespace Common;

namespace Driver
{
    Broadcast::Broadcast() : interval(0, 0, 1), count(10), sendport(10041), receiveport(10042), enabled(false)
    {
    }
    Broadcast::Broadcast(const Broadcast& value)
    {
        this->operator=(value);
    }
    
    void Broadcast::operator=(const Broadcast& value)
    {
        this->interval = value.interval;
        this->count = value.count;
        this->sendport = value.sendport;
        this->receiveport = value.receiveport;
        this->enabled = value.enabled;
    }
    bool Broadcast::operator==(const Broadcast& value) const
    {
        return this->interval == value.interval &&
            this->count == value.count &&
            this->sendport == value.sendport &&
            this->receiveport == value.receiveport &&
            this->enabled == value.enabled;
    }
    bool Broadcast::operator!=(const Broadcast& value) const
    {
        return !operator==(value);
    }
    
    Client::Connection::Connection() : detectionInterval(0, 0, 3), resumeInterval(0, 0, 10), detectionCount(3)
    {
    }
    Client::Connection::Connection(const Client::Connection& value)
    {
        this->operator=(value);
    }
    
    void Client::Connection::operator=(const Connection& value)
    {
        this->detectionInterval = value.detectionInterval;
        this->resumeInterval = value.resumeInterval;
        this->detectionCount = value.detectionCount;
    }
    bool Client::Connection::operator==(const Connection& value) const
    {
        return this->detectionInterval == value.detectionInterval &&
            this->resumeInterval == value.resumeInterval &&
            this->detectionCount == value.detectionCount;
    }
    bool Client::Connection::operator!=(const Connection& value) const
    {
        return !operator==(value);
    }
    
    Client::Timeout::Timeout() : send(0, 0, 3), receive(0, 0, 3), open(0, 0, 3)
    {
    }
    Client::Timeout::Timeout(const Timeout& value)
    {
        this->operator=(value);
    }
    
    void Client::Timeout::operator=(const Timeout& value)
    {
        this->send = value.send;
        this->receive = value.receive;
        this->open = value.open;
    }
    bool Client::Timeout::operator==(const Timeout& value) const
    {
        return this->send == value.send &&
            this->receive == value.receive &&
            this->open == value.open;
    }
    bool Client::Timeout::operator!=(const Timeout& value) const
    {
        return !operator==(value);
    }
    
    Client::Client()
    {
        name = "";
        address = "";
        port = 0;
        packetLength = 0;
        sendBufferSize = 512 * 1024;
        receiveBufferSize = 512 * 1024;
        noDelay = true;
        enabled = false;
    }
    Client::Client(const Client& value)
    {
        this->operator=(value);
    }
    bool Client::isEmpty() const
    {
        return name.empty();
    }
    
    void Client::operator=(const Client& value)
    {
        this->name = value.name;
        this->address = value.address;
        this->broadcast = value.broadcast;
        this->port = value.port;
        this->connection = value.connection;
        this->timeout = value.timeout;
        this->packetLength = value.packetLength;
        this->sendBufferSize = value.sendBufferSize;
        this->receiveBufferSize = value.receiveBufferSize;
        this->noDelay = value.noDelay;
        this->enabled = value.enabled;
    }
    bool Client::operator==(const Client& value) const
    {
        return this->name == value.name &&
            this->address == value.address &&
            this->broadcast == value.broadcast &&
            this->port == value.port &&
            this->connection == value.connection &&
            this->timeout == value.timeout &&
            this->packetLength == value.packetLength &&
            this->sendBufferSize == value.sendBufferSize &&
            this->receiveBufferSize == value.receiveBufferSize &&
            this->noDelay == value.noDelay &&
            this->enabled == value.enabled;
    }
    bool Client::operator!=(const Client& value) const
    {
        return !operator==(value);
    }
    
    Server::Timeout::Timeout() : send(0, 0, 3), receive(0, 0, 3), close(0, 3, 0)
    {
    }
    Server::Timeout::Timeout(const Timeout& value)
    {
        this->operator=(value);
    }
    
    void Server::Timeout::operator=(const Timeout& value)
    {
        this->send = value.send;
        this->receive = value.receive;
        this->close = value.close;
    }
    bool Server::Timeout::operator==(const Timeout& value) const
    {
        return this->send == value.send &&
            this->receive == value.receive &&
            this->close == value.close;
    }
    bool Server::Timeout::operator!=(const Timeout& value) const
    {
        return !operator==(value);
    }
    
    Server::Server()
    {
        name = "";
        address = "";
        port = 0;
        maxConnections = 0;
        sendBufferSize = 512 * 1024;
        receiveBufferSize = 512 * 1024;
        noDelay = true;
        enabled = false;
    }
    Server::Server(const Server& value)
    {
        this->operator=(value);
    }
    
    bool Server::isEmpty() const
    {
        return name.empty();
    }
    
    void Server::operator=(const Server& value)
    {
        this->name = value.name;
        this->address = value.address;
        this->broadcast = value.broadcast;
        this->port = value.port;
        this->maxConnections = value.maxConnections;
        this->timeout = value.timeout;
        this->sendBufferSize = value.sendBufferSize;
        this->receiveBufferSize = value.receiveBufferSize;
        this->noDelay = value.noDelay;
        this->enabled = value.enabled;
    }
    bool Server::operator==(const Server& value) const
    {
        return this->name == value.name &&
            this->address == value.address &&
            this->broadcast == value.broadcast &&
            this->port == value.port &&
            this->maxConnections == value.maxConnections &&
            this->timeout == value.timeout &&
            this->sendBufferSize == value.sendBufferSize &&
            this->receiveBufferSize == value.receiveBufferSize &&
            this->noDelay == value.noDelay &&
            this->enabled == value.enabled;
    }
    bool Server::operator!=(const Server& value) const
    {
        return !operator==(value);
    }
    
    BaseCommConfig::BaseCommConfig(const ConfigFile& file) : Configuration(file)
    {
    }
    
    BaseCommConfig::~BaseCommConfig()
    {
    }
    
    const Server& BaseCommConfig::server() const
    {
        return _server;
    }
    const Client& BaseCommConfig::client() const
    {
        return _client;
    }
    
    void BaseCommConfig::loadServerNode(XmlTextReader& reader)
    {
        ServerConfig::loadServerNode(reader, _server);
    }
    void BaseCommConfig::loadClientNode(XmlTextReader& reader)
    {
        ClientConfig::loadClientNode(reader, _client);
    }
    bool BaseCommConfig::parseBufferLength(const string& str, uint& value)
    {
        if (!str.empty())
        {
            uint length = 0;
            size_t bufferLength = str.length() - 1;
            const char unit = str[bufferLength];
            if (unit == 'k' || unit == 'K')
            {
                if(!Convert::parseUInt32(str.substr(0, bufferLength), length))
                    return false;
                length *= 1024;
            }
            else
            {
                if(!Convert::parseUInt32(str, length))
                    return false;
                length *= 1024;
            }
            value = length;
            return true;
        }
        return false;
    }
    
    ServerConfig::ServerConfig(const ConfigFile& file) : Configuration(file)
    {
    }
    
    ServerConfig::~ServerConfig()
    {
    }

    void ServerConfig::loadServerNode(XmlTextReader& reader)
    {
        loadServerNode(reader, _server);
    }
    void ServerConfig::loadServerNode(XmlTextReader& reader, Server& server)
    {
        if (reader.nodeType() == XmlNodeType::Element &&
            reader.localName() == "server")
        {
            server.name = reader.getAttribute("name");
            server.address = reader.getAttribute("address");
            Convert::parseInt32(reader.getAttribute("port"), server.port);
            Convert::parseInt32(reader.getAttribute("maxConnections"), server.maxConnections);
            BaseCommConfig::parseBufferLength(reader.getAttribute("sendBufferSize"), server.sendBufferSize);
            BaseCommConfig::parseBufferLength(reader.getAttribute("receiveBufferSize"), server.receiveBufferSize);
            Convert::parseBoolean(reader.getAttribute("noDelay"), server.noDelay);            
            Convert::parseBoolean(reader.getAttribute("enabled"), server.enabled);
            while (reader.read())
            {
                if (reader.nodeType() == XmlNodeType::Element &&
                    reader.localName() == "broadcast")
                {
                    TimeSpan::parse(reader.getAttribute("interval"), server.broadcast.interval);
                    Convert::parseInt32(reader.getAttribute("count"), server.broadcast.count);
                    Convert::parseInt32(reader.getAttribute("sendport"), server.broadcast.sendport);
                    Convert::parseInt32(reader.getAttribute("receiveport"), server.broadcast.receiveport);
                    Convert::parseBoolean(reader.getAttribute("enabled"), server.broadcast.enabled);
                }
                else if (reader.nodeType() == XmlNodeType::Element &&
                         reader.localName() == "timeout")
                {
                    TimeSpan::parse(reader.getAttribute("close"), server.timeout.close);
                    TimeSpan::parse(reader.getAttribute("receive"), server.timeout.receive);
                    TimeSpan::parse(reader.getAttribute("send"), server.timeout.send);
                }
            }
        }
    }
    const Server& ServerConfig::server() const
    {
        return _server;
    }
    
    ClientConfig::ClientConfig(const ConfigFile& file) : Configuration(file)
    {
    }
    
    ClientConfig::~ClientConfig()
    {
    }
    
    void ClientConfig::loadClientNode(XmlTextReader& reader)
    {
        loadClientNode(reader, _client);
    }
    void ClientConfig::loadClientNode(XmlTextReader& reader, Client& client)
    {
        if (reader.nodeType() == XmlNodeType::Element &&
            reader.localName() == "client")
        {
            client.name = reader.getAttribute("name");
            client.address = reader.getAttribute("address");
            Convert::parseInt32(reader.getAttribute("port"), client.port);
            BaseCommConfig::parseBufferLength(reader.getAttribute("packetlength"), client.packetLength);
            BaseCommConfig::parseBufferLength(reader.getAttribute("sendBufferSize"), client.sendBufferSize);
            BaseCommConfig::parseBufferLength(reader.getAttribute("receiveBufferSize"), client.receiveBufferSize);
            Convert::parseBoolean(reader.getAttribute("noDelay"), client.noDelay);
            Convert::parseBoolean(reader.getAttribute("enabled"), client.enabled);
            while (reader.read())
            {
                if (reader.nodeType() == XmlNodeType::Element &&
                    reader.localName() == "broadcast")
                {
                    TimeSpan::parse(reader.getAttribute("interval"), client.broadcast.interval);
                    Convert::parseInt32(reader.getAttribute("count"), client.broadcast.count);
                    Convert::parseInt32(reader.getAttribute("sendport"), client.broadcast.sendport);
                    Convert::parseInt32(reader.getAttribute("receiveport"), client.broadcast.receiveport);
                    Convert::parseBoolean(reader.getAttribute("enabled"), client.broadcast.enabled);
                }
                else if (reader.nodeType() == XmlNodeType::Element &&
                         reader.localName() == "connection")
                {
                    TimeSpan::parse(reader.getAttribute("detectioninterval"), client.connection.detectionInterval);
                    TimeSpan::parse(reader.getAttribute("resumeinterval"), client.connection.resumeInterval);
                    Convert::parseInt32(reader.getAttribute("detectioncount"), client.connection.detectionCount);
                }
                else if (reader.nodeType() == XmlNodeType::Element &&
                         reader.localName() == "timeout")
                {
                    TimeSpan::parse(reader.getAttribute("open"), client.timeout.open);
                    TimeSpan::parse(reader.getAttribute("receive"), client.timeout.receive);
                    TimeSpan::parse(reader.getAttribute("send"), client.timeout.send);
                }
            }
        }
    }
    
    const Client& ClientConfig::client() const
    {
        return _client;
    }
}
