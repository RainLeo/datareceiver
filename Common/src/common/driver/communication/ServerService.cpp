#include "common/diag/Trace.h"
#include "common/diag/Stopwatch.h"
#include "common/driver/DriverManager.h"
#include "common/driver/devices/DeviceDescription.h"
#include "common/driver/channels/ChannelDescription.h"
#include "common/Resources.h"
#include "ServerService.h"

namespace Driver
{
    ServerService::ServerService() : BaseCommService()
    {
    }
    ServerService::ServerService(const Server& server) : ServerService()
    {
        _server = server;
    }
    
    ServerService::~ServerService()
    {
    }
    
    void ServerService::createTcpDevice(const InstructionCallback& callback)
    {
        DriverManager* dm = manager();
        assert(dm);
        
        const Server& server = _server;
        string address = server.address;
        int port = server.port;
        int maxConnections = server.maxConnections;
        
        TcpInstructionSet* set = new TcpInstructionSet(this, callback.tcpInstructions);
        ChannelDescription* cd = new ChannelDescription("TcpServer", "TcpServerInteractive");
        DeviceDescription* dd = new DeviceDescription("TcpDevice", cd, set);
        dd->setReceiveTimeout(_server.timeout.receive);
        dd->setSendTimeout(_server.timeout.send);
        
        TcpServerChannelContext* tc = (TcpServerChannelContext*)cd->context();
        tc->setAddress(address);
        tc->setPort(port);
        tc->setMaxConnections(maxConnections);
        tc->setCloseTimeout(_server.timeout.close);
        tc->setSendBufferSize(_server.sendBufferSize);
        tc->setReceiveBufferSize(_server.receiveBufferSize);
        tc->setNoDelay(_server.noDelay);
        tc->setSyncSendReceive(false);
        tc->setReopened(false);
        dm->description()->addDevice(dd);
        dm->addPool(_instructionPool = new ServerSampler(dm, cd, dd));
        
        Trace::writeFormatLine(Resources::getString("CreateTcpServerChannelInfo").c_str(), dd->name().c_str(), tc->address().c_str(), tc->port());
    }
    void ServerService::createUdpServerDevice(const InstructionCallback& callback)
    {
        DriverManager* dm = manager();
        assert(dm);
        
        const Server& server = _server;
        
        UdpInstructionSet* set = new UdpInstructionSet(this, callback.udpInstructions);
        ChannelDescription* cd = new ChannelDescription("UdpServer", "UdpServerInteractive");
        DeviceDescription* dd = new DeviceDescription("UdpServerDevice", cd, set);
        UdpServerChannelContext* uc = (UdpServerChannelContext*)cd->context();
        
        uc->setPort(server.broadcast.receiveport);
        dm->description()->addDevice(dd);

		Trace::writeFormatLine("Create the UDP server ethernet channel: name: %s, port: %d",
			cd->name().c_str(), uc->port());
    }
    void ServerService::createUdpClientDevice(const InstructionCallback& callback)
    {
        DriverManager* dm = manager();
        assert(dm);
        
        const Server& server = _server;
        
        UdpInstructionSet* set = new UdpInstructionSet(this, callback.udpInstructions);
        ChannelDescription* cd = new ChannelDescription("UdpClient", "UdpInteractive");
        DeviceDescription* dd = new DeviceDescription("UdpClientDevice", cd, set);
        UdpChannelContext* uc = (UdpChannelContext*)cd->context();
        
        uc->setPort(server.broadcast.sendport);
        dm->description()->addDevice(dd);
        if(callback.udpSampler != NULL)
        {
            UdpSampler* sampler = new UdpSampler(dm, cd, dd, server.broadcast, callback.udpSampler);
            sampler->pause();
            _udpSampler = sampler;
            dm->addPool(sampler);
        }
        
        Trace::writeFormatLine("Create the UDP client ethernet channel: name: %s, port: %d",
                               cd->name().c_str(), uc->port());
    }
    void ServerService::createDevice(const InstructionCallback& callback)
    {
        if (!_server.enabled)
            return;
        
        createTcpDevice(callback);
        
        if(_server.broadcast.enabled && callback.udpInstructions != NULL)
        {
            // create udp device.
            createUdpServerDevice(callback);
            createUdpClientDevice(callback);
        }
    }
    
    bool ServerService::isSendSuccessfully(const string& instructionName, const InstructionContext* ic) const
    {
        return isSendSuccessfully(instructionName, ic, ic != NULL && !ic->hasException());
    }
    bool ServerService::isSendSuccessfully(const string& instructionName, const InstructionContext* ic, bool condition) const
    {
        if (condition)
        {
#if DEBUG
            Debug::writeFormatLine("Send an instraction '%s' successfully.", instructionName.c_str());
#endif
            return true;
        }
        else
        {
            string mess = Convert::convertStr(Resources::getString("FailedtoSendInstruction").c_str(),
                                              instructionName.c_str(), (ic != NULL && ic->hasException() ? ic->getException()->getMessage() : Resources::getString("CommunicationException").c_str()));
            
            Trace::writeLine(mess.c_str(), Trace::Error);
            return false;
        }
    }
}
