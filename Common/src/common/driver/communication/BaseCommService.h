#ifndef COMMON_BASECOMMSERVICE_H
#define COMMON_BASECOMMSERVICE_H

#include "common/thread/Thread.h"
#include "common/driver/instructions/InstructionContext.h"
#include "common/xml/Configuration.h"
#include "CommSampler.h"
#include "CommInstructionSet.h"

using namespace Common;

namespace Driver
{
    class DriverManager;
    class BaseCommService
    {
    public:
        struct InstructionCallback
        {
            instructions_callback tcpInstructions;
            sampler_callback tcpSampler;
            instructions_callback udpInstructions;
            sampler_callback udpSampler;
            
            InstructionCallback()
            {
                tcpInstructions = NULL;
                tcpSampler = NULL;
                udpInstructions = NULL;
                udpSampler = NULL;
            }
        };
        
        BaseCommService();
        virtual ~BaseCommService();
        
        inline const string& error() const
        {
            return _error;
        }
        
        bool initialize(const InstructionCallback& callback);
        bool unInitialize();
        
        void pauseUdpSampler();
        void resumeUdpSampler();
        
    protected:
        virtual void createDevice(const InstructionCallback& callback) = 0;
        
        DriverManager* manager();
        
    protected:
        string _error;
        
        InstructionPool* _instructionPool;
        InstructionPool* _udpSampler;
        
        DriverManager* _manager;
    };
}

#endif  // COMMON_BASECOMMSERVICE_H