//
//  BaseCommConfig.h
//  common
//
//  Created by baowei on 15/7/28.
//  Copyright (c) 2015 com. All rights reserved.
//

#ifndef common_BaseCommConfig_h
#define common_BaseCommConfig_h

#include "common/xml/Configuration.h"

using namespace Common;

namespace Driver
{
    struct Broadcast
    {
    public:
        TimeSpan interval;
        int count;
        int sendport;
        int receiveport;
        bool enabled;
        
        Broadcast();
        Broadcast(const Broadcast& value);
        
        void operator=(const Broadcast& value);
        bool operator==(const Broadcast& value) const;
        bool operator!=(const Broadcast& value) const;
    };
    
    struct Client
    {
    public:
        struct Connection
        {
        public:
            TimeSpan detectionInterval;
            TimeSpan resumeInterval;
            int detectionCount;
            
            Connection();
            Connection(const Connection& value);
            
            void operator=(const Connection& value);
            bool operator==(const Connection& value) const;
            bool operator!=(const Connection& value) const;
        };
        struct Timeout
        {
        public:
            TimeSpan send;
            TimeSpan receive;
            TimeSpan open;
            
            Timeout();
            Timeout(const Timeout& value);
            
            void operator=(const Timeout& value);
            bool operator==(const Timeout& value) const;
            bool operator!=(const Timeout& value) const;
        };
        
        string name;
        string address;
        Broadcast broadcast;
        int port;
        Connection connection;
        Timeout timeout;
        uint packetLength;		// unit: bytes
        uint sendBufferSize;
        uint receiveBufferSize;
        bool noDelay;
        bool enabled;
        
        Client();
        Client(const Client& value);
        
        bool isEmpty() const;
        
        void operator=(const Client& value);
        bool operator==(const Client& value) const;
        bool operator!=(const Client& value) const;
    };
    
    struct Server
    {
    public:
        struct Timeout
        {
        public:
            TimeSpan send;
            TimeSpan receive;
            TimeSpan close;
            
            Timeout();
            Timeout(const Timeout& value);
            
            void operator=(const Timeout& value);
            bool operator==(const Timeout& value) const;
            bool operator!=(const Timeout& value) const;
        };
        
        string name;
        string address;
        Broadcast broadcast;
        int port;
        int maxConnections;
        bool enabled;
        Timeout timeout;
        uint sendBufferSize;
        uint receiveBufferSize;
        bool noDelay;
        
        Server();
        Server(const Server& value);
        
        bool isEmpty() const;
        
        void operator=(const Server& value);
        bool operator==(const Server& value) const;
        bool operator!=(const Server& value) const;
    };
    
    class COMMON_EXPORT BaseCommConfig : public Configuration
    {
    public:
        BaseCommConfig(const ConfigFile& file);
        ~BaseCommConfig();
        
        const Server& server() const;
        const Client& client() const;
        
    public:
        static bool parseBufferLength(const string& str, uint& value);
        
    protected:
        void loadServerNode(XmlTextReader& reader);
        void loadClientNode(XmlTextReader& reader);
        
    protected:
        Server _server;
        Client _client;
    };
    
    class COMMON_EXPORT ServerConfig : public Configuration
    {
    public:
        ServerConfig(const ConfigFile& file);
        ~ServerConfig();
        
        const Server& server() const;
        
    public:
        static void loadServerNode(XmlTextReader& reader, Server& server);
        
    protected:
        void loadServerNode(XmlTextReader& reader);
        
    protected:
        Server _server;
    };
    
    class COMMON_EXPORT ClientConfig : public Configuration
    {
    public:
        ClientConfig(const ConfigFile& file);
        ~ClientConfig();
        
        const Client& client() const;        
        
    public:
        static void loadClientNode(XmlTextReader& reader, Client& client);
        
    protected:
        void loadClientNode(XmlTextReader& reader);
        
    protected:
        Client _client;
    };
}

#endif
