#include "common/diag/Trace.h"
#include "common/diag/Stopwatch.h"
#include "common/driver/DriverManager.h"
#include "common/driver/devices/DeviceDescription.h"
#include "common/driver/channels/ChannelDescription.h"
#include "common/Resources.h"
#include "BaseCommConfig.h"
#include "BaseCommService.h"

using namespace Common;
using namespace Driver;

namespace Driver
{
    BaseCommService::BaseCommService() : _instructionPool(NULL), _udpSampler(NULL)
    {
        _manager = new DriverManager();
    }
    
    BaseCommService::~BaseCommService()
    {
        unInitialize();
        delete _manager;
        _manager = NULL;
    }
    
    bool BaseCommService::initialize(const InstructionCallback& callback)
    {
#if DEBUG
        Stopwatch sw("Construct the BaseCommService instance.");
#endif
        
        try
        {
            createDevice(callback);

            _manager->open();
            
            return true;
        }
        catch (const Exception& e)
        {
            _error = e.getMessage();
            return false;
        }
    }
    bool BaseCommService::unInitialize()
    {
        if(_manager != NULL)
        {
            _manager->close();
            return true;
        }
        return false;
    }
    
    DriverManager* BaseCommService::manager()
    {
        return _manager;
    }
    
    void BaseCommService::pauseUdpSampler()
    {
        if(_udpSampler != NULL)
        {
            _udpSampler->pause();
        }
    }
    void BaseCommService::resumeUdpSampler()
    {
        if(_udpSampler != NULL)
        {
            _udpSampler->resume();
        }
    }
}
