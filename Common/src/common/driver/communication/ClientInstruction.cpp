#include "common/net/Dns.h"
#include "common/diag/Trace.h"
#include "common/diag/Debug.h"
#include "common/IO/MemoryStream.h"
#include "common/data/BCDUtilities.h"
#include "common/system/CheckUtilities.h"
#include "common/driver/devices/Device.h"
#include "common/driver/channels/Channel.h"
#include "common/driver/channels/TcpInteractive.h"
#include "common/driver/channels/TcpServerInteractive.h"
#include "common/driver/channels/UdpServerInteractive.h"
#include "common/driver/channels/TcpBackgroudReceiver.h"
#include "common/Resources.h"
#include "ClientInstruction.h"

using namespace Common;

namespace Driver
{
    bool isReceiveFinished(void* parameter)
    {
        if(parameter != NULL)
        {
            ClientInstruction* ci = (ClientInstruction*)parameter;
            return ci->isReceiveFinishedInner();
        }
        return false;
    }
    
    char ClientInstruction::_frameId = -1;
    ClientInstruction::ClientInstruction(InstructionDescription* id) : Instruction(id)
    {
        _currentFrameId = 0;
    }
    ClientInstruction::~ClientInstruction(void)
    {
    }
    InstructionContext* ClientInstruction::execute(Interactive* interactive, Device* device, InstructionContext* context, const ByteArray* buffer)
    {
        if(syncSendReceive(interactive))
        {
            return executeSync(interactive, device, context, buffer);
        }
        else
        {
            return executeAsync(interactive, device, context, buffer);
        }
    }
    InstructionContext* ClientInstruction::send(Interactive* interactive, Device* device, InstructionContext* context, byte frameId)
    {
        if (interactive == NULL)
        {
            return NULL;
        }
        
        Channel* channel = interactive->channel();
        if (channel == NULL)
        {
            return NULL;
        }
        ClientContext* ic = dynamic_cast<ClientContext*>(context);
        
        TcpServerInteractive* tsi = dynamic_cast<TcpServerInteractive*>(interactive);
        if(tsi != NULL)
        {
            tsi->setSentAddress(ic->sentAddr());
        }
        
        MemoryStream ms(128 * 1024);
        if (!getSendBuffer(ms, ic, frameId))
        {
            return NULL;
        }

        int sendLen = channel->send(ms.buffer());
        if(sendLen <= 0)
        {
            context->setExcetion(-1, "no_connect");
        }

        if(sendLen > 0)
        	log(ms.buffer(), sendLen, true);
        
        return context;
    }
    InstructionContext* ClientInstruction::receive(Interactive* interactive, Device* device, InstructionContext* context, const ByteArray* buffer)
    {
        if (interactive == NULL)
        {
            return NULL;
        }
        if(buffer == NULL)
        {
            return NULL;
        }
        
        Channel* channel = interactive->channel();
        if (channel == NULL)
        {
            return NULL;
        }
        
        if (buffer->count() < ClientContext::MinFrameLen)
        {
            return NULL;
        }
        
        log(buffer, false);
        
        ClientContext* ic = dynamic_cast<ClientContext*>(context);
        EthernetAddress* ea = dynamic_cast<EthernetAddress*>(interactive);
        if(ea != NULL)
        {
            ic->setPeerAddr(ea->peerAddr());
        }
        
        MemoryStream ms((ByteArray*)buffer, false);
        InstructionContext* result = setReceiveBuffer(ms, ic, false);

		if (result != NULL)
        {
            if(autoResponsed())
            {
                // send back.
                byte frameId = (*buffer)[ClientContext::FrameIdPosition];
                send(interactive, device, context, frameId);
            }
        }
        return result;
    }
    InstructionContext* ClientInstruction::receiveFromBuffer(Interactive* interactive, Device* device, InstructionContext* context)
    {
        if (interactive == NULL)
        {
            return NULL;
        }

        ByteArray buffer(128 * 1024);
        if(hasBackgroudReceiver(interactive))
        {
            ClientInstruction* ci = dynamic_cast<ClientInstruction*>(_receiveInstruction);
            assert(ci);
            uint timeout = device->description()->receiveTimeout();
            TickTimeout::msdelay(timeout, isReceiveFinished, ci);
            ci->copyReceiveBuffer(buffer);
        }
        else
        {
            if(!device->receive(&buffer))
                return NULL;
        }
        log(&buffer, false);
        
        MemoryStream ms(&buffer, false);
        ClientContext* result = setReceiveBuffer(ms, dynamic_cast<ClientContext*>(context));
        
        return result;
    }
    bool ClientInstruction::isReceiveFinishedInner()
    {
        Locker locker(&_backgroundBufferMutex);
        return _backgroundBuffer.count() > 0;
    }
    void ClientInstruction::copyReceiveBuffer(ByteArray& buffer)
    {
        Locker locker(&_backgroundBufferMutex);
        buffer.addRange(&_backgroundBuffer);
        _backgroundBuffer.clear();
    }
    InstructionContext* ClientInstruction::executeAsync(Interactive* interactive, Device* device, InstructionContext* context, const ByteArray* buffer)
    {
        if(buffer == NULL)      // sent
        {
            return send(interactive, device, context);
        }
        else                    // received.
        {
            return receive(interactive, device, context, buffer);
        }
    }
    InstructionContext* ClientInstruction::executeSync(Interactive* interactive, Device* device, InstructionContext* context, const ByteArray* buffer)
    {
        if(buffer == nullptr)
        {
            send(interactive, device, context);
        
            return receiveFromBuffer(interactive, device, context);
        }
        else
        {
            Locker locker(&_backgroundBufferMutex);
            _backgroundBuffer.clear();
            _backgroundBuffer.addRange(buffer);
            return context;
        }
    }
    bool ClientInstruction::getSendBuffer(MemoryStream& ms, ClientContext* context, byte frameId)
    {
        ms.writeByte(ClientContext::Header);
        ms.writeByte(frameId == 0xFF ? getFrameId() : frameId);
        const int lengthCount = ClientContext::BufferBCDLength;
        byte lengthBuffer[lengthCount];
        memset(lengthBuffer, 0, lengthCount);
        ms.write(lengthBuffer, 0, lengthCount);
        
        if (!getCommandBuffer(ms, context))
        {
            return false;
        }
        
        int64_t position = ms.position();
        BCDUtilities::Int64ToBCD(ms.length() - 2 - lengthCount + 2, lengthBuffer, lengthCount);
        ms.seek(ClientContext::LengthPosition);
        ms.write(lengthBuffer, 0, lengthCount);
        
        ushort crc16 = Crc16Utilities::checkByBit(ms.buffer()->data(), 1, (int)(ms.length() - 1));
        ms.seek(position);
        ms.writeUInt16(crc16);
        
        return true;
    }
    ClientContext* ClientInstruction::setReceiveBuffer(MemoryStream& ms, ClientContext* context, bool checkFrameId)
    {
        const ByteArray* buffer = ms.buffer();
        if (!match(buffer))
        {
            return NULL;
        }
        if (checkFrameId && !isSameFrameId((*buffer)[ClientContext::FrameIdPosition]))
        {
            return NULL;
        }
        if (context == NULL)
        {
            return NULL;
        }
        ms.seek(ClientContext::CommandPosition);
        if (setCommandBuffer(ms, context))
        {
            return context;
        }
        return NULL;
    }
    bool ClientInstruction::match(const ByteArray* buffer)
    {
        if(buffer == nullptr)
            return false;
        
        uint count = buffer->count();
        if (count == 0)
        {
            return false;
        }
        if (count < ClientContext::MinFrameLen)	// The shortest instruction.
        {
            return false;
        }
        if ((*buffer)[ClientContext::HeaderPosition] != ClientContext::Header)
        {
            return false;
        }
        if (command() != (*buffer)[ClientContext::CommandPosition])
        {
            return false;
        }
        ushort crc16 = Crc16Utilities::checkByBit(buffer->data(), 1, count - 1 - 2);
        if (!((*buffer)[count - 2] == ((crc16 >> 8) & 0xFF) &&
              (*buffer)[count - 1] == (crc16 & 0xFF)))
        {
            return false;
        }
        return true;
    }
    bool ClientInstruction::autoResponsed() const
    {
        return false;
    }
    bool ClientInstruction::syncSendReceive(Interactive* interactive) const
    {
        return !hasBackgroudReceiver(interactive);
    }
    bool ClientInstruction::hasBackgroudReceiver(Interactive* interactive) const
    {
        if (interactive == NULL)
        {
            return false;
        }
        Channel* channel = interactive->channel();
        if (channel == NULL)
        {
            return false;
        }
        
        ChannelDescription* cd = channel->description();
        TcpChannelBaseContext* tcc = dynamic_cast<TcpChannelBaseContext*>(cd->context());
        return tcc != NULL ? !tcc->syncSendReceive() : false;
    }
}
