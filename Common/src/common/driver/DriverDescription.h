#ifndef DRIVERDESCRIPTION_H
#define DRIVERDESCRIPTION_H

#include "common/common_global.h"
#include "../data/Vector.h"
#include "devices/DeviceDescription.h"

using namespace Common;

namespace Driver
{
	typedef Vector<DeviceDescription> DeviceDescriptions;

	class COMMON_EXPORT DriverDescription
	{
	public:
		DriverDescription(void)
		{
			_devices = new DeviceDescriptions();
		}
		~DriverDescription(void)
		{
			delete _devices;
		}

		inline DeviceDescriptions* getDevices() const
		{
			return _devices;
		}
		inline void addDevice(DeviceDescription* description) 
		{
			_devices->add(description);
		}
	private:
		DeviceDescriptions* _devices;
	};
}
#endif // DRIVERDESCRIPTION_H