#ifndef ESCAPEOPTION_H
#define ESCAPEOPTION_H

#include <string.h>
#include "common/common_global.h"
#include "../system/Convert.h"

using namespace Common;

namespace Driver
{
	struct EscapeOption
	{
		byte ToEscapeBuffer[8];
		int ToEscapeLength;
		byte EscapeBuffer[8];
		int EscapeLength;
		int Offset;
		int Length;

	public:
		EscapeOption()
		{
			memset(ToEscapeBuffer, 0, sizeof(ToEscapeBuffer));
			ToEscapeLength = 0;
			memset(EscapeBuffer, 0, sizeof(EscapeBuffer));
			EscapeLength = 0;
			Offset = 0;
			Length = 0;
		}
	};
}

#endif // ESCAPEOPTION_H
