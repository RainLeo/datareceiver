#include "Context.h"

namespace Driver
{
	Context::Context()
	{
		_data = new Datas();
	}

	Context::~Context()
	{
		delete _data;
		_data = NULL;
	}

	void Context::setValue(const string& name, ValueSet value)
	{
		_data->add(new Data(name, value));
	}

	ValueSet Context::getValue(const string& name)
	{
		Data* data = find(name);
		return data != NULL ? data->getValue() : ValueSet();
	}

	Data* Context::find(const string& name)
	{
		for (uint i = 0; i < _data->count(); i++)
		{
			Data *data=_data->at(i);
			if(data->name() == name)
			{
				return data;
			}
		}
		return NULL;
	}
}
