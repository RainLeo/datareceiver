#ifndef __LIBXML_CONFIG__
#define __LIBXML_CONFIG__

#ifdef WIN32

#define HAVE_CTYPE_H
#define HAVE_STDARG_H
#define HAVE_MALLOC_H
#define HAVE_ERRNO_H
#define SEND_ARG2_CAST
#define GETHOSTBYNAME_ARG_CAST

#if defined(_WIN32_WCE)
#undef HAVE_ERRNO_H
#include <windows.h>
#include "wincecompat.h"
#include <io.h>
#include <direct.h>
#else
#define HAVE_SYS_STAT_H
#define HAVE__STAT
#define HAVE_STAT
#define HAVE_STDLIB_H
#define HAVE_TIME_H
#define HAVE_FCNTL_H
#endif

#include <libxml/xmlversion.h>

#ifndef ICONV_CONST
#define ICONV_CONST const
#endif

#ifdef NEED_SOCKETS
#include "wsockcompat.h"
#endif

/*
 * Windows platforms may define except 
 */
#undef except

#define HAVE_ISINF
#define HAVE_ISNAN
#include <math.h>
#if defined(_MSC_VER) || defined(__BORLANDC__)
/* MS C-runtime has functions which can be used in order to determine if
   a given floating-point variable contains NaN, (+-)INF. These are 
   preferred, because floating-point technology is considered propriatary
   by MS and we can assume that their functions know more about their 
   oddities than we do. */
#include <float.h>
/* Bjorn Reese figured a quite nice construct for isinf() using the _fpclass
   function. */
#ifndef isinf
#define isinf(d) ((_fpclass(d) == _FPCLASS_PINF) ? 1 \
	: ((_fpclass(d) == _FPCLASS_NINF) ? -1 : 0))
#endif
/* _isnan(x) returns nonzero if (x == NaN) and zero otherwise. */
#ifndef isnan
#define isnan(d) (_isnan(d))
#endif
#else /* _MSC_VER */
#ifndef isinf
static int isinf (double d) {
    int expon = 0;
    double val = frexp (d, &expon);
    if (expon == 1025) {
        if (val == 0.5) {
            return 1;
        } else if (val == -0.5) {
            return -1;
        } else {
            return 0;
        }
    } else {
        return 0;
    }
}
#endif
#ifndef isnan
static int isnan (double d) {
    int expon = 0;
    double val = frexp (d, &expon);
    if (expon == 1025) {
        if (val == 0.5) {
            return 0;
        } else if (val == -0.5) {
            return 0;
        } else {
            return 1;
        }
    } else {
        return 0;
    }
}
#endif
#endif /* _MSC_VER */

#if defined(_MSC_VER)
#define mkdir(p,m) _mkdir(p)
#define snprintf _snprintf
#if _MSC_VER < 1500
#define vsnprintf(b,c,f,a) _vsnprintf(b,c,f,a)
#endif
#elif defined(__MINGW32__)
#define mkdir(p,m) _mkdir(p)
#endif

/* Threading API to use should be specified here for compatibility reasons.
   This is however best specified on the compiler's command-line. */
#if defined(LIBXML_THREAD_ENABLED)
#if !defined(HAVE_PTHREAD_H) && !defined(HAVE_WIN32_THREADS) && !defined(_WIN32_WCE)
#define HAVE_WIN32_THREADS
#endif
#endif

/* Some third-party libraries far from our control assume the following
   is defined, which it is not if we don't include windows.h. */
#if !defined(FALSE)
#define FALSE 0
#endif
#if !defined(TRUE)
#define TRUE (!(FALSE))
#endif

#if WIN32
#pragma warning(disable : 4996)
#pragma warning(disable : 4251)
#endif

#elif __APPLE__ || __ANDROID__
/* config.h generated manually for macos.  */

/* Define if you have the strftime function.  */
#define HAVE_STRFTIME

/* Define if you have the ANSI C header files.  */
#define STDC_HEADERS

#define PACKAGE
#define VERSION

#undef HAVE_LIBZ
#undef HAVE_LIBM
#undef HAVE_ISINF
#undef HAVE_ISNAN
#undef HAVE_LIBHISTORY
#undef HAVE_LIBREADLINE

#define XML_SOCKLEN_T socklen_t
#define HAVE_LIBPTHREAD
#define HAVE_PTHREAD_H
#define LIBXML_THREAD_ENABLED

/* Define if you have the _stat function.  */
#define HAVE__STAT

/* Define if you have the class function.  */
#undef HAVE_CLASS

/* Define if you have the finite function.  */
#undef HAVE_FINITE

/* Define if you have the fp_class function.  */
#undef HAVE_FP_CLASS

/* Define if you have the fpclass function.  */
#undef HAVE_FPCLASS

/* Define if you have the fprintf function.  */
#define HAVE_FPRINTF

/* Define if you have the isnand function.  */
#undef HAVE_ISNAND

/* Define if you have the localtime function.  */
#define HAVE_LOCALTIME

/* Define if you have the printf function.  */
#define HAVE_PRINTF

/* Define if you have the signal function.  */
#define HAVE_SIGNAL

/* Define if you have the snprintf function.  */
#define HAVE_SNPRINTF

/* Define if you have the sprintf function.  */
#define HAVE_SPRINTF

/* Define if you have the sscanf function.  */
#define HAVE_SSCANF

/* Define if you have the stat function.  */
#define HAVE_STAT

/* Define if you have the strdup function.  */
#define HAVE_STRDUP

/* Define if you have the strerror function.  */
#define HAVE_STRERROR

/* Define if you have the strftime function.  */
#define HAVE_STRFTIME

/* Define if you have the strndup function.  */
#define HAVE_STRNDUP

/* Define if you have the vfprintf function.  */
#define HAVE_VFPRINTF

/* Define if you have the vsnprintf function.  */
#define HAVE_VSNPRINTF

/* Define if you have the vsprintf function.  */
#define HAVE_VSPRINTF

#if TARGET_OS_MAC
/* Define if you have the <ansidecl.h> header file.  */
#define HAVE_ANSIDECL_H
#endif

/* Define if you have the <arpa/inet.h> header file.  */
#define HAVE_ARPA_INET_H

/* Define if you have the <ctype.h> header file.  */
#define HAVE_CTYPE_H

/* Define if you have the <dirent.h> header file.  */
#define HAVE_DIRENT_H

/* Define if you have the <dlfcn.h> header file.  */
#define HAVE_DLFCN_H

/* Define if you have the <errno.h> header file.  */
#define HAVE_ERRNO_H

/* Define if you have the <fcntl.h> header file.  */
#define HAVE_FCNTL_H

/* Define if you have the <float.h> header file.  */
#define HAVE_FLOAT_H

/* Define if you have the <fp_class.h> header file.  */
#define HAVE_FP_CLASS_H

/* Define if you have the <ieeefp.h> header file.  */
#define HAVE_IEEEFP_H

/* Define if you have the <malloc.h> header file.  */
#undef HAVE_MALLOC_H

/* Define if you have the <math.h> header file.  */
#define HAVE_MATH_H

/* Define if you have the <nan.h> header file.  */
#define HAVE_NAN_H

/* Define if you have the <ndir.h> header file.  */
#define HAVE_NDIR_H

/* Define if you have the <netdb.h> header file.  */
#define HAVE_NETDB_H

/* Define if you have the <netinet/in.h> header file.  */
#define HAVE_NETINET_IN_H

/* Define if you have the <signal.h> header file.  */
#define HAVE_SIGNAL_H

/* Define if you have the <stdarg.h> header file.  */
#define HAVE_STDARG_H

/* Define if you have the <stdlib.h> header file.  */
#define HAVE_STDLIB_H

/* Define if you have the <string.h> header file.  */
#define HAVE_STRING_H

/* Define if you have the <sys/dir.h> header file.  */
#define HAVE_SYS_DIR_H

/* Define if you have the <sys/mman.h> header file.  */
#undef HAVE_SYS_MMAN_H

/* Define if you have the <sys/ndir.h> header file.  */
#undef HAVE_SYS_NDIR_H

/* Define if you have the <sys/select.h> header file.  */
#define HAVE_SYS_SELECT_H

/* Define if you have the <sys/socket.h> header file.  */
#define HAVE_SYS_SOCKET_H

/* Define if you have the <sys/stat.h> header file.  */
#define HAVE_SYS_STAT_H

/* Define if you have the <sys/time.h> header file.  */
#define HAVE_SYS_TIME_H

/* Define if you have the <sys/types.h> header file.  */
#define HAVE_SYS_TYPES_H

/* Define if you have the <time.h> header file.  */
#define HAVE_TIME_H

/* Define if you have the <unistd.h> header file.  */
#define HAVE_UNISTD_H

/* Define if you have the <zlib.h> header file.  */
#undef HAVE_ZLIB_H

/* Name of package */
#define PACKAGE

/* Version number of package */
#define VERSION

/* Define if compiler has function prototypes */
#define PROTOTYPES

#define HAVE_VA_COPY

#define SEND_ARG2_CAST
#define GETHOSTBYNAME_ARG_CAST

#include <libxml/xmlversion.h>
#include <sys/types.h>
//#include <extra/stricmp.h>
//#include <extra/strdup.h>

#ifndef ICONV_CONST
#define ICONV_CONST const
#endif

#elif __linux__

/* config.h generated manually for macos.  */

/* Define if you have the strftime function.  */
#define HAVE_STRFTIME

/* Define if you have the ANSI C header files.  */
#define STDC_HEADERS

#define PACKAGE
#define VERSION

#undef HAVE_LIBZ
#undef HAVE_LIBM
#undef HAVE_ISINF
#undef HAVE_ISNAN
#undef HAVE_LIBHISTORY
#undef HAVE_LIBREADLINE

#define XML_SOCKLEN_T socklen_t
#define HAVE_LIBPTHREAD
#define HAVE_PTHREAD_H
#define LIBXML_THREAD_ENABLED

/* Define if you have the _stat function.  */
#define HAVE__STAT

/* Define if you have the class function.  */
#undef HAVE_CLASS

/* Define if you have the finite function.  */
#undef HAVE_FINITE

/* Define if you have the fp_class function.  */
#undef HAVE_FP_CLASS

/* Define if you have the fpclass function.  */
#undef HAVE_FPCLASS

/* Define if you have the fprintf function.  */
#define HAVE_FPRINTF

/* Define if you have the isnand function.  */
#undef HAVE_ISNAND

/* Define if you have the localtime function.  */
#define HAVE_LOCALTIME

/* Define if you have the printf function.  */
#define HAVE_PRINTF

/* Define if you have the signal function.  */
#define HAVE_SIGNAL

/* Define if you have the snprintf function.  */
#define HAVE_SNPRINTF

/* Define if you have the sprintf function.  */
#define HAVE_SPRINTF

/* Define if you have the sscanf function.  */
#define HAVE_SSCANF

/* Define if you have the stat function.  */
#define HAVE_STAT

/* Define if you have the strdup function.  */
#define HAVE_STRDUP

/* Define if you have the strerror function.  */
#define HAVE_STRERROR

/* Define if you have the strftime function.  */
#define HAVE_STRFTIME

/* Define if you have the strndup function.  */
#define HAVE_STRNDUP

/* Define if you have the vfprintf function.  */
#define HAVE_VFPRINTF

/* Define if you have the vsnprintf function.  */
#define HAVE_VSNPRINTF

/* Define if you have the vsprintf function.  */
#define HAVE_VSPRINTF

#if TARGET_OS_MAC
/* Define if you have the <ansidecl.h> header file.  */
#define HAVE_ANSIDECL_H
#endif

/* Define if you have the <arpa/inet.h> header file.  */
#define HAVE_ARPA_INET_H

/* Define if you have the <ctype.h> header file.  */
#define HAVE_CTYPE_H

/* Define if you have the <dirent.h> header file.  */
#define HAVE_DIRENT_H

/* Define if you have the <dlfcn.h> header file.  */
#define HAVE_DLFCN_H

/* Define if you have the <errno.h> header file.  */
#define HAVE_ERRNO_H

/* Define if you have the <fcntl.h> header file.  */
#define HAVE_FCNTL_H

/* Define if you have the <float.h> header file.  */
#define HAVE_FLOAT_H

/* Define if you have the <fp_class.h> header file.  */
#define HAVE_FP_CLASS_H

/* Define if you have the <ieeefp.h> header file.  */
#define HAVE_IEEEFP_H

/* Define if you have the <malloc.h> header file.  */
#undef HAVE_MALLOC_H

/* Define if you have the <math.h> header file.  */
#define HAVE_MATH_H

/* Define if you have the <nan.h> header file.  */
#define HAVE_NAN_H

/* Define if you have the <ndir.h> header file.  */
#define HAVE_NDIR_H

/* Define if you have the <netdb.h> header file.  */
#define HAVE_NETDB_H

/* Define if you have the <netinet/in.h> header file.  */
#define HAVE_NETINET_IN_H

/* Define if you have the <signal.h> header file.  */
#define HAVE_SIGNAL_H

/* Define if you have the <stdarg.h> header file.  */
#define HAVE_STDARG_H

/* Define if you have the <stdlib.h> header file.  */
#define HAVE_STDLIB_H

/* Define if you have the <string.h> header file.  */
#define HAVE_STRING_H

/* Define if you have the <sys/dir.h> header file.  */
#define HAVE_SYS_DIR_H

/* Define if you have the <sys/mman.h> header file.  */
#undef HAVE_SYS_MMAN_H

/* Define if you have the <sys/ndir.h> header file.  */
#undef HAVE_SYS_NDIR_H

/* Define if you have the <sys/select.h> header file.  */
#define HAVE_SYS_SELECT_H

/* Define if you have the <sys/socket.h> header file.  */
#define HAVE_SYS_SOCKET_H

/* Define if you have the <sys/stat.h> header file.  */
#define HAVE_SYS_STAT_H

/* Define if you have the <sys/time.h> header file.  */
#define HAVE_SYS_TIME_H

/* Define if you have the <sys/types.h> header file.  */
#define HAVE_SYS_TYPES_H

/* Define if you have the <time.h> header file.  */
#define HAVE_TIME_H

/* Define if you have the <unistd.h> header file.  */
#define HAVE_UNISTD_H

/* Define if you have the <zlib.h> header file.  */
#undef HAVE_ZLIB_H

/* Name of package */
#define PACKAGE

/* Version number of package */
#define VERSION

/* Define if compiler has function prototypes */
#define PROTOTYPES

#define HAVE_VA_COPY

#define SEND_ARG2_CAST
#define GETHOSTBYNAME_ARG_CAST

#include <libxml/xmlversion.h>
#include <sys/types.h>
//#include <extra/stricmp.h>
//#include <extra/strdup.h>

#ifndef ICONV_CONST
#define ICONV_CONST const
#endif

#define HAVE_INTTYPES_H

#else

/* config.h.in.  Generated from configure.ac by autoheader.  */

/* Type cast for the gethostbyname() argument */
#undef GETHOSTBYNAME_ARG_CAST

/* Define to 1 if you have the <ansidecl.h> header file. */
#undef HAVE_ANSIDECL_H

/* Define to 1 if you have the <arpa/inet.h> header file. */
#undef HAVE_ARPA_INET_H

/* Define to 1 if you have the <arpa/nameser.h> header file. */
#undef HAVE_ARPA_NAMESER_H

/* Whether struct sockaddr::__ss_family exists */
#undef HAVE_BROKEN_SS_FAMILY

/* Define to 1 if you have the `class' function. */
#undef HAVE_CLASS

/* Define to 1 if you have the <ctype.h> header file. */
#undef HAVE_CTYPE_H

/* Define to 1 if you have the <dirent.h> header file. */
#undef HAVE_DIRENT_H

/* Define to 1 if you have the <dlfcn.h> header file. */
#undef HAVE_DLFCN_H

/* Have dlopen based dso */
#undef HAVE_DLOPEN

/* Define to 1 if you have the <dl.h> header file. */
#undef HAVE_DL_H

/* Define to 1 if you have the <errno.h> header file. */
#undef HAVE_ERRNO_H

/* Define to 1 if you have the <fcntl.h> header file. */
#undef HAVE_FCNTL_H

/* Define to 1 if you have the `finite' function. */
#undef HAVE_FINITE

/* Define to 1 if you have the <float.h> header file. */
#undef HAVE_FLOAT_H

/* Define to 1 if you have the `fpclass' function. */
#undef HAVE_FPCLASS

/* Define to 1 if you have the `fprintf' function. */
#undef HAVE_FPRINTF

/* Define to 1 if you have the `fp_class' function. */
#undef HAVE_FP_CLASS

/* Define to 1 if you have the <fp_class.h> header file. */
#undef HAVE_FP_CLASS_H

/* Define to 1 if you have the `ftime' function. */
#undef HAVE_FTIME

/* Define if getaddrinfo is there */
#undef HAVE_GETADDRINFO

/* Define to 1 if you have the `gettimeofday' function. */
#undef HAVE_GETTIMEOFDAY

/* Define to 1 if you have the <ieeefp.h> header file. */
#undef HAVE_IEEEFP_H

/* Define to 1 if you have the <inttypes.h> header file. */
#undef HAVE_INTTYPES_H

/* Define to 1 if you have the `isascii' function. */
#undef HAVE_ISASCII

/* Define if isinf is there */
#undef HAVE_ISINF

/* Define if isnan is there */
#undef HAVE_ISNAN

/* Define to 1 if you have the `isnand' function. */
#undef HAVE_ISNAND

/* Define if history library is there (-lhistory) */
#undef HAVE_LIBHISTORY

/* Have compression library */
#undef HAVE_LIBLZMA

/* Define if pthread library is there (-lpthread) */
#undef HAVE_LIBPTHREAD

/* Define if readline library is there (-lreadline) */
#undef HAVE_LIBREADLINE

/* Have compression library */
#undef HAVE_LIBZ

/* Define to 1 if you have the <limits.h> header file. */
#undef HAVE_LIMITS_H

/* Define to 1 if you have the `localtime' function. */
#undef HAVE_LOCALTIME

/* Define to 1 if you have the <lzma.h> header file. */
#undef HAVE_LZMA_H

/* Define to 1 if you have the <malloc.h> header file. */
#undef HAVE_MALLOC_H

/* Define to 1 if you have the <math.h> header file. */
#undef HAVE_MATH_H

/* Define to 1 if you have the <memory.h> header file. */
#undef HAVE_MEMORY_H

/* Define to 1 if you have the `mmap' function. */
#undef HAVE_MMAP

/* Define to 1 if you have the `munmap' function. */
#undef HAVE_MUNMAP

/* mmap() is no good without munmap() */
#if defined(HAVE_MMAP) && !defined(HAVE_MUNMAP)
#  undef /**/ HAVE_MMAP
#endif

/* Define to 1 if you have the <nan.h> header file. */
#undef HAVE_NAN_H

/* Define to 1 if you have the <ndir.h> header file, and it defines `DIR'. */
#undef HAVE_NDIR_H

/* Define to 1 if you have the <netdb.h> header file. */
#undef HAVE_NETDB_H

/* Define to 1 if you have the <netinet/in.h> header file. */
#undef HAVE_NETINET_IN_H

/* Define to 1 if you have the <poll.h> header file. */
#undef HAVE_POLL_H

/* Define to 1 if you have the `printf' function. */
#undef HAVE_PRINTF

/* Define if <pthread.h> is there */
#undef HAVE_PTHREAD_H

/* Define to 1 if you have the `putenv' function. */
#undef HAVE_PUTENV

/* Define to 1 if you have the `rand' function. */
#undef HAVE_RAND

/* Define to 1 if you have the `rand_r' function. */
#undef HAVE_RAND_R

/* Define to 1 if you have the <resolv.h> header file. */
#undef HAVE_RESOLV_H

/* Have shl_load based dso */
#undef HAVE_SHLLOAD

/* Define to 1 if you have the `signal' function. */
#undef HAVE_SIGNAL

/* Define to 1 if you have the <signal.h> header file. */
#undef HAVE_SIGNAL_H

/* Define to 1 if you have the `snprintf' function. */
#undef HAVE_SNPRINTF

/* Define to 1 if you have the `sprintf' function. */
#undef HAVE_SPRINTF

/* Define to 1 if you have the `srand' function. */
#undef HAVE_SRAND

/* Define to 1 if you have the `sscanf' function. */
#undef HAVE_SSCANF

/* Define to 1 if you have the `stat' function. */
#undef HAVE_STAT

/* Define to 1 if you have the <stdarg.h> header file. */
#undef HAVE_STDARG_H

/* Define to 1 if you have the <stdint.h> header file. */
#undef HAVE_STDINT_H

/* Define to 1 if you have the <stdlib.h> header file. */
#undef HAVE_STDLIB_H

/* Define to 1 if you have the `strdup' function. */
#undef HAVE_STRDUP

/* Define to 1 if you have the `strerror' function. */
#undef HAVE_STRERROR

/* Define to 1 if you have the `strftime' function. */
#undef HAVE_STRFTIME

/* Define to 1 if you have the <strings.h> header file. */
#undef HAVE_STRINGS_H

/* Define to 1 if you have the <string.h> header file. */
#undef HAVE_STRING_H

/* Define to 1 if you have the `strndup' function. */
#undef HAVE_STRNDUP

/* Define to 1 if you have the <sys/dir.h> header file, and it defines `DIR'.
   */
#undef HAVE_SYS_DIR_H

/* Define to 1 if you have the <sys/mman.h> header file. */
#undef HAVE_SYS_MMAN_H

/* Define to 1 if you have the <sys/ndir.h> header file, and it defines `DIR'.
   */
#undef HAVE_SYS_NDIR_H

/* Define to 1 if you have the <sys/select.h> header file. */
#undef HAVE_SYS_SELECT_H

/* Define to 1 if you have the <sys/socket.h> header file. */
#undef HAVE_SYS_SOCKET_H

/* Define to 1 if you have the <sys/stat.h> header file. */
#undef HAVE_SYS_STAT_H

/* Define to 1 if you have the <sys/timeb.h> header file. */
#undef HAVE_SYS_TIMEB_H

/* Define to 1 if you have the <sys/time.h> header file. */
#undef HAVE_SYS_TIME_H

/* Define to 1 if you have the <sys/types.h> header file. */
#undef HAVE_SYS_TYPES_H

/* Define to 1 if you have the `time' function. */
#undef HAVE_TIME

/* Define to 1 if you have the <time.h> header file. */
#undef HAVE_TIME_H

/* Define to 1 if you have the <unistd.h> header file. */
#undef HAVE_UNISTD_H

/* Whether va_copy() is available */
#undef HAVE_VA_COPY

/* Define to 1 if you have the `vfprintf' function. */
#undef HAVE_VFPRINTF

/* Define to 1 if you have the `vsnprintf' function. */
#undef HAVE_VSNPRINTF

/* Define to 1 if you have the `vsprintf' function. */
#undef HAVE_VSPRINTF

/* Define to 1 if you have the <zlib.h> header file. */
#undef HAVE_ZLIB_H

/* Define to 1 if you have the `_stat' function. */
#undef HAVE__STAT

/* Whether __va_copy() is available */
#undef HAVE___VA_COPY

/* Define as const if the declaration of iconv() needs const. */
#undef ICONV_CONST

/* Define to the sub-directory in which libtool stores uninstalled libraries.
   */
#undef LT_OBJDIR

/* Name of package */
#undef PACKAGE

/* Define to the address where bug reports for this package should be sent. */
#undef PACKAGE_BUGREPORT

/* Define to the full name of this package. */
#undef PACKAGE_NAME

/* Define to the full name and version of this package. */
#undef PACKAGE_STRING

/* Define to the one symbol short name of this package. */
#undef PACKAGE_TARNAME

/* Define to the home page for this package. */
#undef PACKAGE_URL

/* Define to the version of this package. */
#undef PACKAGE_VERSION

/* Type cast for the send() function 2nd arg */
#undef SEND_ARG2_CAST

/* Define to 1 if you have the ANSI C header files. */
#undef STDC_HEADERS

/* Support for IPv6 */
#undef SUPPORT_IP6

/* Define if va_list is an array type */
#undef VA_LIST_IS_ARRAY

/* Version number of package */
#undef VERSION

/* Determine what socket length (socklen_t) data type is */
#undef XML_SOCKLEN_T

/* Define for Solaris 2.5.1 so the uint32_t typedef from <sys/synch.h>,
   <pthread.h>, or <semaphore.h> is not used. If the typedef were allowed, the
   #define below would cause a syntax error. */
#undef _UINT32_T

/* Using the Win32 Socket implementation */
#undef _WINSOCKAPI_

/* ss_family is not defined here, use __ss_family instead */
#undef ss_family

/* Define to the type of an unsigned integer type of width exactly 32 bits if
   such a type exists and the standard includes do not define it. */
#undef uint32_t


#endif


#endif /* __LIBXML_CONFIG__ */

