#pragma once

#include "interface.h"
#include <list>
#include "common/thread/Locker.h"
#include "common/IO/File.h"
#include "common/system/Singleton.h"
#include "common/thread/Timer.h"
#include "Event.h"

using namespace std;
using namespace Common;

typedef bool (*SaveFunc)(const TagValueReq &);

class TagValueCache
{
public:
    const static int MAX_TAG_COUNT = 50;
    int MAX_FILE_COUNT = 7200 / (3 * MAX_TAG_COUNT); // 2 hours period in 3 sec interval
    TagValueCache();
    ~TagValueCache();

public:
    void WriteTags(const TagValueReq & tagValueReq);
    static TagValueCache * Instance() { return Singleton<TagValueCache>::instance(); }
    void ReconnectProc();
    void SetSaveFunc(SaveFunc saveFunc, bool sync, uint cacheBufferSizeOffset = 0) { m_saveFunc = saveFunc; m_saveFuncSync = sync; m_cacheBufferSizeOffset = cacheBufferSizeOffset; }
    void SortFiles(StringArray &files);
    void SaveFuncSucceed(byte reqNo);
    void SetConfig(int sendTimeout, int sendInterval, int countLimit);

private:
    void ClearCache();
    bool secureSaveFunc(TagValueReq &);

private:
    DECLARE_SINGLETON_CLASS(TagValueCache);
    list<TagValueReq *> m_lstTagValue;
    string m_cacheFilesDir;
    Stream* m_stream;
    mutex m_streamMutex;
    Timer *m_pReconnectTimer;
    Thread *m_pThread;
    SaveFunc m_saveFunc;
    bool m_bIsCached;
    event_handle hEvent;
    bool m_saveFuncSync;
    uint m_cacheBufferSizeOffset;
    byte m_reqNo; // used to match req & response
    int m_sendTimeout;
    int m_sendInterval;
};