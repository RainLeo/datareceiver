#pragma once
#include "common/system/DateTime.h"
#include "common/driver/communication/BaseCommConfig.h"
#include "common/driver/communication/ServerService.h"
#include "common/diag/LogTraceListener.h"
#include "common/diag/Trace.h"
using namespace Common;
using namespace Driver;

class ServerCommConfig : public ServerConfig
{
public:
    ServerCommConfig(const ConfigFile& file, string root);
    int m_TagCacheCountLimit;

    struct WebServer
    {
    public:
        string Name;
        string Address;
        TimeSpan SendTimeout;
        TimeSpan ReceiveTimeout;
        int Port;
        int MaxConnections;
        bool Enabled;

        WebServer()
        {
            Name = "";
            Address = "";
            Port = 8080;
            MaxConnections = 100;
            Enabled = false;
            SendTimeout = TimeSpan(0, 0, 5);
            ReceiveTimeout = TimeSpan(0, 0, 5);
        }
    };
    struct WebClient
    {
    public:
        string Name;
        string EndPoint;
        TimeSpan Interval;
        bool Enabled;
        TimeSpan SendTimeout;
        TimeSpan ReceiveTimeout;
        TimeSpan ConnectTimeout;

        WebClient()
        {
            Name = "";
            EndPoint = "";
            Interval = TimeSpan(0, 0, 10);
            Enabled = false;
            SendTimeout = TimeSpan(0, 0, 5);
            ReceiveTimeout = TimeSpan(0, 0, 5);
            ConnectTimeout = TimeSpan(0, 0, 10);
        }
    };
    const WebServer& webserver() const
    {
        return _webserver;
    }
    const WebClient& webclient() const
    {
        return _webclient;
    }

protected:
    bool load(XmlTextReader& reader);
    string m_root;
    LogTraceConfig _log;
    WebServer _webserver;
    WebClient _webclient;
};
