#ifndef EDATA_H
#define EDATA_H

enum EData
{
	ED_None=0,
	ED_Short=2,
	ED_Long=3,
	ED_Float=4,
	ED_Double=5,
	ED_Currency=6,
	ED_Date=7,
	ED_String=8,
	ED_Unknown=10,
	ED_Boolean=11,
	ED_Character=16,
	ED_Byte=17,
	ED_Word=18,
	ED_Dword=19
};

#endif
