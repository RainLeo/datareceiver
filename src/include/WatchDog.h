#pragma once
#include "common/system/Singleton.h"
#include "common/thread/Timer.h"

using namespace Common;

class CWatchDog
{
public:
	CWatchDog();
	~CWatchDog();
public:
	void Start(void);
public:
	void Feed(void);
    static CWatchDog * Instance() { return Singleton<CWatchDog>::instance(); }
private:
    DECLARE_SINGLETON_CLASS(CWatchDog);
    Timer *m_pTimerFeed;
	int m_fd;
};

