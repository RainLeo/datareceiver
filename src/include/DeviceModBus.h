#pragma once
#include "DeviceConfiguration.h"
#include "common/xml/XmlTextReader.h"

class CDeviceSerialDetail : public CDeviceDetail
{
public:
    byte communicationType;
    string dtuId;
    byte serialIndex;
    int serialBaudRate;
    byte serialCharacterSize;
    byte serialStopBits;
    byte serialParity;

    enum
    {
        COMMUNICATION_TYPE_SERIAL = 1,
        COMMUNICATION_TYPE_TCP,
        COMMUNICATION_TYPE_DTU,
        COMMUNICATION_TYPE_DTU_HONGDIAN,
        COMMUNICATION_TYPE_DTU_LICHUANG,
    };

    CDeviceSerialDetail() { communicationType = 0; dtuId = ""; serialIndex = 0; serialBaudRate = 9600; serialCharacterSize = 8; serialStopBits = 1; serialParity = 0; }
    void write(Stream* stream) const;
    void read(Stream* stream);
    virtual CDeviceDetail* Clone() = 0;
    virtual void parse(XmlTextReader &reader);

    byte parseCommunicationType(string type)
    {
        if (Convert::stringEqual(type, "serial", true))
        {
            communicationType = COMMUNICATION_TYPE_SERIAL;
        }
        else if (Convert::stringEqual(type, "tcp", true))
        {
            communicationType = COMMUNICATION_TYPE_TCP;
        }
        else if (Convert::stringEqual(type, "dtu", true))
        {
            communicationType = COMMUNICATION_TYPE_DTU;
        }
        else if (Convert::stringEqual(type, "hongdian_dtu", true))
        {
            communicationType = COMMUNICATION_TYPE_DTU_HONGDIAN;
        }
        else if (Convert::stringEqual(type, "lichuang_dtu", true))
        {
            communicationType = COMMUNICATION_TYPE_DTU_LICHUANG;
        }

        return communicationType;
    }

    string communicationTypeStr()
    {
        switch (communicationType)
        {
        case COMMUNICATION_TYPE_SERIAL:
            return "serial";
        case COMMUNICATION_TYPE_TCP:
            return "tcp";
        case COMMUNICATION_TYPE_DTU:
        case COMMUNICATION_TYPE_DTU_HONGDIAN:
        case COMMUNICATION_TYPE_DTU_LICHUANG:
            return "dtu";
        default:
            return "";
        }
    }

};

class CDeviceModBusDetail : public CDeviceSerialDetail
{
public:
    byte address;
    byte mode;

    CDeviceModBusDetail() { address = 0; mode = 0; }
    virtual CDeviceDetail* Clone()
    {
        CDeviceModBusDetail *pDevConfig = new CDeviceModBusDetail();
        *pDevConfig = *(CDeviceModBusDetail *)this;

        return pDevConfig;
    }
    void write(Stream* stream) const;
    void read(Stream* stream);
    virtual void parse(XmlTextReader &reader);

    byte parseMode(string modeStr)
    {
        if (Convert::stringEqual(modeStr, "rtu", true))
        {
            mode = 1;
        }
        else if (Convert::stringEqual(modeStr, "ascii", true))
        {
            mode = 2;
        }

        return mode;
    }

    string modeStr()
    {
        switch (mode)
        {
        case 1:
            return "rtu";
        case 2:
            return "ascii";
        default:
            return "";
        }
    }
};

class CDeviceModBus : public CDeviceConfiguration
{
public:
    CDeviceModBus() { type = DEVICE_MODBUS; pDetail = createDetail(); }
    ~CDeviceModBus() {};

    CDeviceDetail * createDetail() { return new CDeviceModBusDetail(); }
};

