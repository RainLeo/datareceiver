#pragma once

#include "common/driver/channels/Interactive.h"
#include "common/driver/channels/ChannelContext.h"
#include "common/driver/channels/Channel.h"
#include "common/driver/devices/Device.h"
#include "common/driver/DriverManager.h"
#include "common/Resources.h"
#include "common/diag/Stopwatch.h"
#include "common/thread/Locker.h"
#include "common/thread/Thread.h"
#include "common/diag/Debug.h"
#include "common/diag/Trace.h"
#include "common/driver/communication/CommInstructionSet.h"
#include <list>

using namespace Driver;
using namespace std;

class DualPortsRamChannelContext : public ChannelContext
{
public:
    DualPortsRamChannelContext(int portIndex) : m_portIndex(portIndex) {}
    ~DualPortsRamChannelContext() {}

    int getPortIndex() { return m_portIndex; }

private:
    int m_portIndex;
};

class DualPortsRamInteractive : public Interactive, public BackgroudReceiver
{
public:
    DualPortsRamInteractive(DriverManager* dm, Channel* channel = NULL);
    ~DualPortsRamInteractive(void);

    bool open() override;
    void close() override;

    bool connected() override;
    int available() override;

    int send(byte* buffer, int offset, int count) override;
    int receive(byte* buffer, int offset, int count) override;
    bool ready();
    void sendBuffer();
    void sendBuffer(byte *buffer, int nTotalLen, int nCurLen);
    void receiveProcInner();

private:
    //friend void DualPortsRam_receiveProc(void* parameter);
    inline DualPortsRamChannelContext* getChannelContext()
    {
        return (DualPortsRamChannelContext*)(_channel->description()->context());
    }

private:
    Thread* _receiveThread;
    Device* _device;
#ifdef FILE_STUB
    FILE *m_file_descriptor;
#else
    int m_file_descriptor;
#endif
    mutex m_mutexFile;
    mutex m_mutexSendList;
    int m_port_index;
    list<ByteArray *> m_lstBuffer;
};

class DualPortsRamInstructionSet : public InstructionSet
{
public:
    DualPortsRamInstructionSet(void* owner, instructions_callback action);
    ~DualPortsRamInstructionSet();

    void generateInstructions(Instructions* instructions);

    bool receive(Device* device, Channel* channel, ByteArray* buffer);

    InstructionSet* clone() const
    {
        return new DualPortsRamInstructionSet(_owner, _instructionsCallback);
    }

private:
    instructions_callback _instructionsCallback;
    void* _owner;
};