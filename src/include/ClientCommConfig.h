#pragma once
#include "common/system/DateTime.h"
#include "common/driver/communication/BaseCommConfig.h"
#include "common/driver/communication/ClientService.h"
#include "common/diag/LogTraceListener.h"
#include "common/diag/Trace.h"
#include <vector>

using namespace std;
using namespace Common;
using namespace Driver;

class ClientCommConfig : public ClientConfig
{
public:
    ClientCommConfig(const ConfigFile& file, string root);
    string data_file_path;
    string configuration_file_path;
    int m_TagCacheSendInterval;
    int m_TagCacheSendTimeout;
    int m_TagCacheCountLimit;
    int m_TagFetchInterval;
    int m_TagReceiveTimeout;
    int m_MaintainInterval;
    vector<int> m_dtuPorts;
    vector<string> m_dtuVendors;

protected:
    bool load(XmlTextReader& reader);
    string m_root;
    LogTraceConfig _log;
};

