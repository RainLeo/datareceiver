/***************************************************************************************************
 * Definition of the interface between receiver and reporter, reporter and gatherer
 * Server* indicates the message sent from the client to server
 *
 *
 ****************************************************************************************************/
#ifndef INTERFACE_H
#define INTERFACE_H

#include <vector>
#include "EData.h"
#include "EStatus.h"
#include "common/system/DateTime.h"
#include "common/driver/communication/ServerService.h"
#include "common/driver/communication/ClientService.h"
#include "DeviceConfiguration.h"
#include "TagConfiguration.h"

using namespace Common;
using namespace Driver;

extern Version AppVersion;
struct Alarm;
extern vector<Alarm> GetAlarms();

enum
{
    PROGRAM_ID_GATHERER = 1,
    PROGRAM_ID_REPORTER,
    PROGRAM_ID_RECEIVER
};

struct Alarm
{
    byte programId;
    int alarm;
    static const int ALARM_HIGH_CPU_THRESHOLD = 70;
    static const int ALARM_CHECK_TIMES = 300 / 3; // 5 min and 3 sec internal
    static bool m_sbHighCpu;
    static int m_iCount;

    enum
    {
        ALARM_RESTART = -1, // added by HYC only used for re-sending tag configuration
        ALARM_NONE = 0,
        ALARM_INSUFFICIENT_DISK = 1,
        ALARM_HIGH_CPU = 2,
    };

    static void CheckHighCpu(int cpu);
    static bool IsHighCpu();
};

struct HeartbeatReq
{
public:
	Version version;
    vector<Alarm> vecAlarms;

	HeartbeatReq()
	{
		version = AppVersion;
        vecAlarms = GetAlarms();
	}

	void write(Stream* stream) const
	{
		version.writeBCDByte(stream);
		stream->writeInt16(vecAlarms.size());
        for (uint i = 0; i < vecAlarms.size(); i++)
        {
            const Alarm& alarm = vecAlarms[i];
            stream->writeByte(alarm.programId);
            stream->writeInt32(alarm.alarm);
        }
	}
	void read(Stream* stream)
	{
		version.readBCDByte(stream);
        vecAlarms.clear();
		short int alarmCount = stream->readInt16();
        for (int i = 0; i < alarmCount; i++)
        {
            Alarm alarm;
            alarm.programId = stream->readByte();
            alarm.alarm = stream->readInt32();
            vecAlarms.push_back(alarm);
        }
    }
	void copyFrom(const HeartbeatReq* value)
	{
		this->version = value->version;
		this->vecAlarms = value->vecAlarms;
	}
};
struct Empty
{
	void write(Stream* stream) const
	{
	}
	void read(Stream* stream)
	{
	}
	void copyFrom(const Empty* value)
	{
	}
};

struct HeartbeatResp
{
public:
	DateTime curtime;

	HeartbeatResp()
	{
		curtime = DateTime::now();
	}

	void write(Stream* stream) const
	{
		curtime.writeBCDDateTime(stream);
	}
	void read(Stream* stream)
	{
		curtime.readBCDDateTime(stream);
	}
	void copyFrom(const HeartbeatResp* value)
	{
		this->curtime = value->curtime;
	}
};

class ServerHeartbeatInstruction : public ServerElementInstruction<HeartbeatReq, HeartbeatResp>
{
public:
	ServerHeartbeatInstruction(InstructionDescription* id) : ServerElementInstruction<HeartbeatReq, HeartbeatResp>(id)
	{
	}
	~ServerHeartbeatInstruction()
	{
	}
	byte command() const
	{
		return 0x11;
	}

	bool allowLog() const override
	{
		return false;
	}

	bool autoResponsed() const
	{
		return true;
	}

	bool syncSendReceive(Interactive*) const override
	{
		return false;
	}

	ClientContext* setValue(ClientContext* context);
};
class HeartbeatInstruction : public ElementInstruction<HeartbeatReq, HeartbeatResp>
{
public:
	HeartbeatInstruction(InstructionDescription* id) : ElementInstruction<HeartbeatReq, HeartbeatResp>(id)
	{
	}
	~HeartbeatInstruction()
	{
	}
	byte command() const
	{
		return 0x11;
	}

	bool allowLog() const override
	{
		return false;
	}

    bool syncSendReceive(Interactive*) const override
    {
        return false;
    }

	ClientContext* setValue(ClientContext* context);
};

class HeartbeatContext : public ElementContext<HeartbeatReq, HeartbeatResp>
{
};

struct TagConfigurationResp
{
	byte status;
	TagConfigurationResp()
	{
		status = 0;
	}
	void write(Stream* stream) const
	{
		stream->writeByte(status);
	}
	void read(Stream* stream)
	{
		status = stream->readByte();
	}
	void copyFrom(const TagConfigurationResp* value)
	{
		status = value->status;
	}
};

class TagConfigurationInstruction : public ElementInstruction<TagConfiguration, TagConfigurationResp>
{
public:
	TagConfigurationInstruction(InstructionDescription* id) : ElementInstruction<TagConfiguration, TagConfigurationResp>(id)
	{
	}
	~TagConfigurationInstruction()
	{
	}
	byte command() const
	{
		return 0x21;
	}

	bool allowLog() const override
	{
		return false;
	}

    bool syncSendReceive(Interactive*) const override
    {
        return false;
    }

	ClientContext* setValue(ClientContext* context);
};


class ServerTagConfigurationInstruction : public ServerElementInstruction<TagConfiguration, TagConfigurationResp>
{
public:
	ServerTagConfigurationInstruction(InstructionDescription* id) : ServerElementInstruction<TagConfiguration, TagConfigurationResp>(id)
	{
	}
	~ServerTagConfigurationInstruction()
	{
	}

	byte command() const
	{
		return 0x21;
	}

	bool allowLog() const override
	{
		return false;
	}
	bool autoResponsed() const
	{
		return true;
	}
	ClientContext* setValue(ClientContext* context);
};

class TagConfigurationContext : public ElementContext<TagConfiguration, TagConfigurationResp>
{
};

// Upload Tag Value 0x22
struct TagValueReq
{
    byte reqNo;
    vector<TagValue *> tagValues;
    TagValueReq()
    {
        reqNo = 0;
    }
    ~TagValueReq()
    {
        Clear();
    }

    void Clear()
    {
        reqNo = 0;
        for (vector<TagValue *>::iterator iter = tagValues.begin(); iter != tagValues.end(); iter++)
        {
            if (*iter != NULL)
            {
                delete *iter;
                *iter = NULL;
            }
        }
        tagValues.clear();
    }
    void write(Stream* stream) const
    {
        stream->writeByte(reqNo);
        stream->writeInt16(tagValues.size());
        for (vector<TagValue *>::const_iterator ci = tagValues.begin(); ci != tagValues.end(); ci++)
        {
            (*ci)->write(stream);
        }
    }
    void read(Stream* stream)
    {
        Clear();
        reqNo = stream->readByte();
        short count = stream->readInt16();
        for (int i = 0; i < count; i++)
        {
            TagValue *pTag = new TagValue();
            pTag->read(stream);
            tagValues.push_back(pTag);
        }
    }
    void copyFrom(const TagValueReq* value)
    {
        Clear();
        reqNo = value->reqNo;
        for (vector<TagValue *>::const_iterator ci = value->tagValues.begin(); ci != value->tagValues.end(); ci++)
        {
            TagValue *pTag = new TagValue();
            pTag->copyFrom(*ci);
            tagValues.push_back(pTag);
        }
    }
};

typedef TagConfigurationResp TagValueResp;

class TagValueInstruction : public ElementInstruction<TagValueReq, TagValueResp>
{
public:
	TagValueInstruction(InstructionDescription* id) : ElementInstruction<TagValueReq, TagValueResp>(id)
	{
	}
	~TagValueInstruction()
	{
	}
	byte command() const
	{
		return 0x22;
	}

	bool allowLog() const override
	{
		return false;
	}

    bool syncSendReceive(Interactive*) const override
    {
        return false;
    }

	ClientContext* setValue(ClientContext* context);
};

class ServerTagValueInstruction : public ServerElementInstruction<TagValueReq, TagValueResp>
{
public:
	ServerTagValueInstruction(InstructionDescription* id) : ServerElementInstruction<TagValueReq, TagValueResp>(id)
	{
	}
	~ServerTagValueInstruction()
	{
	}

	byte command() const
	{
		return 0x22;
	}

	bool allowLog() const override
	{
		return false;
	}

	bool autoResponsed() const
	{
		return true;
	}

    ClientContext* setValue(ClientContext* context);
};

class TagValueContext : public ElementContext<TagValueReq, TagValueResp>
{
};

// Maintain command
struct MaintainDownReq
{
public:
    byte programId;

    MaintainDownReq()
    {
        programId = 0;
    }

    void write(Stream* stream) const
    {
        stream->writeByte(programId);
    }
    void read(Stream* stream)
    {
        programId = stream->readByte();
    }
    void copyFrom(const MaintainDownReq* value)
    {
        *this = *value;
    }
};

typedef TagConfigurationResp MaintainDownResp;

class ServerMaintainDownInstruction : public ServerElementInstruction<MaintainDownReq, MaintainDownResp>
{
public:
    ServerMaintainDownInstruction(InstructionDescription* id) : ServerElementInstruction<MaintainDownReq, MaintainDownResp>(id)
    {
    }
    ~ServerMaintainDownInstruction()
    {
    }
    byte command() const
    {
        return 0x31;
    }

    bool allowLog() const override
    {
        return false;
    }

    bool autoResponsed() const
    {
        return true;
    }

    bool syncSendReceive(Interactive*) const override
    {
        return false;
    }

    ClientContext* setValue(ClientContext* context);
};
class MaintainDownInstruction : public ElementInstruction<MaintainDownReq, MaintainDownResp>
{
public:
    MaintainDownInstruction(InstructionDescription* id) : ElementInstruction<MaintainDownReq, MaintainDownResp>(id)
    {
    }
    ~MaintainDownInstruction()
    {
    }
    byte command() const
    {
        return 0x31;
    }

    bool allowLog() const override
    {
        return false;
    }

    ClientContext* setValue(ClientContext* context);
};

class MaintainDownContext : public ElementContext<MaintainDownReq, MaintainDownResp>
{
};

struct Maintain
{
public:
    byte programId;
    byte cpu;
    uint memUsed;
    uint memFree;
    uint disk;
    byte runTimeStatus;
    byte connStatus;
    byte opcStatus;

    Maintain()
    {
        programId = PROGRAM_ID_GATHERER;
        cpu = 0;
        memUsed = 0;
        memFree = 0;
        disk = 0;
        runTimeStatus = 0;
        connStatus = 0;
        opcStatus = -1; // not connect
    }

    void write(Stream* stream) const
    {
        stream->writeByte(programId);
        stream->writeByte(cpu);
        stream->writeUInt32(memUsed);
        stream->writeUInt32(memFree);
        stream->writeUInt32(disk);
        stream->writeByte(runTimeStatus);
        stream->writeByte(connStatus);
        stream->writeByte(opcStatus);
    }
    void read(Stream* stream)
    {
        programId = stream->readByte();
        cpu = stream->readByte();
        memUsed = stream->readUInt32();
        memFree = stream->readUInt32();
        disk = stream->readUInt32();
        runTimeStatus = stream->readByte();
        connStatus = stream->readByte();
        opcStatus = stream->readByte();
    }
    void copyFrom(const Maintain* value)
    {
        *this = *value;
    }
};

struct MaintainUpReq
{
public:
    vector<Maintain> vecMaintain;

    MaintainUpReq()
    {
    }

    void write(Stream* stream) const
    {
        stream->writeInt16(vecMaintain.size());
        for (vector<Maintain>::const_iterator ci = vecMaintain.begin(); ci != vecMaintain.end(); ci++)
        {
            ci->write(stream);
        }
    }
    void read(Stream* stream)
    {
        int size = stream->readInt16();
        if (size > 3)
        {
            Trace::writeFormatLine("Maintenace information out of range: too many elements (%d)", size);
            return;
        }
        vecMaintain = vector<Maintain>(size);
        for (vector<Maintain>::iterator ci = vecMaintain.begin(); ci != vecMaintain.end(); ci++)
        {
            ci->read(stream);
        }
    }
    void copyFrom(const MaintainUpReq* value)
    {
        *this = *value;
    }
};

typedef TagConfigurationResp MaintainUpResp;

class ServerMaintainUpInstruction : public ServerElementInstruction<MaintainUpReq, MaintainUpResp>
{
public:
    ServerMaintainUpInstruction(InstructionDescription* id) : ServerElementInstruction<MaintainUpReq, MaintainUpResp>(id)
    {
    }
    ~ServerMaintainUpInstruction()
    {
    }
    byte command() const
    {
        return 0x32;
    }

    bool allowLog() const override
    {
        return false;
    }

    bool autoResponsed() const
    {
        return true;
    }

    bool syncSendReceive(Interactive*) const override
    {
        return false;
    }

    ClientContext* setValue(ClientContext* context);
};
class MaintainUpInstruction : public ElementInstruction<MaintainUpReq, MaintainUpResp>
{
public:
    MaintainUpInstruction(InstructionDescription* id) : ElementInstruction<MaintainUpReq, MaintainUpResp>(id)
    {
    }
    ~MaintainUpInstruction()
    {
    }
    byte command() const
    {
        return 0x32;
    }

    bool allowLog() const override
    {
        return false;
    }

    bool syncSendReceive(Interactive*) const override
    {
        return false;
    }

    ClientContext* setValue(ClientContext* context);
};

class MaintainUpContext : public ElementContext<MaintainUpReq, MaintainUpResp>
{
};
// Maintain command end

// Log command
struct DownloadLogReq
{
public:
    byte programId;
    time_t startDate;
    time_t endDate;

    DownloadLogReq()
    {
        programId = 0;
    }

    void write(Stream* stream) const
    {
        stream->writeByte(programId);
        stream->writeBCDDate(startDate);
        stream->writeBCDDate(endDate);
    }
    void read(Stream* stream)
    {
        programId = stream->readByte();
        startDate = stream->readBCDDate();
        endDate = stream->readBCDDate();
    }
    void copyFrom(const DownloadLogReq* value)
    {
        *this = *value;
    }
};

typedef TagConfigurationResp DownloadLogResp;
// Status: 0: log will consequently upload; 1: invalid program id; 2: invalid start date; 3: invalid end date; 4: end date earlier than start date; 5: no log

class ServerDownloadLogInstruction : public ServerElementInstruction<DownloadLogReq, DownloadLogResp>
{
public:
    ServerDownloadLogInstruction(InstructionDescription* id) : ServerElementInstruction<DownloadLogReq, DownloadLogResp>(id)
    {
    }
    ~ServerDownloadLogInstruction()
    {
    }
    byte command() const
    {
        return 0x33;
    }

    bool allowLog() const override
    {
        return false;
    }

    bool autoResponsed() const
    {
        return true;
    }

    ClientContext* setValue(ClientContext* context);
};
class DownloadLogInstruction : public ElementInstruction<DownloadLogReq, DownloadLogResp>
{
public:
    DownloadLogInstruction(InstructionDescription* id) : ElementInstruction<DownloadLogReq, DownloadLogResp>(id)
    {
    }
    ~DownloadLogInstruction()
    {
    }
    byte command() const
    {
        return 0x33;
    }

    bool allowLog() const override
    {
        return false;
    }

    bool syncSendReceive(Interactive*) const override
    {
        return false;
    }

    ClientContext* setValue(ClientContext* context);
};

class DownloadLogContext : public ElementContext<DownloadLogReq, DownloadLogResp>
{
};

struct UploadLogReq
{
public:
    byte programId;

    UploadLogReq()
    {
    }

    void write(Stream* stream) const
    {
        stream->writeByte(programId);
    }
    void read(Stream* stream)
    {
        programId = stream->readByte();
    }
    void copyFrom(const UploadLogReq* value)
    {
        *this = *value;
    }
};

typedef TagConfigurationResp UploadLogResp;

class ServerUploadLogInstruction : public ServerUploadFileInstruction<UploadLogReq>
{
public:
    ServerUploadLogInstruction(InstructionDescription* id) : ServerUploadFileInstruction<UploadLogReq>(id)
    {
    }
    ~ServerUploadLogInstruction()
    {
    }
    byte command() const
    {
        return 0x34;
    }

    bool allowLog() const override
    {
        return false;
    }

    //bool autoResponsed() const
    //{
    //    return true;
    //}

    //bool syncSendReceive(Interactive*) const override
    //{
    //    return false;
    //}

    ClientContext* setValue(ClientContext* context);
};

class UploadLogInstruction : public UploadFileInstruction<UploadLogReq>
{
public:
    UploadLogInstruction(InstructionDescription* id) : UploadFileInstruction<UploadLogReq>(id)
    {
    }
    ~UploadLogInstruction()
    {
    }
    byte command() const
    {
        return 0x34;
    }

    bool allowLog() const override
    {
        return false;
    }

    ClientContext* setValue(ClientContext* context);
};

class UploadLogContext : public FileContext<UploadLogReq>
{
};

#endif //INTERFACE_H