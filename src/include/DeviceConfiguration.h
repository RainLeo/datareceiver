#pragma once
#include <vector>
#include "EData.h"
#include "EStatus.h"
#include "Tag.h"

using namespace Common;

typedef rtdb::Tag TagValue;
typedef rtdb::Tag::Type TagType;

class CDeviceDetail
{
public:
    CDeviceDetail() {};
    virtual CDeviceDetail * Clone() = 0;
    virtual ~CDeviceDetail() {};
    virtual void write(Stream* stream) const = 0;
    virtual void read(Stream* stream) = 0;
};

struct DeviceTag
{
    string address; // used in RS485 for multiple devices use only one serial port, 2015/09/26
    string strName;
    int tagId;
    char type;
    string strRegisterName;
    short sStartAddr;
    short sUnitLen;
    byte function;
    bool bigEndian;
    bool inverse;
    DeviceTag()
    {
        address = "";
        strName = "";
        tagId = -1;
        type = 0;
        strRegisterName = "";
        sStartAddr = 0;
        sUnitLen = 0;
        function = 3;
        bigEndian = true;
        inverse = false;
    }
    TagType GetTagType() const
    {
        switch (type)
        {
        case 0:
        default:
            return TagType::Null;
        case 1: // Int8
        case 2:
            return TagType::Integer32;
        case 3:
            return TagType::Float32;
        case 4:
            return TagType::Digital;
        }
    }

    bool IsValid() const
    {
        return tagId != -1;
    }
};

class CDeviceConfiguration
{
public:
    char szDeviceId[32];
    byte type;
    CDeviceDetail *pDetail;
    const static int MAX_TAG_NUM = 10000;
    const static int MAX_NAME_LEN = 300;
    vector<DeviceTag> vecTags;

    CDeviceConfiguration();
    virtual ~CDeviceConfiguration();

    virtual CDeviceConfiguration* Clone();
    virtual CDeviceDetail * createDetail();
    virtual void write(Stream* stream) const;
    virtual void read(Stream* stream);
    bool IsValid();
    bool AddTag(DeviceTag &tag);
};

enum DEVICETYPE_EN
{
    DEVICE_NONE = 0,
    DEVICE_FILE = 1,
    DEVICE_MODBUS = 2,
    DEVICE_DL_T645 = 3,
    DEVICE_HART = 4,
    DEVICE_OPC = 5,
    DEVICE_ACG = 6,
};

