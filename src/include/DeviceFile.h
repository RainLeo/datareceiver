#pragma once

#include "common/system/DateTime.h"
#include "common/thread/Timer.h"
#include "DeviceConfiguration.h"

using namespace Common;

class CDeviceFileDetail : public CDeviceDetail
{
public:
    CDeviceFileDetail();
    ~CDeviceFileDetail();
    string strConfigFilePath;
    string strDataFilePath;

    CDeviceDetail * Clone()
    {
        CDeviceFileDetail *pDevConfig = new CDeviceFileDetail();
        *pDevConfig = *(CDeviceFileDetail *)this;

        return pDevConfig;
    }
    void write(Stream* stream) const;
    void read(Stream* stream);
};

class CDeviceFile : public CDeviceConfiguration
{
public:
	CDeviceFile() { type = DEVICE_FILE; pDetail = createDetail(); }
	~CDeviceFile() {};

	CDeviceDetail * createDetail() { return new CDeviceFileDetail(); }
};
