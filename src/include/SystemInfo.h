#pragma once

int getDiskInfo();
int getDiskUsagePercentage();
int getMemInfo(int &);
int getCpuInfo();
#ifndef WIN32
bool adjustdate(int year, int mon, int mday, int hour, int min, int sec);
bool procExisted(char *name = "java"); /* Proc name */
#endif