#ifndef ESTATUS_H
#define ESTATUS_H

enum EStatus
{
	ES_Stopped=0,
	ES_Normal=1,
	ES_Unconnected,
	ES_Unknown,
	ES_TimedOut,
	ES_PartialNormal
};

#endif
