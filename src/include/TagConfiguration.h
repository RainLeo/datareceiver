#pragma once
#include "DeviceConfiguration.h"

struct TagConfiguration
{
    const static int MAX_DEVICE_NUM = 200;
    string id;
    int iFetchInterval = 0;
    vector<CDeviceConfiguration *> vecDevices;

    TagConfiguration();
    ~TagConfiguration();

    void write(Stream* stream) const;
    void read(Stream* stream);
    void copyFrom(const TagConfiguration* value);
    void Clear();
    TagConfiguration & operator=(const TagConfiguration &rhs);
    bool IsValid();
    bool AddDevice(CDeviceConfiguration *pDeviceConfiguration);
};
