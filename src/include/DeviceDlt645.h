#pragma once
#include "DeviceModBus.h"

class CDeviceDlt645Detail : public CDeviceSerialDetail
{
public:
    string address;

    CDeviceDlt645Detail() { address[0] = '\0'; }
    virtual CDeviceDetail* Clone()
    {
        CDeviceDlt645Detail *pDevConfig = new CDeviceDlt645Detail();
        *pDevConfig = *(CDeviceDlt645Detail *)this;

        return pDevConfig;
    }
    void write(Stream* stream) const;
    void read(Stream* stream);
    virtual void parse(XmlTextReader &reader);

};

class CDeviceDlt645 : public CDeviceConfiguration
{
public:
    CDeviceDlt645() { type = DEVICE_DL_T645; pDetail = createDetail(); }
    ~CDeviceDlt645() {};

    CDeviceDetail * createDetail() { return new CDeviceDlt645Detail(); }
};

