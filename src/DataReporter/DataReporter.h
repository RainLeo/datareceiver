#pragma once

#include "common/driver/communication/ServerService.h"
#include "common/driver/communication/ClientService.h"
#include "common/system/Singleton.h"
#include "common/system/DateTime.h"
#include "interface.h"
#include "TagValueCache.h"
#include "ClientCommConfig.h"
#include "ServerCommConfig.h"

using namespace Common;
using namespace Driver;

class CDataReporterServer : public ServerService
{
public:
    CDataReporterServer(const Server& server) : ServerService(server) {}

	bool initialize(const ConfigFile* file);
    bool unInitialize()
    {
        return ServerService::unInitialize();
    }

    void createDualPortsRamDevice(const InstructionCallback &callback, const int portIndex = 0);

	bool initialize(InstructionCallback instructions)
	{
		return ServerService::initialize(instructions);
	}

	template<class T, class C>
	bool send2Gatherer(const T& inputData, const string& name)
	{
		return ServerService::sendAsync<T, C>("", inputData, name);
	}
    bool isOnline()
    {
        if (!_instructionPool)
            return false;

        return _instructionPool->channelConnected();
    }
};

class CDataReporterClient : public ClientService
{
public:
    CDataReporterClient(const Client& client) : ClientService(client) {}

    bool isOnline()
    {
        Sampler *pool = clientSampler();
        if (pool == NULL) return false;
        return pool->isOnline();
    };
};

class CDataReporter
{
public:
	CDataReporter();
	~CDataReporter();
	bool initialize(const ConfigFile* file);
	bool unInitialize();
    bool sendTags(const TagValueReq &input)
    {
        bool ret = m_pDataReporterClient->sendAsync<TagValueReq, TagValueContext>(input, "TagValue");
        return ret;
    }
    static CDataReporter * Instance() { return Singleton<CDataReporter>::instance(); }
	void SetTagConfiguration(const TagConfiguration & tagConfiguration)
    {
        m_tagConfiguration.copyFrom(&tagConfiguration);
        m_bRecvTagConfiguration = true;
        m_bSentTagConfiguration = false;
    }
	void SendTagConfigurationSuccess() { m_bSentTagConfiguration = true; }
    void SendTagConfiguration(bool force = false);
    bool IsTagConfigurationReceived()
    {
        return m_bRecvTagConfiguration;
    }
    template<class T, class C>
    bool send2Gatherer(const T& inputData, const string& name)
    {
        return m_pDataReporterServer->send2Gatherer<T, C>(inputData, name);
    }
    bool IsMaintainRequested()
    {
        return true;// m_bMaintainRequested;
    }
    void RequestMaintain()
    {
        m_bMaintainRequested = true;
        m_bGathererMaintainRequested = true;
    }
    bool sendMaintain(MaintainUpReq &input)
    {
        if ((m_bGathererMaintainRequested && !m_bGathererMaintainReceived) || m_vecMaintain.size() == 0)
        {
            return false; // Gatherer maintain not ready
        }

        m_bMaintainRequested = false;
        input.vecMaintain.push_back(*m_vecMaintain.begin()); // add gatherer maintain
        m_vecMaintain.clear(); // clear to be ready for receiving next maintain information from gatherer
        m_bGathererMaintainRequested = false;
        m_bGathererMaintainReceived = false;
        return m_pDataReporterClient->sendAsync<MaintainUpReq, MaintainUpContext>(input, "MaintainUp");
    }
    bool getGathererMaintain(const MaintainUpReq &input)
    {
        if (m_bGathererMaintainReceived)
        {
            return false;
        }

        m_bGathererMaintainReceived = true;
        m_vecMaintain.push_back(*input.vecMaintain.begin());
        return true;
    }
    void MaintainProc();
    bool IsStarting() { return m_bStarting; }
    void Started() { m_bStarting = false; }

    /////////////////////////////////////////////////////////////
    // Log file upload
    FileHeader header;
    Thread *m_pSendLogThread;

    static void staticSendLogProc(void* parameter)
    {
        CDataReporter* app = CDataReporter::Instance();

        app->sendLog(*(FileHeader *)parameter);
    }

    bool sendLog(const string &path, const string &fileName)
    {
#ifdef DEBUG
        m_pSendLogThread->setName("SendLog thread");
#endif
        header.path = path;
        header.file_name = fileName;
        int count = 0;
        while (m_pSendLogThread->isAlive())
        {
            count++;
            if (count > 300) // 300 * 10  msec
            {
                Trace::writeFormatLine("Warning: sending log (%s) blocked by a long log upload process.", fileName.c_str());
                break;
            }
            Thread::msleep(10);
        }
        m_pSendLogThread->start(staticSendLogProc, &header);

        return true;
    }

    bool sendLog(FileHeader &header)
    {
        UploadLogReq input;
        input.programId = PROGRAM_ID_GATHERER;
        return m_pDataReporterClient->uploadFileSync<UploadLogReq, UploadLogContext>(header, input, "UploadLog");
    }

private:
    DECLARE_SINGLETON_CLASS(CDataReporter);
    CDataReporterServer *m_pDataReporterServer;
    CDataReporterClient *m_pDataReporterClient;
	TagConfiguration m_tagConfiguration;
	bool m_bSentTagConfiguration;
    bool m_bRecvTagConfiguration;
    bool m_bMaintainRequested;
    bool m_bGathererMaintainRequested;
    bool m_bGathererMaintainReceived;
    vector<Maintain> m_vecMaintain;
    bool m_bStarting;

    TagValueCache *m_pTagValueCache;
    Timer *m_pMaintainTimer;
    ClientCommConfig *m_pConfig;
    uint m_activeTime;

public:
    void Activate()
    {
        m_activeTime = TickTimeout::GetCurrentTickCount();
    }
    bool IsActive() const
    {
        TimeSpan clientTimeout = m_pDataReporterServer->server().timeout.close;
        uint deadTime = TickTimeout::GetDeadTickCount(m_activeTime, (uint)clientTimeout.totalMilliseconds());
        if (TickTimeout::IsTimeout(m_activeTime, deadTime))
            return false;

        return true;
    }

#if 0
    // T is input data, K is output data, C is context.
    // Sent all of clients if address is empty.
    // Use , or ; to split address.
    template<class T, class K, class C>
    bool send(const string& address, const T& inputData, K& outputData, const string& name, bool sync = true)
    {
#if DEBUG
        Stopwatch sw(Convert::convertStr("ServerService::send, name: %s", name.c_str()), 200);
#endif

        if (!_instructionPool)
            return false;
        if (!_instructionPool->channelConnected())
            return false;

        C* scontext = new C();
        scontext->setInputData(&inputData);
        scontext->setSentAddr(address);
        InstructionDescription* id = new InstructionDescription(name, scontext);
        return sync ? (_instructionPool->executeInstructionSync(id) != NULL) : (_instructionPool->addInstruction(id) != NULL);
    }
#endif
};

