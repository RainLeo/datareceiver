// DataReporter.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#if WIN32
#if _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif
#else
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#endif
#include <iostream>
#include "DataReporter.h"
#include "common/system/Application.h"
#include "common/diag/Trace.h"
#include "common/diag/Stopwatch.h"
#include "DualPortsRamInteractive.h"
#include "common/driver/channels/SerialInteractive.h"
#include "common/thread/Thread.h"
#include "SystemInfo.h"

#if WIN32
int _tmain(int argc, _TCHAR* argv[])
#else
void daemonize(void)
{
    pid_t pid;

    /*
    * Become a session leader to lose controlling TTY.
    */
    if ((pid = fork()) < 0) {
        perror("fork");
        exit(1);
    }
    else if (pid != 0) /* parent */
        exit(0);
    setsid();

    /*
    * Change the current working directory to the root.
    */
    if (chdir("/") < 0) {
        perror("chdir");
        exit(1);
    }

    /*
    * Attach file descriptors 0, 1, and 2 to /dev/null.
    */
    close(0);
    open("/dev/null", O_RDWR);
    dup2(0, 1);
    dup2(0, 2);
}

int main(int argc, char* argv[])
#endif
{
#if WIN32 && _DEBUG
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

    Trace::writeFormatLine("Reporter App Starting: Version: %s", AppVersion.toString().c_str());

    Application app(argc, (const char **)argv);

	ConfigFile _rootFile;
	_rootFile.rootPath = app.rootPath();
	CDataReporter *pReporter = CDataReporter::Instance();
	pReporter->initialize(&_rootFile);
    Trace::writeFormatLine("Reporter App Started: Version: %s", AppVersion.toString().c_str());

#if !WIN32
    if (argc == 2 && strcmp(argv[1], "d") == 0)
    {
        daemonize();
    }
#endif

#if defined(DEBUG) && defined(WIN32)
    string input;
    while (!Convert::stringEqual(input, "exit", true))
    {
        Trace::writeLine("Reporter: Input 'exit' to exit...");
        getline(cin, input);
    }
#else
    while (true)
    {
        Thread::msleep(3000);
    }
#endif

    Trace::writeFormatLine("Reporter App Shuts down.");
    delete pReporter;
    return 0;
}

vector<Alarm> GetAlarms()
{
    vector<Alarm> alarms;
    CDataReporter *pDataRepoter = CDataReporter::Instance();
    if (pDataRepoter != NULL)
    {
        if (!pDataRepoter->IsTagConfigurationReceived())
        {
            Alarm alarm;
            alarm.programId = PROGRAM_ID_REPORTER;
            alarm.alarm = Alarm::ALARM_RESTART;
            alarms.push_back(alarm);
        }
        if (getDiskInfo() < 10)
        {
            Alarm alarm;
            alarm.programId = PROGRAM_ID_REPORTER;
            alarm.alarm = Alarm::ALARM_INSUFFICIENT_DISK;
            alarms.push_back(alarm);
        }
        if (Alarm::IsHighCpu())
        {
            Alarm alarm;
            alarm.programId = PROGRAM_ID_REPORTER;
            alarm.alarm = Alarm::ALARM_HIGH_CPU;
            alarms.push_back(alarm);
        }
    }
    return alarms;
}

bool WriteDb(const TagValueReq &tagValueReq)
{
    CDataReporter *pDr = CDataReporter::Instance();
    assert(pDr);

    return pDr->sendTags(tagValueReq);
}

void staticMaintainProc(void* parameter)
{
    CDataReporter* pReporter = (CDataReporter*)parameter;
    pReporter->MaintainProc();
}

void CDataReporter::MaintainProc()
{
    int cpu = getCpuInfo();
    Alarm::CheckHighCpu(cpu);
    if (IsMaintainRequested())
    {
        int memUsed = 0, memFree;
        memFree = getMemInfo(memUsed);
        int disk = getDiskUsagePercentage();
#if DEBUG
        printf("disk: %d%%, free mem: %d M, used mem: %d M, cpu: %d%%\n", disk, memFree, memUsed, cpu);
#endif
        MaintainUpReq input;
        Maintain maintain;
        maintain.programId = PROGRAM_ID_REPORTER;
        maintain.cpu = cpu;
        maintain.memFree = memFree;
        maintain.memUsed = memUsed;
        maintain.disk = disk;
        maintain.connStatus = IsActive();
        if (!m_pDataReporterServer->isOnline())
        {
            maintain.connStatus = -100;
        }
        input.vecMaintain.push_back(maintain);
        sendMaintain(input);
    }
}

CDataReporter::CDataReporter() :
    m_pDataReporterServer(NULL),
    m_pDataReporterClient(NULL),
    m_bMaintainRequested(false),
    m_bGathererMaintainRequested(false),
    m_bGathererMaintainReceived(false),
    m_bStarting(true),
    m_pSendLogThread(NULL),
    m_pMaintainTimer(NULL),
    m_pConfig(NULL),
    m_activeTime(0)
{
	m_bSentTagConfiguration = true;
    m_bRecvTagConfiguration = false;
    m_pTagValueCache = TagValueCache::Instance();
    m_pTagValueCache->SetSaveFunc(WriteDb, false, 10);
    m_pSendLogThread = new Thread();
}

CDataReporter::~CDataReporter()
{
    unInitialize();

    if (m_pTagValueCache != NULL)
    {
        delete m_pTagValueCache;
        m_pTagValueCache = NULL;
    }

    if (m_pSendLogThread != NULL)
    {
        m_pSendLogThread->stop();
        delete m_pSendLogThread;
        m_pSendLogThread = NULL;
    }

    if (m_pMaintainTimer != NULL)
    {
        delete m_pMaintainTimer;
        m_pMaintainTimer = NULL;
    }
}

InstructionDescription* heartbeat()
{
	return new InstructionDescription("Heartbeat", new HeartbeatContext());
}

void client_generateInstructions(void* owner, Instructions* instructions)
{
	HeartbeatInstruction* hi = new HeartbeatInstruction(new InstructionDescription("Heartbeat", new HeartbeatContext()));
	instructions->add(hi);
    ServerTagConfigurationInstruction *ti = new ServerTagConfigurationInstruction(new InstructionDescription("TagConfiguration", new TagConfigurationContext()));
	instructions->add(ti);
	TagValueInstruction* tvi = new TagValueInstruction(new InstructionDescription("TagValue", new TagValueContext()));
	instructions->add(tvi);
    ServerMaintainDownInstruction * smdi = new ServerMaintainDownInstruction(new InstructionDescription("MaintainDown", new MaintainDownContext()));
    instructions->add(smdi);
    MaintainUpInstruction *mui = new MaintainUpInstruction(new InstructionDescription("MaintainUp", new MaintainUpContext()));
    instructions->add(mui);
    Instruction *i = new ServerDownloadLogInstruction(new InstructionDescription("DownloadLog", new DownloadLogContext()));
    instructions->add(i);
    i = new UploadLogInstruction(new InstructionDescription("UploadLog", new UploadLogContext()));
    instructions->add(i);
}

void server_generateInstructions(void* owner, Instructions* instructions)
{
	ServerHeartbeatInstruction* hi = new ServerHeartbeatInstruction(new InstructionDescription("Heartbeat", new HeartbeatContext()));
	instructions->add(hi);
	TagConfigurationInstruction *ti = new TagConfigurationInstruction(new InstructionDescription("TagConfiguration", new TagConfigurationContext()));
	instructions->add(ti);
	ServerTagValueInstruction* tvi = new ServerTagValueInstruction(new InstructionDescription("TagValue", new TagValueContext()));
	instructions->add(tvi);
    MaintainDownInstruction * smdi = new MaintainDownInstruction(new InstructionDescription("MaintainDown", new MaintainDownContext()));
    instructions->add(smdi);
    ServerMaintainUpInstruction *mui = new ServerMaintainUpInstruction(new InstructionDescription("MaintainUp", new MaintainUpContext()));
    instructions->add(mui);
    Instruction *i = new DownloadLogInstruction(new InstructionDescription("DownloadLog", new DownloadLogContext()));
    instructions->add(i);

    UploadLogContext *c = new UploadLogContext();
    FileHeader header;
    header.path = Path::combine(Application::instance()->rootPath(), "upload");
    c->setHeader(&header);
    i = new ServerUploadLogInstruction(new InstructionDescription("UploadLog", c));
    instructions->add(i);
}

bool CDataReporter::initialize(const ConfigFile* file)
{
	// load communication.config
	ConfigFile temp = *file;
	temp.fileName = "reporter.config";
    m_pConfig = new ClientCommConfig(temp, "reporter");
    ClientCommConfig &reader = *m_pConfig;
	if (!reader.Configuration::load())
		return false;

    m_pTagValueCache->SetConfig(reader.m_TagCacheSendTimeout, reader.m_TagCacheSendInterval, reader.m_TagCacheCountLimit);

    ServerCommConfig serverReader(temp, "reporter");
    if (!serverReader.Configuration::load())
		return false;

    m_pDataReporterServer = new CDataReporterServer(serverReader.server());

    BaseCommService::InstructionCallback callback, callbackclient;
    callback.tcpInstructions = server_generateInstructions;

    if (m_pDataReporterServer->server().address == "dual_ports_ram")
    {
        m_pDataReporterServer->createDualPortsRamDevice(callback, m_pDataReporterServer->server().port);
    }
    else
    {
        m_pDataReporterServer->initialize(callback);
    }

    m_pDataReporterClient = new CDataReporterClient(reader.client());
    callbackclient.tcpInstructions = client_generateInstructions;
    callbackclient.tcpSampler = heartbeat;
    if (!m_pDataReporterClient->initialize(callbackclient))
		return false;

    if (reader.m_MaintainInterval > 0)
    {
        Trace::writeFormatLine("Reporter maintain timer starts in interval %d ms.", reader.m_MaintainInterval);
        m_pMaintainTimer = new Timer(staticMaintainProc, this, reader.m_MaintainInterval);
    }
    return true;
}

bool CDataReporter::unInitialize()
{
    if (m_pDataReporterServer != NULL)
    {
        m_pDataReporterServer->unInitialize();
        delete m_pDataReporterServer;
        m_pDataReporterServer = NULL;
    }
    if (m_pDataReporterClient != NULL)
    {
        m_pDataReporterClient->unInitialize();
        delete m_pDataReporterClient;
        m_pDataReporterClient = NULL;
    }
    if (m_pConfig != NULL)
    {
        delete m_pConfig;
        m_pConfig = NULL;
    }

    return true;
}

void CDataReporter::SendTagConfiguration(bool force)
{
    if (!m_bRecvTagConfiguration || (m_bSentTagConfiguration && !force))
	{
		return;
	}

    if (m_pDataReporterServer->send2Gatherer<TagConfiguration, TagConfigurationContext>(m_tagConfiguration, "TagConfiguration"))
    {
        m_bSentTagConfiguration = true; // avoid repetitively sending the tag configuration if too many tags are included to take too long to handle
        Trace::writeFormatLine("Reporter: sending tag configuration to gatherer.");
    }
    else
    {
        Trace::writeFormatLine("Reporter: sending tag configuration failed to gatherer.");
    }
}

void CDataReporterServer::createDualPortsRamDevice(const InstructionCallback &callback, const int portIndex)
{
    DriverManager* dm = manager();
    assert(dm);

    DualPortsRamInstructionSet* set = new DualPortsRamInstructionSet(this, callback.tcpInstructions);
    DualPortsRamChannelContext * scc = new DualPortsRamChannelContext(portIndex);
    DualPortsRamInteractive *si = new DualPortsRamInteractive(dm);
    ChannelDescription* cd = new ChannelDescription("DualPortsRam0", scc, si);
    Channel *ch = new Channel(dm, cd);
    DeviceDescription* dd = new DeviceDescription("DualPortsRamDevice", cd, set);

    dm->description()->addDevice(dd);
    dm->addPool(_instructionPool = new ServerSampler(dm, cd, dd));
    dm->open();
}

