#include "DataReporter.h"
#include "common/system/LocalTime.h"
#include "SystemInfo.h"
#include "common/system/Application.h"

uint startTime = TickTimeout::GetCurrentTickCount();

ClientContext* HeartbeatInstruction::setValue(ClientContext* context)
{
    CDataReporter *pDataReporter = CDataReporter::Instance();
    // set time for the first time or after an specified interval
    const int timeSyncHourInterval = 1;
    uint deadTime = TickTimeout::GetDeadTickCount(startTime, timeSyncHourInterval * 3600 * CLOCKS_PER_SEC);

    if (pDataReporter->IsStarting() || TickTimeout::IsTimeout(startTime, deadTime))
    {
        HeartbeatContext *hbc = (HeartbeatContext *)context;
        DateTime &peerTime = hbc->outputData()->curtime;
#ifdef WIN32
        bool ret = LocalTime::setTime(peerTime);
#else
        bool ret = adjustdate(peerTime.year(), peerTime.month(), peerTime.day(), peerTime.hour(), peerTime.minute(), peerTime.second());
#endif
        startTime = TickTimeout::GetCurrentTickCount(); // reset startTime to current for next period
        Trace::writeFormatLine("Time synchronized to receiver %s (interval: %d hours).", ret ? "successfully" : "failed", timeSyncHourInterval);
    }

    pDataReporter->Started();
    return context;
}

ClientContext* TagConfigurationInstruction::setValue(ClientContext* context)
{
    TagConfigurationContext *pContext = (TagConfigurationContext*)context;
    if (pContext->outputData()->status == 0)
    {
        Trace::writeFormatLine("Reporter: tag configuration response succeeds from gatherer.");
        CDataReporter *pDataReporter = CDataReporter::Instance();
        assert(pDataReporter);
        pDataReporter->SendTagConfigurationSuccess();
    }
    else
    {
        Trace::writeFormatLine("Reporter: tag configuration response failed from gatherer. error code: %d(0x%x)", pContext->outputData()->status, pContext->outputData()->status);
    }
    return context;
}

ClientContext* ServerTagConfigurationInstruction::setValue(ClientContext* context)
{
    TagConfigurationContext *pContext = (TagConfigurationContext*)context;

    string clientAddr = pContext->peerAddr();
    Trace::writeFormatLine("Tag configuration received from receiver: %s.", clientAddr.c_str());

    TagConfigurationResp resp;
    const TagConfiguration &req = *pContext->inputData();
    if (req.iFetchInterval < 0)
    {
        resp.status = 0x1;
        Trace::writeFormatLine("Tag configuration parameter error: fetch interval is negative.");
    }
    else
    {
        CDataReporter *pDataReporter = CDataReporter::Instance();
        assert(pDataReporter);
        pDataReporter->SetTagConfiguration(req);
    }
    pContext->setOutputData(&resp);

    return context;
}

ClientContext* ServerHeartbeatInstruction::setValue(ClientContext* context)
{
    HeartbeatContext *pHbc = (HeartbeatContext*)context;
    const HeartbeatReq &req = *pHbc->inputData();

    if (!AppVersion.IsCompatible(pHbc->inputData()->version))
    {
        Trace::writeFormatLine("Warning: Inconsistent version (%s) from Gatherer %s with Reporter version %s.", pHbc->inputData()->version.toString().c_str(), pHbc->peerAddr().c_str(), AppVersion.toString().c_str());
    }

    const vector<Alarm> &alarms = req.vecAlarms;
    bool bNeedTagConfiguration = false;
    for (uint i = 0; i < alarms.size(); i++)
    {
        const Alarm &alarm = alarms[i];
        if (alarm.alarm == Alarm::ALARM_RESTART)
        {
            bNeedTagConfiguration = true;
            break;
        }
    }

    HeartbeatResp resp; // DateTime Now
    pHbc->setOutputData(&resp);

    CDataReporter *pDr = CDataReporter::Instance();
    assert(pDr);
    pDr->SendTagConfiguration(bNeedTagConfiguration);
    pDr->Activate();
    return context;
}

ClientContext* ServerTagValueInstruction::setValue(ClientContext* context)
{
    TagValueContext *tvc = dynamic_cast<TagValueContext *>(context);
    assert(tvc);
    TagValueCache::Instance()->WriteTags(*tvc->inputData());
    TagValueResp resp;
    resp.status = tvc->inputData()->reqNo;
    tvc->setOutputData(&resp);
    return context;
}

ClientContext* TagValueInstruction::setValue(ClientContext* context)
{
    TagValueContext *tvc = dynamic_cast<TagValueContext *>(context);
    assert(tvc);
    TagValueCache::Instance()->SaveFuncSucceed(tvc->outputData()->status);

    return context;
}

ClientContext* ServerMaintainDownInstruction::setValue(ClientContext* context)
{
    MaintainDownContext *pContext = (MaintainDownContext*)context;
    const MaintainDownReq &maintainReq = *pContext->inputData();

    if (maintainReq.programId == PROGRAM_ID_REPORTER || maintainReq.programId == PROGRAM_ID_GATHERER)
    {
        CDataReporter *pDr = CDataReporter::Instance();
        assert(pDr);

        pDr->RequestMaintain();
        // Request maintenance information
        pDr->send2Gatherer<MaintainDownReq, MaintainDownContext>(maintainReq, "MaintainDown");
    }
    else
    {
        MaintainDownResp resp;
        resp.status = 1; // incorrect Program ID
        pContext->setOutputData(&resp);
    }

    return context;
}

ClientContext* MaintainDownInstruction::setValue(ClientContext* context)
{
    MaintainDownContext *pContext = (MaintainDownContext*)context;
    if (pContext->outputData()->status != 0)
    {
        Trace::writeFormatLine("Warning: Request maintenance information response failed from gatherer, status = %d(0x%x)", pContext->outputData()->status, pContext->outputData()->status);
    }

    return context;
}

ClientContext* ServerMaintainUpInstruction::setValue(ClientContext* context)
{
    MaintainUpContext *pContext = (MaintainUpContext*)context;
    const MaintainUpReq &maintainReq = *pContext->inputData();

    Debug::writeFormatLine("Maintenance information received from gatherer.");
    const vector<Maintain> & vecMaintain = maintainReq.vecMaintain;
    for (vector<Maintain>::const_iterator ci = vecMaintain.begin(); ci != vecMaintain.end(); ci++)
    {
        Debug::writeFormatLine("    ProgramId: %d. CPU: %d%%, Used Mem: %u M, Free Mem: %u M, Disk: %u%%", ci->programId, ci->cpu, ci->memUsed, ci->memFree, ci->disk);
    }

    CDataReporter *pDr = CDataReporter::Instance();
    assert(pDr);

    pDr->getGathererMaintain(maintainReq);
    return context;
}

ClientContext* MaintainUpInstruction::setValue(ClientContext* context)
{
    return context;
}

ClientContext* ServerDownloadLogInstruction::setValue(ClientContext* context)
{
    DownloadLogContext *pContext = (DownloadLogContext*)context;
    const DownloadLogReq &req = *pContext->inputData();
    DownloadLogResp resp;

    CDataReporter *pDr = CDataReporter::Instance();
    assert(pDr);

    if (req.programId == PROGRAM_ID_REPORTER)
    {
        string dateStr = Convert::getDateStr(req.startDate);
        string path = Path::combine(Application::instance()->rootPath(), "logs");
        string logFile = dateStr + ".log";
        string fullLogFile = Path::combine(path, logFile);
        if (!File::exists(fullLogFile))
        {
            resp.status = 5; // no log
        }
        else
        {
            resp.status = 0;
            pDr->sendLog(path, logFile);
        }
    }
    else if (req.programId == PROGRAM_ID_GATHERER)
    {
        // Request maintenance information
        pDr->send2Gatherer<DownloadLogReq, DownloadLogContext>(req, "DownloadLog");
    }
    else
    {
        resp.status = 1; // incorrect Program ID
    }

    pContext->setOutputData(&resp);
    return context;
}

ClientContext* DownloadLogInstruction::setValue(ClientContext* context)
{
    DownloadLogContext *pContext = (DownloadLogContext*)context;
    if (pContext->outputData()->status != 0)
    {
        string clientAddr = pContext->peerAddr();
        Trace::writeFormatLine("Warning: Request log response failed from client: %s, status = %d(0x%x)", clientAddr.c_str(), pContext->outputData()->status, pContext->outputData()->status);
    }

    return context;
}

ClientContext* ServerUploadLogInstruction::setValue(ClientContext* context)
{
    UploadLogContext *c = (UploadLogContext *)context;
    const FileHeader* header = c->header();
    const FileDatas* fds = c->outputData();
    for (uint i = 0; i < fds->count(); i++)
    {
        bool isLastPart = fds->at(i)->packetNo == header->packetCount - 1;

        if (isLastPart)
        {
            string filename = header->file_name;
            File::move(header->tempFullFileName(), header->fullFileName());

            string path = header->path;
            CDataReporter *pDr = CDataReporter::Instance();
            assert(pDr);
            pDr->sendLog(path, filename);
        }
    }
    return context;
}

ClientContext* UploadLogInstruction::setValue(ClientContext* context)
{
    return context;
}
