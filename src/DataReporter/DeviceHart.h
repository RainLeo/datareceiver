#pragma once

#include "DeviceModBus.h"

class CDeviceHart :
    public CDeviceConfiguration
{
public:
public:
    CDeviceHart() { type = DEVICE_HART; pDetail = createDetail(); }
    virtual ~CDeviceHart() {}

    CDeviceDetail * createDetail() { return new CDeviceModBusDetail(); }
};

