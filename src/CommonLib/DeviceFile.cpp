#include "DeviceFile.h"
#include "common/diag/Trace.h"

CDeviceFileDetail::CDeviceFileDetail()
{
	strDataFilePath = "";
	strConfigFilePath = "";
}

CDeviceFileDetail::~CDeviceFileDetail()
{
}

void CDeviceFileDetail::write(Stream* stream) const
{
	stream->writeInt16(strConfigFilePath.size());
	stream->writeFixedLengthStr(strConfigFilePath, strConfigFilePath.size());
	stream->writeInt16(strDataFilePath.size());
	stream->writeFixedLengthStr(strDataFilePath, strDataFilePath.size());
}

void CDeviceFileDetail::read(Stream* stream)
{
	int len = stream->readInt16();
	#if WIN32
	if (len < 0 || len > _MAX_PATH)
	#else
	if (len < 0 || len > PATH_MAX)
	#endif
	{
		Trace::writeFormatLine("Warning: data file path len (%d) is out of range.", len);
		return;
	}

	strDataFilePath = stream->readFixedLengthStr(len);
	len = stream->readInt16();
	#if WIN32
	if (len < 0 || len > _MAX_PATH)
	#else
	if (len < 0 || len > PATH_MAX)
	#endif

	{
		Trace::writeFormatLine("Warning: configuration file path len (%d) is out of range.", len);
		return;
	}
	strConfigFilePath = stream->readFixedLengthStr(len);
}
