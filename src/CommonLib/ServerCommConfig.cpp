#include "ServerCommConfig.h"
#include "common/system/TimeSpan.h"

ServerCommConfig::ServerCommConfig(const ConfigFile& file, string root) : ServerConfig(file),
    m_root(root),
    m_TagCacheCountLimit(2400)
{
}

bool ServerCommConfig::load(XmlTextReader &reader)
{
    if (reader.localName() == m_root)
    {
        while (reader.read())
        {
            if (reader.nodeType() == XmlNodeType::Element &&
                reader.localName() == "log")
            {
                _log.read(reader);
                LogTraceListener _logListener(_log);
                Trace::enableLog(&_logListener);
                Trace::enableConsoleOutput();
            }
            else if (reader.nodeType() == XmlNodeType::Element &&
                reader.localName() == "tag")
            {
                Convert::parseInt32(reader.getAttribute("cache_count_limit"), m_TagCacheCountLimit);
            }
            else if (reader.nodeType() == XmlNodeType::Element &&
                reader.localName() == "webserver")
            {
                _webserver.Name = reader.getAttribute("name");
                _webserver.Address = reader.getAttribute("address");
                Convert::parseInt32(reader.getAttribute("port"), _webserver.Port);
                Convert::parseInt32(reader.getAttribute("maxConnections"), _webserver.MaxConnections);
                Convert::parseBoolean(reader.getAttribute("enabled"), _webserver.Enabled);
                while (reader.read())
                {
                    if (reader.nodeType() == XmlNodeType::Element)
                    {
                        if (reader.localName() == "timeout")
                        {
                            TimeSpan::parse(reader.getAttribute("receive"), _webserver.ReceiveTimeout);
                            TimeSpan::parse(reader.getAttribute("send"), _webserver.SendTimeout);
                        }
                        else
                            break;
                    }
                }
            }
            
            if (reader.nodeType() == XmlNodeType::Element &&
                reader.localName() == "webclient")
            {
                _webclient.Name = reader.getAttribute("name");
                _webclient.EndPoint = reader.getAttribute("endpoint");
                TimeSpan::parse(reader.getAttribute("interval"), _webclient.Interval);
                Convert::parseBoolean(reader.getAttribute("enabled"), _webclient.Enabled);
                while (reader.read())
                {
                    if (reader.nodeType() == XmlNodeType::Element)
                    {
                        if (reader.localName() == "timeout")
                        {
                            TimeSpan::parse(reader.getAttribute("connect"), _webclient.ConnectTimeout);
                            TimeSpan::parse(reader.getAttribute("receive"), _webclient.ReceiveTimeout);
                            TimeSpan::parse(reader.getAttribute("send"), _webclient.SendTimeout);
                        }
                        else
                            break;
                    }
                }
            }

            if(reader.nodeType() == XmlNodeType::Element)
            {
                loadServerNode(reader);
            }
        }
        return true;
    }
    return false;
}
