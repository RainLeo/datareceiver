#include "ClientCommConfig.h"

ClientCommConfig::ClientCommConfig(const ConfigFile& file, string root) : ClientConfig(file),
    m_root(root),
    data_file_path(""),
    configuration_file_path(""),
    m_TagCacheSendInterval(0),
    m_TagCacheSendTimeout(0),
    m_TagCacheCountLimit(2400),
    m_TagFetchInterval(0),
    m_TagReceiveTimeout(0),
    m_MaintainInterval(3000)
{
}

bool ClientCommConfig::load(XmlTextReader &reader)
{
    if (reader.localName() == m_root)
    {
        while (reader.read())
        {
            if (reader.nodeType() == XmlNodeType::Element &&
                reader.localName() == "log")
            {
                _log.read(reader);
                LogTraceListener _logListener(_log);
                Trace::enableLog(&_logListener);
                Trace::enableConsoleOutput();
            }
            else if (reader.nodeType() == XmlNodeType::Element &&
                reader.localName() == "source")
            {
                data_file_path = reader.getAttribute("data_file_path");
                configuration_file_path = reader.getAttribute("configuration_file_path");
            }
            else if (reader.nodeType() == XmlNodeType::Element &&
                reader.localName() == "tag")
            {
                Convert::parseInt32(reader.getAttribute("cache_send_timeout"), m_TagCacheSendTimeout);
                Convert::parseInt32(reader.getAttribute("cache_send_interval"), m_TagCacheSendInterval);
                Convert::parseInt32(reader.getAttribute("cache_count_limit"), m_TagCacheCountLimit);
                Convert::parseInt32(reader.getAttribute("fetch_interval"), m_TagFetchInterval);
                Convert::parseInt32(reader.getAttribute("receive_timeout"), m_TagReceiveTimeout);
                Convert::parseInt32(reader.getAttribute("maintain_interval"), m_MaintainInterval);
            }
            else if (reader.nodeType() == XmlNodeType::Element &&
                reader.localName() == "dtu")
            {
                int iDtuPort = 0;
                Convert::parseInt32(reader.getAttribute("port"), iDtuPort);
                m_dtuPorts.push_back(iDtuPort);
                m_dtuVendors.push_back(reader.getAttribute("vendor"));
            }
            else
            {
                loadClientNode(reader);
            }
        }
        return true;
    }
    return false;
}
