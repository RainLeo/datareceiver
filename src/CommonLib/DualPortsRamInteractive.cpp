#if WIN32
#define FILE_STUB 1
#endif

#include "DualPortsRamInteractive.h"
#include "common/data/BCDUtilities.h"
#include "common/system/Math.h"
#include "common/driver/channels/Channel.h"
#include "common/driver/devices/Device.h"
#include "common/thread/Thread.h"
#if defined(FILE_STUB) && defined(WIN32)
#include "stdio.h"
#include "winsock2.h"
#else
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <arpa/inet.h>
#endif

#ifdef FILE_STUB
#define FILE_NAME "./testDualPortsRam.txt"
#endif

#define MAX_SENDING_LENGTH (65532-0x2000)		// substract 2K and also exclude indicator and length
#define MAX_CACHE_SIZE 64*1024*1024		//64M
#define BASE_ADDRESS 0x0

void DualPortsRam_receiveProc(void* parameter)
{
    DualPortsRamInteractive* ui = (DualPortsRamInteractive*)parameter;
	assert(ui);
	ui->receiveProcInner();
}


DualPortsRamInteractive::DualPortsRamInteractive(DriverManager* dm, Channel* channel) : Interactive(dm, channel),
    _receiveThread(NULL),
    _device(NULL),
#ifdef FILE_STUB
    m_file_descriptor(NULL)
#else
    m_file_descriptor(-1)
#endif
{
}

DualPortsRamInteractive::~DualPortsRamInteractive(void)
{
    close();
}

bool DualPortsRamInteractive::open()
{
    close();

    DualPortsRamChannelContext* scc = getChannelContext();
    m_port_index = scc->getPortIndex();

#ifndef FILE_STUB
    m_file_descriptor = ::open("/dev/dual_ports_ram", O_RDWR);
#endif
    if (!connected())
    {
        Trace::writeFormatLine("Failed to open /dev/dual_ports_ram, error %d", errno);
        return false;
    }

    if (m_port_index == 1)
    {
        sendBuffer(NULL, 0, 0); // init the duanl port ram
    }
    Trace::writeLine("/dev/dual_ports_ram opened");

    _receiveThread = new Thread();
#ifdef DEBUG
	_receiveThread->setName("DualPortsRam_receiveProc");
#endif
    _receiveThread->startProc(DualPortsRam_receiveProc, this, 1);

    return true;
}

void DualPortsRamInteractive::close()
{
	if(_receiveThread != NULL)
	{
		_receiveThread->stop();
		delete _receiveThread;
		_receiveThread = NULL;
	}
        
	_device = NULL;

    for (list<ByteArray *>::const_iterator ci = m_lstBuffer.begin(); ci != m_lstBuffer.end(); ci++)
    {
        delete *ci;
    }
    m_lstBuffer.clear();

    if (connected())
    {
#ifdef FILE_STUB
        fclose(m_file_descriptor);
        m_file_descriptor = NULL;
#else
        ::close(m_file_descriptor);
        m_file_descriptor = -1;
#endif
    }
}

bool DualPortsRamInteractive::connected()
{
#ifdef FILE_STUB
    int count = 0;
    while (m_file_descriptor == NULL)
    {
        m_file_descriptor = fopen(FILE_NAME, "rb+");
        count++;
        if (count > 10000)
        {
            Trace::writeFormatLine("Error open file for 10000 times.");
            return false;
        }
        Thread::msleep(1);
    }
    return true;
#else
    return m_file_descriptor != -1;
#endif
}

#if defined(FILE_STUB)
#define lseek fseek
#define read(m_file_descriptor, buffer, len) fread(buffer, len, 1, m_file_descriptor)
#define write(m_file_descriptor, buffer, len) fwrite(buffer, len, 1, m_file_descriptor)
#endif

bool DualPortsRamInteractive::ready()
{
    lseek(m_file_descriptor, BASE_ADDRESS, SEEK_SET);
    char buffer[3];

    if (read(m_file_descriptor, buffer, 2) > 0)
    {
        buffer[1] = 0;
        unsigned char index = (unsigned char)*buffer % 2;
        if (index != m_port_index)
        {
            return false;
        }
    }

    return true;
}

int DualPortsRamInteractive::available()
{
    //Locker lock(&m_mutexFile);

    if (connected() && ready())
    {
        lseek(m_file_descriptor, BASE_ADDRESS, SEEK_SET);
        char buffer[3] = { '\0', '\0', '\0' };

        lseek(m_file_descriptor, BASE_ADDRESS + 2, SEEK_SET);
        read(m_file_descriptor, buffer, 2);
        buffer[2] = 0;
        unsigned short length;
        memcpy(&length, buffer, 2);
        length = ntohs(length);
        //if (length > MAX_SENDING_LENGTH)
        //{
        //    length = 0;
        //}
        if (length)
        {
            return length;
        }
    }
    return 0;
}

int DualPortsRamInteractive::send(byte* buffer, int offset, int count)
{
#ifdef DEBUG
    Stopwatch sw("dual ports ram send", 1000);
#endif
    int len = 0;

    ByteArray *ba = new ByteArray(buffer, count);

    Locker lock(&m_mutexSendList); // make sure buffer list not read & write simultanously
    m_lstBuffer.push_back(ba);
    len = count;

    return len;
}

int DualPortsRamInteractive::receive(byte* recvbuffer, int offset, int count)
{
#ifdef DEBUG
    Stopwatch sw("DualPortsRam receive", 1000);
#endif
    //Locker lock(&m_mutexFile);

    int len = 0;
    if (connected())
    {

        lseek(m_file_descriptor, BASE_ADDRESS, SEEK_SET);
        char buffer[MAX_SENDING_LENGTH + 1];

        if (read(m_file_descriptor, buffer, 2) > 0)
        {
            buffer[1] = 0;
            unsigned char index = (unsigned char)*buffer % 2;
            if (index != m_port_index)
            {
                Trace::writeFormatLine("Dual Ports Ram receive: ID_byte: %u, port Index: %u", (unsigned char)*buffer, m_port_index);
#ifdef FILE_STUB
                fclose(m_file_descriptor);
                m_file_descriptor = NULL;
#endif
                return 0;
            }
            lseek(m_file_descriptor, BASE_ADDRESS + 2, SEEK_SET);
            read(m_file_descriptor, buffer, 2);
            buffer[2] = 0;
            unsigned short length;
            memcpy(&length, buffer, 2);
            length = ntohs(length);
            if (length > BasePacketContext::DefaultPacketLength)
            {
                Trace::writeFormatLine("Dual Ports Ram receive: buffer overflow error. Length: %u > max buffer: %u", length, BasePacketContext::DefaultPacketLength);
                length = 0;
            }
            if (length)
            {
                int nPos = 0;
                while (length > 0)
                {
                    while (connected() && !ready())
                    {
                    }
                    lseek(m_file_descriptor, BASE_ADDRESS + 2, SEEK_SET);
                    read(m_file_descriptor, buffer, 2);
                    buffer[2] = 0;
                    memcpy(&length, buffer, 2);
                    length = ntohs(length);

                    int recvBufferLen = length;
                    if (length > MAX_SENDING_LENGTH)
                    {
                        recvBufferLen = MAX_SENDING_LENGTH;
                        Debug::writeFormatLine("Dual Ports Ram recv length:%u and to be continued...", recvBufferLen);
                    }
                    else
                    {
                        Debug::writeFormatLine("Dual Ports Ram recv length:%u", recvBufferLen);
                    }
                    lseek(m_file_descriptor, BASE_ADDRESS + 4, SEEK_SET);
                    read(m_file_descriptor, buffer, recvBufferLen);
                    buffer[recvBufferLen] = 0;
                    memcpy(recvbuffer + nPos, buffer, recvBufferLen);
#ifdef DEBUG
                    ByteArray ba(recvbuffer + nPos, Math::min(recvBufferLen, 128));
                    Debug::writeFormatLine("<==== %s", ba.toString().c_str());
#endif
                    nPos += recvBufferLen;
                    length -= recvBufferLen;
                    if (length > 0)
                    {
                        sendBuffer(NULL, 0, 0); // prepare for next recv
                    }
                }
                len = nPos;
            }
            sendBuffer();
        }
    }
    return len;
}

void DualPortsRamInteractive::sendBuffer()
{
    if (!connected()) return;

    size_t nSendingLen = 0;
    ByteArray *pSendingBuffer = NULL;
    {
        Locker lock(&m_mutexSendList); // make sure buffer list not read & write simultanously
        if (m_lstBuffer.size() > 0)
        {
            pSendingBuffer = m_lstBuffer.front();
            m_lstBuffer.pop_front();
            nSendingLen = pSendingBuffer->count();
        }
    }
    if (nSendingLen)
    {
        int nPos = 0;
        while (nSendingLen)
        {
            Debug::writeFormatLine("Dual Ports Ram sending length:%u", nSendingLen);
            int sendingBufferLen = nSendingLen;
            if (sendingBufferLen > MAX_SENDING_LENGTH)
            {
                sendingBufferLen = MAX_SENDING_LENGTH;
            }
            byte *buffer = &(pSendingBuffer->data()[nPos]);
            while (connected() && !ready())
            {
            }
            sendBuffer(buffer, nSendingLen, sendingBufferLen);
#ifdef DEBUG
            ByteArray ba(buffer, Math::min((int)sendingBufferLen, 128));
            Debug::writeFormatLine("===> %s", ba.toString().c_str());
#endif
            nSendingLen -= sendingBufferLen;
            nPos += sendingBufferLen;
        }
    }
    else
    {
        sendBuffer(NULL, 0, 0);
    }

    if (pSendingBuffer != NULL)
    {
        delete pSendingBuffer;
    }
}

void DualPortsRamInteractive::sendBuffer(byte *buffer, int nTotalLen, int nCurLen)
{
    // Length
    lseek(m_file_descriptor, BASE_ADDRESS + 2, SEEK_SET);
    if (nTotalLen > MAX_SENDING_LENGTH)
    {
        nTotalLen = MAX_SENDING_LENGTH + 1;
    }
    unsigned short net_length = htons((unsigned short)(nTotalLen));
    write(m_file_descriptor, &net_length, 2);
    // Message content
    if (nCurLen > 0 && buffer != NULL)
    {
        write(m_file_descriptor, buffer, nCurLen);
    }
    // Header: port index
    lseek(m_file_descriptor, BASE_ADDRESS, SEEK_SET);
    unsigned char peer_index[2];
    peer_index[0] = m_port_index ? 0 : 1;
    peer_index[1] = 0;
    write(m_file_descriptor, peer_index, 2);

#ifdef FILE_STUB
    fclose(m_file_descriptor);
    m_file_descriptor = NULL;
#endif
}

void DualPortsRamInteractive::receiveProcInner()
{
#ifdef DEBUG
	Stopwatch sw("receiveProc", 1000);
#endif
	if(_device == NULL)
	{
		DriverManager* dm = manager();
		assert(dm);
		_device = dm->getDevice(_channel);
	}
        
    //Locker lock(&m_mutexFile);
    if (connected() && ready())
    {
        if (available() > 0)
		{
            receiveFromBuffer(_device);
        }

        //Locker lock(&m_mutexFile);

        if (connected() && ready())
        {
            sendBuffer();
        }
    }
}

DualPortsRamInstructionSet::DualPortsRamInstructionSet(void* owner, instructions_callback action)
{
    _owner = owner;
    if (action == NULL)
        throw ArgumentException("action");
    _instructionsCallback = action;
}

DualPortsRamInstructionSet::~DualPortsRamInstructionSet()
{
}

void DualPortsRamInstructionSet::generateInstructions(Instructions* instructions)
{
    if (_instructionsCallback != NULL)
    {
        _instructionsCallback(_owner, instructions);
    }
}

bool DualPortsRamInstructionSet::receive(Device* device, Channel* channel, ByteArray* buffer)
{
    bool result = false;
    if (NULL == channel)
    {
        return result;
    }
    if (!channel->connected())
    {
        return result;
    }

    uint timeout = device->description()->receiveTimeout();

    const int BufferLength = BasePacketContext::DefaultPacketLength;
    byte* rbuffer = new byte[BufferLength];
    memset(rbuffer, 0, BasePacketContext::DefaultPacketLength);
    int bufferLen = channel->receiveBySize(rbuffer, BufferLength, ClientContext::HeaderLength, timeout);
    if (ClientContext::HeaderLength > bufferLen || rbuffer[ClientContext::HeaderPosition] != ClientContext::Header)
    {
        delete[] rbuffer;
        return result;
    }
    int validLength = (int)BCDUtilities::BCDToInt64(rbuffer, 2, ClientContext::BufferBCDLength);
    if (validLength + ClientContext::HeaderLength == bufferLen)
    {
        buffer->addRange(rbuffer, bufferLen);
        result = true;
    }
    else
    {
        ByteArray messages(rbuffer, Math::min(bufferLen, 128));
        Debug::writeFormatLine("error: CommError! recv: %s", messages.toString().c_str());
    }

    delete[] rbuffer;
    return result;
}
