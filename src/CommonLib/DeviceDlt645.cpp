#include "DeviceDlt645.h"


void CDeviceDlt645Detail::write(Stream* stream) const
{
    CDeviceSerialDetail::write(stream);
    stream->writeStr(address);
}

void CDeviceDlt645Detail::read(Stream* stream)
{
    CDeviceSerialDetail::read(stream);
    address = stream->readStr();
}

void CDeviceDlt645Detail::parse(XmlTextReader &reader)
{
    address = reader.getAttribute("address");
    CDeviceSerialDetail::parse(reader);
}