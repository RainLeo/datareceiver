#include "interface.h"

bool Alarm::m_sbHighCpu = false;
int Alarm::m_iCount = 0;

void Alarm::CheckHighCpu(int cpu)
{
    if (cpu < ALARM_HIGH_CPU_THRESHOLD)
    {
        m_sbHighCpu = false;
        m_iCount = 0;
    }
    else
    {
        if (m_iCount < ALARM_CHECK_TIMES)
        {
            m_iCount++;
        }
        else
        {
            m_iCount = 0;
            m_sbHighCpu = true;
        }
    }
}

bool Alarm::IsHighCpu()
{
    return m_sbHighCpu;
}
