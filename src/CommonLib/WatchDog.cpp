//#include "stdafx.h"
#include "WatchDog.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <linux/watchdog.h>
#include <sys/ioctl.h>
#include <unistd.h>

void staticFeedProc(void* parameter)
{
    CWatchDog* pWatchDog = (CWatchDog*)parameter;
    pWatchDog->Feed();
}

CWatchDog::CWatchDog()
{
    m_pTimerFeed = new Timer(staticFeedProc, CWatchDog::Instance(), 10000); //10 secs

    m_fd = open("/dev/watchdog", O_WRONLY);
	int interval = 30;
	ioctl(m_fd, WDIOC_SETTIMEOUT, &interval);
}


CWatchDog::~CWatchDog()
{
	close(m_fd);
}

void CWatchDog::Feed(void)
{
	write(m_fd, "A", 1);
}
