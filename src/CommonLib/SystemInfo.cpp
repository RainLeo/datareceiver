#include <string>
#if WIN32
#include <Windows.h>
#else
#include <sys/vfs.h>
#include <unistd.h> 
#include "sys/sysinfo.h"
#include <string.h>
#include <signal.h>
#endif
#include "common/system/Version.h"

using namespace std;

// Global app version
Common::Version AppVersion(2, 2, 17, 314);

// return free Mb
int getDiskInfo()
{
#if WIN32
    string path = "C:";
    ULARGE_INTEGER available, total, free;
    if (GetDiskFreeSpaceEx(path.c_str(), &available, &total, &free))
    {
        return (int)(available.QuadPart / 1024 / 1024);
    }
    return 0;
#else
    struct statfs diskInfo;
    statfs("/", &diskInfo);
    unsigned long long totalBlocks = diskInfo.f_bsize;
    //unsigned long long totalSize = totalBlocks * diskInfo.f_blocks;
    //size_t mbTotalsize = totalSize >> 20;
    unsigned long long freeDisk = diskInfo.f_bfree * totalBlocks;
    size_t mbFreedisk = freeDisk >> 20;
    return mbFreedisk;
#endif
}

// return Disk usage percentage
int getDiskUsagePercentage()
{
#if WIN32
    string path = "C:";
    ULARGE_INTEGER available, total, free;
    if (GetDiskFreeSpaceEx(path.c_str(), &available, &total, &free) && total.QuadPart != 0)
    {
        return (int)((total.QuadPart - available.QuadPart) * 100 / total.QuadPart);
    }
    return 0;
#else
    struct statfs diskInfo;
    statfs("/", &diskInfo);
    unsigned long long totalBlocks = diskInfo.f_bsize;
    //unsigned long long totalSize = totalBlocks * diskInfo.f_blocks;
    //size_t mbTotalsize = totalSize >> 20;
    double all = diskInfo.f_blocks - diskInfo.f_bfree + diskInfo.f_bavail;
    if (0 != all)
    {
        return (diskInfo.f_blocks - diskInfo.f_bfree) * 100 / all + 1;
    }

    return 0;
#endif
}

#if WIN32
typedef   void(WINAPI*   FunctionGlobalMemoryStatusEx)(LPMEMORYSTATUSEX); //function pointer to GlobalMemoryStatusEx
void GetMemoryStatus(LPMEMORYSTATUSEX pStatus)
{
    FunctionGlobalMemoryStatusEx GlobalMemoryStatusEx;
    MEMORYSTATUSEX &status = *pStatus;

    status.dwLength = sizeof(status);
    GlobalMemoryStatusEx = (FunctionGlobalMemoryStatusEx)GetProcAddress(GetModuleHandle("kernel32"), "GlobalMemoryStatusEx");
    if (NULL == GlobalMemoryStatusEx)//if function not found
    {
        //error
        return;
    }

    GlobalMemoryStatusEx(&status);//get the memory status
    return;
}
#else
typedef struct CPU_OCCUPY_STRU         //cpu occupy struct definition
{
    char name[20];
    unsigned int user;
    unsigned int nice;
    unsigned int system;
    unsigned int idle; 
}CPU_OCCUPY;

typedef struct MEM_OCCUPY_STRU         //mem occupy struct definition
{
    char name[20];
    unsigned long total;
    char name2[20];
    unsigned long free;
} MEM_OCCUPY;

void get_memoccupy(MEM_OCCUPY *mem)
{
    FILE *fd;
    char buff[256];
    MEM_OCCUPY *m;
    m = mem;

    fd = fopen("/proc/meminfo", "r");

    //read size of buff into buff from fd file until a new line or end of file
    fgets(buff, sizeof(buff), fd);
    sscanf(buff, "%s %lu %s", m->name, &m->total, m->name2);

    fgets(buff, sizeof(buff), fd);
    sscanf(buff, "%s %lu %s", m->name, &m->free, m->name2);

    // Free memory should also include buffers and cached memory
    unsigned long bufferCachedMem = 0;
    fgets(buff, sizeof(buff), fd);
    sscanf(buff, "%s %lu %s", m->name, &bufferCachedMem, m->name2);
    m->free += bufferCachedMem; // buffers
    fgets(buff, sizeof(buff), fd);
    sscanf(buff, "%s %lu %s", m->name, &bufferCachedMem, m->name2);
    m->free += bufferCachedMem; // cached

    fclose(fd);     //close file fd
}

int cal_cpuoccupy(CPU_OCCUPY *o, CPU_OCCUPY *n)
{
    unsigned long od, nd;
    unsigned long id, sd;
    int cpu_use = 0;

    od = (unsigned long)(o->user + o->nice + o->system + o->idle);//first (user+priority+system+idle) => od
    nd = (unsigned long)(n->user + n->nice + n->system + n->idle);//second(user+priority+system+idle) => od

    id = (unsigned long)(n->user - o->user);    //user   second - first => id
    sd = (unsigned long)(n->system - o->system);//system second - first => sd
    if ((nd - od) != 0)
        cpu_use = (int)((sd + id) * 100 + 0.5) / (nd - od); //((user+system)x100)/(interval) => cpu_use
    else cpu_use = 0;
    //printf("cpu: %u\n",cpu_use);
    return cpu_use;
}

void get_cpuoccupy(CPU_OCCUPY *cpust)
{
    FILE *fd;
    char buff[256];
    CPU_OCCUPY *cpu_occupy;
    cpu_occupy = cpust;

    fd = fopen("/proc/stat", "r");
    fgets(buff, sizeof(buff), fd);

    sscanf(buff, "%s %u %u %u %u", cpu_occupy->name, &cpu_occupy->user, &cpu_occupy->nice, &cpu_occupy->system, &cpu_occupy->idle);

    fclose(fd);
}

#define VMRSS_LINE 15// VMRSS line #

int get_phy_mem(const pid_t p)
{
    char file[64] = { 0 }; //filename

    FILE *fd;         //file pointer
    char line_buff[256] = { 0 };  //buffer to read a line
    sprintf(file, "/proc/%d/status", p);

    fd = fopen(file, "r"); //read in file

    //get vmrss:real phy mem occupy
    int i;
    char name[32];//name
    int vmrss;//mem
    for (i = 0; i < VMRSS_LINE - 1; i++)
    {
        fgets(line_buff, sizeof(line_buff), fd);
    }//read to the 15th line
    fgets(line_buff, sizeof(line_buff), fd);//read VmRSS,VmRSS on the 15th line
    sscanf(line_buff, "%s %d", name, &vmrss);

    fclose(fd);     //رļfd
    return vmrss;
}
#endif

int getMemInfo(int &used)
{
#if WIN32
    MEMORYSTATUSEX memStatus;
    GetMemoryStatus(&memStatus);
    // The following function only returns the memory on the first slot (e.g. only 2G), NOT the total mem (e.g. 4 mem sticks, total 8G)
    //MEMORYSTATUS memStatus;
    //GlobalMemoryStatus(&memStatus);
    used = (int)((memStatus.ullTotalPhys - memStatus.ullAvailPhys) / 1024 / 1024);
    return (int)(memStatus.ullAvailPhys / 1024 / 1024);
#else
    MEM_OCCUPY mem_stat;

    //get memory
    get_memoccupy((MEM_OCCUPY *)&mem_stat);
    used = (mem_stat.total - mem_stat.free) / 1024;//get_phy_mem(getpid());
    return mem_stat.free / 1024;
#endif
}

#if WIN32
typedef struct
{
    DWORD   dwUnknown1;
    ULONG   uKeMaximumIncrement;
    ULONG   uPageSize;
    ULONG   uMmNumberOfPhysicalPages;
    ULONG   uMmLowestPhysicalPage;
    ULONG   uMmHighestPhysicalPage;
    ULONG   uAllocationGranularity;
    PVOID   pLowestUserAddress;
    PVOID   pMmHighestUserAddress;
    ULONG   uKeActiveProcessors;
    BYTE    bKeNumberProcessors;
    BYTE    bUnknown2;
    WORD    wUnknown3;
} SYSTEM_BASIC_INFORMATION;

typedef struct
{
    LARGE_INTEGER   liIdleTime;
    DWORD           dwSpare[76];
} SYSTEM_PERFORMANCE_INFORMATION;

typedef struct
{
    LARGE_INTEGER liKeBootTime;
    LARGE_INTEGER liKeSystemTime;
    LARGE_INTEGER liExpTimeZoneBias;
    ULONG         uCurrentTimeZoneId;
    DWORD         dwReserved;
} SYSTEM_TIME_INFORMATION;
typedef LONG(WINAPI *PROCNTQSI)(UINT, PVOID, ULONG, PULONG);

PROCNTQSI NtQuerySystemInformation;

#define SystemBasicInformation       0
#define SystemPerformanceInformation 2
#define SystemTimeInformation        3
#define Li2Double(x) ((double)((x).HighPart) * 4.294967296E9 + (double)((x).LowPart))
#endif

int getCpuInfo()
{
#if WIN32
    SYSTEM_PERFORMANCE_INFORMATION SysPerfInfo;
    SYSTEM_TIME_INFORMATION        SysTimeInfo;
    SYSTEM_BASIC_INFORMATION       SysBaseInfo;
    double                         dbIdleTime;
    double                         dbSystemTime;
    LONG                           status;
    static LARGE_INTEGER                  liOldIdleTime = {0,0};
    static LARGE_INTEGER                  liOldSystemTime = {0,0};
    int UsageCpu = 0;
    NtQuerySystemInformation = (PROCNTQSI)GetProcAddress(
        GetModuleHandle("ntdll"),
        "NtQuerySystemInformation"
        );

    if (!NtQuerySystemInformation)
        return 0;

    status = NtQuerySystemInformation(SystemBasicInformation,&SysBaseInfo,sizeof(SysBaseInfo),NULL);
    if (status != NO_ERROR)
        return 0;

    status = NtQuerySystemInformation(SystemTimeInformation, &SysTimeInfo, sizeof(SysTimeInfo), 0);
    if (status != NO_ERROR)
        return 0;

    status = NtQuerySystemInformation(SystemPerformanceInformation, &SysPerfInfo, sizeof(SysPerfInfo), NULL);
    if (status != NO_ERROR)
        return 0;

    if (liOldIdleTime.QuadPart != 0)
    {
        dbIdleTime = Li2Double(SysPerfInfo.liIdleTime) - Li2Double(liOldIdleTime);
        dbSystemTime = Li2Double(SysTimeInfo.liKeSystemTime) - Li2Double(liOldSystemTime);

        dbIdleTime = dbIdleTime / dbSystemTime;


        dbIdleTime = 100.0 - dbIdleTime * 100.0 / (double)SysBaseInfo.bKeNumberProcessors + 0.5;
        UsageCpu = (int)dbIdleTime;
    }

    // store new CPU's idle and system time
    liOldIdleTime = SysPerfInfo.liIdleTime;
    liOldSystemTime = SysTimeInfo.liKeSystemTime;

    return UsageCpu;
#else
    static CPU_OCCUPY cpu_stat1;
    CPU_OCCUPY cpu_stat2;
    int cpu;


    //first cpu info
    //get_cpuoccupy((CPU_OCCUPY *)&cpu_stat1);
    //sleep(10);

    //second cpu info
    get_cpuoccupy((CPU_OCCUPY *)&cpu_stat2);

    //calculate cpu usage
    cpu = cal_cpuoccupy((CPU_OCCUPY *)&cpu_stat1, (CPU_OCCUPY *)&cpu_stat2);

    cpu_stat1 = cpu_stat2;

    return cpu;
#endif
}

#if !WIN32
int us_os_info_get_memory_usage(int & physMemUsed)
{
    int retv = 0;

    struct sysinfo memInfo;

    memset(&memInfo, 0, sizeof(struct sysinfo));

    retv = sysinfo(&memInfo);

    if (0 != retv)
    {
        // return 0 when sysinfo failed.
        return 0;
    }

    physMemUsed = (memInfo.totalram - memInfo.freeram) / 1024 / 1024;
    return memInfo.freeram / 1024 / 1024;
}

bool adjustdate(int year, int mon, int mday, int hour, int min, int sec)
{
    time_t t;
    struct  tm nowtime;
    nowtime.tm_sec = sec;/* Seconds.[0-60] (1 leap second)*/
    nowtime.tm_min = min;/* Minutes.[0-59] */
    nowtime.tm_hour = hour;/* Hours.[0-23] */
    nowtime.tm_mday = mday;/* Day.[1-31] */
    nowtime.tm_mon = mon - 1;/* Month.[0-11] */
    nowtime.tm_year = year - 1900;/* Year- 1900.*/
    nowtime.tm_isdst = -1;/* DST.[-1/0/1]*/
    t = mktime(&nowtime);
    return stime(&t) == 0;
}

bool procExisted(char *name) /* Proc name */
{
    FILE *pstr;
    char cmd[128], buff[512];
    pid_t pID;
    int pidnum;
    char *p = NULL;
    int ret = 3;
    memset(cmd, 0, sizeof(cmd));
    sprintf(cmd, "ps -ef|grep %s", name);
    pstr = popen(cmd, "r");
    if (pstr == NULL)
    {
        return false;
    }
    memset(buff, 0, sizeof(buff));
    fgets(buff, 512, pstr);
    p = strtok(buff, " ");
    p = strtok(NULL, " ");
    pclose(pstr); //这句是否去掉，取决于当前系统中ps后，进程ID号是否是第一个字段
    if (p == NULL)
    {
        return false;
    }

    if (strlen(p) == 0)
    {
        return false;
    }
    if ((pidnum = atoi(p)) == 0)
    {
        return false;
    }

    pID = (pid_t)pidnum;
    ret = kill(pID, 0);

    return (0 == ret); // exists?
}
#endif
