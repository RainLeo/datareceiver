#include "TagConfiguration.h"
#include "common/diag/Trace.h"
#include "common/diag/Stopwatch.h"

TagConfiguration::TagConfiguration()
{
}

TagConfiguration::~TagConfiguration()
{
    Clear();
}

void TagConfiguration::write(Stream* stream) const
{
#ifdef DEBUG
    Stopwatch sw("TagConfiguration");
#endif
    stream->writeInt32(iFetchInterval);
    stream->writeInt16(vecDevices.size());
    for (uint i = 0; i < vecDevices.size(); i++)
    {
        const CDeviceConfiguration *pDevConfig = vecDevices[i];
        pDevConfig->write(stream);
    }
}

void TagConfiguration::read(Stream* stream)
{
    Clear();
    iFetchInterval = stream->readInt32();
    int size = stream->readInt16();
    if (size < 0 || size > MAX_DEVICE_NUM)
    {
        Trace::writeFormatLine("Warning: device count (%d) is out of range.", size);
        return;
    }
    vecDevices = vector<CDeviceConfiguration *>();
    for (int i = 0; i < size; i++)
    {
        CDeviceConfiguration *pDevConfig = new CDeviceConfiguration();
        pDevConfig->read(stream);
        vecDevices.push_back(pDevConfig);
    }
}

void TagConfiguration::copyFrom(const TagConfiguration* value)
{
    Clear();
    iFetchInterval = value->iFetchInterval;
    for (uint i = 0; i < value->vecDevices.size(); i++)
    {
        CDeviceConfiguration *pDevConfig = value->vecDevices[i]->Clone();
        vecDevices.push_back(pDevConfig);
    }
}

void TagConfiguration::Clear()
{
    for (uint i = 0; i < vecDevices.size(); i++)
    {
        if (vecDevices[i] != NULL)
        {
            delete vecDevices[i];
            vecDevices[i] = NULL;
        }
    }
    vecDevices.clear();
}

TagConfiguration& TagConfiguration::operator=(const TagConfiguration &rhs)
{
    copyFrom(&rhs);
    return *this;
}

bool TagConfiguration::IsValid()
{
    return vecDevices.size() > 0;
}

bool TagConfiguration::AddDevice(CDeviceConfiguration *pDeviceConfiguration)
{
    if (pDeviceConfiguration == NULL) return false;

    if (vecDevices.size() > MAX_DEVICE_NUM)
    {
        Trace::writeFormatLine("Warning: device count is out of range (%d).", MAX_DEVICE_NUM);
        return false;
    }

    if (!pDeviceConfiguration->IsValid())
    {
        Trace::writeFormatLine("Warning: device is invalid.");
        return false;
    }
    else
    {
        Trace::writeFormatLine("%d Tags are loaded.", pDeviceConfiguration->vecTags.size());
    }

    vecDevices.push_back(pDeviceConfiguration);
    return true;
}
