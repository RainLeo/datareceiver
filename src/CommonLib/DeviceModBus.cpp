#include "DeviceModBus.h"
#include "common/IO/SerialInfo.h"

void CDeviceSerialDetail::write(Stream* stream) const
{
    stream->writeByte(communicationType);
    stream->writeStr(dtuId);
    stream->writeByte(serialIndex);
    stream->writeInt32(serialBaudRate);
    stream->writeByte(serialCharacterSize);
    stream->writeByte(serialStopBits);
    stream->writeByte(serialParity);
}

void CDeviceSerialDetail::read(Stream* stream)
{
    communicationType = stream->readByte();
    dtuId = stream->readStr();
    serialIndex = stream->readByte();
    serialBaudRate = stream->readInt32();
    serialCharacterSize = stream->readByte();
    serialStopBits = stream->readByte();
    serialParity = stream->readByte();
}

void CDeviceSerialDetail::parse(XmlTextReader &reader)
{
    communicationType = parseCommunicationType(reader.getAttribute("communication_type"));
    dtuId = reader.getAttribute("dtu_id");
    Convert::parseByte(reader.getAttribute("serial_index"), serialIndex);
    Convert::parseInt32(reader.getAttribute("serial_baud_rate"), serialBaudRate);
    Convert::parseByte(reader.getAttribute("serial_character_size"), serialCharacterSize);
    Convert::parseByte(reader.getAttribute("serial_stop_bits"), serialStopBits);
    string parity = reader.getAttribute("serial_parity");
    if (!Convert::parseByte(parity, serialParity))
    {
        serialParity = (byte)SerialInfo::parseParity(parity);
    }
}

void CDeviceModBusDetail::write(Stream* stream) const
{
    CDeviceSerialDetail::write(stream);
    stream->writeByte(address);
    stream->writeByte(mode);
}

void CDeviceModBusDetail::read(Stream* stream)
{
    CDeviceSerialDetail::read(stream);
    address = stream->readByte();
    mode = stream->readByte();
}

void CDeviceModBusDetail::parse(XmlTextReader &reader)
{
    mode = parseMode(reader.getAttribute("mode"));
    CDeviceSerialDetail::parse(reader);
}