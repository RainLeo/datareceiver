#include "DeviceConfiguration.h"
#include "DeviceFile.h"
#include "DeviceModBus.h"
#include "DeviceDlt645.h"
#include "common/diag/Trace.h"

CDeviceConfiguration::CDeviceConfiguration()
{
    szDeviceId[0] = '\0';
    type = 0;
    pDetail = NULL;
}

CDeviceConfiguration* CDeviceConfiguration::Clone()
{
    CDeviceConfiguration *pDeviceConfiguration = new CDeviceConfiguration();
    memcpy(pDeviceConfiguration->szDeviceId, szDeviceId, sizeof(szDeviceId));
    pDeviceConfiguration->type = type;
    pDeviceConfiguration->pDetail = pDetail->Clone();
    pDeviceConfiguration->vecTags = vecTags;

    return pDeviceConfiguration;
}

CDeviceConfiguration::~CDeviceConfiguration()
{
    if (pDetail != NULL)
    {
        delete pDetail;
        pDetail = NULL;
    }
};

CDeviceDetail* CDeviceConfiguration::createDetail()
{
    return NULL; 
}

void CDeviceConfiguration::write(Stream* stream) const
{
    stream->write((const byte *)szDeviceId, 0, sizeof(szDeviceId));
    stream->writeByte(type);
    pDetail->write(stream);
    // Tags
    stream->writeInt16(vecTags.size());
    for (uint i = 0; i < vecTags.size(); i++)
    {
        const DeviceTag &tag = vecTags[i];
        stream->writeStr(tag.address);
        stream->writeInt16(tag.strName.size());
        stream->writeFixedLengthStr(tag.strName, tag.strName.size());
        stream->writeInt32(tag.tagId);
        stream->writeByte(tag.type);
        stream->writeInt16(tag.strRegisterName.size());
        stream->writeFixedLengthStr(tag.strRegisterName, tag.strRegisterName.size());
        stream->writeInt16(tag.sStartAddr);
        stream->writeInt16(tag.sUnitLen);
        stream->writeByte(tag.function);
        stream->writeBoolean(tag.bigEndian);
        stream->writeBoolean(tag.inverse);
    }
}

void CDeviceConfiguration::read(Stream* stream)
{
    stream->read((byte *)szDeviceId, 0, sizeof(szDeviceId));
    type = stream->readByte();
    switch (type)
    {
    case DEVICE_FILE:
        pDetail = new CDeviceFileDetail();
        break;
    case DEVICE_MODBUS:
    case DEVICE_HART:
        pDetail = new CDeviceModBusDetail();
        break;
    case DEVICE_DL_T645:
        pDetail = new CDeviceDlt645Detail();
        break;
    default:
        Trace::writeFormatLine("Error: device type (%d) has not been implemented yet.", type);
        return;
    }
    pDetail->read(stream);
    // Tags
    int size = stream->readInt16();
    if (size < 0 || size > MAX_TAG_NUM)
    {
        Trace::writeFormatLine("Warning: tag count (%d) is out of range.", size);
        return;
    }
    vecTags = vector<DeviceTag>(size);
    for (uint i = 0; i < vecTags.size(); i++)
    {
        DeviceTag &tag = vecTags[i];
        tag.address = stream->readStr();
        // name
        int len = stream->readInt16();
        if (len < 0 || len > MAX_NAME_LEN)
        {
            Trace::writeFormatLine("Warning: tag name len (%d) is out of range.", len);
            return;
        }
        tag.strName = stream->readFixedLengthStr(len);
        tag.tagId = stream->readInt32();
        // type
        tag.type = stream->readByte();
        // register name
        len = stream->readInt16();
        if (len < 0 || len > MAX_NAME_LEN)
        {
            Trace::writeFormatLine("Warning: register name len (%d) is out of range.", len);
            return;
        }
        tag.strRegisterName = stream->readFixedLengthStr(len);
        tag.sStartAddr = stream->readInt16();
        tag.sUnitLen = stream->readInt16();
        tag.function = stream->readByte();
        tag.bigEndian = stream->readBoolean();
        tag.inverse = stream->readBoolean();
    }
}

bool CDeviceConfiguration::IsValid()
{
    return vecTags.size() > 0;
}

bool CDeviceConfiguration::AddTag(DeviceTag &tag)
{
    if (vecTags.size() > MAX_TAG_NUM)
    {
        Trace::writeFormatLine("Warning: Tag number is out of limit (%d).", MAX_TAG_NUM);
        return false;
    }

    vecTags.push_back(tag);

    return true;
}