#include "TagValueCache.h"
#include "common/IO/FileInfo.h"
#include "common/IO/FileStream.h"
#include "common/IO/File.h"
#include "common/IO/Directory.h"
#include "common/diag/Debug.h"
#include "common/IO/Path.h"
#include "common/system/Application.h"
#include "common/thread/Thread.h"
#include <algorithm>

void staticReconnectProc(void* parameter)
{
    TagValueCache* pThis = (TagValueCache*)parameter;
    pThis->ReconnectProc();
}

TagValueCache::TagValueCache() :
    m_pReconnectTimer(NULL),
    m_pThread(NULL),
    m_bIsCached(false),
    m_saveFuncSync(false),
    m_cacheBufferSizeOffset(0),
    m_reqNo(0),
    m_sendTimeout(15000),
    m_sendInterval(3000)
{
    m_cacheFilesDir = Path::combine(Application::instance()->rootPath(), "TagCache");
    if (!Directory::exists(m_cacheFilesDir))
    {
        Directory::createDirectory(m_cacheFilesDir);
    }
    else
    {
        // Remove all cached files if app is restarted, in the future it may be good to consider to improve this to restore the files data but make sure no any new tag is sent before these cached files.
        StringArray files;
        Directory::getFiles(m_cacheFilesDir, "*.*", SearchOption::TopDirectoryOnly, files);
        for (uint i = 0; i < files.count(); i++)
        {
            File::deleteFile(files[i]);
        }
    }
    hEvent = event_create(true, false);
}

TagValueCache::~TagValueCache()
{
    ClearCache();
    if (m_pReconnectTimer != NULL)
    {
        delete m_pReconnectTimer;
        m_pReconnectTimer = NULL;
    }
    if (m_pThread != NULL)
    {
        delete m_pThread;
        m_pThread = NULL;
    }
    if (hEvent != NULL)
    {
        event_destroy(hEvent);
    }
}

void TagValueCache::ClearCache()
{
    for (list<TagValueReq *>::const_iterator ci = m_lstTagValue.begin(); ci != m_lstTagValue.end(); ci++)
    {
        if (*ci != NULL)
        {
            delete *ci;
        }
    }
    m_lstTagValue.clear();
}

void TagValueCache::SortFiles(StringArray &files)
{
    // sort the file names alphabetically: only for linux which is not sorted by OS
    for (uint i = 0; i < files.count(); i++)
    {
        for (uint j = 1; j < files.count(); j++)
        {
            if (files[j - 1] > files[j])
            {
                // swap
                string tmp = files[j];
                files.set(j, files[j - 1]);
                files.set(j - 1, tmp);
            }
        }
    }
}

void TagValueCache::WriteTags(const TagValueReq & tagValueReq)
{
    if (m_saveFunc == NULL) return; // bail out

    if (!m_bIsCached && m_saveFuncSync)
    {
        if (m_saveFunc(tagValueReq)) // success
        {
            Trace::writeFormatLine("%u Tags gathered successfully.", tagValueReq.tagValues.size());
            return;
        }
    }

    // go to cache process if already in cache or writeTag failed
    TagValueReq *pTag = new TagValueReq();
    pTag->copyFrom(&tagValueReq);

    Locker locker(&m_streamMutex);
    m_lstTagValue.push_back(pTag);
    m_bIsCached = true;
    Trace::writeFormatLine("%u Tags received.", tagValueReq.tagValues.size());

    if (m_lstTagValue.size() >= MAX_TAG_COUNT + m_cacheBufferSizeOffset)
    {
        // if files are cached for too long period, delete first one (usually by alphabetical order <in NTFS or CDFS> or written time <in FAT> but not guaranteed)
        StringArray files;
        Directory::getFiles(m_cacheFilesDir, "*.*", SearchOption::TopDirectoryOnly, files);
        if (files.count() > (uint)MAX_FILE_COUNT)
        {
            SortFiles(files);
            File::deleteFile(files[0]);
            Trace::writeFormatLine("Too many tag cache files (> %d). Deleted '%s'.", MAX_FILE_COUNT, files[0].c_str());
        }

        // create new file for cache
        string fileNameBase = DateTime::now().toString().c_str();
        replace(fileNameBase.begin(), fileNameBase.end(), ':', '-');
        fileNameBase = Path::combine(m_cacheFilesDir, fileNameBase);
        string fileName = fileNameBase;
        int index = 1;
        while (File::exists(fileName))
        {
            index++;
            fileName = fileNameBase + "_" + Convert::convertStr(index);
        }
        FileStream fs(fileName, FileMode::FileCreate, FileAccess::FileReadWrite);
        Trace::writeFormatLine("Tag cache file created: '%s'.", fileName.c_str());
        for (list<TagValueReq *>::const_iterator ci = m_lstTagValue.begin(); ci != m_lstTagValue.end(); ci++)
        {
            if (*ci != NULL)
            {
                (*ci)->write(&fs);
            }
        }

        ClearCache();
    }

    if (m_pThread != NULL)
    {
        if (m_pThread->isAlive())
        {
            return;
        }
        delete m_pThread;
        m_pThread = NULL;
    }
    m_pThread = new Thread();
    m_pThread->setName("TagCacheThread");
    m_pThread->start(staticReconnectProc, this);
    //m_pReconnectTimer = new Timer(staticReconnectProc, this, 60000);
}

bool TagValueCache::secureSaveFunc(TagValueReq &tagValueReq)
{
    if (hEvent == NULL || m_saveFuncSync)
    {
        return m_saveFunc(tagValueReq); // no wait event or SaveFunc directly returns value
    }

    event_reset(hEvent);
    m_reqNo++;
    tagValueReq.reqNo = m_reqNo; // added req # to match the response later
    if (!m_saveFunc(tagValueReq))
    {
        return false;
    }
    int ret = event_timedwait(hEvent, m_sendTimeout); // wait 15 sec for response
    if (ret == 0)
    {
        Trace::writeFormatLine("%u Tags sent successfully.", tagValueReq.tagValues.size());
        return true;
    }
    else if (ret == 1) // timeout
    {
        Trace::writeFormatLine("Timeout error in sending tags!");
    }
    else
    {
        Trace::writeFormatLine("Event error in sending tags!");
    }
    return false;
}

void TagValueCache::SetConfig(int sendTimeout, int sendInterval, int countLimit)
{
    if (sendTimeout != 0)
    {
        m_sendTimeout = sendTimeout;
    }
    if (sendInterval != 0)
    {
        m_sendInterval = sendInterval;
    }
    if (countLimit != 0)
    {
        int countPerFile = MAX_TAG_COUNT + m_cacheBufferSizeOffset;
        MAX_FILE_COUNT = (countLimit + countPerFile - 1) / countPerFile;
        Trace::writeFormatLine("Tag cache count limit changed to %d. count per file: %d, max file count: %d.", countLimit, countPerFile, MAX_FILE_COUNT);
    }
}

void TagValueCache::SaveFuncSucceed(byte reqNo)
{
    if (hEvent == NULL) return;
    if (reqNo != m_reqNo)
    {
        Trace::writeFormatLine("Error: tags request #%u does NOT match response #%u!", m_reqNo, reqNo);
        return;
    }
    event_set(hEvent);
}

void TagValueCache::ReconnectProc()
{
    static bool isConnecting = false; // flag to avoid reentrant

    if (m_saveFunc == NULL || isConnecting) return; // bail out
    isConnecting = true;

    TagValueReq tagValueReq;
    const int interval = m_sendInterval;

    while (true)
    {
        int nOneTimeTags = 0;
        int nTotalTags = 0, nTotalReqs = 0;
        bool errSaveTag = false;
        // File cache
        int nFileCount = 0;
        while (!errSaveTag)
        {
            StringArray files;
            {
                Locker locker(&m_streamMutex);
                Directory::getFiles(m_cacheFilesDir, "*.*", SearchOption::TopDirectoryOnly, files);
                if (files.count() == 0)
                {
                    break;
                }
            }

            //Sort files alphabetically
            SortFiles(files);

            for (uint i = 0; i < files.count(); i++)
            {
                if (i > 0)
                {
                    // Wait to not send too many tags at the same time which will create cache files on the peer
                    Thread::msleep(interval);
                }

                int nTotalTagsInFile = 0, nTotalReqsInFile = 0;
                string fileName = files[i];

                FileStream fs(fileName, FileMode::FileOpen, FileAccess::FileRead);
                int64_t fileLen = fs.length();
                while (fs.position() + 2 < fileLen)
                {
                    if (nOneTimeTags >= CDeviceConfiguration::MAX_TAG_NUM)
                    {
                        Trace::writeFormatLine("Sent cached %d tags in file (current req #%d). To be continued...", nOneTimeTags, nTotalReqsInFile);
                        nOneTimeTags = 0;
                        Thread::msleep(interval);
                    }

                    tagValueReq.read(&fs);
                    if (!secureSaveFunc(tagValueReq))
                    {
                        errSaveTag = true;
                        Trace::writeFormatLine("Error in sending cached tags in file. Waiting %d seconds for next try!", interval / 1000);
                        break; // still error
                    }
                    nTotalReqsInFile++;
                    nTotalTagsInFile += tagValueReq.tagValues.size();
                    nOneTimeTags += tagValueReq.tagValues.size();
                }
                if (nTotalTagsInFile > 0)
                {
                    Trace::writeFormatLine("Sent cached tags in current file: total %d tags in total %d cycles.", nTotalTagsInFile, nTotalReqsInFile);
                }
                if (errSaveTag)
                {
                    //isConnecting = false;
                    break;
                }
                nTotalReqs += nTotalReqsInFile;
                nTotalTags += nTotalTagsInFile;
                nFileCount++;
                fs.close();
                File::deleteFile(fileName);
                Trace::writeFormatLine("Cached tag file deleted: '%s'.", fileName.c_str());
            }
        }

        if (nTotalTags > 0)
        {
            Trace::writeFormatLine("Sent cached tags in all %d file(s): total %d tags in total %d cycles.", nFileCount, nTotalTags, nTotalReqs);
        }

        // memory cache
        TagValueReq *pTagValueReq = NULL;
        nOneTimeTags = 0; // restarted for memory tag count accumulation
        nTotalTags = 0;
        nTotalReqs = 0;
        while (!errSaveTag)
        {
            {
                Locker locker(&m_streamMutex);
                if (m_lstTagValue.size() == 0)
                {
                    break;
                }

                pTagValueReq = m_lstTagValue.front();
            }

            if (pTagValueReq != NULL)
            {
                if (!secureSaveFunc(*pTagValueReq))
                {
                    // still error
                    errSaveTag = true;
                    Trace::writeFormatLine("Error in sending tags. Waiting %d seconds for next try...", interval / 1000);
                    break;
                }
                Locker locker(&m_streamMutex);
                m_lstTagValue.pop_front();
            }

            if (pTagValueReq != NULL)
            {
                if (nOneTimeTags >= CDeviceConfiguration::MAX_TAG_NUM)
                {
                    Trace::writeFormatLine("%d tags in memory sent: total %d tags in total %d cycles. To be continued...", nOneTimeTags, nTotalTags, nTotalReqs);
                    nOneTimeTags = 0;
                    Thread::msleep(interval);
                    //isConnecting = false;
                    //return;
                }

                nTotalReqs++;
                nTotalTags += pTagValueReq->tagValues.size();
                nOneTimeTags += pTagValueReq->tagValues.size();

                delete pTagValueReq;
                pTagValueReq = NULL;
            }
        }

        if (nTotalReqs > 1)
        {
            Trace::writeFormatLine("Sent tags in memory: total %d tags in total %d cycles.", nTotalTags, nTotalReqs);
        }

        if (errSaveTag)
        {
            //isConnecting = false;
            //return;
        }

        Thread::msleep(interval);
    }
    m_bIsCached = false;
    isConnecting = false;
}