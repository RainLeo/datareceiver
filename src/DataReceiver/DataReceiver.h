#pragma once

#include "common/driver/communication/ServerService.h"
#include "common/driver/communication/ClientService.h"
#include "common/system/Singleton.h"
#include "Node.h"
#include "InfoPlus.h"
#include "TagValueCache.h"
#include "ServerCommConfig.h"

using namespace Common;
using namespace Driver;

class CDataReceiver
{
public:
	CDataReceiver();
	~CDataReceiver();
	bool initialize();
	bool unInitialize();
    inline static CDataReceiver *Instance() { return Singleton<CDataReceiver>::instance(); }
    typedef map<string, CNode> MapNode;
    MapNode m_mapNodes;
    typedef map<string, string> MapNodeAddr;
    MapNodeAddr m_mapNodeAddr;
	template<class T, class C>
	bool send(const string addr, const T& inputData, const string& name)
	{
        return m_pServerService->sendAsync<T, C>(addr, inputData, name);
	}
    bool IsSendingConfiguration() { return m_bIsSendingConfiguration; }
    void SetSendingConfiguration(bool value) { m_bIsSendingConfiguration = value; }
    void CollectMonitor();
    void StartWebService();
    ServerCommConfig& Config() { return *m_pConfig; }
    string ReceivedLogFile();
    void SetLogFileReceived(const string& filename);
private:
    DECLARE_SINGLETON_CLASS(CDataReceiver);
    CInfoPlus *m_pInfoPlus;
    TagValueCache *m_pTagValueCache;
    ServerService *m_pServerService;
    bool m_bIsSendingConfiguration;
    Timer *m_pCollectMonitorTimer;
    Thread *m_pWebService;
    ServerCommConfig *m_pConfig;
    event_handle m_hLogEvent;
    string m_logFileName;
};
