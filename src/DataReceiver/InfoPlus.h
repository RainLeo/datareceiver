#ifndef INFOPLUS_H
#define INFOPLUS_H

#if WIN32
#pragma warning(disable : 4005) // warning C4005: 'NAN' : macro redefinition in vc\include\math.h(106)
#endif

#include "interface.h"
#include <infoplus21_api.h>
#include "common/system/Singleton.h"
#include <map>

class CInfoPlus
{
public:
    ~CInfoPlus();

    bool Initialize();
    int Uninitialize();
    int checkerror(ERRBLOCK dberror);
    bool WriteDb(const TagValueReq &tagValueReq);
    bool WriteDb(const vector<TagValue *> tagValues);
    static CInfoPlus *Instance() { return Singleton<CInfoPlus>::instance(); }
    bool GetTagId(string &tagName, long &recid);
    long GetTagId(string &tagName);
    int GetStatus() { return err.ERRCODE; }
    TagType GetTagType(long tagId);

private:
    CInfoPlus();
    long m_field_tag;
    long m_field_quality_tag;
    ERRBLOCK err;
    DECLARE_SINGLETON_CLASS(CInfoPlus);
    bool m_bInit;
    typedef map<long, TagType> MapTagType;
    MapTagType m_mapTagTypes;
};

#endif