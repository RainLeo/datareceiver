#pragma once

#include "common\driver\communication\ServerService.h"
#include "common\driver\communication\ClientService.h"
#include "common/driver/communication/ClientConfig.h"
#include "common/driver/communication/ClientContext.h"
#include "common/system/DateTime.h"
#include "interface.h"

using namespace Common;
using namespace Driver;

class CDataReporter :
	public ClientService
{
public:
	CDataReporter();
	~CDataReporter();
	bool initialize(const ReaderFile* file);
	bool unInitialize();
};

class ClientCommConfig : public ClientConfig
{
public:
	ClientCommConfig(const ReaderFile& file);

protected:
	bool load(XmlTextReader& reader);
};

