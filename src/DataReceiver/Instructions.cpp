#include "interface.h"
//#include "common/driver/communication/ClientContext.h"
#include "DataReceiver.h"

ClientContext* ServerHeartbeatInstruction::setValue(ClientContext* context)
{
    HeartbeatContext *pHbc = (HeartbeatContext*)context;
    const HeartbeatReq &req = *pHbc->inputData();
    const vector<Alarm> &alarms = req.vecAlarms;
    bool bNeedTagConfiguration = false;

    if (!AppVersion.IsCompatible(req.version))
    {
        Trace::writeFormatLine("Warning: Inconsistent version (%s) from Reporter %s with Receiver version %s.", pHbc->inputData()->version.toString().c_str(), pHbc->peerAddr().c_str(), AppVersion.toString().c_str());
    }

    for (uint i = 0; i < alarms.size(); i++)
    {
        const Alarm &alarm = alarms[i];
        if (alarm.alarm == Alarm::ALARM_RESTART)
        {
            bNeedTagConfiguration = true;
            Trace::writeFormatLine("Reporter (%s) restarted flag received.", pHbc->peerAddr().c_str());
            break;
        }
    }

    HeartbeatResp resp; // DateTime Now
    pHbc->setOutputData(&resp);
    string clientAddr = pHbc->peerAddr();
    CDataReceiver *pDr = CDataReceiver::Instance();
    assert(pDr);
    if (pDr->m_mapNodes.find(clientAddr) == pDr->m_mapNodes.end())
    {
        pDr->m_mapNodes[clientAddr] = CNode(clientAddr);
    }
    CNode &node = pDr->m_mapNodes[clientAddr];
    node.Activate(); // maintain the connection status for collecting monitor data

    if (node.IsModified() || (bNeedTagConfiguration) && !pDr->IsSendingConfiguration())
    {
        if (node.LoadConfiguration())
        {
            // update map: id<->IP addr
            pDr->m_mapNodeAddr[node.m_tagConfiguration.id] = clientAddr;

            Trace::writeFormatLine("Sending configuration message to id %s at addr: %s", node.m_tagConfiguration.id.c_str(), clientAddr.c_str());
            TagConfiguration &tagConfiguration = node.m_tagConfiguration;
            pDr->send<TagConfiguration, TagConfigurationContext>(clientAddr, tagConfiguration, "TagConfiguration");
            pDr->SetSendingConfiguration(true);
        }
        else
        {
            Trace::writeFormatLine("Error: Loading configuration failed for %s.", clientAddr.c_str());
        }
    }
    else if (!node.IsExisted() && node.IsConfigured())
    {
        // Stop fetching by setting interval to zero & clear all device configurations for efficiency
        node.m_tagConfiguration.iFetchInterval = 0;
        node.m_tagConfiguration.vecDevices.clear();

        Trace::writeFormatLine("Sending stop configuration message to id %s at addr: %s", node.m_tagConfiguration.id.c_str(), clientAddr.c_str());
        TagConfiguration &tagConfiguration = node.m_tagConfiguration;
        pDr->send<TagConfiguration, TagConfigurationContext>(clientAddr, tagConfiguration, "TagConfiguration");
        // keep node configuration until here
        pDr->m_mapNodes.erase(pDr->m_mapNodes.find(clientAddr));
    }
#if TEST_MAINTAIN
    // Request maintenance information
    MaintainDownReq maintainReq;
    MaintainDownResp maintainResp;
    maintainReq.programId = PROGRAM_ID_GATHERER;
    pDr->send<MaintainDownReq, MaintainDownResp, MaintainDownContext>(clientAddr, maintainReq, maintainResp, "MaintainDown");
#endif
    return context;
}

ClientContext* ServerMaintainDownInstruction::setValue(ClientContext* context)
{
    return context;
}

ClientContext* MaintainDownInstruction::setValue(ClientContext* context)
{
    MaintainDownContext *pContext = (MaintainDownContext*)context;
    if (pContext->outputData()->status != 0)
    {
        string clientAddr = pContext->peerAddr();
        Trace::writeFormatLine("Warning: Request maintenance information response failed from client: %s, status = %d(0x%x)", clientAddr.c_str(), pContext->outputData()->status, pContext->outputData()->status);
    }

    return context;
}

ClientContext* ServerMaintainUpInstruction::setValue(ClientContext* context)
{
    MaintainUpContext *pContext = (MaintainUpContext*)context;
    string clientAddr = pContext->peerAddr();
    const MaintainUpReq &maintainReq = *pContext->inputData();

    Debug::writeFormatLine("Maintenance information received from client: %s.", clientAddr.c_str());
    const vector<Maintain> & vecMaintain = maintainReq.vecMaintain;
    CDataReceiver *pDr = CDataReceiver::Instance();
    assert(pDr);
    CDataReceiver::MapNode::iterator iterNode = pDr->m_mapNodes.find(clientAddr);
    if (iterNode == pDr->m_mapNodes.end())
    {
        Trace::writeFormatLine("Client (%s) not configured.", clientAddr.c_str());
        return context;
    }

    for (vector<Maintain>::const_iterator ci = vecMaintain.begin(); ci != vecMaintain.end(); ci++)
    {
        Debug::writeFormatLine("    ProgramId: %d. CPU: %d%%, Used Mem: %u M, Free Mem: %u M, Disk: %u%%", ci->programId, ci->cpu, ci->memUsed, ci->memFree, ci->disk);
        switch (ci->programId)
        {
        case PROGRAM_ID_GATHERER:
            iterNode->second.m_monitorData.runTimeStatus = ci->runTimeStatus;
            iterNode->second.m_monitorData.collectorInner = ci->connStatus;
            iterNode->second.m_monitorData.opcConnStatus = ci->opcStatus;
            iterNode->second.m_monitorData.cpu2 = ci->cpu;
            iterNode->second.m_monitorData.memory2 = ci->memUsed * 100 / (ci->memFree + ci->memUsed);
            iterNode->second.m_monitorData.disk2 = ci->disk;
            break;
        case PROGRAM_ID_REPORTER:
            iterNode->second.m_monitorData.cpu = ci->cpu;
            iterNode->second.m_monitorData.memory = ci->memUsed * 100 / (ci->memFree + ci->memUsed);
            iterNode->second.m_monitorData.disk = ci->disk;
            break;
        default:
            break;
        }
    }

    return context;
}

ClientContext* MaintainUpInstruction::setValue(ClientContext* context)
{
    return context;
}

ClientContext* TagConfigurationInstruction::setValue(ClientContext* context)
{
    TagConfigurationContext *pContext = (TagConfigurationContext*)context;

    string clientAddr = pContext->peerAddr();
    if (pContext->outputData()->status == 0)
    {
        Trace::writeFormatLine("Tag configuration response succeeds from client: %s.", clientAddr.c_str());
    }
    else
    {
        Trace::writeFormatLine("Warning: Tag configuration response failed from client: %s, status = %d(0x%x)", clientAddr.c_str(), pContext->outputData()->status, pContext->outputData()->status);
    }

    CDataReceiver *pDr = CDataReceiver::Instance();
    assert(pDr);
    pDr->SetSendingConfiguration(false);

    return context;
}

ClientContext* ServerTagValueInstruction::setValue(ClientContext* context)
{
    TagValueContext *tvc = dynamic_cast<TagValueContext *>(context);
    assert(tvc);
    TagValueCache::Instance()->WriteTags(*tvc->inputData());
    TagValueResp resp;
    resp.status = tvc->inputData()->reqNo;
    tvc->setOutputData(&resp);
    return context;
}

ClientContext* DownloadLogInstruction::setValue(ClientContext* context)
{
    DownloadLogContext *pContext = (DownloadLogContext*)context;
    if (pContext->outputData()->status != 0)
    {
        string clientAddr = pContext->peerAddr();
        Trace::writeFormatLine("Warning: Request log response failed from client: %s, status = %d(0x%x)", clientAddr.c_str(), pContext->outputData()->status, pContext->outputData()->status);
    }

    return context;
}

ClientContext* ServerUploadLogInstruction::setValue(ClientContext* context)
{
    UploadLogContext *c = (UploadLogContext *)context;
    const FileHeader* header = c->header();
    const FileDatas* fds = c->outputData();
    for (uint i = 0; i < fds->count(); i++)
    {
        bool isLastPart = fds->at(i)->packetNo == header->packetCount - 1;

        if (isLastPart)
        {
            File::move(header->tempFullFileName(), header->fullFileName());
            CDataReceiver *pDr = CDataReceiver::Instance();
            assert(pDr);
            pDr->SetLogFileReceived(header->fullFileName());
        }
    }
    return context;
}
