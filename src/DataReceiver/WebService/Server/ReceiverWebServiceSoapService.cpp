//#include "ReceiverWebServiceSoap.nsmap"
#include "soapReceiverWebServiceSoapService.h"
#include "../../DataReceiver.h"
#include "interface.h"
#include "common/system/Application.h"

int ReceiverWebServiceSoapService::saveConfig(_ns1__saveConfig *req, _ns1__saveConfigResponse &resp)
{
    if (req == NULL || req->xml == NULL) return SOAP_FAULT;

    resp.saveConfigResult = FALSE;

    CDataReceiver *pDr = CDataReceiver::Instance();

    if (pDr == NULL) return SOAP_OK;

    //translate xml string to internal node xml files
    xmlDocPtr doc;
    LIBXML_TEST_VERSION

#if defined(STUB)
    FileStream fs = FileStream(".\\webservicexml.config", FileMode::FileOpen, FileAccess::FileRead);
    ByteArray buffer;
    if (!fs.readToEnd(buffer))
    {
        Trace::writeLine("Error: failed to read the webservice xml file...");
        return SOAP_OK;
    }
    fs.close();
    //Parse memory into DOM
    doc = xmlParseMemory((const char *)buffer.data(), buffer.count());
#else
    //Parse memory into DOM
    doc = xmlParseMemory((const char *)req->xml->data(), req->xml->size());
#endif
    if (doc == NULL) {
        /*
        * ouptut the result.
        */
        char *msg = (char*)soap_malloc(this, 1024);
        _snprintf(msg, 1024, "Error parsing configuration xml string.");
        Trace::writeFormatLine("WebService: %s", msg);
        return soap_senderfault("XML format error", msg);
    }
    xmlDocDump(stdout, doc);
    xmlNodePtr root = xmlDocGetRootElement(doc);
    xmlNodePtr collector = root->children;

    string collectorPath = Path::combine(Application::instance()->rootPath(), "Nodes");

    while (collector != NULL)
    {
        if ((!xmlStrcmp(collector->name, (const xmlChar *)"collectMachine")))
        {
            xmlChar *propId = xmlGetProp(collector, BAD_CAST "id");
            xmlChar *prop = xmlGetProp(collector, BAD_CAST "ip");
            if (propId == NULL)
            {
                Trace::writeFormatLine("WebService saveConfig error: id not found.");
                return SOAP_OK;
            }

            if (prop == NULL)
            {
                Trace::writeFormatLine("WebService saveConfig error: IP not found for id (%s).", propId);
                xmlFree(propId);
                return SOAP_OK;
            }

            string id = string((const char*)propId);
            string clientAddr = string((const char*)prop);
            xmlFree(prop);
            xmlFree(propId);

            xmlDocPtr pNewDoc = xmlNewDoc((const xmlChar*)"1.0");

            if (pNewDoc == NULL)
            {
                char *msg = (char*)soap_malloc(this, 1024);
                _snprintf(msg, 1024, "Fail to create new XML doc.");
                Trace::writeFormatLine("WebService error: %s", msg);
                return soap_senderfault("Internal error", msg);
            }

            xmlNodePtr newRoot = xmlNewDocNode(pNewDoc, NULL, (const xmlChar*)"root", NULL);

            if (root == NULL)
            {
                char *msg = (char*)soap_malloc(this, 1024);
                _snprintf(msg, 1024, "Fail to create doc node.");
                Trace::writeFormatLine("WebService error: %s", msg);
                return soap_senderfault("Internal error", msg);
            }
            // Added ID property to xml files
            xmlSetProp(newRoot, (const xmlChar *)"id", BAD_CAST id.c_str());

            prop = xmlGetProp(collector, BAD_CAST "collectCycle");
            xmlSetProp(newRoot, (const xmlChar *)"fetching_interval", prop);
            xmlFree(prop);
            xmlDocSetRootElement(pNewDoc, newRoot);
            xmlNodePtr src = xmlCopyNodeList(collector->children);
            xmlAddChildList(newRoot, src);

            /*
            * ouptut the result.
            */
            xmlDocDump(stdout, pNewDoc);

            string fileName = Path::combine(collectorPath, clientAddr + ".xml");
            if (pDr->m_mapNodeAddr.find(id) != pDr->m_mapNodeAddr.end())
            {
                string oldIpAddr = pDr->m_mapNodeAddr[id];
                Trace::writeFormatLine("WebService saveConfig: ID(%s) already exists and will be overwritten.", id.c_str());
                if (pDr->m_mapNodes.find(oldIpAddr) != pDr->m_mapNodes.end())
                {
                    pDr->m_mapNodes[oldIpAddr].RemoveConfiguration();
                    Trace::writeFormatLine("WebService saveConfig: old IP(%s) xml file was removed.", oldIpAddr.c_str());
                }
            }
            pDr->m_mapNodeAddr[id] = clientAddr;
            if (pDr->m_mapNodes.find(clientAddr) != pDr->m_mapNodes.end())
            {
                Trace::writeFormatLine("WebService saveConfig: IP(%s) already exists and is overwritten for id (%s).", clientAddr.c_str(), id.c_str());
            }
            pDr->m_mapNodes[clientAddr] = CNode(clientAddr);
            pDr->m_mapNodes[clientAddr].m_id = id;

            int ret = xmlSaveFile(fileName.c_str(), pNewDoc);
            if (ret != -1)
            {
                Trace::writeFormatLine("WebService saveConfig: updated %s successfully.", fileName.c_str());
            }
            /*
            * don't forget to free up the doc
            */
            xmlFreeDoc(pNewDoc);
        }
        collector = collector->next;
    }

    /*
    * don't forget to free up the doc
    */
    xmlFreeDoc(doc);

    /*
    * this is to debug memory for regression tests
    */
    xmlMemoryDump();

    resp.saveConfigResult = TRUE;
    return SOAP_OK;
}

int ReceiverWebServiceSoapService::deleteConfig(_ns1__deleteConfig *req, _ns1__deleteConfigResponse &resp)
{
    if (req == NULL || req->collecterId == NULL) return SOAP_FAULT;

    resp.deleteConfigResult = FALSE;

    CDataReceiver *pDr = CDataReceiver::Instance();

    if (pDr == NULL) return SOAP_OK;

    CDataReceiver::MapNodeAddr::iterator iterAddr = pDr->m_mapNodeAddr.find(*req->collecterId);
    if (iterAddr == pDr->m_mapNodeAddr.end())
    {
        char *msg = (char*)soap_malloc(this, 1024);
        _snprintf(msg, 1024, "The specified collector %s has not been configured yet.", req->collecterId->c_str());
        Trace::writeFormatLine("WebService Warning: %s", msg);
        return soap_senderfault("Collector not configured", msg);
    }

    CDataReceiver::MapNode::iterator iter = pDr->m_mapNodes.find(iterAddr->second);
    if (iter == pDr->m_mapNodes.end())
    {
        char *msg = (char*)soap_malloc(this, 1024);
        _snprintf(msg, 1024, "The specified collector IP %s has not been configured yet.", iterAddr->second.c_str());
        Trace::writeFormatLine("WebService Warning: %s", msg);
        return soap_senderfault("Collector not configured", msg);
    }

    iter->second.RemoveConfiguration();
    //pDr->m_mapNodes.erase(iter);
    pDr->m_mapNodeAddr.erase(iterAddr);

    resp.deleteConfigResult = TRUE;
    return SOAP_OK;
}

// used for log file transfer in downloadLog method
ByteArray g_buffer;
xsd__base64Binary g_logBinary;

int ReceiverWebServiceSoapService::downloadLog(_ns1__downloadLog *req, _ns1__downloadLogResponse &resp)
{
    if (req == NULL || req->collecterId == NULL) return SOAP_FAULT;

    resp.downloadLogResult = NULL;

    CDataReceiver *pDr = CDataReceiver::Instance();

    if (pDr == NULL) return SOAP_OK;

    CDataReceiver::MapNodeAddr::iterator iterAddr = pDr->m_mapNodeAddr.find(*req->collecterId);
    if (iterAddr == pDr->m_mapNodeAddr.end())
    {
        char *msg = (char*)soap_malloc(this, 1024);
        _snprintf(msg, 1024, "The specified collector %s has not been configured yet.", req->collecterId->c_str());
        Trace::writeFormatLine("WebService Warning: %s", msg);
        return soap_senderfault("Collector not configured", msg);
    }

    string path = Path::combine(Application::instance()->rootPath(), "downloadLog");
    Directory::createDirectory(path);
    StringArray filenames;
    Directory::getFiles(path, "*", SearchOption::AllDirectories, filenames);
    for (uint i = 0; i < filenames.count(); i++)
    {
        string filename = filenames[i];
        File::deleteFile(filename);
    }

    // Receiver log file
    string logPath = Path::combine(Application::instance()->rootPath(), "logs");
    File::copy(Path::combine(logPath, Convert::getDateStr(req->logTime) + ".log"), Path::combine(path, Convert::getDateStr(req->logTime) + "_receiver.log"));

    DownloadLogReq logReq;
    // Gatherer log file
    logReq.programId = PROGRAM_ID_GATHERER;
    logReq.startDate = logReq.endDate = req->logTime;
    pDr->send<DownloadLogReq, DownloadLogContext>(iterAddr->second, logReq, "DownloadLog");

    string file = pDr->ReceivedLogFile();
    if (file != "")
    {
        File::move(file, Path::combine(path, Convert::getDateStr(logReq.startDate) + "_gatherer.log"));
    }

#ifdef LOG_SYN_ISSUE_RESOLVED
    // Reporter log file
    logReq.programId = PROGRAM_ID_REPORTER;
    //logReq.startDate = logReq.endDate = req->logTime;
    pDr->send<DownloadLogReq, DownloadLogContext>(iterAddr->second, logReq, "DownloadLog");

    file = pDr->ReceivedLogFile();
    if (file != "")
    {
        File::move(file, Path::combine(path, Convert::getDateStr(logReq.startDate) + "_reporter.log"));
    }
#endif

    string zipFile = Path::combine(path, Convert::getDateStr(logReq.startDate) + ".zip");
    if (Zip::compress(path, zipFile))
    {
        FileStream fs = FileStream(zipFile, FileMode::FileOpen, FileAccess::FileRead);
        g_buffer.clear();
        if (!fs.readToEnd(g_buffer))
        {
            Trace::writeFormatLine("Error: failed to read the log file (%s)...", zipFile.c_str());
            return SOAP_OK;
        }
        fs.close();

        xsd__base64Binary * pLog = &g_logBinary;

        pLog->__ptr = g_buffer.data();
        pLog->__size = g_buffer.count();
        resp.downloadLogResult = pLog;
        Trace::writeFormatLine("WebService: zip the log files successfully. zip file: %s, size %u.", zipFile.c_str(), pLog->__size);
    }
    else
    {
        Trace::writeFormatLine("WebService: zip the log files failed.");
    }

    return SOAP_OK;
}

int ReceiverWebServiceSoapService::saveConfig_(_ns1__saveConfig *req, _ns1__saveConfigResponse &resp)
{
    return saveConfig(req, resp);
}

int ReceiverWebServiceSoapService::deleteConfig_(_ns1__deleteConfig *req, _ns1__deleteConfigResponse &resp)
{
    return deleteConfig(req, resp);
}

int ReceiverWebServiceSoapService::downloadLog_(_ns1__downloadLog *req, _ns1__downloadLogResponse &resp)
{
    return downloadLog(req, resp);
}