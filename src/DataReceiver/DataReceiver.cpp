// Defines the entry point for the application.

#include "stdafx.h"
#include "WebService/Client/ClientCollectMonitorWSServiceImplServiceSoapBindingProxy.h"
#include "WebService/Client/CollectMonitorWSServiceImplServiceSoapBinding.nsmap"
#include "WebService/Server/soapReceiverWebServiceSoapService.h"
#if WIN32 && _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif
#include <iostream>
#include "DataReceiver.h"
#include "common/diag/Stopwatch.h"
#include "common/system/Application.h"
#include "common/diag/Trace.h"
#include "interface.h"
#include "DataReceiverService.h"

#define DATA_RECEIVER_SERVICE

using namespace Common;
using namespace Client;

int _tmain(int argc, _TCHAR* argv[])
{
#if WIN32 && _DEBUG
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

    Application app(argc, (const char **)argv);

    int ret = service_main(argc, argv);
    if (ret != -1)
    {
        return ret; // run as a console application for debug purpose
    }

    CDataReceiver* pDr = CDataReceiver::Instance();
    pDr->initialize();

    string input;
    while (!Convert::stringEqual("exit", input.substr(0, 4), true))
    {
        Trace::writeLine("Input 'exit' to exit...");
        getline(cin, input);
#if 0 //debug to send down load log request
        DownloadLogReq logReq;
        logReq.programId = PROGRAM_ID_GATHERER;
        logReq.startDate = logReq.endDate = time(NULL);
        pDr->send<DownloadLogReq, DownloadLogContext>("192.168.13.120", logReq, "DownloadLog");
#endif
    }

    pDr->unInitialize();
    delete pDr;
    return 0;
}

vector<Alarm> GetAlarms()
{
    // Dummy
    return vector<Alarm>();
}

bool WriteDb(const TagValueReq &tagValueReq)
{
    return CInfoPlus::Instance()->WriteDb(tagValueReq);
}

void staticCollectMonitorProc(void* parameter)
{
    CDataReceiver* pDataReceiver = (CDataReceiver*)parameter;
    pDataReceiver->CollectMonitor();
}

void staticWebServiceProc(void* parameter)
{
    CDataReceiver* pDataReceiver = (CDataReceiver*)parameter;
    pDataReceiver->StartWebService();
}

CDataReceiver::CDataReceiver() :
    m_pServerService(NULL),
    m_bIsSendingConfiguration(false),
    m_pCollectMonitorTimer(NULL),
    m_pConfig(NULL),
    m_logFileName("")
{
    m_pInfoPlus = CInfoPlus::Instance();
    m_pTagValueCache = TagValueCache::Instance();
    m_pTagValueCache->SetSaveFunc(WriteDb, true, 20); // true means the WriteDb return value directly indicates success or failed, no need for response
    m_hLogEvent = event_create(true, false);
}

CDataReceiver::~CDataReceiver()
{
    if (m_pInfoPlus != NULL)
    {
        delete m_pInfoPlus;
        m_pInfoPlus = NULL;
    }
    if (m_pTagValueCache != NULL)
    {
        delete m_pTagValueCache;
        m_pTagValueCache = NULL;
    }
    if (m_pWebService != NULL)
    {
        delete m_pWebService;
        m_pWebService = NULL;
    }
    if (m_pConfig != NULL)
    {
        delete m_pConfig;
        m_pConfig = NULL;
    }
    if (m_hLogEvent != NULL)
    {
        event_destroy(m_hLogEvent);
    }
}

string CDataReceiver::ReceivedLogFile()
{
    event_reset(m_hLogEvent);
    m_logFileName = "";

    int ret = event_timedwait(m_hLogEvent, 3000); // wait 3 sec for response
    if (ret == 0)
    {
        Trace::writeFormatLine("Log file %s received successfully.", m_logFileName.c_str());
        return m_logFileName;
    }

    return m_logFileName;
}

void CDataReceiver::SetLogFileReceived(const string &filename)
{
    m_logFileName = filename;
    event_set(m_hLogEvent);
}

void server_generateInstructions(void* owner, Instructions* instructions)
{
	ServerHeartbeatInstruction* hi = new ServerHeartbeatInstruction(new InstructionDescription("Heartbeat", new HeartbeatContext()));
	instructions->add(hi);
    TagConfigurationInstruction *ti = new TagConfigurationInstruction(new InstructionDescription("TagConfiguration", new TagConfigurationContext()));
	instructions->add(ti);
	ServerTagValueInstruction* tvi = new ServerTagValueInstruction(new InstructionDescription("TagValue", new TagValueContext()));
	instructions->add(tvi);
    MaintainDownInstruction * smdi = new MaintainDownInstruction(new InstructionDescription("MaintainDown", new MaintainDownContext()));
    instructions->add(smdi);
    ServerMaintainUpInstruction *mui = new ServerMaintainUpInstruction(new InstructionDescription("MaintainUp", new MaintainUpContext()));
    instructions->add(mui);
    Instruction * i = new DownloadLogInstruction(new InstructionDescription("DownloadLog", new DownloadLogContext()));
    instructions->add(i);
    UploadLogContext *c = new UploadLogContext();
    FileHeader header;
    header.path = Path::combine(Application::instance()->rootPath(), "upload");
    c->setHeader(&header);
    i = new ServerUploadLogInstruction(new InstructionDescription("UploadLog", c));
    instructions->add(i);
}

bool CDataReceiver::initialize()
{
    ConfigFile file;
    file.rootPath = Application::instance()->rootPath();

    // load communication.config
	file.fileName = "receiver.config";
    m_pConfig = new ServerCommConfig(file, "receiver");
    ServerCommConfig &reader = *m_pConfig;
	if (!reader.Configuration::load())
		return false;

    Trace::writeFormatLine("Receiver App Started: Version: %s", AppVersion.toString().c_str());
    m_pTagValueCache->SetConfig(0, 0, reader.m_TagCacheCountLimit);

    // Init web service
    if (m_pConfig->webclient().Enabled)
    {
        m_pCollectMonitorTimer = new Timer(staticCollectMonitorProc, this, (int)m_pConfig->webclient().Interval.totalMilliseconds());
    }

    if (m_pConfig->webserver().Enabled)
    {
        m_pWebService = new Thread();
#ifdef DEBUG
        m_pWebService->setName("WebServiceProc");
#endif
        m_pWebService->start(staticWebServiceProc, this);
    }

    m_pServerService = new ServerService(reader.server());
    BaseCommService::InstructionCallback callback;
    callback.tcpInstructions = server_generateInstructions;
    return m_pServerService->initialize(callback);
}

void CDataReceiver::CollectMonitor()
{
#if 0
    // Request maintenance information
    MaintainDownReq maintainReq;
    MaintainDownResp maintainResp;
    maintainReq.programId = PROGRAM_ID_GATHERER;
    for (map<string, CNode>::const_iterator ci = m_mapNodes.begin(); ci != m_mapNodes.end(); ci++)
    {
        string clientAddr = ci->first;
        send<MaintainDownReq, MaintainDownContext>(clientAddr, maintainReq, "MaintainDown");
    }
#endif
    CollectMonitorWSServiceImplServiceSoapBindingProxy q;
    ns1__saveMonitorData req;
    ns1__saveMonitorDataResponse resp;
    q.connect_timeout = (int)Config().webclient().ConnectTimeout.totalSeconds(); // connect within 10s
    q.send_timeout = (int)Config().webclient().SendTimeout.totalSeconds(); // send timeout is 5s
    q.recv_timeout = (int)Config().webclient().ReceiveTimeout.totalSeconds(); // receive timeout is 5s

    /* create xml content for monitor data*/
    string tmpFileName = Path::combine(Application::instance()->rootPath(), "tmp.xml");

    XmlTextWriter xw(tmpFileName);
    xw.enableIndent();
    //xw.writeStartDocument();
    xw.writeStartElement("root");
    // for loop
    for (MapNode::const_iterator ci = m_mapNodes.begin(); ci != m_mapNodes.end(); ci++)
    {
        xw.writeStartElement("collectMachineStatus");
        const CNode &collector = ci->second;
        xw.writeAttributeString("id", ci->second.m_id);
        byte type = DEVICE_NONE;
        if (collector.m_tagConfiguration.vecDevices.size() > 0 && collector.m_tagConfiguration.vecDevices[0] != NULL)
        {
            type = collector.m_tagConfiguration.vecDevices[0]->type;
            xw.writeAttributeString("type", type == (int)DEVICE_FILE ? "dcs" : "meter");
        }
        const CNode::MonitorData &monitorData = ci->second.m_monitorData;
        int runTimeStatus = monitorData.runTimeStatus;
        if (type == (int)DEVICE_FILE && monitorData.collectorInner != 0)
        {
            runTimeStatus = 1; // abnormal because gatherer could not work
        }
        xw.writeAttributeString("runTimeStatus", Convert::convertStr(runTimeStatus)); //"0/1/2"
        if (type == (int)DEVICE_FILE) // DCS
        {
            xw.writeAttributeString("cpu1", Convert::convertStr(monitorData.cpu) + "%");
            xw.writeAttributeString("cpu2", Convert::convertStr(monitorData.cpu2) + "%");
            xw.writeAttributeString("memory1", Convert::convertStr(monitorData.memory) + "%");
            xw.writeAttributeString("memory2", Convert::convertStr(monitorData.memory2) + "%");
            xw.writeAttributeString("disk1", Convert::convertStr(monitorData.disk) + "%");
            xw.writeAttributeString("disk2", Convert::convertStr(monitorData.disk2) + "%");
            switch (monitorData.opcConnStatus)
            {
            case -100:
                xw.writeAttributeString("opcCollectStatus1", "1");
                // Keep other OPC status same as last data because they are obtained by the jave proc
                break;
            case -3:
                xw.writeAttributeString("opcCollectStatus1", "0");
                xw.writeAttributeString("opcConnStatus", "1");
                xw.writeAttributeString("opc2collecter", "1");
                break;
            case -1:
                xw.writeAttributeString("opcConnStatus", "1");
                xw.writeAttributeString("opc2collecter", "0");
                break;
            default:
                xw.writeAttributeString("opcCollectStatus1", "0");
                xw.writeAttributeString("opcConnStatus", "0");
                xw.writeAttributeString("opc2collecter", "0");
                break;
            }
            switch (monitorData.collectorInner)
            {
            case -100:
                xw.writeAttributeString("collecterInner", "1");
                break;
            default:
                xw.writeAttributeString("collecterInner", "0");
                xw.writeAttributeString("opcCollectStatus2", Convert::convertStr(monitorData.collectorInner));
                break;
            }
        }
        else
        {
            xw.writeAttributeString("cpu", Convert::convertStr(monitorData.cpu) + "%");
            xw.writeAttributeString("memory", Convert::convertStr(monitorData.memory) + "%");
            xw.writeAttributeString("disk", Convert::convertStr(monitorData.disk) + "%");
        }

        xw.writeAttributeString("collecter2IP21", collector.IsActive() ? "0" : "1");
        xw.writeAttributeString("cmiostatus", Convert::convertStr(CInfoPlus::Instance()->GetStatus())); //״̬�����

        xw.writeEndElement(); // End collectMachineStatus
    }
    // end loop
    xw.writeEndElement(); // End root
    xw.close();

    FileStream fs = FileStream(tmpFileName, FileMode::FileOpen, FileAccess::FileRead);
    ByteArray buffer;
    if (!fs.readToEnd(buffer))
    {
        Trace::writeLine("Error: failed to read the temp xml file...");
        return;
    }
    fs.close();
    //status
    req.arg0 = new string((char *)buffer.data());

    /*"http://192.168.13.120:54506/CollectMonitorWebService.asmx"*/
    const char * endpoint = Config().webclient().EndPoint.c_str();
    Trace::writeFormatLine("WebService: connecting to %s and calling saveMonitorData...", endpoint);
    if (q.saveMonitorData(endpoint, NULL, &req, resp) == SOAP_OK)
        std::cout << resp.return_ << std::endl;
    else
        q.soap_print_fault(stderr);
}

void CDataReceiver::StartWebService()
{
    ReceiverWebServiceSoapService webService;
    int port = Config().webserver().Port;

    webService.send_timeout = (int)Config().webserver().SendTimeout.totalSeconds(); // send timeout is 5s
    webService.recv_timeout = (int)Config().webserver().ReceiveTimeout.totalSeconds(); // receive timeout is 5s
    Trace::writeFormatLine("WebService: starting web service on port %u...", port);
    while (webService.run(port) != SOAP_OK && webService.error != SOAP_TCP_ERROR)
    {
        webService.soap_stream_fault(std::cerr);
        Thread::msleep(3000); // wait 3 seconds for next try
    }
}

bool CDataReceiver::unInitialize()
{
    Trace::writeFormatLine("Receiver App Shuts down.");
    bool ret = m_pServerService->unInitialize();
    delete m_pServerService;
    m_pServerService = NULL;
    return ret;
}
