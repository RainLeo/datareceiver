#include "stdafx.h"
#include "Node.h"
#include "common\xml\XmlTextReader.h"
#include "common\system\Application.h"
#include "common\IO\File.h"
#include "common\IO\FileInfo.h"
#include "common\IO\Path.h"
#include "DeviceFile.h"
#include "DeviceModBus.h"
#include "DeviceHart.h"
#include "DeviceDlt645.h"
#include "InfoPlus.h"
#include "DataReceiver.h"

CNode::CNode()
{

}

CNode::CNode(string addr) :
	m_strAddr(addr),
	m_configFileModifiedTime(0),
    m_activeTime(0)
{
	m_strConfigFileName = Path::combine(Application::instance()->rootPath(), "Nodes\\" + addr + ".xml");
}


CNode::~CNode()
{
}

CDeviceConfiguration* CNode::LoadDeviceConfiguration(XmlTextReader &reader)
{
    CDeviceConfiguration *pDeviceConfiguration = NULL;

    string type = reader.getAttribute("type");
    if (type == "file" || type == "") // Added empty source type attribute as default file type on 2016/1/3
    {
        pDeviceConfiguration = new CDeviceFile();
        CDeviceFileDetail &deviceFile = *dynamic_cast<CDeviceFileDetail *>(pDeviceConfiguration->pDetail);
        deviceFile.strDataFilePath = reader.getAttribute("data_file_path");
        deviceFile.strConfigFilePath = reader.getAttribute("configuration_file_path");
        while (reader.read())
        {
            if (reader.nodeType() == XmlNodeType::Element &&
                reader.localName() == "tag")
            {
                DeviceTag tag;
                tag.strName = reader.getAttribute("database_tag");
                tag.tagId = CInfoPlus::Instance()->GetTagId(tag.strName);
                tag.strRegisterName = reader.getAttribute("original_tag");
                if (!tag.IsValid())
                {
                    continue; // ignore, try next
                }
                if (!pDeviceConfiguration->AddTag(tag))
                {
                    break; // stop if out of limit
                }
            }
        }
    }
    else if (type == "opc")
    {
        //TODO: DeviceOpc;
    }
    else if (type == "acg")
    {
    }
    else if (type == "modbus")
    {
        pDeviceConfiguration = new CDeviceModBus();
        CDeviceModBusDetail &deviceDetail = *dynamic_cast<CDeviceModBusDetail *>(pDeviceConfiguration->pDetail);
        deviceDetail.parse(reader);

        while (reader.read())
        {
            if (reader.nodeType() == XmlNodeType::Element && reader.localName() == "tag")
            {
                DeviceTag tag;
                tag.strName = reader.getAttribute("tag_name");
                tag.tagId = CInfoPlus::Instance()->GetTagId(tag.strName);
                //Convert::parseByte(reader.getAttribute("address"), deviceDetail.address);
                tag.address = reader.getAttribute("address");
                Convert::parseInt16(reader.getAttribute("start_address"), tag.sStartAddr);
                Convert::parseInt16(reader.getAttribute("quantity"), tag.sUnitLen);
                tag.type = reader.getAttribute("type") == "float" ? 3 : 2;
                Convert::parseByte(reader.getAttribute("register"), tag.function);
                Convert::parseBoolean(reader.getAttribute("bigendian"), tag.bigEndian);
                Convert::parseBoolean(reader.getAttribute("inverse"), tag.inverse);
                if (!tag.IsValid())
                {
                    continue; // ignore, try next
                }
                if (!pDeviceConfiguration->AddTag(tag))
                {
                    break; // stop if out of limit
                }
            }
            else if (reader.nodeType() == XmlNodeType::Element && reader.localName() == "source")
            {
                CDeviceConfiguration *pDeviceConfiguration = LoadDeviceConfiguration(reader);
                if (pDeviceConfiguration == NULL)
                {
                    continue;
                }

                m_tagConfiguration.AddDevice(pDeviceConfiguration);
            }
        }
    }
    else if (type == "dl_t645")
    {
        pDeviceConfiguration = new CDeviceDlt645();
        CDeviceDlt645Detail &deviceDetail = *dynamic_cast<CDeviceDlt645Detail *>(pDeviceConfiguration->pDetail);
        deviceDetail.parse(reader);

        while (reader.read())
        {
            if (reader.nodeType() == XmlNodeType::Element && reader.localName() == "tag")
            {
                DeviceTag tag;
                tag.strName = reader.getAttribute("tag_name");
                tag.tagId = CInfoPlus::Instance()->GetTagId(tag.strName);
                tag.address = reader.getAttribute("address");
                tag.strRegisterName = reader.getAttribute("data_identity");
                tag.type = 3; // TODO: fixed to be float for now, need to change in the future
                //tag.type = reader.getAttribute("type") == "float" ? 3 : 2;
                if (!tag.IsValid())
                {
                    continue; // ignore, try next
                }
                if (!pDeviceConfiguration->AddTag(tag))
                {
                    break; // stop if out of limit
                }
            }
            else if (reader.nodeType() == XmlNodeType::Element && reader.localName() == "source")
            {
                CDeviceConfiguration *pDeviceConfiguration = LoadDeviceConfiguration(reader);
                if (pDeviceConfiguration == NULL)
                {
                    continue;
                }

                m_tagConfiguration.AddDevice(pDeviceConfiguration);
            }
        }
    }
    else if (type == "hart")
    {
        pDeviceConfiguration = new CDeviceHart();
        CDeviceModBusDetail &deviceDetail = *dynamic_cast<CDeviceModBusDetail *>(pDeviceConfiguration->pDetail);
        deviceDetail.parse(reader);

        while (reader.read())
        {
            if (reader.nodeType() == XmlNodeType::Element && reader.localName() == "command")
            {
                Convert::parseByte(reader.getAttribute("address"), deviceDetail.address);
            }
            else if (reader.nodeType() == XmlNodeType::Element && reader.localName() == "tag")
            {
                DeviceTag tag;
                tag.strName = reader.getAttribute("tag_name");
                tag.tagId = CInfoPlus::Instance()->GetTagId(tag.strName);
                tag.address = reader.getAttribute("address");
                Convert::parseInt16(reader.getAttribute("begin"), tag.sStartAddr);
                Convert::parseInt16(reader.getAttribute("quantity"), tag.sUnitLen);
                tag.type = reader.getAttribute("type") == "float" ? 3 : 2;
                Convert::parseByte(reader.getAttribute("register"), tag.function);
                Convert::parseBoolean(reader.getAttribute("bigendian"), tag.bigEndian);
                Convert::parseBoolean(reader.getAttribute("inverse"), tag.inverse);
                if (!tag.IsValid())
                {
                    continue; // ignore, try next
                }
                if (!pDeviceConfiguration->AddTag(tag))
                {
                    break; // stop if out of limit
                }
            }
            else if (reader.nodeType() == XmlNodeType::Element && reader.localName() == "source")
            {
                CDeviceConfiguration *pDeviceConfiguration = LoadDeviceConfiguration(reader);
                if (pDeviceConfiguration == NULL)
                {
                    continue;
                }

                m_tagConfiguration.AddDevice(pDeviceConfiguration);
            }
        }
    }
    return pDeviceConfiguration;
}

bool CNode::LoadConfiguration()
{
	m_tagConfiguration.Clear();

	XmlTextReader reader(m_strConfigFileName);

	while (reader.read())
	{
		if (reader.nodeType() == XmlNodeType::Element &&
			reader.localName() == "root")
		{
            m_tagConfiguration.id = reader.getAttribute("id");
            m_id = m_tagConfiguration.id;
			Convert::parseInt32(reader.getAttribute("fetching_interval"), m_tagConfiguration.iFetchInterval);
			while (reader.read())
			{
                if (reader.nodeType() == XmlNodeType::Element && reader.localName() == "source")
                {
                    CDeviceConfiguration *pDeviceConfiguration = LoadDeviceConfiguration(reader);
                    if (pDeviceConfiguration == NULL)
                    {
                        continue;
                    }

                    m_tagConfiguration.AddDevice(pDeviceConfiguration);
                }
            }
		}
	}

	//Update modified time cache
	FileInfo fi = FileInfo(m_strConfigFileName);
	m_configFileModifiedTime = fi.modifiedTime();

    return m_tagConfiguration.IsValid();
}

bool CNode::RemoveConfiguration()
{
    Trace::writeFormatLine("Remove configuration file: %s.", m_strConfigFileName.c_str());
    return File::deleteFile(m_strConfigFileName);
}

bool CNode::IsModified()
{
	if (File::exists(m_strConfigFileName))
	{
		FileInfo fi = FileInfo(m_strConfigFileName);
		return (m_configFileModifiedTime != fi.modifiedTime());
	}

	return false;
}

bool CNode::IsExisted()
{
    return File::exists(m_strConfigFileName);
}

bool CNode::IsConfigured()
{
    return m_tagConfiguration.IsValid();
}

void CNode::Activate()
{
    m_activeTime = TickTimeout::GetCurrentTickCount();
}

bool CNode::IsActive() const
{
    CDataReceiver* pDr = CDataReceiver::Instance();
    TimeSpan clientTimeout = pDr->Config().server().timeout.close;
    uint deadTime = TickTimeout::GetDeadTickCount(m_activeTime, (uint)clientTimeout.totalMilliseconds());
    if (TickTimeout::IsTimeout(m_activeTime, deadTime))
        return false;

    return true;
}