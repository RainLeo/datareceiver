#pragma once
#include <map>
#include <vector>
#include "interface.h"

using namespace std;


class CNode
{
public:
    string m_id;
	string m_strAddr;
	string m_strConfigFileName;
	TagConfiguration m_tagConfiguration;
	time_t m_configFileModifiedTime;
    uint m_activeTime;
    //Monitor data
    struct MonitorData
    {
        byte runTimeStatus; // 0/1/2
        byte cpu;
        uint memory;
        uint disk;
        byte cpu2;
        uint memory2;
        uint disk2;
        byte opcConnStatus; // 0/1
        byte collectorInner;
        MonitorData()
        {
            memset(this, 0, sizeof(struct MonitorData));
        }
    } m_monitorData;

public:
	CNode();
	CNode(string addr);
	~CNode();
	bool LoadConfiguration();
    bool RemoveConfiguration();
	bool IsModified();
    bool IsExisted();
    bool IsConfigured();
    CDeviceConfiguration* LoadDeviceConfiguration(XmlTextReader &reader);
    void Activate();
    bool IsActive() const;
};

