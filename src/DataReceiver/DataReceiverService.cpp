#include "stdafx.h"
#include "DataReceiverService.h"
#include "DataReceiver.h"

//内部变量
SERVICE_STATUS			ssStatus;
SERVICE_STATUS_HANDLE	sshStatusHandle;

HANDLE g_hServiceStopEvent = NULL;
HANDLE g_hServiceWorkerThread = NULL;

BOOL ServiceStop()
{
    if (g_hServiceStopEvent == NULL) return FALSE; // bail out if not started yet

	LogEvent(_T("Stop DataReceiver Service"));
	//添加停止服务代码
    // This will signal the worker thread to start shutting down
    SetEvent(g_hServiceStopEvent);

    while (g_hServiceStopEvent != NULL)
    {
        Sleep(100);
    }

    CloseHandle(g_hServiceWorkerThread);

    if (!(ReportStatusToSCMgr(SERVICE_STOPPED, NO_ERROR, TIMEOUT)))
		return FALSE;
	return TRUE;
}

BOOL ServicePause()
{
	LogEvent(_T("Pause DataReceiver Service"));
	//添加暂停服务代码
	if(!(ReportStatusToSCMgr(SERVICE_PAUSED, NO_ERROR, TIMEOUT)))
		return FALSE;
	return TRUE;
}

BOOL ServiceContinue()
{
	LogEvent(_T("Continue DataReceiver Service"));
	//添加恢复服务代码
	if(!(ReportStatusToSCMgr(SERVICE_RUNNING, NO_ERROR, TIMEOUT)))
		return FALSE;
	return TRUE;

}

BOOL ServiceStart(DWORD dwArgc,LPTSTR* lpszArgv)
{
	LogEvent(_T("Start DataReceiver Service"));
	//添加开始服务代码
    // Create stop event to wait on later.
    g_hServiceStopEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (g_hServiceStopEvent == NULL)
    {
        return FALSE;
    }
    // Start the thread that will perform the main task of the service
    g_hServiceWorkerThread = CreateThread(NULL, 0, ServiceWorkerThread, NULL, 0, NULL);
    if (g_hServiceStopEvent == NULL)
    {
        CloseHandle(g_hServiceStopEvent);
        return FALSE;
    }

	if(!(ReportStatusToSCMgr(SERVICE_RUNNING, NO_ERROR, TIMEOUT)))
		return FALSE;
	return TRUE;
}

DWORD WINAPI ServiceWorkerThread(LPVOID lpParam)
{
    LogEvent(_T("Start DataReceiver Service Thread."));

    CDataReceiver* pDr = CDataReceiver::Instance();
    pDr->initialize();

    //  Periodically check if the service has been requested to stop
    while (WaitForSingleObject(g_hServiceStopEvent, 0) != WAIT_OBJECT_0)
    {
        /*
        * Perform main service function here
        */

        //  Simulate some work by sleeping
        Sleep(100);
    }

    pDr->unInitialize();
    delete pDr;

    LogEvent(_T("DataReceiver Service Thread Ends."));
    CloseHandle(g_hServiceStopEvent);
    g_hServiceStopEvent = NULL;

    return ERROR_SUCCESS;
}

BOOL ReportStatusToSCMgr(DWORD dwCurrentState,DWORD dwWin32ExitCode,DWORD dwWaitHint)
{
	ssStatus.dwCurrentState = dwCurrentState;
	ssStatus.dwCheckPoint = 0;
	ssStatus.dwWin32ExitCode = dwWin32ExitCode;
	ssStatus.dwWaitHint = dwWaitHint;
	if(!SetServiceStatus(sshStatusHandle, &ssStatus))
	{
		LogEvent(_T("SetServiceStatus failed!"));
		return FALSE;
	}
	return TRUE;
}
void WINAPI Service_Main(DWORD dwArgc, LPTSTR *lpszArgv)
{
	ssStatus.dwServiceType        = SERVICE_WIN32;			//指明可执行文件类型
	ssStatus.dwCurrentState       = SERVICE_START_PENDING;  //指明服务当前状态
	ssStatus.dwControlsAccepted   = SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_SHUTDOWN;	//指明服务接受何种控制   
	ssStatus.dwWin32ExitCode      = 0;   
	ssStatus.dwServiceSpecificExitCode = 0;   
	ssStatus.dwCheckPoint         = 0;   
	ssStatus.dwWaitHint           = 0;    

	//注册服务控制处理函数
	sshStatusHandle=RegisterServiceCtrlHandler(TEXT(SZSERVICENAME), Service_Ctrl);
	//如果注册失败
	if(!sshStatusHandle)
	{
		goto cleanup;
		return;
	}

	//更新服务状态
	if(!ReportStatusToSCMgr(SERVICE_START_PENDING, NO_ERROR, TIMEOUT))
		goto cleanup; //更新服务状态失败则转向 cleanup
    if (!ServiceStart(dwArgc, lpszArgv))
    {
        goto cleanup; //Start Service Failed, goto cleanup
    }
    return; // success
cleanup:
	//把服务状态更新为 SERVICE_STOPPED，并退出。
	if(sshStatusHandle)
		ReportStatusToSCMgr(SERVICE_STOPPED, GetLastError(), 0);
}
//控制处理程序函数
void WINAPI Service_Ctrl(DWORD dwCtrlCode)
{
	//处理控制请求码
	switch(dwCtrlCode)
	{
	case SERVICE_CONTROL_STOP:			//先更新服务状态为 SERVICDE_STOP_PENDING,再停止服务。
    case SERVICE_CONTROL_SHUTDOWN:
		ReportStatusToSCMgr(SERVICE_STOP_PENDING, NO_ERROR, 500);
		ServiceStop(); 
		return;
#if 0
	case SERVICE_CONTROL_PAUSE:			//暂停服务
		ReportStatusToSCMgr(SERVICE_STOP_PENDING,NO_ERROR,500);
		ServicePause(); 
		ssStatus.dwCurrentState=SERVICE_PAUSED;
		return;
	case SERVICE_CONTROL_CONTINUE:		//继续服务
		ReportStatusToSCMgr(SERVICE_STOP_PENDING, NO_ERROR, 500);
		ServiceContinue();
		ssStatus.dwCurrentState=SERVICE_RUNNING;
		return;
	case SERVICE_CONTROL_INTERROGATE:	//更新服务状态
		break;
#endif
    default:	//无效控制码
		break;
	}
	ReportStatusToSCMgr(ssStatus.dwCurrentState, NO_ERROR, 0);
}

BOOL IsInstalled()		//判断服务是否已安装
{
	BOOL bResult = FALSE;
	//打开服务控制管理器
	SC_HANDLE schSCManager = ::OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if (schSCManager != NULL)
	{
		//打开服务
		SC_HANDLE schService = ::OpenService(schSCManager, TEXT(SZSERVICENAME), SERVICE_QUERY_CONFIG);
		if (schService != NULL)
		{
			bResult = TRUE;
			::CloseServiceHandle(schService);
		}
		::CloseServiceHandle(schSCManager);
	}
	return bResult;
}

BOOL Install()		//安装服务程序
{
	BOOL bRet = FALSE;
	bRet = IsInstalled();
	if(bRet)
	{
		_tprintf(TEXT("%s alrealy installed!"), TEXT(SZSERVICENAME));
		return FALSE;
	}
	SC_HANDLE schService;
	SC_HANDLE schSCManager;
	TCHAR szPath[512];
	//得到程序磁盘文件的路径
	if(GetModuleFileName(NULL, szPath, 512)==0)
	{
        _tprintf(TEXT("Unable to install %s!"), TEXT(SZSERVICENAME));//获取调用函数返回的最后错误码
		return FALSE;
	}
	//打开服务管理数据库
	schSCManager=OpenSCManager(
		NULL, //本地计算机
		NULL, //默认的数据库
		SC_MANAGER_ALL_ACCESS //要求所有的访问权
		);
	if(schSCManager)
	{
		//登记服务程序
		schService=CreateService(
			schSCManager, //服务管理数据库句柄
			TEXT(SZSERVICENAME), //服务名
			TEXT(SZAPPNAME),	 //用于显示服务的标识
			SERVICE_ALL_ACCESS, //响应所有的访问请求
			SERVICE_WIN32_OWN_PROCESS, //服务类型
            SERVICE_AUTO_START, //启动类型
			SERVICE_ERROR_NORMAL, //错误控制类型
			szPath, //服务程序磁盘文件的路径
			NULL, //服务不属于任何组
			NULL, //没有tag标识符
			NULL, //启动服务所依赖的服务或服务组,这里仅仅是一个空字符串
			NULL, //LocalSystem 帐号
			NULL);
		if(schService)
		{
			_tprintf(TEXT("%s installed!"), TEXT(SZSERVICENAME));
			CloseServiceHandle(schService);
		}
		else
		{
			_tprintf(TEXT("CreateService failed!"));
		}
		CloseServiceHandle(schSCManager);
	}
	else
	{
		_tprintf(TEXT("OpenSCManager failed!"));
		return FALSE;
	}
	return TRUE;
}
BOOL UnInstall()
{
	if (!IsInstalled())
	{
		_tprintf(TEXT("%s not exist!"), TEXT(SZSERVICENAME));
		return TRUE;
	}
	SC_HANDLE schSCManager = ::OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if (schSCManager == NULL)
	{
		_tprintf(TEXT("OpenSCManager failed!"));
		return FALSE;
	}
	SC_HANDLE schService = ::OpenService(schSCManager, SZSERVICENAME, SERVICE_STOP | DELETE);
	if (schService == NULL)
	{
		::CloseServiceHandle(schSCManager);
		_tprintf(TEXT("OpenService failed!"));
		return FALSE;
	}
	SERVICE_STATUS status;
	::ControlService(schService, SERVICE_CONTROL_STOP, &status);
	//删除服务
	BOOL bDelete = ::DeleteService(schService);
	::CloseServiceHandle(schService);
	::CloseServiceHandle(schSCManager);
	if (bDelete)
	{
		_tprintf(TEXT("%s removed!"), TEXT(SZSERVICENAME));
		return TRUE;
	}
	_tprintf(TEXT("%s removed failed!"), TEXT(SZSERVICENAME));
	return FALSE;
}
void LogEvent(LPCTSTR pFormat, ...)
{
	TCHAR chMsg[256];
	HANDLE hEventSource;
	LPTSTR lpszStrings[1];
	va_list pArg;
	va_start(pArg, pFormat);
	_vstprintf(chMsg, pFormat, pArg);
	va_end(pArg);
	lpszStrings[0] = chMsg;
	hEventSource = RegisterEventSource(NULL, TEXT(SZSERVICENAME));
	if (hEventSource != NULL)
	{
		ReportEvent(hEventSource, EVENTLOG_INFORMATION_TYPE, 0, 0, NULL, 1, 0, (LPCTSTR*) &lpszStrings[0], NULL);
		DeregisterEventSource(hEventSource);
	}
}
//服务程序主函数。
int service_main(int argc, _TCHAR* argv[])
{
	SERVICE_TABLE_ENTRY dispatchTable[]=
	{
		{TEXT(SZSERVICENAME),(LPSERVICE_MAIN_FUNCTION)Service_Main},
		{ NULL,NULL}
	};
	if((argc>1)&&((*argv[1]=='-')||(argv[1]=="/")))
	{
		if(_stricmp("install",argv[1]+1)==0)
		{
			Install();
		}
		else if(_stricmp("uninstall",argv[1]+1)==0)
		{
			UnInstall();
		}
        else if (_stricmp("debug", argv[1] + 1) == 0)
        {
            ServiceStop();
            return -1;
        }
    }
	else
	{ //如果未能和上面的如何参数匹配，则可能是服务控制管理程序来启动该程序。立即调用
		//StartServiceCtrlDispatcher 函数。
		printf("%s -install    to install the service.\n", SZAPPNAME);
		printf("%s -uninstall  to uninstall the service.\n", SZAPPNAME);
        printf("%s -debug      to run as a console application for debug purpose.\n", SZAPPNAME);

        printf("\nStartServiceCtrlDispatcher being called.\n");
		//把程序主线程连接到服务控制管理程序
        if (!StartServiceCtrlDispatcher(dispatchTable))
        {
            _tprintf(TEXT("StartServiceCtrlDispatcher failed!\n"));
            return GetLastError();
        }
		else
			_tprintf(TEXT("StartServiceCtrlDispatcher OK."));
	}
	return 0;
}
