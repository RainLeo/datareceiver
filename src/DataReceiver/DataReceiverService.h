#pragma once
#include "stdafx.h"
#include "Windows.h"

#define SZAPPNAME       "DataReceiver"		//服务程序名  
#define SZSERVICENAME	"DataReceiverSvc"	//标识服务的内部名
#define TIMEOUT			9000			//报送超时

//内部变量
extern SERVICE_STATUS			ssStatus;
extern SERVICE_STATUS_HANDLE	sshStatusHandle;

//下面的函数由程序实现
void WINAPI Service_Main(DWORD dwArgc, LPTSTR *lpszArgv);
void WINAPI Service_Ctrl(DWORD dwCtrlCode);
BOOL Install();
BOOL IsInstalled();
BOOL UnInstall();
BOOL ReportStatusToSCMgr(DWORD dwCurrentState,DWORD dwWin32ExitCode,DWORD dwWaitHint);
void LogEvent(LPCTSTR pFormat, ...);



BOOL ServiceStart(DWORD dwArgc,LPTSTR* lpszArgv);//开始服务
BOOL ServiceStop();			//停止服务
BOOL ServicePause();		//暂停服务
BOOL ServiceContinue();		//恢复服务

int service_main(int argc, _TCHAR* argv[]);
DWORD WINAPI ServiceWorkerThread(LPVOID lpParam);
