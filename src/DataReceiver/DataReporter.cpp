#include "stdafx.h"
#include "DataReceiver.h"
#include "DataReporter.h"
#include "common/diag/Stopwatch.h"
#include "common/driver/communication/ServerInstruction.h"

CDataReporter::CDataReporter() : ClientService()
{
	//_tagService = NULL;
}
CDataReporter::~CDataReporter()
{
	//_tagService = NULL;
}

ClientCommConfig::ClientCommConfig(const ReaderFile& file) : ClientConfig(file)
{
}

bool ClientCommConfig::load(XmlTextReader &reader)
{
	if (reader.localName() == "reporter")
	{
		while (reader.read())
		{
			loadClientNode(reader);
		}
		return true;
	}
	return false;
}


InstructionDescription* heartbeat()
{
	return new InstructionDescription("Heartbeat", new HeartbeatContext());
}

ClientContext* HeartbeatInstruction::setValue(ClientContext* context)
{
	return context;
}

void client_generateInstructions(void* owner, Instructions* instructions)
{
	HeartbeatInstruction* hi = new HeartbeatInstruction(new InstructionDescription("Heartbeat", new HeartbeatContext()));
	instructions->add(hi);
}

bool CDataReporter::initialize(const ReaderFile* file)
{
	// load communication.config
	ReaderFile temp = *file;
	temp.fileName = "reporter.config";
	ClientCommConfig reader(temp);
	if (!reader.BaseReader::load())
		return false;

	return ClientService::initialize(&reader, client_generateInstructions, heartbeat);
}
bool CDataReporter::unInitialize()
{
	//assert(_tagService);
	//_tagService->removeTagValueChangedHandler(Delegate(this, comm_tagValueChanged), false);

	return true;
}

