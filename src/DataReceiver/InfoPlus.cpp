//

#include "stdafx.h"
#include "InfoPlus.h"
#include "common/diag/Trace.h"

using namespace Common;


CInfoPlus::CInfoPlus() :
    m_field_tag(0),
    m_field_quality_tag(0)
{
    m_bInit = Initialize();
}

CInfoPlus::~CInfoPlus()
{
    Uninitialize();
}

int CInfoPlus::checkerror(ERRBLOCK dberror)
{
	ERRARRAY    error_msg;
	short       error_len;

	if (dberror.ERRCODE != SUCCESS) /* check if there is error code */
	{
		ERRMESS(&dberror, error_msg, &error_len); /* read error message */
		error_msg[error_len] = '\0'; /* add end of string to error message */
		Trace::writeFormatLine("***Error: %d - %s...\n", dberror.ERRCODE, error_msg); /* print error code and error message */
	}
	return(dberror.ERRCODE != SUCCESS); /* return error status */
}

bool CInfoPlus::Initialize()
{
    if (!INISETC())
    {
        Trace::writeLine("Cannot connect to database", Trace::Error);
        return false;
    }
    else
    {
        Trace::writeLine("Connected to database", Trace::Information);
    }

    const char* field_name = "IP_INPUT_VALUE";
    const char* field_name_quality = "IP_INPUT_QUALITY";

    DECODFT(field_name, strlen(field_name), &m_field_tag, &err);
    if (err.ERRCODE)
    {
        Trace::writeLine("Could not get field id of IP_INPUT_VALUE.");
        checkerror(err);
        return false;
    }
    DECODFT(field_name_quality, strlen(field_name_quality), &m_field_quality_tag, &err);
    if (err.ERRCODE)
    {
        Trace::writeLine("Could not get field id of IP_INPUT_QUALITY.");
        checkerror(err);
        return false;
    }

    return true;
}

TagType CInfoPlus::GetTagType(long tagId)
{
    if (m_mapTagTypes.find(tagId) != m_mapTagTypes.end())
    {
        return m_mapTagTypes[tagId];
    }

    long recid = tagId;
    long  ptfts[1] = { m_field_tag };
    short ptdtypes[1] = { DTYPREAL };
    float data[1] = { 0.0 };
    short numok = 0;
    RDBVALS(recid, 1,
        ptfts,     /* Fields tags of the fields to read            */
        ptdtypes,  /* datat types of fields to read                */
        data,   /* Array of addresses for the destination buffer
                for each field requested                     */
                &numok,       /* Number of fields read successfully           */
                &err          /* overall error code                           */
                );
    if (err.ERRCODE == SUCCESS)
    {
        m_mapTagTypes[recid] = TagType::Float32;
    }
    else
    {
        ptdtypes[0] = DTYPLONG;
        RDBVALS(recid, 1,
            ptfts,     /* Fields tags of the fields to read            */
            ptdtypes,  /* datat types of fields to read                */
            data,   /* Array of addresses for the destination buffer
                    for each field requested                     */
                    &numok,       /* Number of fields read successfully           */
                    &err          /* overall error code                           */
                    );
        if (err.ERRCODE == SUCCESS)
        {
            m_mapTagTypes[recid] = TagType::Integer32;
        }
        else if (err.ERRCODE = -21)
        {
            m_mapTagTypes[recid] = TagType::Integer16;
        }
        else
        {
            return TagType::Null;
        }
    }

    return m_mapTagTypes[recid];
}

int CInfoPlus::Uninitialize()
{
    //结束与服务器的连接 
    ENDSETC();

    return 0;
}

long CInfoPlus::GetTagId(string &tagName)
{
    long recid = -1;

    GetTagId(tagName, recid);

    return recid;
}

bool CInfoPlus::GetTagId(string &tagName, long &recid)
{
    const char* tag = tagName.c_str();

    DECODNAM(tag, strlen(tag), &recid, &err);
    if (err.ERRCODE)
    {
        Debug::writeFormatLine("Could not decode tag name (%s) to record id.", tag, recid);
        //checkerror(err);
        return false;
    }

    GetTagType(recid);

    return true;
}

bool CInfoPlus::WriteDb(const TagValueReq &tagValueReq)
{
    vector<TagValue *> tagValues = tagValueReq.tagValues;
    return WriteDb(tagValues);
}

bool CInfoPlus::WriteDb(vector<TagValue *> tagValues)
{
    Trace::writeFormatLine("Begin to write into database...");

    int count = 0;
    while (!m_bInit && count < 3)
    {
        Sleep(100 * count);
        count++;
        Uninitialize();
        m_bInit = Initialize();
    }

    if (!m_bInit)
    {
        // Error connecting to InfoPlus database
        return false;
    }

    int NUMREC = tagValues.size(); // Maximum number of data in one time
    if (NUMREC > CDeviceConfiguration::MAX_TAG_NUM)
    {
        Trace::writeFormatLine("Maximum number of tag records (%d) exceeds limit (%d) and truncated.", NUMREC, CDeviceConfiguration::MAX_TAG_NUM);
        NUMREC = CDeviceConfiguration::MAX_TAG_NUM;
    }
    IDANDFT *idanddf = new IDANDFT[NUMREC];
    short *ptdtypes = new short[NUMREC];
    RVANDQU *data = new RVANDQU[NUMREC];

    //debug info: float, short & long(int) are same sizes in InfoPlus.21
    //Trace::writeFormatLine("Debug: size of R (%d), S(%d), L(%d)", sizeof(RVANDQU), sizeof(SVANDQU), sizeof(LVANDQU));
    // all are same size: 16 bytes for above 3 types

    int i = 0;
    for (vector<TagValue *>::const_iterator ci = tagValues.begin(); ci != tagValues.end() && i < NUMREC; ci++)
    {
        TagValue &tagValue = **ci;
        string strTag = tagValue.name();
        long recid = tagValue.id();

        //if (!GetTagId(strTag, recid))
        //{
        //    continue;
        //}
        short qualityDb = QSTATUS_INITIAL;
        switch (tagValue.qualitystamp())
        {
        case TagValue::Qualitystamp::Good:
            qualityDb = QSTATUS_GOOD;
            break;
        case TagValue::Qualitystamp::Bad:
            qualityDb = QSTATUS_BAD;
            break;
        default:
            qualityDb = QSTATUS_NO_STATUS;
            break;
        }

        idanddf[i].RECIDENT = recid;
        idanddf[i].FTIDENT = m_field_tag;
        switch (GetTagType(recid)/*tagValue.type()*/)
        {
        case TagType::Integer16:
        case TagType::Digital:
            ptdtypes[i] = DTYPQUSHRT;
            tagValue.getValue((*((SVANDQU *)(data + i))).SVALUE);
            (*((SVANDQU *)(data + i))).QSTATUS = qualityDb;
            break;
        case TagType::Integer32:
            ptdtypes[i] = DTYPQULONG;
            tagValue.getValue((int&)(*((LVANDQU *)(data + i))).LVALUE);
            (*((LVANDQU *)(data + i))).QSTATUS = qualityDb;
            break;
        case TagType::Null:
            Trace::writeFormatLine("Error: the data field type of recid %d (%s) is invalid. %d tags NOT write into database.", recid, strTag.c_str(), NUMREC);
            delete idanddf;
            delete ptdtypes;
            delete data;
            return false;
        default:
            ptdtypes[i] = DTYPQUREAL;
            tagValue.getValue(data[i].RVALUE);
            data[i].QSTATUS = qualityDb;
            break;
        }
        // all type's timestamp is in same location of the structures
        data[i].TIMESTAMP.secs = (int)((tagValue.timestamp().ticks() - DateTime(1970, 1, 1).ticks()) / (uint64_t)1e7);
        data[i].TIMESTAMP.usecs = (int)((tagValue.timestamp().ticks() - DateTime(1970, 1, 1).ticks()) * 10 % (uint64_t)1e6);
        i++;
    }

    short numOk = 0;

    //MRDBVALS(NUMREC, idanddf, ptdtypes, data, &numOk, &err);
    MWDBVALS(i, idanddf, ptdtypes, data, &numOk, &err);

    Trace::writeFormatLine("End:   %d (of %d) tag records => database.", numOk, i);

    delete idanddf;
    delete ptdtypes;
    delete data;

    if (err.ERRCODE)
    {
        Trace::writeFormatLine("Error writing into database.");
        checkerror(err);
        if (err.ERRCODE == -21)
        {
            Trace::writeFormatLine("Warning: -21 error ignored to NOT blocking the queue or NOT restarting this app. The error may be fixed by changing the data field type in database or in xml configuration file.");
            return true;
        }
        return false;
    }

    return true; // success
}

	////读数据 
	//float pValue;

	//DB2REAL(recid, field_tag/*FT_IP_INPUT_VALUE*/, &pValue, &err);
	//if (err.ERRCODE)
	//{
	//	Trace::writeLine("Cannot read from db.");
	//	checkerror(err);
	//}
