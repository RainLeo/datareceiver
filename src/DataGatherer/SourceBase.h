#pragma once
#include "interface.h"
#include <map>

class CSourceBase
{
public:
    CSourceBase();
    virtual ~CSourceBase();

    virtual void Init(TagConfiguration & tagConfiguration, CDeviceConfiguration *pDevConfig);
    virtual void Fetch() = 0;
    bool UpdateTag(string tagName, TagValue *pTagValue);
    TagValue* GetTag(string tagName);
protected:
    TagConfiguration *m_pTagConfiguration;
    typedef map<string, TagValue*> TagMap;
    TagMap m_tag_map;

    void clearTagCache();
};

