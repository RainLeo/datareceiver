#include "SourceBase.h"
#include "DataGatherer.h"

#define IGNORE_DUPLICATE_TAG

CSourceBase::CSourceBase()
{
}


CSourceBase::~CSourceBase()
{
    clearTagCache();
}

void CSourceBase::Init(TagConfiguration & tagConfiguration, CDeviceConfiguration *pDevConfig)
{
    clearTagCache();
}

void CSourceBase::clearTagCache()
{
    for (TagMap::const_iterator ci = m_tag_map.begin(); ci != m_tag_map.end(); ci++)
    {
        if (ci->second != NULL)
        {
            delete ci->second;
        }
    }
    m_tag_map.clear();
}

bool CSourceBase::UpdateTag(string tagName, TagValue *pTagValue)
{
    TagValue *pOldTagValue = GetTag(tagName);
    if (pOldTagValue == NULL)
    {
        m_tag_map[tagName] = new TagValue();
        *m_tag_map[tagName] = *pTagValue;
    }
    else if(*pTagValue == *pOldTagValue)
    {
#ifdef IGNORE_DUPLICATE_TAG
        //Debug::writeFormatLine("Tag %s NOT updated.", tagName.c_str());
        return false;
#endif
    }
    else
    {
        *pOldTagValue = *pTagValue; // Update cache;
    }

    if (pTagValue->isNullValue())
    {
        return false;
    }

    CDataGatherer::Instance()->GatherTagValue(pTagValue);
    //Debug::writeFormatLine("Tag %s updated.", tagName.c_str());
    return true;
}

TagValue* CSourceBase::GetTag(string tagName)
{
    TagMap::const_iterator ci = m_tag_map.find(tagName);
    if (ci != m_tag_map.end())
    {
        return ci->second;
    }

    return NULL;
}