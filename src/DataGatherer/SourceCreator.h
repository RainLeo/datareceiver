#pragma once
#include <map>
#include "SourceBase.h"

using namespace std;

typedef CSourceBase* (*CreateSourceFn)(void);

class CSourceCreator
{
private:
    CSourceCreator();
    CSourceCreator(const CSourceCreator &) { }
    CSourceCreator &operator=(const CSourceCreator &) { return *this; }

    typedef map<byte, CreateSourceFn> FactoryMap;
    FactoryMap m_FactoryMap;
public:
    ~CSourceCreator() { m_FactoryMap.clear(); }

    static CSourceCreator *Instance()
    {
        static CSourceCreator instance;
        return &instance;
    }

    void Register(const byte type, CreateSourceFn pfnCreate);
    CSourceBase *CreateSource(const byte type);
};

