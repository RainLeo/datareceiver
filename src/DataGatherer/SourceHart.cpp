#include "SourceHart.h"


CSourceHart::CSourceHart()
{
}


CSourceHart::~CSourceHart()
{
}

string CSourceHart::GetChannelName()
{
    return "Hart_channel";
}

string CSourceHart::GetRegisterName(byte function)
{
    return Convert::convertStr(function);
}

void CSourceHart::InitDeviceInfo(XmlTextWriter &xw, CDeviceConfiguration *pDevConfig, const string address)
{
    static int devIndex = 0;
    CDeviceModBusDetail *pDetail = dynamic_cast<CDeviceModBusDetail*>(pDevConfig->pDetail);

    xw.writeStartElement("device");
    xw.writeAttributeString("name", "Hart_Master" + Convert::convertStr(devIndex++));
    xw.writeAttributeString("address", /*Convert::convertStr(pDetail->address)*/address);
    xw.writeAttributeString("channel", GetChannelName());
    xw.writeAttributeString("type", "HartDevice");
    xw.writeAttributeString("property", /*"manuid:253;devtype:253;devid:0x010203"*/"framelength:short");
    xw.writeAttributeString("batchread", "true");
    xw.writeAttributeString("batchwrite", "false");
    xw.writeAttributeString("enable", "true");
}