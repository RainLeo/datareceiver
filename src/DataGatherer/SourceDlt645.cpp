#include "SourceDlt645.h"
#include "DeviceDlt645.h"


CSourceDlt645::CSourceDlt645()
{
}


CSourceDlt645::~CSourceDlt645()
{
}

string CSourceDlt645::GetChannelName()
{
    return "DLT645_channel";
}

string CSourceDlt645::GetRegisterName(byte function)
{
    return Convert::convertStr(function);
}

void CSourceDlt645::InitTagAddr(XmlTextWriter &xw, const DeviceTag *pDeviceTag)
{
    xw.writeAttributeString("register", pDeviceTag->strRegisterName);
}

void CSourceDlt645::InitTagProperty(XmlTextWriter &xw, const DeviceTag *pDeviceTag)
{
    // No bigendian or property:inverse
}

void CSourceDlt645::InitDeviceInfo(XmlTextWriter &xw, CDeviceConfiguration *pDevConfig, const string address)
{
    static int devIndex = 0;
    CDeviceDlt645Detail *pDetail = dynamic_cast<CDeviceDlt645Detail*>(pDevConfig->pDetail);

    xw.writeStartElement("device");
    xw.writeAttributeString("name", "DLT645_Master" + Convert::convertStr(devIndex++));
    xw.writeAttributeString("address", "");
    xw.writeAttributeString("channel", GetChannelName());
    xw.writeAttributeString("type", "DLT645Device");
    xw.writeAttributeString("property", "address:" + address);
    xw.writeAttributeString("batchread", "false");
    xw.writeAttributeString("batchwrite", "false");
    xw.writeAttributeString("enable", "true");
}