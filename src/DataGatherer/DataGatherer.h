#pragma once

#include "common/driver/communication/ClientService.h"
#include "common/system/Singleton.h"
#include "common/system/DateTime.h"
#include "common/thread/Timer.h"
#include "interface.h"
#include "SourceBase.h"
#include "SourceCreator.h"
#include <map>
#include "TagValueCache.h"
#include "common/driver/channels/DtuService.h"
#include "ClientCommConfig.h"

using namespace Common;
using namespace Driver;
void staticSendLogProc(void* parameter);

class GathererService : public ClientService
{
public:
    GathererService(const Client& client) : ClientService(client) {}
    void createDualPortsRamDevice(const BaseCommService::InstructionCallback &callback, const int portIndex = 1);
    bool isOnline();
};

class CDataGatherer
{
public:
	CDataGatherer();
	~CDataGatherer();
	bool initialize(const ConfigFile* file);
	bool unInitialize();
	bool sendTags(const TagValueReq &input)
	{
        bool ret = m_pClientService->sendAsync<TagValueReq, TagValueContext>(input, "TagValue");
        return ret;
	}
    static CDataGatherer * Instance() { return Singleton<CDataGatherer>::instance(); }
    void StopFetch();
    void StartFetch(const TagConfiguration &tagConfiguration);
	void FetchProc();
	void SetConfigFile(const TagConfiguration &tagConfiguration, CDeviceConfiguration *pDevConfig);
	void ReadDataFile();
    bool GatherTagValue(TagValue *pTagValue);
    bool IsStarting() { return m_bStarting; }
    void Started() { m_bStarting = false; }
    bool IsTagConfigurationReceived()
    {
        return m_bRecvTagConfiguration;
    }
    bool IsMaintainRequested()
    {
        return true;// m_bMaintainRequested;
    }
    void RequestMaintain()
    {
        m_bMaintainRequested = true;
    }
    bool sendMaintain(MaintainUpReq &input)
    {
        m_bMaintainRequested = false;
        return m_pClientService->sendAsync<MaintainUpReq, MaintainUpContext>(input, "MaintainUp");
    }
    void MaintainProc();

    FileHeader header;
    Thread *m_pSendLogThread;
    bool sendLog(const string &path, const string &fileName)
    {
#ifdef DEBUG
        m_pSendLogThread->setName("SendLog thread");
#endif
        header.path = path;
        header.file_name = fileName;
        m_pSendLogThread->start(staticSendLogProc, &header);

        return true;
    }

    bool sendLog(FileHeader &header)
    {
        UploadLogReq input;
        input.programId = PROGRAM_ID_GATHERER;
        return m_pClientService->uploadFileSync<UploadLogReq, UploadLogContext>(header, input, "UploadLog");
    }

    void SetFetchInterval(int interval)
    {
        m_iFetchInterval = interval;
    }
    int GetFetchInterval()
    {
        return m_iFetchInterval;
    }
    ClientCommConfig& Config()
    {
        return *m_pConfig;
    }
    void SetOpcStatus(int status)
    {
        m_iOpcStatus = status;
    }
    void SetRunningStatus(int status)
    {
        m_iRunningStatus = status;
    }

private:
    DECLARE_SINGLETON_CLASS(CDataGatherer);
    Timer *m_pFetchTimer;
    Timer *m_pMaintainTimer;
    int m_iFetchInterval;
    TagConfiguration m_tagConfiguration;
    string data_file_path;
    string configuration_file_path;
    bool m_bStarting;
    bool m_bRecvTagConfiguration;
    bool m_bMaintainRequested;
    vector<CSourceBase *> m_sources;
    vector<TagValue *> *m_pTagList;
    TagValueCache *m_pTagValueCache;
    mutex m_mutexFetch;
    GathererService *m_pClientService;
    DtuService *m_pDtuService;
    ClientCommConfig *m_pConfig;
    int m_iOpcStatus;
    int m_iRunningStatus;
};
