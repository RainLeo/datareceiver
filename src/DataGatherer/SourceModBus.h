#pragma once
#include "SourceBase.h"
#include "../../EVCommon/src/driver/DriverService.h"
#include "../../EVCommon/src/tag/TagService.h"
#include "DeviceModBus.h"

class CSourceModBus :
    public CSourceBase
{
public:
    CSourceModBus();
    virtual ~CSourceModBus();
    DriverService *m_pDriverService;
    TagService *m_pTagService;

    static CSourceBase * Create() { return new CSourceModBus(); }

    void Init(TagConfiguration &tagConfiguration, CDeviceConfiguration *pDevConfig);
    virtual string GetChannelName();
    virtual string GetRegisterName(byte function);
    virtual void InitDeviceInfo(XmlTextWriter &xw, CDeviceConfiguration *pDevConfig, const string address);
    virtual void InitTagAddr(XmlTextWriter &xw, const DeviceTag *pDeviceTag);
    virtual void InitTagProperty(XmlTextWriter &xw, const DeviceTag *pDeviceTag);
    virtual void Fetch();
    void Clear();
private:
    SerialInfo getSerialInfo(CDeviceSerialDetail *pDetail);
    map<int, int> m_mapTagIds;
};

