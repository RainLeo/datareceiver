#pragma once
#include "SourceModBus.h"
class CSourceHart :
    public CSourceModBus
{
public:
    CSourceHart();
    virtual ~CSourceHart();

    static CSourceBase * Create() { return new CSourceHart(); }

    virtual string GetChannelName();
    virtual string GetRegisterName(byte function);
    virtual void InitDeviceInfo(XmlTextWriter &xw, CDeviceConfiguration *pDevConfig, const string address);
};

