#include "SourceModBus.h"
#include "common/system/Application.h"
#include "DataGatherer.h"
#include "DeviceModBus.h"

CSourceModBus::CSourceModBus()
{
    m_pDriverService = NULL;
    m_pTagService = NULL;
}

CSourceModBus::~CSourceModBus()
{
    Clear();
}

void CSourceModBus::Clear()
{
    // Note: Must clear DriverService first, or assert will trigger because TagService is null
    if (m_pDriverService != NULL)
    {
        m_pDriverService->unInitialize();
        delete m_pDriverService;
        m_pDriverService = NULL;
    }
    if (m_pTagService != NULL)
    {
        m_pTagService->unInitialize();
        delete m_pTagService;
        m_pTagService = NULL;
    }

    m_mapTagIds.clear();
}

SerialInfo CSourceModBus::getSerialInfo(CDeviceSerialDetail *pDetail)
{
#ifdef WIN32
    string portName = "COM";
#else // Linux
    string portName = "/dev/ttyS";
#endif
    portName += Convert::convertStr(pDetail->serialIndex);
    SerialInfo serial(portName);
    serial.baudRate = pDetail->serialBaudRate;
    serial.dataBits = (SerialInfo::DataBitsType)pDetail->serialCharacterSize;
    serial.stopBits = (SerialInfo::StopBitsType)pDetail->serialStopBits;
    serial.parity = (SerialInfo::ParityType)pDetail->serialParity;
    return serial;
}

string CSourceModBus::GetChannelName()
{
    return "ModBus_channel";
}

string CSourceModBus::GetRegisterName(byte function)
{
    return "0" + Convert::convertStr(function);
}

void CSourceModBus::InitDeviceInfo(XmlTextWriter &xw, CDeviceConfiguration *pDevConfig, const string address)
{
    static int devIndex = 0;
    CDeviceModBusDetail *pDetail = dynamic_cast<CDeviceModBusDetail*>(pDevConfig->pDetail);

    xw.writeStartElement("device");
    xw.writeAttributeString("name", "ModBus_Master" + Convert::convertStr(devIndex++));
    xw.writeAttributeString("address", address);
    xw.writeAttributeString("channel", GetChannelName());
    xw.writeAttributeString("type", "ModbusDevice");
    xw.writeAttributeString("property", pDetail->mode == 2 ? "mode:ASCII" : "mode:RTU");
    xw.writeAttributeString("batchread", "true");
    xw.writeAttributeString("batchwrite", "false");
    xw.writeAttributeString("enable", "true");
}

void CSourceModBus::InitTagAddr(XmlTextWriter &xw, const DeviceTag *pDeviceTag)
{
    xw.writeAttributeString("register", GetRegisterName(pDeviceTag->function)); // Function: 03 Holding Register(4x)
    xw.writeAttributeString("address", Convert::convertStr(pDeviceTag->sStartAddr));
    xw.writeAttributeString("length", Convert::convertStr(pDeviceTag->sUnitLen));
}

void CSourceModBus::InitTagProperty(XmlTextWriter &xw, const DeviceTag *pDeviceTag)
{
    xw.writeAttributeString("bigendian", Convert::convertStr(pDeviceTag->bigEndian));
    xw.writeAttributeString("property", "inverse:" + Convert::convertStr(pDeviceTag->inverse));
}

void CSourceModBus::Init(TagConfiguration &tagConfiguration, CDeviceConfiguration *pDevConfig)
{
    CSourceBase::Init(tagConfiguration, pDevConfig);

    Clear();

    ConfigFile _rootFile;
    _rootFile.rootPath = Application::instance()->rootPath();


    m_pTagService = new TagService();
    int nTagId = 500;

    //m_pTagService->initialize(&_rootFile);

    m_pDriverService = new DriverService();
    m_pDriverService->addService(m_pTagService);

    CDeviceSerialDetail *pDetail = dynamic_cast<CDeviceSerialDetail*>(pDevConfig->pDetail);

    string fullFileName = Path::combine(Application::instance()->rootPath(), "driver.config");
    string channelName = GetChannelName();
    XmlTextWriter xw(fullFileName);
    xw.enableIndent();
    xw.writeStartDocument();
    xw.writeStartElement("driver");
    xw.writeAttributeString("allowlog", "false"); // Change to true to print messages for debug purpose
    xw.writeAttributeString("compareTimestamp", "true"); // Driver tag timestamp updated even the next sample value is same as previous one
    xw.writeStartElement("channel");
    xw.writeAttributeString("name", channelName);
    xw.writeAttributeString("linktype", pDetail->communicationTypeStr());
    xw.writeAttributeString("receive", Convert::convertStr(CDataGatherer::Instance()->Config().m_TagReceiveTimeout));

    switch (pDetail->communicationType)
    {
    case CDeviceSerialDetail::COMMUNICATION_TYPE_SERIAL:
        getSerialInfo(pDetail).write(xw);
        break;
    case CDeviceSerialDetail::COMMUNICATION_TYPE_DTU:
        xw.writeAttributeString("dtuid", pDetail->dtuId);
        xw.writeAttributeString("dtuvendor", "caimao");
        break;
    case CDeviceSerialDetail::COMMUNICATION_TYPE_DTU_HONGDIAN:
        xw.writeAttributeString("dtuid", pDetail->dtuId);
        xw.writeAttributeString("dtuvendor", "hongdian");
        break;
    case CDeviceSerialDetail::COMMUNICATION_TYPE_DTU_LICHUANG:
        xw.writeAttributeString("dtuid", pDetail->dtuId);
        xw.writeAttributeString("dtuvendor", "lichuang");
        break;
    case CDeviceSerialDetail::COMMUNICATION_TYPE_TCP:
        // TCP is not supported by now, Reuse serial structure for debug purpose only
        xw.writeAttributeString("address", Convert::convertIPStr(pDetail->serialIndex, pDetail->serialCharacterSize, pDetail->serialStopBits, pDetail->serialParity));
        xw.writeAttributeString("port", Convert::convertStr(pDetail->serialBaudRate));
        break;
    default:
        break;
    }
    xw.writeEndElement(); // End Channel

    // DeviceInfo
    string strDeviceAddr = "NULL"; // init as null and any device should not be it even not use it

    // Tag
    uint tagId = 0;
    // interval for the device to fetch, usually half of the global interval or less to make sure a sample collected during a global interval
    int interval = CDataGatherer::Instance()->GetFetchInterval();
    if (interval == 0)
    {
        interval = tagConfiguration.iFetchInterval;
    }
    if (interval < 1000) interval = 1000; // at least 1 second

    for (vector<DeviceTag>::const_iterator ci = pDevConfig->vecTags.begin(); ci != pDevConfig->vecTags.end(); ci++)
    {
        nTagId++;
        m_mapTagIds[nTagId] = ci->tagId; // add tag id map to avoid modification to Tags & TagService code from EVCommon
        Tag* tag = new Tag(ci->GetTagType(), nTagId, ci->strName);
        m_pTagService->addTag(tag);

        if (ci->address != strDeviceAddr)
        {
            if (tagId > 0)
            {
                xw.writeEndElement(); // End last device first before start a new device
            }
            InitDeviceInfo(xw, pDevConfig, ci->address);
            strDeviceAddr = ci->address;
            if (pDevConfig->type == DEVICE_HART && ci->function != 0)
            {
                // Add a dummy tag with register = 0 (hart cmd 0)
                tagId++;
                xw.writeStartElement("tag");
                xw.writeAttributeString("name", ci->strName);
                xw.writeAttributeString("id", Convert::convertStr(tagId));
                xw.writeAttributeString("type", Tag::toTypeStr(ci->GetTagType()));
                DeviceTag dummyTag(*ci);
                dummyTag.function = 0; // cmd 0
                InitTagAddr(xw, &dummyTag);
                xw.writeAttributeString("interval", TimeSpan::fromSeconds(180).toString()); // Adjusted to be in the time limit of disconnection
                xw.writeAttributeString("access", "read");
                xw.writeAttributeString("transform", "none");
                xw.writeAttributeString("linkedid", Convert::convertStr(nTagId));
                InitTagProperty(xw, &*ci);
                xw.writeEndElement(); // End Tag
            }
        }
        tagId++;
        xw.writeStartElement("tag");
        xw.writeAttributeString("name", ci->strName);
        xw.writeAttributeString("id", Convert::convertStr(tagId));
        xw.writeAttributeString("type", Tag::toTypeStr(ci->GetTagType()));
        InitTagAddr(xw, &*ci);
        xw.writeAttributeString("interval", TimeSpan::fromMilliseconds(interval).toString());
        xw.writeAttributeString("access", "read");
        xw.writeAttributeString("transform", "none");
        xw.writeAttributeString("linkedid", Convert::convertStr(nTagId));
        InitTagProperty(xw, &*ci);
        xw.writeEndElement(); // End Tag
    }
    xw.writeEndElement(); // End Device
    xw.writeEndDocument();
    xw.close(); // Make sure the file is saved

    m_pDriverService->initialize(&_rootFile);
}

void CSourceModBus::Fetch()
{
    if (m_pTagService == NULL) return;

    const Tags &tags = m_pTagService->allTags();
    for (uint i = 0; i < tags.count(); i++)
    {
        if (tags[i] == NULL) continue;
        if (tags[i]->isEmpty() || m_mapTagIds.find(tags[i]->id()) == m_mapTagIds.end()) continue;

        Tag *pTag = new Tag(tags[i]->type(), m_mapTagIds[tags[i]->id()], tags[i]->name());
        pTag->setValue(tags[i]->value(), tags[i]->qualitystamp(), tags[i]->timestamp());
        if (!UpdateTag(pTag->name(), pTag))
        {
            delete pTag;
            pTag = NULL;
        }
    }
}