#include "SourceCreator.h"
#include "SourceFile.h"
#include "SourceModBus.h"
#include "SourceHart.h"
#include "SourceDlt645.h"

CSourceCreator::CSourceCreator()
{
    Register(DEVICE_FILE, &CSourceFile::Create);
    Register(DEVICE_MODBUS, &CSourceModBus::Create);
    Register(DEVICE_HART, &CSourceHart::Create);
    Register(DEVICE_DL_T645, &CSourceDlt645::Create);
}

void CSourceCreator::Register(const byte type, CreateSourceFn pfnCreate)
{
    m_FactoryMap[type] = pfnCreate;
}

CSourceBase *CSourceCreator::CreateSource(const byte type)
{
    FactoryMap::iterator it = m_FactoryMap.find(type);
    if (it != m_FactoryMap.end())
    {
        return it->second();
    }
    return NULL;
}
