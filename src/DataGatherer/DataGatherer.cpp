// DataGatherer.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#if WIN32
#if _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif
#else
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#endif
#include <iostream>
#include "DataGatherer.h"
#include "common/system/Application.h"
#include "common/diag/Trace.h"
#include "common/diag/Stopwatch.h"
#include "common/driver/communication/ServerInstruction.h"
#include "common/IO/File.h"
#include "common/IO/FileInfo.h"
#include "common/thread/Thread.h"
#include "DualPortsRamInteractive.h"
#include "SystemInfo.h"
#include "DeviceFile.h"
#if WIN32
#include "gprsdll.h"
#endif

#if WIN32
//int _tmain(int argc, char* argv[])
int CALLBACK WinMain(
    _In_  HINSTANCE hInstance,
    _In_  HINSTANCE hPrevInstance,
    _In_  LPSTR lpCmdLine,
    _In_  int nCmdShow
    )
#else
void daemonize(void)
{
    pid_t pid;

    /*
    * Become a session leader to lose controlling TTY.
    */
    if ((pid = fork()) < 0) {
        perror("fork");
        exit(1);
    }
    else if (pid != 0) /* parent */
        exit(0);
    setsid();

    /*
    * Change the current working directory to the root.
    */
    if (chdir("/") < 0) {
        perror("chdir");
        exit(1);
    }

    /*
    * Attach file descriptors 0, 1, and 2 to /dev/null.
    */
    close(0);
    open("/dev/null", O_RDWR);
    dup2(0, 1);
    dup2(0, 2);
}

int main(int argc, char* argv[])
#endif
{
#ifdef WIN32
#if defined(_DEBUG) || defined(DEBUG)
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

    AllocConsole();
    // redirect unbuffered STDOUT to the console

    intptr_t pStdHandle = reinterpret_cast<intptr_t>(::GetStdHandle(STD_OUTPUT_HANDLE));
    int	hConHandle = _open_osfhandle(pStdHandle, _O_TEXT);

    FILE* pFile = _fdopen(hConHandle, "w");
    *stdout = *pFile;
    setvbuf(stdout, NULL, _IONBF, 0);

    // redirect unbuffered STDIN to the console

    pStdHandle = reinterpret_cast<intptr_t>(::GetStdHandle(STD_INPUT_HANDLE));
    hConHandle = _open_osfhandle(pStdHandle, _O_TEXT);
    pFile = _fdopen(hConHandle, "r");
    *stdin = *pFile;
    setvbuf(stdin, NULL, _IONBF, 0);

    // redirect unbuffered STDERR to the console

    pStdHandle = reinterpret_cast<intptr_t>(::GetStdHandle(STD_ERROR_HANDLE));
    hConHandle = _open_osfhandle(pStdHandle, _O_TEXT);
    pFile = _fdopen(hConHandle, "w");
    *stderr = *pFile;
    setvbuf(stderr, NULL, _IONBF, 0);

    // make cout, wcout, cin, wcin, wcerr, cerr, wclog and clog

    // point to console as well

    ios::sync_with_stdio();

    //::SetConsoleTitle(title.c_str());
#endif

    Trace::writeFormatLine("Gatherer app starting. Version: %s", AppVersion.toString().c_str());

	Application app;

	ConfigFile _rootFile;
	_rootFile.rootPath = app.rootPath();
	CDataGatherer *pGatherer = CDataGatherer::Instance();
    // try to parse the last argument as fetching interval
#ifndef WIN32
    if (argc > 1)
    {
        int interval = atoi(argv[argc - 1]);
        if (interval > 0)
        {
            pGatherer->SetFetchInterval(interval);
        }
    }
#endif
    pGatherer->initialize(&_rootFile);
    Trace::writeFormatLine("Gatherer app started. Version: %s", AppVersion.toString().c_str());

#if !WIN32
    if(argc == 2 && strcmp(argv[1], "d") == 0)
    {
        daemonize();
    }
#endif

#if defined(WIN32)
//    string input;
//    while (!Convert::stringEqual(input, "exit", true))
//    {
//        Trace::writeLine("Gatherer: Input 'exit' to exit...");
//        getline(cin, input);
//    }

    // Message loop is necessary for CaiMao DTU gprsdll.dll to work correctly. Console application doesn't support gprsdll.dll.
    // message loop to process user input 
    MSG msg;
    while (true)
    {
        if (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            // process message 
            ::TranslateMessage(&msg);
            ::DispatchMessage(&msg);
        }
    }
#else
    while (true)
    {
        Thread::msleep(3000);
    }
#endif

    Trace::writeFormatLine("Gatherer app shuts down.");
    delete pGatherer;
    return 0;
}

vector<Alarm> GetAlarms()
{
    vector<Alarm> alarms;
    CDataGatherer *pDataGatherer = CDataGatherer::Instance();
    if (pDataGatherer != NULL)
    {
        if (pDataGatherer->IsStarting())
        {
            Alarm alarm;
            alarm.programId = PROGRAM_ID_REPORTER;
            alarm.alarm = Alarm::ALARM_RESTART;
            alarms.push_back(alarm);
        }
        if (getDiskInfo() < 10)
        {
            Alarm alarm;
            alarm.programId = PROGRAM_ID_REPORTER;
            alarm.alarm = Alarm::ALARM_INSUFFICIENT_DISK;
            alarms.push_back(alarm);
        }
        if (Alarm::IsHighCpu())
        {
            Alarm alarm;
            alarm.programId = PROGRAM_ID_REPORTER;
            alarm.alarm = Alarm::ALARM_HIGH_CPU;
            alarms.push_back(alarm);
        }
    }
    return alarms;
}

bool WriteDb(const TagValueReq &tagValueReq)
{
    CDataGatherer *pDg = CDataGatherer::Instance();
    assert(pDg);

    return pDg->sendTags(tagValueReq);
}

void staticSendLogProc(void* parameter)
{
    CDataGatherer* app = CDataGatherer::Instance();

    app->sendLog(*(FileHeader *)parameter);
}

void staticMaintainProc(void* parameter)
{
    CDataGatherer* pDataGatherer = (CDataGatherer*)parameter;
    pDataGatherer->MaintainProc();
}

void CDataGatherer::MaintainProc()
{
    int cpu = getCpuInfo();
    Alarm::CheckHighCpu(cpu);
    if (IsMaintainRequested())
    {
        int memUsed = 0, memFree;
        memFree = getMemInfo(memUsed);
        int disk = getDiskUsagePercentage();
#if DEBUG
        printf("Gatherer: disk usage: %d%%, free mem: %d M, used mem: %d M, cpu: %d%%\n", disk, memFree, memUsed, cpu);
#endif
        MaintainUpReq input;
        Maintain maintain;
        maintain.programId = PROGRAM_ID_GATHERER;
        maintain.cpu = cpu;
        maintain.memFree = memFree;
        maintain.memUsed = memUsed;
        maintain.disk = disk;
        maintain.opcStatus = m_iOpcStatus;
#if !WIN32
        if (!procExisted())
        {
            maintain.opcStatus = -100;
        }
#endif
        maintain.runTimeStatus = m_iRunningStatus;
        input.vecMaintain.push_back(maintain);
        sendMaintain(input);
    }
}

CDataGatherer::CDataGatherer() :
    m_pClientService(NULL),
    m_pFetchTimer(NULL),
    m_pMaintainTimer(NULL),
    m_iFetchInterval(0),
    m_bRecvTagConfiguration(false),
    m_bMaintainRequested(false),
    m_bStarting(true),
    data_file_path(""),
    configuration_file_path(""),
    m_pConfig(NULL),
    m_pSendLogThread(NULL),
    m_iOpcStatus(0),
    m_iRunningStatus(0)
{ 
    m_pTagValueCache = TagValueCache::Instance();
    m_pTagValueCache->SetSaveFunc(WriteDb, true); // no need to wait for reporter's response because for TCP: on the same computer, or for Dual ports ram: assumes secure transmission
    m_pSendLogThread = new Thread();
}

CDataGatherer::~CDataGatherer()
{
    unInitialize();

    if (m_pTagValueCache != NULL)
    {
        delete m_pTagValueCache;
        m_pTagValueCache = NULL;
    }

    if (m_pConfig != NULL)
    {
        delete m_pConfig;
        m_pConfig = NULL;
    }

    if (m_pSendLogThread != NULL)
    {
        m_pSendLogThread->stop();
        delete m_pSendLogThread;
        m_pSendLogThread = NULL;
    }

    if (m_pMaintainTimer != NULL)
    {
        delete m_pMaintainTimer;
        m_pMaintainTimer = NULL;
    }
}

void staticFetchProc(void* parameter)
{
	CDataGatherer* pDataGatherer = (CDataGatherer*)parameter;
	pDataGatherer->FetchProc();
}

void CDataGatherer::FetchProc()
{
    Locker locker(&m_mutexFetch);
    TagValueReq req;
    m_pTagList = &req.tagValues;

    for (vector<CSourceBase *>::const_iterator ci = m_sources.begin(); ci != m_sources.end(); ci++)
    {
        if (*ci != NULL)
        {
            (*ci)->Fetch();
        }
    }

    if (m_pTagList->size() == 0) return; // no tag value data gathered
    // Send tagvalue
    TagValueCache::Instance()->WriteTags(req);
}

void CDataGatherer::StopFetch()
{
    if (m_pFetchTimer != NULL)
    {
        delete m_pFetchTimer;
        m_pFetchTimer = NULL;
    }
    Locker locker(&m_mutexFetch);
    for (vector<CSourceBase *>::iterator iter = m_sources.begin(); iter != m_sources.end(); iter++)
    {
        if (*iter != NULL)
        {
            delete *iter;
            *iter = NULL;
        }
    }
    m_sources.clear();
}

void CDataGatherer::StartFetch(const TagConfiguration &tagConfiguration)
{
    m_bRecvTagConfiguration = true;
    int iInterval = CDataGatherer::Instance()->GetFetchInterval();
    if (iInterval == 0)
    {
        iInterval = tagConfiguration.iFetchInterval / 6;
    }
    if (iInterval < 1000) iInterval = 1000;

    m_tagConfiguration.copyFrom(&tagConfiguration);

    StopFetch();

    for (vector<CDeviceConfiguration *>::const_iterator iter = m_tagConfiguration.vecDevices.begin(); iter != m_tagConfiguration.vecDevices.end(); iter++)
	{
		CDeviceConfiguration *pDevConfig = *iter;
        CSourceBase * pSrc = CSourceCreator::Instance()->CreateSource(pDevConfig->type);
        if (pSrc != NULL)
        {
            if (pDevConfig->type == DEVICE_FILE)
            {
                iInterval = 1000; // 1 sec interval, Speed up File type: OPC server
                // override data file path and config file path if needed
                CDeviceFileDetail *pDevFile = dynamic_cast<CDeviceFileDetail *>(pDevConfig->pDetail);
                if (data_file_path != "")
                {
                    pDevFile->strDataFilePath = data_file_path;
                }
                if (configuration_file_path != "")
                {
                    pDevFile->strConfigFilePath = configuration_file_path;
                }
            }
            pSrc->Init(m_tagConfiguration, pDevConfig);
            m_sources.push_back(pSrc);
        }
	}

    if (tagConfiguration.iFetchInterval == 0)
    {
        // do not fetch if fetch interval configuration is zero
        iInterval = 0;
    }

    if (m_sources.size() > 0 && iInterval > 0)
    {
        m_pFetchTimer = new Timer(staticFetchProc, CDataGatherer::Instance(), iInterval);
    }
}

bool CDataGatherer::GatherTagValue(TagValue *pTagValue)
{
    m_pTagList->push_back(pTagValue);
    return true;
}

InstructionDescription* heartbeat()
{
	return new InstructionDescription("Heartbeat", new HeartbeatContext());
}

void client_generateInstructions(void* owner, Instructions* instructions)
{
	HeartbeatInstruction* hi = new HeartbeatInstruction(new InstructionDescription("Heartbeat", new HeartbeatContext()));
	instructions->add(hi);
    ServerTagConfigurationInstruction *ti = new ServerTagConfigurationInstruction(new InstructionDescription("TagConfiguration", new TagConfigurationContext()));
	instructions->add(ti);
	TagValueInstruction* tvi = new TagValueInstruction(new InstructionDescription("TagValue", new TagValueContext()));
	instructions->add(tvi);
    ServerMaintainDownInstruction * smdi = new ServerMaintainDownInstruction(new InstructionDescription("MaintainDown", new MaintainDownContext()));
    instructions->add(smdi);
    MaintainUpInstruction *mui = new MaintainUpInstruction(new InstructionDescription("MaintainUp", new MaintainUpContext()));
    instructions->add(mui);
    Instruction *i = new ServerDownloadLogInstruction(new InstructionDescription("DownloadLog", new DownloadLogContext()));
    instructions->add(i);
    i = new UploadLogInstruction(new InstructionDescription("UploadLog", new UploadLogContext()));
    instructions->add(i);
}

bool CDataGatherer::initialize(const ConfigFile* file)
{
	// load communication.config
	ConfigFile temp = *file;
	temp.fileName = "gatherer.config";
    m_pConfig = new ClientCommConfig(temp, "gatherer");
    ClientCommConfig& reader = Config();
	if (!reader.Configuration::load())
		return false;

    // read overridden data file path and configuration file path
    data_file_path = reader.data_file_path;
    configuration_file_path = reader.configuration_file_path;
    SetFetchInterval(reader.m_TagFetchInterval);
    vector<int>::iterator iterPort = reader.m_dtuPorts.begin();
    vector<string>::iterator iterVendor = reader.m_dtuVendors.begin();
    for (; iterPort != reader.m_dtuPorts.end(), iterVendor != reader.m_dtuVendors.end(); iterPort++, iterVendor++)
    {
        DtuService::Instance()->initialize(*iterPort, *iterVendor);
    }

    BaseCommService::InstructionCallback callback;
    callback.tcpInstructions = client_generateInstructions;
    callback.tcpSampler = heartbeat;

    m_pClientService = new GathererService(reader.client());
    if (reader.client().address == "dual_ports_ram")
    {
        m_pClientService->createDualPortsRamDevice(callback, reader.client().port);
    }
    else
    {
        if (!m_pClientService->initialize(callback))
            return false;
    }

    if (reader.m_MaintainInterval > 0)
    {
        Trace::writeFormatLine("Gatherer maintain timer starts in interval %d ms.", reader.m_MaintainInterval);
        m_pMaintainTimer = new Timer(staticMaintainProc, CDataGatherer::Instance(), reader.m_MaintainInterval);
    }
    return true;
}
bool CDataGatherer::unInitialize()
{
    StopFetch();
    DtuService::Instance()->unInitialize();
    if (m_pClientService != NULL)
    {
        m_pClientService->unInitialize();
        delete m_pClientService;
        m_pClientService = NULL;
    }
    return true;
}

class COMMON_EXPORT DualPortsRamSampler : public Sampler
{
public:
    DualPortsRamSampler(DriverManager* dm, ChannelDescription* cd, DeviceDescription* dd, const Client::Connection& connection, sampler_callback action = NULL);
    ~DualPortsRamSampler();

protected:
    // return the sample interval, unit: ms
    uint detetionInterval() const
    {
        return _detetionInterval;
    }
    // return the resume interval, unit: ms
    uint resumeInterval() const
    {
        return _resumeInterval;
    }
    // return the sample interval, unit: ms
    int detetionCount() const
    {
        return _detetionCount;
    }

    InstructionDescription* sampleInstruction();
    void resultInstruction(InstructionDescription* id) {}
    void setConnectStatus(Device::Status status);

private:
    uint _detetionInterval;
    uint _resumeInterval;
    int _detetionCount;

    sampler_callback _sampleCallback;
};
DualPortsRamSampler::DualPortsRamSampler(DriverManager* dm, ChannelDescription* cd, DeviceDescription* dd, const Client::Connection& connection, sampler_callback action)
: Sampler(dm, cd, dd)
{
    _detetionInterval = (uint)connection.detectionInterval.totalMilliseconds();
    _resumeInterval = (uint)connection.resumeInterval.totalMilliseconds();
    _detetionCount = connection.detectionCount;

    _sampleCallback = action;
}
DualPortsRamSampler::~DualPortsRamSampler()
{
}

void DualPortsRamSampler::setConnectStatus(Device::Status status)
{
    Device::Status oldStatus = getConnectStatus();
    Sampler::setConnectStatus(status);
    if (oldStatus != status)
    {
        if (Device::isStatusChanged(oldStatus, status))
        {
            Trace::writeFormatLine("Dual Ports Ram connection state changes from %s to %s", Device::getStatusStr(oldStatus).c_str(), Device::getStatusStr(status).c_str());
        }
    }
}

InstructionDescription* DualPortsRamSampler::sampleInstruction()
{
    if (_sampleCallback != NULL)
        return _sampleCallback();

    return NULL;
}

void GathererService::createDualPortsRamDevice(const BaseCommService::InstructionCallback &callback, const int portIndex)
{
    DriverManager* dm = manager();
    assert(dm);

    DualPortsRamInstructionSet* set = new DualPortsRamInstructionSet(this, callback.tcpInstructions);
    DualPortsRamChannelContext * scc = new DualPortsRamChannelContext(portIndex);
    DualPortsRamInteractive *si = new DualPortsRamInteractive(dm);
    ChannelDescription* cd = new ChannelDescription("DualPortsRam1", scc, si);
    Channel *ch = new Channel(dm, cd);
    DeviceDescription* dd = new DeviceDescription("DualPortsRamDevice", cd, set);

    dm->description()->addDevice(dd);
    dm->addPool(_instructionPool = new DualPortsRamSampler(dm, cd, dd, Client::Connection(), callback.tcpSampler));
    dm->open();
}

bool GathererService::isOnline()
{
    DriverManager* dm = manager();
    assert(dm);

    DualPortsRamSampler *pool = dynamic_cast<DualPortsRamSampler*>(_instructionPool);
    if (pool == NULL) return false;

    return pool->isOnline();
}