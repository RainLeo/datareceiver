#include "DataGatherer.h"
#include "common/system/LocalTime.h"
#include "SystemInfo.h"
#include "common/system/Application.h"

uint startTime = TickTimeout::GetCurrentTickCount();

ClientContext* HeartbeatInstruction::setValue(ClientContext* context)
{
    CDataGatherer *pDataGatherer = CDataGatherer::Instance();
    if (pDataGatherer != NULL)
    {
        // set time for the first time or after an specified interval
        const int timeSyncHourInterval = 1;
        uint deadTime = TickTimeout::GetDeadTickCount(startTime, timeSyncHourInterval * 3600 * CLOCKS_PER_SEC);

        HeartbeatContext *hbc = (HeartbeatContext *)context;
        if (hbc->peerAddr() != "127.0.0.1" && (pDataGatherer->IsStarting() || TickTimeout::IsTimeout(startTime, deadTime)))
        {
            DateTime &peerTime = hbc->outputData()->curtime;
#ifdef WIN32
            bool ret = LocalTime::setTime(peerTime);
#else
            bool ret = adjustdate(peerTime.year(), peerTime.month(), peerTime.day(), peerTime.hour(), peerTime.minute(), peerTime.second());
#endif
            startTime = TickTimeout::GetCurrentTickCount(); // reset startTime to current for next period
            Trace::writeFormatLine("Time synchronized to reporter %s (interval: %d hours).", ret ? "successfully" : "failed", timeSyncHourInterval);
        }

        pDataGatherer->Started();
    }
    return context;
}

ClientContext* TagConfigurationInstruction::setValue(ClientContext* context)
{
    return context;
}

ClientContext* ServerTagConfigurationInstruction::setValue(ClientContext* context)
{
    TagConfigurationContext *pContext = (TagConfigurationContext*)context;

    string clientAddr = pContext->peerAddr();
    Trace::writeFormatLine("Tag configuration received from reporter: %s.", clientAddr.c_str());

    TagConfigurationResp resp;
    const TagConfiguration &req = *pContext->inputData();
    if (req.iFetchInterval < 0)
    {
        resp.status = 0x1;
    }
    else
    {
        CDataGatherer *pDataGatherer = CDataGatherer::Instance();
        assert(pDataGatherer);
        pDataGatherer->StartFetch(req);
    }
    pContext->setOutputData(&resp);

    return context;
}

ClientContext* TagValueInstruction::setValue(ClientContext* context)
{
    return context;
}

ClientContext* ServerMaintainDownInstruction::setValue(ClientContext* context)
{
    MaintainDownContext *pContext = (MaintainDownContext*)context;
    const MaintainDownReq &maintainReq = *pContext->inputData();

    // Request maintenance information
    MaintainDownResp maintainResp;

    if (maintainReq.programId != PROGRAM_ID_GATHERER)
    {
        maintainResp.status = 1;
    }
    else
    {
        CDataGatherer *pDataGatherer = CDataGatherer::Instance();
        assert(pDataGatherer);
        pDataGatherer->RequestMaintain();

        maintainResp.status = 0;
    }

    pContext->setOutputData(&maintainResp);

    return context;
}

ClientContext* MaintainDownInstruction::setValue(ClientContext* context)
{
    return context;
}

ClientContext* ServerMaintainUpInstruction::setValue(ClientContext* context)
{
    return context;
}

ClientContext* MaintainUpInstruction::setValue(ClientContext* context)
{
    return context;
}

ClientContext* ServerDownloadLogInstruction::setValue(ClientContext* context)
{
    DownloadLogContext *pContext = (DownloadLogContext*)context;
    const DownloadLogReq &req = *pContext->inputData();

    DownloadLogResp resp;
    if (req.programId != PROGRAM_ID_GATHERER)
    {
        resp.status = 1; // incorrect Program ID
    }
    else
    {
        CDataGatherer *pDataGatherer = CDataGatherer::Instance();
        assert(pDataGatherer);

        string dateStr = Convert::getDateStr(req.startDate);
        string path = Path::combine(Application::instance()->rootPath(), "logs");
        string logFile = dateStr + ".log";
        string fullLogFile = Path::combine(path, logFile);
        if (!File::exists(fullLogFile))
        {
            resp.status = 5; // no log
        }
        else
        {
            resp.status = 0;
            pDataGatherer->sendLog(path, logFile);
        }
    }

    pContext->setOutputData(&resp);

    return context;
}

ClientContext* UploadLogInstruction::setValue(ClientContext* context)
{
    return context;
}
