#pragma once
#include "SourceBase.h"
#include <map>
class CSourceFile :
    public CSourceBase
{
public:
    CSourceFile();
    virtual ~CSourceFile();
    static CSourceBase * Create() { return new CSourceFile(); }

    void Init(TagConfiguration &tagConfiguration, CDeviceConfiguration *pDevConfig);
    void Fetch();

    void SetConfigFile(const TagConfiguration &tagConfiguration, CDeviceConfiguration *pDevConfig);
    void ReadDataFile();

private:
    string m_data_file_path;
    time_t m_last_update_time;
    EStatus m_status;

};

