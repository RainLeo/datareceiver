#include "SourceFile.h"
#include "DeviceFile.h"
#include "common/IO/File.h"
#include "common/IO/FileInfo.h"
#include "DataGatherer.h"

#define sprintf_s(a,b,c,d) sprintf(a,c,d)

CSourceFile::CSourceFile() :
    m_last_update_time(0)
{
}

CSourceFile::~CSourceFile()
{
}

void CSourceFile::Init(TagConfiguration &tagConfiguration, CDeviceConfiguration *pDevConfig)
{
    CSourceBase::Init(tagConfiguration, pDevConfig);

    SetConfigFile(tagConfiguration, pDevConfig);
}

void CSourceFile::Fetch()
{
    ReadDataFile();
}

void CSourceFile::SetConfigFile(const TagConfiguration &tagConfiguration, CDeviceConfiguration *pDevConfig)
{
    CDeviceFileDetail *pDevFile = dynamic_cast<CDeviceFileDetail *>(pDevConfig->pDetail);
    m_data_file_path = pDevFile->strDataFilePath;
    // create configuration file
    FILE* file;
    #if WIN32
    int i = fopen_s(&file, pDevFile->strConfigFilePath.c_str(), "wb");
    #else
    file = fopen(pDevFile->strConfigFilePath.c_str(), "wb");
    int i = file == NULL ? 1 : 0;
    #endif
    char temp[64];
    if (i)
    {
        Trace::writeFormatLine("Could not open config file to write. '%s'", pDevFile->strConfigFilePath.c_str());
        return;
    }

    fwrite("1\r\n", 3, 1, file);
    sprintf_s(temp, sizeof(temp), "%d", tagConfiguration.iFetchInterval);
    fwrite(temp, strlen(temp), 1, file);
    fwrite("\r\n", 2, 1, file);
    const vector<DeviceTag> tags = pDevConfig->vecTags;
    for (vector<DeviceTag>::const_iterator ci = tags.begin(); ci != tags.end(); ++ci)
    {
        const char* tag = ci->strRegisterName.c_str();
        const char* database_tag = ci->strName.c_str();
        TagValue *pTagValue = new TagValue(TagValue::Null, ci->tagId, database_tag);
        if (!UpdateTag(tag, pTagValue))
        {
            delete pTagValue;
            pTagValue = NULL;
        }
        fwrite(tag, strlen(tag), 1, file);
        fwrite(",0\r\n", 4, 1, file);
    }
    fclose(file);
}

void CSourceFile::ReadDataFile()
{
    static int s_lastDataLen = 0; // local static variable in case of incomplete data file
    try
    {
        FileInfo fi = FileInfo(m_data_file_path);
        if (m_last_update_time == fi.modifiedTime() || fi.modifiedTime() == 0) // file not updated or existed
        {
            return;
        }

        bool if_should_send = m_last_update_time != 0;
        m_last_update_time = fi.modifiedTime();
        if (!if_should_send)				//first time, doesn't send
        {
            Trace::writeLine("First time to read the data file.");
            return;
        }
        Trace::writeLine("Fetching data: Reading the data file...");
        //parse file and send message
        //parse file
        if (!File::exists(m_data_file_path))
        {
            throw FileNotFoundException(m_data_file_path.c_str());
        }
        FileStream fs = FileStream(m_data_file_path, FileMode::FileOpen, FileAccess::FileRead);
        ByteArray buffer;
        if (!fs.readToEnd(buffer))
        {
            Trace::writeLine("Error: failed to read the data file...");
            return;
        }
        fs.close();
        //status
        char * data = (char *)buffer.data();
        int status = atoi(data);
        data = strchr(data, '\n');
        if (!data)
        {
            Trace::writeLine("Error: no status.");
            return;
        }
        Trace::writeFormatLine("OPC Server status: %d", status);
        data++;
        //time
        data = strchr(data, '\n');
        if (!data)
        {
            Trace::writeLine("Error: no time.");
            return;
        }
        data++;
        //tag count
        int tagCount = atoi(data);
        data = strchr(data, '\n');
        if (!data)
        {
            Trace::writeLine("Error: no tag count.");
            return;
        }
        Trace::writeFormatLine("OPC Server tag count: %d", tagCount);
        data++;
        //length
        int dataLen = atoi(data);
        data = strchr(data, '\n');
        if (!data)
        {
            Trace::writeLine("Error: no length.");
            return;
        }
        data++;
        char * pDataStartPos = data;
        int actualDataLen = (int)buffer.count() - (data - (char *)buffer.data());
        if (actualDataLen != dataLen)
        {
            Trace::writeFormatLine("Warning: OPC server data length: %d is NOT equal to actual data len = %d, actual file length = %d.", dataLen, actualDataLen, buffer.count());
            // bail out for next try
            if (actualDataLen < dataLen && actualDataLen > s_lastDataLen)
            {
                Trace::writeFormatLine("Incomplete data file: ready for next try. Actual data length = %d, last length = %d.", actualDataLen, s_lastDataLen);
                s_lastDataLen = actualDataLen;
                m_last_update_time--;
            }
            return;
        }
        s_lastDataLen = 0; // reset last data length
        //tags
        unsigned short line_number = 0;
        while (line_number < tagCount)
        {
            char * line_end = strchr(data, '\n');
            if (!line_end)
            {
                Trace::writeLine("Error: Bad file format!");
                return;
            }
            if (line_end - pDataStartPos > dataLen)
            {
                Trace::writeFormatLine("Error: Inconsistent data length found! current data length: %d > %d in file header.", (int)(line_end - pDataStartPos), dataLen);
                return;
            }
            if ((uint)(line_end - (char *)buffer.data()) > buffer.count())
            {
                Trace::writeFormatLine("Error: reading data after end of file! current file position: %d > file length: %d.", (int)(line_end - (char *)buffer.data()), buffer.count());
                return;
            }
            string line(data, line_end);
            StringArray items;
            Convert::splitStr(line, ',', items);
            if (items.count() != 5)
            {
                Trace::writeFormatLine("Error: Parse file interrupted! Tag line format incorrect: %s", line.c_str());
                break;
            }

            data = line_end;
            data++;
            line_number++;

            string tag = items[0];
            TagValue* pOldTagValue = GetTag(tag);
            if (pOldTagValue != NULL)
            {
                int type;
                int quality;
                TagType tagType;
                //time_t timeStamp;
                DateTime timeStamp;

                Convert::parseInt32(items[1], type);
                //timeStamp = Convert::parseDateTime(items[3]);
                DateTime::parse(items[3], timeStamp);
                Convert::parseInt32(items[4], quality);

                switch ((EData)type)
                {
                case ED_None:
                    tagType = TagType::Null;
                    break;
                case ED_Boolean:
                    tagType = TagType::Digital;
                    break;
                case ED_Short:
                    tagType = TagType::Integer16;
                    break;
                case ED_Long:
                    tagType = TagType::Integer32;
                    break;
                case ED_Float:
                case ED_Double:
                default: // always try float for unknown types
                    tagType = TagType::Float32;
                    break;
                case ED_String:
                    tagType = TagType::Text;
                    //break;
                    Trace::writeFormatLine("Warning: Tag type string(%d) is not supported. Tag #%d: %s", type, line_number, line.c_str());
                    continue;
                case ED_Byte:
                case ED_Character:
                case ED_Word:
                case ED_Dword:
                    tagType = TagType::Integer32;
                    break;
                //default:
                //    Trace::writeFormatLine("Tag value type incorrect: %d('%s')", type, items[1].c_str());
                //    return;
                }

                TagValue *pTagValue = new TagValue(tagType, pOldTagValue->id(), pOldTagValue->name());
                float value;
                if (type == ED_Character)
                {
                    char c = '\0';
                    sscanf(items[2].c_str(), "%c", &c);
                    value = c;
                }
                else if (!Convert::parseSingle(items[2], value))
                {
                    Trace::writeFormatLine("Error: Tag value format incorrect: %s, type: %s", items[2].c_str(), items[1].c_str());
                    delete pTagValue;
                    continue; // go to next tag instead of return, modified on 2015/09/26
                }
                pTagValue->setValue(value, (rtdb::Tag::Qualitystamp)quality, timeStamp.toUtcTime()); // TimeStamp set to the DateTime in file

                if (!UpdateTag(tag, pTagValue))
                {
                    delete pTagValue;
                    pTagValue = NULL;
                }
            }
            else
            {
                Trace::writeFormatLine("Tag ignored: %s is NOT configured to read.", tag.c_str());
            }
        }

        CDataGatherer *pGatherer = CDataGatherer::Instance();

        switch (status)
        {
        case 1:
            m_status = ES_Normal;
            pGatherer->SetOpcStatus(status);
            pGatherer->SetRunningStatus(0);
            Trace::writeLine("OPC server status 1: normal with all tags.");
            break;
        case 0:
            m_status = ES_PartialNormal;
            pGatherer->SetOpcStatus(status);
            pGatherer->SetRunningStatus(1);
            Trace::writeLine("OPC server status 0: normal with part of tags.");
            break;
        case -1:
            m_status = ES_Unconnected;
            pGatherer->SetOpcStatus(status);
            pGatherer->SetRunningStatus(0);
            Trace::writeLine("OPC server status -1: cannot connect to OPC server correctly.");
            break;
        case -2:
            m_status = ES_Unconnected;
            pGatherer->SetOpcStatus(status);
            pGatherer->SetRunningStatus(2);
            Trace::writeLine("OPC server status -2: no tag need to read.");
            break;
        case -3:
            m_status = ES_Unconnected;
            pGatherer->SetOpcStatus(status);
            pGatherer->SetRunningStatus(2);
            Trace::writeLine("OPC server status -3: OPC network disconnected.");
            break;
        default:
            m_status = ES_Unknown;
            pGatherer->SetOpcStatus(status);
            pGatherer->SetRunningStatus(0);
            Trace::writeFormatLine("OPC server status %d: unknown status.", m_status);
            break;
        }
    }
    catch (Exception ex)
    {
        Trace::writeFormatLine("Error in parsing file '%s'. Additional information: %s", m_data_file_path.c_str(), ex.getMessage());
    }
}
