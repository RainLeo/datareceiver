#pragma once
#include "SourceModBus.h"
class CSourceDlt645 :
    public CSourceModBus
{
public:
    CSourceDlt645();
    virtual ~CSourceDlt645();
    static CSourceBase * Create() { return new CSourceDlt645(); }

    virtual string GetChannelName();
    virtual string GetRegisterName(byte function);
    virtual void InitDeviceInfo(XmlTextWriter &xw, CDeviceConfiguration *pDevConfig, const string address);
    virtual void InitTagAddr(XmlTextWriter &xw, const DeviceTag *pDeviceTag);
    virtual void InitTagProperty(XmlTextWriter &xw, const DeviceTag *pDeviceTag);
};

