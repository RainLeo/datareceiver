#ifndef BASESERVICE_H
#define BASESERVICE_H

#include <string>
#include <mutex>
#include "common/common_global.h"
#include "common/data/Vector.h"
#include "common/data/Dictionary.h"
#include "common/thread/Thread.h"
#include "common/xml/ConfigFile.h"
#include "common/data/PrimitiveType.h"
#include "common/driver/instructions/Instruction.h"

using namespace Common;
using namespace Driver;

namespace EVCommon
{
    class ProjectService;
    class COMMON_EXPORT BaseService
    {
    public:
        BaseService();
        virtual ~BaseService();
        
        virtual bool initialize(const ConfigFile* file = NULL) = 0;
        virtual bool unInitialize() = 0;
        virtual string name() const = 0;
        
        virtual void active();
        virtual void inactive();
        
        virtual void generateInstructions(Instructions* instructions);
        
    public:
        const Vector<BaseService>& services() const;
        void addService(const BaseService* service);
        void addService(const ProjectService* service);
        const ProjectService* projectService() const;
        
        template<class T>
        T* getService() const
        {
            for (uint j=0; j<_services.count(); j++)
            {
                T* service = dynamic_cast<T*>(_services.at(j));
                if(service != NULL)
                {
                    return service;
                }
            }
            return NULL;
        }
        
    protected:
        ProjectService* _projectService;
        Vector<BaseService> _services;
        mutex _servicesMutex;
    };
    
    typedef Vector<BaseService> Services;
    typedef Dictionary<String, Services*> NamedServices;
}

#endif	// BASESERVICE_H
