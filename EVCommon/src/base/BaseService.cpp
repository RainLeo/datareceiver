#include "BaseService.h"
#include "common/thread/Locker.h"
#include "project/ProjectService.h"

namespace EVCommon
{
    BaseService::BaseService()
    {
        _services.setAutoDelete(false);
        _projectService = NULL;
    }
    BaseService::~BaseService()
    {
        _projectService = NULL;
    }
    void BaseService::addService(const BaseService* service)
    {
        Locker locker(&_servicesMutex);
        _services.add(service);
    }
    void BaseService::addService(const ProjectService* service)
    {
        _projectService = (ProjectService*)service;
    }
    const ProjectService* BaseService::projectService() const
    {
        return _projectService;
    }
    const Vector<BaseService>& BaseService::services() const
    {
        return _services;
    }
    
    void BaseService::active()
    {
    }
    void BaseService::inactive()
    {
    }
    
    void BaseService::generateInstructions(Instructions* instructions)
    {
    }
}
