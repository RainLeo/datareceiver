//
//  DeviceInfo.h
//  EVCommon
//
//  Created by baowei on 15/7/20.
//  Copyright (c) 2015 com. All rights reserved.
//

#ifndef __EVCommon__DeviceInfo__
#define __EVCommon__DeviceInfo__

#include <string>
#include "common/common_global.h"
#include "common/IO/Stream.h"
#include "common/xml/XmlTextReader.h"
#include "common/xml/XmlTextWriter.h"
#include "DriverTag.h"

using namespace std;
using namespace Common;

namespace Driver
{
    class Mock
    {
    public:
        string registerStr;
        ByteArray send;
        ByteArray recv;
        bool enable;
        Mock()
        {
            registerStr = "";
            enable = false;
        }
    };
    typedef Vector<Mock> Mocks;
    
    class DeviceInfo
    {
    public:
        string name;
        int address;
        string channelName;
        string type;
        string property;
        bool batchread;
        bool batchwrite;
        bool enable;
        Mocks mocks;

        DeviceInfo();
        ~DeviceInfo();
        
    public:
        void read(XmlTextReader& reader);
        void write(XmlTextWriter& reader)
        {
            // todo: write config file.
        }
        
        DriverTags* tags() const;
        
        bool hasMock() const;
        
    private:
        void addTag(const DriverTag* tag);
        
    private:
        DriverTags _tags;
    };
    
    class COMMON_EXPORT DeviceInfos : public Vector<DeviceInfo>
    {
    public:
        DeviceInfos(bool autoDelete = true, uint capacity = Vector<DeviceInfo>::DefaultCapacity) : Vector<DeviceInfo>(autoDelete, capacity)
        {
        }
    };
}

#endif /* defined(__EVCommon__DeviceInfo__) */
