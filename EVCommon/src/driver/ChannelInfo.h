//
//  ChannelInfo.h
//  EVCommon
//
//  Created by baowei on 15/7/20.
//  Copyright (c) 2015 com. All rights reserved.
//

#ifndef __EVCommon__ChannelInfo__
#define __EVCommon__ChannelInfo__

#include <string>
#include "common/common_global.h"
#include "common/xml/XmlTextReader.h"
#include "common/IO/SerialInfo.h"
#include "DriverTag.h"

using namespace std;
using namespace Common;

namespace Driver
{
    class ChannelInfo
    {
    public:
        struct Timeout
        {
        public:
            TimeSpan send;
            TimeSpan receive;
            TimeSpan open;
            TimeSpan close;
            
            Timeout() : send(0, 0, 3), receive(0, 0, 3), open(0, 0, 3), close(0, 0, 3)
            {
            }
        };
        struct TcpInfo
        {
            string address;
            int port;
            
            int sendBufferSize;
            int receiveBufferSize;
            bool noDelay;
            Timeout timeout;
            
            TcpInfo()
            {
                address = "";
                port = 0;
                sendBufferSize = 0; // default size
                sendBufferSize = 0; // default size
                noDelay = false;
            }
            void read(XmlTextReader& reader);
        };
        struct DtuInfo
        {
            string vendor;
            string id;
            DtuInfo()
            {
                vendor = "";
                id = "";
            }
        };
        struct LinkInfo
        {
            TcpInfo tcp;
            SerialInfo serial;
            DtuInfo dtu;
        };
        
        enum LinkType
        {
            LinkNone = 0,
            LinkTcp = 1,
            LinkSerial = 2,
            LinkDtu = 3,
        };
        
    public:
        LinkInfo link;
        LinkType linkType;
        string name;
        bool reopened;
        bool enable;
        
        ChannelInfo()
        {
            linkType = LinkNone;
            name = "";
            reopened = true;
            enable = true;
        }
        
        bool isValid() const;
        
        void read(XmlTextReader& reader);
        
        string toInteractiveName() const;
        
        static LinkType parseLinkType(const string& value);
        static string convertLinkTypeStr(LinkType value);
    };
    
    class COMMON_EXPORT ChannelInfos : public Vector<ChannelInfo>
    {
    public:
        ChannelInfos(bool autoDelete = true, uint capacity = Vector<ChannelInfo>::DefaultCapacity) : Vector<ChannelInfo>(autoDelete, capacity)
        {
        }
    };
}

#endif /* defined(__EVCommon__ChannelInfo__) */
