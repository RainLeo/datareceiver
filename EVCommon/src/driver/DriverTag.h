//
//  DriverTag.h
//  EVCommon
//
//  Created by baowei on 15/7/20.
//  Copyright (c) 2015 com. All rights reserved.
//

#ifndef __EVCommon__DriverTag__
#define __EVCommon__DriverTag__

#include <string>
#include <mutex>
#include "common/common_global.h"
#include "common/system/DateTime.h"
#include "common/data/Vector.h"
#include "common/system/Delegate.h"
#include "common/system/Convert.h"
#include "common/thread/Locker.h"
#include "common/xml/XmlTextReader.h"
#include "tag/Tag.h"

using namespace std;
using namespace Common;
using namespace rtdb;

namespace Driver
{
    class DeviceInfo;
    class DriverData;
    class DriverService;
	class DriverTag
    {
    public:
        enum TransformType
        {
            TransformNone = 0,
            TransformLinear,
            TransformRoot
        };
        struct Transform
        {
        public:
            TransformType type;
            union
            {
                struct
                {
                    double minValue;
                    double maxValue;
                    double minDeviceValue;
                    double maxDeviceValue;
                }linear;
            }config;
            
            Transform();
            
            bool needTransform() const;
            
            // from device value to tag value.
            bool transferDValue(const double& dValue, double& tValue) const;
            // from tag value to device value.
            bool transferTValue(const double& tValue, double& dValue) const;
            
        public:
            static bool parse(const string& str, Transform& trans);
        };
        enum Access
        {
            AccessNone = 0,
            AccessRead = 1,
            AccessWrite = 2,
            AccessReadWrite = 3,
        };
        
        DriverTag(const string& name = "", uint id = 0, Tag::Type type = Tag::Type::Null);
        
        uint id() const;
        bool isValid() const;
       
        Tag::Type type() const;
        
        bool validLinkedId() const;
        uint linkedId() const;
        
        const string& name() const;
        const string& registerStr() const;
        int address() const;
        int length() const;
        TimeSpan interval() const;
        bool bigEndian() const;
        string property() const;
        
        bool canRead() const;
        bool canWrite() const;
        
        bool setValue(const bool& v);
        bool setValue(const byte& v);
        bool setValue(const short& v);
        bool setValue(const int& v);
        bool setValue(const float& v);
        bool setValue(const int64_t& v);
        bool setValue(const double& v);
        
        bool setValue(const bool& v, Tag::Qualitystamp qualitystamp, DateTime timestamp);
        bool setValue(const byte& v, Tag::Qualitystamp qualitystamp, DateTime timestamp);
        bool setValue(const short& v, Tag::Qualitystamp qualitystamp, DateTime timestamp);
        bool setValue(const int& v, Tag::Qualitystamp qualitystamp, DateTime timestamp);
        bool setValue(const float& v, Tag::Qualitystamp qualitystamp, DateTime timestamp);
        bool setValue(const int64_t& v, Tag::Qualitystamp qualitystamp, DateTime timestamp);
        bool setValue(const double& v, Tag::Qualitystamp qualitystamp, DateTime timestamp);
        
        bool getValue(byte& v) const;
        bool getValue(short& v) const;
        bool getValue(int& v) const;
        bool getValue(float& v) const;
        bool getValue(int64_t& v) const;
        bool getValue(double& v) const;
        
        DateTime timestamp() const;
        Tag::Qualitystamp qualitystamp() const;
        
        Delegates* valueChangedDelegates();
        
    private:
        DriverService* service() const;
        
        void read(XmlTextReader& reader);
        
        void setDbTag(Tag* tag);
        bool setDbValue(const Tag::Type type, const Tag::Value& v);
        
        bool compareValue(const bool& v);
        bool compareValue(const byte& v);
        bool compareValue(const short& v);
        bool compareValue(const int& v);
        bool compareValue(const float& v);
        bool compareValue(const int64_t& v);
        bool compareValue(const double& v);
        
        bool setValueInner(const Tag::Value& v);
        bool setValueInner(const Tag::Value& v, Tag::Qualitystamp qualitystamp, DateTime timestamp);
        
        void valueChanged();
        
        DateTime now() const;
        
        friend void driver_tagValueChanged(void* owner, void* sender, EventArgs* args);
        
    private:
        static string toAccessStr(Access value);
        static Access fromAccessStr(const string& value);
        
    private:
        friend DeviceInfo;
        friend DriverService;
        
        string _name;
        uint _id;
        Tag::Type _type;
        
        string _registerStr;
        int _address;
        int _length;
        TimeSpan _interval;
        bool _bigEndian;
        string _property;
        
        Tag::Value _readValue;
        bool _initValue;
        Tag::Value _writeValue;
        Transform _transform;
        
        DateTime _timestamp;
        Tag::Qualitystamp _qualitystamp;
        
        Access _access;
        
        uint _linkedId;
        
        DriverService* _service;
        DriverData* _data;
        
        Tag* _tag;
        
        Delegates _valueChangedDelegates;
    };
    
	class COMMON_EXPORT DriverTags : public Vector<DriverTag>
    {
    public:
		DriverTags(bool autoDelete = true, uint capacity = Vector<DriverTag>::DefaultCapacity) : Vector<DriverTag>(autoDelete, capacity)
        {
        }
        
        DriverTag* getTag(int address) const;
    };
}

#endif /* defined(__EVCommon__DriverTag__) */
