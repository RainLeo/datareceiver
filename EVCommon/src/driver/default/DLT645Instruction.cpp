//
//  DLT645Instruction.cpp
//  EVCommon
//
//  Created by baowei on 15/9/1.
//  Copyright (c) 2015 com. All rights reserved.
//

#include "DLT645Instruction.h"
#include "common/diag/Debug.h"
#include "common/system/CheckUtilities.h"
#include "common/driver/devices/Device.h"
#include "common/driver/channels/Channel.h"
#include "common/data/BCDUtilities.h"

namespace Driver
{
    namespace DLT645
    {
        DLT645DeviceContext::DLT645DeviceContext()
        {
            preambleCount = MinPreambleCount;
            broadcast = false;
            memset(address, EmptyAddress, sizeof(address));
        }
        DeviceContext* DLT645DeviceContext::clone() const
        {
            DLT645DeviceContext* context = new DLT645DeviceContext();
            context->copyFrom(this);
            context->preambleCount = this->preambleCount;
            context->broadcast = this->broadcast;
            memcpy(context->address, this->address, sizeof(address));
            return context;
        }
        void DLT645DeviceContext::updateProperty(const Convert::KeyPairs& properties)
        {
            for (uint i=0; i<properties.count(); i++)
            {
                const Convert::KeyPair* kp = properties[i];
                if(String::stringEquals(kp->name, "broadcast", true))
                {
                    Convert::parseBoolean(kp->value, broadcast);
                }
                else if(String::stringEquals(kp->name, "preamblecount", true))
                {
                    int count;
                    if(Convert::parseInt32(kp->value, count) &&
                       count >= MinPreambleCount && count <= MaxPreambleCount)
                    {
                        preambleCount = count;
                    }
                }
                else if(String::stringEquals(kp->name, "address", true))
                {
                    string addressStr = kp->value;
                    for (uint i=0;i<addressStr.length();i+=2)
                    {
                        byte hi = addressStr[i];
                        byte lo = addressStr[i+1];
                        int pos = sizeof(address) - i/2 - 1;
                        if(pos >= 0 && pos < (int)sizeof(address))
                        {
                            address[pos] = ((hi << 4) & 0xF0) + (lo & 0x0F);
                        }
                    }
                }
            }
            
            if(broadcast)
            {
                for (uint i=0; i<sizeof(address); i++)
                {
                    address[i] = BroadcastAddress;
                }
            }
        }
        bool DLT645DeviceContext::isCorrectAddress(const byte* address, int offset) const
        {
            for (uint i=0; i<AddressCount; i++)
            {
                if(this->address[i] != address[offset + i])
                    return false;
            }
            return true;
        }
        
        DLT645Context::DLT645Context() : DriverInstructionContext()
        {
        }
        
        ReadContext::ReadContext() : DLT645Context()
        {
        }
        
        DLT645Instruction::DLT645Instruction(InstructionDescription* id) : DriverInstruction(id)
        {
        }
        InstructionContext* DLT645Instruction::execute(Interactive* interactive, Device* device, InstructionContext* context, const ByteArray* buffer)
        {
            DriverInstructionContext* dc = dynamic_cast<DriverInstructionContext*>(context);
            assert(dc);
            
            send(interactive, device, dc);
            
            return receive(interactive, device, dc);
        }
        bool DLT645Instruction::getSendBuffer(ByteArray& buffer, DeviceDescription* dd, DriverInstructionContext* context)
        {
            const DriverTags& tags = context->tags();
            if(tags.count() == 0)
                return false;
            
            DLT645DeviceContext* dc = dynamic_cast<DLT645DeviceContext*>(dd->context());
            assert(dc);
            
            MemoryStream ms;
            
            // preambles
            if(dc->preambleCount > 0)
            {
                byte preambles[DLT645DeviceContext::MaxPreambleCount];
                memset(preambles, PreambleValue, sizeof(preambles));
                ms.write(preambles, 0, dc->preambleCount);
            }
            
            // header
            int64_t delimiterPos = ms.position();
            ms.writeByte(Header);
            
            // address
            ms.write(dc->address, 0, sizeof(dc->address));
            
            // header
            ms.writeByte(Header);
            
            // control code
            byte code = controlCode(context) | DLT645DeviceContext::MasterToSlave;
            ms.writeByte(code);
            
            // data
            MemoryStream tempStream;
            if (!getCommandBuffer(tempStream, context))
            {
                return false;
            }
            byte length = (byte)tempStream.length();
            ms.writeByte(length);   // length
            const ByteArray* commandBuffer = tempStream.buffer();
            ByteArray newCommandBuffer;
            for (uint i=0; i<length; i++)
            {
                newCommandBuffer.add(commandBuffer->at(i) + Transfer);
            }
            ms.write(newCommandBuffer.data(), 0, length);
            
            // check
            const ByteArray* checkBuffer = ms.buffer();
            byte check = SumUtilities::checkByBit(checkBuffer->data(), (int)delimiterPos, checkBuffer->count()-(int)delimiterPos);
            ms.writeByte(check);
            
            // tail
            ms.writeByte(Tail);
            
            ms.copyTo(buffer);
            
            return true;
        }
        DriverInstructionContext* DLT645Instruction::setReceiveBuffer(const ByteArray& buffer, DeviceDescription* dd, DriverInstructionContext* context)
        {
            MemoryStream ms(&buffer);
            ms.seek(ControlCodePosition, SeekOrigin::SeekBegin);
            byte controlCode = ms.readByte();
            if((controlCode & DLT645DeviceContext::SlaveException) == 0)
            {
                ms.readByte();  // skip length
                return setCommandBuffer(ms, context);
            }
            else
            {
                // exception
                ms.readByte();  // skip length
                byte error = ms.readByte();
                Trace::writeFormatLine("DLT645 received error: %x", (int)error);
            }
            
            return context;
        }
        bool DLT645Instruction::match(const ByteArray& buffer, DeviceDescription* dd)
        {
            DLT645DeviceContext* dc = dynamic_cast<DLT645DeviceContext*>(dd->context());
            assert(dc);
            
            if(buffer.count() <= MinFrameLen)
                return false;
            
            int nPreambleOffset = 0;
            while (buffer[nPreambleOffset] == PreambleValue && nPreambleOffset < DLT645DeviceContext::MaxPreambleCount)
            {
                nPreambleOffset++;
            }
            // header
            if (buffer[HeaderPosition + nPreambleOffset] != Header)
                return false;
            if (!dc->isCorrectAddress(buffer.data(), AddressPosition + nPreambleOffset))
                return false;               
            if (buffer[AddressPosition + nPreambleOffset + DLT645DeviceContext::AddressCount] != Header)
                return false;
            
            // control code
            byte controlCode = buffer[ControlCodePosition + nPreambleOffset];
            if((controlCode & DLT645DeviceContext::SlaveToMaster) == 0)
                return false;
            if((controlCode & DLT645DeviceContext::SlaveException) == 0)
            {
                if (buffer[LengthPosition + nPreambleOffset] < 2)
                    return false;
            }
            else
            {
                // exception
                if (buffer[LengthPosition + nPreambleOffset] != 1)
                    return false;
            }

            // check
            byte check = buffer[buffer.count()-2];
            byte calcCheck = SumUtilities::checkByBit(buffer.data(), 0, buffer.count()-2);
            if(calcCheck != check)
                return false;
            
            if(buffer[buffer.count()-1] != Tail)
                return false;
            return true;
        }
        bool DLT645Instruction::parseRegisterStr(const string& registerStr, ushort& value) const
        {
            if(registerStr.length() == 4)
            {
                ushort temp;
                if(Convert::parseUInt16(registerStr, temp, false))
                {
                    //byte buffer[2];
                    //BCDUtilities::UInt16ToBCD(temp, buffer);
                    //value = ((buffer[1] << 8) & 0xFF00) + (buffer[0]);
                    value = temp; //((temp << 8) & 0xFF00) + ((temp >> 8) & 0x00FF);;
                    return true;
                }
            }
            return false;
        }
        bool DLT645Instruction::parseRegisterStr(const string& registerStr, uint& value) const
        {
            if(registerStr.length() == 8)
            {
                uint temp;
                if(Convert::parseUInt32(registerStr, temp, false))
                {
                    //byte buffer[4];
                    //BCDUtilities::UInt16ToBCD(temp, buffer);
                    //value = ((buffer[3] << 24) & 0xFF000000) + ((buffer[2] << 16) & 0xFF0000) +
                    //    ((buffer[1] << 8) & 0xFF00) + (buffer[0]);
                    value = temp; // ((temp << 24) & 0xFF000000) + ((temp << 8) & 0xFF0000) + ((temp >> 8) & 0xFF00) + ((temp >> 24) & 0x00FF);
                    return true;
                }
            }
            return false;
        }
        
        ReadInstruction::ReadInstruction(InstructionDescription* id) : DLT645Instruction(id)
        {
        }
        bool ReadInstruction::getCommandBuffer(MemoryStream& ms, DriverInstructionContext* context)
        {
            const DriverTags& tags = context->tags();
            if(tags.count() != 1)
                return false;
            
            const DriverTag* tag = tags[0];
            assert(tag);
            const string& registerStr = tag->registerStr();
            if(registerStr.length() == 4)
            {
                ushort value;
                if(parseRegisterStr(registerStr, value))
                {
                    ms.writeUInt16(value, false);
                    return true;
                }
            }
            else if(registerStr.length() == 8)
            {
                uint value;
                if(parseRegisterStr(registerStr, value))
                {
                    ms.writeUInt32(value, false);
                    return true;
                }
            }
            
            return false;
        }
        DriverInstructionContext* ReadInstruction::setCommandBuffer(MemoryStream& ms, DriverInstructionContext* context)
        {
            const DriverTags& tags = context->tags();
            if(tags.count() == 0)
                return NULL;
            
            // Get length of data field
            int64_t pos = ms.position();
            ms.seek(pos - 1);
            byte dataLen = ms.readByte(); // including data ID, data, etc.

            uint command = 0xFFFFFFFF;
            DriverTag* tag = tags[0];
            const string& registerStr = tag->registerStr();
            if(registerStr.length() == 4)
            {
                byte di0 = ms.readByte() - Transfer;
                byte di1 = ms.readByte() - Transfer;
                command = ((di1 << 8) & 0xFF00) + (di0 & 0x00FF);
                dataLen -= 2;
                ushort value;
                if (parseRegisterStr(registerStr, value))
                {
                    if (command != value)
                    {
                        Trace::writeFormatLine("DLT645 received DI(%x) NOT match: %x. Debug info: tags count = %d", command, value, tags.count());
                        return NULL;
                    }
                }
            }
            else if(registerStr.length() == 8)
            {
                byte di0 = ms.readByte() - Transfer;
                byte di1 = ms.readByte() - Transfer;
                byte di2 = ms.readByte() - Transfer;
                byte di3 = ms.readByte() - Transfer;
                command = ((di3 << 24) & 0xFF000000) + ((di2 << 16) & 0xFF0000) + ((di1 << 8) & 0xFF00) + (di0 & 0x00FF);
                dataLen -= 4;
                uint value;
                if (parseRegisterStr(registerStr, value))
                {
                    if (command != value)
                    {
                        Trace::writeFormatLine("DLT645 received DI(%x) NOT match: %x", command, value);
                        return NULL;
                    }
                }
            }

            // Reverse the data buffer
            byte *pDataBuffer = new byte[dataLen];
            ms.read(pDataBuffer, 0, dataLen);
            for (int i = 0; i < dataLen / 2; i++)
            {
                byte tmp = pDataBuffer[i];
                pDataBuffer[i] = pDataBuffer[dataLen - i - 1];
                pDataBuffer[dataLen - i - 1] = tmp;
            }
            // write back and keep the same position
            pos = ms.position();
            ms.seek(pos - dataLen);
            ms.write(pDataBuffer, 0, dataLen);
            ms.seek(pos - dataLen);
            delete[] pDataBuffer;

            if((command >= 0x9010 && command <= 0x996F) ||       // XXXXXX.XX
                (registerStr.length() == 8 && command <= 0x00C2000C))   // command >= 0x00000000
            {
                byte buffer[3];
                ms.read(buffer, 0, sizeof(buffer));
                for (uint i=0; i<sizeof(buffer); i++)
                {
                    buffer[i] -= Transfer;
                }
				float iValue = (float)BCDUtilities::BCDToInt64(buffer, 0, sizeof(buffer));
				float pValue = (float)BCDUtilities::BCDToByte(ms.readByte() - Transfer) / 100.f;
                float value = iValue + pValue;
                tag->setValue(value);
            }
            else if((command >= 0xA010 && command <= 0xA96F) || // XX.XXXX
                    (command >= 0xB630 && command <= 0xB633))
            {
                float iValue = (float)BCDUtilities::BCDToByte(ms.readByte() - Transfer);
                byte buffer[2];
                ms.read(buffer, 0, sizeof(buffer));
                for (uint i=0; i<sizeof(buffer); i++)
                {
                    buffer[i] -= Transfer;
                }
                float pValue = (float)BCDUtilities::BCDToInt64(buffer, 0, sizeof(buffer)) / 10000.f;
                float value = iValue + pValue;
                tag->setValue(value);
            }
            else if((command >= 0xB650 && command <= 0xB653) || // X.XXX
                    (command >= 0x02060000 && command <= 0x02060300) ||
                    (command == 0x0206FF00))
            {
                float iValue = (float)BCDUtilities::BCDToByte(ms.readByte() - Transfer) / 10.f;
                float pValue = (float)BCDUtilities::BCDToByte(ms.readByte() - Transfer) / 1000.f;
                float value = iValue + pValue;
                tag->setValue(value);
            }
            else if((command >= 0xB634 && command <= 0xB643) || // XX.XX
                    (command >= 0xB621 && command <= 0xB623))
            {
                float iValue = (float)BCDUtilities::BCDToByte(ms.readByte() - Transfer);
                float pValue = (float)BCDUtilities::BCDToByte(ms.readByte() - Transfer) / 100.f;
                float value = iValue + pValue;
                tag->setValue(value);
            }
            else if(command >= 0xB611 && command <= 0xB613)     // XXX
            {
                byte buffer[2];
                ms.read(buffer, 0, sizeof(buffer));
                for (uint i=0; i<sizeof(buffer); i++)
                {
                    buffer[i] -= Transfer;
                }
                int value = (int)(BCDUtilities::BCDToInt64(buffer, 0, sizeof(buffer)));
                tag->setValue(value);
            }
            else if((command >= 0xB212 && command <= 0xB213) ||  // NNNN
                    (command >= 0xB310 && command <= 0xB313))
            {
                byte buffer[2];
                ms.read(buffer, 0, sizeof(buffer));
                for (uint i=0; i<sizeof(buffer); i++)
                {
                    buffer[i] -= Transfer;
                }
                int value = (int)BCDUtilities::BCDToInt64(buffer, 0, sizeof(buffer));
                tag->setValue(value);
            }
            else if((command >= 0xB214 && command <= 0xB214) ||  // NNNNNN
                    (command >= 0xB320 && command <= 0xB323))
            {
                byte buffer[3];
                ms.read(buffer, 0, sizeof(buffer));
                for (uint i=0; i<sizeof(buffer); i++)
                {
                    buffer[i] -= Transfer;
                }
                int value = (int)BCDUtilities::BCDToInt64(buffer, 0, sizeof(buffer));
                tag->setValue(value);
            }
            
            return context;
        }
        byte ReadInstruction::controlCode(DriverInstructionContext* context) const
        {
            const DriverTags& tags = context->tags();
            if(tags.count() != 1)
                return false;
            
            const DriverTag* tag = tags[0];
            assert(tag);
            const string& registerStr = tag->registerStr();
            if(registerStr.length() == 4)
            {
                return 0x01;
            }
            else if(registerStr.length() == 8)
            {
                return 0x11;
            }
            return 0;
        }
        
        DLT645MasterSet::DLT645MasterSet(const DeviceInfo* di) : DriverInstructionSet(di)
        {
        }
        bool DLT645MasterSet::receive(Device* device, Channel* channel, ByteArray* buffer)
        {
            if (NULL == channel)
            {
                return false;
            }
            if (!channel->connected())
            {
                return false;
            }
            
            uint timeout = device->description()->receiveTimeout();
            
            const size_t BufferLength = 1024;
            byte rbuffer[BufferLength];
            memset(rbuffer, 0, BufferLength);
            
            // header
            bool matchHeader = false;
            do
            {
                int rLength = channel->receiveBySize(rbuffer, BufferLength, 1, timeout);
                if (rLength == 1 && rbuffer[DLT645Instruction::HeaderPosition] == DLT645Instruction::Header)
                {
                    matchHeader = true;
                    break;
                }
                else if (rLength != 1)
                {
                    matchHeader = false;
                    break;
                }
            } while (!matchHeader);
            if (!matchHeader)
            {
                return false;
            }
            
            // address
            int offset = 1;
            int rLength = channel->receiveBySize(rbuffer, BufferLength-offset, offset, DLT645Instruction::HeaderLength2, timeout);
            if (rLength != DLT645Instruction::HeaderLength2)
            {
                return false;
            }
            offset += DLT645Instruction::HeaderLength2;
            byte byteCount = rbuffer[offset-1] + 2; // the last one is length, included check & tail.
            
            // data + check
            rLength = channel->receiveBySize(rbuffer, BufferLength-offset, offset, byteCount, timeout);
            if (rLength != byteCount)
            {
                return false;
            }
            buffer->addRange(rbuffer, offset+byteCount);
            return true;
        }
        void DLT645MasterSet::generateInstructions(Instructions* instructions)
        {
            instructions->add(new ReadInstruction(create("read")));
        }
        InstructionDescription* DLT645MasterSet::create(const string& name) const
        {
            if(String::stringEquals(name, "read", true))
                return new InstructionDescription("read", new ReadContext());
            else
            {
                if(name.length() == 4)
                {
                    if(name[0] == '9' || name[0] == 'A' ||
                       name[0] == 'B' || name[0] == 'C' ||
                       name[0] == 'D' || (name[0] >= 'a' && name[0] <= 'd'))
                    {
                        return new InstructionDescription("read", new ReadContext());
                    }
                }
                else if(name.length() == 8)
                {
                    byte value;
                    if(Convert::parseByte(name.substr(0, 2), value, false) &&
                       (value >=0 && value <= 0x1F))
                    {
                        return new InstructionDescription("read", new ReadContext());
                    }
                }
            }
            return NULL;
        }
        
        InstructionSet* DLT645Device::createInstructionSet(const DeviceInfo* di) const
        {
            return new DLT645MasterSet(di);
        }
        DeviceContext* DLT645Device::createDeviceContext(const Convert::KeyPairs& properties) const
        {
            DLT645DeviceContext* context = new DLT645DeviceContext();
            context->updateProperty(properties);
            return context;
        }
    }
}
