//
//  DLT645Instruction.h
//  EVCommon
//
//  Created by baowei on 15/9/1.
//  Copyright (c) 2015 com. All rights reserved.
//

#ifndef __EVCommon__DLT645Instruction__
#define __EVCommon__DLT645Instruction__

#include <string>
#include "common/common_global.h"
#include "common/IO/MemoryStream.h"
#include "common/xml/XmlTextReader.h"
#include "common/driver/instructions/InstructionSet.h"
#include "../DriverTag.h"
#include "../DriverInstruction.h"

using namespace std;
using namespace Common;

namespace Driver
{
    namespace DLT645
    {
        class DLT645DeviceContext : public DriverDeviceContext
        {
        public:
            const static int MinPreambleCount = 1;
            const static int MaxPreambleCount = 4;
            const static int AddressCount = 6;
            const static byte EmptyAddress = 0xAA;
            const static byte BroadcastAddress = 0x99;
            const static byte SlaveException = 0x40;
            
        public:
            enum Direction : byte
            {
                MasterToSlave = 0x00,
                SlaveToMaster = 0x80
            };

            int preambleCount;      // 1-4
            bool broadcast;
            byte address[AddressCount];
            
            DLT645DeviceContext();
            
            DeviceContext* clone() const override;
            
            void updateProperty(const Convert::KeyPairs& properties) override;
            
            bool isCorrectAddress(const byte* address, int offset) const;
        };
        
        class DLT645Context : public DriverInstructionContext
        {
        public:
            DLT645Context();
        };
        
        class ReadContext : public DLT645Context
        {
        public:
            ReadContext();
        };
        
        // Tx: 68 68 00 31 08 10 04 68 01 02 43 C3 8E 16
        // Rx: 
        class DLT645Instruction : public DriverInstruction
        {
        public:
            DLT645Instruction(InstructionDescription* id);
            
            InstructionContext* execute(Interactive* interactive, Device* device, InstructionContext* context, const ByteArray* buffer = NULL) override;
            
        protected:
            bool getSendBuffer(ByteArray& buffer, DeviceDescription* dd, DriverInstructionContext* context) override;
            DriverInstructionContext* setReceiveBuffer(const ByteArray& buffer, DeviceDescription* dd, DriverInstructionContext* context) override;
            
            bool match(const ByteArray& buffer, DeviceDescription* dd) override;
            
            virtual bool getCommandBuffer(MemoryStream& ms, DriverInstructionContext* context) = 0;
            virtual DriverInstructionContext* setCommandBuffer(MemoryStream& ms, DriverInstructionContext* context) = 0;
            
            virtual byte controlCode(DriverInstructionContext* context) const = 0;
            
            bool parseRegisterStr(const string& registerStr, ushort& value) const;
            bool parseRegisterStr(const string& registerStr, uint& value) const;
            
        public:
            const static byte PreambleValue = 0xFE;
            const static byte Header = 0x68;
            const static byte Tail = 0x16;
            const static byte Transfer = 0x33;
            const static int MinFrameLen = 11;
            const static int HeaderPosition = 0;
            const static int AddressPosition = 1;
            const static int HeaderLength = 1 + DLT645DeviceContext::AddressCount + 1 + 1 + 1;  // Header + Address + Header + ControlCode + Length
            const static int HeaderLength2 = HeaderLength - 1;
            const static int ControlCodePosition = 1 + DLT645DeviceContext::AddressCount + 1;
            const static int LengthPosition = ControlCodePosition + 1;
        };
        
        class ReadInstruction : public DLT645Instruction
        {
        public:
            ReadInstruction(InstructionDescription* id);
            
        protected:
            bool getCommandBuffer(MemoryStream& ms, DriverInstructionContext* context) override;
            DriverInstructionContext* setCommandBuffer(MemoryStream& ms, DriverInstructionContext* context) override;
            
            byte controlCode(DriverInstructionContext* context) const override;
        };
        
        class DLT645MasterSet : public DriverInstructionSet
        {
        public:
            DLT645MasterSet(const DeviceInfo* di);
            
        protected:
            bool receive(Device* device, Channel* channel, ByteArray* buffer) override;
            void generateInstructions(Instructions* instructions) override;
            InstructionDescription* create(const string& name) const override;
        };
        
        class DLT645Device : public DriverDevice
        {
        public:
            InstructionSet* createInstructionSet(const DeviceInfo* di) const override;
            DeviceContext* createDeviceContext(const Convert::KeyPairs& properties) const override;
        };
    }
}

#endif /* defined(__EVCommon__DLT645Instruction__) */
