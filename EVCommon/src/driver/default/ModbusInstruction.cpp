//
//  ModbusInstruction.cpp
//  EVCommon
//
//  Created by baowei on 15/7/21.
//  Copyright (c) 2015 com. All rights reserved.
//

#include "ModbusInstruction.h"
#include "common/diag/Debug.h"
#include "common/system/CheckUtilities.h"
#include "common/driver/devices/Device.h"
#include "common/driver/channels/Channel.h"
#include "common/driver/channels/TcpInteractive.h"
#include "common/driver/channels/TcpServerInteractive.h"
#include "common/driver/channels/TcpBackgroudReceiver.h"

namespace Driver
{
    namespace Modbus
    {
        DeviceContext* ModbusDeviceContext::clone() const
        {
            ModbusDeviceContext* context = new ModbusDeviceContext();
            context->copyFrom(this);
            context->mode = this->mode;
            return context;
        }
        void ModbusDeviceContext::updateProperty(const Convert::KeyPairs& properties)
        {
            for (uint i=0; i<properties.count(); i++)
            {
                const Convert::KeyPair* kp = properties[i];
                if(String::stringEquals(kp->name, "mode", true))
                {
                    mode = parseMode(kp->value);
                }
            }
        }
        
        ModbusDeviceContext::Mode ModbusDeviceContext::parseMode(const string& value)
        {
            if(String::stringEquals(value, "RTU", true))
            {
                return Mode::RTU;
            }
            else if(String::stringEquals(value, "ASCII", true))
            {
                return Mode::ASCII;
            }
            return Mode::RTU;
        }
        string ModbusDeviceContext::convertModeStr(Mode value)
        {
            switch (value)
            {
                case Mode::ASCII:
                    return "ASCII";
                case Mode::RTU:
                default:
                    return "RTU";
            }
        }
        
        ModbusContext::ModbusContext() : DriverInstructionContext()
        {
        }
        
        ModbusReadContext::ModbusReadContext() : ModbusContext()
        {
        }
        bool ModbusReadContext::getBatchInfo(const DriverTags& tags, int& min, int& max, int& quantity) const
        {
            if(tags.count() == 0)
                return false;
            
            DriverTag* maxTag = NULL;
            min = MaxAddress;
            max = MinAddress;
            for (uint i=0; i<tags.count(); i++)
            {
                DriverTag* temp = tags[i];
                if(temp->address() <= min)
                    min = temp->address();
                if(temp->address() >= max)
                {
                    max = temp->address();
                    maxTag = temp;
                }
            }
            if(maxTag != NULL)
            {
                quantity = maxTag->length();
                quantity += max - min;
                return true;
            }
            return false;
        }
        bool ModbusReadContext::getBatchInfo(int& min, int&max, int& quantity) const
        {
            return getBatchInfo(_tags, min, max, quantity);
        }
        bool ModbusReadContext::canBatchRead(const DriverTag* tag) const
        {
            if(_tags.count() == 0)
                return true;
            
            DriverTags tags(false);
            tags.addRange(&_tags);
            tags.add(tag);
            
            int min, max, quantity;
            if(!getBatchInfo(tags, min, max, quantity))
                return true;
            
            return isValid(min, max, quantity);
        }
        bool ModbusReadContext::isValid(int min, int max, int quantity) const
        {
            if(!(min >= MinAddress && max <= MaxAddress))
                return false;
            
            if(quantity > maxQuantity())
                return false;
            
            return true;
        }
        
        uint ModbusInstruction::_frameId = 0;
        ModbusInstruction::ModbusInstruction(InstructionDescription* id) : DriverInstruction(id)
        {
        }
        InstructionContext* ModbusInstruction::execute(Interactive* interactive, Device* device, InstructionContext* context, const ByteArray* buffer)
        {
            ModbusContext* mc = dynamic_cast<ModbusContext*>(context);
            assert(mc);
            
            send(interactive, device, mc);
            
            return receive(interactive, device, mc);
        }
        bool ModbusInstruction::getSendBuffer(ByteArray& buffer, DeviceDescription* dd, DriverInstructionContext* context)
        {
            const DriverTags& tags = context->tags();
            if(tags.count() == 0)
                return false;
            
            if(isTcpChannel(dd))
            {
                return getTCPSendBuffer(buffer, dd, context);
            }
            else
            {
                ModbusDeviceContext* mc = dynamic_cast<ModbusDeviceContext*>(dd->context());
                if(mc->mode == ModbusDeviceContext::RTU)
                {
                    return getRTUSendBuffer(buffer, dd, context);
                }
                else if(mc->mode == ModbusDeviceContext::ASCII)
                {
                    return getASCIISendBuffer(buffer, dd, context);
                }
                return false;
            }
        }
        bool ModbusInstruction::getTCPSendBuffer(ByteArray& buffer, DeviceDescription* dd, DriverInstructionContext* context)
        {
            const DriverTags& tags = context->tags();
            if(tags.count() == 0)
                return false;
            
            MemoryStream ms;
            ms.writeUInt16(_frameId++);
            int64_t lenPosition = ms.position();
            ms.writeInt32(0);   // length
            int64_t addrPosition = ms.position();
            ms.writeByte((byte)(dd->address() & 0xFF));
            
            if (!getCommandBuffer(ms, context))
            {
                return false;
            }
            
            int64_t rearPosition = ms.position();
            int length = (int)(rearPosition-addrPosition);
            ms.seek(lenPosition, SeekOrigin::SeekBegin);
            ms.writeInt32(length);   // length
            
            ms.copyTo(buffer);
            
            return true;
        }
        bool ModbusInstruction::getRTUSendBuffer(ByteArray& buffer, DeviceDescription* dd, DriverInstructionContext* context)
        {
            const DriverTags& tags = context->tags();
            if(tags.count() == 0)
                return false;
            
            MemoryStream ms;
            ms.writeByte((byte)(dd->address() & 0xFF));
            
            if (!getCommandBuffer(ms, context))
            {
                return false;
            }
            
            const ByteArray* temp = ms.buffer();
            ushort crc16 = Crc16Utilities::checkByBit(temp->data(), 0, temp->count(), 0xFFFF);
            ms.writeByte(crc16 & 0xFF);
            ms.writeByte((crc16 >> 8) & 0xFF);
            
            ms.copyTo(buffer);
            
            return true;
        }
        bool ModbusInstruction::getASCIISendBuffer(ByteArray& buffer, DeviceDescription* dd, DriverInstructionContext* context)
        {
            const DriverTags& tags = context->tags();
            if(tags.count() == 0)
                return false;
            
            MemoryStream cms;
            cms.writeByte((byte)(dd->address() & 0xFF));
            
            if (!getCommandBuffer(cms, context))
            {
                return false;
            }
            
            MemoryStream ms;
            ms.writeByte(ASCIIHeader);
            
            const ByteArray* cbuffer = cms.buffer();
            for (uint i=0; i<cbuffer->count(); i++)
            {
                ms.writeUInt16(convertASCII(cbuffer->at(i)));
            }
            
            byte lrc8 = LrcUtilities::checkByBit(cbuffer->data(), 0, cbuffer->count());
            ms.writeByte((byte)convertASCII((byte)(lrc8 >> 4) & 0xF));
            ms.writeByte((byte)convertASCII((byte)(lrc8 & 0xF)));
            ms.writeUInt16(ASCIITail);
            
            ms.copyTo(buffer);
            
            return true;
        }
        bool ModbusInstruction::isTcpChannel(const DeviceDescription* dd)
        {
            ChannelDescription* cd = dd->getChannel();
            TcpChannelContext* tc = dynamic_cast<TcpChannelContext*>(cd->context());
            return tc != NULL;
        }
        DriverInstructionContext* ModbusInstruction::setReceiveBuffer(const ByteArray& buffer, DeviceDescription* dd, DriverInstructionContext* context)
        {
            if(isTcpChannel(dd))
            {
                MemoryStream ms(&buffer);
                ms.seek(TCPCommandPosition, SeekOrigin::SeekBegin);
                if(!setCommandBuffer(ms, context))
                    return NULL;
            }
            else
            {
                ModbusDeviceContext* mc = dynamic_cast<ModbusDeviceContext*>(dd->context());
                if(mc->mode == ModbusDeviceContext::RTU)
                {
                    MemoryStream ms(&buffer);
                    ms.seek(RTUCommandPosition, SeekOrigin::SeekBegin);
                    if(!setCommandBuffer(ms, context))
                        return NULL;
                }
                else if(mc->mode == ModbusDeviceContext::ASCII)
                {
                    ByteArray rbuffer;
                    for (uint i=1; i<buffer.count()-1-4; i+=2)
                    {
                        ushort value = ((buffer[i] << 8) & 0xFF00) + (buffer[i+1] & 0xFF);
                        rbuffer.add(convertByte(value));
                    }
                    MemoryStream ms(&rbuffer);
                    ms.seek(ASCIICommandPosition, SeekOrigin::SeekBegin);
                    if(!setCommandBuffer(ms, context))
                        return NULL;
                }
            }
            
            return context;
        }
        ushort ModbusInstruction::convertASCII(byte value)
        {
            ushort result = 0;
            byte hi = Convert::ascToHex((value >> 4) & 0xF);
            result |= (hi << 8) & 0xFF00;
            byte lo = Convert::ascToHex(value & 0xF);
            result |= lo;
            return result;
        }
        byte ModbusInstruction::convertByte(ushort value)
        {
            byte hi = Convert::hexToAsc((value >> 8) & 0xFF);
            byte lo = Convert::hexToAsc(value & 0xFF);
            return ((hi << 4) & 0xF0) + (lo & 0x0F);
        }
        bool ModbusInstruction::match(const ByteArray& buffer, DeviceDescription* dd)
        {
            if(isTcpChannel(dd))
            {
                return matchTCP(buffer, dd);
            }
            else
            {
                ModbusDeviceContext* mc = dynamic_cast<ModbusDeviceContext*>(dd->context());
                if(mc->mode == ModbusDeviceContext::RTU)
                {
                    return matchRTU(buffer, dd);
                }
                else if(mc->mode == ModbusDeviceContext::ASCII)
                {
                    return matchASCII(buffer, dd);
                }
                return false;
            }
        }
        bool ModbusInstruction::matchTCP(const ByteArray& buffer, DeviceDescription* dd)
        {
            if(buffer.count() <= TCPMinFrameLen)
                return false;
            
            int address = buffer[TCPAddressPosition];
            if(address != dd->address())
                return false;
            
            int c = buffer[TCPCommandPosition];
            if(!(c == command() || c == errorCode()))
                return false;
            
            return true;
        }
        bool ModbusInstruction::matchRTU(const ByteArray& buffer, DeviceDescription* dd)
        {
            if(buffer.count() <= RTUMinFrameLen)
                return false;
            
            int address = buffer[RTUAddressPosition];
            if(address != dd->address())
                return false;
            
            int c = buffer[RTUCommandPosition];
            if(!(c == command() || c == errorCode()))
                return false;
            
            // check CRC16
            uint length = buffer.count();
            ushort crc16 = Crc16Utilities::checkByBit(buffer.data(), 0, length - 2, 0xFFFF);
            if (!(buffer[length - 1] == ((crc16 >> 8) & 0xFF) &&
                  buffer[length - 2] == (crc16 & 0xFF)))
            {
                return false;
            }
            
            return true;
        }
        bool ModbusInstruction::matchASCII(const ByteArray& buffer, DeviceDescription* dd)
        {
            if(buffer.count() <= ASCIIMinFrameLen)
                return false;
            
            if(ASCIIHeader != buffer[ASCIIHeaderPosition])
                return false;
            
            uint count = buffer.count();
            ushort tail = ((buffer[count-2] << 8) & 0xFF00) + (buffer[count-1] & 0xFF);
            if(tail != ASCIITail)
                return false;
            
            ByteArray rbuffer;
            for (uint i=1; i<count-1-4; i+=2)
            {
                ushort value = ((buffer[i] << 8) & 0xFF00) + (buffer[i+1] & 0xFF);
                rbuffer.add(convertByte(value));
            }
            
            int address = rbuffer[ASCIIAddressPosition];
            if(address != dd->address())
                return false;
            
            int c = rbuffer[ASCIICommandPosition];
            if(!(c == command() || c == errorCode()))
                return false;
            
            // check LRC
            byte lrc8 = LrcUtilities::checkByBit(rbuffer.data(), 0, rbuffer.count());
            byte check = convertByte(((buffer[count - 4] << 8) & 0xFF00) + (buffer[count - 3] & 0xFF));
            if (lrc8 != check)
            {
                return false;
            }
            
            return true;
        }
        byte ModbusInstruction::errorCode() const
        {
            return command() + 0x80;
        }
        
        ModbusReadInstruction::ModbusReadInstruction(InstructionDescription* id) : ModbusInstruction(id)
        {
        }
        bool ModbusReadInstruction::getCommandBuffer(MemoryStream& ms, DriverInstructionContext* context)
        {
            const DriverTags& tags = context->tags();
            if(tags.count() == 0)
                return false;
            
            // batch read.
            ModbusReadContext* rc = dynamic_cast<ModbusReadContext*>(context);
            assert(rc);
            int min, max, quantity;
            if(!rc->getBatchInfo(min, max, quantity))
                return false;
            
            if(!rc->isValid(min, max, quantity))
                return false;
            
            ms.writeByte(command());
            ms.writeInt16((short)(min & 0xFFFF));
            ms.writeInt16((short)(quantity & 0xFFFF));
            
            return true;
        }
        DriverInstructionContext* ModbusReadInstruction::setCommandBuffer(MemoryStream& ms, DriverInstructionContext* context)
        {
            const DriverTags& tags = context->tags();
            if(tags.count() == 0)
                return NULL;
            
            // batch read
            byte c = ms.readByte();
            if(c == command())
            {
                ModbusReadContext* rc = dynamic_cast<ModbusReadContext*>(context);
                assert(rc);
                int min, max, quantity;
                if(!rc->getBatchInfo(min, max, quantity))
                    return NULL;
                
                byte count = ms.readByte();
                byte realCount = 0;
                byte realQuantity = quantity;
                int qor = quantityOfRegister();
                assert(qor == 1 || qor == 2);
                if(qor == 2)
                {
                    if(count % 2 != 0)
                    {
                        Debug::writeLine("modbus error, divide byteCount by 2, the remainder must be equal to 0.");
                        return NULL;
                    }
                    realCount = count / qor;
                }
                else if(qor == 1)
                {
                    realCount = count;
                    realQuantity = (quantity % 8) == 0 ? (quantity / 8) : (quantity / 8 + 1);
                }
                if(!(realCount >= rc->minCount() && realCount <= rc->maxCount()))
                {
                    Debug::writeFormatLine("modbus error, byteCount must be between %d to %d.", rc->minCount(), rc->maxCount());
                    return NULL;
                }
                if(realCount != realQuantity)
                {
                    Debug::writeFormatLine("modbus error, byteCount must be equal to %d.", quantity);
                    return NULL;
                }
                
                // set tag value.
                setValue(tags, &ms, min, quantity);
            }
            else if(c == errorCode())
            {
#if DEBUG
                byte reason = ms.readByte();    // Exception code, 01 or 02 or 03 or 04
                string reasonStr = errorStr(reason);
                Debug::writeFormatLine("modbus error, code:%d, resion: %s", reason, reasonStr.c_str());
#endif
            }
            
            return context;
        }
        template <class T>
        void ModbusReadInstruction::setMultiBytesValue(DriverTag* tag, Stream* stream)
        {
            // {0xCD, 0xCC, 0x1C, 0x41};  // 9.8
            // CC-CD-41-1C float
            // 41-1C-CC-CD float inverse
            T value;
            const int count = sizeof(value);
            byte buffer[count];
            stream->read(buffer, 0, count);
            
            // inverse
            Convert::KeyPairs properties;
            Convert::splitItems(tag->property(), properties);
            for (uint i=0; i<properties.count(); i++)
            {
                const Convert::KeyPair* kp = properties[i];
                if(String::stringEquals(kp->name, "inverse", true))
                {
                    bool inverse = false;
                    if(Convert::parseBoolean(kp->value, inverse) && inverse)
                    {
                        byte swap;
                        for (uint i=0; i<count; i+=4)
                        {
                            swap = buffer[i];
                            buffer[i] = buffer[i+2];
                            buffer[i+2] = swap;
                            swap = buffer[i+1];
                            buffer[i+1] = buffer[i+3];
                            buffer[i+3] = swap;
                        }
                    }
                }
            }
            
            bool bigEndian = tag->bigEndian();
            if((isBigEndian() && !bigEndian) ||
               (isLittleEndian() && bigEndian))
            {
                byte swap;
                for (uint i=0; i<count; i+=2)
                {
                    swap = buffer[i];
                    buffer[i] = buffer[i+1];
                    buffer[i+1] = swap;
                }
                memcpy(&value, buffer, count);
            }
            else
            {
                memcpy(&value, buffer, count);
            }
            tag->setValue(value);
        }
        void ModbusReadInstruction::setValue(DriverTag* tag, Stream* stream, int min)
        {
            int qor = quantityOfRegister();
            assert(qor == 1 || qor == 2);
            int length = tag->length() * qor;
            if(length == 1)
            {
                byte value = stream->readByte();
                if(tag->type() == Tag::Digital)
                {
                    byte bit = ((tag->address()-min) % 8);
                    value = (value >> bit) & 0x01;
                    tag->setValue(value == 1 ? true : false);
                }
                else
                {
                    tag->setValue(value);
                }
            }
            else if(length == 2)
                tag->setValue(stream->readInt16(tag->bigEndian()));
            else if(length == 3)
                tag->setValue(stream->readInt24(tag->bigEndian()));
            else if(length == 4)
            {
                if(tag->type() == Tag::Float32)
                {
                    setMultiBytesValue<float>(tag, stream);
                }
                else if(tag->type() == Tag::Integer32)
                {
                    setMultiBytesValue<int>(tag, stream);
                }
                else
                {
                    Debug::writeFormatLine("modbus error, the tag'%d' has incorrect length.", tag->id());
                }
            }
            else if(length == 6)
                tag->setValue(stream->readInt48(tag->bigEndian()));
            else if(length == 8)
            {
                if(tag->type() == Tag::Float64)
                {
                    setMultiBytesValue<double>(tag, stream);
                }
                else if(tag->type() == Tag::Integer64)
                {
                    setMultiBytesValue<int64_t>(tag, stream);
                }
                else
                {
                    Debug::writeFormatLine("modbus error, the tag'%d' has incorrect length.", tag->id());
                }
            }
            else
            {
                Debug::writeFormatLine("modbus error, the tag'%d' has incorrect length.", tag->id());
            }
        }
        void ModbusReadInstruction::setValue(const DriverTags& tags, Stream* stream, int min, int quantity)
        {
            int qor = quantityOfRegister();
            assert(qor == 1 || qor == 2);
            int64_t valuePosition = stream->position();
            for (uint i=0; i<tags.count(); i++)
            {
                DriverTag* tag = tags.at(i);
                if(tag->address() >= min && tag->address() < min + quantity)
                {
                    int addr = tag->address();
                    if(qor == 1)
                    {
                        addr = addr < 8 ? addr - min : (addr - min) / 8;
                    }
                    int64_t pos = valuePosition + (addr-min) * qor;
                    if(stream->seek(pos, SeekOrigin::SeekBegin))
                    {
                        setValue(tag, stream, min);
                    }
                }
            }
        }
        
        ReadCoilsInstruction::ReadCoilsInstruction(InstructionDescription* id) : ModbusReadInstruction(id)
        {
        }
        byte ReadCoilsInstruction::command() const
        {
            return 1;
        }
        string ReadCoilsInstruction::errorStr(byte error) const
        {
            string errorStr = "unknown";
            switch (error)
            {
                case 1:
                    errorStr = "￼Function code unsupported";
                    break;
                case 2:
                    errorStr = "￼!(Starting Address == OK AND Starting Address + Quantity of Outputs == OK)";
                    break;
                case 3:
                    errorStr = "!(￼￼0x0001 <= Quantity of Inputs <= 0x007D0)";
                    break;
                case 4:
                    errorStr = "￼ReadDiscreteOutputs != OK";
                    break;
                    
                default:
                    break;
            }
            return errorStr;
        }
        int ReadCoilsInstruction::quantityOfRegister() const
        {
            return 1;
        }
        
        ReadDiscreteInstruction::ReadDiscreteInstruction(InstructionDescription* id) : ModbusReadInstruction(id)
        {
        }
        byte ReadDiscreteInstruction::command() const
        {
            return 2;
        }
        string ReadDiscreteInstruction::errorStr(byte error) const
        {
            string errorStr = "unknown";
            switch (error)
            {
                case 1:
                    errorStr = "￼Function code unsupported";
                    break;
                case 2:
                    errorStr = "￼!(Starting Address == OK AND Starting Address + Quantity of Inputs == OK)";
                    break;
                case 3:
                    errorStr = "!(￼￼0x0001 <= Quantity of Inputs <= 0x007D0)";
                    break;
                case 4:
                    errorStr = "￼￼ReadDiscreteInputs != OK";
                    break;
                    
                default:
                    break;
            }
            return errorStr;
        }
        int ReadDiscreteInstruction::quantityOfRegister() const
        {
            return 1;
        }
        
        ReadHoldingInstruction::ReadHoldingInstruction(InstructionDescription* id) : ModbusReadInstruction(id)
        {
        }
        byte ReadHoldingInstruction::command() const
        {
            return 3;
        }
        string ReadHoldingInstruction::errorStr(byte error) const
        {
            string errorStr = "unknown";
            switch (error)
            {
                case 1:
                    errorStr = "￼Function code unsupported";
                    break;
                case 2:
                    errorStr = "￼!(Starting Address == OK AND Starting Address + Quantity of Registers == OK)";
                    break;
                case 3:
                    errorStr = "!(￼￼0x0001 <= Quantity of Registers <= 0x007D)";
                    break;
                case 4:
                    errorStr = "￼ReadMultipleRegisters != OK";
                    break;
                    
                default:
                    break;
            }
            return errorStr;
        }
        int ReadHoldingInstruction::quantityOfRegister() const
        {
            return 2;
        }
        
        ReadInputInstruction::ReadInputInstruction(InstructionDescription* id) : ModbusReadInstruction(id)
        {
        }
        byte ReadInputInstruction::command() const
        {
            return 4;
        }
        string ReadInputInstruction::errorStr(byte error) const
        {
            string errorStr = "unknown";
            switch (error)
            {
                case 1:
                    errorStr = "￼Function code unsupported";
                    break;
                case 2:
                    errorStr = "￼!(Starting Address == OK AND Starting Address + Quantity of Registers == OK)";
                    break;
                case 3:
                    errorStr = "!(￼￼0x0001 <= Quantity of Registers <= 0x007D)";
                    break;
                case 4:
                    errorStr = "￼ReadInputRegisters != OK";
                    break;
                    
                default:
                    break;
            }
            return errorStr;
        }
        int ReadInputInstruction::quantityOfRegister() const
        {
            return 2;
        }
        
        WriteCoilInstruction::WriteCoilInstruction(InstructionDescription* id) : ModbusInstruction(id)
        {
        }
        bool WriteCoilInstruction::getCommandBuffer(MemoryStream& ms, DriverInstructionContext* context)
        {
            const DriverTags& tags = context->tags();
            if(tags.count() != 1)
                return false;
            
            const DriverTag* tag = tags[0];
            short value;
            if(tag->getValue(value))
            {
                ms.writeByte(command());
                ms.writeInt16((short)(tag->address() & 0xFFFF));
                ms.writeInt16((short)(value & 0xFFFF));
                
                return true;
            }
            return false;
        }
        DriverInstructionContext* WriteCoilInstruction::setCommandBuffer(MemoryStream& ms, DriverInstructionContext* context)
        {
            const DriverTags& tags = context->tags();
            if(tags.count() != 1)
                return NULL;
            
            byte c = ms.readByte();
            if(c == command())
            {
                const DriverTag* tag = tags[0];
                short raddress = ms.readInt16();
                short rvalue = ms.readInt16();
                short value;
                if(tag->getValue(value))
                {
                    return raddress == tag->address() && rvalue == value ? context : NULL;
                }
            }
            else if(c == errorCode())
            {
                byte reason = ms.readByte();    // Exception code, 01 or 02 or 03 or 04
                string reasonStr = "unknown";
                switch (reason)
                {
                    case 1:
                        reasonStr = "￼Function code unsupported";
                        break;
                    case 2:
                        reasonStr = "￼!(Output Address == OK)";
                        break;
                    case 3:
                        reasonStr = "!(￼Output Value == 0x0000 OR 0xFF00)";
                        break;
                    case 4:
                        reasonStr = "WriteSingleOutput != OK";
                        break;
                        
                    default:
                        break;
                }
                Debug::writeFormatLine("modbus error, code:%d, resion: %s", reason, reasonStr.c_str());
            }
            
            return context;
        }
        byte WriteCoilInstruction::command() const
        {
            return 5;
        }
        
        WriteSingleInstruction::WriteSingleInstruction(InstructionDescription* id) : ModbusInstruction(id)
        {
        }
        bool WriteSingleInstruction::getCommandBuffer(MemoryStream& ms, DriverInstructionContext* context)
        {
            const DriverTags& tags = context->tags();
            if(tags.count() != 1)
                return false;
            
            const DriverTag* tag = tags[0];
            short value;
            if(tag->getValue(value))
            {
                ms.writeByte(command());
                ms.writeInt16((short)(tag->address() & 0xFFFF));
                ms.writeInt16((short)(value & 0xFFFF));
                
                return true;
            }
            return false;
        }
        DriverInstructionContext* WriteSingleInstruction::setCommandBuffer(MemoryStream& ms, DriverInstructionContext* context)
        {
            const DriverTags& tags = context->tags();
            if(tags.count() != 1)
                return NULL;
            
            byte c = ms.readByte();
            if(c == command())
            {
                const DriverTag* tag = tags[0];
                short raddress = ms.readInt16();
                short rvalue = ms.readInt16();
                short value;
                if(tag->getValue(value))
                {
                    return raddress == tag->address() && rvalue == value ? context : NULL;
                }
            }
            else if(c == errorCode())
            {
                byte reason = ms.readByte();    // Exception code, 01 or 02 or 03 or 04
                string reasonStr = "unknown";
                switch (reason)
                {
                    case 1:
                        reasonStr = "￼Function code unsupported";
                        break;
                    case 2:
                        reasonStr = "￼!(Register Address == OK)";
                        break;
                    case 3:
                        reasonStr = "!(0x0000 <= Register Value <= 0xFFFF)";
                        break;
                    case 4:
                        reasonStr = "WriteSingleRegister != OK";
                        break;
                        
                    default:
                        break;
                }
                Debug::writeFormatLine("modbus error, code:%d, resion: %s", reason, reasonStr.c_str());
            }
            
            return context;
        }
        byte WriteSingleInstruction::command() const
        {
            return 6;
        }
        
        ModbusMasterSet::ModbusMasterSet(const DeviceInfo* di) : DriverInstructionSet(di)
        {
        }
        bool ModbusMasterSet::receive(Device* device, Channel* channel, ByteArray* buffer)
        {
            bool result = false;
            if (NULL == channel)
            {
                return result;
            }
            if (!channel->connected())
            {
                return result;
            }
            
            DeviceDescription* dd = device->description();
            if(ModbusInstruction::isTcpChannel(device->description()))
            {
                return receiveTCP(device, channel, buffer);
            }
            else
            {
                ModbusDeviceContext* mc = dynamic_cast<ModbusDeviceContext*>(dd->context());
                if(mc->mode == ModbusDeviceContext::RTU)
                {
                    return receiveRTU(device, channel, buffer);
                }
                else if(mc->mode == ModbusDeviceContext::ASCII)
                {
                    return receiveASCII(device, channel, buffer);
                }
                return false;
            }
        }
        bool ModbusMasterSet::receiveTCP(Device* device, Channel* channel, ByteArray* buffer)
        {
            bool result = false;
            if (NULL == channel)
            {
                return result;
            }
            if (!channel->connected())
            {
                return result;
            }
            
            uint timeout = device->description()->receiveTimeout();
            
            const size_t BufferLength = 1024;
            byte* rbuffer = new byte[BufferLength];
            memset(rbuffer, 0, BufferLength);
            
            int len = channel->receiveBySize(rbuffer, BufferLength, ModbusInstruction::TCPHeaderLength, timeout);
            if (ModbusInstruction::TCPHeaderLength != len)
            {
                delete[] rbuffer;
                return result;
            }
            MemoryStream ms(rbuffer, len);
            ms.readUInt16();    // skip frame id
            int dataLen = ms.readInt32();
            len = channel->receiveBySize(rbuffer, BufferLength - ModbusInstruction::TCPHeaderLength, ModbusInstruction::TCPHeaderLength, dataLen, timeout);
            if (len == dataLen)
            {
                buffer->addRange(rbuffer, len + ModbusInstruction::TCPHeaderLength);
                result = true;
            }
            delete[] rbuffer;
            return result;
        }
        bool ModbusMasterSet::receiveRTU(Device* device, Channel* channel, ByteArray* buffer)
        {
            bool result = false;
            if (NULL == channel)
            {
                return result;
            }
            if (!channel->connected())
            {
                return result;
            }
            
            uint timeout = device->description()->receiveTimeout();
            
            const size_t BufferLength = 1024;
            byte* rbuffer = new byte[BufferLength];
            memset(rbuffer, 0, BufferLength);
            
            int len = channel->receiveBySize(rbuffer, BufferLength, ModbusInstruction::RTUHeaderLength, timeout);
            if (ModbusInstruction::RTUHeaderLength != len)
            {
                delete[] rbuffer;
                return result;
            }
            MemoryStream ms(rbuffer, len);
            ms.readByte();      // skip frame id
            ms.readByte();      // skip address
            byte dataLen = ms.readByte() + 2;   // included crc16
            len = channel->receiveBySize(rbuffer, BufferLength - ModbusInstruction::RTUHeaderLength, ModbusInstruction::RTUHeaderLength, dataLen, timeout);
            if (len == dataLen)
            {
                buffer->addRange(rbuffer, len + ModbusInstruction::RTUHeaderLength);
                result = true;
            }
            delete[] rbuffer;
            return result;
        }
        bool ModbusMasterSet::receiveASCII(Device* device, Channel* channel, ByteArray* buffer)
        {
            bool result = false;
            if (NULL == channel)
            {
                return result;
            }
            if (!channel->connected())
            {
                return result;
            }
            
            uint timeout = device->description()->receiveTimeout();
            
            const size_t BufferLength = 1024;
            byte* rbuffer = new byte[BufferLength];
            memset(rbuffer, 0, BufferLength);
            
            byte startBuffer[1] = { ModbusInstruction::ASCIIHeader };
            byte endBuffer[2] = { (ModbusInstruction::ASCIITail >> 8) & 0xFF, ModbusInstruction::ASCIITail & 0xFF };
            int len = channel->receiveByEndBytes(rbuffer, BufferLength, startBuffer, 1, endBuffer, 2, 0, timeout);
            if (len > ModbusInstruction::ASCIIMinFrameLen)
            {
                buffer->addRange(rbuffer, len);
                result = true;
            }
            delete[] rbuffer;
            return result;
        }
        void ModbusMasterSet::generateInstructions(Instructions* instructions)
        {
            instructions->add(new ReadCoilsInstruction(create("01")));
            instructions->add(new ReadDiscreteInstruction(create("02")));
            instructions->add(new ReadHoldingInstruction(create("03")));
            instructions->add(new ReadInputInstruction(create("04")));
            instructions->add(new WriteCoilInstruction(create("05")));
            instructions->add(new WriteSingleInstruction(create("06")));
        }
        InstructionDescription* ModbusMasterSet::create(const string& name) const
        {
            if(String::stringEquals(name, "01", true))
                return new InstructionDescription("01", new ReadCoilsContext());
            else if(String::stringEquals(name, "02", true))
                return new InstructionDescription("02", new ReadDiscreteContext());
            else if(String::stringEquals(name, "03", true))
                return new InstructionDescription("03", new ReadHoldingContext());
            else if(String::stringEquals(name, "04", true))
                return new InstructionDescription("04", new ReadInputContext());
            else if(String::stringEquals(name, "05", true))
                return new InstructionDescription("05", new WriteCoilContext());
            else if(String::stringEquals(name, "06", true))
                return new InstructionDescription("06", new WriteSingleContext());
            return NULL;
        }
        
        InstructionSet* ModbusDevice::createInstructionSet(const DeviceInfo* di) const
        {
            return new ModbusMasterSet(di);
        }
        DeviceContext* ModbusDevice::createDeviceContext(const Convert::KeyPairs& properties) const
        {
            ModbusDeviceContext* context = new ModbusDeviceContext();
            context->updateProperty(properties);
            return context;
        }
    }
}
