//
//  HartInstruction.h
//  EVCommon
//
//  Created by baowei on 15/8/26.
//  Copyright (c) 2015 com. All rights reserved.
//

#ifndef __EVCommon__HartInstruction__
#define __EVCommon__HartInstruction__

#include <string>
#include "common/common_global.h"
#include "common/IO/MemoryStream.h"
#include "common/xml/XmlTextReader.h"
#include "common/driver/instructions/InstructionSet.h"
#include "../DriverTag.h"
#include "../DriverInstruction.h"

using namespace std;
using namespace Common;

namespace Driver
{
    namespace Hart
    {
        class HartDeviceContext : public DriverDeviceContext
        {
        public:
            enum FrameLength : byte
            {
                LongFrame = 0x80,
                ShortFrame = 0x00
            };
            enum FrameType : byte
            {
                BurstFrame = 0x01,
                Master = 0x02,
                Slave = 0x06
            };
            enum LinkRole : byte
            {
                Primary = 0x80,
                Secondary = 0x00
            };
            enum BurstMode : byte
            {
                SlaveInBurstMode = 0x40,
                NoneBurstMode = 0x00
            };
            
            int preambleCount;      // 2-20
            FrameLength frameLength;
            FrameType frameType;
            LinkRole linkRole;
            BurstMode burstMode;
            bool broadcast;
            byte manuId;
            byte devType;
            uint devId;
            
        public:
            HartDeviceContext();
            
            DeviceContext* clone() const override;
            
            void updateProperty(const Convert::KeyPairs& properties) override;
            
        public:
            static FrameLength parseFrameLength(const string& value);
            static string convertFrameLengthStr(FrameLength value);
            
            static FrameType parseFrameType(const string& value);
            static string convertFrameTypeStr(FrameType value);
            
            static LinkRole parseLinkRole(const string& value);
            static string convertLinkRoleStr(LinkRole value);
            
            static BurstMode parseBurstMode(const string& value);
            static string convertBurstModeStr(BurstMode value);
            
        public:
            const static int MinPreambleCount = 2;
            const static int MaxPreambleCount = 20;
        };
        
        class HartContext : public DriverInstructionContext
        {
        public:
            HartContext();
        };
        
        class ReadCommand0Context : public HartContext
        {
        public:
            ReadCommand0Context();
            
            bool canBatchRead(const DriverTag* tag) const override;
        };
        
        class ReadCommand1Context : public HartContext
        {
        public:
            ReadCommand1Context();
            
            bool canBatchRead(const DriverTag* tag) const override;
        };
        
        class ReadCommand3Context : public HartContext
        {
        public:
            ReadCommand3Context();
            
            bool canBatchRead(const DriverTag* tag) const override;
        };
        
        // Tx: FF FF 82 A6 06 BC 61 4E 01 00 B0
        // Rx: FF FF 86 A6 06 BC 61 4E 01 07 00 00 06 06 40 B0 00 00 45
        class HartInstruction : public DriverInstruction
        {
        public:
            HartInstruction(InstructionDescription* id);
            
            InstructionContext* execute(Interactive* interactive, Device* device, InstructionContext* context, const ByteArray* buffer = NULL) override;
            
        protected:
            bool getSendBuffer(ByteArray& buffer, DeviceDescription* dd, DriverInstructionContext* context) override;
            DriverInstructionContext* setReceiveBuffer(const ByteArray& buffer, DeviceDescription* dd, DriverInstructionContext* context) override;
            
            bool match(const ByteArray& buffer, DeviceDescription* dd) override;
            
            virtual bool getCommandBuffer(MemoryStream& ms, DriverInstructionContext* context) = 0;
            virtual DriverInstructionContext* setCommandBuffer(MemoryStream& ms, DriverInstructionContext* context) = 0;
            
            virtual byte command() const = 0;
            
        public:
            const static int PreambleValue = 0xFF;
            const static int MinFrameLen = 7;   // only include 2 preambles.
            const static int LongAddressCount = 5;
            const static int ShortAddressCount = 1;
        };
        
        class ReadCommand0Instruction : public HartInstruction
        {
        public:
            ReadCommand0Instruction(InstructionDescription* id);
            
        protected:
            bool getCommandBuffer(MemoryStream& ms, DriverInstructionContext* context) override;
            DriverInstructionContext* setCommandBuffer(MemoryStream& ms, DriverInstructionContext* context) override;
            
            byte command() const override;
        };
        
        class ReadCommand1Instruction : public HartInstruction
        {
        public:
            ReadCommand1Instruction(InstructionDescription* id);
            
        protected:
            bool getCommandBuffer(MemoryStream& ms, DriverInstructionContext* context) override;
            DriverInstructionContext* setCommandBuffer(MemoryStream& ms, DriverInstructionContext* context) override;
            
            byte command() const override;
        };
        
        class ReadCommand3Instruction : public HartInstruction
        {
        public:
            ReadCommand3Instruction(InstructionDescription* id);
            
        protected:
            bool getCommandBuffer(MemoryStream& ms, DriverInstructionContext* context) override;
            DriverInstructionContext* setCommandBuffer(MemoryStream& ms, DriverInstructionContext* context) override;
            
            byte command() const override;
        };
        
        class HartMasterSet : public DriverInstructionSet
        {
        public:
            HartMasterSet(const DeviceInfo* di);
            
        protected:
            bool receive(Device* device, Channel* channel, ByteArray* buffer) override;
            void generateInstructions(Instructions* instructions) override;
            InstructionDescription* create(const string& name) const override;
        };
        
        class HartDevice : public DriverDevice
        {
        public:
            InstructionSet* createInstructionSet(const DeviceInfo* di) const override;
            DeviceContext* createDeviceContext(const Convert::KeyPairs& properties) const override;
        };
    }
}

#endif /* defined(__EVCommon__HartInstruction__) */
