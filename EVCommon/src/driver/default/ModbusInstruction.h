//
//  ModbusInstruction.h
//  EVCommon
//
//  Created by baowei on 15/7/21.
//  Copyright (c) 2015 com. All rights reserved.
//

#ifndef __EVCommon__ModbusInstruction__
#define __EVCommon__ModbusInstruction__

#include <string>
#include "common/common_global.h"
#include "common/IO/MemoryStream.h"
#include "common/xml/XmlTextReader.h"
#include "common/driver/instructions/InstructionSet.h"
#include "../DriverTag.h"
#include "../DriverInstruction.h"

using namespace std;
using namespace Common;

namespace Driver
{
    namespace Modbus
    {
        class ModbusDeviceContext : public DriverDeviceContext
        {
        public:
            enum Mode
            {
                RTU = 1,
                ASCII = 2
            };
            
            Mode mode;
            
            DeviceContext* clone() const override;
            
            void updateProperty(const Convert::KeyPairs& properties) override;
            
        public:
            static Mode parseMode(const string& value);
            static string convertModeStr(Mode value);
        };
        
        class ModbusContext : public DriverInstructionContext
        {
        public:
            ModbusContext();
            
        public:
            const static int MinAddress = 0x0;
            const static int MaxAddress = 0xFFFF;
        };
        
        class ModbusReadInstruction;
        class ModbusReadContext : public ModbusContext
        {
        public:
            ModbusReadContext();
            
            bool canBatchRead(const DriverTag* tag) const override;
            
        protected:
            virtual int minCount() const = 0;
            virtual int maxCount() const = 0;
            virtual int maxQuantity() const = 0;
            
            bool isValid(int min, int max, int quantity) const;
            
            bool getBatchInfo(const DriverTags& tags, int& min, int& max, int& quantity) const;
            bool getBatchInfo(int& min, int&max, int& quantity) const;
            
        private:
            friend ModbusReadInstruction;
        };
        
        // TCP
        // Rx:01 D9 00 00 00 06 01 04 00 00 00 0A
        // Tx:01 D9 00 00 00 17 01 04 14 00 00 00 02 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
        // RTU
        // Rx:01-04-00-01-00-02-20-0B
        // Tx:01-04-04-00-02-00-03-1A-45
        // ASCII
        // Rx:3A-30-31-30-34-30-30-30-31-30-30-30-32-46-38-0D-0A
        // Tx:3A-30-31-30-34-30-34-30-30-30-32-30-30-30-33-46-32-0D-0A
        class ModbusInstruction : public DriverInstruction
        {
        public:
            ModbusInstruction(InstructionDescription* id);
            
            InstructionContext* execute(Interactive* interactive, Device* device, InstructionContext* context, const ByteArray* buffer = NULL) override;
            
        public:
            static bool isTcpChannel(const DeviceDescription* dd);
            static ushort convertASCII(byte value);
            static byte convertByte(ushort value);
            
        protected:
            bool getSendBuffer(ByteArray& buffer, DeviceDescription* dd, DriverInstructionContext* context) override;
            DriverInstructionContext* setReceiveBuffer(const ByteArray& buffer, DeviceDescription* dd, DriverInstructionContext* context) override;
            
            bool match(const ByteArray& buffer, DeviceDescription* dd) override;
            
            virtual bool getCommandBuffer(MemoryStream& ms, DriverInstructionContext* context) = 0;
            virtual DriverInstructionContext* setCommandBuffer(MemoryStream& ms, DriverInstructionContext* context) = 0;
            
            virtual byte command() const = 0;
            virtual byte errorCode() const;
            
        private:
            bool getTCPSendBuffer(ByteArray& buffer, DeviceDescription* dd, DriverInstructionContext* context);
            bool getRTUSendBuffer(ByteArray& buffer, DeviceDescription* dd, DriverInstructionContext* context);
            bool getASCIISendBuffer(ByteArray& buffer, DeviceDescription* dd, DriverInstructionContext* context);
            
            bool matchTCP(const ByteArray& buffer, DeviceDescription* dd);
            bool matchRTU(const ByteArray& buffer, DeviceDescription* dd);
            bool matchASCII(const ByteArray& buffer, DeviceDescription* dd);
            
        private:
            static uint _frameId;
            
        public:
            const static int TCPHeaderLength = 6;
            const static int TCPLengthPosition = 2;
            const static int TCPAddressPosition = TCPHeaderLength;
            const static int TCPCommandPosition = TCPAddressPosition + 1;
            const static int TCPMinFrameLen = TCPHeaderLength + 2;
            
            const static int RTUHeaderLength = 3;
            const static int RTULengthPosition = 1;
            const static int RTUAddressPosition = 0;
            const static int RTUCommandPosition = RTUAddressPosition + 1;
            const static int RTUMinFrameLen = RTUHeaderLength + 2;
            
            const static int ASCIIHeaderPosition = 0;
            const static int ASCIIHeaderLength = 5;
            const static int ASCIILengthPosition = 2;
            const static int ASCIIAddressPosition = 0;
            const static int ASCIICommandPosition = ASCIIAddressPosition + 1;
            const static int ASCIIMinFrameLen = ASCIIHeaderLength + 4;
            const static byte ASCIIHeader = 0x3A;
            const static ushort ASCIITail = 0x0D0A;
        };
        
        class ModbusReadInstruction : public ModbusInstruction
        {
        public:
            ModbusReadInstruction(InstructionDescription* id);
            
        protected:
            bool getCommandBuffer(MemoryStream& ms, DriverInstructionContext* context) override;
            DriverInstructionContext* setCommandBuffer(MemoryStream& ms, DriverInstructionContext* context) override;
            
            virtual string errorStr(byte error) const = 0;
            virtual int quantityOfRegister() const = 0;
            
            void setValue(DriverTag* tag, Stream* stream, int min);
            void setValue(const DriverTags& tags, Stream* stream, int min, int quantity);
            
            template <class T>
            void setMultiBytesValue(DriverTag* tag, Stream* stream);
            
        private:
            static bool isLittleEndian()
            {
                int x = 1;
                return *(char*)&x == '\1';
            }
            static bool isBigEndian()
            {
                return !isLittleEndian();
            }
        };
        
        // Read coils(01)
        class ReadCoilsContext : public ModbusReadContext
        {
        public:
            ReadCoilsContext() : ModbusReadContext()
            {
            }
            
        protected:
            int minCount() const override
            {
                return MinCount;
            }
            int maxCount() const override
            {
                return MaxCount;
            }
            int maxQuantity() const override
            {
                return MaxQuantity;
            }
            
        private:
            const static int MinCount = 0x01;
            const static int MaxCount = 0x7D0;
            const static int MaxQuantity = 0x7D0;
        };
        // Read coils(01)
        class ReadCoilsInstruction : public ModbusReadInstruction
        {
        public:
            ReadCoilsInstruction(InstructionDescription* id);
            
        protected:
            byte command() const override;
            
            string errorStr(byte error) const override;
            int quantityOfRegister() const override;
        };
        
        // Read Discrete Inputs(02)
        class ReadDiscreteContext : public ModbusReadContext
        {
        public:
            ReadDiscreteContext() : ModbusReadContext()
            {
            }
            
        protected:
            int minCount() const override
            {
                return MinCount;
            }
            int maxCount() const override
            {
                return MaxCount;
            }
            int maxQuantity() const override
            {
                return MaxQuantity;
            }
            
        private:
            const static int MinCount = 0x01;
            const static int MaxCount = 0x7D0;
            const static int MaxQuantity = 0x7D0;
        };
        // Read Discrete Inputs(02)
        class ReadDiscreteInstruction : public ModbusReadInstruction
        {
        public:
            ReadDiscreteInstruction(InstructionDescription* id);
            
        protected:
            byte command() const override;
            
            string errorStr(byte error) const override;
            int quantityOfRegister() const override;
        };
        
        // Read Holding registers(03)
        class ReadHoldingContext : public ModbusReadContext
        {
        public:
            ReadHoldingContext() : ModbusReadContext()
            {
            }
            
        protected:
            int minCount() const override
            {
                return MinCount;
            }
            int maxCount() const override
            {
                return MaxCount;
            }
            int maxQuantity() const override
            {
                return MaxQuantity;
            }
            
        private:
            const static int MinCount = 0x01;
            const static int MaxCount = 0x7D;
            const static int MaxQuantity = 0x7D;
        };
        // Read Holding registers(03)
        class ReadHoldingInstruction : public ModbusReadInstruction
        {
        public:
            ReadHoldingInstruction(InstructionDescription* id);
            
        protected:
            byte command() const override;
            
            string errorStr(byte error) const override;
            int quantityOfRegister() const override;
        };
        
        // Read input registers(04)
        class ReadInputContext : public ModbusReadContext
        {
        public:
            ReadInputContext() : ModbusReadContext()
            {
            }
            
        protected:
            int minCount() const override
            {
                return MinCount;
            }
            int maxCount() const override
            {
                return MaxCount;
            }
            int maxQuantity() const override
            {
                return MaxQuantity;
            }
            
        private:
            const static int MinCount = 0x01;
            const static int MaxCount = 0x7D;
            const static int MaxQuantity = 0x7D;
        };
        // Read input registers(04)
        class ReadInputInstruction : public ModbusReadInstruction
        {
        public:
            ReadInputInstruction(InstructionDescription* id);
            
        protected:
            byte command() const override;
            
            string errorStr(byte error) const override;
            int quantityOfRegister() const override;
        };
        
        // Write Single Coil(05)
        class WriteCoilContext : public ModbusContext
        {
        public:
            WriteCoilContext() : ModbusContext()
            {
            }
        };
        // Write Single Coil(05)
        class WriteCoilInstruction : public ModbusInstruction
        {
        public:
            WriteCoilInstruction(InstructionDescription* id);
            
        protected:
            bool getCommandBuffer(MemoryStream& ms, DriverInstructionContext* context) override;
            DriverInstructionContext* setCommandBuffer(MemoryStream& ms, DriverInstructionContext* context) override;
            
            byte command() const override;
        };
        
        // Write single register(06)
        class WriteSingleContext : public ModbusContext
        {
        public:
            WriteSingleContext() : ModbusContext()
            {
            }
        };
        // Write single register(06)
        class WriteSingleInstruction : public ModbusInstruction
        {
        public:
            WriteSingleInstruction(InstructionDescription* id);
            
        protected:
            bool getCommandBuffer(MemoryStream& ms, DriverInstructionContext* context) override;
            DriverInstructionContext* setCommandBuffer(MemoryStream& ms, DriverInstructionContext* context) override;
            
            byte command() const override;
        };
        
        class ModbusMasterSet : public DriverInstructionSet
        {
        public:
            ModbusMasterSet(const DeviceInfo* di);
            
        protected:
            bool receive(Device* device, Channel* channel, ByteArray* buffer) override;
            void generateInstructions(Instructions* instructions) override;
            InstructionDescription* create(const string& name) const override;
            
        private:
            bool receiveTCP(Device* device, Channel* channel, ByteArray* buffer);
            bool receiveRTU(Device* device, Channel* channel, ByteArray* buffer);
            bool receiveASCII(Device* device, Channel* channel, ByteArray* buffer);
        };
        
        class ModbusDevice : public DriverDevice
        {
        public:
            InstructionSet* createInstructionSet(const DeviceInfo* di) const override;
            DeviceContext* createDeviceContext(const Convert::KeyPairs& properties) const override;
        };
    }
}

#endif /* defined(__EVCommon__Instruction__) */
