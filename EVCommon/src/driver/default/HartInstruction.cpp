//
//  HartInstruction.cpp
//  EVCommon
//
//  Created by baowei on 15/8/26.
//  Copyright (c) 2015 com. All rights reserved.
//

#include "HartInstruction.h"
#include "common/diag/Debug.h"
#include "common/system/CheckUtilities.h"
#include "common/driver/devices/Device.h"
#include "common/driver/channels/Channel.h"
#include "common/driver/channels/TcpInteractive.h"
#include "common/driver/channels/TcpServerInteractive.h"
#include "common/driver/channels/TcpBackgroudReceiver.h"

namespace Driver
{
    namespace Hart
    {
        HartDeviceContext::HartDeviceContext()
        {
            preambleCount = 5;
            frameLength = FrameLength::LongFrame;
            frameType = FrameType::Master;
            linkRole = LinkRole::Primary;
            burstMode = BurstMode::NoneBurstMode;
            broadcast = false;
            manuId = 0xFF;
            devType = 0xFF;
            devId = 0xFFFFFF;
        }
        DeviceContext* HartDeviceContext::clone() const
        {
            HartDeviceContext* context = new HartDeviceContext();
            context->copyFrom(this);
            
            return context;
        }
        void HartDeviceContext::updateProperty(const Convert::KeyPairs& properties)
        {
            for (uint i=0; i<properties.count(); i++)
            {
                const Convert::KeyPair* kp = properties[i];
                if(String::stringEquals(kp->name, "framelength", true))
                {
                    frameLength = parseFrameLength(kp->value);
                }
                else if(String::stringEquals(kp->name, "frametype", true))
                {
                    frameType = parseFrameType(kp->value);
                }
                else if(String::stringEquals(kp->name, "preamblecount", true))
                {
                    int count;
                    if(Convert::parseInt32(kp->value, count) &&
                       count >= MinPreambleCount && count <= MaxPreambleCount)
                    {
                        preambleCount = count;
                    }
                }
                else if(String::stringEquals(kp->name, "linkrole", true))
                {
                    linkRole = parseLinkRole(kp->value);
                }
                else if(String::stringEquals(kp->name, "burstmode", true))
                {
                    burstMode = parseBurstMode(kp->value);
                }
                else if(String::stringEquals(kp->name, "broadcast", true))
                {
                    Convert::parseBoolean(kp->value, broadcast);
                }
                else if(String::stringEquals(kp->name, "manuid", true))
                {
                    Convert::parseByte(kp->value, manuId);
                }
                else if(String::stringEquals(kp->name, "devtype", true))
                {
                    Convert::parseByte(kp->value, devType);
                }
                else if(String::stringEquals(kp->name, "devid", true))
                {
                    Convert::parseUInt32(kp->value, devId);
                }
            }
        }
        HartDeviceContext::FrameLength HartDeviceContext::parseFrameLength(const string& value)
        {
            if(String::stringEquals(value, "long", true))
            {
                return FrameLength::LongFrame;
            }
            else if(String::stringEquals(value, "short", true))
            {
                return FrameLength::ShortFrame;
            }
            return FrameLength::LongFrame;
        }
        string HartDeviceContext::convertFrameLengthStr(FrameLength value)
        {
            switch (value)
            {
                case FrameLength::LongFrame:
                    return "long";
                case FrameLength::ShortFrame:
                default:
                    return "short";
            }
        }
        HartDeviceContext::FrameType HartDeviceContext::parseFrameType(const string& value)
        {
            if(String::stringEquals(value, "burst", true))
            {
                return FrameType::BurstFrame;
            }
            else if(String::stringEquals(value, "master", true))
            {
                return FrameType::Master;
            }
            else if(String::stringEquals(value, "slave", true))
            {
                return FrameType::Slave;
            }
            return FrameType::Master;
        }
        string HartDeviceContext::convertFrameTypeStr(FrameType value)
        {
            switch (value)
            {
                case FrameType::BurstFrame:
                    return "burst";
                case FrameType::Slave:
                    return "slave";
                case FrameType::Master:
                default:
                    return "master";
            }
        }
        
        HartDeviceContext::LinkRole HartDeviceContext::parseLinkRole(const string& value)
        {
            if(String::stringEquals(value, "primary", true))
            {
                return LinkRole::Primary;
            }
            else if(String::stringEquals(value, "secondary", true))
            {
                return LinkRole::Secondary;
            }
            return LinkRole::Primary;
        }
        string HartDeviceContext::convertLinkRoleStr(LinkRole value)
        {
            switch (value)
            {
                case LinkRole::Secondary:
                    return "secondary";
                case LinkRole::Primary:
                default:
                    return "primary";
            }
        }
        
        HartDeviceContext::BurstMode HartDeviceContext::parseBurstMode(const string& value)
        {
            if(String::stringEquals(value, "burst", true))
            {
                return BurstMode::SlaveInBurstMode;
            }
            else if(String::stringEquals(value, "none", true))
            {
                return BurstMode::NoneBurstMode;
            }
            return BurstMode::NoneBurstMode;
        }
        string HartDeviceContext::convertBurstModeStr(BurstMode value)
        {
            switch (value)
            {
                case BurstMode::SlaveInBurstMode:
                    return "burst";
                case BurstMode::NoneBurstMode:
                default:
                    return "none";
            }
        }
        
        HartContext::HartContext() : DriverInstructionContext()
        {
        }
        
        HartInstruction::HartInstruction(InstructionDescription* id) : DriverInstruction(id)
        {
        }
        InstructionContext* HartInstruction::execute(Interactive* interactive, Device* device, InstructionContext* context, const ByteArray* buffer)
        {
            DriverInstructionContext* dc = dynamic_cast<DriverInstructionContext*>(context);
            assert(dc);
            
            send(interactive, device, dc);
            
            return receive(interactive, device, dc);
        }
        bool HartInstruction::getSendBuffer(ByteArray& buffer, DeviceDescription* dd, DriverInstructionContext* context)
        {
            const DriverTags& tags = context->tags();
            if(tags.count() == 0)
                return false;
            
            HartDeviceContext* dc = dynamic_cast<HartDeviceContext*>(dd->context());
            assert(dc);
            
            MemoryStream ms;
            
            // preambles
            byte preambles[HartDeviceContext::MaxPreambleCount];
            memset(preambles, 0xFF, sizeof(preambles));
            ms.write(preambles, 0, dc->preambleCount);
            
            // delimiter
            int64_t delimiterPos = ms.position();
            byte delimiter = command() == 0 ? (HartDeviceContext::ShortFrame | dc->frameType) : (dc->frameLength | dc->frameType);
            ms.writeByte(delimiter);
            
            // address
            if(dc->frameLength == HartDeviceContext::FrameLength::ShortFrame ||
               command() == 0)
            {
                byte address = dc->linkRole | dc->burstMode | (dc->address() & 0xF);
                ms.writeByte(address);
            }
            else
            {
                assert(dc->frameLength == HartDeviceContext::FrameLength::LongFrame);
                byte manuId = !dc->broadcast ? (dc->manuId & 0x3F) : 0;
                byte address = dc->linkRole | dc->burstMode | manuId;
                ms.writeByte(address);
                ms.writeByte(!dc->broadcast ? dc->devType : 0);
                ms.writeUInt24(!dc->broadcast ? dc->devId : 0);
            }
            
            // command
            ms.writeByte(command());
            
            // byteCount
            byte byteCount = 0;
            ms.writeByte(byteCount);
            
            // data
            if(!getCommandBuffer(ms, context))
                return false;
            
            // check
            const ByteArray* temp = ms.buffer();
            byte check = BccUtilities::checkByBit(temp->data(), (int)delimiterPos, temp->count()-(int)delimiterPos);
            ms.writeByte(check);
            
            ms.copyTo(buffer);
            
            return true;
        }
        DriverInstructionContext* HartInstruction::setReceiveBuffer(const ByteArray& buffer, DeviceDescription* dd, DriverInstructionContext* context)
        {
            HartDeviceContext* dc = dynamic_cast<HartDeviceContext*>(dd->context());
            assert(dc);
            
            int delimiterPos = dc->preambleCount;
            byte delimiter = buffer[delimiterPos];
            int commandPos = delimiterPos + 1;
            bool longFrame = (delimiter & HartDeviceContext::FrameLength::LongFrame) != 0 ? true : false;
            commandPos += (longFrame ? LongAddressCount : ShortAddressCount);
            int responseCodePos = commandPos + 2;   // skip command & byteCount
            
            MemoryStream ms(&buffer);
            ms.seek(commandPos, SeekOrigin::SeekBegin);
            byte cmd = ms.readByte(); // read cmd for cmd 0 process later
            ms.seek(responseCodePos, SeekOrigin::SeekBegin);
            
            // check response code, 2bytes
            byte commCode = ms.readByte();
            ms.readByte();  // skip device code.
            // todo: analyse response code.
            if(commCode != 0)
                return NULL;
            
            // only include bcc.
            if(ms.position() + 1 == ms.length())
                return NULL;

            if (cmd == 0)
            {
                if (ms.length() - ms.position() > 12)
                {
                    ms.readByte(); // skip data byte 0: 254
                    dc->frameLength = HartDeviceContext::LongFrame;
                    dc->manuId = ms.readByte();
                    dc->devType = ms.readByte();
                    ms.seek(ms.position() + 6); // offset 6 bytes
                    dc->devId = ms.readUInt24();
                }
            }
            // seek to data position.
            if(!setCommandBuffer(ms, context))
                return NULL;
            
            return context;
        }
        bool HartInstruction::match(const ByteArray& buffer, DeviceDescription* dd)
        {
            if(buffer.count() <= MinFrameLen)
                return false;
            
            HartDeviceContext* dc = dynamic_cast<HartDeviceContext*>(dd->context());
            assert(dc);
            
            // check preambles.
            for (int i=0; i<dc->preambleCount; i++)
            {
                if(buffer[i] != PreambleValue)
                    return false;
            }
            
            // check frame type.
            int delimiterPos = dc->preambleCount;
            byte delimiter = buffer[delimiterPos];
            byte frameType = delimiter & 0x7;
            if(!(frameType == HartDeviceContext::FrameType::BurstFrame ||
                 frameType == HartDeviceContext::FrameType::Slave))
            {
                return false;
            }
            
            // check address
            int addressPos = delimiterPos+1;
            if(dc->frameLength == HartDeviceContext::FrameLength::ShortFrame ||
               command() == 0)
            {
                byte address = buffer[addressPos] & 0x0F;
                if(dc->address() != address)
                    return false;
            }
            else
            {
                assert(dc->frameLength == HartDeviceContext::FrameLength::LongFrame);
                if(!dc->broadcast)
                {
                    byte manuId = buffer[addressPos] & 0x3F;
                    if((dc->manuId & 0x3F) != manuId)
                        return false;
                    byte devType = buffer[addressPos+1];
                    if(dc->devType != devType)
                        return false;
                    uint devId = ((buffer[addressPos+2+0] << 16) & 0xFF0000) + ((buffer[addressPos+2+1] << 8) & 0x00FF00) + buffer[addressPos+2+2];
                    if(dc->devId != devId)
                        return false;
                }
                else
                {
                    byte manuId = buffer[addressPos] & 0x3F;
                    if(0 != manuId)
                        return false;
                    byte devType = buffer[addressPos+1];
                    if(0 != devType)
                        return false;
                    uint devId = ((buffer[addressPos+2+0] << 16) & 0xFF0000) + ((buffer[addressPos+2+1] << 8) & 0x00FF00) + buffer[addressPos+2+2];
                    if(0 != devId)
                        return false;
                }
            }
            
            int commandPos = delimiterPos + 1;
            bool longFrame = (delimiter & HartDeviceContext::FrameLength::LongFrame) != 0 ? true : false;
            commandPos += (longFrame ? LongAddressCount : ShortAddressCount);
            
            // check command
            int c = buffer[commandPos];
            if(c != command())
                return false;
            
            // check BCC
            byte check = BccUtilities::checkByBit(buffer.data(), (int)delimiterPos, buffer.count()-(int)delimiterPos-1);
            if (check != buffer[buffer.count()-1])
            {
                return false;
            }
            
            return true;
        }
        
        ReadCommand0Context::ReadCommand0Context() : HartContext()
        {
        }
        bool ReadCommand0Context::canBatchRead(const DriverTag* tag) const
        {
            return true;
        }
        
        ReadCommand1Context::ReadCommand1Context() : HartContext()
        {
        }
        bool ReadCommand1Context::canBatchRead(const DriverTag* tag) const
        {
            return true;
        }
        
        ReadCommand3Context::ReadCommand3Context() : HartContext()
        {
        }
        bool ReadCommand3Context::canBatchRead(const DriverTag* tag) const
        {
            return true;
        }
        
        ReadCommand0Instruction::ReadCommand0Instruction(InstructionDescription* id) : HartInstruction(id)
        {
        }
        bool ReadCommand0Instruction::getCommandBuffer(MemoryStream& ms, DriverInstructionContext* context)
        {
            return true;
        }
        DriverInstructionContext* ReadCommand0Instruction::setCommandBuffer(MemoryStream& ms, DriverInstructionContext* context)
        {
            // process cmd 0 data out of this method to store the IDs into device context structure
            return context;
        }
        byte ReadCommand0Instruction::command() const
        {
            return 0;
        }
        
        ReadCommand1Instruction::ReadCommand1Instruction(InstructionDescription* id) : HartInstruction(id)
        {
        }
        bool ReadCommand1Instruction::getCommandBuffer(MemoryStream& ms, DriverInstructionContext* context)
        {
            return true;
        }
        DriverInstructionContext* ReadCommand1Instruction::setCommandBuffer(MemoryStream& ms, DriverInstructionContext* context)
        {
            const DriverTags& tags = context->tags();
            if(tags.count() == 0)
                return NULL;
            
            int64_t valuePosition = ms.position();
            for (uint i=0; i<tags.count(); i++)
            {
                DriverTag* tag = tags.at(i);
                if(tag->address() == 0)
                {
                    // read PV unit code
                    if(ms.seek(valuePosition+tag->address(), SeekOrigin::SeekBegin))
                    {
                        byte pvUnitCode = ms.readByte();
                        tag->setValue(pvUnitCode);
                    }
                }
                else if(tag->address() == 1)
                {
                    // read PV
                    if(ms.seek(valuePosition+tag->address(), SeekOrigin::SeekBegin))
                    {
                        float value = ms.readFloat();
                        tag->setValue(value);
                    }
                }
            }
            
            return context;
        }
        byte ReadCommand1Instruction::command() const
        {
            return 1;
        }
        
        ReadCommand3Instruction::ReadCommand3Instruction(InstructionDescription* id) : HartInstruction(id)
        {
        }
        bool ReadCommand3Instruction::getCommandBuffer(MemoryStream& ms, DriverInstructionContext* context)
        {
            return true;
        }
        DriverInstructionContext* ReadCommand3Instruction::setCommandBuffer(MemoryStream& ms, DriverInstructionContext* context)
        {
            const DriverTags& tags = context->tags();
            if(tags.count() == 0)
                return NULL;
            
            int64_t valuePosition = ms.position();
            for (uint i=0; i<tags.count(); i++)
            {
                DriverTag* tag = tags.at(i);
                if(tag->registerStr() == "3")
                {
                    if(tag->address() == 0)
                    {
                        // read current
                        if(ms.seek(valuePosition+tag->address(), SeekOrigin::SeekBegin))
                        {
                            float current = ms.readFloat();
                            tag->setValue(current);
                        }
                    }
                    else if(tag->address() == 4)
                    {
                        // read PV unit code
                        if(ms.seek(valuePosition+tag->address(), SeekOrigin::SeekBegin))
                        {
                            byte unitCode = ms.readByte();
                            tag->setValue(unitCode);
                        }
                    }
                    else if(tag->address() == 5)
                    {
                        // read PV
                        if(ms.seek(valuePosition+tag->address(), SeekOrigin::SeekBegin))
                        {
                            float value = ms.readFloat();
                            tag->setValue(value);
                        }
                    }
                    else if(tag->address() == 9)
                    {
                        // read SV unit code
                        if(ms.seek(valuePosition+tag->address(), SeekOrigin::SeekBegin))
                        {
                            byte unitCode = ms.readByte();
                            tag->setValue(unitCode);
                        }
                    }
                    else if(tag->address() == 10)
                    {
                        // read SV
                        if(ms.seek(valuePosition+tag->address(), SeekOrigin::SeekBegin))
                        {
                            float value = ms.readFloat();
                            tag->setValue(value);
                        }
                    }
                    else if(tag->address() == 14)
                    {
                        // read TV unit code
                        if(ms.seek(valuePosition+tag->address(), SeekOrigin::SeekBegin))
                        {
                            byte unitCode = ms.readByte();
                            tag->setValue(unitCode);
                        }
                    }
                    else if(tag->address() == 15)
                    {
                        // read TV
                        if(ms.seek(valuePosition+tag->address(), SeekOrigin::SeekBegin))
                        {
                            float value = ms.readFloat();
                            tag->setValue(value);
                        }
                    }
                    else if(tag->address() == 19)
                    {
                        // read FV unit code
                        if(ms.seek(valuePosition+tag->address(), SeekOrigin::SeekBegin))
                        {
                            byte unitCode = ms.readByte();
                            tag->setValue(unitCode);
                        }
                    }
                    else if(tag->address() == 20)
                    {
                        // read FV
                        if(ms.seek(valuePosition+tag->address(), SeekOrigin::SeekBegin))
                        {
                            float value = ms.readFloat();
                            tag->setValue(value);
                        }
                    }
                }
                else if(tag->registerStr() == "current")
                {
                    if(ms.seek(valuePosition+0, SeekOrigin::SeekBegin))
                    {
                        float value = ms.readFloat();
                        tag->setValue(value);
                    }
                }
                else if(tag->registerStr() == "PV")
                {
                    if(ms.seek(valuePosition+5, SeekOrigin::SeekBegin))
                    {
                        float value = ms.readFloat();
                        tag->setValue(value);
                    }
                }
                else if(tag->registerStr() == "SV")
                {
                    if(ms.seek(valuePosition+10, SeekOrigin::SeekBegin))
                    {
                        float value = ms.readFloat();
                        tag->setValue(value);
                    }
                }
                else if(tag->registerStr() == "TV")
                {
                    if(ms.seek(valuePosition+15, SeekOrigin::SeekBegin))
                    {
                        float value = ms.readFloat();
                        tag->setValue(value);
                    }
                }
                else if(tag->registerStr() == "FV")
                {
                    if(ms.seek(valuePosition+20, SeekOrigin::SeekBegin))
                    {
                        float value = ms.readFloat();
                        tag->setValue(value);
                    }
                }
            }
            return context;
        }
        byte ReadCommand3Instruction::command() const
        {
            return 3;
        }
        
        HartMasterSet::HartMasterSet(const DeviceInfo* di) : DriverInstructionSet(di)
        {
        }
        bool HartMasterSet::receive(Device* device, Channel* channel, ByteArray* buffer)
        {
            if (NULL == channel)
            {
                return false;
            }
            if (!channel->connected())
            {
                return false;
            }
            
            HartDeviceContext* dc = dynamic_cast<HartDeviceContext*>(device->description()->context());
            assert(dc);
            
            uint timeout = device->description()->receiveTimeout();
            
            const size_t BufferLength = 1024;
            byte rbuffer[BufferLength];
            memset(rbuffer, 0, BufferLength);
            
            // preamble
            int preambleCount = dc->preambleCount;
            int offset = 0;
            bool matchHeader = false;
            do
            {
                int rLength = channel->receiveBySize(rbuffer, BufferLength-offset, offset, 1, timeout);
                if (rLength == 1 && rbuffer[offset] == HartInstruction::PreambleValue)
                {
                    offset++;
                    matchHeader = offset == preambleCount;
                }
                else
                {
                    matchHeader = false;
                    break;
                }
                
            } while (offset != preambleCount);
            if (!matchHeader)
            {
                return false;
            }
            
            // delimiter
            int rLength = channel->receiveBySize(rbuffer, BufferLength-offset, offset, 1, timeout);
            if (rLength != 1)
            {
                return false;
            }
            offset++;
            byte delimiter = rbuffer[preambleCount];
            bool longFrame = (delimiter & HartDeviceContext::FrameLength::LongFrame) != 0 ? true : false;
            int frameLength = longFrame ? HartInstruction::LongAddressCount + 2 : HartInstruction::ShortAddressCount + 2;
            
            // address + command + byteCount
            rLength = channel->receiveBySize(rbuffer, BufferLength-offset, offset, frameLength, timeout);
            if (rLength != frameLength)
            {
                return false;
            }
            offset += frameLength;
            byte byteCount = rbuffer[offset-1] + 1;     // included check.
            
            // data + check
            rLength = channel->receiveBySize(rbuffer, BufferLength-offset, offset, byteCount, timeout);
            if (rLength != byteCount)
            {
                return false;
            }
            buffer->addRange(rbuffer, offset+byteCount);
            return true;
        }
        void HartMasterSet::generateInstructions(Instructions* instructions)
        {
            instructions->add(new ReadCommand0Instruction(create("0")));
            instructions->add(new ReadCommand1Instruction(create("1")));
            instructions->add(new ReadCommand3Instruction(create("3")));
        }
        InstructionDescription* HartMasterSet::create(const string& name) const
        {
            if(String::stringEquals(name, "0", true))
                return new InstructionDescription("0", new ReadCommand0Context());
            else if(String::stringEquals(name, "1", true))
                return new InstructionDescription("1", new ReadCommand1Context());
            else if(String::stringEquals(name, "3", true) ||
                    String::stringEquals(name, "current", true) ||
                    String::stringEquals(name, "PV", true) ||
                    String::stringEquals(name, "SV", true) ||
                    String::stringEquals(name, "TV", true) ||
                    String::stringEquals(name, "FV", true))
            {
                return new InstructionDescription("3", new ReadCommand3Context());
            }
            return NULL;
        }
        
        InstructionSet* HartDevice::createInstructionSet(const DeviceInfo* di) const
        {
            return new HartMasterSet(di);
        }
        DeviceContext* HartDevice::createDeviceContext(const Convert::KeyPairs& properties) const
        {
            HartDeviceContext* context = new HartDeviceContext();
            context->updateProperty(properties);
            return context;
        }
    }
}
