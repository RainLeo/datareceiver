//
//  ChannelInfo.cpp
//  EVCommon
//
//  Created by baowei on 15/7/20.
//  Copyright (c) 2015 com. All rights reserved.
//

#include "ChannelInfo.h"

namespace Driver
{
    void ChannelInfo::TcpInfo::read(XmlTextReader& reader)
    {
        address = reader.getAttribute("address");
        Convert::parseInt32(reader.getAttribute("port"), port);
    }
    
    bool ChannelInfo::isValid() const
    {
        return linkType != LinkType::LinkNone;
    }
    
    void ChannelInfo::read(XmlTextReader& reader)
    {
        name = reader.getAttribute("name");
        linkType = parseLinkType(reader.getAttribute("linktype"));
        Convert::parseBoolean(reader.getAttribute("enable"), enable);
        Convert::parseBoolean(reader.getAttribute("reopened"), reopened);
        TimeSpan::parse(reader.getAttribute("receive"), link.tcp.timeout.receive);
        switch (linkType)
        {
            case LinkTcp:
                link.tcp.read(reader);
                break;
            case LinkSerial:
                link.serial.read(reader);
                break;
            case LinkDtu:
                link.dtu.id = reader.getAttribute("dtuid");
                link.dtu.vendor = reader.getAttribute("dtuvendor");
                break;
            default:
                break;
        }
    }
    
    string ChannelInfo::toInteractiveName() const
    {
        switch (linkType)
        {
            case LinkTcp:
                return "TcpInteractive";
            case LinkSerial:
                return "SerialInteractive";
            case LinkDtu:
                return "DtuInteractive";
            default:
                break;
        }
        return "";
    }
    
    ChannelInfo::LinkType ChannelInfo::parseLinkType(const string& value)
    {
        if(String::stringEquals(value, "None", true))
        {
            return LinkType::LinkNone;
        }
        else if(String::stringEquals(value, "Tcp", true))
        {
            return LinkType::LinkTcp;
        }
        else if(String::stringEquals(value, "Serial", true))
        {
            return LinkType::LinkSerial;
        }
        else if (String::stringEquals(value, "Dtu", true))
        {
            return LinkType::LinkDtu;
        }
        return LinkType::LinkNone;
    }
    string ChannelInfo::convertLinkTypeStr(ChannelInfo::LinkType value)
    {
        switch (value)
        {
            case LinkType::LinkNone:
                return "None";
            case LinkType::LinkTcp:
                return "Tcp";
            case LinkType::LinkSerial:
                return "Serial";
            case LinkType::LinkDtu:
                return "Dtu";
            default:
                return "None";
        }
    }
}