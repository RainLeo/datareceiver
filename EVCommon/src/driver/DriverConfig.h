#ifndef DRIVERCONFIG_H
#define DRIVERCONFIG_H

#include <string>
#include "common/common_global.h"
#include "common/system/Singleton.h"
#include "common/xml/Configuration.h"
#include "DriverTag.h"
#include "DeviceInfo.h"
#include "ChannelInfo.h"

using namespace std;
using namespace Common;

namespace Driver
{
    class DriverData
    {
    public:
        bool allowLog;
        bool useUtcTime;
        bool compareTimestamp;
        
        ChannelInfos channels;
        DeviceInfos devices;
        
        DriverData();
//        DriverData(const DriverData& data);
//        
//        void operator=(const DriverData& value);
//        bool operator==(const DriverData& value) const;
//        bool operator!=(const DriverData& value) const;
    };
    
    class DriverService;
    class COMMON_EXPORT DriverConfig : public Configuration
    {
    public:
        DriverConfig(const ConfigFile& file, DriverData* data);
        ~DriverConfig();
        
    protected:
        bool load(XmlTextReader& reader);
        
    private:
        DriverData* _data;
    };
}

#endif	// DRIVERCONFIG_H
