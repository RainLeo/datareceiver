//
//  DriverInstruction.h
//  EVCommon
//
//  Created by baowei on 15/7/21.
//  Copyright (c) 2015 com. All rights reserved.
//

#ifndef __EVCommon__DriverInstruction__
#define __EVCommon__DriverInstruction__

#include <string>
#include "common/common_global.h"
#include "common/xml/XmlTextReader.h"
#include "common/driver/devices/DeviceDescription.h"
#include "common/driver/instructions/InstructionSet.h"
#include "DriverTag.h"
#include "DeviceInfo.h"

using namespace std;
using namespace Common;

namespace Driver
{
    class DriverInstruction;
    class DriverInstructionSet;
    class DriverDeviceContext : public DeviceContext
    {
        virtual void updateProperty(const Convert::KeyPairs& properties) = 0;
    };
    
    class DriverInstructionContext : public InstructionContext
    {
    public:
        DriverInstructionContext();
        
        const DriverTags& tags() const;
        void addTag(const DriverTag* tag);
        bool contains(const DriverTag* tag);
        
        virtual bool canBatchRead(const DriverTag* tag) const;
        virtual bool canBatchWrite(const DriverTag* tag) const;
        
    protected:
        DriverTags _tags;
        Mock* _mock;
        
    private:
        friend DriverInstruction;
        friend DriverInstructionSet;
    };

    class ICommAllowLog;
    class DriverSampler;
    class DriverInstruction : public Instruction
    {
    public:
        DriverInstruction(InstructionDescription* id);
        
    protected:
        virtual bool getSendBuffer(ByteArray& buffer, DeviceDescription* dd, DriverInstructionContext* context) = 0;
        virtual DriverInstructionContext* setReceiveBuffer(const ByteArray& buffer, DeviceDescription* dd, DriverInstructionContext* context) = 0;
        
        virtual bool match(const ByteArray& buffer, DeviceDescription* dd) = 0;
        
        InstructionContext* send(Interactive* interactive, Device* device, DriverInstructionContext* context);
        InstructionContext* receive(Interactive* interactive, Device* device, DriverInstructionContext* context);
        
    private:
        InstructionDescription* find(const DriverTag* tag) const;
        
    private:
        friend DriverInstructionSet;
        friend DriverSampler;
    };
    
    class DeviceInfo;
    class DriverSampler;
    class DriverInstructionSet : public InstructionSet
    {
    public:
        DriverInstructionSet(const DeviceInfo* di);
        
        string getInstructionName(const string& registerStr) const;
        
    protected:
        virtual bool receive(Device* device, Channel* channel, ByteArray* buffer) = 0;
        virtual InstructionDescription* create(const string& name) const = 0;
        
    private:
        InstructionDescription* create(const InstructionDescription* id, const DriverTag* tag);
        InstructionDescription* create(const InstructionDescriptions& ids, const DriverTag* tag);
        bool updateBatchRead(Instructions* instructions, const DriverTag* tag) const;
        void updateMock(DriverInstructionContext* di, const DriverTag* tag);
        
    private:
        friend DriverSampler;
        
        DeviceInfo* _di;
    };
    
    class DriverDevice
    {
    public:
        virtual ~DriverDevice()
        {
        }
        
        virtual InstructionSet* createInstructionSet(const DeviceInfo* di) const = 0;
        virtual DeviceContext* createDeviceContext(const Convert::KeyPairs& properties) const
        {
            return NULL;
        }
    };
}

#endif /* defined(__EVCommon__DriverInstruction__) */
