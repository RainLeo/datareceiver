//
//  DriverInstruction.cpp
//  EVCommon
//
//  Created by baowei on 15/7/21.
//  Copyright (c) 2015 com. All rights reserved.
//

#include "common/diag/Debug.h"
#include "common/data/ByteArray.h"
#include "common/driver/devices/Device.h"
#include "common/driver/channels/Channel.h"
#include "common/driver/channels/TcpInteractive.h"
#include "common/driver/channels/TcpServerInteractive.h"
#include "common/driver/channels/TcpBackgroudReceiver.h"
#include "DeviceInfo.h"
#include "DriverInstruction.h"
#include "DriverService.h"
#include "common/system/Math.h"

namespace Driver
{
    DriverInstructionContext::DriverInstructionContext()
    {
        _tags.setAutoDelete(false);
        _mock = NULL;
    }
    const DriverTags& DriverInstructionContext::tags() const
    {
        return _tags;
    }
    void DriverInstructionContext::addTag(const DriverTag* tag)
    {
        _tags.add(tag);
    }
    bool DriverInstructionContext::contains(const DriverTag* tag)
    {
        return tag != NULL && _tags.contains(tag);
    }
    bool DriverInstructionContext::canBatchRead(const DriverTag*) const
    {
        return false;
    }
    bool DriverInstructionContext::canBatchWrite(const DriverTag* tag) const
    {
        return false;
    }
    
    DriverInstruction::DriverInstruction(InstructionDescription* id) : Instruction(id)
    {
    }
    InstructionContext* DriverInstruction::send(Interactive* interactive, Device* device, DriverInstructionContext* context)
    {
        if (interactive == NULL)
        {
            return NULL;
        }
        context->clearException(); // added by HYC on 1/30/2016 to fix issue: always no_connect error if first send was failed
        setContext(context);
        
        Channel* channel = interactive->channel();
        if (channel == NULL)
        {
            return NULL;
        }
        
        ByteArray buffer;
        if(context->_mock != NULL &&
           context->_mock->send.count() > 0)
        {
            buffer.addRange(&context->_mock->send);
        }
        else
        {
            if (!getSendBuffer(buffer, device->description(), context))
            {
                return NULL;
            }
        }
        
        try
        {
            int sendLen = channel->send(buffer.data(), 0, buffer.count());
            if(sendLen <= 0)
            {
                context->setExcetion(-1, "no_connect");
            }
        }
        catch (Exception& e)
        {
            if (strcmp(e.getMessage(), "no_connect") == 0)
            {
                context->setExcetion(-1, "no_connect");
                Debug::writeFormatLine("error: no_connect");
                return context;
            }
        }
        log(&buffer, true);
        
        return context;
    }
    InstructionContext* DriverInstruction::receive(Interactive* interactive, Device* device, DriverInstructionContext* context)
    {
        if (interactive == NULL)
        {
            return NULL;
        }
        setContext(context);
        
        Channel* channel = interactive->channel();
        if (channel == NULL)
        {
            return NULL;
        }
        
        if(context->_mock != NULL &&
           context->_mock->recv.count() > 0)
        {
            Channel* channel = interactive->channel();
            channel->updateMockRecvBuffer(context->_mock->recv);
        }
        
        ByteArray buffer;
        if(!device->instructionSet()->receive(device, interactive->channel(), &buffer))
            return NULL;
        
        log(&buffer, false);
        
        if (!match(buffer, device->description()))
        {
            return NULL;
        }
        
        InstructionContext* result = setReceiveBuffer(buffer, device->description(), context);
        
        return result;
    }
    InstructionDescription* DriverInstruction::find(const DriverTag* tag) const
    {
        DriverInstructionContext* dc = dynamic_cast<DriverInstructionContext*>(context());
        if(dc != NULL && dc->contains(tag))
        {
            return description();
        }
        return NULL;
    }
    
    DriverInstructionSet::DriverInstructionSet(const DeviceInfo* di) : InstructionSet()
    {
        if(di == NULL)
            throw ArgumentNullException("di");
        
        _di = (DeviceInfo*)di;
    }
    string DriverInstructionSet::getInstructionName(const string& registerStr) const
    {
        InstructionDescription* id = create(registerStr);
        if(id != NULL)
        {
            string name = id->name();
            delete id;
            return name;
        }
        return registerStr;
    }
    InstructionDescription* DriverInstructionSet::create(const InstructionDescription* id, const DriverTag* tag)
    {
        bool combined = false;
        if(_di->batchwrite && id != NULL)
        {
            DriverInstructionContext* di = dynamic_cast<DriverInstructionContext*>(id->context());
            assert(di);
            if(di->canBatchWrite(tag))
            {
                combined = true;
                di->addTag(tag);
                updateMock(di, tag);
            }
        }
        
        if(!combined)
        {
            InstructionDescription *temp = create(tag->registerStr());
            DriverInstructionContext* di = dynamic_cast<DriverInstructionContext*>(temp->context());
            assert(di);
            di->addTag(tag);
            updateMock(di, tag);
            return temp;
        }
        return NULL;
    }
    InstructionDescription* DriverInstructionSet::create(const InstructionDescriptions& ids, const DriverTag* tag)
    {
        bool combined = false;
        if(_di->batchread)
        {
            for (uint i=0; i<ids.count(); i++)
            {
                const InstructionDescription* id = ids[i];
                DriverInstructionContext* di = dynamic_cast<DriverInstructionContext*>(id->context());
                assert(di);
                if(di->canBatchRead(tag))
                {
                    combined = true;
                    di->addTag(tag);
                    updateMock(di, tag);
                    return NULL;
                }
            }
        }
        
        if(!combined)
        {
            InstructionDescription *id = create(tag->registerStr());
            if(id != NULL)
            {
                DriverInstructionContext* di = dynamic_cast<DriverInstructionContext*>(id->context());
                assert(di);
                di->addTag(tag);
                updateMock(di, tag);
                return id;
            }
        }
        
        Trace::writeFormatLine("Can not find register'%s', tag name: %s", tag->registerStr().c_str(), tag->name().c_str());
        return NULL;
    }
    void DriverInstructionSet::updateMock(DriverInstructionContext* di, const DriverTag* tag)
    {
        if(_di->enable && _di->hasMock())
        {
            for (uint i=0; i<_di->mocks.count(); i++)
            {
                Mock* mock = _di->mocks[i];
                if(mock->enable &&
                   mock->registerStr == tag->registerStr())
                {
                    di->_mock = mock;
                    break;
                }
            }
        }
    }
}
