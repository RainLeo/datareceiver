﻿#include "DriverService.h"
#include "common/system/Convert.h"
#include "common/thread/Locker.h"
#include "common/diag/Trace.h"
#include "DriverConfig.h"
#include "DriverSampler.h"
#include "default/ModbusInstruction.h"
#include "default/HartInstruction.h"
#include "default/DLT645Instruction.h"
#include "tag/TagService.h"

using namespace rtdb;

namespace Driver
{
    void driver_tagValueChanged(void* owner, void* sender, EventArgs* args)
    {
        DriverTag* dTag = static_cast<DriverTag*>(owner);
        if(dTag != NULL)
        {
            DriverService* ds = dTag->service();
            if(ds != NULL)
            {
                Tag* tag = static_cast<Tag*>(sender);
                assert(tag);
                
                if(dTag->setDbValue(tag->type(), tag->value()))
                {
                    // add an instruction.
                    ds->writeDriverTag(dTag);
                }
            }
        }
    }
    void driver_openStartAction(void* parameter)
    {
        DriverService* ds = static_cast<DriverService*>(parameter);
        if(ds != nullptr)
        {
            ds->_manager->open();
        }
    }
    
    DriverService::DriverService()
    {
        _manager = new DriverManager();
        _openThread = nullptr;
    }
    DriverService::~DriverService()
    {
        if(_openThread != nullptr)
        {
            _openThread->stop();
            delete _openThread;
            _openThread = nullptr;
        }
        
        delete _manager;
        _manager = NULL;
    }
    
    bool DriverService::initialize(const ConfigFile* file)
    {
        ConfigFile temp = *file;
        temp.fileName = "driver.config";
        DriverConfig reader(temp, &_data);
        if(!reader.Configuration::load())
        {
            Trace::writeFormatLine("Failed to load config file. name: %s", temp.fileName.c_str());
            return false;
        }
        
        TagService* ts = getService<TagService>();
        for(uint i=0;i<_data.devices.count();i++)
        {
            const DeviceInfo* di = _data.devices[i];
            if(di->enable)
            {
                const DriverTags* tags = di->tags();
                for (uint j=0; j<tags->count(); j++)
                {
                    DriverTag* dTag = tags->at(j);
                    dTag->_service = this;
                    dTag->_data = &_data;
                    
                    if(dTag->validLinkedId())
                    {
                        if(ts)
                        {
                            Tag* tag = ts->getTag(dTag->linkedId());
                            if(tag != NULL)
                            {
                                dTag->setDbTag(tag);
                                ts->addTagValueChangedHandler(dTag->linkedId(), Delegate(dTag, driver_tagValueChanged));
                            }
                        }
                    }
                }
            }
        }
        
        if(!createDevices())
            return false;
        
        _openThread = new Thread;
        _openThread->setName("driver_openStartAction");
        _openThread->start(driver_openStartAction, this);
        
        return true;
    }
    bool DriverService::unInitialize()
    {
        _manager->close();

        if (_openThread != nullptr)
        {
            _openThread->stop();
            delete _openThread;
            _openThread = nullptr;
        }
        
        TagService* ts = getService<TagService>();
        for(uint i=0;i<_data.devices.count();i++)
        {
            const DeviceInfo* di = _data.devices[i];
            if(di->enable)
            {
                const DriverTags* tags = di->tags();
                for (uint j=0; j<tags->count(); j++)
                {
                    DriverTag* dTag = tags->at(j);
                    
                    if(dTag->validLinkedId())
                    {
                        if(ts)
                        {
                            ts->removeTagValueChangedHandler(dTag->linkedId(), Delegate(dTag, driver_tagValueChanged));
                        }
                    }
                }
            }
        }
    
        return false;
    }
    
    bool DriverService::createDevices()
    {
        for(uint i=0;i<_data.devices.count();i++)
        {
            const DeviceInfo* di = _data.devices[i];
            if(di->enable)
            {
                createDevice(di);
            }
        }
        
        return true;
    }
    bool DriverService::createDevice(const DeviceInfo* di)
    {
        DriverManager* dm = manager();
        assert(dm);
        
        const ChannelInfo* ci = getChannelInfo(di->channelName);
        if(ci == NULL || !ci->isValid() || !ci->enable)
            return false;
        
        InstructionSet* set = createInstructionSet(di);
        if(set == NULL)
            return false;
        
        ChannelDescription* cd = new ChannelDescription(ci->name, ci->toInteractiveName());
        updateChannelContext(ci, cd->context());
        
        DeviceContext* dc = createDeviceContext(di);
        DeviceDescription* dd = new DeviceDescription(di->name, cd, set, dc);
        updateDeviceDescription(ci, di, dd);
        
        dm->description()->addDevice(dd);
        
        DriverSampler* sampler = nullptr;
        sampler = dynamic_cast<DriverSampler*>(dm->getPoolByChannelName(ci->name));
        if(sampler != nullptr)
        {
            sampler->addDevice(dd);
        }
        else
        {
            sampler = new DriverSampler(this, cd, dd);   // createSampler(di, cd, dd);
            if(sampler != NULL)
            {
                dm->addPool(sampler);
            }
        }
        
        Trace::writeFormatLine("create a device, name: %s, linkType = %s", di->name.c_str(), ChannelInfo::convertLinkTypeStr(ci->linkType).c_str());
        
        return true;
    }
    
    DriverDevice* DriverService::createDevice(const DeviceInfo* di) const
    {
        if(String::stringEquals(di->type, "ModbusDevice", true))
        {
            return new Modbus::ModbusDevice();
        }
        else if(String::stringEquals(di->type, "HartDevice", true))
        {
            return new Hart::HartDevice();
        }
        else if(String::stringEquals(di->type, "DLT645Device", true))
        {
            return new DLT645::DLT645Device();
        }
        
        return NULL;
        
        //        string value = "";
        //        Convert::KeyPairs pairs;
        //        if(Convert::splitItems(di->instruction, pairs))
        //        {
        //            for (uint i=0; i<pairs.count(); i++)
        //            {
        //                const Convert::KeyPair* kp = pairs[i];
        //                if(String::stringEquals(kp->name, "set", true))
        //                {
        //                    value = kp->value;
        //                    break;
        //                }
        //            }
        //        }
        //
        //        if(value.empty())
        //            return NULL;
    }
    InstructionSet* DriverService::createInstructionSet(const DeviceInfo* di) const
    {
        DriverDevice* dd = createDevice(di);
        if(dd != NULL)
        {
            InstructionSet* is = dd->createInstructionSet(di);
            delete dd;
            return is;
        }
        
        return NULL;
    }
    DeviceContext* DriverService::createDeviceContext(const DeviceInfo* di) const
    {
        DriverDevice* dd = createDevice(di);
        if(dd != NULL)
        {
            DeviceContext* context = NULL;
            Convert::KeyPairs properties;
            Convert::splitItems(di->property, properties);
            context = dd->createDeviceContext(properties);
            
            delete dd;
            return context;
        }
        
        return NULL;
    }
    Sampler* DriverService::createSampler(const DeviceInfo* di, ChannelDescription* cd, DeviceDescription* dd)
    {
        // todo: create a custom sampler.
        return NULL;
    }
    void DriverService::updateChannelContext(const ChannelInfo* ci, ChannelContext* cc)
    {
        cc->setReopened(ci->reopened);
        switch (ci->linkType)
        {
            case ChannelInfo::LinkTcp:
            {
                TcpChannelContext* tc = dynamic_cast<TcpChannelContext*>(cc);
                if(tc != NULL)
                {
                    tc->setAddress(ci->link.tcp.address);
                    tc->setPort(ci->link.tcp.port);
                    tc->setSendBufferSize(ci->link.tcp.sendBufferSize);
                    tc->setReceiveBufferSize(ci->link.tcp.receiveBufferSize);
                    tc->setNoDelay(ci->link.tcp.noDelay);
                    tc->setOpenTimeout(ci->link.tcp.timeout.open);
                }
            }
                break;
            case ChannelInfo::LinkSerial:
            {
                SerialChannelContext* sc = dynamic_cast<SerialChannelContext*>(cc);
                if(sc != NULL)
                {
                    sc->setSerial(ci->link.serial);
                }
            }
                break;
            case ChannelInfo::LinkDtu:
            {
                DtuChannelContext* dcc = dynamic_cast<DtuChannelContext*>(cc);
                if (dcc != NULL)
                {
                    dcc->setId(ci->link.dtu.id);
                    dcc->setVendor(ci->link.dtu.vendor);
                }
            }
                break;

            default:
                break;
        }
    }
    void DriverService::updateDeviceDescription(const ChannelInfo* ci, const DeviceInfo* di, DeviceDescription* dd)
    {
        dd->setAddress(di->address);
        dd->setReceiveTimeout(ci->link.tcp.timeout.receive);
        switch (ci->linkType)
        {
            case ChannelInfo::LinkTcp:
            {
                dd->setReceiveTimeout(ci->link.tcp.timeout.receive);
                dd->setSendTimeout(ci->link.tcp.timeout.send);
            }
                break;
            case ChannelInfo::LinkSerial:
                break;
            case ChannelInfo::LinkDtu:
                break;
            default:
                break;
        }
    }
    const ChannelInfo* DriverService::getChannelInfo(const string& channelName) const
    {
        for(uint i=0;i<_data.channels.count();i++)
        {
            const ChannelInfo* ci = _data.channels[i];
            if(ci->name == channelName)
                return ci;
        }
        return NULL;
    }
    
    DriverTags* DriverService::getTags(const string& deviceName) const
    {
        for(uint i=0;i<_data.devices.count();i++)
        {
            const DeviceInfo* di = _data.devices[i];
            if(di->enable && di->name == deviceName)
            {
                return di->tags();
            }
        }
        return NULL;
    }
    
    DriverManager* DriverService::manager()
    {
        return _manager;
    }

    void DriverService::addTagValueChangedHandler(const DriverTag* tag, const Delegate& delegate)
    {
        if(tag != NULL)
        {
            DriverTag* temp = getTag(tag->id());
            if(temp != NULL)
            {
                temp->valueChangedDelegates()->add(delegate);
            }
        }
    }
    void DriverService::removeTagValueChangedHandler(const DriverTag* tag, const Delegate& delegate)
    {
        if(tag != NULL)
        {
            DriverTag* temp = getTag(tag->id());
            if(temp != NULL)
            {
                temp->valueChangedDelegates()->remove(delegate);
            }
            
        }
    }
    
    void DriverService::addTagValueChangedHandler(uint tagId, const Delegate& delegate)
    {
        DriverTag* tag = getTag(tagId);
        if(tag != NULL)
        {
            tag->valueChangedDelegates()->add(delegate);
        }
    }
    void DriverService::removeTagValueChangedHandler(uint tagId, const Delegate& delegate)
    {
        DriverTag* tag = getTag(tagId);
        if(tag != NULL)
        {
            tag->valueChangedDelegates()->remove(delegate);
        }
    }
    
    void DriverService::addTagValueChangedHandler(const Delegate& delegate)
    {
        for(uint i=0;i<_data.devices.count();i++)
        {
            const DeviceInfo* di = _data.devices.at(i);
            if(di->enable)
            {
                const DriverTags* tags = di->tags();
                for (uint j = 0; j<tags->count(); j++)
                {
                    DriverTag* tag = tags->at(i);
                    tag->valueChangedDelegates()->add(delegate);
                }
            }
        }
    }
    void DriverService::removeTagValueChangedHandler(const Delegate& delegate)
    {
        for(uint i=0;i<_data.devices.count();i++)
        {
            const DeviceInfo* di = _data.devices.at(i);
            if(di->enable)
            {
                const DriverTags* tags = di->tags();
                for (uint j=0; j<tags->count(); j++)
                {
                    DriverTag* tag = tags->at(i);
                    tag->valueChangedDelegates()->remove(delegate);
                }
            }
        }
    }
    
    DriverTag* DriverService::getTag(uint tagId) const
    {
        for(uint i=0;i<_data.devices.count();i++)
        {
            const DeviceInfo* di = _data.devices.at(i);
            if(di->enable)
            {
                const DriverTags* tags = di->tags();
                for (uint j = 0; j<tags->count(); j++)
                {
                    DriverTag* tag = tags->at(i);
                    if(tag->id() == tagId)
                    {
                        return tag;
                    }
                }
            }
        }
        return NULL;
    }
    
    bool DriverService::allowLog() const
    {
        return _data.allowLog;
    }
    
    void DriverService::writeDriverTag(const DriverTag* tag)
    {
        DriverManager* dm = manager();
        assert(dm);
        
        const InstructionPools* pools = dm->getPools();
        for (uint i=0; i<pools->count(); i++)
        {
            DriverSampler* ds = dynamic_cast<DriverSampler*>(pools->at(i));
            if(ds != nullptr)
            {
                ds->addInstruction(tag);
            }
        }
    }
}
