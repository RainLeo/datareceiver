//
//  DriverSampler.cpp
//  EVCommon
//
//  Created by baowei on 15/7/21.
//  Copyright (c) 2015 com. All rights reserved.
//

#include "common/diag/Debug.h"
#include "DriverSampler.h"
#include "DriverInstruction.h"
#include "DriverService.h"
#include "common/thread/TickTimeout.h"

namespace Driver
{
    DriverSampler::ReadGroup::ReadGroup()
    {
        this->dd = nullptr;
        instructions.setAutoDelete(false);
        start = (uint)-1;
    }
    bool DriverSampler::ReadGroup::isTimeup()
    {
        if(start == (uint)-1)
        {
            start = TickTimeout::GetCurrentTickCount();
            return true;
        }
        
        assert(interval != TimeSpan::Zero);
        bool result = TickTimeout::IsTimeout(start, start + (uint)interval.totalMilliseconds());
        if(result)
        {
            start = TickTimeout::GetCurrentTickCount();
        }
        return result;
    }
    DriverSampler::ReadGroup* DriverSampler::ReadGroups::getSameGroup(DeviceDescription* dd, const string& registerStr, TimeSpan interval) const
    {
        for (uint i=0; i<count(); i++)
        {
            DriverSampler::ReadGroup* group = at(i);
            if(group->dd == dd && group->registerStr == registerStr && group->interval == interval)
                return group;
        }
        return NULL;
    }
    
    DriverSampler::WriteGroup::WriteGroup(const DriverTag* tag)
    {
        this->dd = nullptr;
        this->instruction = NULL;
        this->tag = (DriverTag*)tag;
    }
    bool DriverSampler::WriteGroup::isValid() const
    {
        return this->instruction != NULL && this->tag != NULL;
    }
    DriverSampler::WriteGroup* DriverSampler::WriteGroups::getGroup(DeviceDescription* dd, const DriverTag* tag) const
    {
        for (uint i=0; i<count(); i++)
        {
            DriverSampler::WriteGroup* group = at(i);
            if(group->dd == dd && group->tag == tag)
                return group;
        }
        return NULL;
    }
    DriverSampler::WriteGroup* DriverSampler::WriteGroups::getGroup(const DriverTag* tag) const
    {
        for (uint i=0; i<count(); i++)
        {
            DriverSampler::WriteGroup* group = at(i);
            if(group->tag == tag)
                return group;
        }
        return NULL;
    }
    
    DriverSampler::DriverSampler(DriverService* service, ChannelDescription* cd, DeviceDescription* dd) : InstructionPool(service->manager(), cd, dd, true)
    {
        if(service == NULL)
            throw ArgumentNullException("service");
        
        _service = service;
        
        _connected = Device::Unknown;
        _showConnectedError = true;
        _isInvalidStatus = false;
        _checkOnlineFailedCount = 0;
        _connetedFailedCount = 0;
        
        _detetionCount = 3;
        _resumeStart = 0;
        _resumeInterval = 10 * 1000;     // 10s
    }
    DriverSampler::~DriverSampler()
    {
    }
    
    void DriverSampler::start()
    {
        group(_dd);
        if(hadAnotherDevices())
            group(_dds);
        
        updateLog();
        
        InstructionPool::start();
    }
    
    Device::Status DriverSampler::getConnectStatus() const
    {
        return  _connected;
    }
    bool DriverSampler::isOnline() const
    {
        return getConnectStatus() == Device::Online;
    }
    
    void DriverSampler::addInstruction(DeviceDescription* dd, const DriverTag* tag)
    {
        // get instruction by tag.
        WriteGroup* group = _writeGroups.getGroup(dd, tag);
        if(group != NULL && group->isValid())
        {
            if(!hadAnotherDevices())
                addInstructionInner(group->instruction, Packet::AutoDelete::PacketOnly, ReadWritePriority::WritePriority);
            else
                addInstructionInner(group->dd, group->instruction, Packet::AutoDelete::PacketOnly, ReadWritePriority::WritePriority);
        }
    }
    void DriverSampler::addInstruction(const DriverTag* tag)
    {
        // get instruction by tag.
        WriteGroup* group = _writeGroups.getGroup(tag);
        if(group != NULL && group->isValid())
        {
            if(!hadAnotherDevices())
                addInstructionInner(group->instruction, Packet::AutoDelete::PacketOnly, ReadWritePriority::WritePriority);
            else
                addInstructionInner(group->dd, group->instruction, Packet::AutoDelete::PacketOnly, ReadWritePriority::WritePriority);
        }
    }
    
    void DriverSampler::group(DeviceDescription* dd)
    {
        DriverTags* tags = _service->getTags(dd->name());
        if(tags != NULL)
        {
            for (uint i=0; i<tags->count(); i++)
            {
                const DriverTag* tag = tags->at(i);
                if(tag->canRead())
                {
                    groupReadTag(dd, tag);
                }
                if(tag->canWrite())
                {
                    groupWriteTag(dd, tag);
                }
            }
        }
    }
    void DriverSampler::group(const DeviceDescriptions& dds)
    {
        for (uint i=0; i<dds.count(); i++)
        {
            group(dds[i]);
        }
    }
    void DriverSampler::groupReadTag(DeviceDescription* dd, const DriverTag* tag)
    {
        DriverInstructionSet* is = dynamic_cast<DriverInstructionSet*>(dd->instructionSet());
        assert(is);
        string name = is->getInstructionName(tag->registerStr());
        ReadGroup* group = _readGroups.getSameGroup(dd, name, tag->interval());
        if(group == NULL)
        {
            group = new ReadGroup();
            group->dd = dd;
            group->registerStr = name;
            group->interval = tag->interval();
            _readGroups.add(group);
        }
        
        // add instructionDescription to group.
        InstructionDescription* id = is->create(group->instructions, tag);
        if(id != NULL)
        {
            group->instructions.add(id);
        }
    }
    void DriverSampler::groupWriteTag(DeviceDescription* dd, const DriverTag* tag)
    {
        // add instructionDescription to group.
        WriteGroup* group = _writeGroups.getGroup(dd, tag);
        if(group == NULL)
        {
            group = new WriteGroup(tag);
            group->dd = dd;
            _writeGroups.add(group);
        }
        
        DriverInstructionSet* is = dynamic_cast<DriverInstructionSet*>(dd->instructionSet());
        assert(is);
        InstructionDescription* id = is->create(group->instruction, tag);
        if(id != NULL)
        {
            group->instruction = id;
        }
    }
    
    void DriverSampler::updateLog()
    {
        if(_device != nullptr)
        {
            _device->setAllowLog(_service->allowLog());
        }
    }
    
    void DriverSampler::addSampleInstruction()
    {
        // add the sample instruction into the looparray.
        if (_isInvalidStatus)
        {
            if(_resumeStart == 0)
            {
                _resumeStart = TickTimeout::GetCurrentTickCount();
            }
            uint interval = _resumeInterval;
            if(TickTimeout::IsTimeout(_resumeStart, _resumeStart + interval))
            {
                _isInvalidStatus = false;
                
#ifdef DEBUG
                if(_showConnectedError)
                    writeMessage("Reconnect the device, name: %s");
#endif
            }
        }
        else
        {
            for (uint i=0; i<_readGroups.count(); i++)
            {
                ReadGroup* group = _readGroups[i];
                if(group->isTimeup())
                {
//                    Debug::writeFormatLine("driver sampler is time up!, device name: %s", group->dd->name().c_str());
                    if(!hadAnotherDevices())
                        addInstructionInner(group->instructions, Packet::AutoDelete::PacketOnly, ReadWritePriority::ReadPriority);
                    else
                        addInstructionInner(group->dd, group->instructions, Packet::AutoDelete::PacketOnly, ReadWritePriority::ReadPriority);
                }
            }
        }
    }
    void DriverSampler::instructionProcInner()
    {
        if (!_pause)
        {
            addSampleInstruction();
        }
        InstructionPool::instructionProcInner();
    }
    
    void DriverSampler::errorHandle(Channel* channel, bool error)
    {
        if (error)
        {
#if DEBUG
            if(_showConnectedError)
                writeMessage("Unable to connect the device, name: %s");
#endif
            
            _checkOnlineFailedCount++;
            
            if (_checkOnlineFailedCount >= _detetionCount)
            {
                if(channel != NULL && channel->context()->reopened())
                {
                    channel->close();
                }
                setConnectStatus(Device::Offline);
#if DEBUG
                if(_showConnectedError)
                    writeMessage("The device was failure, name: %s");
#endif
                
                _resumeStart = 0;
                _isInvalidStatus = true;
                
                _checkOnlineFailedCount = 0;
                _connetedFailedCount++;
            }
        }
        else
        {
            setConnectStatus(Device::Online);
            
            _checkOnlineFailedCount = 0;
            _connetedFailedCount = 0;
        }
    }
    
    void DriverSampler::setConnectedError(bool showError)
    {
        _showConnectedError = showError;
    }
    
    void DriverSampler::setConnectStatus(Device::Status status)
    {
        if(_connected != status)
        {
            _connected = status;
        }
    }
    
    void DriverSampler::writeMessage(const char* info)
    {
        if(!hadAnotherDevices())
            Debug::writeFormatLine(info, _dd->name().c_str());
        else
        {
            Debug::writeFormatLine(info, _dd->name().c_str());
            for (uint i=0; i<_dds.count(); i++)
            {
                Debug::writeFormatLine(info, _dds[i]->name().c_str());
            }
        }
    }
}
