//
//  DriverTag.cpp
//  EVCommon
//
//  Created by baowei on 15/7/10.
//  Copyright (c) 2015 com. All rights reserved.
//

#include "DriverTag.h"
#include "common/diag/Debug.h"
#include "DriverService.h"

namespace Driver
{
    DriverTag::Transform::Transform()
    {
        type = TransformNone;
        memset(&config.linear, 0, sizeof(config.linear));
    }
    bool DriverTag::Transform::needTransform() const
    {
        return type != TransformNone;
    }
    // from device value to tag value.
    bool DriverTag::Transform::transferDValue(const double& dValue, double& tValue) const
    {
        switch (type)
        {
            case TransformNone:
                tValue = dValue;
                return true;
            case TransformLinear:
            {
                double x1 = config.linear.minValue;
                double x2 = config.linear.maxValue;
                if(x2 == x1)
                    return false;
                double y1 = config.linear.minDeviceValue;
                double y2 = config.linear.maxDeviceValue;
                double k = (y2 - y1) / (x2 - x1);
                double b = y1 - k * x1;
                double x = dValue;
                double y = k * x + b;
                tValue = y;
                return true;
            }
            case TransformRoot:
                throw NotImplementedException("TransformRoot");
                break;
            default:
                break;
        }
        return false;
    }
    // from tag value to device value.
    bool DriverTag::Transform::transferTValue(const double& tValue, double& dValue) const
    {
        switch (type)
        {
            case TransformNone:
                dValue = tValue;
                return true;
            case TransformLinear:
            {
                double x1 = config.linear.minDeviceValue;
                double x2 = config.linear.maxDeviceValue;
                if(x2 == x1)
                    return false;
                double y1 = config.linear.minValue;
                double y2 = config.linear.maxValue;
                double k = (y2 - y1) / (x2 - x1);
                double b = y1 - k * x1;
                double x = tValue;
                double y = k * x + b;
                dValue = y;
                return true;
            }
            case TransformRoot:
                throw NotImplementedException("TransformRoot");
                break;
            default:
                break;
        }
        return false;
    }
    bool DriverTag::Transform::parse(const string& str, Transform& trans)
    {
        if(String::stringEquals(str, "None", true))
        {
            trans.type = TransformNone;
            return true;
        }
        else if(String::stringEquals(str, "Root", true))
        {
            trans.type = TransformRoot;
            return true;
        }
        else
        {
            string value = "";
            Convert::KeyPairs pairs;
            if(Convert::splitItems(str, pairs))
            {
                for (uint i=0; i<pairs.count(); i++)
                {
                    const Convert::KeyPair* kp = pairs[i];
                    if(String::stringEquals(kp->name, "Linear", true))
                    {
                        StringArray texts;
                        Convert::splitStr(kp->value, ',', texts);
                        if(texts.count() == 4)
                        {
                            if(Convert::parseDouble(texts[0], trans.config.linear.minValue) &&
                               Convert::parseDouble(texts[1], trans.config.linear.maxValue) &&
                               Convert::parseDouble(texts[2], trans.config.linear.minDeviceValue) &&
                               Convert::parseDouble(texts[3], trans.config.linear.maxDeviceValue))
                            {
                                trans.type = TransformLinear;
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
    }
    
    DriverTag::DriverTag(const string& name, uint id, Tag::Type type)
    {
        _name = name;
        _id = id;
        
        _type = type;
        
        _readValue.nValue = 0;
        _initValue = true;
        
        _access = Access::AccessReadWrite;
        
        _linkedId = 0;
        
        _registerStr = "";
        _address = 0;
        _length = 0;
        _bigEndian = true;
        _property = "";
        
        _service = NULL;
        _tag = NULL;
        
        _qualitystamp = Tag::InitValue;
    }
    
    uint DriverTag::id() const
    {
        return _id;
    }
    bool DriverTag::isValid() const
    {
        return _id > 0 && Tag::isValidType(_type);
    }
    
    Tag::Type DriverTag::type() const
    {
        return _type;
    }
    
    bool DriverTag::validLinkedId() const
    {
        return _linkedId > 0;
    }
    uint DriverTag::linkedId() const
    {
        return _linkedId;
    }
    const string& DriverTag::name() const
    {
        return _name;
    }
    const string& DriverTag::registerStr() const
    {
        return _registerStr;
    }
    int DriverTag::address() const
    {
        return _address;
    }
    int DriverTag::length() const
    {
        return _length;
    }
    TimeSpan DriverTag::interval() const
    {
        return _interval;
    }
    bool DriverTag::bigEndian() const
    {
        return _bigEndian;
    }
    string DriverTag::property() const
    {
        return _property;
    }
    
    DriverService* DriverTag::service() const
    {
        return _service;
    }
    
    void DriverTag::read(XmlTextReader& reader)
    {
        uint tagId;
        if(Convert::parseUInt32(reader.getAttribute("id"), tagId))
        {
            _id = tagId;
            _name = reader.getAttribute("name");
            Tag::Type type = Tag::fromTypeStr(reader.getAttribute("type"));
            if(Tag::isValidType(type))
            {
                _type = type;
            }
            
            Convert::parseUInt32(reader.getAttribute("linkedid"), _linkedId);
            _registerStr = reader.getAttribute("register");
            Convert::parseInt32(reader.getAttribute("address"), _address);
            Convert::parseInt32(reader.getAttribute("length"), _length);
            TimeSpan::parse(reader.getAttribute("interval"), _interval);
            Convert::parseBoolean(reader.getAttribute("bigendian"), _bigEndian);
            _property = reader.getAttribute("property");
            _access = fromAccessStr(reader.getAttribute("access"));
            Transform::parse(reader.getAttribute("transform"), _transform);
        }
    }
    
    bool DriverTag::canRead() const
    {
        return _access == Access::AccessRead || _access == Access::AccessReadWrite;
    }
    bool DriverTag::canWrite() const
    {
        return _access == Access::AccessWrite || _access == Access::AccessReadWrite;
    }
    
    void DriverTag::setDbTag(Tag* tag)
    {
        _tag = tag;
    }
    
    bool DriverTag::setValue(const bool& v)
    {
        return setValue(v, Tag::Qualitystamp::Good, now());
    }
    bool DriverTag::setValue(const byte& v)
    {
        return setValue(v, Tag::Qualitystamp::Good, now());
    }
    bool DriverTag::setValue(const short& v)
    {
        return setValue(v, Tag::Qualitystamp::Good, now());
    }
    bool DriverTag::setValue(const int& v)
    {
        return setValue(v, Tag::Qualitystamp::Good, now());
    }
    bool DriverTag::setValue(const float& v)
    {
        return setValue(v, Tag::Qualitystamp::Good, now());
    }
    bool DriverTag::setValue(const int64_t& v)
    {
        return setValue(v, Tag::Qualitystamp::Good, now());
    }
    bool DriverTag::setValue(const double& v)
    {
        return setValue(v, Tag::Qualitystamp::Good, now());
    }
    
    bool DriverTag::setValue(const bool& v, Tag::Qualitystamp qualitystamp, DateTime timestamp)
    {
        if(_data != nullptr && _data->compareTimestamp)
        {
            if(_timestamp == timestamp && compareValue(v))
                return false;
        }
        else
        {
            if(compareValue(v))
                return false;
        }
        
        Tag::Value value;
        value.bValue = v;
        Tag::Value destValue;
        Tag::changeValue(Tag::Digital, value, type(), destValue);
        return setValueInner(destValue, qualitystamp, timestamp);
    }
    bool DriverTag::setValue(const byte& v, Tag::Qualitystamp qualitystamp, DateTime timestamp)
    {
        if(_data != nullptr && _data->compareTimestamp)
        {
            if(_timestamp == timestamp && compareValue(v))
                return false;
        }
        else
        {
            if(compareValue(v))
                return false;
        }
        
        Tag::Value value;
        value.sValue = v;
        Tag::Value destValue;
        Tag::changeValue(Tag::Integer16, value, type(), destValue);
        return setValueInner(destValue, qualitystamp, timestamp);
    }
    bool DriverTag::setValue(const short& v, Tag::Qualitystamp qualitystamp, DateTime timestamp)
    {
        if(_data != nullptr && _data->compareTimestamp)
        {
            if(_timestamp == timestamp && compareValue(v))
                return false;
        }
        else
        {
            if(compareValue(v))
                return false;
        }
        
        Tag::Value value;
        value.sValue = v;
        Tag::Value destValue;
        Tag::changeValue(Tag::Integer16, value, type(), destValue);
        return setValueInner(destValue, qualitystamp, timestamp);
    }
    bool DriverTag::setValue(const int& v, Tag::Qualitystamp qualitystamp, DateTime timestamp)
    {
        if(_data != nullptr && _data->compareTimestamp)
        {
            if(_timestamp == timestamp && compareValue(v))
                return false;
        }
        else
        {
            if(compareValue(v))
                return false;
        }
        
        Tag::Value value;
        value.nValue = v;
        Tag::Value destValue;
        Tag::changeValue(Tag::Integer32, value, type(), destValue);
        return setValueInner(destValue, qualitystamp, timestamp);
    }
    bool DriverTag::setValue(const float& v, Tag::Qualitystamp qualitystamp, DateTime timestamp)
    {
        if(_data != nullptr && _data->compareTimestamp)
        {
            if(_timestamp == timestamp && compareValue(v))
                return false;
        }
        else
        {
            if(compareValue(v))
                return false;
        }
        
        Tag::Value value;
        value.fValue = v;
        Tag::Value destValue;
        Tag::changeValue(Tag::Float32, value, type(), destValue);
        return setValueInner(destValue, qualitystamp, timestamp);
    }
    bool DriverTag::setValue(const int64_t& v, Tag::Qualitystamp qualitystamp, DateTime timestamp)
    {
        if(_data != nullptr && _data->compareTimestamp)
        {
            if(_timestamp == timestamp && compareValue(v))
                return false;
        }
        else
        {
            if(compareValue(v))
                return false;
        }
        
        Tag::Value value;
        value.lValue = v;
        Tag::Value destValue;
        Tag::changeValue(Tag::Integer64, value, type(), destValue);
        return setValueInner(destValue, qualitystamp, timestamp);
    }
    bool DriverTag::setValue(const double& v, Tag::Qualitystamp qualitystamp, DateTime timestamp)
    {
        if(_data != nullptr && _data->compareTimestamp)
        {
            if(_timestamp == timestamp && compareValue(v))
                return false;
        }
        else
        {
            if(compareValue(v))
                return false;
        }
        
        Tag::Value value;
        value.dValue = v;
        Tag::Value destValue;
        Tag::changeValue(Tag::Float64, value, type(), destValue);
        return setValueInner(destValue, qualitystamp, timestamp);
    }
    
    bool DriverTag::setDbValue(const Tag::Type type, const Tag::Value& v)
    {
        if(_transform.needTransform())
        {
            Tag::Value value;
            if(Tag::changeValue(type, v, this->type(), value))
            {
                double tValue = Tag::toAnalogValue(this->type(), value);
                double dValue;
                if(_transform.transferTValue(tValue, dValue))
                {
                    _writeValue = Tag::fromAnalogValue(this->type(), tValue);
                    return true;
                }
            }
        }
        else
        {
            return Tag::changeValue(type, v, this->type(), _writeValue);
        }
        return false;
    }
    bool DriverTag::setValueInner(const Tag::Value& v)
    {
        return setValueInner(v, Tag::Qualitystamp::Good, now());
    }
    bool DriverTag::setValueInner(const Tag::Value& v, Tag::Qualitystamp qualitystamp, DateTime timestamp)
    {
        if(!canRead())
            return false;
        
        _initValue = false;
        _readValue = v;
        
        _qualitystamp = qualitystamp;
        _timestamp = timestamp;
        
        if(_tag != NULL)
        {
            switch (type())
            {
                case Tag::Digital:
//                    Debug::writeFormatLine("The DriverTag value: %s", v.bValue ? "true" : "false");
                    _tag->setValue(v, qualitystamp, timestamp);
                    break;
                case Tag::Text:
//                    Debug::writeFormatLine("The DriverTag value: %s", v.strValue);
                    _tag->setValue(v, qualitystamp, timestamp);
                    break;
                default:
                {
                    double calcValue;
                    if(_transform.transferDValue(Tag::toAnalogValue(type(), v), calcValue))
                    {
//                        Debug::writeFormatLine("The DriverTag value: %s", Convert::convertStr(calcValue).c_str());
                        _tag->setValue(calcValue, qualitystamp, timestamp);
                        return true;
                    }
                }
                    break;
            }
        }
        
        valueChanged();
        
        return false;
    }
    
    bool DriverTag::getValue(byte& v) const
    {
        if(!canWrite())
            return false;
        
        return Tag::changeByteValue(type(), _writeValue, v);
    }
    bool DriverTag::getValue(short& v) const
    {
        if(!canWrite())
            return false;
        
        return Tag::changeInt16Value(type(), _writeValue, v);
    }
    bool DriverTag::getValue(int& v) const
    {
        if(!canWrite())
            return false;
        
        return Tag::changeInt32Value(type(), _writeValue, v);
    }
    bool DriverTag::getValue(float& v) const
    {
        if(!canWrite())
            return false;
        
        return Tag::changeFloat32Value(type(), _writeValue, v);
    }
    bool DriverTag::getValue(int64_t& v) const
    {
        if(!canWrite())
            return false;
        
        return Tag::changeInt64Value(type(), _writeValue, v);
    }
    bool DriverTag::getValue(double& v) const
    {
        if(!canWrite())
            return false;
        
        return Tag::changeFloat64Value(type(), _writeValue, v);
    }

    bool DriverTag::compareValue(const bool& v)
    {
        if(_initValue)
            return false;
        
        return Tag::compareValue(type(), _readValue, v);
    }
    bool DriverTag::compareValue(const byte& v)
    {
        if(_initValue)
            return false;
        
        return Tag::compareValue(type(), _readValue, (short)v);
    }
    bool DriverTag::compareValue(const short& v)
    {
        if(_initValue)
            return false;
        
        return Tag::compareValue(type(), _readValue, v);
    }
    bool DriverTag::compareValue(const int& v)
    {
        if(_initValue)
            return false;
        
        return _readValue.nValue == v;
    }
    bool DriverTag::compareValue(const float& v)
    {
        if(_initValue)
            return false;
        
        return _readValue.fValue == v;
    }
    bool DriverTag::compareValue(const int64_t& v)
    {
        if(_initValue)
            return false;
        
        return _readValue.lValue == v;
    }
    bool DriverTag::compareValue(const double& v)
    {
        if(_initValue)
            return false;
        
        return _readValue.dValue == v;
    }
    
    void DriverTag::valueChanged()
    {
        //Debug::writeLine("Tag::valueChanged");
        _valueChangedDelegates.invoke(this);
    }
    
    DateTime DriverTag::timestamp() const
    {
        return _timestamp;
    }
    Tag::Qualitystamp DriverTag::qualitystamp() const
    {
        return _qualitystamp;
    }
    
    Delegates* DriverTag::valueChangedDelegates()
    {
        return &_valueChangedDelegates;
    }
    string DriverTag::toAccessStr(DriverTag::Access value)
    {
        switch (value)
        {
            case DriverTag::Access::AccessRead:
                return "Read";
            case DriverTag::Access::AccessWrite:
                return "Write";
            case DriverTag::Access::AccessReadWrite:
                return "ReadWrite";
            default:
                break;
        }
        return "";
    }
    DriverTag::Access DriverTag::fromAccessStr(const string& value)
    {
        if(String::stringEquals(value, "Read", true))
        {
            return DriverTag::Access::AccessRead;
        }
        else if(String::stringEquals(value, "Write", true))
        {
            return DriverTag::Access::AccessWrite;
        }
        else if(String::stringEquals(value, "ReadWrite", true))
        {
            return DriverTag::Access::AccessReadWrite;
        }
        return DriverTag::Access::AccessNone;
    }
    
    DateTime DriverTag::now() const
    {
        if(_data != nullptr)
        {
            return _data->useUtcTime ? DateTime::utcNow() : DateTime::now();
        }
        return DateTime::utcNow();
    }
    
    DriverTag* DriverTags::getTag(int address) const
    {
        for (uint i=0; i<count(); i++)
        {
            DriverTag* tag = at(i);
            if(tag->address() == address)
                return tag;
        }
        return NULL;
    }
}
