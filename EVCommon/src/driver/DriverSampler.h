//
//  DriverSampler.h
//  EVCommon
//
//  Created by baowei on 15/7/21.
//  Copyright (c) 2015 com. All rights reserved.
//

#ifndef __EVCommon__DriverSampler__
#define __EVCommon__DriverSampler__

#include <string>
#include "common/common_global.h"
#include "common/xml/XmlTextReader.h"
#include "common/driver/devices/Device.h"
#include "common/driver/devices/InstructionPool.h"
#include "DriverTag.h"

using namespace std;
using namespace Common;

namespace Driver
{
    class DriverService;
    class DriverSampler : public InstructionPool
    {
    public:
        class ReadGroup
        {
        public:
            DeviceDescription* dd;
            InstructionDescriptions instructions;
            uint start;
            TimeSpan interval;
            string registerStr;
            
            ReadGroup();
            
            bool isTimeup();
        };
        class ReadGroups : public Vector<ReadGroup>
        {
        public:
            ReadGroups(bool autoDelete = true, uint capacity = Vector<ReadGroup>::DefaultCapacity) : Vector<ReadGroup>(autoDelete, capacity)
            {
            }
            
            ReadGroup* getSameGroup(DeviceDescription* dd, const string& registerStr, TimeSpan interval) const;
        };

        class WriteGroup
        {
        public:
            DeviceDescription* dd;
            InstructionDescription* instruction;
            DriverTag* tag;
            
            WriteGroup(const DriverTag* tag = NULL);
            
            bool isValid() const;
        };
        class WriteGroups : public Vector<WriteGroup>
        {
        public:
            WriteGroups(bool autoDelete = true, uint capacity = Vector<WriteGroup>::DefaultCapacity) : Vector<WriteGroup>(autoDelete, capacity)
            {
            }
            
            WriteGroup* getGroup(DeviceDescription* dd, const DriverTag* tag) const;
            WriteGroup* getGroup(const DriverTag* tag) const;
        };
        
        DriverSampler(DriverService* service, ChannelDescription* cd, DeviceDescription* dd);
        ~DriverSampler();
        
        void start() override;
        
        Device::Status getConnectStatus() const;
        bool isOnline() const;
        
        void setConnectedError(bool showError = false);
        
    private:
        void instructionProcInner() override;
        void errorHandle(Channel* channel, bool error) override;
        
        virtual void setConnectStatus(Device::Status status);
        
        void addInstruction(DeviceDescription* dd, const DriverTag* tag);
        void addInstruction(const DriverTag* tag);
        
        void addSampleInstruction();
        
        void group(DeviceDescription* dd);
        void group(const DeviceDescriptions& dds);
        void groupReadTag(DeviceDescription* dd, const DriverTag* tag);
        void groupWriteTag(DeviceDescription* dd, const DriverTag* tag);
        
        void updateLog();
        
        void writeMessage(const char* info);
        
    private:
        enum ReadWritePriority
        {
            WritePriority = 0,
            ReadPriority = 1
        };
        
    private:
        friend DriverService;
        
        ReadGroups _readGroups;
        WriteGroups _writeGroups;
        
        DriverService* _service;
        
        Device::Status _connected;
        bool _showConnectedError;
        int _checkOnlineFailedCount;
        bool _isInvalidStatus;
        int _connetedFailedCount;
        int _detetionCount;
        uint _resumeStart;
        uint _resumeInterval;
    };
}

#endif /* defined(__EVCommon__DriverSampler__) */
