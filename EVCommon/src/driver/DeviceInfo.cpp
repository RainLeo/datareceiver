//
//  DeviceInfo.cpp
//  EVCommon
//
//  Created by baowei on 15/7/20.
//  Copyright (c) 2015年 com. All rights reserved.
//

#include "DeviceInfo.h"

namespace Driver
{
    DeviceInfo::DeviceInfo()
    {
        name = "";
        address = 0;
        channelName = "";
        type = "";
        property = "";
        batchread = true;
        batchwrite = false;
        enable = false;
    }
    DeviceInfo::~DeviceInfo()
    {
    }
    void DeviceInfo::read(XmlTextReader& reader)
    {
        name = reader.getAttribute("name");
        Convert::parseInt32(reader.getAttribute("address"), address);
        channelName = reader.getAttribute("channel");
        type = reader.getAttribute("type");
        property = reader.getAttribute("property");
        Convert::parseBoolean(reader.getAttribute("batchread"), batchread);
        Convert::parseBoolean(reader.getAttribute("batchwrite"), batchwrite);
        Convert::parseBoolean(reader.getAttribute("enable"), enable);
        
        while (reader.read() && reader.nodeType() != XmlNodeType::EndElement)
        {
            if(reader.nodeType() == XmlNodeType::Element)
            {
                if(reader.name() == "tag")
                {
                    DriverTag* tag = new DriverTag();
                    tag->read(reader);
                    if(tag->isValid())
                    {
                        _tags.add(tag);
                    }
                }
                else if(reader.name() == "mock")
                {
                    Mock* mock = new Mock();
                    mock->registerStr = reader.getAttribute("register");
                    Convert::parseBoolean(reader.getAttribute("enable"), mock->enable);
                    byte value;
                    StringArray send;
                    Convert::splitStr(reader.getAttribute("send"), ' ', send);
                    for (uint i=0; i<send.count(); i++)
                    {
                        if(Convert::parseByte(send[i], value, false))
                        {
                            mock->send.add(value);
                        }
                    }
                    StringArray recv;
                    Convert::splitStr(reader.getAttribute("recv"), ' ', recv);
                    for (uint i=0; i<recv.count(); i++)
                    {
                        if(Convert::parseByte(recv[i], value, false))
                        {
                            mock->recv.add(value);
                        }
                    }
                    mocks.add(mock);
                }
                else
                {
                    break;
                }
            }
        }
    }
    
    DriverTags* DeviceInfo::tags() const
    {
        return (DriverTags*)&_tags;
    }
    
    void DeviceInfo::addTag(const DriverTag* tag)
    {
        _tags.add(tag);
    }
    
    bool DeviceInfo::hasMock() const
    {
        return mocks.count() > 0;
    }
}
