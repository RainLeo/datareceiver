#ifndef DRIVERSERVICE_H
#define DRIVERSERVICE_H

#include <string>
#include <mutex>
#include "common/common_global.h"
#include "common/thread/Thread.h"
#include "common/driver/DriverManager.h"
#include "common/driver/devices/Sampler.h"
#include "base/BaseService.h"
#include "DriverConfig.h"
#include "DriverSampler.h"

using namespace std;
using namespace Common;
using namespace EVCommon;

namespace Driver
{
    class DriverDevice;
    class COMMON_EXPORT DriverService : public BaseService
    {
    public:
        DriverService();
        ~DriverService();
        
        bool initialize(const ConfigFile* file = NULL) override;
        bool unInitialize() override;
        
        void addTagValueChangedHandler(const DriverTag* tag, const Delegate& delegate);
        void removeTagValueChangedHandler(const DriverTag* tag, const Delegate& delegate);
        
        void addTagValueChangedHandler(uint tagId, const Delegate& delegate);
        void removeTagValueChangedHandler(uint tagId, const Delegate& delegate);
        
        void addTagValueChangedHandler(const Delegate& delegate);
        void removeTagValueChangedHandler(const Delegate& delegate);
        
        DriverTag* getTag(uint tagId) const;

        string name() const override
        {
            return "driver";
        }
        
        bool allowLog() const;
        
    private:
        bool createDevices();
        bool createDevice(const DeviceInfo* di);
        const ChannelInfo* getChannelInfo(const string& channelName) const;
        
        DriverDevice* createDevice(const DeviceInfo* di) const;
        InstructionSet* createInstructionSet(const DeviceInfo* di) const;
        DeviceContext* createDeviceContext(const DeviceInfo* di) const;
        Sampler* createSampler(const DeviceInfo* di, ChannelDescription* cd, DeviceDescription* dd);
        void updateChannelContext(const ChannelInfo* ci, ChannelContext* cc);
        void updateDeviceDescription(const ChannelInfo* ci, const DeviceInfo* di, DeviceDescription* dd);
        
        DriverTags* getTags(const string& deviceName) const;
        
        DriverManager* manager();
        
        void writeDriverTag(const DriverTag* tag);
        
        friend DriverTag;
        friend DriverConfig;
        friend DriverSampler;
        friend void driver_tagValueChanged(void* owner, void* sender, EventArgs* args);
        friend void driver_openStartAction(void* parameter);

    private:
        DriverManager* _manager;
        Thread* _openThread;
        
        DriverData _data;
    };
}

#endif	// DRIVERSERVICE_H
