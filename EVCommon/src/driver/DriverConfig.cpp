#include "DriverConfig.h"
#include "common/system/TimeSpan.h"
#include "common/IO/Directory.h"
#include "common/IO/File.h"
#include "common/IO/Path.h"
#include "common/system/Convert.h"
#include "common/xml/XmlTextReader.h"
#include "common/xml/XmlDocument.h"
#include "DriverTag.h"
#include "DriverService.h"

using namespace Common;
using namespace rtdb;

namespace Driver
{
    DriverData::DriverData()
    {
        allowLog = true;
        useUtcTime = true;
        compareTimestamp = false;
    }
//    DriverData::DriverData(const DriverData& data)
//    {
//        this->operator=(data);
//    }
//    void DriverData::operator=(const DriverData& value)
//    {
//        this->allowLog = value.allowLog;
//        this->useUtcTime = value.useUtcTime;
//        this->compareTimestamp = value.compareTimestamp;
//    }
//    bool DriverData::operator==(const DriverData& value) const
//    {
//        return this->allowLog == value.allowLog &&
//            this->useUtcTime == value.useUtcTime &&
//            this->compareTimestamp == value.compareTimestamp;
//    }
//    bool DriverData::operator!=(const DriverData& value) const
//    {
//        return !operator==(value);
//    }
    
    DriverConfig::DriverConfig(const ConfigFile& file, DriverData* data) : Configuration(file)
    {
        if(data == NULL)
            throw ArgumentException("data");
        
        _data = data;
    }
    
    DriverConfig::~DriverConfig()
    {
        _data = NULL;
    }
    
    bool DriverConfig::load(XmlTextReader& reader)
    {
        if (reader.nodeType() == XmlNodeType::Element &&
            reader.localName() == "driver")
        {
            Convert::parseBoolean(reader.getAttribute("allowlog"), _data->allowLog);
            Convert::parseBoolean(reader.getAttribute("useUtcTime"), _data->useUtcTime);
            Convert::parseBoolean(reader.getAttribute("compareTimestamp"), _data->compareTimestamp);
            
            while (reader.read())
            {
                if (reader.nodeType() == XmlNodeType::Element &&
                    reader.localName() == "channel")
                {
                    ChannelInfo* ci = new ChannelInfo();
                    ci->read(reader);
                    _data->channels.add(ci);
                }
                else if (reader.nodeType() == XmlNodeType::Element &&
                    reader.localName() == "device")
                {
                    DeviceInfo* di = new DeviceInfo();
                    di->read(reader);
                    _data->devices.add(di);
                }
            }
            return true;
        }
        return false;
    }
}
