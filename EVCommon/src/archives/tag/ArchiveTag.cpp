//
//  ArchiveTag.cpp
//  EVCommon
//
//  Created by baowei on 15/7/10.
//  Copyright (c) 2015年 com. All rights reserved.
//

#include "ArchiveTag.h"

namespace Archives
{
    ArchiveTag::ArchiveTag(uint id, Tag::Type type)
    {
        _id = id;
        
        _type = type;
        _excDev = 1.0f;
        _excDevPercent = 10;
        _excMin = 600;
        _excMax = 0;
        _timeResolution = DateTime::Resolutions::ResSecond;
        
        _linkedId = 0;
        
        _service = NULL;
    }
    
    uint ArchiveTag::id() const
    {
        return _id;
    }
    
    Tag::Type ArchiveTag::type() const
    {
        return _type;
    }
    void ArchiveTag::setType(Tag::Type type)
    {
        _type = type;
    }
    
    float ArchiveTag::excDev() const
    {
        if (_excDevPercent > 0)
        {
        }
        return _excDev;
    }
    
    ushort ArchiveTag::excDevPercent() const
    {
        return _excDevPercent;
    }
    
    /// <summary>
    /// The Exception Minimum attribute, It is specified in seconds
    /// </summary>
    uint ArchiveTag::excMin() const
    {
        return _excMin;
    }
    
    /// <summary>
    /// The Exception Maximum attribute, It is specified in seconds
    /// </summary>
    ushort ArchiveTag::excMax() const
    {
        return _excMax;
    }
    
    bool ArchiveTag::enableExcTest() const
    {
        return _excMin == 0 && _excMax == 0;
    }
    
    DateTime::Resolutions ArchiveTag::timeResolutions() const
    {
        return _timeResolution;
    }
    void ArchiveTag::setTimeResolutions(DateTime::Resolutions tr)
    {
        _timeResolution = tr;
    }
    
    bool ArchiveTag::validLinkedId() const
    {
        return _linkedId > 0;
    }
    uint ArchiveTag::linkedId() const
    {
        return _linkedId;
    }
    void ArchiveTag::setLinkedId(uint linkedId)
    {
        _linkedId = linkedId;
    }
    ArchiveService* ArchiveTag::service() const
    {
        return _service;
    }
    
    double ArchiveTag::toAnalogValue(Tag::Type type, const Tag::Value& value)
    {
        return Tag::toAnalogValue(type, value);
    }
    Tag::Value ArchiveTag::fromAnalogValue(Tag::Type type, double value)
    {
        return Tag::fromAnalogValue(type, value);
    }
    
    void ArchiveTag::writeValue(Stream* stream, Tag::Type type, const Tag::Value& value)
    {
        Tag::writeValue(stream, type, value);
    }
    void ArchiveTag::readValue(Stream* stream, Tag::Type type, Tag::Value& value)
    {
        Tag::readValue(stream, type, value);
    }
}
