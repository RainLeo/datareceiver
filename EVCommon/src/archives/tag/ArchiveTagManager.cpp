//
//  TagManager.cpp
//  EVCommon
//
//  Created by baowei on 15/7/10.
//  Copyright (c) 2015年 com. All rights reserved.
//

#include "ArchiveTagManager.h"

namespace Archives
{
    ArchiveTagManager::ArchiveTagManager(ArchiveService* as)
    {
        if(as == NULL)
            throw ArgumentException("as");
        
        _service = as;
        
        _tags.setCapacity(4096);
    }
    
    bool ArchiveTagManager::initialize(const ConfigFile* file)
    {
        return true;
    }
    bool ArchiveTagManager::unInitialize()
    {
        return true;
    }
    
	void ArchiveTagManager::addTag(const ArchiveTag* tag)
    {
        _tags.addTag(tag);
    }
    uint ArchiveTagManager::tagCount() const
    {
        return _tags.count();
    }
    
	const ArchiveTags& ArchiveTagManager::allTags() const
    {
        return _tags;
    }
    
    Tag::Type ArchiveTagManager::getTagType(uint tagId) const
    {
        Tag::Type type = Tag::Type::Null;
		const ArchiveTag* tag = getTag(tagId);
        if(tag != NULL)
            return tag->type();
        
        return type;
    }
    
	const ArchiveTag* ArchiveTagManager::getTag(uint tagId) const
    {
        return _tags.getTag(tagId);
    }
}