//
//  Tag.h
//  EVCommon
//
//  Created by baowei on 15/7/10.
//  Copyright (c) 2015 com. All rights reserved.
//

#ifndef __EVCommon__Tag__
#define __EVCommon__Tag__

#include <string>
#include <mutex>
#include "common/common_global.h"
#include "common/system/DateTime.h"
#include "common/data/Vector.h"
#include "common/system/Delegate.h"
#include "common/system/Convert.h"
#include "common/thread/Locker.h"
#include "tag/Tag.h"

using namespace std;
using namespace Common;
using namespace rtdb;

namespace Archives
{
    class ArchiveService;
	class ArchiveTag
    {
    public:
        ArchiveTag(uint id = 0, Tag::Type type = Tag::Type::Null);
        
        uint id() const;
        
        Tag::Type type() const;
        void setType(Tag::Type type);
        
        float excDev() const;
        
        ushort excDevPercent() const;
        
        /// <summary>
        /// The Exception Minimum attribute, It is specified in seconds
        /// </summary>
        uint excMin() const;
        
        /// <summary>
        /// The Exception Maximum attribute, It is specified in seconds
        /// </summary>
        ushort excMax() const;
        
        bool enableExcTest() const;
        
        DateTime::Resolutions timeResolutions() const;
        void setTimeResolutions(DateTime::Resolutions tr);
        
        bool validLinkedId() const;
        uint linkedId() const;
        void setLinkedId(uint linkedId);
        
        ArchiveService* service() const;
        
    public:
        static double toAnalogValue(Tag::Type type, const Tag::Value& value);
        static Tag::Value fromAnalogValue(Tag::Type type, double value);
        
        static void writeValue(Stream* stream, Tag::Type type, const Tag::Value& value);
        static void readValue(Stream* stream, Tag::Type type, Tag::Value& value);
        
    private:
        uint _id;
        Tag::Type _type;
        
        float _excDev;
        ushort _excDevPercent;
        uint _excMin;
        ushort _excMax;
        DateTime::Resolutions _timeResolution;
        
        uint _linkedId;
        
        friend ArchiveService;
        ArchiveService* _service;
    };
    
	class COMMON_EXPORT ArchiveTags : public Vector<ArchiveTag>
    {
    public:
		ArchiveTags(bool autoDelete = true, uint capacity = Vector<ArchiveTag>::DefaultCapacity) : Vector<ArchiveTag>(autoDelete, capacity)
        {
        }
        
		inline void addTag(const ArchiveTag* tag)
        {
            uint tagId = tag->id();
            if(tagId > 0)
                Vector<ArchiveTag>::set(tagId - 1, tag, true);
        }
		inline ArchiveTag* getTag(uint tagId) const
        {
            if(tagId > 0 && tagId <= count())
                return at(tagId - 1);
            
            return NULL;
        }
        inline bool containsTag(uint tagId) const
        {
            return getTag(tagId) != NULL;
        }
        inline Tag::Type getTagType(uint tagId) const
        {
            const ArchiveTag* tag = getTag(tagId);
            return tag != NULL ? tag->type() : Tag::Type::Null;
        }
    };
}

#endif /* defined(__EVCommon__Tag__) */
