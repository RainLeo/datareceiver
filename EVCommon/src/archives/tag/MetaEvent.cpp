//
//  MetaEvent.cpp
//  EVCommon
//
//  Created by baowei on 15/7/4.
//  Copyright (c) 2015 com. All rights reserved.
//

#include "MetaEvent.h"
#include "common/exception/Exception.h"
#include "../utils/DataExtensions.h"

namespace Archives
{
    MetaEvent::MetaEvent()
    {
        this->tagId = 0;
        this->type = Tag::Type::Null;
        this->value.nValue = 0;
        this->qualitystamp = Tag::Qualitystamp::InitValue;
    }
    MetaEvent::MetaEvent(uint tagId, Tag::Type type, Tag::Value value, DateTime timestamp, Tag::Qualitystamp qualitystamp)
    {
        // todo: can not archive String tag.
        assert(type != Tag::Type::Text);
        
        this->tagId = tagId;
        this->type = type;
        this->value = value;
        this->timestamp = timestamp;
        this->qualitystamp = qualitystamp;
    }
    
    bool MetaEvent::isValid() const
    {
        return tagId > 0;
    }
    bool MetaEvent::isAnalogValue() const
    {
        return Tag::isAnalogValue(type);
    }
    string MetaEvent::toString(TimeZone tz) const
    {
        string str = "";
        switch (type)
        {
            case Tag::Type::Null:
                break;
            case Tag::Type::Digital:
                str = Convert::convertStr(value.dValue);
                break;
            case Tag::Type::Integer16:
                str = Convert::convertStr(value.sValue);
                break;
            case Tag::Type::Integer32:
                str = Convert::convertStr(value.nValue);
                break;
            case Tag::Type::Integer64:
                str = Convert::convertStr(value.lValue);
                break;
            case Tag::Type::Float32:
                str = Convert::convertStr(value.fValue);
                break;
            case Tag::Type::Float64:
                str = Convert::convertStr(value.dValue);
                break;
            case Tag::Type::Text:
                str = (value.strValue);
                break;
            case Tag::Type::Date:
            {
                DateTime t = DateTime(value.tValue);
                str = t.toString();
            }
                break;
            default:
                break;
                
        }
        
        DateTime time = tz == TimeZone::Zero ? timestamp : tz.toLocalTime(timestamp);
        return Convert::convertStr("TagId = %d, Type = %s, Value = %s, Time = %s, Qualitystamp = %d",
                                   tagId, Tag::toTypeStr(type).c_str(), str.c_str(), time.toString().c_str(), qualitystamp);
    }
    
    bool MetaEvent::binarySearch(const MetaEvents& mes, DateTime startTime, DateTime endTime, MetaEvents& result)
    {
        return binarySearch(mes, 0, mes.count(), startTime, endTime, result);
    }
    
    bool MetaEvent::binarySearch(const MetaEvents& mes, int index, int length, DateTime startTime, DateTime endTime, MetaEvents& result)
    {
        checkArgumentMes(mes, index, length);
        
        if (startTime > endTime)
        {
            throw ArgumentOutOfRangeException("startTime", "endTime must be greater than or equal to startTime.");
        }
        
        int start = binarySearch(mes, index, length, startTime);
        int end = binarySearch(mes, index, length, endTime);
        start = start < 0 ? ~start : start;
        end = end < 0 ? ((~end) - 1) : end;
        if (start >= index && start <= index + length &&
            end >= index && end <= index + length &&
            end >= start)
        {
            result.addRange(&mes, start, end - start + 1);
            return true;
        }
        return false;
    }
    
    int MetaEvent::binarySearch(const MetaEvents& mes, DateTime time)
    {
        return binarySearch(mes, 0, mes.count(), time);
    }
    
    int MetaEvent::binarySearch(const MetaEvents& mes, int index, int length, DateTime time)
    {
        checkArgumentMes(mes, index, length);
        
        int low = index;
        int hi = (index + length) - 1;
        while (low <= hi)
        {
            int median = getMedian(low, hi);
            DateTime medianTime = mes[median].timestamp;
            DateTime prevTime = median - 1 >= index ? mes[median - 1].timestamp : DateTime::MinValue;
            //DateTime nextTime = median + 1 <= length - 1 ? mes[median + 1].TimeStamp : DateTime::MinValue;
            
            if (medianTime == time ||
                (time > prevTime && medianTime > time))// ||
                //(time > medianTime && nextTime > time))
            {
                return median;
            }
            else if (medianTime < time)
            {
                low = median + 1;
            }
            else
            {
                hi = median - 1;
            }
        }
        return ~low;
    }
    
    void MetaEvent::checkArgumentMes(const MetaEvents& mes, int index, int length)
    {
        if (mes.count() == 0)
        {
            throw ArgumentNullException("mes");
        }
        if (!(index >= 0 && (uint)index <= mes.count() - 1))
        {
            throw ArgumentOutOfRangeException("index", Convert::convertStr("index must be between %d and %d.", 0, mes.count() - 1).c_str());
        }
        if (!(length > 0 && (uint)length <= mes.count()))
        {
            throw ArgumentOutOfRangeException("length", Convert::convertStr("length must be between %d and %d.", 0, mes.count()).c_str());
        }
    }
    
    int MetaEvent::getMedian(int low, int hi)
    {
        return (low + ((hi - low) >> 1));
    }
    
    MetaEvent MetaEvent::find(const MetaEvents& mes, int index, int length, const MetaEvent& lastEvent, DateTime t, RetrievalModes mode, bool& finished)
    {
        return find(mes, index, length, lastEvent, mes[index].timestamp, mes[index + length - 1].timestamp, t, mode, finished);
    }
    
    MetaEvent MetaEvent::find(const MetaEvents& mes, const MetaEvent& lastEvent, DateTime t, RetrievalModes mode, bool& finished)
    {
        return find(mes, 0, mes.count(), lastEvent, mes[0].timestamp, mes[mes.count() - 1].timestamp, t, mode, finished);
    }
    
    MetaEvent MetaEvent::find(const MetaEvents& mes, const MetaEvent& lastEvent, DateTime st, DateTime et, DateTime t, RetrievalModes mode, bool& finished)
    {
        return find(mes, 0, mes.count(), lastEvent, st, et, t, mode, finished);
    }
    
    MetaEvent MetaEvent::find(const MetaEvents& mes, int index, int length, const MetaEvent& lastEvent,
                              DateTime st, DateTime et, DateTime t, RetrievalModes mode, bool& finished)
    {
        checkArgumentMes(mes, index, length);
        
        finished = false;
        MetaEvent me;
        switch (mode)
        {
            case RetrievalModes::RMAuto:
                if (t > et)
                {
                    MetaEvent me1 = mes[index + length - 1];
                    MetaEvent me2 = lastEvent;
                    me = getInterpolatedMetaValue(t, me1, me2);
                    finished = true;
                }
                else if (t >= st && t <= et)
                {
                    int pos = binarySearch(mes, index, length, t);
                    assert(pos >= index);
                    MetaEvent current = mes[pos];
                    if (current.timestamp == t)
                    {
                        me = current;
                        finished = true;
                    }
                    else
                    {
                        if (pos > index)
                        {
                            MetaEvent me1 = mes[pos - 1];
                            MetaEvent me2 = mes[pos];
                            me = getInterpolatedMetaValue(t, me1, me2);
                            finished = true;
                        }
                    }
                }
                break;
            case RetrievalModes::RMPrevious:
                if (t > et)
                {
                    me = mes[index + length - 1];
                    finished = true;
                }
                else if (t >= st && t <= et)
                {
                    int pos = binarySearch(mes, index, length, t);
                    assert(pos >= index);
                    MetaEvent current = mes[pos];
                    if (current.timestamp == t)
                    {
                        me = current;
                    }
                    else
                    {
                        assert(me.timestamp > t);
                        assert(pos > index);
                        me = mes[pos - 1];
                    }
                    finished = true;
                }
                break;
            case RetrievalModes::RMPreviousOnly:
                if (t > et)
                {
                    me = mes[index + length - 1];
                    finished = true;
                }
                else if (t >= st && t <= et)
                {
                    int pos = binarySearch(mes, index, length, t);
                    if (pos > index)
                    {
                        me = mes[pos - 1];
                        finished = true;
                    }
                }
                break;
            case RetrievalModes::RMInterpolated:
                if (t > et)
                {
                    MetaEvent me1 = mes[index + length - 1];
                    MetaEvent me2 = lastEvent;
                    me = getInterpolatedMetaValue(t, me1, me2);
                    finished = true;
                }
                else if (t >= st && t <= et)
                {
                    int pos = binarySearch(mes, index, length, t);
                    if (pos > index)
                    {
                        MetaEvent me1 = mes[pos - 1];
                        MetaEvent me2 = mes[pos];
                        me = getInterpolatedMetaValue(t, me1, me2);
                        finished = true;
                    }
                }
                break;
            case RetrievalModes::RMNext:
                if (t > et)
                {
                    me = lastEvent;
                    finished = true;
                }
                else if (t >= st && t <= et)
                {
                    int pos = binarySearch(mes, index, length, t);
                    assert(pos >= index);
                    me = mes[pos];
                    finished = true;
                }
                break;
            case RetrievalModes::RMNextOnly:
                if (t > et)
                {
                    me = lastEvent;
                    finished = true;
                }
                else if (t >= st && t <= et)
                {
                    int pos = binarySearch(mes, index, length, t);
                    assert(pos >= index);
                    MetaEvent current = mes[pos];
                    if (current.timestamp == t)
                    {
                        me = mes[pos + 1];
                    }
                    else
                    {
                        assert(current.timestamp > t);
                        assert(pos > index);
                        me = current;
                    }
                    finished = true;
                }
                break;
            case RetrievalModes::RMExtractTime:
                if (t > et)
                {
                    finished = true;
                }
                else if (t >= st && t <= et)
                {
                    int pos = binarySearch(mes, index, length, t);
                    assert(pos >= index);
                    MetaEvent current = mes[pos];
                    if (current.timestamp == t)
                    {
                        me = current;
                    }
                    finished = true;
                }
                break;
            default:
                break;
        }
        return me;
    }
    
    MetaEvent MetaEvent::getInterpolatedMetaValue(DateTime t, const MetaEvent& me1, const MetaEvent& me2)
    {
        if (me1.type == me2.type && me1.isAnalogValue() && me1.timestamp != me2.timestamp)
        {
            double x1 = (me1.timestamp - DataExtensions::MinTime).totalMilliseconds();
            double x2 = (me2.timestamp - DataExtensions::MinTime).totalMilliseconds();
            double y1 = Tag::toAnalogValue(me1.type, me1.value);
            double y2 = Tag::toAnalogValue(me2.type, me2.value);
            double k = (y2 - y1) / (x2 - x1);
            double b = y1 - k * x1;
            double x = (t - DataExtensions::MinTime).totalMilliseconds();
            double y = k * x + b;
            // ToDo: QualityStamp need to process.
            return MetaEvent(me1.tagId, me1.type, Tag::fromAnalogValue(me1.type, y), t, me1.qualitystamp);
        }
        return MetaEvent();
    }
    
    MetaEvent MetaEvent::find(const MetaEvent& me, DateTime t, RetrievalModes mode, bool& finished)
    {
        finished = false;
        MetaEvent result;
        switch (mode)
        {
            case RetrievalModes::RMAuto:
            case RetrievalModes::RMPrevious:
            case RetrievalModes::RMNext:
                if (t >= me.timestamp)
                {
                    result = me;
                    finished = true;
                }
                break;
            case RetrievalModes::RMPreviousOnly:
                if (t > me.timestamp)
                {
                    result = me;
                    finished = true;
                }
                break;
            case RetrievalModes::RMNextOnly:
                if (t >= me.timestamp)
                {
                    finished = true;
                }
                break;
            case RetrievalModes::RMInterpolated:
                if (t > me.timestamp)
                {
                    finished = true;
                }
                break;
            case RetrievalModes::RMExtractTime:
                if (t == me.timestamp)
                {
                    result = me;
                    finished = true;
                }
                else if (t > me.timestamp)
                {
                    finished = true;
                }
                break;
            default:
                break;
        }
        return result;
    }
    
    MetaEvent MetaEvent::find(const MetaEvent& me, DateTime startTime, DateTime endTime, BoundaryTypes type, bool& finished)
    {
        finished = false;
        MetaEvent result;
        switch (type)
        {
            case BoundaryTypes::BTInside:
            case BoundaryTypes::BTInterpolated:
                if (me.timestamp >= startTime && me.timestamp <= endTime)
                {
                    result = me;
                    if (me.timestamp == startTime)
                    {
                        finished = true;
                    }
                }
                else if (me.timestamp < startTime)
                {
                    finished = true;
                }
                break;
            case BoundaryTypes::BTOutside:
                if (me.timestamp >= startTime && me.timestamp <= endTime)
                {
                    result = me;
                    if (me.timestamp == startTime)
                    {
                        finished = true;
                    }
                }
                else if (me.timestamp < startTime)
                {
                    result = me;
                    finished = true;
                }
                break;
            default:
                break;
        }
        return result;
    }
    
    MetaEvent MetaEvent::find(const MetaEvent& me, DateTime startTime, int numberOfValues, BoundaryTypes type, bool& finished)
    {
        finished = false;
        MetaEvent result;
        switch (type)
        {
            case BoundaryTypes::BTInside:
            case BoundaryTypes::BTInterpolated:
                if (numberOfValues > 0)
                {
                    if (startTime > me.timestamp)
                    {
                        finished = true;
                    }
                    else if (startTime == me.timestamp)
                    {
                        result = me;
                        finished = true;
                    }
                }
                else
                {
                    assert(numberOfValues < 0);
                    if (startTime >= me.timestamp)
                    {
                        result = me;
                        if (1 == numberOfValues)
                        {
                            finished = true;
                        }
                    }
                }
                break;
            case BoundaryTypes::BTOutside:
                if (numberOfValues > 0)
                {
                    if (startTime >= me.timestamp)
                    {
                        result = me;
                        finished = true;
                    }
                }
                else
                {
                    assert(numberOfValues < 0);
                    if (startTime >= me.timestamp)
                    {
                        result = me;
                        if (1 == numberOfValues)
                        {
                            finished = true;
                        }
                    }
                }
                break;
            default:
                break;
        }
        return result;
    }
    
    bool MetaEvent::find(const MetaEvents& mes, const MetaEvent& lastEvent, DateTime startTime, DateTime endTime,
                         BoundaryTypes type, bool& finished, MetaEvents& result)
    {
        return find(mes, 0, mes.count(), lastEvent, startTime, endTime, type, finished, result);
    }
    
    bool MetaEvent::find(const MetaEvents& mes, int index, int length, const MetaEvent& lastEvent, DateTime startTime, DateTime endTime,
                         BoundaryTypes type, bool &finished, MetaEvents& result)
    {
        return find(mes, 0, mes.count(), lastEvent, startTime, endTime, mes[index].timestamp, mes[index + length - 1].timestamp, type, finished, result);
    }
    
    bool MetaEvent::find(const MetaEvents& mes, int index, int length, const MetaEvent& lastEvent, DateTime startTime, DateTime endTime,
                         DateTime st, DateTime et, BoundaryTypes type, bool& finished, MetaEvents& result)
    {
        checkArgumentMes(mes, index, length);
        
        finished = false;
        switch (type)
        {
            case BoundaryTypes::BTInside:
                if (endTime < st)       // R1
                {
                }
                else if (startTime < st && endTime > st && endTime < et)       //  R2
                {
                    binarySearch(mes, st, endTime, result);
                }
                else if (startTime < st && endTime > et)     // R3
                {
                    binarySearch(mes, st, et, result);
                }
                else if (startTime >= st && startTime <= et && endTime >= st && endTime <= et)       // R4
                {
                    binarySearch(mes, startTime, endTime, result);
                    finished = true;
                }
                else if (startTime > st && startTime < et && endTime > et)     // R5
                {
                    binarySearch(mes, startTime, et, result);
                    finished = true;
                }
                else if (startTime > et)        // R6
                {
                    finished = true;
                }
                else
                {
                    assert(false);
                }
                break;
            case BoundaryTypes::BTOutside:
                if (endTime < st)       // R1
                {
                }
                else if (startTime < st && endTime > st && endTime < et)       //  R2
                {
                    binarySearch(mes, st, endTime, result);
                }
                else if (startTime < st && endTime > et)     // R3
                {
                    binarySearch(mes, st, et, result);
                }
                else if (startTime >= st && startTime <= et && endTime >= st && endTime <= et)       // R4
                {
                    binarySearch(mes, startTime, endTime, result);
                    finished = true;
                }
                else if (startTime > st && startTime < et && endTime > et)     // R5
                {
                    binarySearch(mes, startTime, et, result);
                    finished = true;
                }
                else if (startTime > et)        // R6
                {
                    result.add(mes[index + length - 1]);
                    finished = true;
                }
                else
                {
                    assert(false);
                }
                break;
            case BoundaryTypes::BTInterpolated:
                if (endTime < st)       // R1
                {
                }
                else if (startTime < st && endTime > st && endTime < et)       //  R2
                {
                    binarySearch(mes, st, endTime, result);
                }
                else if (startTime < st && endTime > et)     // R3
                {
                    binarySearch(mes, st, et, result);
                }
                else if (startTime >= st && startTime <= et && endTime >= st && endTime <= et)       // R4
                {
                    binarySearch(mes, startTime, endTime, result);
                    finished = true;
                }
                else if (startTime > st && startTime < et && endTime > et)     // R5
                {
                    binarySearch(mes, startTime, et, result);
                    
                    MetaEvent me1 = mes[index + length - 1];
                    MetaEvent me2 = lastEvent;
                    
                    assert(endTime > me1.timestamp && endTime < me2.timestamp);
                    MetaEvent endme = getInterpolatedMetaValue(endTime, me1, me2);
                    result.add(endme);
                    finished = true;
                }
                else if (startTime > et)        // R6
                {
                    MetaEvent me1 = mes[index + length - 1];
                    MetaEvent me2 = lastEvent;
                    
                    assert(startTime > me1.timestamp && startTime < me2.timestamp);
                    assert(endTime > me1.timestamp && endTime < me2.timestamp);
                    
                    MetaEvent startme = getInterpolatedMetaValue(startTime, me1, me2);
                    MetaEvent endme = getInterpolatedMetaValue(endTime, me1, me2);
                    result.add(startme);
                    result.add(endme);
                    finished = true;
                }
                else
                {
                    assert(false);
                }
                break;
            default:
                break;
        }
        return result.count() > 0;
    }
}
