//
//  TagManager.h
//  EVCommon
//
//  Created by baowei on 15/7/10.
//  Copyright (c) 2015年 com. All rights reserved.
//

#ifndef __EVCommon__ArchiveTagManager__
#define __EVCommon__ArchiveTagManager__

#include "ArchiveTag.h"
#include "common/xml/ConfigFile.h"

namespace Archives
{
    class ArchiveService;
    class ArchiveTagManager
    {
    public:
        ArchiveTagManager(ArchiveService* as);
        
		void addTag(const ArchiveTag* tag);
        
		const ArchiveTags& allTags() const;
        uint tagCount() const;
        
        Tag::Type getTagType(uint tagId) const;
        
		const ArchiveTag* getTag(uint tagId) const;
        
    private:
        bool initialize(const ConfigFile* file);
        bool unInitialize();
        
    private:
        friend ArchiveService;
        
    private:
		ArchiveTags _tags;
        ArchiveService* _service;
    };
}

#endif /* defined(__EVCommon__ArchiveTagManager__) */
