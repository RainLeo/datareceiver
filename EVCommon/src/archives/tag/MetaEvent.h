//
//  MetaEvent.h
//  EVCommon
//
//  Created by baowei on 15/7/4.
//  Copyright (c) 2015 com. All rights reserved.
//

#ifndef METAEVENT_H
#define METAEVENT_H

#include <string>
#include <mutex>
#include "common/common_global.h"
#include "common/system/DateTime.h"
#include "common/data/Vector.h"
#include "common/data/Array.h"
#include "common/data/LoopArray.h"
#include "common/system/Convert.h"
#include "common/system/TimeZone.h"
#include "common/thread/Locker.h"
#include "tag/Tag.h"
#include "../archive/Enums.h"

using namespace std;
using namespace Common;
using namespace rtdb;

namespace Archives
{
    class MetaEvents;
    struct MetaEvent
    {
    public:
        uint tagId;
        Tag::Type type;
        Tag::Value value;
        DateTime timestamp;
        Tag::Qualitystamp qualitystamp;
        
    public:
        MetaEvent();
        MetaEvent(uint tagId, Tag::Type type, Tag::Value value, DateTime timestamp, Tag::Qualitystamp quality);
        
        bool isValid() const;
        bool isAnalogValue() const;
        
        string toString(TimeZone tz = TimeZone::Zero) const;
        
    public:
        static bool binarySearch(const MetaEvents& mes, DateTime startTime, DateTime endTime, MetaEvents& result);
        static bool binarySearch(const MetaEvents& mes, int index, int length, DateTime startTime, DateTime endTime, MetaEvents& result);
        
        static MetaEvent find(const MetaEvents& mes, int index, int length, const MetaEvent& lastEvent, DateTime t, RetrievalModes mode, bool& finished);
        static MetaEvent find(const MetaEvents& mes, const MetaEvent& lastEvent, DateTime t, RetrievalModes mode, bool& finished);
        static MetaEvent find(const MetaEvents& mes, const MetaEvent& lastEvent, DateTime st, DateTime et, DateTime t, RetrievalModes mode, bool& finished);
        static MetaEvent find(const MetaEvents& mes, int index, int length, const MetaEvent& lastEvent,
                              DateTime st, DateTime et, DateTime t, RetrievalModes mode, bool& finished);
        static MetaEvent find(const MetaEvent& me, DateTime t, RetrievalModes mode, bool& finished);
        static MetaEvent find(const MetaEvent& me, DateTime startTime, DateTime endTime, BoundaryTypes type, bool& finished);
        static MetaEvent find(const MetaEvent& me, DateTime startTime, int numberOfValues, BoundaryTypes type, bool& finished);
        static bool find(const MetaEvents& mes, const MetaEvent& lastEvent, DateTime startTime, DateTime endTime,
                         BoundaryTypes type, bool& finished, MetaEvents& result);
        static bool find(const MetaEvents& mes, int index, int length, const MetaEvent& lastEvent, DateTime startTime, DateTime endTime,
                         BoundaryTypes type, bool &finished, MetaEvents& result);
        static bool find(const MetaEvents& mes, int index, int length, const MetaEvent& lastEvent, DateTime startTime, DateTime endTime,
                         DateTime st, DateTime et, BoundaryTypes type, bool& finished, MetaEvents& result);
        
    private:
        static int binarySearch(const MetaEvents& mes, DateTime time);
        static int binarySearch(const MetaEvents& mes, int index, int length, DateTime time);
        static void checkArgumentMes(const MetaEvents& mes, int index, int length);
        static int getMedian(int low, int hi);
        static MetaEvent getInterpolatedMetaValue(DateTime t, const MetaEvent& me1, const MetaEvent& me2);
    };
    
    class MetaEvents : public Array<MetaEvent>
    {
    public:
        MetaEvents(uint capacity = Array<MetaEvent>::DefaultCapacity) : Array<MetaEvent>(capacity)
        {
        }
    };
    
    class LoopMetaEvents : public LoopArray<MetaEvent>
    {
    public:
        LoopMetaEvents(int maxLength = LoopArray<MetaEvent>::DefaultMaxLength) : LoopArray<MetaEvent>(maxLength)
        {
        }
        
        void copyTo(MetaEvents* mes) const
        {
            uint c = count();
            MetaEvent* temp = new MetaEvent[c];
            LoopArray<MetaEvent>::copyTo(temp);
            mes->addRange(temp, c);
            delete[] temp;
        }
    };
    
    class LoopMetaEventss : public Vector<LoopMetaEvents>
    {
    public:
        LoopMetaEventss(bool autoDelete = true, uint capacity = Vector<LoopMetaEvents>::DefaultCapacity) : Vector<LoopMetaEvents>(autoDelete, capacity)
        {
        }
    };
}

#endif /* defined(METAEVENT_H) */
