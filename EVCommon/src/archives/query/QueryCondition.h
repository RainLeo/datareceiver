//
//  QueryCondition.h
//  EVCommon
//
//  Created by baowei on 15/7/7.
//  Copyright (c) 2015年 com. All rights reserved.
//

#ifndef __EVCommon__QueryCondition__
#define __EVCommon__QueryCondition__

#include "common/IO/Stream.h"
#include "common/system/DateTime.h"
#include "common/system/Version.h"
#include "common/exception/Exception.h"
#include "../utils/DataExtensions.h"
#include "../tag/MetaEvent.h"
#include "QueryResult.h"

using namespace Common;

namespace Archives
{
    struct QueryCondition
    {
    public:
        uint tagId;
        DateTime startTime;
        DateTime endTime;
        /// <summary>
        /// The number of desired values (negative if counting backward in time)
        /// </summary>
        int numberOfValues;
        TimeSpan interval;
        RetrievalModes retrievalMode;
        BoundaryTypes boundaryType;
        
    public:
        QueryCondition(uint tagId = 0);
        QueryCondition(uint tagId, DateTime startTime, DateTime endTime, TimeSpan interval = TimeSpan::Zero);
        
        string tagName() const;
        void setTagName(uint tagId);
        
        DateTime timestamp() const;
        void setTimestamp(DateTime time);
        
        bool specifiedTimeValue() const;
        
        bool isQueryAll() const;
        
        bool hasTimeRange() const;
        
        bool hasInterval() const;
        
        bool hasNumberOfValues() const;
        
        bool sampleTimes(Array<DateTime>& times) const;
        
        bool isEmpty() const;
        
    public:
        static QueryCondition Empty;
    };
}

#endif /* defined(__EVCommon__QueryCondition__) */
