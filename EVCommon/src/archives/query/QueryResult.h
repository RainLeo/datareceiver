//
//  QueryResult.h
//  EVCommon
//
//  Created by baowei on 15/7/4.
//  Copyright (c) 2015 com. All rights reserved.
//

#ifndef __EVCommon__QueryResult__
#define __EVCommon__QueryResult__

#include <string>
#include <mutex>
#include "common/common_global.h"
#include "common/system/DateTime.h"
#include "common/data/Vector.h"
#include "common/data/Array.h"
#include "common/system/Convert.h"
#include "common/thread/Locker.h"
#include "../tag/MetaEvent.h"

using namespace std;
using namespace Common;

namespace Archives
{
    class QueryResult
    {
    public:
        QueryResult();
        virtual ~QueryResult();
        const MetaEvents& events() const;
        
        MetaEvent event() const;
        
        uint count() const;
        
        void add(const MetaEvent& me); 
        void add(const MetaEvents& mes);        
        virtual void add(const QueryResult& result);
        
    private:
        MetaEvents _metaEvents;
    };
    
    class QueryResultInternal : public QueryResult
    {
    public:
        QueryResultInternal();
        
        bool finished() const;
        void setFinished(bool finished);
        
        void add(const QueryResult& result) override;
        
    private:
        bool _finished;
    };
}

#endif /* defined(__EVCommon__QueryResult__) */
