//
//  QueryManager.cpp
//  EVCommon
//
//  Created by baowei on 15/7/10.
//  Copyright (c) 2015年 com. All rights reserved.
//

#include "QueryManager.h"
#include "../ArchiveService.h"

namespace Archives
{
    QueryManager::QueryManager(ArchiveService* as)
    {
        if(as == NULL)
            throw ArgumentException("as");
        
        _service = as;
    }
    QueryManager::~QueryManager()
    {
    }
    
    bool QueryManager::initialize(const ConfigFile* file)
    {
        return true;
    }
    bool QueryManager::unInitialize()
    {
        return true;
    }
    
    void QueryManager::queryAll(uint tagId, MetaEvents& mes)
    {
        QueryCondition condition(tagId);
        QueryResult result;
        query(condition, result);
        const MetaEvents& temp = result.events();
        mes.addRange(&temp);
    }
    
    //t1, t2 是文件的起始、结束时间，t2>=t1；start, end是查询的起始、结束时间，end>=start
    //下面6种情况：
    //t1	    start	end	    t2
    //t1	    start	t2	    end
    //t1	    t2	    start	end
    //start	    t1	    end	    t2
    //start	    end	    t1	    t2
    //start	    t1	    t2	    end
    
    void QueryManager::query(const QueryCondition& condition, QueryResult& result)
    {
        checkQueryCondition(condition);
        QueryCondition tc = trimTime(condition);
        
#if DEBUG
        Stopwatch sw("QueryManager::query");
        
        uint tagId = tc.tagId;
        DateTime startTime = tc.startTime;
        DateTime endTime = tc.endTime;
#endif
        
        QueryResultInternal fResult;
        FilterManager* fm = _service->filterManager();
        if (fm != NULL)
        {
            fm->query(tc, fResult);
        }
        bool hasfilterResult = fResult.finished();
        
        QueryResultInternal cResult;
        if (!hasfilterResult)
        {
            ArchiveCacheManager* acm = _service->cacheManager();
            if (acm != NULL)
            {
                acm->query(tc, cResult);
            }
        }
        bool hascacheResult = cResult.finished();
        
        QueryResultInternal aResult;
        if (!hasfilterResult && !hascacheResult)
        {
            ArchiveManager* am = _service->archiveManager();
            if (am != NULL)
            {
                am->query(tc, aResult);
            }
        }
        
        result.add(aResult);
        result.add(cResult);
        result.add(fResult);
        
#if DEBUG
        sw.stop();
		uint elapsed = (uint)sw.elapsed();

		double queryPerSecond = (double)result.count() / ((double)elapsed / 1000.0);
        Debug::writeFormatLine("Query the records from '%s' to '%s', tagId = %d, Count = %d, Time: %d ms, EPS: %.0lf",
			startTime.toString().c_str(), endTime.toString().c_str(), tagId, result.count(), elapsed, queryPerSecond);
#endif
    }
    
    void QueryManager::query(uint tagId, DateTime startTime, DateTime endTime, QueryResult& result)
    {
        QueryCondition condition(tagId, startTime, endTime);
        query(condition, result);
    }
    
    void QueryManager::checkQueryCondition(const QueryCondition& condition)
    {
        if (condition.hasTimeRange())
        {
            if (condition.endTime < condition.startTime)
            {
                throw ArgumentException("End time must be greater than start time.");
            }
        }
    }
    
    QueryCondition QueryManager::trimTime(const QueryCondition& condition)
    {
        uint tagId = condition.tagId;
        DateTime startTime = condition.startTime > DataExtensions::MinTime ? condition.startTime.toUtcTime() : DataExtensions::MinTime;
        DateTime endTime = condition.endTime < DataExtensions::MaxTime ? condition.endTime.toUtcTime() : DataExtensions::MaxTime;
        
        QueryCondition result = condition;
        ArchiveTagManager* tm = _service->tagManager();
        if (tm != NULL)
        {
            const ArchiveTag* tag = tm->getTag(tagId);
            if (tag != NULL)
            {
                switch (tag->timeResolutions())
                {
                    case DateTime::Resolutions::ResMillisecond:
                        result.startTime = startTime;
                        result.endTime = endTime;
                        break;
                    case DateTime::Resolutions::ResSecond:
                        result.startTime = DateTime(startTime.year(), startTime.month(), startTime.day(), startTime.hour(), startTime.minute(), startTime.second());
                        result.endTime = DateTime(endTime.year(), endTime.month(), endTime.day(), endTime.hour(), endTime.minute(), endTime.second());
                        break;
                    case DateTime::Resolutions::ResMinute:
                        result.startTime = DateTime(startTime.year(), startTime.month(), startTime.day(), startTime.hour(), startTime.minute(), 0);
                        result.endTime = DateTime(endTime.year(), endTime.month(), endTime.day(), endTime.hour(), endTime.minute(), 0);
                        break;
                    case DateTime::Resolutions::ResHour:
                        result.startTime = DateTime(startTime.year(), startTime.month(), startTime.day(), startTime.hour(), 0, 0);
                        result.endTime = DateTime(endTime.year(), endTime.month(), endTime.day(), endTime.hour(), 0, 0);
                        break;
                    default:
                        break;
                }
            }
        }
        else
        {
            result.startTime = startTime;
            result.endTime = endTime;
        }
        return result;
    }
}
