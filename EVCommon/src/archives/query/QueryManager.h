//
//  QueryManager.h
//  EVCommon
//
//  Created by baowei on 15/7/10.
//  Copyright (c) 2015年 com. All rights reserved.
//

#ifndef __EVCommon__QueryManager__
#define __EVCommon__QueryManager__

#include <string>
#include <mutex>
#include "common/common_global.h"
#include "common/system/DateTime.h"
#include "common/data/Vector.h"
#include "common/data/Array.h"
#include "common/system/Convert.h"
#include "common/thread/Locker.h"
#include "common/IO/File.h"
#include "common/diag/Stopwatch.h"
#include "common/xml/ConfigFile.h"
#include "base/BaseService.h"
#include "../query/QueryResult.h"
#include "tag/Tag.h"
#include "QueryCondition.h"

using namespace Common;

namespace Archives
{
    class ArchiveService;
    class QueryManager
    {
    public:
        QueryManager(ArchiveService* as);
        ~QueryManager();
        
        void queryAll(uint tagId, MetaEvents& mes);
        //t1, t2 是文件的起始、结束时间，t2>=t1；start, end是查询的起始、结束时间，end>=start
        //下面6种情况：
        //t1	    start	end	    t2
        //t1	    start	t2	    end
        //t1	    t2	    start	end
        //start	    t1	    end	    t2
        //start	    end	    t1	    t2
        //start	    t1	    t2	    end
        void query(const QueryCondition& condition, QueryResult& result);
        void query(uint tagId, DateTime startTime, DateTime endTime, QueryResult& result);
        
    private:
        bool initialize(const ConfigFile* file);
        bool unInitialize();

        QueryCondition trimTime(const QueryCondition& condition);
        static void checkQueryCondition(const QueryCondition& condition);
        
    private:
        friend ArchiveService;
        
        ArchiveService* _service;
    };
}

#endif /* defined(__EVCommon__QueryManager__) */
