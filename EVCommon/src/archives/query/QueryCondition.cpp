//
//  QueryCondition.cpp
//  EVCommon
//
//  Created by baowei on 15/7/7.
//  Copyright (c) 2015年 com. All rights reserved.
//

#include "QueryCondition.h"
#include "tag/TagService.h"

namespace Archives
{
    QueryCondition QueryCondition::Empty;
    
    QueryCondition::QueryCondition(uint tagId) : QueryCondition(tagId, DataExtensions::MinTime, DataExtensions::MaxTime)
    {
    }
    QueryCondition::QueryCondition(uint tagId, DateTime startTime, DateTime endTime, TimeSpan interval)
    {
        this->tagId = tagId;
        this->startTime = startTime;
        this->endTime = endTime;
        this->interval = interval;
        this->numberOfValues = 0;
        this->retrievalMode = RetrievalModes::RMAuto;
        this->boundaryType = BoundaryTypes::BTInside;
    }
    
    string QueryCondition::tagName() const
    {
        return "";
    }
    void QueryCondition::setTagName(uint tagId)
    {
        
    }
    
    DateTime QueryCondition::timestamp() const
    {
        return startTime;
    }
    void QueryCondition::setTimestamp(DateTime time)
    {
        startTime = time;
    }
    
    bool QueryCondition::specifiedTimeValue() const
    {
        return startTime > DataExtensions::MinTime && endTime == DataExtensions::MaxTime;
    }
    
    bool QueryCondition::isQueryAll() const
    {
        return startTime == DataExtensions::MinTime && endTime == DataExtensions::MaxTime;
    }
    
    bool QueryCondition::hasTimeRange() const
    {
        return startTime > DataExtensions::MinTime && endTime < DataExtensions::MaxTime;
    }
    
    bool QueryCondition::hasInterval() const
    {
        return interval != TimeSpan::Zero;
    }
    
    bool QueryCondition::hasNumberOfValues() const
    {
        return startTime > DataExtensions::MinTime && endTime == DataExtensions::MaxTime && numberOfValues != 0;
    }
    
    bool QueryCondition::sampleTimes(Array<DateTime>& times) const
    {
        if (hasTimeRange() && hasInterval())
        {
            TimeSpan i = interval > TimeSpan::Zero ? interval : -interval;
#if DEBUG
            int64_t count = i >= (endTime - startTime) ? 2 : ((endTime - startTime).ticks() / i.ticks() + 1);
#endif
            if (interval >= (endTime - startTime))
            {
                times.add(startTime);
                times.add(endTime);
            }
            else
            {
                DateTime time;
                long index = 0;
                if (interval > TimeSpan::Zero)
                {
                    do
                    {
                        time = startTime.addTicks(index * interval.ticks());
                        times.add(time);
                    } while (time < endTime);
                }
                else
                {
                    do
                    {
                        time = endTime.addTicks(index * interval.ticks());
                        times.add(time);
                    } while (time > startTime);
                }
#if DEBUG
                assert(index == count);
#endif
            }
            return true;
        }
        return false;
    }
    
    bool QueryCondition::isEmpty() const
    {
        return tagId == 0;
    }
}
