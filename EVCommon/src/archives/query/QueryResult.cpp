//
//  QueryResult.cpp
//  EVCommon
//
//  Created by baowei on 15/7/4.
//  Copyright (c) 2015 com. All rights reserved.
//

#include "QueryResult.h"

namespace Archives
{
    QueryResult::QueryResult()
    {
        _metaEvents.setCapacity(4096);
    }
    QueryResult::~QueryResult()
    {
    }
    const MetaEvents& QueryResult::events() const
    {
        return _metaEvents;
    }
    
    MetaEvent QueryResult::event() const
    {
        if (count())
        {
            return _metaEvents[0];
        }
        return MetaEvent();
    }
    
    uint QueryResult::count() const
    {
        return _metaEvents.count();
    }

    void QueryResult::add(const MetaEvent& me)
    {
        _metaEvents.add(me);
    }
    void QueryResult::add(const MetaEvents& mes)
    {
        if (mes.count() > 0)
        {
            _metaEvents.addRange(&mes);
        }
    }
    
    void QueryResult::add(const QueryResult& result)
    {
        add(result._metaEvents);
    }
    
    QueryResultInternal::QueryResultInternal() : QueryResult()
    {
        _finished = false;
    }
    
    bool QueryResultInternal::finished() const
    {
        return _finished;
    }
    void QueryResultInternal::setFinished(bool finished)
    {
        _finished = finished;
    }
    
    void QueryResultInternal::add(const QueryResult& result)
    {
        QueryResult::add(result);
        
        const QueryResultInternal* qri = dynamic_cast<const QueryResultInternal*>(&result);
        if (qri != NULL)
        {
            _finished = qri->_finished;
        }
    }
}