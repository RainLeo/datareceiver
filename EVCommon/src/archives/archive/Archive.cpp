//
//  Archive.cpp
//  EVCommon
//
//  Created by baowei on 15/7/6.
//  Copyright (c) 2015年 com. All rights reserved.
//

#include "Archive.h"
#include "common/system/Math.h"
#include "common/IO/FileInfo.h"
#include "common/IO/FileStream.h"
#include "common/diag/Debug.h"
#include "common/IO/Path.h"
#include "../tag/ArchiveTagManager.h"
#include "common/IO/MappingStream.h"
#include "../ArchiveService.h"

namespace Archives
{
    Archive::Archive(ArchiveService* as, const string& fileName, int64_t fileSize)
    {
        if(as == NULL)
            throw ArgumentException("as");
        
        _service = as;
        
        ArchiveTagManager* tm = _service->tagManager();
        _maxPrimaryPos = ((tm->tagCount() + 1) * Page::MaxSize);
        //            if (mTimeResolutions == NULL)
        //            {
        //                mTimeResolutions = new Dictionary<uint, DateTime::Resolutions>(tm.TagCount);
        //                foreach (var tag in tm.AllTags)
        //                {
        //                    mTimeResolutions[tag.TagId] = tag.TimeResolution;
        //                }
        //            }
        
        _fileName = fileName;
        _fileSize = fileSize;
        _filePage = NULL;
        _closing = false;
        _stream = NULL;
    }
    Archive::~Archive()
    {
        close();
    }
    
    bool Archive::isFull()
    {
        if(opened())
        {
            assert(_maxPrimaryPos > 0);
            return (fileSize() - _filePage->currentPage) <= _maxPrimaryPos;
        }
        return false;
    }
    
    string Archive::fileName() const
    {
        return Path::getFileName(_fileName);
    }
    
    bool Archive::opened() const
    {
        return _stream != NULL;
    }
    
    int64_t Archive::fileSize()
    {
        if(opened())
        {
            if (_fileSize == 0)
            {
                _fileSize = _stream->length();
            }
        }
        return _fileSize;
    }
    
    int Archive::used()
    {
        if(opened())
        {
            if (isFull())
            {
                return 100;
            }
            else
            {
                return (int)Math::round((double)(1.0 + _filePage->currentPage + _maxPrimaryPos) / (double)fileSize() * 100.0);
            }
        }
        return 0;
    }
    
    double Archive::addratePerHour()
    {
        ArchiveTagManager* tm = _service->tagManager();
        double addratePerHour = 0.0;
        const ArchiveTags& tags = tm->allTags();
        uint tagCount = tags.count();
        for (uint i=0; i<tagCount; i++)
        {
            const ArchiveTag* tag = tags[i];
            if(tag != NULL)
            {
                addratePerHour += (getAddratePerHour(tag->id()) / tagCount);
            }
        }
        return addratePerHour;
    }
    
    int64_t Archive::recordCount()
    {
        ArchiveTagManager* tm = _service->tagManager();
        int64_t count = 0;
        const ArchiveTags& tags = tm->allTags();
        uint tagCount = tags.count();
        for (uint i=0; i<tagCount; i++)
        {
            const ArchiveTag* tag = tags[i];
            if(tag != NULL)
            {
                count += getEventCount(tag->id());
            }
        }
        return count;
    }
    
    void Archive::open()
    {
        if (!File::exists(_fileName))
        {
            FileStream fs(_fileName, FileMode::FileCreate, FileAccess::FileReadWrite);
            FilePage fp(_fileName);
            fp.write(&fs);
            fs.setLength(_fileSize);
        }
        else
        {
            FileInfo info(_fileName);
            _fileSize = info.size();
        }
        
        Locker locker(&_streamMutex);
        _stream = new MappingStream(_fileName, _fileSize);
        
        _filePage = new FilePage(_fileName);
        _filePage->read(_stream);
    }
    void Archive::close()
    {
        _closing = true;
        
        if(opened())
        {
            delete _filePage;
            _filePage = NULL;
            
            Locker locker(&_streamMutex);
            delete _stream;
            _stream = NULL;
        }
        
        _closing = false;
    }
    
    int Archive::addRecords(uint tagId, const MetaEvents& mes, int offset)
    {
        if (_closing)
            return offset;
        
        if (mes.count() == 0 || mes.count() <= (uint)offset)
            return -1;
        
        //#if DEBUG
        //            Stopwatch sw = new Stopwatch();
        //            sw.Start();
        //#endif
        assert(_filePage);
        if (_filePage->startTime == DataExtensions::MinTime && mes.count() > 0)
        {
            _filePage->startTime = mes[0].timestamp;
        }
        
        int pos = offset;
        PrimaryPage pp;
        getOrNewPrimaryPage(tagId, pp);
        if (!pp.hasNextPage())
        {
            pos = addPrimaryRecords(pp, mes, pos);
        }
        else
        {
            pos = addIndexRecords(pp, mes, pos);
        }
        
        writeFilePage(_filePage);
        
        //#if DEBUG
        //            sw.Stop();
        
        //            int remain = (int)(FileSize - (long)_filePage->OverflowPos) - _maxPrimaryPos;
        //            Debug.WriteLine(string.Format("Archived, TagId = {0}, Count = {1}, Time: {2} ms, Remain: {3} k",
        //                tagId, mes.Length, sw.Elapsed.TotalMilliseconds, (remain / 1024).ToString("#,#")));
        //#endif
        
        return pos;
    }
    
    string Archive::getInformation(int index)
    {
        StringBuilder sb;
        sb.appendLine(Convert::convertStr("Archive[%d]: %s Size: %dM (Used: %d%%)", index, _fileName.c_str(), fileSize() / 1024 / 1024, used()));
        //sb.AppendLine(string.Format("Version: {0} Path: {1}", _filePage->Version, _fileName));
        sb.appendLine(Convert::convertStr("Record Size: %d Count: %lld Add Rate/Hour: %lf", Page::MaxSize, recordCount(), addratePerHour()));
        
        return sb.toString();
    }
    
    void Archive::query(const QueryCondition& condition, QueryResultInternal& result)
    {
#if DEBUG
        Stopwatch sw("query");
#endif
        uint tagId = condition.tagId;
        DateTime startTime = condition.startTime;
        DateTime endTime = condition.endTime;
        
        PrimaryPage pp;
        getPrimaryPage(tagId, pp);
        if (!pp.isEmpty())
        {
            DateTime primaryStartTime = pp.startTime;
            DateTime primaryEndTime = pp.endTime;
            DateTime archiveStartTime = primaryStartTime;
            DateTime archiveEndTime = pp.archiveEndTime;
            
            if (condition.specifiedTimeValue())
            {
                //                DateTime t = condition.Timestamp();
                MetaEvent lastEvent = getLastEvent(pp, tagId);
                
                MetaEvents mes;
                pp.toEvents(pp.timeResolution, mes);
                assert(lastEvent.timestamp > mes[mes.count() - 1].timestamp);
                
                bool finished = false;
                MetaEvent newme = MetaEvent::find(mes, lastEvent, condition.timestamp(), condition.retrievalMode, finished);
                if (newme.isValid())
                {
                    result.QueryResult::add(newme);
                }
                if (finished)
                {
                    result.setFinished(true);
                }
                
                if (!result.finished())
                {
                    query(pp, condition, result);
                }
            }
            else
            {
                if (DataExtensions::containTime(startTime, endTime, archiveStartTime, archiveEndTime))
                {
                    // It's in this archive.
                    if (DataExtensions::containTime(startTime, endTime, primaryStartTime, primaryEndTime))
                    {
                        MetaEvents mes, allmes;
                        pp.toEvents(pp.timeResolution, allmes);
                        MetaEvent::binarySearch(allmes, startTime, endTime, mes);
                        result.QueryResult::add(mes);
                        
                        if (DataExtensions::containAllTime(startTime, endTime, primaryStartTime, primaryEndTime))
                        {
                            result.setFinished(true);
                        }
                    }
                    
                    if (!result.finished())
                    {
                        query(pp, condition, result);
                    }
                }
            }
        }
#if DEBUG
        sw.stop(false);
        Debug::writeFormatLine("query the records of archive, TagId = %d, Count = %d, Time: %d ms, FileName: %s",
                               condition.tagId, result.count(), (uint)sw.elapsed(), _fileName.c_str());
#endif
    }
    
    void Archive::shift()
    {
        ArchiveTagManager* tm = _service->tagManager();
        const ArchiveTags& tags = tm->allTags();
        for (uint i=0; i<tags.count(); i++)
        {
            const ArchiveTag* tag = tags[i];
            if(tag != NULL)
            {
                updateArchiveEndTime(tag->id());
            }
        }
    }
    
    bool Archive::getLastOverflowRecord(const PrimaryPage& pp, OverflowPage& op)
    {
        if (pp.hasNextPage())
        {
            IndexPage ip;
            getNextIndexRecord(pp, ip);
            
            return getLastOverflowRecord(ip, op, false);
        }
        return false;
    }
    
    void Archive::updateArchiveEndTime(uint tagId)
    {
        PrimaryPage pp;
        getPrimaryPage(tagId, pp);
        if (pp.hasNextPage())
        {
            OverflowPage op;
            if (getLastOverflowRecord(pp, op))
            {
                pp.archiveEndTime = op.getEndTime(pp.timeResolution);
            }
            else
            {
                pp.archiveEndTime = pp.getEndTime(pp.timeResolution);
            }
            writeRecord(pp);
        }
        else if (!pp.isEmpty())
        {
            pp.archiveEndTime = pp.getEndTime(pp.timeResolution);
            writeRecord(pp);
        }
    }
    
    double Archive::getAddratePerHour(uint tagId)
    {
        PrimaryPage pp;
        getPrimaryPage(tagId, pp, true);
        if (!pp.isEmpty())
        {
            if (!pp.hasNextPage())
            {
                getPrimaryPage(tagId, pp);
            }
            
            DateTime st;
            DateTime et;
            getTimeRange(pp, st, et);
            
            int64_t count = getEventCount(pp);
            return (double)count / (et - st).totalHours();
        }
        return 0.0;
    }
    
    void Archive::getTimeRange(const PrimaryPage& pp, DateTime& st, DateTime& et)
    {
        DateTime primaryStartTime = pp.startTime;
        DateTime primaryEndTime = pp.endTime;
        DateTime archiveStartTime = primaryStartTime;
        DateTime archiveEndTime = pp.archiveEndTime;
        
        if (archiveEndTime != DataExtensions::MaxTime)
        {
            assert(isFull());
            st = archiveStartTime;
            et = archiveEndTime;
        }
        else
        {
            if (primaryEndTime != DataExtensions::MaxTime)
            {
                OverflowPage op;
                if (getLastOverflowRecord(pp, op))
                {
                    primaryEndTime = op.getEndTime(pp.timeResolution);
                }
            }
            else
            {
                primaryEndTime = pp.getEndTime(pp.timeResolution);
            }
            
            st = primaryStartTime;
            et = primaryEndTime;
        }
    }
    
    int64_t Archive::getEventCount(uint tagId)
    {
        PrimaryPage pp;
        getPrimaryPage(tagId, pp, true);
        return getEventCount(pp);
    }
    
    int64_t Archive::getEventCount(const RecordPage& rp, uint tagId)
    {
        int64_t count = 0;
        if (rp.hasNextPage())
        {
            uint pos = rp.nextPage * Page::MaxSize;
            assert(pos < fileSize());
            
            _streamMutex.lock();
            _stream->seek(pos, SeekOrigin::SeekBegin);
            IndexPage ip;
            ip.read(_stream);
            
            count += ip.getEventCount(_stream);
            _streamMutex.unlock();
        }
        return count;
    }
    
    int64_t Archive::getEventCount(const PrimaryPage& pp)
    {
        int64_t count = 0;
        if (!pp.isEmpty())
        {
            count += pp.count();
        }
        
        count += getEventCount(pp, pp.tagId);
        return count;
    }
    
    MetaEvent Archive::getNextPageFirstEvent(const RecordPage& rp)
    {
        MetaEvent me;
        if (rp.hasNextPage())
        {
            uint pos = rp.nextPage * Page::MaxSize;
            assert(pos < fileSize());
            Locker locker(&_streamMutex);
            _stream->seek(pos, SeekOrigin::SeekBegin);
            IndexPage ip;
            ip.read(_stream);
            
            MetaEvents mes;
            ip.toEvents(mes, _stream, 1);
            if (mes.count() > 0)
            {
                me = mes[0];
            }
        }
        return me;
    }
    
    MetaEvent Archive::getLastEvent(const RecordPage& ip, uint tagId)
    {
        MetaEvent lastEvent;
        if (ip.hasNextPage())
        {
            lastEvent = getNextPageFirstEvent(ip);
        }
        else
        {
            ArchiveCacheManager* acm = _service->cacheManager();
            lastEvent = acm->getFirstMetaEvent(tagId);
        }
        return lastEvent;
    }
    
    void Archive::query(const RecordPage& rp, const QueryCondition& condition, QueryResultInternal& result)
    {
        if (rp.hasNextPage())
        {
            uint pos = rp.nextPage * Page::MaxSize;
            assert(pos < fileSize());
            _streamMutex.lock();
            _stream->seek(pos, SeekOrigin::SeekBegin);
            IndexPage ip;
            ip.read(_stream);
            _streamMutex.unlock();
            
            uint tagId = condition.tagId;
            DateTime startTime = condition.startTime;
            DateTime endTime = condition.endTime;
            DateTime ipStartTime = ip.startTime;
            DateTime ipEndTime = ip.endTime;
            
            if (condition.specifiedTimeValue())
            {
                //                    DateTime t = condition.Timestamp();
                MetaEvent lastEvent = getLastEvent(ip, tagId);
                
                _streamMutex.lock();
                MetaEvents mes;
                ip.toEvents(mes, _stream, condition);
                assert(lastEvent.timestamp > mes[mes.count() - 1].timestamp);
                _streamMutex.unlock();
                
                bool finished = false;
                MetaEvent newme = MetaEvent::find(mes, lastEvent, condition.timestamp(), condition.retrievalMode, finished);
                if (!newme.isValid())
                {
                    result.QueryResult::add(newme);
                }
                if (finished)
                {
                    result.setFinished(true);
                }
                else
                {
                    query(ip, condition, result);
                }
            }
            else
            {
                if (DataExtensions::containTime(startTime, endTime, ipStartTime, ipEndTime))
                {
                    _streamMutex.lock();
                    MetaEvents newmes;
                    ip.toEvents(newmes, _stream, condition);
                    _streamMutex.unlock();
                    
                    result.QueryResult::add(newmes);
                    
                    if (DataExtensions::containAllTime(startTime, endTime, ipStartTime, ipEndTime))
                    {
                        result.setFinished(true);
                    }
                }
                
                if (!result.finished())
                {
                    query(ip, condition, result);
                }
            }
        }
    }
    
    int Archive::addPrimaryRecords(PrimaryPage& pp, const MetaEvents& mes, int offset)
    {
        int pos = offset;
        DateTime::Resolutions tr = getTimeResolution(pp.tagId);
        if (pp.timeResolution == tr)
        {
            pos = pp.addRecords(tr, mes, offset);
        }
        else
        {
            pp.updateEndTime(pp.timeResolution);
        }
        
        // A tag can have as many overflow records as needed.
        if (pos >= 0)
        {
            IndexPage ip;
            if (createIndexRecord(pp, ip))
            {
                OverflowPage op;
                pos = addRecords(ip, op, mes, pos);
            }
        }
        
        writeRecord(pp);
        
        return pos;
    }
    
    int Archive::addIndexRecords(const PrimaryPage& pp, const MetaEvents& mes, int offset)
    {
        int pos = offset;
        IndexPage ip;
        if (getNextIndexRecord(pp, ip))
        {
            DateTime::Resolutions tr = getTimeResolution(ip.tagId);
            if (ip.timeResolution == tr)
            {
                OverflowPage op;
                getLastOverflowRecord(ip, op);
                pos = addRecords(ip, op, mes, pos);
                
            }
            else
            {
                ip.updateEndTime(ip.timeResolution);
                
                IndexPage previp(ip);
                if (createIndexRecord(previp, ip))
                {
                    OverflowPage op;
                    pos = addRecords(ip, op, mes, pos);
                }
                writeRecord(previp);
            }
        }
        return pos;
    }
    
    int Archive::addRecords(IndexPage& ip, OverflowPage& op, const MetaEvents& mes, int offset)
    {
        int pos = offset;
        if (op.hasTagId())
        {
            pos = op.addRecords(ip.timeResolution, mes, pos);
            writeRecord(op);
        }
        
        if (pos >= 0)
        {
            IndexPage previp;
            OverflowPage prevop;
            do
            {
                if ((uint)pos < mes.count())
                {
                    if (ip.isFull())
                    {
                        Debug::writeFormatLine("Create a new index record. overflow record count = %d", ip.count());
                        
                        assert(!ip.hasNextPage());
                        previp = ip;
                        if (!createIndexRecord(previp, ip))
                        {
                            break;
                        }
                    }
                    
                    prevop = op;
                    if (!createOverflowRecord(ip, prevop, op))
                    {
                        break;
                    }
                    
                    pos = op.addRecords(ip.timeResolution, mes, pos);
                    writeRecord(op);
                    if (prevop.hasTagId())
                    {
                        writeRecord(prevop);
                    }
                    
                    ip.addRecords(ip.timeResolution, op);
                    
                    if (previp.hasTagId())
                    {
                        writeRecord(previp);
                    }
                    writeRecord(ip);
                    
                    //Debug.WriteLine(string.Format("ip = '{0}'", ip.ToString()));
                }
            } while (pos >= 0);
        }
        return pos;
    }
    
    void Archive::getOrNewPrimaryPage(uint tagId, PrimaryPage& pp)
    {
        getPrimaryPage(tagId, pp);
        if (!pp.hasTagId())
        {
            pp = PrimaryPage();
            pp.tagId = tagId;
            pp.type = _service->tagManager()->getTagType(pp.tagId);
            pp.currentPage = tagId;
            pp.timeResolution = getTimeResolution(tagId);
        }
    }
    
    void Archive::getPrimaryPage(uint tagId, PrimaryPage& pp, bool onlyReadHeader)
    {
        Locker locker(&_streamMutex);
        _stream->seek(tagId * Page::MaxSize, SeekOrigin::SeekBegin);
        if (onlyReadHeader)
        {
            pp.readHeader(_stream);
        }
        else
        {
            pp.read(_stream);
        }
    }
    
    bool Archive::getLastOverflowRecord(const IndexPage& ip, OverflowPage& op, bool fullReturnNull)
    {
        if (ip.isEmpty())
            return false;
        
        uint recordNo = ip.lastRecordNo();
        uint pos = recordNo * Page::MaxSize;
        Locker locker(&_streamMutex);
        
        assert(pos < fileSize());
        _stream->seek(pos, SeekOrigin::SeekBegin);
        op.read(_stream);
        
        if (!op.isFull())
        {
            return true;
        }
        else
        {
            return fullReturnNull ? false : true;
        }
    }
    
    bool Archive::createIndexRecord(RecordPage& prevPage, IndexPage& ip)
    {
        if (isFull())
            return false;
        
        ip = IndexPage();
        ip.tagId = prevPage.tagId;
        ip.currentPage = (uint)((fileSize() - _filePage->currentPage) / Page::MaxSize) - 1;
        ip.previousPage = prevPage.currentPage;
        prevPage.nextPage = ip.currentPage;
        ip.timeResolution = getTimeResolution(ip.tagId);
        
        _filePage->currentPage += Page::MaxSize;
        
        return true;
    }
    
    bool Archive::getNextIndexRecord(const RecordPage& prevPage, IndexPage& ip)
    {
        assert(prevPage.hasNextPage());
        
        _streamMutex.lock();
        uint pos = prevPage.nextPage * Page::MaxSize;
        assert(pos < fileSize());
        _stream->seek(pos, SeekOrigin::SeekBegin);
        ip.read(_stream);
        _streamMutex.unlock();
        
        if (ip.hasNextPage())
        {
            IndexPage previp(ip);
            return getNextIndexRecord(previp, ip);
        }
        else
        {
            return true;
        }
    }
    
    bool Archive::createOverflowRecord(const IndexPage& ip, OverflowPage& prevop, OverflowPage& op)
    {
        if (isFull())
            return false;
        
        op = OverflowPage();
        op.tagId = ip.tagId;
        op.type = _service->tagManager()->getTagType(ip.tagId);
        op.currentPage = (uint)((fileSize() - (int64_t)_filePage->currentPage) / Page::MaxSize) - 1;
        op.parentPage = ip.currentPage;
        
        if (prevop.hasTagId())
        {
            op.previousPage = prevop.currentPage;
            prevop.nextPage = op.currentPage;
        }
        
        _filePage->currentPage += Page::MaxSize;
        
        return true;
    }
    
    void Archive::writeRecord(const RecordPage& rp)
    {
        uint pos = rp.currentPage * Page::MaxSize;
#if DEBUG
        const PrimaryPage* pp = dynamic_cast<const PrimaryPage*>(&rp);
        if (pp == NULL)
        {
            assert(pos >= _maxPrimaryPos);
            assert(pos <= (fileSize() - Page::MaxSize));
        }
#endif
        Locker locker(&_streamMutex);
        _stream->seek(pos, SeekOrigin::SeekBegin);
        rp.write(_stream);
    }
    
    void Archive::writeFilePage(const FilePage* fp)
    {
        Locker locker(&_streamMutex);
        
        _stream->seek(0, SeekOrigin::SeekBegin);
        fp->write(_stream);
    }
    
    DateTime::Resolutions Archive::getTimeResolution(uint tagId)
    {
        ArchiveTagManager* tm = _service->tagManager();
        DateTime::Resolutions tr = DateTime::Resolutions::ResNone;
        const ArchiveTag* tag = tm->getTag(tagId);
        if(tag != NULL)
        {
            tr = tag->timeResolutions();
            assert(tr != DateTime::Resolutions::ResNone);
        }
        return tr;
    }
}
