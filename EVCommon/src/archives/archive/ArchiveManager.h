//
//  ArchiveManager.h
//  EVCommon
//
//  Created by baowei on 15/7/10.
//  Copyright (c) 2015年 com. All rights reserved.
//

#ifndef __EVCommon__ArchiveManager__
#define __EVCommon__ArchiveManager__

#include <string>
#include <mutex>
#include "common/common_global.h"
#include "common/system/DateTime.h"
#include "common/data/Vector.h"
#include "common/data/Array.h"
#include "common/system/Convert.h"
#include "common/thread/Locker.h"
#include "common/IO/File.h"
#include "common/diag/Stopwatch.h"
#include "common/xml/ConfigFile.h"
#include "../query/QueryResult.h"
#include "tag/Tag.h"
#include "Archive.h"

using namespace Common;

namespace Archives
{
    class ArchiveConfig;
    class ArchiveService;
    class ArchiveStatConfig;
    class ArchiveManager
    {
    public:
        ArchiveManager(ArchiveService* as);
        ~ArchiveManager();
        
        void newArchive();
        void clearAllArchives();
        
        void addRecords(uint tagId, const MetaEvents& mes, int offset = 0);
        
        string getAllArchiveInformations() const;
        
        void query(const QueryCondition& condition, QueryResultInternal& result) const;
        
    private:
        bool initialize(const ConfigFile* file);
        bool unInitialize();
        
        string newArchiveFileName() const;
        
        void updatePrimaryArchiveId();
        
        bool saveStatConfig();
        
    private:
        friend ArchiveConfig;
        friend ArchiveService;
        friend ArchiveStatConfig;
        
        ArchiveService* _service;
        
        Archives _archives;
        
        string _path;
        int _primaryArchiveId;
        const int64_t DefaultArchiveFileSize = 20L * Page::MaxSize * Page::MaxSize;
        int64_t _archiveFileSize = DefaultArchiveFileSize;
        Archive* _primaryArchive;
    };
}

#endif /* defined(__EVCommon__ArchiveManager__) */
