//
//  Archive.h
//  EVCommon
//
//  Created by baowei on 15/7/6.
//  Copyright (c) 2015 com. All rights reserved.
//

#ifndef __EVCommon__Archive__
#define __EVCommon__Archive__

#include <string>
#include <mutex>
#include "common/common_global.h"
#include "common/system/DateTime.h"
#include "common/data/Vector.h"
#include "common/data/Array.h"
#include "common/system/Convert.h"
#include "common/thread/Locker.h"
#include "common/IO/File.h"
#include "common/diag/Stopwatch.h"
#include "../query/QueryResult.h"
#include "tag/Tag.h"
#include "Page.h"

using namespace Common;

namespace Archives
{
    class ArchiveService;
    class Archive
    {
    public:
        Archive(ArchiveService* as, const string& fileName, int64_t fileSize = 0);
        ~Archive();
        
        bool isFull();
        
        string fileName() const;
        
        bool opened() const;
        
        int64_t fileSize();
        
        void open();
        void close();
        
        int addRecords(uint tagId, const MetaEvents& mes, int offset = 0);
        string getInformation(int index);
        void query(const QueryCondition& condition, QueryResultInternal& result);
        void shift();
        
    private:
        int used();
        
        double addratePerHour();
        
        int64_t recordCount();
        
        bool getLastOverflowRecord(const PrimaryPage& pp, OverflowPage& op);
        void updateArchiveEndTime(uint tagId);
        double getAddratePerHour(uint tagId);
        
        void getTimeRange(const PrimaryPage& pp, DateTime& st, DateTime& et);
        
        int64_t getEventCount(uint tagId);
        int64_t getEventCount(const RecordPage& rp, uint tagId);
        int64_t getEventCount(const PrimaryPage& pp);
        
        MetaEvent getNextPageFirstEvent(const RecordPage& rp);
        
        MetaEvent getLastEvent(const RecordPage& ip, uint tagId);
        
        void query(const RecordPage& rp, const QueryCondition& condition, QueryResultInternal& result);
        
        int addPrimaryRecords(PrimaryPage& pp, const MetaEvents& mes, int offset);
        int addIndexRecords(const PrimaryPage& pp, const MetaEvents& mes, int offset);
        int addRecords(IndexPage& ip, OverflowPage& op, const MetaEvents& mes, int offset);
        
        void getOrNewPrimaryPage(uint tagId, PrimaryPage& pp);
        void getPrimaryPage(uint tagId, PrimaryPage& pp, bool onlyReadHeader = false);
        bool getLastOverflowRecord(const IndexPage& ip, OverflowPage& op, bool fullReturnNull = true);
        bool getNextIndexRecord(const RecordPage& prevPage, IndexPage& ip);
        
        bool createIndexRecord(RecordPage& prevPage, IndexPage& ip);
        bool createOverflowRecord(const IndexPage& ip, OverflowPage& prevop, OverflowPage& op);
        
        void writeRecord(const RecordPage& rp);
        void writeFilePage(const FilePage* fp);
        
        DateTime::Resolutions getTimeResolution(uint tagId);
        
    private:
        string _fileName;
        int64_t _fileSize;
        FilePage* _filePage;
        Stream* _stream;
        mutex _streamMutex;
        int64_t _maxPrimaryPos;
        bool _closing;
        
        ArchiveService* _service;
    };
    
    class Archives : public Vector<Archive>
    {
    public:
        Archives(bool autoDelete = true, uint capacity = Vector<Archive>::DefaultCapacity) : Vector<Archive>(autoDelete, capacity)
        {
        }
    };
}

#endif /* defined(__EVCommon__Archive__) */
