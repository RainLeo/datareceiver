//
//  Page.h
//  EVCommon
//
//  Created by baowei on 15/7/6.
//  Copyright (c) 2015 com. All rights reserved.
//

#ifndef __EVCommon__Page__
#define __EVCommon__Page__

#include "common/IO/Stream.h"
#include "common/system/DateTime.h"
#include "common/system/Version.h"
#include "common/exception/Exception.h"
#include "../utils/DataExtensions.h"
#include "../tag/MetaEvent.h"
#include "../query/QueryCondition.h"
#include "tag/Tag.h"

using namespace Common;

namespace Archives
{
    class Page
    {
    public:
        virtual int size() const = 0;
        virtual void copyFrom(const Page* page) = 0;
        virtual void write(Stream* stream) const = 0;
        virtual void read(Stream* stream) = 0;
        
        virtual ~Page()
        {
        }

    public:
        const static int MaxSize = 1024;
    };
    
    class RecordPage : public Page
    {
    public:
        uint currentPage;
        uint previousPage;
        uint nextPage;
        uint parentPage;
        uint tagId;
        DateTime startTime;
        DateTime endTime;
        
    public:
        RecordPage();
        RecordPage(uint tagId);
        RecordPage(const RecordPage& rp);
        
        int size() const override;
        void copyFrom(const Page* page) override;
        void write(Stream* stream) const override;
        void read(Stream* stream) override;
        
        virtual uint count() const = 0;
        virtual bool isFull() const;
        
        bool hasTagId() const;
        bool hasNextPage() const;
        bool isEmpty() const;
    };
    
    class DataPage : public RecordPage
    {
    public:
        struct Record
        {
        public:
            uint time;
            Tag::Value value;
            Tag::Type type;
            Tag::Qualitystamp qualitystamp;
            
        public:
            Record(Tag::Type type = Tag::Type::Null);
            Record(Tag::Type type, Tag::Value value, uint time, Tag::Qualitystamp qualitystamp);
            
            int size() const;
            static int maxSize();
            
            void write(Stream* stream) const;
            void read(Stream* stream);
            MetaEvent toEvent(uint tagId, DateTime time) const;
            
        private:
            const static int MaxTimeLength;
        };
        class Records : public Array<Record>
        {
        public:
            Records(uint capacity = Array<Record>::DefaultCapacity);
        };
        
    public:
        Tag::Type type;
        
    public:
        DataPage();
        DataPage(uint tagId, Tag::Type type);
        DataPage(const DataPage& dp);
        
        int size() const override;
        void copyFrom(const Page* page) override;
        void write(Stream* stream) const override;
        void read(Stream* stream) override;
        uint count() const override;
        bool isFull() const override;
        
        byte version() const;
        void readHeader(Stream* stream);
        DateTime getEndTime(DateTime::Resolutions tr) const;
        int addRecords(DateTime::Resolutions tr, const MetaEvents& mes, int offset = 0);
        void updateEndTime(DateTime::Resolutions tr);
        void toEvents(DateTime::Resolutions tr, MetaEvents& mes);
        
    protected:
        virtual void writeInternal(Stream* stream) const = 0;
        virtual void readInternal(Stream* stream) = 0;
        
    private:
        void read(Stream* stream, bool onlyReadHeader);
        DateTime getLastRecordTime(DateTime::Resolutions tr) const;
        void updateEndTime(DateTime time);
        
    private:
        Records _records;
    };
    
    class OverflowPage : public DataPage
    {
    public:
        OverflowPage();
        OverflowPage(uint tagId, Tag::Type type);
        OverflowPage(const OverflowPage& op);
        
        void copyFrom(const Page* page) override;
        
        void operator=(const OverflowPage& value);
        
    protected:
        void writeInternal(Stream* stream) const override;
        void readInternal(Stream* stream) override;
    };
    class OverflowPages : public Vector<OverflowPage>
    {
    public:
        OverflowPages(bool autoDelete = true, uint capacity = Vector<OverflowPage>::DefaultCapacity);
    };
    
    class FilePage : public Page
    {
    public:
        uint currentPage;
        Version version;
        string fileName;
        DateTime startTime;
        DateTime endTime;
        
    public:
        FilePage();
        FilePage(const string& fileName);
        FilePage(const FilePage& fp);
        
        int size() const override;
        void copyFrom(const Page* page) override;
        void write(Stream* stream) const override;
        void read(Stream* stream) override;
        
        void operator=(const FilePage& value);
        
    protected:
        const static DateTime DefaultStartTime;
        const static DateTime DefaultEndTime;
        
    private:
        const static uint Header = 0x12e63ea1;
    };
    
    class IndexPage : public RecordPage
    {
    public:
        struct Record
        {
        public:
            uint pageNumber;
            uint time;
            
        public:
            Record();
            Record(uint time, uint pageNumber);
            
            int size() const;
            void write(Stream* stream) const;
            void read(Stream* stream);
            
        public:
            static int maxSize();
            
        private:
            byte timeLength() const;
        };
        class Records : public Array<Record>
        {
        public:
            Records(uint capacity = Array<Record>::DefaultCapacity);
            Record getLastRecord() const;
        };
        
    public:
        DateTime::Resolutions timeResolution;
        
    public:
        IndexPage();
        IndexPage(uint tagId);
        IndexPage(const IndexPage& ip);
        
        uint count() const override;
        int size() const override;
        void copyFrom(const Page* page) override;
        void write(Stream* stream) const override;
        void read(Stream* stream) override;
        
        bool isFull() const override;
        uint lastRecordNo() const;
        DateTime getEndTime(DateTime::Resolutions tr) const;
        void updateEndTime(DateTime::Resolutions tr);
        DateTime getLastRecordTime(DateTime::Resolutions tr) const;
        
        int addRecords(DateTime::Resolutions tr, const OverflowPages& ops, int offset = 0);
        int addRecords(DateTime::Resolutions tr, const OverflowPage& op, int offset = 0);
        
        int64_t getEventCount(Stream* stream) const;
        int64_t getEventCount(Stream* stream, uint overflowPageCount) const;
        
        void toEvents(MetaEvents& mes, Stream* stream, const QueryCondition& condition = QueryCondition::Empty) const;
        void toEvents(MetaEvents& mes, Stream* stream, uint overflowPageCount, const QueryCondition& condition = QueryCondition::Empty) const;
        
        void operator=(const IndexPage& value);
        
    private:
        void updateEndTime(DateTime time);
        
    private:
        Records _records;
    };
    
    class PrimaryPage : public DataPage
    {
    public:
        DateTime archiveEndTime;
        DateTime::Resolutions timeResolution;
        
    public:
        PrimaryPage();
        PrimaryPage(uint tagId, Tag::Type type);
        PrimaryPage(const PrimaryPage& pp);
        
        int size() const override;
        void copyFrom(const Page* page) override;
        
        void operator=(const PrimaryPage& value);
        
    protected:
        void readInternal(Stream* stream) override;
        void writeInternal(Stream* stream) const override;
    };
}

#endif /* defined(__EVCommon__Page__) */
