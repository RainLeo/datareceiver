//
//  ArchiveStatConfig.cpp
//  EVCommon
//
//  Created by baowei on 15/7/8.
//  Copyright (c) 2015 com. All rights reserved.
//

#include "ArchiveStatConfig.h"
#include "common/system/TimeSpan.h"
#include "common/IO/Directory.h"
#include "common/IO/File.h"
#include "common/IO/Path.h"
#include "common/system/Convert.h"
#include "common/xml/XmlTextReader.h"
#include "common/xml/XmlDocument.h"
#include "../ArchiveService.h"

using namespace Common;

namespace Archives
{
    ArchiveStatConfig::ArchiveStatConfig(const ConfigFile& file, ArchiveService* service) : Configuration(file)
    {
        if(service == NULL)
            throw ArgumentException("service");
        
        _service = service;
    }
    
    ArchiveStatConfig::~ArchiveStatConfig()
    {
        _service = NULL;
    }
    
    bool ArchiveStatConfig::save(XmlTextWriter& writer)
    {
        ArchiveManager* am = _service->archiveManager();
        
        writer.writeStartElement("archives");
        writer.writeAttributeString("primaryid", Convert::convertStr(am->_primaryArchiveId));
        
        for (uint i=0; i<am->_archives.count(); i++)
        {
            const Archive* archive = am->_archives[i];
            writer.writeStartElement("archive");
            writer.writeAttributeString("filename", archive->fileName());
            if (am->_primaryArchive == archive)
            {
                writer.writeAttributeString("primary", "true");
            }
            writer.writeEndElement();
        }
        
        writer.writeEndElement();
        
        return true;
    }
    bool ArchiveStatConfig::load(XmlTextReader& reader)
    {
        if (reader.nodeType() == XmlNodeType::Element &&
            reader.localName() == "archives")
        {
            ArchiveManager* am = _service->archiveManager();
            Convert::parseInt32(reader.getAttribute("primaryid"), am->_primaryArchiveId);
            while (reader.read())
            {
                if (reader.nodeType() == XmlNodeType::Element &&
                    reader.localName() == "archive")
                {
                    string filename = reader.getAttribute("filename");
                    filename = Path::combine(am->_path, filename);
                    Archive* archive = new Archive(_service, filename);
                    am->_archives.add(archive);
                    
                    bool primary = false;
                    Convert::parseBoolean(reader.getAttribute("primary"), primary);
                    if(primary)
                    {
                        am->_primaryArchive = archive;
                        archive->open();
                    }
                }
            }
            return true;
        }
        return false;
    }
}
