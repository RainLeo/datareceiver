//
//  Page.cpp
//  EVCommon
//
//  Created by baowei on 15/7/6.
//  Copyright (c) 2015年 com. All rights reserved.
//

#include "Page.h"
#include "common/system/Math.h"
#include "../tag/MetaEvent.h"
#include "../tag/ArchiveTag.h"

namespace Archives
{
    RecordPage::RecordPage() : RecordPage(0)
    {
    }
    RecordPage::RecordPage(uint tagId)
    {
        this->tagId = tagId;
        
        startTime = DataExtensions::MinTime;
        endTime = DataExtensions::MaxTime;
        
        currentPage = 0;
        previousPage = 0;
        nextPage = 0;
        parentPage = 0;
    }
    RecordPage::RecordPage(const RecordPage& rp)
    {
        copyFrom(&rp);
    }
    bool RecordPage::hasTagId() const
    {
        return tagId > 0;
    }
    bool RecordPage::isFull() const
    {
        if (hasNextPage())
            return true;
        
        return false;
    }
    bool RecordPage::hasNextPage() const
    {
        return (nextPage > 0);
    }
    bool RecordPage::isEmpty() const
    {
        return count() == 0;
    }
    int RecordPage::size() const
    {
        int s = sizeof(tagId) +
        DataExtensions::TimeSize +      // size of startTime
        DataExtensions::TimeSize +      // size of endTime
        sizeof(currentPage) + sizeof(previousPage) + sizeof(nextPage) + sizeof(parentPage);
        return s;
    }
    void RecordPage::copyFrom(const Page* page)
    {
        const RecordPage* rp = dynamic_cast<const RecordPage*>(page);
        assert(rp);
        
        this->tagId = rp->tagId;
        
        this->startTime = rp->startTime;
        this->endTime = rp->endTime;
        
        this->currentPage = rp->currentPage;
        this->previousPage = rp->previousPage;
        this->nextPage = rp->nextPage;
        this->parentPage = rp->parentPage;
    }
    void RecordPage::write(Stream* stream) const
    {
        if (stream == NULL)
        {
            throw ArgumentNullException("stream");
        }
        
        if (!stream->canWrite())
        {
            throw Exception("stream can not write.");
        }
        
        stream->writeUInt32(tagId);
        DataExtensions::writeDateTime(stream, startTime);
        DataExtensions::writeDateTime(stream, endTime);
        stream->writeUInt32(currentPage);
        stream->writeUInt32(previousPage);
        stream->writeUInt32(nextPage);
        stream->writeUInt32(parentPage);
    }
    void RecordPage::read(Stream* stream)
    {
        if (stream == NULL)
        {
            throw ArgumentNullException("stream");
        }
        
        if (!stream->canRead())
        {
            throw Exception("stream can not read.");
        }
        
        tagId = stream->readUInt32();
        startTime = DataExtensions::readDateTime(stream);
        endTime = DataExtensions::readDateTime(stream);
        currentPage = stream->readUInt32();
        previousPage = stream->readUInt32();
        nextPage = stream->readUInt32();
        parentPage = stream->readUInt32();
    }
    
    const int DataPage::Record::MaxTimeLength = DataExtensions::getTimeLength(UINT32_MAX);
    DataPage::Record::Record(Tag::Type type)
    {
        this->type = type;
        this->value.nValue = 0;
        this->time = 0;
        this->qualitystamp = Tag::Qualitystamp::InitValue;
    }
    DataPage::Record::Record(Tag::Type type, Tag::Value value, uint time, Tag::Qualitystamp qualitystamp)
    {
        assert(type != Tag::Type::Null);
        
        this->type = type;
        this->value = value;
        this->time = time;
        this->qualitystamp = qualitystamp;
    }
    int DataPage::Record::size() const
    {
        int s = DataExtensions::getTimeLength(time) + Tag::valueSize(type, value) + sizeof(qualitystamp);
        return s;
    }
    int DataPage::Record::maxSize()
    {
        int s = MaxTimeLength + sizeof(Tag::Value) + sizeof(ushort);
        return s;
    }
    void DataPage::Record::write(Stream* stream) const
    {
        DataExtensions::writeTime(stream, time);
        Tag::writeValue(stream, type, value);
        DataExtensions::writeQuality(stream, qualitystamp);
    }
    void DataPage::Record::read(Stream* stream)
    {
        time = DataExtensions::readTime(stream);
        Tag::readValue(stream, type, value);
        qualitystamp = DataExtensions::readQuality(stream);
    }
    MetaEvent DataPage::Record::toEvent(uint tagId, DateTime time) const
    {
        MetaEvent me;
        me.tagId = tagId;
        me.timestamp = time;
        me.type = type;
        me.value = value;
        me.qualitystamp = qualitystamp;
        return me;
    }
    DataPage::Records::Records(uint capacity) : Array<Record>(capacity)
    {
    }
    
    DataPage::DataPage() : RecordPage()
    {
    	this->type = Tag::Type::Null;
    }
    DataPage::DataPage(uint tagId, Tag::Type type) : RecordPage(tagId)
    {
        this->type = type;
    }
    DataPage::DataPage(const DataPage& dp)
    {
        copyFrom(&dp);
    }
    uint DataPage::count() const
    {
        return _records.count();
    }
    byte DataPage::version() const
    {
        return 1;
    }
    bool DataPage::isFull() const
    {
        if (RecordPage::isFull())
            return true;
        
        int s = size();
        bool full = (s + Record::maxSize()) >= MaxSize;
        return full;
    }
    int DataPage::size() const
    {
        byte ver = version();
        uint c = count();
        int s = RecordPage::size() + sizeof(ver) + sizeof(type) + sizeof(c);
        for (uint i=0; i<_records.count(); i++)
        {
            s += _records[i].size();
        }
        return s;
    }
    void DataPage::copyFrom(const Page* page)
    {
        RecordPage::copyFrom(page);
        
        const DataPage* dp = dynamic_cast<const DataPage*>(page);
        assert(dp);
        
        this->type = dp->type;
        this->_records.clear();
        this->_records.addRange(&dp->_records);
    }
    void DataPage::write(Stream* stream) const
    {
#if DEBUG
        assert(stream->canSeek());
        assert(type != Tag::Type::Null);
        int64_t startpos = stream->position();
#endif
        RecordPage::write(stream);

        assert(type != Tag::Type::Null);
        stream->writeByte(version());
        stream->writeByte((byte)type);
        
        writeInternal(stream);
        
        uint c = count();
        stream->writeUInt32(c);
        
        for (uint i=0; i<c; i++)
        {
            _records[i].write(stream);
        }
        
#if DEBUG
        int64_t endpos = MaxSize - (stream->position() - startpos);
        assert(endpos >= 0);
#endif
    }
    void DataPage::read(Stream* stream, bool onlyReadHeader)
    {
        RecordPage::read(stream);
      
        stream->readByte();     // skip version
        type = (Tag::Type)stream->readByte();
        
        readInternal(stream);
        
        uint c = stream->readUInt32();
        if (!onlyReadHeader)
        {
            _records.clear();
            for (uint i = 0; i < c; i++)
            {
                Record record(type);
                record.read(stream);
                _records.add(record);
            }
        }
    }
    void DataPage::readHeader(Stream* stream)
    {
        read(stream, true);
    }
    void DataPage::read(Stream* stream)
    {
        read(stream, false);
    }
    DateTime DataPage::getEndTime(DateTime::Resolutions tr) const
    {
        assert(startTime > DataExtensions::MinTime);
        
        if (isEmpty())
        {
            return startTime;
        }
        else if (isFull())
        {
            if (endTime == DataExtensions::MaxTime)
            {
                throw ArgumentException("EndTime must not be equal to DefaultEndTime.");
            }
            return endTime;
        }
        else
        {
            return getLastRecordTime(tr);
        }
    }
    int DataPage::addRecords(DateTime::Resolutions tr, const MetaEvents& mes, int offset)
    {
        if (mes.count() == 0 || mes.count() <= (uint)offset)
            return -1;
        
        if (_records.count() == 0)
        {
            startTime = mes[offset].timestamp;
        }
        assert(startTime > DataExtensions::MinTime);
        
        int pos = -1;
        int s = size();
        MetaEvent me;
        DateTime prevTime = getEndTime(tr);
        for (uint i = offset; i < mes.count(); i++)
        {
            me = mes[i];
            assert(me.timestamp >= startTime);
            if (i > (uint)offset)
            {
                prevTime = mes[i - 1].timestamp;
                assert(prevTime >= startTime);
            }
            
            Record record(me.type, me.value, me.timestamp.subtract(prevTime, tr), me.qualitystamp);
            s += record.size();
            if (s > MaxSize)
            {
                updateEndTime(me.timestamp);
                pos = i;
                break;
            }
            _records.add(record);
        }
        
        if (endTime == DataExtensions::MaxTime && isFull())
        {
            updateEndTime(tr);
        }
        return pos;
    }
    void DataPage::updateEndTime(DateTime::Resolutions tr)
    {
        updateEndTime(getLastRecordTime(tr));
    }
    void DataPage::toEvents(DateTime::Resolutions tr, MetaEvents& mes)
    {
        uint addTime = 0;
        DateTime time = startTime;
        for (uint i = 0; i < _records.count(); i++)
        {
            const Record record = _records[i];
            
            addTime += record.time;
            mes.add(record.toEvent(tagId, time.add(addTime, tr)));
        }
    }
    DateTime DataPage::getLastRecordTime(DateTime::Resolutions tr) const
    {
        assert(startTime > DataExtensions::MinTime);
        
        uint time = 0;
        for (uint i = 0; i < _records.count(); i++)
        {
            time += _records[i].time;
        }
        DateTime temp = startTime;
        return temp.add(time, tr);
    }
    void DataPage::updateEndTime(DateTime time)
    {
        endTime = time;
    }
    
    OverflowPage::OverflowPage() : DataPage()
    {
    }
    OverflowPage::OverflowPage(uint tagId, Tag::Type type) : DataPage(tagId, type)
    {
    }
    OverflowPage::OverflowPage(const OverflowPage& op)
    {
        copyFrom(&op);
    }
    void OverflowPage::copyFrom(const Page* page)
    {
        DataPage::copyFrom(page);
    }
    void OverflowPage::operator=(const OverflowPage& value)
    {
        copyFrom(&value);
    }
    void OverflowPage::writeInternal(Stream* stream) const
    {
    }
    void OverflowPage::readInternal(Stream* stream)
    {
    }
    OverflowPages::OverflowPages(bool autoDelete, uint capacity) : Vector<OverflowPage>(autoDelete, capacity)
    {
    }
    
    FilePage::FilePage() : FilePage("")
    {
    }
    FilePage::FilePage(const string& fileName)
    {
        this->version = Version("1.0.0.0");
        this->fileName = fileName;
        this->startTime = DataExtensions::MinTime;
        this->endTime = DataExtensions::MaxTime;
        this->currentPage = 0;
    }
    FilePage::FilePage(const FilePage& fp)
    {
        copyFrom(&fp);
    }
    int FilePage::size() const
    {
        int strSize = sizeof(ushort) + (int)fileName.length();
        return sizeof(Header) + sizeof(version) + strSize +
        DataExtensions::TimeSize +      // size of startTime
        DataExtensions::TimeSize +      // size of endTime
        sizeof(currentPage);
    }
    void FilePage::copyFrom(const Page* page)
    {
        const FilePage* fp = dynamic_cast<const FilePage*>(page);
        assert(fp);
        
        this->version = fp->version;
        this->fileName = fp->fileName;
        this->startTime = fp->startTime;
        this->endTime = fp->endTime;
        this->currentPage = fp->currentPage;
    }
    void FilePage::write(Stream* stream) const
    {
        if (!fileName.empty())
        {
#if DEBUG
            assert(stream->canSeek());
            int64_t startpos = stream->position();
#endif
            
            stream->writeUInt32(Header);
            version.write(stream);
            stream->writeStr(fileName);
            DataExtensions::writeDateTime(stream, startTime);
            DataExtensions::writeDateTime(stream, endTime);
            stream->writeUInt32(currentPage);
            
#if DEBUG
            int64_t endpos = MaxSize - (stream->position() - startpos);
            assert(endpos > 0);
#endif
        }
    }
    void FilePage::read(Stream* stream)
    {
        stream->readUInt32();   // skip header
        version.read(stream);
        fileName = stream->readStr();
        startTime = DataExtensions::readDateTime(stream);
        endTime = DataExtensions::readDateTime(stream);
        currentPage = stream->readUInt32();
    }
    void FilePage::operator=(const FilePage& value)
    {
        copyFrom(&value);
    }
    
    IndexPage::Record::Record() : Record(0, 0)
    {
    }
    IndexPage::Record::Record(uint time, uint pageNumber)
    {
        this->time = time;
        this->pageNumber = pageNumber;
    }
    int IndexPage::Record::size() const
    {
        return timeLength() + sizeof(pageNumber);
    }
    void IndexPage::Record::write(Stream* stream) const
    {
        DataExtensions::writeTime(stream, time);
        stream->writeUInt32(pageNumber);
    }
    void IndexPage::Record::read(Stream* stream)
    {
        time = DataExtensions::readTime(stream);
        pageNumber = stream->readUInt32();
    }
    int IndexPage::Record::maxSize()
    {
        return sizeof(uint64_t) + sizeof(int);
    }
    byte IndexPage::Record::timeLength() const
    {
        return DataExtensions::getTimeLength(time);
    }
    IndexPage::Records::Records(uint capacity) : Array<Record>(capacity)
    {
    }
    IndexPage::Record IndexPage::Records::getLastRecord() const
    {
        if (count() == 0)
            return Record();
        
        return this->at(count() - 1);
    }
    IndexPage::IndexPage() : RecordPage()
    {
        timeResolution = DateTime::Resolutions::ResNone;
    }
    IndexPage::IndexPage(uint tagId) : RecordPage(tagId)
    {
        timeResolution = DateTime::Resolutions::ResNone;
    }
    IndexPage::IndexPage(const IndexPage& ip)
    {
        copyFrom(&ip);
    }
    uint IndexPage::count() const
    {
        return _records.count();
    }
    bool IndexPage::isFull() const
    {
        if (RecordPage::isFull())
            return true;
        
        return (size() + Record::maxSize()) >= MaxSize;
    }
    int IndexPage::size() const
    {
        uint c = count();
        int s = RecordPage::size() + sizeof(c) + sizeof(timeResolution);
        for (uint i=0; i<_records.count(); i++)
        {
            s += _records[i].size();
        }
        return s;
    }
    void IndexPage::copyFrom(const Page* page)
    {
        RecordPage::copyFrom(page);
        
        const IndexPage* ip = dynamic_cast<const IndexPage*>(page);
        assert(ip);
        
        this->timeResolution = ip->timeResolution;
        this->_records.clear();
        this->_records.addRange(&ip->_records);
    }
    uint IndexPage::lastRecordNo() const
    {
        if (!isEmpty())
        {
            const Record record = _records[count() - 1];
            return record.pageNumber;
        }
        return 0;
    }
    void IndexPage::write(Stream* stream) const
    {
#if DEBUG
        assert(stream->canSeek());
        int64_t startpos = stream->position();
#endif
        
        RecordPage::write(stream);
        
        stream->writeByte(timeResolution);
        stream->writeUInt32(count());
        for (uint i=0; i<_records.count(); i++)
        {
            _records[i].write(stream);
        }
        
#if DEBUG
        int64_t endpos = MaxSize - (stream->position() - startpos);
        assert(endpos > 0);
#endif
    }
    void IndexPage::read(Stream* stream)
    {
        RecordPage::read(stream);
        
        timeResolution = (DateTime::Resolutions)stream->readByte();
        uint count = stream->readUInt32();
        _records.clear();
        for (uint i = 0; i < count; i++)
        {
            Record record;
            record.read(stream);
            _records.add(record);
        }
    }
    DateTime IndexPage::getEndTime(DateTime::Resolutions tr) const
    {
        assert(startTime > DataExtensions::MinTime);
        
        if (isEmpty())
        {
            return startTime;
        }
        else if (isFull())
        {
            assert(endTime != DataExtensions::MaxTime);
            return endTime;
        }
        else
        {
            return getLastRecordTime(tr);
        }
    }
    DateTime IndexPage::getLastRecordTime(DateTime::Resolutions tr) const
    {
        assert(startTime > DataExtensions::MinTime);
        
        uint time = 0;
        for (uint i = 0; i < _records.count(); i++)
        {
            time += _records[i].time;
        }
        return startTime.add(time, tr);
    }
    int IndexPage::addRecords(DateTime::Resolutions tr, const OverflowPages& ops, int offset)
    {
        if (ops.count() == 0 || ops.count() <= (uint)offset)
            return -1;
        
        if (_records.count() == 0)
        {
            startTime = ops[offset]->startTime;
        }
        assert(startTime > DataExtensions::MinTime);
        
        int pos = -1;
        int s = size();
        OverflowPage* op = NULL;
        DateTime prevTime = getEndTime(tr);
        for (uint i = offset; i < ops.count(); i++)
        {
            op = ops[i];
            assert(op->startTime >= startTime);
            if (i > (uint)offset)
            {
                prevTime = ops[i - 1]->startTime;
                assert(prevTime >= startTime);
            }
            
            Record record(op->startTime.subtract(prevTime, tr), op->currentPage);
            s += record.size();
            if (s > MaxSize)
            {
                updateEndTime(op->endTime);
                pos = i;
                break;
            }
            _records.add(record);
        }
        
        if (endTime == DataExtensions::MaxTime && isFull())
        {
            updateEndTime(tr);
        }
        return pos;
    }
    int IndexPage::addRecords(DateTime::Resolutions tr, const OverflowPage& op, int offset)
    {
        OverflowPages ops;
        OverflowPage* newop = new OverflowPage(op);
        ops.add(newop);
        return addRecords(tr, ops, offset);
    }
    void IndexPage::updateEndTime(DateTime::Resolutions tr)
    {
        updateEndTime(getLastRecordTime(tr));
    }
    int64_t IndexPage::getEventCount(Stream* stream) const
    {
        return getEventCount(stream, count());
    }
    int64_t IndexPage::getEventCount(Stream* stream, uint overflowPageCount) const
    {
        int64_t c = 0;
        for (uint i = 0; i < Math::min(overflowPageCount, count()); i++)
        {
            const Record record = _records[i];
            if (record.pageNumber > 0)
            {
                uint pos = record.pageNumber * Page::MaxSize;
                assert(pos < stream->length());
                stream->seek(pos, SeekOrigin::SeekBegin);
                OverflowPage* op = new OverflowPage();
                op->readHeader(stream);
                
                c += op->count();
            }
        }
        return c;
    }
    void IndexPage::toEvents(MetaEvents& mes, Stream* stream, const QueryCondition& condition) const
    {
        toEvents(mes, stream, count(), condition);
    }
    void IndexPage::toEvents(MetaEvents& mes, Stream* stream, uint overflowPageCount, const QueryCondition& condition) const
    {
        DateTime startTime = condition.isEmpty() ? condition.startTime : DataExtensions::MinTime;
        DateTime endTime = condition.isEmpty() ? condition.endTime : DataExtensions::MaxTime;
        
        uint addTime = 0;
        DateTime time = startTime;
        for (uint i = 0; i < Math::min(overflowPageCount, count()); i++)
        {
            const Record record = _records[i];
            addTime += record.time;
            
            if (record.pageNumber > 0)
            {
                bool hasRecord = condition.isEmpty();
                if (!hasRecord)
                {
                    DateTime t1 = time.add(addTime, timeResolution);
                    DateTime t2 = DataExtensions::MaxTime;
                    if (i + 1 < count())
                    {
                        t2 = time.add(addTime + _records[i + 1].time, timeResolution);
                        assert(t2 > t1);
                    }
                    else if (hasNextPage())
                    {
                        uint pos = nextPage * Page::MaxSize;
                        assert(pos < stream->length());
                        stream->seek(pos, SeekOrigin::SeekBegin);
                        IndexPage* ip = new IndexPage();
                        ip->read(stream);
                        
                        t2 = ip->startTime;
                        assert(t2 > t1);
                    }
                    hasRecord = DataExtensions::containTime(startTime, endTime, t1, t2);
                }
                
                if (hasRecord)
                {
                    uint pos = record.pageNumber * Page::MaxSize;
                    assert(pos < stream->length());
                    stream->seek(pos, SeekOrigin::SeekBegin);
                    OverflowPage* op = new OverflowPage();
                    op->read(stream);
                    MetaEvents temp;
                    op->toEvents(timeResolution, temp);
                    MetaEvent::binarySearch(temp, startTime, endTime, mes);
                }
            }
        }
    }
    void IndexPage::updateEndTime(DateTime time)
    {
        endTime = time;
    }
    void IndexPage::operator=(const IndexPage& value)
    {
        copyFrom(&value);
    }
    
    PrimaryPage::PrimaryPage() : DataPage()
    {
        archiveEndTime = DataExtensions::MaxTime;
        timeResolution = DateTime::Resolutions::ResNone;
    }
    PrimaryPage::PrimaryPage(uint tagId, Tag::Type type) : DataPage(tagId, type)
    {
        archiveEndTime = DataExtensions::MaxTime;
        timeResolution = DateTime::Resolutions::ResNone;
    }
    PrimaryPage::PrimaryPage(const PrimaryPage& pp)
    {
        copyFrom(&pp);
    }
    int PrimaryPage::size() const
    {
        return DataPage::size() + DataExtensions::TimeSize + sizeof(timeResolution);
    }
    void PrimaryPage::copyFrom(const Page* page)
    {
        DataPage::copyFrom(page);
        
        const PrimaryPage* pp = dynamic_cast<const PrimaryPage*>(page);
        assert(pp);

        this->archiveEndTime = pp->archiveEndTime;
        this->timeResolution = pp->timeResolution;
    }
    void PrimaryPage::readInternal(Stream* stream)
    {
        archiveEndTime = DataExtensions::readDateTime(stream);
        timeResolution = (DateTime::Resolutions)stream->readByte();
    }
    void PrimaryPage::writeInternal(Stream* stream) const
    {
        assert(timeResolution != DateTime::Resolutions::ResNone);
        DataExtensions::writeDateTime(stream, archiveEndTime);
        stream->writeByte(timeResolution);
    }
    void PrimaryPage::operator=(const PrimaryPage& value)
    {
        copyFrom(&value);
    }
}
