//
//  ArchiveManager.cpp
//  EVCommon
//
//  Created by baowei on 15/7/10.
//  Copyright (c) 2015 com. All rights reserved.
//

#include "ArchiveManager.h"
#include "common/diag/Debug.h"
#include "common/IO/Path.h"
#include "common/IO/Directory.h"
#include "ArchiveStatConfig.h"

namespace Archives
{
    ArchiveManager::ArchiveManager(ArchiveService* as)
    {
        if(as == NULL)
            throw ArgumentException("as");
        
        _service = as;
        _primaryArchive = NULL;
        _primaryArchiveId = 1;
    }
    ArchiveManager::~ArchiveManager()
    {
        _service = NULL;
        _primaryArchive = NULL;
    }
    
    bool ArchiveManager::initialize(const ConfigFile* file)
    {
        string path = Path::combine(file->rootPath, "archives");
        if (!Directory::exists(path))
        {
            Directory::createDirectory(path);
        }
        _path = path;
        
        ConfigFile temp(path, "arstat.config");
        string fullFileName = Path::combine(temp.rootPath, temp.fileName);
        if (File::exists(fullFileName))
        {
            ArchiveStatConfig reader(temp, _service);
            if(!reader.Configuration::load())
            {
                Trace::writeFormatLine("Failed to load config file. name: %s", temp.fileName.c_str());
                return false;
            }
        }
        
        if (_primaryArchive == NULL)
        {
            newArchive();
        }
        
        return true;
    }
    bool ArchiveManager::unInitialize()
    {
        _primaryArchive->close();
        
        return saveStatConfig();
    }
    
    bool ArchiveManager::saveStatConfig()
    {
        // save config file.
        ConfigFile temp(_path, "arstat.config");
        ArchiveStatConfig writer(temp, _service);
        return writer.Configuration::save();
    }
    
    void ArchiveManager::newArchive()
    {
        string fileName = newArchiveFileName();
        Archive* archive = new Archive(_service, fileName, _archiveFileSize);
        archive->open();
        _archives.add(archive);
        
        _primaryArchive = archive;
        
        saveStatConfig();
        
        Debug::writeFormatLine("New archive, FileName = %s, Time = %s", fileName.c_str(), DateTime::now().toString().c_str());
    }
    
    void ArchiveManager::clearAllArchives()
    {
        if (Directory::exists(_path))
        {
            Directory::deleteDirectory(_path);
            
            Directory::createDirectory(_path);
        }
    }
    
    void ArchiveManager::addRecords(uint tagId, const MetaEvents& mes, int offset)
    {
        assert(_primaryArchive != NULL);
        
        int pos = _primaryArchive->addRecords(tagId, mes, offset);
        if (pos >= 0)
        {
            // Shift archive.
            assert(_primaryArchive->isFull());
            
            _primaryArchive->shift();
            _primaryArchive->close();
            
            updatePrimaryArchiveId();
            
            newArchive();
            
            addRecords(tagId, mes, pos);
            
            saveStatConfig();
        }
    }
    
    string ArchiveManager::getAllArchiveInformations() const
    {
#if DEBUG
        Stopwatch sw("ArchiveManager::GetAllArchiveInformations");
#endif
        string path = _path;
        if (Directory::exists(path))
        {
            StringBuilder sb;
            for (uint i = 0; i < _archives.count(); i++)
            {
                Archive* archive = _archives[i];
                if (!archive->opened())
                {
                    archive->open();
                }
                
                sb.appendLine(archive->getInformation(i));
            }
            return sb.toString();
        }
        return "";
    }
    
    void ArchiveManager::query(const QueryCondition& condition, QueryResultInternal& result) const
    {
#if DEBUG
        Stopwatch sw("ArchiveManager::Query");
#endif
        for (uint i=0; i<_archives.count(); i++)
        {
            Archive* archive = _archives[i];
            if (!archive->opened())
            {
                archive->open();
            }
            
            QueryResultInternal aResult;
            archive->query(condition, aResult);
            result.add(aResult);
            if (aResult.finished())
            {
                break;
            }
        }
        
#if DEBUG
        sw.stop();
        Debug::writeLine(Convert::convertStr("Query the records of all archives, TagId = %d, Time: %d ms", condition.tagId, sw.elapsed()));
#endif
    }
    
    string ArchiveManager::newArchiveFileName() const
    {
        string fileName = Path::combine(_path, Convert::convertStr("archive.%03d", _primaryArchiveId));
        return fileName;
    }
    
    void ArchiveManager::updatePrimaryArchiveId()
    {
        _primaryArchiveId++;
    }
}
