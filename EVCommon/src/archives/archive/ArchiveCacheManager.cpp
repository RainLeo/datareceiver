//
//  ArchiveCacheManager.cpp
//  EVCommon
//
//  Created by baowei on 15/7/10.
//  Copyright (c) 2015 com. All rights reserved.
//

#include "ArchiveCacheManager.h"
#include "../ArchiveService.h"
#include "tag/TagService.h"

namespace Archives
{
    void checkProc(void* parameter)
    {
        ArchiveCacheManager* acm = (ArchiveCacheManager*)parameter;
        acm->checkProcInner();
    }
    void groupProc(void* parameter)
    {
        ArchiveCacheManager* acm = (ArchiveCacheManager*)parameter;
        acm->groupProcInner();
    }
    bool checkSize(const LoopMetaEvents* array)
    {
        bool result = array != NULL && (array->count() * ArchiveCacheManager::MetaEventSize >= ArchiveCacheManager::MaxCacheSize);
        return result;
    }
    
    const TimeSpan ArchiveCacheManager::Interval = TimeSpan(0, 15, 0);
    
    ArchiveCacheManager::ArchiveCacheManager(ArchiveService* as)
    {
        if(as == NULL)
            throw ArgumentException("as");
        
        _service = as;
        
#if DEBUG
        _highSpeed = false;
        _hasEvents = true;
        _recordCount = 0;
        _archiveCount = 0;
        _archivePerSecond = 0.0;
#endif
    }
    ArchiveCacheManager::~ArchiveCacheManager()
    {
        _service = NULL;
    }
    
    bool ArchiveCacheManager::initialize(const ConfigFile* file)
    {
        Locker locker(&_metaEventsMutex);
        _metaEvents.setCapacity(_service->tagManager()->tagCount());
        
        uint count = TimerGroupCount();
        DateTime now = DateTime::now();
        for (uint i = 0; i < count; i++)
        {
            mGroupTimes[i] = now.addSeconds(i * Interval.totalSeconds() / count);
        }
        
        _checkThread = new Thread();
        _checkThread->setName("archive_checkProc");
        _checkThread->startProc(checkProc, this, 1);
        
        _groupThread = new Thread();
        _groupThread->setName("archive_groupProc");
        _groupThread->startProc(groupProc, this, (uint)(Interval.totalSeconds() / count * 1000.0));
        
        return true;
    }
    bool ArchiveCacheManager::unInitialize()
    {
        archive();
        if(_checkThread != NULL)
        {
            _checkThread->stop();
            delete _checkThread;
            _checkThread = NULL;
        }
        
        if(_groupThread != NULL)
        {
            _groupThread->stop();
            delete _groupThread;
            _groupThread = NULL;
        }
        
#if DEBUG
        if (_archiveCount > 0)
        {
            double archivePerSecond = _archivePerSecond / (double)_archiveCount;
            Debug::writeFormatLine("Archive EPS: %lf, RecordCount = %d", archivePerSecond, _recordCount);
        }
#endif
        
        return true;
    }
    
#if DEBUG
    void ArchiveCacheManager::setHighSpeed(bool highSpeed)
    {
        if (highSpeed)
        {
            _highSpeed = highSpeed;
        }
        else
        {
            _hasEvents = false;
        }
    }
#endif
    
    void ArchiveCacheManager::addMetaEvent(const MetaEvent& me)
    {
        Locker locker(&_metaEventsMutex);
        if (me.isValid())
        {
            LoopMetaEvents* array = _metaEvents[me.tagId - 1];
            if (array == NULL)
            {
                int maxCount = MaxArrayCount;
#if DEBUG
                if (_highSpeed)
                {
                    maxCount = 10000;
                }
#endif
                array = new LoopMetaEvents(maxCount);
                _metaEvents.set(me.tagId - 1, array, true);
            }
            
            //            Debug::writeFormatLine("array.count=%d", array->count());
            assert(!array->full());
            
            array->enqueue(me);
        }
    }
    
    MetaEvent ArchiveCacheManager::getFirstMetaEvent(uint tagId)
    {
        MetaEvent me;
        Locker locker(&_metaEventsMutex);
        
        if (tagId > 0 && tagId <= _metaEvents.count())
        {
            LoopMetaEvents* array = _metaEvents[me.tagId - 1];
            if (array != NULL && !array->empty())
            {
                MetaEvents mes;
                array->copyTo(&mes);
                me = mes[0];
            }
        }
        return me;
    }
    
    MetaEvent ArchiveCacheManager::getLastMetaEvent(uint tagId)
    {
        MetaEvent me;
        Locker locker(&_metaEventsMutex);
        
        if (tagId > 0 && tagId <= _metaEvents.count())
        {
            LoopMetaEvents* array = _metaEvents[me.tagId - 1];
            if (array != NULL && !array->empty())
            {
                MetaEvents mes;
                array->copyTo(&mes);
                me = mes[mes.count() - 1];
            }
        }
        return me;
    }
    
    void ArchiveCacheManager::query(QueryCondition condition, QueryResultInternal& result)
    {
#if DEBUG
        Stopwatch sw("ArchiveCacheManager::Query");
#endif
        uint tagId = condition.tagId;
        DateTime startTime = condition.startTime;
        DateTime endTime = condition.endTime;
        
        Locker locker(&_metaEventsMutex);
        if (tagId > 0 && tagId <= _metaEvents.count())
        {
            LoopMetaEvents* array = _metaEvents[tagId - 1];
            if (array != NULL && !array->empty())
            {
                MetaEvents mes;
                array->copyTo(&mes);
                
                FilterManager* fm = _service->filterManager();
                assert(fm != NULL);
                MetaEvent lastEvent = fm->getSnapshot(tagId);
                
                bool finished = false;
                if (condition.specifiedTimeValue())
                {
                    MetaEvent newme = MetaEvent::find(mes, lastEvent, condition.timestamp(), condition.retrievalMode, finished);
                    if (newme.isValid())
                    {
                        result.QueryResult::add(newme);
                    }
                }
                else
                {
                    MetaEvents newmes;
                    if (condition.hasNumberOfValues())
                    {
                    }
                    else if (!condition.hasInterval())
                    {
                        MetaEvent::find(mes, lastEvent, startTime, endTime, condition.boundaryType, finished, newmes);
                    }
                    else
                    {
                        assert(condition.hasTimeRange());
                        assert(condition.hasInterval());
                    }
                    
                    if (newmes.count() > 0)
                    {
                        result.QueryResult::add(newmes);
                    }
                }
                
                if (finished)
                {
                    result.setFinished(true);
                }
            }
        }
#if DEBUG
        sw.stop();
        Debug::writeFormatLine("Query all the records of cache, TagId = %d, Count = %d, Time: %d ms", tagId, result.count(), sw.elapsed());
#endif
    }
    
    uint ArchiveCacheManager::TimerGroupCount() const
    {
        ArchiveTagManager* tm = _service->tagManager();
        uint tagCount = tm->tagCount();
        uint groupCount = (tagCount / TagCountPerGroup + (tagCount % TagCountPerGroup > 0 ? 1 : 0));
        if (groupCount > Interval.totalSeconds())
        {
            groupCount = (uint)Interval.totalSeconds();
        }
        return groupCount;
    }
    
    uint ArchiveCacheManager::TagGroupCount() const
    {
        uint count = TagCountPerGroup;
        if (count > TimerGroupCount())
        {
            ArchiveTagManager* tm = _service->tagManager();
            uint tagCount = tm->tagCount();
            count = (tagCount / TimerGroupCount() + (tagCount % TimerGroupCount() > 0 ? 1 : 0));
        }
        return count;
    }
    
    void ArchiveCacheManager::groupProcInner()
    {
        for (uint i = 0; i < mGroupTimes.count(); i++)
        {
            DateTime now = DateTime::now();
            if ((now - mGroupTimes[i]) >= Interval)
            {
                mGroupTimes[i] = now;
                
                uint tgc = TagGroupCount();
                uint tagFromId = (i * tgc + 1);
                uint tagToId = tagFromId + tgc - 1;
                archive(NULL, tagFromId, tagToId);
            }
        }
    }
    
    void ArchiveCacheManager::checkProcInner()
    {
        archive(checkSize);
#if DEBUG
        if (_highSpeed)
        {
            if (!_hasEvents)
            {
                Locker locker(&_metaEventsMutex);
                bool isEmpty = true;
                for (uint i = 0; i < _metaEvents.count(); i++)
                {
                    const LoopMetaEvents* array = _metaEvents[i];
                    if (checkSize(array))
                    {
                        isEmpty = false;
                        break;
                    }
                }
                if (isEmpty)
                {
                    _highSpeed = false;
                    
                    Debug::writeLine("ArchiveCacheManager(HighSpeed) has been completed!");
                }
            }
        }
#endif
    }
    
    void ArchiveCacheManager::archive(check_callback func, uint tagFromId, uint tagToId)
    {
        Vector<MetaEvents> events;
        _metaEventsMutex.lock();
        for (uint i = 0; i < _metaEvents.count(); i++)
        {
            LoopMetaEvents* array = _metaEvents[i];
            if (array != NULL && !array->empty())
            {
                if (func == NULL || func(array))
                {
                    uint tagId = i + 1;
                    if (tagId >= tagFromId && tagId <= tagToId)
                    {
                        MetaEvents* mes = new MetaEvents();
                        array->copyTo(mes);
                        array->clear();
                        
                        assert(mes->count() > 0 && mes->at(0).isValid());
                        events.add(mes);
                    }
                }
            }
        }
        _metaEventsMutex.unlock();
        
#if DEBUG
        Stopwatch* sw = NULL;
        if (events.count() > 0)
        {
            sw = new Stopwatch("ArchiveCacheManager::archive", 100);
        }
#endif
        for (uint i=0; i<events.count(); i++)
        {
            const MetaEvents* mes = events[i];
            uint tagId = mes->at(0).tagId;
            archive(tagId, *mes);
        }
        
#if DEBUG
        if (sw != NULL)
        {
            int recordCount = 0;
            for (uint i=0; i<events.count(); i++)
            {
                const MetaEvents* mes = events[i];
                recordCount += mes->count();
            }
            
            sw->stop();
            uint elapsed = (uint)sw->elapsed();
            if(elapsed > 1)
            {
                double archivePerSecond = (double)recordCount / ((double)elapsed / 1000.0);
                _archiveCount++;
                _archivePerSecond += archivePerSecond;
                _recordCount += recordCount;
                
                Debug::writeFormatLine("Archived, Time: %s, TagFromId = %s, TagToId = %s, TagCount = %d, RecordCount = %d, Elapsed: %d ms, EPS: %.0lf",
                                       DateTime::now().toString().c_str(),
                                       tagFromId == 0 ? "Min" : Convert::convertStr(tagFromId).c_str(),
                                       tagToId == UINT32_MAX ? "Max" : Convert::convertStr(tagToId).c_str(),
                                       events.count(), recordCount, elapsed, archivePerSecond);
            }
            delete sw;
        }
#endif
    }
    
    void ArchiveCacheManager::archive(uint tagId, const MetaEvents& mes)
    {
        ArchiveManager* am = _service->archiveManager();
        if (am != NULL)
        {
            am->addRecords(tagId, mes);
        }
    }
}