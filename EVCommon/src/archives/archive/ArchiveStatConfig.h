//
//  ArchiveStatConfig.h
//  EVCommon
//
//  Created by baowei on 15/7/8.
//  Copyright (c) 2015 com. All rights reserved.
//

#ifndef __EVCommon__ArchiveStatConfig__
#define __EVCommon__ArchiveStatConfig__

#include <string>
#include "common/common_global.h"
#include "common/system/Singleton.h"
#include "common/xml/Configuration.h"

using namespace std;
using namespace Common;

namespace Archives
{
    class ArchiveService;
    class COMMON_EXPORT ArchiveStatConfig : public Configuration
    {
    public:
        ArchiveStatConfig(const ConfigFile& file, ArchiveService* service);
        ~ArchiveStatConfig();
        
    protected:
        bool load(XmlTextReader& reader) override;
        bool save(XmlTextWriter& writer) override;
        
    private:
        ArchiveService* _service;
    };
}

#endif /* defined(__EVCommon__ArchiveStatConfig__) */
