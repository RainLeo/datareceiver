//
//  Enums.h
//  EVCommon
//
//  Created by baowei on 15/7/8.
//  Copyright (c) 2015年 com. All rights reserved.
//

#ifndef EVCommon_Enums_h
#define EVCommon_Enums_h

namespace Archives
{
    enum RetrievalModes
    {
        /// <summary>
        /// interpolates the value at the specified time, or follows Previous behavior.
        /// </summary>
        RMAuto,
        /// <summary>
        /// retrieves the value exactly matching or preceding the specified timestamp.
        /// </summary>
        RMPrevious,
        /// <summary>
        /// retrieves the value preceding the specified timestamp.
        /// </summary>
        RMPreviousOnly,
        /// <summary>
        /// interpolates the value at the specified time, or follows Previous Only behavior.
        /// </summary>
        RMInterpolated,
        /// <summary>
        /// retrieves the value exactly matching or following the specified timestamp.
        /// </summary>
        RMNext,
        /// <summary>
        /// retrieves the value following the specified timestamp.
        /// </summary>
        RMNextOnly,
        /// <summary>
        /// retrieves only a value exactly matching the timestamp, or returns No events found if no value exists.
        /// </summary>
        RMExtractTime,
    };
    
    enum BoundaryTypes
    {
        /// <summary>
        /// Returns values at start and end times, if they exist, or the nearest values occurring within the range.
        /// </summary>
        BTInside,
        /// <summary>
        /// Returns the closest values occurring immediately outside the range.
        /// </summary>
        BTOutside,
        /// <summary>
        /// Returns interpolated values at start and end times.
        /// </summary>
        BTInterpolated,
    };
}

#endif
