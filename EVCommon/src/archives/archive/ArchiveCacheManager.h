//
//  ArchiveCacheManager.h
//  EVCommon
//
//  Created by baowei on 15/7/10.
//  Copyright (c) 2015 com. All rights reserved.
//

#ifndef __EVCommon__ArchiveCacheManager__
#define __EVCommon__ArchiveCacheManager__

#include <string>
#include <mutex>
#include "common/common_global.h"
#include "common/system/DateTime.h"
#include "common/data/Vector.h"
#include "common/data/Array.h"
#include "common/system/Convert.h"
#include "common/thread/Locker.h"
#include "common/thread/Thread.h"
#include "common/IO/File.h"
#include "common/diag/Stopwatch.h"
#include "base/BaseService.h"
#include "../query/QueryResult.h"
#include "tag/Tag.h"
#include "Archive.h"

using namespace Common;

namespace Archives
{
    class ArchiveService;
    class ArchiveCacheManager
    {
    public:
        ArchiveCacheManager(ArchiveService* as);
        ~ArchiveCacheManager();
        
#if DEBUG
        void setHighSpeed(bool highSpeed = true);
#endif
        
        void addMetaEvent(const MetaEvent& me);
        MetaEvent getFirstMetaEvent(uint tagId);
        MetaEvent getLastMetaEvent(uint tagId);
        
        void query(QueryCondition condition, QueryResultInternal& result);
        
    private:
        bool initialize(const ConfigFile* file);
        bool unInitialize();
        
        uint TimerGroupCount() const;
        uint TagGroupCount() const;
        
        friend void checkProc(void* parameter);
        void checkProcInner();
        
        friend void groupProc(void* parameter);
        void groupProcInner();
        
        friend bool checkSize(const LoopMetaEvents* array);
        
        void archive(uint tagId, const MetaEvents& mes);
        typedef bool (*check_callback)(const LoopMetaEvents*);
        void archive(check_callback func = NULL, uint tagFromId = 0, uint tagToId = UINT32_MAX);
        
        bool ThreadIsFinished();
        
    private:
        friend ArchiveService;
        
        const static int MaxCacheSize = 4096;
        const static int TagCountPerGroup = 500;
        const static TimeSpan Interval;
        const static int MetaEventSize = sizeof(MetaEvent);
        const static int MaxArrayCount = (MaxCacheSize / MetaEventSize) + (MaxCacheSize / MetaEventSize) / 5;
        
        ArchiveService* _service;
        
        Thread* _groupThread;
        Thread* _checkThread;
        LoopMetaEventss _metaEvents;
        mutex _metaEventsMutex;
        
        Dictionary<int, DateTime> mGroupTimes;
        
#if DEBUG
        bool _highSpeed;
        bool _hasEvents;
        long _recordCount;
        int _archiveCount;
        double _archivePerSecond;
#endif
    };
}

#endif /* defined(__EVCommon__ArchiveCacheManager__) */
