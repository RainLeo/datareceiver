//
//  DataExtensions.h
//  EVCommon
//
//  Created by baowei on 15/7/6.
//  Copyright (c) 2015年 com. All rights reserved.
//

#ifndef __EVCommon__DataExtensions__
#define __EVCommon__DataExtensions__

#include <string>
#include <mutex>
#include "common/common_global.h"
#include "common/system/DateTime.h"
#include "common/data/Vector.h"
#include "common/data/Array.h"
#include "common/data/ByteArray.h"
#include "common/system/Convert.h"
#include "common/thread/Locker.h"
#include "../query/QueryResult.h"

using namespace std;
using namespace Common;

namespace Archives
{
    class DataExtensions
    {
    public:
        static void writeTime(Stream* stream, uint value, bool bigEndian = true);
        static uint readTime(Stream* stream, bool bigEndian = true);
        static byte getTimeLength(uint value);
        static void getTimeBytes(ByteArray& buffer, uint value, bool bigEndian = true);
        
        static void writeDateTime(Stream* stream, DateTime time, bool bigEndian = true);
        static DateTime readDateTime(Stream* stream, bool bigEndian = true);
        
        static void writeQuality(Stream* stream, Tag::Qualitystamp value);
        static Tag::Qualitystamp readQuality(Stream* stream);
        static byte getQualityLength(Tag::Qualitystamp value);
        
        static bool containTime(DateTime startTime, DateTime endTime, DateTime t1, DateTime t2);
        static bool containAllTime(DateTime startTime, DateTime endTime, DateTime t1, DateTime t2);
        
    public:
        static const int TimeSize = 6;
        
        static const DateTime MinTime;
        static const DateTime MaxTime;
        
    private:
        
    };
}

#endif /* defined(__EVCommon__DataExtensions__) */
