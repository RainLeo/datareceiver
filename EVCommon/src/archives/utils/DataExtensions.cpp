//
//  DataExtensions.cpp
//  EVCommon
//
//  Created by baowei on 15/7/6.
//  Copyright (c) 2015年 com. All rights reserved.
//

#include "DataExtensions.h"

namespace Archives
{
    const DateTime DataExtensions::MinTime = DateTime(1970, 1, 1);
    const DateTime DataExtensions::MaxTime = DateTime(2038, 1, 1);
    
    void DataExtensions::writeTime(Stream* stream, uint value, bool bigEndian)
    {
        if (stream == NULL)
        {
            throw ArgumentNullException("stream");
        }
        
        ByteArray buffer;
        getTimeBytes(buffer, value, bigEndian);
        stream->write(buffer.data(), 0, buffer.count());
    }
    
    uint DataExtensions::readTime(Stream* stream, bool bigEndian)
    {
        if (stream == NULL)
        {
            throw ArgumentNullException("stream");
        }
#if DEBUG
        int64_t pos = stream->position();
#endif
        const int bufferLength = 8;
        byte buffer[bufferLength];
        memset(&buffer, 0, bufferLength);
        byte time = 0;
        int index = 0;
        do
        {
            time = stream->readByte();
            buffer[index++] = time;
        } while (!((time & 0x80) == 0x80 && index < bufferLength));
        
        buffer[index - 1] &= 0x7f;

        int64_t temp = 0;
        if (bigEndian)
        {
            for (int i = 0; i < index; i++)
            {
                temp <<= 8;
                temp |= buffer[i];
            }
            
        }
        else
        {
            for (int i = index - 1; i >= 0; i--)
            {
                temp <<= 8;
                temp |= buffer[i];
            }
        }

        uint64_t result = 0;
        for (int i = 0; i < bufferLength; i++)
        {
            result |= (((temp >> (i * 8)) & 0xff) << (i * 7));
        }
#if DEBUG
        assert(stream->position() - pos == index);
#endif
        return (uint)result;
    }    
    byte DataExtensions::getTimeLength(uint value)
    {
        if (value <= 0x7f)
            return 1;
        
        const int bufferLength = 8;
        int prev = -1;
        int cur = -1;
        byte i = 0;
        for (i = 0; i < bufferLength; i++)
        {
            cur = (int)(((uint64_t)value >> (i * 7)) & 0x7f);
            if (prev > 0 && cur == 0)
            {
                break;
            }
            prev = cur;
        }
        return i;
    }
    void DataExtensions::getTimeBytes(ByteArray& buffer, uint value, bool bigEndian)
    {
        const int bufferLength = 8;
        uint64_t result = 0;
        for (int i = 0; i < bufferLength; i++)
        {
            result |= ((((uint64_t)value >> (i * 7)) & 0x7f) << (i * 8));
        }
        
        byte temp[bufferLength];
        if (bigEndian)
        {
            temp[0] = (byte)((result & 0xFF00000000000000) >> 56);
            temp[1] = (byte)((result & 0x00FF000000000000) >> 48);
            temp[2] = (byte)((result & 0x0000FF0000000000) >> 40);
            temp[3] = (byte)((result & 0x000000FF00000000) >> 32);
            temp[4] = (byte)((result & 0x00000000FF000000) >> 24);
            temp[5] = (byte)((result & 0x0000000000FF0000) >> 16);
            temp[6] = (byte)((result & 0x000000000000FF00) >> 8);
            temp[7] = (byte)((result & 0x00000000000000FF) >> 0);
        }
        else
        {
            temp[7] = (byte)((result & 0xFF00000000000000) >> 56);
            temp[6] = (byte)((result & 0x00FF000000000000) >> 48);
            temp[5] = (byte)((result & 0x0000FF0000000000) >> 40);
            temp[4] = (byte)((result & 0x000000FF00000000) >> 32);
            temp[3] = (byte)((result & 0x00000000FF000000) >> 24);
            temp[2] = (byte)((result & 0x0000000000FF0000) >> 16);
            temp[1] = (byte)((result & 0x000000000000FF00) >> 8);
            temp[0] = (byte)((result & 0x00000000000000FF) >> 0);
        }
        
        int offset = 0;
        int length = getTimeLength(value);
        if (bigEndian)
        {
            offset = bufferLength - length;
            temp[bufferLength - 1] |= 0x80;
        }
        else
        {
            offset = 0;
            temp[length - 1] |= 0x80;
        }
        
        buffer.addRange(temp, offset, length);
    }

    void DataExtensions::writeDateTime(Stream* stream, DateTime time, bool bigEndian)
    {
        assert(time >= MinTime);
        
        int64_t milliseconds = (int64_t)(time - MinTime).totalMilliseconds();
        assert(((milliseconds >> TimeSize * 8) & 0xff) == 0);
        
        assert(TimeSize == 6);
        stream->writeInt48(milliseconds, bigEndian);
    }
    DateTime DataExtensions::readDateTime(Stream* stream, bool bigEndian)
    {
        assert(TimeSize == 6);
        int64_t milliseconds = stream->readInt48(bigEndian);
        return MinTime + TimeSpan::fromMilliseconds((double)milliseconds);
    }
    
    void DataExtensions::writeQuality(Stream* stream, Tag::Qualitystamp value)
    {
        if (stream == NULL)
        {
            throw ArgumentNullException("stream");
        }
        
        if (!Tag::IsGoodQuality(value))
        {
            stream->writeUInt16(value);
        }
        else
        {
            stream->writeByte(((byte)0x80));
        }
    }
    Tag::Qualitystamp DataExtensions::readQuality(Stream* stream)
    {
        if (stream == NULL)
        {
            throw ArgumentNullException("stream");
        }
        
        byte quality1 = stream->readByte();
        if ((quality1 & 0x80) == 0x80)
        {
            return Tag::Qualitystamp::Good;
        }
        else
        {
            byte quality2 = stream->readByte();
            return (Tag::Qualitystamp)((quality2 << 8) + quality1);
        }
    }
    byte DataExtensions::getQualityLength(Tag::Qualitystamp value)
    {
        return (Tag::IsGoodQuality(value) ? (byte)1 : (byte)sizeof(value));
    }
    
    bool DataExtensions::containAllTime(DateTime startTime, DateTime endTime, DateTime t1, DateTime t2)
    {
        return (startTime >= t1 && t2 >= endTime);
    }
    bool DataExtensions::containTime(DateTime startTime, DateTime endTime, DateTime t1, DateTime t2)
    {
        //t1, t2 是文件的起始、结束时间，t2>=t1；start, end是查询的起始、结束时间，end>=start
        //下面6种情况：
        //t1	    start	end	    t2
        //t1	    start	t2	    end
        //t1	    t2	    start	end
        //start	    t1	    end	    t2
        //start	    end	    t1	    t2
        //start	    t1	    t2	    end
        
        if (endTime >= startTime && t2 >= t1)
        {
            DateTime from = startTime > t1 ? startTime : t1;
            DateTime to = endTime < t2 ? endTime : t2;
            
            return to >= from;
        }
        return false;
    }
}