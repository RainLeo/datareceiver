//
//  ArchiveService.h
//  EVCommon
//
//  Created by baowei on 15/7/8.
//  Copyright (c) 2015 com. All rights reserved.
//

#ifndef __EVCommon__ArchiveService__
#define __EVCommon__ArchiveService__

#include <string>
#include <mutex>
#include "common/common_global.h"
#include "common/system/DateTime.h"
#include "common/data/Vector.h"
#include "common/data/Array.h"
#include "common/system/Convert.h"
#include "common/thread/Locker.h"
#include "common/IO/File.h"
#include "common/diag/Stopwatch.h"
#include "base/BaseService.h"
#include "archive/ArchiveManager.h"
#include "archive/ArchiveCacheManager.h"
#include "query/QueryManager.h"
#include "filters/FilterManager.h"
#include "tag/ArchiveTagManager.h"
#include "tag/TagService.h"

using namespace Common;
using namespace EVCommon;

namespace Archives
{
    class ArchiveService : public EVCommon::BaseService
    {
    public:
        ArchiveService();
        ~ArchiveService();
        
        bool initialize(const ConfigFile* file = NULL) override;
        bool unInitialize() override;
        
        ArchiveTagManager* tagManager() const;
        ArchiveManager* archiveManager() const;
        ArchiveCacheManager* cacheManager() const;
        QueryManager* queryManager() const;
        FilterManager* filterManager() const;
        
        string name() const override
        {
            return "archive";
        }
        
    private:
        TagService* tagService();
        
    private:
        ArchiveTagManager* _tagManager;
        ArchiveManager* _archiveManager;
        ArchiveCacheManager* _cacheManager;
        QueryManager* _queryManager;
        FilterManager* _filterManager;
    };
}

#endif /* defined(__EVCommon__ArchiveService__) */
