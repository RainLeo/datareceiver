//
//  ArchiveConfig.h
//  EVCommon
//
//  Created by baowei on 15/7/8.
//  Copyright (c) 2015 com. All rights reserved.
//

#ifndef __EVCommon__ArchiveConfig__
#define __EVCommon__ArchiveConfig__

#include <string>
#include "common/common_global.h"
#include "common/system/Singleton.h"
#include "common/xml/Configuration.h"

using namespace std;
using namespace Common;

namespace Archives
{
    class ArchiveService;
    class COMMON_EXPORT ArchiveConfig : public Configuration
    {
    public:
        ArchiveConfig(const ConfigFile& file, ArchiveService* service);
        ~ArchiveConfig();
        
    protected:
        bool load(XmlTextReader& reader);
        
    private:
        static int64_t fromSizeString(const string& sizeString);
        
    private:
        ArchiveService* _service;
    };
}

#endif /* defined(__EVCommon__ArchiveConfig__) */
