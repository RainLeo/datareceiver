//
//  ArchiveService.cpp
//  EVCommon
//
//  Created by baowei on 15/7/8.
//  Copyright (c) 2015 com. All rights reserved.
//

#include "ArchiveService.h"
#include "tag/TagService.h"
#include "ArchiveConfig.h"
#include "common/diag/Trace.h"

namespace Archives
{
    void archive_tagValueChanged(void* owner, void* sender, EventArgs* args)
    {
        ArchiveTag* aTag = static_cast<ArchiveTag*>(owner);
        if(aTag != NULL)
        {
            ArchiveService* as = aTag->service();
            if(as != NULL)
            {
                Tag* tag = static_cast<Tag*>(sender);
                assert(tag);
                
                Tag::Value value;
                if(Tag::changeValue(tag, aTag->type(), value))
                {
                    MetaEvent me(aTag->id(), aTag->type(), value, tag->timestamp(), tag->qualitystamp());
                    as->filterManager()->addMetaEvent(me);
                }
            }
        }
    }
    
    ArchiveService::ArchiveService()
    {
        _tagManager = new ArchiveTagManager(this);
        _archiveManager = new ArchiveManager(this);
        _cacheManager = new ArchiveCacheManager(this);
        _queryManager = new QueryManager(this);
        _filterManager = new FilterManager(this);
    }
    ArchiveService::~ArchiveService()
    {
        delete _tagManager;
        _tagManager = NULL;
        delete _archiveManager;
        _archiveManager = NULL;
        delete _cacheManager;
        _cacheManager = NULL;
        delete _queryManager;
        _queryManager = NULL;
        delete _filterManager;
        _filterManager = NULL;
    }
    
    bool ArchiveService::initialize(const ConfigFile* file)
    {
        ConfigFile temp = *file;
        temp.fileName = "archive.config";
        ArchiveConfig reader(temp, this);
        if(!reader.Configuration::load())
        {
            Trace::writeFormatLine("Failed to load config file. name: %s", temp.fileName.c_str());
            return false;
        }
        
        if(!_tagManager->initialize(file))
            return false;
        if(!_archiveManager->initialize(file))
            return false;
        if(!_cacheManager->initialize(file))
            return false;
        if(!_filterManager->initialize(file))
            return false;
        if(!_queryManager->initialize(file))
            return false;
        
        TagService* ts = tagService();
        assert(ts);
        
        ArchiveTagManager* tm = tagManager();
        const ArchiveTags& tags = tm->allTags();
        for (uint i=0; i<tags.count(); i++)
        {
            ArchiveTag* tag = tags[i];
            if(tag->validLinkedId())
            {
                tag->_service = this;
                ts->addTagValueChangedHandler(tag->linkedId(), Delegate(tag, archive_tagValueChanged));
            }
        }
        
        return true;
    }
    bool ArchiveService::unInitialize()
    {
        TagService* ts = tagService();
        assert(ts);
        
        ArchiveTagManager* tm = tagManager();
        const ArchiveTags& tags = tm->allTags();
        for (uint i=0; i<tags.count(); i++)
        {
            ArchiveTag* tag = tags[i];
            if(tag->validLinkedId())
            {
                tag->_service = NULL;
                ts->removeTagValueChangedHandler(tag->linkedId(), Delegate(tag, archive_tagValueChanged));
            }
        }
        
        if(!_queryManager->unInitialize())
            return false;
        if(!_filterManager->unInitialize())
            return false;
        if(!_cacheManager->unInitialize())
            return false;
        if(!_archiveManager->unInitialize())
            return false;
        if(!_tagManager->unInitialize())
            return false;
        
        return true;
    }
    
    ArchiveTagManager* ArchiveService::tagManager() const
    {
        return _tagManager;
    }
    ArchiveManager* ArchiveService::archiveManager() const
    {
        return _archiveManager;
    }
    ArchiveCacheManager* ArchiveService::cacheManager() const
    {
        return _cacheManager;
    }
    QueryManager* ArchiveService::queryManager() const
    {
        return _queryManager;
    }
    FilterManager* ArchiveService::filterManager() const
    {
        return _filterManager;
    }
    
    TagService* ArchiveService::tagService()
    {
        return getService<TagService>();
    }
}
