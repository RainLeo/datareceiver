//
//  FilterManager.cpp
//  EVCommon
//
//  Created by baowei on 15/7/10.
//  Copyright (c) 2015 com. All rights reserved.
//

#include "FilterManager.h"
#include "../ArchiveService.h"
#include "../archive/ArchiveCacheManager.h"

using namespace EVCommon;

namespace Archives
{
    FilterManager::Snapshot::Snapshot(ArchiveService* as)
    {
        if(as == NULL)
            throw ArgumentException("as");
        
        _service = as;
    }
    
    MetaEvent FilterManager::Snapshot::update(const MetaEvent& me)
    {
        if (me.isValid())
        {
            MetaEvent old;
            Locker locker(&_metaEventsMutex);
            
            uint index = me.tagId - 1;
            old = index < _metaEvents.count() ? _metaEvents[index] : MetaEvent();
            DateTime::Resolutions tr = getTimeResolution(me.tagId);
            if (!old.isValid() || me.timestamp.subtract(old.timestamp, tr) > 0)
            {
                _metaEvents.set(index, me, true);
            }
            else
            {
                return MetaEvent();
            }
            
            return old;
        }
        return MetaEvent();
    }
    
    MetaEvent FilterManager::Snapshot::getSnapshot(uint tagId)
    {
        if (tagId > 0 && tagId <= _metaEvents.count())
        {
            Locker locker(&_metaEventsMutex);
            return _metaEvents[tagId - 1];
        }
        return MetaEvent();
    }
    
    void FilterManager::Snapshot::getAllSnapshot(MetaEvents& mes)
    {
        Locker locker(&_metaEventsMutex);
        {
            mes.addRange(&_metaEvents);
        }
    }
    DateTime::Resolutions FilterManager::Snapshot::getTimeResolution(uint tagId)
    {
        ArchiveTagManager* tm = _service->tagManager();
        DateTime::Resolutions tr = DateTime::Resolutions::ResNone;
        const ArchiveTag* tag = tm->getTag(tagId);
        if(tag != NULL)
        {
            tr = tag->timeResolutions();
            assert(tr != DateTime::Resolutions::ResNone);
        }
        return tr;
    }
    
    void filterProc(void* parameter)
    {
        FilterManager* fm = (FilterManager*)parameter;
        fm->filterProcInner();
    }
    FilterManager::FilterManager(ArchiveService* as) : _snapshot(as)
    {
        if(as == NULL)
            throw ArgumentException("as");
        
        _service = as;
        
#if DEBUG
        _hasEvents = true;
#endif
    }
    FilterManager::~FilterManager()
    {
        _service = NULL;
    }
    
    bool FilterManager::initialize(const ConfigFile* file)
    {
        ArchiveTagManager* tm = _service->tagManager();
        if (tm != NULL)
        {
            Locker locker(&_metaEventsMutex);
            _metaEvents.setMaxLength(300 * tm->tagCount());
        }
        
        _filterThread = new Thread();
        _filterThread->setName("archive_filterProc");
        _filterThread->startProc(filterProc, this, 1);
        
        return true;
    }
    bool FilterManager::unInitialize()
    {
        filterSnapshot();
        if (_filterThread != NULL)
        {
            _filterThread->stop();
            delete _filterThread;
            _filterThread = NULL;
        }
        
        return true;
    }
    
#if DEBUG
    void FilterManager::setHighSpeed(bool highSpeed)
    {
        if (highSpeed)
        {
            _highSpeed = highSpeed;
            ArchiveTagManager* tm = _service->tagManager();
            if (tm != NULL)
            {
                Locker locker(&_metaEventsMutex);
                _metaEvents.setMaxLength(3000 * tm->tagCount());
            }
        }
        else
        {
            _hasEvents = false;
        }
    }
#endif
    
    void FilterManager::addMetaEvent(const MetaEvent& me)
    {
        Locker locker(&_metaEventsMutex);
        assert(!_metaEvents.full());
        
        _metaEvents.enqueue(me);
    }
    
    MetaEvent FilterManager::getSnapshot(uint tagId)
    {
        MetaEvent me;
        Locker locker(&_metaEventsMutex);
        {
            me = _snapshot.getSnapshot(tagId);
        }
        return me;
    }
    
    void FilterManager::query(const QueryCondition& condition, QueryResultInternal& result)
    {
#if DEBUG
        Stopwatch sw("FilterManager::Query");
#endif
        uint tagId = condition.tagId;
        
        bool finished = false;
        MetaEvent newme;
        MetaEvent me = getSnapshot(tagId);
        if (me.isValid())
        {
            if (condition.specifiedTimeValue())
            {
                newme = MetaEvent::find(me, condition.timestamp(), condition.retrievalMode, finished);
            }
            else
            {
                if (condition.hasNumberOfValues())
                {
                    newme = MetaEvent::find(me, condition.startTime, condition.numberOfValues, condition.boundaryType, finished);
                }
                else if (!condition.hasInterval())
                {
                    newme = MetaEvent::find(me, condition.startTime, condition.endTime, condition.boundaryType, finished);
                }
                else
                {
                    assert(condition.hasTimeRange());
                    assert(condition.hasInterval());
                    
                    if (condition.startTime > me.timestamp)
                    {
                        finished = true;
                    }
                }
            }
            
            if (newme.isValid())
            {
                result.QueryResult::add(newme);
            }
            if (finished)
            {
                result.setFinished(true);
            }
        }
#if DEBUG
        sw.stop();
        Debug::writeFormatLine("Query all the records of filter, TagId = %d, Count = %d, Time: %d ms",
                               tagId, (me.isValid() ? 1 : 0), sw.elapsed());
#endif
    }
    
    void FilterManager::filterProcInner()
    {
        filter();
#if DEBUG
        if (_highSpeed)
        {
            if (!_hasEvents)
            {
                Locker locker(&_metaEventsMutex);
                if (_metaEvents.count() == 0)
                {
                    _highSpeed = false;
                    
                    Debug::writeLine("FilterManager(HighSpeed) has been completed!");
                }
            }
        }
#endif
    }
    
    void FilterManager::filter()
    {
        // ExceptionTest
        _metaEventsMutex.lock();
        MetaEvent me = _metaEvents.front();
        if (me.isValid())
        {
            _metaEvents.dequeue();
        }
        _metaEventsMutex.unlock();
        
        if (me.isValid())
        {
            bool exception = _exceptionTest.ExceptionReport(me);
            
            // Snapshot
            MetaEvent cme;
            if (exception)
            {
                cme = _snapshot.update(me);
            }
            
            bool c = compress(cme);
            
            // Scaling
            if (!c)
            {
                MetaEvent sme = scale(cme);
                if (sme.isValid())
                {
                    addtoCache(cme);
                }
            }
        }
    }
    
    void FilterManager::filterSnapshot()
    {
        MetaEvents mes;
        _snapshot.getAllSnapshot(mes);
        for (uint i=0; i<mes.count(); i++)
        {
            MetaEvent me = mes[i];
            bool c = compress(me);
            
            // Scaling
            if (!c)
            {
                MetaEvent sme = scale(me);
                if (sme.isValid())
                {
                    addtoCache(me);
                }
            }
        }
    }
    
    bool FilterManager::compress(const MetaEvent& me)
    {
        // Compression
        bool compress = true;
        if (me.isValid())
        {
            compress = _compression.compress(me);
        }
        return compress;
    }
    
    void FilterManager::addtoCache(const MetaEvent& me)
    {
        ArchiveCacheManager* acm = _service->cacheManager();
        if (acm != NULL)
        {
            acm->addMetaEvent(me);
        }
    }
    MetaEvent FilterManager::scale(const MetaEvent& me)
    {
        // Todo: scale
        return me;
    }
}
