//
//  FilterManager.h
//  EVCommon
//
//  Created by baowei on 15/7/10.
//  Copyright (c) 2015 com. All rights reserved.
//

#ifndef __EVCommon__FilterManager__
#define __EVCommon__FilterManager__

#include <string>
#include <mutex>
#include "common/common_global.h"
#include "common/system/DateTime.h"
#include "common/data/Vector.h"
#include "common/data/Array.h"
#include "common/system/Convert.h"
#include "common/thread/Locker.h"
#include "common/IO/File.h"
#include "common/diag/Stopwatch.h"
#include "base/BaseService.h"
#include "../query/QueryResult.h"
#include "../query/QueryCondition.h"

using namespace Common;

namespace Archives
{
    class ArchiveService;
    class FilterManager
    {
    public:
        class Compression
        {
        public:
            bool compress(const MetaEvent& me)
            {
                return false;
            }
        };
        class ExceptionTest
        {
        public:
            bool ExceptionReport(const MetaEvent& me)
            {
                if (me.isValid())
                {
                    return true;
                }
                return false;
            }
        };
        class Scaling
        {
        public:
            Scaling()
            {
            }
        };
        class Snapshot
        {
        public:
            Snapshot(ArchiveService* ts);
            
            MetaEvent update(const MetaEvent& me);
            MetaEvent getSnapshot(uint tagId);
            void getAllSnapshot(MetaEvents& mes);
            
        private:
            DateTime::Resolutions getTimeResolution(uint tagId);
            
        private:
            ArchiveService* _service;
            
            MetaEvents _metaEvents;
            mutex _metaEventsMutex;
        };
        
        FilterManager(ArchiveService* as);
        ~FilterManager();
        
#if DEBUG
        void setHighSpeed(bool highSpeed = true);
#endif
        
        void addMetaEvent(const MetaEvent& me);
        
        MetaEvent getSnapshot(uint tagId);
        
        void query(const QueryCondition& condition, QueryResultInternal& result);
        
    private:
        bool initialize(const ConfigFile* file);
        bool unInitialize();
        
        void filter();
        
        void filterSnapshot();
        
        bool compress(const MetaEvent& me);
        
        friend void filterProc(void* parameter);
        void filterProcInner();
        
        void addtoCache(const MetaEvent& me);
        MetaEvent scale(const MetaEvent& me);
        
    private:
        friend ArchiveService;
        
        ArchiveService* _service;
        
        Thread* _filterThread;
        LoopMetaEvents _metaEvents;
        mutex _metaEventsMutex;
        ExceptionTest _exceptionTest;
        Snapshot _snapshot;
        Compression _compression;
        Scaling _scaling;
#if DEBUG
        bool _highSpeed;
        bool _hasEvents;
#endif
    };
}

#endif /* defined(__EVCommon__FilterManager__) */
