//
//  ArchiveConfig.cpp
//  EVCommon
//
//  Created by baowei on 15/7/8.
//  Copyright (c) 2015 com. All rights reserved.
//

#include "ArchiveConfig.h"
#include "common/system/TimeSpan.h"
#include "common/IO/Directory.h"
#include "common/IO/File.h"
#include "common/IO/Path.h"
#include "common/system/Convert.h"
#include "common/xml/XmlTextReader.h"
#include "common/xml/XmlDocument.h"
#include "ArchiveService.h"

using namespace Common;

namespace Archives
{
    ArchiveConfig::ArchiveConfig(const ConfigFile& file, ArchiveService* service) : Configuration(file)
    {
        if(service == NULL)
            throw ArgumentException("service");
        
        _service = service;
    }
    
    ArchiveConfig::~ArchiveConfig()
    {
        _service = NULL;
    }
    
    bool ArchiveConfig::load(XmlTextReader& reader)
    {
        if (reader.nodeType() == XmlNodeType::Element &&
            reader.localName() == "archive")
        {
            _service->archiveManager()->_archiveFileSize = fromSizeString(reader.getAttribute("filesize"));
            while (reader.read())
            {
                if (reader.nodeType() == XmlNodeType::Element &&
                    reader.localName() == "tag")
                {
                    uint tagId;
                    if(Convert::parseUInt32(reader.getAttribute("id"), tagId))
                    {
                        string name = reader.getAttribute("name");
                        Tag::Type type = Tag::fromTypeStr(reader.getAttribute("type"));
                        if(Tag::isValidType(type))
                        {
                            ArchiveTag* tag = new ArchiveTag(tagId, type);
                            
                            DateTime::Resolutions tr = DateTime::fromResolutionStr(reader.getAttribute("resolution"));
                            tag->setTimeResolutions(tr);
                            
                            uint linkedId = 0;
                            if(Convert::parseUInt32(reader.getAttribute("linkedid"), linkedId))
                                tag->setLinkedId(linkedId);
                            
                            _service->tagManager()->addTag(tag);
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }
    
    int64_t ArchiveConfig::fromSizeString(const string& sizeString)
    {
        if(sizeString.empty())
            return 0;
        
        int64_t size = 0;
        size_t unitLength = sizeString.length() - 1;
        const char unit = sizeString[unitLength];
        switch (unit)
        {
            case 'M':
            case 'm':
                Convert::parseInt64(sizeString.substr(0, unitLength), size);
                size *= 1024 * 1024;
                break;
            case 'K':
            case 'k':
                Convert::parseInt64(sizeString.substr(0, unitLength), size);
                size *= 1024;
                break;
            default:
                Convert::parseInt64(sizeString, size);
                size *= 1;
                break;
        }
        return size;
    }
}
