#ifndef KEY_H
#define KEY_H

#include <string>
#include <mutex>
#include "common/common_global.h"
#include "common/thread/Thread.h"
#include "common/system/TimeSpan.h"
#include "common/drawing/Point.h"
#include "common/data/Array.h"
#include "KeyValues.h"

using namespace std;
using namespace Common;

namespace EVCommon
{
    struct Key
    {
    public:
        KeyValues key;
        
        Key(KeyValues key = KeyValues::None)
        {
            this->key = key;
        }
        
        inline bool alt() const
        {
            return (key & KeyValues::Alt) == KeyValues::Alt;
        }
        inline bool control() const
        {
            return (key & KeyValues::Control) == KeyValues::Control;
        }
        inline bool shift() const
        {
            return (key & KeyValues::Shift) == KeyValues::Shift;
        }
    };
    typedef Array<Key> Keys;
}

#endif // KEY_H
