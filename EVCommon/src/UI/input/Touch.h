#ifndef TOUCH_H
#define TOUCH_H

#include <string>
#include <mutex>
#include "common/common_global.h"
#include "common/thread/Thread.h"
#include "common/system/TimeSpan.h"
#include "common/drawing/Point.h"
#include "common/data/Array.h"

using namespace std;
using namespace Common;

namespace Drawing
{
    struct Touch
    {
    public:
        enum Phase
        {
            Began,             // whenever a finger touches the surface.
            Moved,             // whenever a finger moves on the surface.
            Stationary,        // whenever a finger is touching the surface but hasn't moved since the previous event.
            Ended,             // whenever a finger leaves the surface.
            Cancelled,         // whenever a touch doesn't end but we need to stop tracking (e.g. putting device to face)
        };
        
        TimeSpan interval;
        Phase phase;
        uint tapCount;
        Drawing::Point location;
        Drawing::Point previousLocation;
    };
    typedef Array<Touch> Touches;
}

#endif // TOUCH_H
