#ifndef MOUSE_H
#define MOUSE_H

#include <string>
#include <mutex>
#include "common/common_global.h"
#include "common/thread/Thread.h"
#include "common/system/TimeSpan.h"
#include "common/drawing/Point.h"
#include "common/data/Array.h"

using namespace std;
using namespace Common;

namespace Drawing
{
    struct Mouse
    {
    public:
        enum Buttons
        {
            None = 0x00000000,
            Left = 0x00100000,
            Right = 0x00200000,
            Middle = 0x00400000,
            XButton1 = 0x00800000,
            XButton2 = 0x01000000,
        };
        
        Buttons button;
        int clicks;
        float deltaX;
        float deltaY;
        float deltaZ;
        Drawing::Point location;
        
        Mouse()
        {
            button = None;
            clicks = 0;
            deltaX = 0.0f;
            deltaY = 0.0f;
            deltaZ = 0.0f;
        }
    };
    typedef Array<Mouse> Mouses;
}

#endif // MOUSE_H
