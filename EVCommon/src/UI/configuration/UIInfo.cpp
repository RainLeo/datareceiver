﻿#include "UIInfo.h"
#include "common/system/Convert.h"

using namespace Common;

namespace Drawing
{
    UIInfo::UIInfo()
    {
		doubleBuffer = false;
    }

	void UIInfo::addPage(const PageInfo* page)
	{
		_pages.add(page);
	}
    const PageInfos* UIInfo::pages() const
    {
        return &_pages;
    }
    uint UIInfo::pageCount() const
    {
        return _pages.count();
    }
}
