﻿#include "PageReader.h"
#include "common/system/Convert.h"
#include "../utils/Size.h"
#include "../utils/SkConvert.h"
#include "../commands/Command.h"
#include "../components/GPage.h"

using namespace Common;

namespace Drawing
{
    PageReader::PageReader(const ConfigFile& file, GPage* page) : Configuration(file)
    {
        if(page == NULL)
            throw ArgumentException("page");
        _page = page;
    }
    PageReader::~PageReader()
    {
        _page = NULL;
    }
    
    bool PageReader::load(XmlTextReader& reader)
    {
        // read from config node
        if (reader.nodeType() == XmlNodeType::Element &&
            reader.localName() == "Page")
        {
            Brush background;
            if(Brush::parse(reader.getAttribute("background"), background))
            {
                background.setConfigSource(reader.configFile());
                _page->setBackground(background);
            }
            string openedCommandStr = reader.getAttribute("openedCommand");
            if (!openedCommandStr.empty())
            {
                //Command::parse(openedCommandStr, _uiInfo.loadedCommand);
            }
            bool transfer;
            if(Convert::parseBoolean(reader.getAttribute("transfer"), transfer))
            {
                _page->setTransfer(transfer);
            }
            
            while (reader.read())
            {
                if (reader.name() == "Shapes" && reader.nodeType() == XmlNodeType::Element)
                {
                    while (reader.read() &&
                           !(reader.name() == "Shapes" && reader.nodeType() == XmlNodeType::EndElement))
                    {
                        if (reader.nodeType() == XmlNodeType::Element)
                        {
                            GShape* shape = GShape::createShape(reader.name());
                            if(shape != NULL)
                            {
                                shape->read(reader);
                                shape->updateMatrix();
                                _page->addShape(shape);
                            }
                        }
                    }
                }
                else if (reader.name() == "Animations" && reader.nodeType() == XmlNodeType::Element)
                {
                    while (reader.read() &&
                           !(reader.name() == "Animations" && reader.nodeType() == XmlNodeType::EndElement))
                    {
                        if (reader.nodeType() == XmlNodeType::Element)
                        {
                            Animation* animation = Animation::createAnimation(reader.name());
                            if(animation != NULL)
                            {
                                animation->read(reader);
                                _page->addAnimation(animation);
                            }
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }
}
