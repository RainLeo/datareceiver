#ifndef PAGEREADER_H
#define PAGEREADER_H

#include <string>
#include "common/common_global.h"
#include "common/xml/Configuration.h"
#include "UIInfo.h"

using namespace std;

namespace Drawing
{
    class GPage;
    class COMMON_EXPORT PageReader : public Configuration
    {
    public:
        PageReader(const ConfigFile& file, GPage* page);
        ~PageReader();
        
    protected:
        bool load(XmlTextReader& reader);
        
    private:
        GPage* _page;
    };
}

#endif	// PAGEREADER_H
