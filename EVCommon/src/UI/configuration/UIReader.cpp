﻿#include "UIReader.h"
#include "common/system/Convert.h"
#include "common/IO/Path.h"
#include "../utils/Size.h"

using namespace Common;

namespace Drawing
{
    UIReader::UIReader(const ConfigFile& file, UIInfo* uiInfo) : Configuration(file)
    {
        if(uiInfo == NULL)
            throw ArgumentException("uiInfo");
        _uiInfo = uiInfo;
    }
    UIReader::~UIReader()
    {
        _uiInfo = NULL;
    }
    
    bool UIReader::load(XmlTextReader& reader)
    {
        // read from config node
        if (reader.nodeType() == XmlNodeType::Element &&
            reader.localName() == "UI")
        {
            string sourceSizeStr = reader.getAttribute("sourceSize");
            string actualSizeStr = reader.getAttribute("actualSize");
            string doubleBufferStr = reader.getAttribute("doubleBuffer");
            
            if (!sourceSizeStr.empty())
            {
                Size::parse(sourceSizeStr, _uiInfo->sourceSize);
            }
            if (!actualSizeStr.empty())
            {
                Size::parse(actualSizeStr, _uiInfo->actualSize);
            }
            if (!actualSizeStr.empty())
            {
				Convert::parseBoolean(doubleBufferStr, _uiInfo->doubleBuffer);
            }

			while (reader.read())
			{
				if (reader.localName() == "Pages" && reader.nodeType() == XmlNodeType::Element)
				{
					string rootPath = reader.getAttribute("rootPath");
					if (!rootPath.empty())
					{
						_uiInfo->rootFile = _file;
                        string path = Path::getDirectoryName(_file.fileName);
                        _uiInfo->rootFile.fileName = Path::combine(path, rootPath);
					}
					continue;
				}
				if (reader.localName() == "Page" && reader.nodeType() == XmlNodeType::Element)
				{
					bool mainPage = false;
					Convert::parseBoolean(reader.getAttribute("mainPage"), mainPage);
					bool loadedInMemory = false;
					Convert::parseBoolean(reader.getAttribute("loadedInMemory"), loadedInMemory);
					bool loadedAtStartup = false;
					Convert::parseBoolean(reader.getAttribute("loadedAtStartup"), loadedAtStartup);

					PageInfo* page = new PageInfo(reader.getAttribute("name"),
                                  _uiInfo->rootFile,
                                  reader.getAttribute("source"),
                                  mainPage, loadedAtStartup, loadedInMemory);
					_uiInfo->addPage(page);
					continue;
				}
			}
            return true;
        }
        return false;
    }
}
