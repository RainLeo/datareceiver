#ifndef PAGEINFO_H
#define PAGEINFO_H

#include <string>
#include "common/common_global.h"
#include "common/data/Vector.h"
#include "common/xml/ConfigFile.h"

using namespace std;
using namespace Common;

namespace Drawing
{
    class PageInfo
    {
    public:
        PageInfo();
		PageInfo(const string& name, const ConfigFile& rootFile, const string& source, bool mainPage = false, bool loadedAtStartup = false, bool loadedInMemory = false);
        
    public:
		string name;
		string source;
		bool mainPage;
		bool loadedInMemory;
		bool loadedAtStartup;
        
        ConfigFile rootFile;
    };
	typedef Vector<PageInfo> PageInfos;
}

#endif	// PAGEINFO_H
