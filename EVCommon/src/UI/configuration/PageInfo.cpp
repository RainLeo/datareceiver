﻿#include "PageInfo.h"
#include "common/system/Convert.h"

using namespace Common;

namespace Drawing
{
    PageInfo::PageInfo() : PageInfo("", ConfigFile(), "")
    {
    }
	PageInfo::PageInfo(const string& name, const ConfigFile& rootFile, const string& source, bool mainPage, bool loadedAtStartup, bool loadedInMemory)
    {
		this->name = name;
		this->source = source;
		this->mainPage = mainPage;
		this->loadedAtStartup = loadedAtStartup;
		this->loadedInMemory = loadedInMemory;
        this->rootFile = rootFile;
    }
}
