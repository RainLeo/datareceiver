#ifndef UIINFO_H
#define UIINFO_H

#include <string>
#include "common/common_global.h"
#include "../utils/Size.h"
#include "PageInfo.h"

using namespace std;

namespace Drawing
{
    class UIInfo
    {
    public:
        UIInfo();

		void addPage(const PageInfo* page);
        const PageInfos* pages() const;
        uint pageCount() const;
        
    public:
        Drawing::Size sourceSize;
		Drawing::Size actualSize;
		bool doubleBuffer;
        
        ConfigFile rootFile;

	private:
		PageInfos _pages;
    };
}

#endif	// UIINFO_H
