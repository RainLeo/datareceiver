#ifndef UIREADER_H
#define UIREADER_H

#include <string>
#include "common/common_global.h"
#include "common/xml/Configuration.h"
#include "UIInfo.h"

using namespace std;

namespace Drawing
{
    class COMMON_EXPORT UIReader : public Configuration
    {
    public:
        UIReader(const ConfigFile& file, UIInfo* uiInfo);
        ~UIReader();
        
    protected:
        bool load(XmlTextReader& reader);
        
    private:
        UIInfo* _uiInfo;
    };
}

#endif	// UIREADER_H
