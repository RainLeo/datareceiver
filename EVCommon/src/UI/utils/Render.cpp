#include "Render.h"

namespace Drawing
{
    Point Render::measure(const Paint& paint, const string& text, const Rectangle& bounds, const Font& font)
    {
        float x = bounds.x;
        float y = bounds.y + font.size;
        switch (font.align) {
            case Font::ContentAlignment::TopLeft:
                break;
            case Font::ContentAlignment::MiddleLeft:
            {
                float height = bounds.height > font.size ? (bounds.height - font.size)/2.0f : 0.0f;
                y += height;
                break;
            }
            case Font::ContentAlignment::BottomLeft:
            {
                float height = bounds.height > font.size ? (bounds.height - font.size)/2.0f : 0.0f;
                y += height;
                break;
            }
            case Font::ContentAlignment::TopCenter:
            {
                float height = bounds.height > font.size ? (bounds.height - font.size)/2.0f : 0.0f;
                y += height;
                break;
            }
            case Font::ContentAlignment::MiddleCenter:
            {
                SkRect temp;
                paint.measureText(text.c_str(), text.length(), &temp);
                float width = bounds.width > temp.width() ? (bounds.width - temp.width())/2.0f : 0.0f;
                x += width;
                float height = bounds.height > font.size ? (bounds.height - font.size)/2.0f : 0.0f;
                y += height;
                break;
            }
            case Font::ContentAlignment::BottomCenter:
            {
                SkRect temp;
                paint.measureText(text.c_str(), text.length(), &temp);
                float width = bounds.width > temp.width() ? (bounds.width - temp.width())/2.0f : 0.0f;
                x += width;
                float height = bounds.height - font.size;
                y += height;
                break;
            }
            case Font::ContentAlignment::TopRight:
            {
                SkRect temp;
                paint.measureText(text.c_str(), text.length(), &temp);
                float width = bounds.width > temp.width() ? (bounds.width - temp.width()) : 0.0f;
                x += width;
                break;
            }
            case Font::ContentAlignment::MiddleRight:
            {
                SkRect temp;
                paint.measureText(text.c_str(), text.length(), &temp);
                float width = bounds.width > temp.width() ? (bounds.width - temp.width()) : 0.0f;
                x += width;
                float height = bounds.height - font.size;
                y += height;
                break;
            }
            case Font::ContentAlignment::BottomRight:
            {
                SkRect temp;
                paint.measureText(text.c_str(), text.length(), &temp);
                float width = bounds.width > temp.width() ? (bounds.width - temp.width()) : 0.0f;
                x += width;
                float height = bounds.height - font.size;
                y += height;
                break;
            }
            default:
                break;
        }
        return Point(x, y);
    }
    
    void Render::drawText(Graphics* graphics, Paint& paint, const string& text, const Rectangle& bounds, const Font& font)
    {
        if(text.length() == 0)
            return;
        
        const char* str = text.c_str();
        size_t length = text.length();
        if(font.autoScale)
        {
            paint.setTextSize(bounds.height);
            SkRect temp;
            paint.measureText(str, length, &temp);
            paint.setTextScaleX(bounds.height / temp.height());
            graphics->drawText(str, length, bounds.x, bounds.y - temp.y(), paint);
        }
        else
        {
            Point position = Render::measure(paint, text, bounds, font);
            graphics->drawText(str, length, position.x, position.y, paint);
        }
    }
}
