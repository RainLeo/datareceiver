#ifndef RENDER_H
#define RENDER_H

#include <string>
#include "common/common_global.h"
#include "common/data/Array.h"
#include "Point.h"
#include "Font.h"
#include "Rectangle.h"
#include "Graphics.h"

using namespace std;
using namespace Common;

namespace Drawing
{
    class Render
    {
    public:
        static Point measure(const Paint& paint, const string& text, const Rectangle& bounds, const Font& font);
        
        static void drawText(Graphics* graphics, Paint& paint, const string& text, const Rectangle& bounds, const Font& font);
    };
}

#endif /* RENDER_H */
