#include "common/system/Convert.h"
#include "common/data/PrimitiveType.h"
#include "../utils/Color.h"
#include "SkConvert.h"
#include "SkGradientShader.h"

using namespace Common;

namespace Drawing
{
    string SkPointConvert::toString(const SkPoint& value)
    {
        return Convert::convertStr("%f,%f", value.x(), value.y());
    }
    bool SkPointConvert::parse(const string& str, SkPoint& value)
    {
        StringArray texts;
        Convert::splitStr(str, ',', texts);
        if (texts.count() == 2)
        {
            float x = 0.f, y = 0.f;
            if(Convert::parseSingle(Convert::trimStr(texts[0]), x) &&
               Convert::parseSingle(Convert::trimStr(texts[1]), y))
            {
                value = SkPoint::Make(x, y);
                return true;
            }
        }
        return false;
    }
    // point:{0, 0};point:{300, 300}
    // 0, 0;300, 300
    bool SkPointConvert::parse(const string& str, SkPoints& value)
    {
        StringArray texts;
        Convert::splitStr(str, ';', texts);
        if (texts.count() > 0)
        {
            for (uint i=0; i<texts.count(); i++)
            {
                SkPoint point;
                
                Convert::KeyPairs pairs;
                if(Convert::splitItems(texts[i], pairs))
                {
                    for (uint j=0; j<pairs.count(); j++)
                    {
                        const Convert::KeyPair* kp = pairs[j];
                        if(String::stringEquals(kp->name, "point") &&
                           parse(kp->value, point))
                        {
                            value.add(point);
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    if(parse(texts[i], point))
                    {
                        value.add(point);
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        return false;
    }
    SkPoint SkPointConvert::toSkPoint(const Point& from)
    {
        return SkPoint::Make(from.x, from.y);
    }
    void SkPointConvert::toSkPoint(const Points& from, SkPoints& to)
    {
        for (uint i=0; i<from.count(); i++)
        {
            to.add(toSkPoint(from[i]));
        }
    }
    SkPoint SkPointConvert::convert(const Point& value)
    {
        return SkPoint::Make(value.x, value.y);
    }
    Point SkPointConvert::convert(const SkPoint& value)
    {
        return Point(value.x(), value.y());
    }
    
    SkColor SkColorConvert::toSkColor(const Color& from)
    {
        return from.toArgb();
    }
    void SkColorConvert::toSkColor(const Colors& from, SkColors& to)
    {
        for (uint i=0; i<from.count(); i++)
        {
            to.add(toSkColor(from[i]));
        }
    }
    
    string SkSizeConvert::toString(const SkSize& value)
    {
        return Convert::convertStr("%f,%f", value.width(), value.height());
    }
    bool SkSizeConvert::parse(const string& str, SkSize& value)
    {
        StringArray texts;
        Convert::splitStr(str, ',', texts);
        if (texts.count() == 2)
        {
            float width = 0.f, height = 0.f;
            if(Convert::parseSingle(Convert::trimStr(texts[0]), width) &&
               Convert::parseSingle(Convert::trimStr(texts[1]), height))
            {
                value = SkSize::Make(width, height);
                return true;
            }
        }
        return false;
    }
    
    string SkRectConvert::toString(const SkRect& value)
    {
        return Convert::convertStr("%f,%f,%f,%f", value.x(), value.y(), value.width(), value.height());
    }
    bool SkRectConvert::parse(const string& str, SkRect& value)
    {
        StringArray texts;
        Convert::splitStr(str, ',', texts);
        if (texts.count() == 4)
        {
            float x = 0.f, y = 0.f, width = 0.f, height = 0.f;
            if(Convert::parseSingle(Convert::trimStr(texts[0]), x) &&
               Convert::parseSingle(Convert::trimStr(texts[1]), y) &&
               Convert::parseSingle(Convert::trimStr(texts[2]), width) &&
               Convert::parseSingle(Convert::trimStr(texts[3]), height))
            {
                value = SkRect::MakeXYWH(x, y, width, height);
                return true;
            }
        }
        return false;
    }
    SkRect SkRectConvert::convert(const Rectangle& value)
    {
        return SkRect::MakeXYWH(value.x, value.y, value.width, value.height);
    }
    Rectangle SkRectConvert::convert(const SkRect& value)
    {
        return Rectangle(value.x(), value.y(), value.width(), value.height());
    }
    Rectangle SkRectConvert::convert(const SkIRect& value)
    {
        return Rectangle((float)value.x(), (float)value.y(), (float)value.width(), (float)value.height());
    }
    
    string SkPathConvert::toString(const SkPath& value)
    {
        return "";
    }
    bool SkPathConvert::parse(const string& str, SkPath& value)
    {
        return false;
    }
    
    string SkPaintConvert::toString(const SkPaint& value)
    {
        return "";
    }
    bool SkPaintConvert::parse(const string& str, SkPaint& stroke, SkPaint& fill)
    {
        // text:{size:20;scaleX:0;skewX:0;encoding:utf8};stroke:{width:2;color:{gold};miter:0;cap:butt;join:miter};fill:{color:{yellow}}
        StringArray texts;
        Convert::splitItems(str, texts);
        if(texts.count() > 0)
        {
            bool parsed = false;
            for (uint i=0; i<texts.count(); i++)
            {
                StringArray values;
                Convert::splitItems(texts[i], values, ':');
                if(values.count() == 2)
                {
                    string name = Convert::trimStr(values[0], ' ', '{', '}');
                    string value = Convert::trimStartStr(values[1], ' ', '{', '}');
                    value = Convert::trimEndStr(value, ' ', '{', '}');
                    
                    if(String::stringEquals(name, "text", true))
                    {
                        parsed = true;
                        if(!parseText(value, fill))
                            return false;
                    }
                    else if(String::stringEquals(name, "stroke", true))
                    {
                        parsed = true;
                        if(!parseStroke(value, stroke))
                            return false;
                        
                        stroke.setStyle(SkPaint::Style::kStroke_Style);
                    }
                    else if(String::stringEquals(name, "fill", true))
                    {
                        parsed = true;
                        if(!parseFill(value, fill))
                            return false;
                        
                        fill.setStyle(SkPaint::Style::kFill_Style);
                    }
                }
            }
            return parsed;
        }
        return false;
        
        //        linear:{points:{point:{0, 0};point:{300, 300}};colors:{color:{A:255;R:0;G:255;B:0};color:{red}};mode:mirror}
        //        SkPoint linearPoints[] = {
        //            {0, 0},
        //            {300, 300}
        //        };
        //        SkColor linearColors[] = {SK_ColorGREEN, SK_ColorBLACK};
        //
        //        SkShader* shader = SkGradientShader::CreateLinear(
        //                                                          linearPoints, linearColors, NULL, 2,
        //                                                          SkShader::kMirror_TileMode);
        //        SkAutoUnref shader_deleter(shader);
        //
        //        value.setShader(shader);
        //        value.setFlags(SkPaint::kAntiAlias_Flag);
        //        value.setTextSize(20);
        
        // the order we read must match the order we wrote in flatten()
        // paint:{text:{size:20;scaleX:0;skewX:0;encoding:utf8};stroke:{width:2;color:{gold};miter:0;cap:butt;join:miter};fill:{color:{yellow}}}
        
        //        this->setTextSize(read_scalar(pod));
        //        this->setTextScaleX(read_scalar(pod));
        //        this->setTextSkewX(read_scalar(pod));
        //        this->setTextEncoding(static_cast<TextEncoding>((tmp >> 0) & 0xFF));
        //
        //        this->setStrokeWidth(read_scalar(pod));
        //        this->setStrokeMiter(read_scalar(pod));
        //        this->setStrokeCap(static_cast<Cap>((tmp >> 24) & 0xFF));
        //        this->setStrokeJoin(static_cast<Join>((tmp >> 16) & 0xFF));
        //
        //        this->setColor(*pod++);
        //
        //        this->setStyle(static_cast<Style>((tmp >> 8) & 0xFF));
        
        //        if (flatFlags & kHasTypeface_FlatFlag) {
        //            this->setTypeface(buffer.readTypeface());
        //        } else {
        //            this->setTypeface(NULL);
        //        }
        //
        //        if (flatFlags & kHasEffects_FlatFlag) {
        //            SkSafeUnref(this->setPathEffect(buffer.readPathEffect()));
        //            SkSafeUnref(this->setShader(buffer.readShader()));
        //            SkSafeUnref(this->setXfermode(buffer.readXfermode()));
        //            SkSafeUnref(this->setMaskFilter(buffer.readMaskFilter()));
        //            SkSafeUnref(this->setColorFilter(buffer.readColorFilter()));
        //            SkSafeUnref(this->setRasterizer(buffer.readRasterizer()));
        //            SkSafeUnref(this->setLooper(buffer.readDrawLooper()));
        //            SkSafeUnref(this->setImageFilter(buffer.readImageFilter()));
        //
        //            if (buffer.readBool()) {
        //                this->setAnnotation(SkAnnotation::Create(buffer))->unref();
        //            }
        //        } else {
        //            this->setPathEffect(NULL);
        //            this->setShader(NULL);
        //            this->setXfermode(NULL);
        //            this->setMaskFilter(NULL);
        //            this->setColorFilter(NULL);
        //            this->setRasterizer(NULL);
        //            this->setLooper(NULL);
        //            this->setImageFilter(NULL);
        //        }
    }
    bool SkPaintConvert::parseText(const string& str, SkPaint& value)
    {
        // size:20;scaleX:0;skewX:0;encoding:utf8
        Convert::KeyPairs pairs;
        if(Convert::splitItems(str, pairs))
        {
            float size = 0.0f, scaleX = 0.0f, skewX = 0.0f;
            for (uint i=0; i<pairs.count(); i++)
            {
                const Convert::KeyPair* kp = pairs[i];
                if(String::stringEquals(kp->name, "size", true) &&
                   Convert::parseSingle(kp->value, size))
                {
                    value.setTextSize(size);
                }
                else if(String::stringEquals(kp->name, "scaleX", true) &&
                        Convert::parseSingle(kp->value, scaleX))
                {
                    value.setTextScaleX(scaleX);
                }
                else if(String::stringEquals(kp->name, "skewX", true) &&
                        Convert::parseSingle(kp->value, skewX))
                {
                    value.setTextSkewX(skewX);
                }
            }
            return true;
        }
        return false;
    }
    bool SkPaintConvert::parseStroke(const string& str, SkPaint& value)
    {
        // stroke:{width:2;color:{gold};miter:0;cap:butt;join:miter}
        Convert::KeyPairs pairs;
        if(Convert::splitItems(str, pairs))
        {
            Color color;
            float width = 1.0f, miter = 4.0f;
            for (uint i=0; i<pairs.count(); i++)
            {
                const Convert::KeyPair* kp = pairs[i];
                if(String::stringEquals(kp->name, "color", true) &&
                   Color::parse(kp->value, color))
                {
                    value.setColor(color.toArgb());
                }
                else if(String::stringEquals(kp->name, "width", true) &&
                        Convert::parseSingle(kp->value, width))
                {
                    value.setStrokeWidth(width);
                }
                else if(String::stringEquals(kp->name, "miter", true) &&
                        Convert::parseSingle(kp->value, miter))
                {
                    value.setStrokeMiter(width);
                }
            }
            return true;
        }
        return false;
    }
    bool SkPaintConvert::parseFill(const string& str, SkPaint& value)
    {
        StringArray texts;
        Convert::splitItems(str, texts);
        if(texts.count() > 0)
        {
            bool parsed = false;
            for (uint i=0; i<texts.count(); i++)
            {
                StringArray values;
                Convert::splitItems(texts[i], values, ':');
                if(values.count() == 2)
                {
                    string name = Convert::trimStr(values[0], ' ', '{', '}');
                    string text = Convert::trimStartStr(values[1], ' ', '{', '}');
                    text = Convert::trimEndStr(text, ' ', '{', '}');
                    
                    // color:red
                    // color:{A:255;R:255;G:255;B:255}
                    Color color;
                    if(String::stringEquals(name, "color", true))
                    {
                        parsed = true;
                        if(!Color::parse(text, color))
                            return false;
                        
                        value.setColor(color.toArgb());
                    }
                    // linear:{points:{point:{0, 0};point:{300, 300}};colors:{color:{A:255;R:0;G:255;B:0};color:{red}};mode:mirror}
                    else if(String::stringEquals(name, "linear", true))
                    {
                        parsed = true;
                        if(!parseFillLinear(text, value))
                            return false;
                    }
                }
            }
            return parsed;
        }
        return false;
    }
    bool SkPaintConvert::parseFillLinear(const string& str, SkPaint& value)
    {
        // {points:{point:{0, 0};point:{300, 300}};colors:{color:{A:255;R:0;G:255;B:0};color:{red}};mode:mirror}
        StringArray texts;
        Convert::splitItems(str, texts);
        if(texts.count() > 0)
        {
            bool parsed = false;
            SkPoints points;
            Colors colors;
            SkShader::TileMode tileMode = SkShader::kMirror_TileMode;
            for (uint i=0; i<texts.count(); i++)
            {
                StringArray values;
                Convert::splitItems(texts[i], values, ':');
                if(values.count() == 2)
                {
                    string name = Convert::trimStr(values[0], ' ', '{', '}');
                    string text = Convert::trimStartStr(values[1], ' ', '{', '}');
                    text = Convert::trimEndStr(text, ' ', '{', '}');
                    
                    if(String::stringEquals(name, "points", true))
                    {
                        parsed = true;
                        if(!SkPointConvert::parse(text, points) && points.count() == 2)
                            return false;
                    }
                    else if(String::stringEquals(name, "colors", true))
                    {
                        parsed = true;
                        if(!Colors::parse(text, colors))
                            return false;
                    }
                    else if(String::stringEquals(name, "mode", true))
                    {
                        parsed = true;
                        if(String::stringEquals(text, "mirror", true))
                        {
                            tileMode = SkShader::kMirror_TileMode;
                        }
                        else if(String::stringEquals(text, "clamp", true))
                        {
                            tileMode = SkShader::kClamp_TileMode;
                        }
                        else if(String::stringEquals(text, "repeat", true))
                        {
                            tileMode = SkShader::kRepeat_TileMode;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            
            if(parsed)
            {
                SkPoint linearPoints[] = { points[0], points[1] };
                SkColor linearColors[] = { colors[0].toArgb(), colors[1].toArgb()};
                
                SkShader* shader = SkGradientShader::CreateLinear(
                                                                  linearPoints, linearColors, NULL, 2,
                                                                  tileMode);
                SkAutoUnref shader_deleter(shader);
                
                value.setShader(shader);
            }
            return parsed;
        }
        return false;
    }
    
    string SkFontStyleConvert::toString(const SkFontStyle& value)
    {
        return "";
    }
    bool SkFontStyleConvert::parse(const string& str, SkFontStyle& value)
    {
        return false;
    }
}
