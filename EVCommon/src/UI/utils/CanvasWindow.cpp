/*
 * Copyright 2015 Google Inc.
 *
 *
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 *
 */

#include "CanvasWindow.h"

#include "gl/GrGLInterface.h"
#include "SkApplication.h"
#include "SkCanvas.h"
#include "SkGradientShader.h"
#include "SkGraphics.h"
#include "SkGr.h"

#include "common/diag/Debug.h"
#ifdef DEBUG
#include "common/system/Application.h"
#endif

// Should be 3x + 1
#define kMaxFatBitsScale    28
#define MAX_ZOOM_LEVEL  8
#define MIN_ZOOM_LEVEL  -8

void application_init() {
    SkGraphics::Init();
    SkEvent::Init();
}

void application_term() {
    SkEvent::Term();
    SkGraphics::Term();
}

#ifdef __ANDROID__
void invalidateInner(SkWindow* win)
{
	win->inval(NULL);
//	CanvasWindow* cw = dynamic_cast<CanvasWindow*>(win);
//	assert(cw);
//    cw->postInvalDelay();
}
void invalidateRectInner(SkWindow* win, const Drawing::Rectangle& rect)
{
    SkRect bounds = SkRectConvert::convert(rect);
    win->inval((SkRect*)&bounds);
}
#elif WIN32 | __linux__
void invalidateInner(SkWindow* win)
{
    win->inval(NULL);
}
#if WIN32
//BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM lParam)
//{
//	DWORD dwCurProcessId = *((DWORD*)lParam);
//	DWORD dwProcessId = 0;
//
//	GetWindowThreadProcessId(hwnd, &dwProcessId);
//	if (dwProcessId == dwCurProcessId && GetParent(hwnd) == NULL)
//	{
//		*((HWND *)lParam) = hwnd;
//		return FALSE;
//	}
//	return TRUE;
//}
//HWND GetMainWindow()
//{
//	DWORD dwCurrentProcessId = GetCurrentProcessId();
//	if (!EnumWindows(EnumWindowsProc, (LPARAM)&dwCurrentProcessId))
//	{
//		return (HWND)dwCurrentProcessId;
//	}
//	return NULL;
//}
#endif
void invalidateRectInner(SkWindow* win, const Drawing::Rectangle& rect)
{
#if WIN32
    //HWND hWnd = GetMainWindow();
    //if (hWnd)
    //{
    //	HDC hdc = GetDC(hWnd);
    
    //	SelectObject(hdc, GetStockObject(DC_BRUSH));
    //	int r = rand() & 0xFF;
    //	int g = rand() & 0xFF;
    //	int b = rand() & 0xFF;
    //	SetDCBrushColor(hdc, RGB(r, g, b));
    //	::PatBlt(hdc, rect.x, rect.y, rect.width, rect.height, PATCOPY);
    
    //	ReleaseDC(hWnd, hdc);
    //}
    //return;
#endif
    SkRect bounds = SkRectConvert::convert(rect);
    win->inval((SkRect*)&bounds);
}
#endif

CanvasWindow::CanvasWindow(void* hwnd)
: INHERITED(hwnd)
{
    fMouseX = fMouseY = 0;
    fFatBitsScale = 8;
    fShowZoomer = false;
    
    fZoomLevel = 0;
    fZoomScale = SK_Scalar1;
    
    fZoomCenterX = 0;
    fZoomCenterY = 0;

    fMagnify = false;
    
    fContext = NULL;
    fInterface = NULL;
    fRenderTarget = NULL;
    this->setTitle();
    this->setUpBackend();
    
    _canvas = new Canvas(this);
    
#ifdef DEBUG
    _sampleDraw = false;
#endif
}

CanvasWindow::~CanvasWindow() {
    tearDownBackend();
    
    if(_canvas != NULL)
    {
        delete _canvas;
        _canvas = NULL;
    }
}

void CanvasWindow::tearDownBackend() {
    SkSafeUnref(fContext);
    fContext = NULL;
    
    SkSafeUnref(fInterface);
    fInterface = NULL;
    
    SkSafeUnref(fRenderTarget);
    fRenderTarget = NULL;
    
    INHERITED::detach();
}

void CanvasWindow::setTitle() {
    SkString title("Canvas");
    INHERITED::setTitle(title.c_str());
}

bool CanvasWindow::setUpBackend() {
    this->setColorType(kRGBA_8888_SkColorType);
    this->setVisibleP(true);
    this->setClipToBounds(false);
    
    bool result = attach(kNativeGL_BackEndType, 0 /*msaa*/, &fAttachmentInfo);
    if (false == result) {
        SkDebugf("Not possible to create backend.\n");
        detach();
        return false;
    }
    
    fInterface = GrGLCreateNativeInterface();
    
    SkASSERT(NULL != fInterface);
    
    fContext = GrContext::Create(kOpenGL_GrBackend, (GrBackendContext)fInterface);
    SkASSERT(NULL != fContext);
    
    this->setUpRenderTarget();
    return true;
}

void CanvasWindow::setUpRenderTarget() {
    SkSafeUnref(fRenderTarget);
    fRenderTarget = this->renderTarget(fAttachmentInfo, fInterface, fContext);
}

#ifdef DEBUG
void drawSampleContents(SkCanvas* canvas) {
    SkPaint paint;
    paint.setColor(SK_ColorRED);
    
    // Draw a rectangle with red paint
    SkRect rect = {
        10, 10,
        128, 128
    };
    canvas->drawRect(rect, paint);
    
    // Set up a linear gradient and draw a circle
    {
        SkPoint linearPoints[] = {
            {0, 0},
            {300, 300}
        };
        SkColor linearColors[] = {SK_ColorGREEN, SK_ColorBLACK};
        
        SkShader* shader = SkGradientShader::CreateLinear(
                                                          linearPoints, linearColors, NULL, 2,
                                                          SkShader::kMirror_TileMode);
        SkAutoUnref shader_deleter(shader);
        
        paint.setShader(shader);
        paint.setFlags(SkPaint::kAntiAlias_Flag);
        
        canvas->drawCircle(200, 200, 64, paint);
        
        // Detach shader
        paint.setShader(NULL);
    }
    
    // Draw a message with a nice black paint.
    paint.setFlags(
                   SkPaint::kAntiAlias_Flag |
                   SkPaint::kSubpixelText_Flag |  // ... avoid waggly text when rotating.
                   SkPaint::kUnderlineText_Flag);
    paint.setColor(SK_ColorBLACK);
    paint.setTextSize(20);
    
    canvas->save();
    
    static const char message[] = "Hello World";
    
    // Translate and rotate
    canvas->translate(300, 300);
    canvas->rotate(45);
    
    // Draw the text:
    canvas->drawText(message, strlen(message), 0, 0, paint);
    
    canvas->restore();
}
void CanvasWindow::setSampleDraw(bool sampleDraw)
{
    _sampleDraw = sampleDraw;
}
#endif

void CanvasWindow::draw()
{
    SkAutoTUnref<SkSurface> surface(this->createSurface());
    SkCanvas* canvas = surface->getCanvas();
    draw(canvas);
}
void CanvasWindow::draw(SkCanvas* canvas) {
#if __linux__
	// fixed bug in linux.
    if(_canvas->bounds().isEmpty())
    {
    	onSizeChange();
    }
#endif

    // It must draw background before matrix transform.
    if(_canvas != NULL)
    {
        _canvas->onPaintBackground(canvas);
    }
    
    SkAutoCanvasRestore as(canvas, true);
    
    if(_canvas != NULL && _canvas->transfer())
    {
        const SkMatrix& matrix = getLocalMatrix();
        canvas->concat(matrix);
    }
    
    darwContext(canvas);
    
//    canvas->restoreToCount(sc);
    
//#ifdef __linux__
    // in case we have queued drawing calls
    fContext->flush();
    
    INHERITED::present();
//#endif
}
void CanvasWindow::darwContext(SkCanvas* canvas)
{
    if(_canvas != NULL)
    {
        _canvas->onPaint(canvas, SkRectConvert::convert(INHERITED::getDirtyBounds()));
        
#ifdef DEBUG
        if(_sampleDraw)
        {
//            SkDebugf("Draw sample contents.\n");
            drawSampleContents(canvas);
        }
#endif
    }
}

Canvas* CanvasWindow::canvas() const
{
    return _canvas;
}

void CanvasWindow::onSizeChange() {
    this->INHERITED::onSizeChange();
    
    setUpRenderTarget();
    
    if(_canvas != NULL)
    {
        SkRect bounds;
        getLocalBounds(&bounds);
        _canvas->setBounds(SkRectConvert::convert(bounds));
    }
    
    fZoomCenterX = SkScalarHalf(this->width());
    fZoomCenterY = SkScalarHalf(this->height());
    
//#ifdef __ANDROID__
//    // FIXME: The first draw after a size change does not work on Android, so
//    // we post an invalidate.
//    this->postInvalDelay();
//#endif
}

bool CanvasWindow::onHandleChar(SkUnichar unichar) {
	if (' ' == unichar) {
		if (_canvas != nullptr)
		{
			_canvas->reopenCurrentPage();
			this->inval(NULL);
		}
	}
    return true;
}
bool CanvasWindow::onHandleKey(SkKey key)
{
    return true;
}
bool CanvasWindow::onEvent(const SkEvent& evt)
{
//#ifdef __ANDROID__
//	if (isInvalEvent(evt)) {
//        this->inval(nullptr);
//        return true;
//	}
//#endif
    return INHERITED::onEvent(evt);
}

//#ifdef __ANDROID__
//static const char view_inval_msg[] = "view-inval-msg";
//
//void CanvasWindow::postInvalDelay() {
//	(new SkEvent(view_inval_msg, this->getSinkID()))->postDelay(1);
//}
//
//bool CanvasWindow::isInvalEvent(const SkEvent& evt) {
//	return evt.isType(view_inval_msg);
//}
//#endif

void CanvasWindow::setZoomCenter(float x, float y) {
    fZoomCenterX = x;
    fZoomCenterY = y;
}

bool CanvasWindow::zoomIn() {
    // Arbitrarily decided
    if (fFatBitsScale == kMaxFatBitsScale) return false;
    fFatBitsScale++;
    this->inval(nullptr);
    return true;
}

bool CanvasWindow::zoomOut() {
    if (fFatBitsScale == 1) return false;
    fFatBitsScale--;
    this->inval(nullptr);
    return true;
}

void CanvasWindow::updatePointer(int x, int y) {
    fMouseX = x;
    fMouseY = y;
    if (fShowZoomer) {
        this->inval(nullptr);
    }
}

static const char gGestureClickType[] = "GestureClickType";

bool CanvasWindow::onDispatchClick(int x, int y, Click::State state,
                                   void* owner, unsigned modi) {
    if (Click::kMoved_State == state) {
        updatePointer(x, y);
    }
    int w = SkScalarRoundToInt(this->width());
    int h = SkScalarRoundToInt(this->height());
    
    // check for the resize-box
    if (w - x < 16 && h - y < 16) {
        return false;   // let the OS handle the click
    }
    else if (fMagnify) {
        //it's only necessary to update the drawing if there's a click
        this->inval(nullptr);
        return false; //prevent dragging while magnify is enabled
    } else {
        // capture control+option, and trigger debugger
        if ((modi & kControl_SkModifierKey) && (modi & kOption_SkModifierKey)) {
            if (Click::kDown_State == state) {
                SkEvent evt("debug-hit-test");
                evt.setS32("debug-hit-test-x", x);
                evt.setS32("debug-hit-test-y", y);
                this->doEvent(evt);
            }
            return true;
        } else {
            return this->INHERITED::onDispatchClick(x, y, state, owner, modi);
        }
    }
}

class GestureClick : public SkView::Click {
public:
    GestureClick(SkView* target) : SkView::Click(target) {
        this->setType(gGestureClickType);
    }
    
    static bool IsGesture(Click* click) {
        return click->isType(gGestureClickType);
    }
};

SkView::Click* CanvasWindow::onFindClickHandler(SkScalar x, SkScalar y,
                                                unsigned modi) {
    return new GestureClick(this);
}

bool CanvasWindow::onClick(Click* click) {
    if (GestureClick::IsGesture(click)) {
        if(_canvas != nullptr)
        {
            _canvas->onClick(click);
        }
        float x = static_cast<float>(click->fICurr.fX);
        float y = static_cast<float>(click->fICurr.fY);
        
        switch (click->fState) {
            case SkView::Click::kDown_State:
                fGesture.touchBegin(click->fOwner, x, y);
                break;
            case SkView::Click::kMoved_State:
                fGesture.touchMoved(click->fOwner, x, y);
                this->updateMatrix();
                break;
            case SkView::Click::kUp_State:
                fGesture.touchEnd(click->fOwner);
                this->updateMatrix();
                break;
        }
        return true;
    }
    return false;
}

void CanvasWindow::changeZoomLevel(float delta) {
    fZoomLevel += delta;
    if (fZoomLevel > 0) {
        fZoomLevel = SkMinScalar(fZoomLevel, MAX_ZOOM_LEVEL);
        fZoomScale = fZoomLevel + SK_Scalar1;
    } else if (fZoomLevel < 0) {
        fZoomLevel = SkMaxScalar(fZoomLevel, MIN_ZOOM_LEVEL);
        fZoomScale = SK_Scalar1 / (SK_Scalar1 - fZoomLevel);
    } else {
        fZoomScale = SK_Scalar1;
    }
    this->updateMatrix();
}

void CanvasWindow::updateMatrix(){
    if(_canvas != NULL && _canvas->transfer())
    {
        SkMatrix m;
        m.reset();
        if (fZoomLevel) {
            SkPoint center;
            //m = this->getLocalMatrix();//.invert(&m);
            m.mapXY(fZoomCenterX, fZoomCenterY, &center);
            SkScalar cx = center.fX;
            SkScalar cy = center.fY;
            
            m.setTranslate(-cx, -cy);
            m.postScale(fZoomScale, fZoomScale);
            m.postTranslate(cx, cy);
        }
        
        // Apply any gesture matrix
        m.preConcat(fGesture.localM());
        m.preConcat(fGesture.globalM());
        
        this->setLocalMatrix(m);
        
        this->inval(nullptr);
    }
}

SkOSWindow* create_sk_window(void* hwnd, int , char** ) {
    return new CanvasWindow(hwnd);
}
