#ifndef FONT_H
#define FONT_H

#include <string>
#include "common/common_global.h"
#include "Color.h"
#include "SkPaint.h"
#include "SkTypeface.h"

using namespace std;

namespace Drawing
{
    struct COMMON_EXPORT Font
    {
    public:
        enum ContentAlignment {
            TopLeft = 0x001,
            TopCenter = 0x002,
            TopRight = 0x004,
            MiddleLeft = 0x010,
            MiddleCenter = 0x020,
            MiddleRight = 0x040,
            BottomLeft = 0x100,
            BottomCenter = 0x200,
            BottomRight = 0x400,
        };
        
        string name;
        SkTypeface::Style style;
        SkPaint::TextEncoding encoding;
        ContentAlignment align;
        float size;
        float scaleX;
        float skewX;
        bool autoScale;
        
        Font();
        
        bool isEmpty() const;
        string toString() const;
        
        void operator=(const Font& value);
        bool operator==(const Font& value) const;
        bool operator!=(const Font& value) const;
        
        bool toSkPaint(SkPaint& paint);
        
    public:
        static bool parse(const string& str, Font& font);
        
    private:
        static void setFontName(const string& name, SkTypeface::Style style, SkPaint& paint);

        static SkTypeface::Style parseStyle(const string& value);
        static string convertStyleStr(SkTypeface::Style value);

        static SkPaint::TextEncoding parseTextEncoding(const string& value);
        static string convertTextEncodingStr(SkPaint::TextEncoding value);
        
        static ContentAlignment parseAlign(const string& value);
        static string convertAlignStr(ContentAlignment value);
    };
}

#endif	// FONT_H
