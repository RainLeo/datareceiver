#ifndef BRUSH_H
#define BRUSH_H

#include <string>
#include "common/common_global.h"
#include "Color.h"
#include "Point.h"
#include "Rectangle.h"
#include "Image.h"

#include "SkShader.h"

using namespace std;
using namespace Common;

namespace Drawing
{
	class COMMON_EXPORT Brush
    {
    public:
        enum BrushTypes
        {
            Empty = 0,
            Solid,
            LinearGradient,
            RadialGradient,
            Texture
        };
        class SolidInfo
        {
        public:            
            Color color;
            SolidInfo(const Color& color = Color())
            {
                this->color = color;
            }
            
            bool isEmpty() const;
            string toString() const;
            
            void operator=(const SolidInfo& value);
            bool operator==(const SolidInfo& value) const;
            bool operator!=(const SolidInfo& value) const;
            
        private:
            static bool parse(const string& str, SolidInfo& value);
            
        private:
            friend Brush;
        };
        class LinearInfo
        {
        public:
            float angle;    // horz is zero, anticlockwise increase.
            Colors colors;
            Array<float> positions;
            SkShader::TileMode tileMode;
            
            LinearInfo()
            {
                angle = 0.0f;
            }
            
            bool isEmpty() const;
            string toString() const;
            
            void operator=(const LinearInfo& value);
            bool operator==(const LinearInfo& value) const;
            bool operator!=(const LinearInfo& value) const;
            
        private:
            static SkShader::TileMode parseTileMode(const string& value);
            static string convertTileModeStr(SkShader::TileMode value);
            static bool parse(const string& str, LinearInfo& value);
            
        private:
            friend Brush;
            
            Rectangle _bounds;
        };
        class RadialInfo
        {
        public:
            Colors colors;
            Array<float> positions;
            SkShader::TileMode tileMode;
            
            RadialInfo()
            {
            }
            
            bool isEmpty() const;
            string toString() const;
            
            void operator=(const RadialInfo& value);
            bool operator==(const RadialInfo& value) const;
            bool operator!=(const RadialInfo& value) const;
            
        private:
            static bool parse(const string& str, RadialInfo& value);
            
        private:
            friend Brush;
            
            Rectangle _bounds;
        };
        class TextureInfo
        {
        public:
            enum FillLayout
            {
                Wrap = 0,
                Center = 1,
                Stretch =2
            };
            enum Position
            {
                Owner = 0,
                Parent = 1
            };
            
            FillLayout layout;
            SkShader::TileMode tileModeX;
            SkShader::TileMode tileModeY;
            Position position;
            Image image;
            
            TextureInfo()
            {
                layout = Wrap;
                tileModeX = SkShader::kRepeat_TileMode;
                tileModeY = SkShader::kRepeat_TileMode;
                position = Owner;
            }
            
            bool isEmpty() const;
            string toString() const;
            
            void operator=(const TextureInfo& value);
            bool operator==(const TextureInfo& value) const;
            bool operator!=(const TextureInfo& value) const;
            
        private:
            static FillLayout parseFillLayout(const string& value);
            static string convertFillLayoutStr(FillLayout value);
            static Position parsePosition(const string& value);
            static string convertPositionStr(Position value);
            static bool parse(const string& str, TextureInfo& value);
            
        private:
            friend Brush;
            
            Rectangle _bounds;
        };

        bool enable;
        BrushTypes type;
        SolidInfo solid;
        LinearInfo linear;
        RadialInfo radial;
        TextureInfo texture;
        
        Brush();
        Brush(const Brush& brush);
        Brush(const Color& color);
        
        bool isEmpty() const;
        string toString() const;
        
        void operator=(const Brush& value);
        bool operator==(const Brush& value) const;
        bool operator!=(const Brush& value) const;
        
        void update(const Rectangle& bounds);
        
        bool toSkPaint(SkPaint& paint);
        
        void setConfigSource(const ConfigFile& cf);
        
    public:
        static bool parse(const string& str, Brush& brush);

    private:
        static BrushTypes parseType(const string& value);
        static string convertTypeStr(BrushTypes value);
        
        static bool toSolid(const SolidInfo& info, SkPaint& paint);
        static bool toLinearGradient(const LinearInfo& info, SkPaint& paint);
        static bool toRadialGradient(const RadialInfo& info, SkPaint& paint);
        static bool toTexture(TextureInfo& info, SkPaint& paint);
    };
}

#endif	// BRUSH_H
