﻿#include "SystemColor.h"
#include "common/system/Convert.h"
#include "common/data/PrimitiveType.h"

using namespace Common;

namespace Drawing
{
    const SystemColor::ConstColor SystemColor::ConstColors[SystemColor::ConstColorCount] =
    {
        // We've reserved the value "1" as unknown.  If for some odd reason "1" is added to the
        // list, redefined UnknownColor
        
        { "AliceBlue", AliceBlue },
        { "AntiqueWhite", AntiqueWhite },
        { "Aqua", Aqua },
        { "Aquamarine", Aquamarine },
        { "Azure", Azure },
        { "Beige", Beige },
        { "Bisque", Bisque },
        { "Black", Black },
        { "BlanchedAlmond", BlanchedAlmond },
        { "Blue", Blue },
        { "BlueViolet", BlueViolet },
        { "Brown", Brown },
        { "BurlyWood", BurlyWood },
        { "CadetBlue", CadetBlue },
        { "Chartreuse", Chartreuse },
        { "Chocolate", Chocolate },
        { "Coral", Coral },
        { "CornflowerBlue", CornflowerBlue },
        { "Cornsilk", Cornsilk },
        { "Crimson", Crimson },
        { "Cyan", Cyan },
        { "DarkBlue", DarkBlue },
        { "DarkCyan", DarkCyan },
        { "DarkGoldenrod", DarkGoldenrod },
        { "DarkGray", DarkGray },
        { "DarkGreen", DarkGreen },
        { "DarkKhaki", DarkKhaki },
        { "DarkMagenta", DarkMagenta },
        { "DarkOliveGreen", DarkOliveGreen },
        { "DarkOrange", DarkOrange },
        { "DarkOrchid", DarkOrchid },
        { "DarkRed", DarkRed },
        { "DarkSalmon", DarkSalmon },
        { "DarkSeaGreen", DarkSeaGreen },
        { "DarkSlateBlue", DarkSlateBlue },
        { "DarkSlateGray", DarkSlateGray },
        { "DarkTurquoise", DarkTurquoise },
        { "DarkViolet", DarkViolet },
        { "DeepPink", DeepPink },
        { "DeepSkyBlue", DeepSkyBlue },
        { "DimGray", DimGray },
        { "DodgerBlue", DodgerBlue },
        { "Firebrick", Firebrick },
        { "FloralWhite", FloralWhite },
        { "ForestGreen", ForestGreen },
        { "Fuchsia", Fuchsia },
        { "Gainsboro", Gainsboro },
        { "GhostWhite", GhostWhite },
        { "Gold", Gold },
        { "Goldenrod", Goldenrod },
        { "Gray", Gray },
        { "Green", Green },
        { "GreenYellow", GreenYellow },
        { "Honeydew", Honeydew },
        { "HotPink", HotPink },
        { "IndianRed", IndianRed },
        { "Indigo", Indigo },
        { "Ivory", Ivory },
        { "Khaki", Khaki },
        { "Lavender", Lavender },
        { "LavenderBlush", LavenderBlush },
        { "LawnGreen", LawnGreen },
        { "LemonChiffon", LemonChiffon },
        { "LightBlue", LightBlue },
        { "LightCoral", LightCoral },
        { "LightCyan", LightCyan },
        { "LightGoldenrodYellow", LightGoldenrodYellow },
        { "LightGreen", LightGreen },
        { "LightGray", LightGray },
        { "LightPink", LightPink },
        { "LightSalmon", LightSalmon },
        { "LightSeaGreen", LightSeaGreen },
        { "LightSkyBlue", LightSkyBlue },
        { "LightSlateGray", LightSlateGray },
        { "LightSteelBlue", LightSteelBlue },
        { "LightYellow", LightYellow },
        { "Lime", Lime },
        { "LimeGreen", LimeGreen },
        { "Linen", Linen },
        { "Magenta", Magenta },
        { "Maroon", Maroon },
        { "MediumAquamarine", MediumAquamarine },
        { "MediumBlue", MediumBlue },
        { "MediumOrchid", MediumOrchid },
        { "MediumPurple", MediumPurple },
        { "MediumSeaGreen", MediumSeaGreen },
        { "MediumSlateBlue", MediumSlateBlue },
        { "MediumSpringGreen", MediumSpringGreen },
        { "MediumTurquoise", MediumTurquoise },
        { "MediumVioletRed", MediumVioletRed },
        { "MidnightBlue", MidnightBlue },
        { "MintCream", MintCream },
        { "MistyRose", MistyRose },
        { "Moccasin", Moccasin },
        { "NavajoWhite", NavajoWhite },
        { "Navy", Navy },
        { "OldLace", OldLace },
        { "Olive", Olive },
        { "OliveDrab", OliveDrab },
        { "Orange", Orange },
        { "OrangeRed", OrangeRed },
        { "Orchid", Orchid },
        { "PaleGoldenrod", PaleGoldenrod },
        { "PaleGreen", PaleGreen },
        { "PaleTurquoise", PaleTurquoise },
        { "PaleVioletRed", PaleVioletRed },
        { "PapayaWhip", PapayaWhip },
        { "PeachPuff", PeachPuff },
        { "Peru", Peru },
        { "Pink", Pink },
        { "Plum", Plum },
        { "PowderBlue", PowderBlue },
        { "Purple", Purple },
        { "Red", Red },
        { "RosyBrown", RosyBrown },
        { "RoyalBlue", RoyalBlue },
        { "SaddleBrown", SaddleBrown },
        { "Salmon", Salmon },
        { "SandyBrown", SandyBrown },
        { "SeaGreen", SeaGreen },
        { "SeaShell", SeaShell },
        { "Sienna", Sienna },
        { "Silver", Silver },
        { "SkyBlue", SkyBlue },
        { "SlateBlue", SlateBlue },
        { "SlateGray", SlateGray },
        { "Snow", Snow },
        { "SpringGreen", SpringGreen },
        { "SteelBlue", SteelBlue },
        { "Tan", Tan },
        { "Teal", Teal },
        { "Thistle", Thistle },
        { "Tomato", Tomato },
        { "Transparent", Transparent },
        { "Turquoise", Turquoise },
        { "Violet", Violet },
        { "Wheat", Wheat },
        { "White", White },
        { "WhiteSmoke", WhiteSmoke },
        { "Yellow", Yellow },
        { "YellowGreen", YellowGreen },
        { "UnknownColor", UnknownColor }
    };
    
    bool SystemColor::toSystemColorStr(const Color& color, string& str)
    {
        for (int i=0; i<ConstColorCount; i++)
        {
            const ConstColor& cc = ConstColors[i];
            if(cc.value == color._value)
            {
                str = cc.name;
                return true;
            }
        }
        return false;
    }
    bool SystemColor::parse(const string& str, Color& color)
    {
        for (int i=0; i<ConstColorCount; i++)
        {
            const ConstColor& cc = ConstColors[i];
            string temp = Convert::trimStr(str, ' ', '{', '}');
            if(Common::String::stringEquals(cc.name, temp, true))
            {
                color = Color(cc.value);
                return true;
            }
        }
        return false;
    }
}
