#ifndef PEN_H
#define PEN_H

#include <string>
#include "common/common_global.h"
#include "Color.h"
#include "SkPaint.h"

using namespace std;
using namespace Common;

namespace Drawing
{
    class COMMON_EXPORT Pen
    {
    public:
        bool enable;
        Color color;
        float width;
        float miter;
        SkPaint::Cap cap;
        SkPaint::Join join;
        Array<float> intervals;
        float phase;
        
        Pen();
        Pen(const Color& color, float width);
        Pen(const Pen& pen);
        
        bool isEmpty() const;
        string toString() const;
        
        void operator=(const Pen& value);
        bool operator==(const Pen& value) const;
        bool operator!=(const Pen& value) const;
        
        bool toSkPaint(SkPaint& paint) const;
        
        bool isDash() const;
        
    public:
        static bool parse(const string& str, Pen& pen);
        
    private:
        static bool parseDash(const string& str, Pen& pen);
        
        static SkPaint::Cap parseCap(const string& value);
        static string convertCapStr(SkPaint::Cap value);
        
        static SkPaint::Join parseJoin(const string& value);
        static string convertJoinStr(SkPaint::Join value);
    };
}

#endif	// PEN_H
