﻿#include "Point.h"
#include "common/system/Convert.h"
#include "common/data/PrimitiveType.h"

using namespace Common;

namespace Drawing
{
	const Point Point::Empty;
    const Point Point::MinPoint = Point(-65535.f, -65535.f);
    const Point Point::MaxPoint = Point(65535.f, 65535.f);

	Point::Point(float x, float y)
	{
		this->x = x;
		this->y = y;
	}

	bool Point::isEmpty() const
	{
		return x == 0.0f && y == 0.0f;
	}
	string Point::toString() const
	{
		return Convert::convertStr("%f,%f", x, y);
	}

	void Point::operator=(const Point& value)
	{
		this->x = value.x;
		this->y = value.y;
	}
	bool Point::operator==(const Point& value) const
	{
		return this->x == value.x && this->y == value.y;
	}
	bool Point::operator!=(const Point& value) const
	{
		return !operator==(value);
	}
    
    void Point::offset(const Point& pos)
    {
        offset(pos.x, pos.y);
    }
    void Point::offset(float dx, float dy)
    {
        x += dx;
        y += dy;
    }
    
    bool Point::parse(const string& str, Point& point)
    {
        StringArray texts;
        Convert::splitStr(str, ',', texts);
        if (texts.count() == 2)
        {
            Point temp;
            if(Convert::parseSingle(Convert::trimStr(texts[0]), temp.x) &&
               Convert::parseSingle(Convert::trimStr(texts[1]), temp.y))
            {
                point = temp;
                return true;
            }
        }
        return false;
    }
    
    // point:{0, 0};point:{300, 300}
    // 0, 0;300, 300
    bool Points::parse(const string& str, Points& points)
    {
        StringArray texts;
        Convert::splitStr(str, ';', texts);
        if (texts.count() > 0)
        {
            for (uint i=0; i<texts.count(); i++)
            {
                Point point;
                
                Convert::KeyPairs pairs;
                if(Convert::splitItems(texts[i], pairs))
                {
                    for (uint j=0; j<pairs.count(); j++)
                    {
                        const Convert::KeyPair* kp = pairs[j];
                        if(String::stringEquals(kp->name, "point") &&
                           Point::parse(kp->value, point))
                        {
                            points.add(point);
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    if(Point::parse(texts[i], point))
                    {
                        points.add(point);
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        return false;
    }
}
