#ifndef SKCONVERT_H
#define SKCONVERT_H

#include <string>
#include "common/common_global.h"
#include "common/data/Array.h"
#include "Point.h"
#include "Size.h"
#include "Color.h"
#include "Rectangle.h"

#include "SkRect.h"
#include "SkPath.h"
#include "SkPoint.h"
#include "SKSize.h"
#include "SkPaint.h"
#include "SkFontStyle.h"

using namespace std;
using namespace Common;

namespace Drawing
{
    typedef Array<SkPoint> SkPoints;
    class COMMON_EXPORT SkPointConvert
    {
    public:
        static string toString(const SkPoint& value);
        static bool parse(const string& str, SkPoint& value);
        static bool parse(const string& str, SkPoints& value);
        static SkPoint toSkPoint(const Point& from);
        static void toSkPoint(const Points& from, SkPoints& to);
        static SkPoint convert(const Point& value);
        static Point convert(const SkPoint& value);
    };
    
    typedef Array<SkColor> SkColors;
    class COMMON_EXPORT SkColorConvert
    {
    public:
        static SkColor toSkColor(const Color& from);
        static void toSkColor(const Colors& from, SkColors& to);
    };
    
    class COMMON_EXPORT SkSizeConvert
    {
    public:
        static string toString(const SkSize& value);
        static bool parse(const string& str, SkSize& value);
    };
    
    class COMMON_EXPORT SkRectConvert
    {
    public:
        static string toString(const SkRect& value);
        static bool parse(const string& str, SkRect& value);
        static SkRect convert(const Rectangle& value);
        static Rectangle convert(const SkRect& value);
        static Rectangle convert(const SkIRect& value);
    };
    
    class COMMON_EXPORT SkPathConvert : public SkPath
    {
    public:
        static string toString(const SkPath& value);
        static bool parse(const string& str, SkPath& value);
    };
    
    class COMMON_EXPORT SkPaintConvert
    {
    public:
        static string toString(const SkPaint& value);
        static bool parse(const string& str, SkPaint& stroke, SkPaint& fill);
        static bool parseStroke(const string& str, SkPaint& value);
        static bool parseFill(const string& str, SkPaint& value);
        static bool parseText(const string& str, SkPaint& value);
        
    private:
        static bool parseFillLinear(const string& str, SkPaint& value);
    };
    
    class COMMON_EXPORT SkFontStyleConvert
    {
    public:
        static string toString(const SkFontStyle& value);
        static bool parse(const string& str, SkFontStyle& value);
    };
}

#endif	// SKCONVERT_H
