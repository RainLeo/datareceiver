﻿#include "Size.h"
#include "Point.h"
#include "common/system/Convert.h"

using namespace Common;

namespace Drawing
{
	const Size Size::Empty;
    const Size Size::MaxSize = Size(Point::MaxPoint.x - Point::MinPoint.x, Point::MaxPoint.y - Point::MinPoint.y);

	Size::Size(float width, float height)
	{
		this->width = width;
		this->height = height;
	}
	Size::Size(int width, int height)
	{
		this->width = (float)width;
		this->height = (float)height;
	}

	bool Size::isEmpty() const
	{
		return width == 0.0f && height == 0.0f;
	}
	string Size::toString() const
	{
		return Convert::convertStr("%f,%f", width, height);
	}

	void Size::operator=(const Size& value)
	{
		this->width = value.width;
		this->height = value.height;
	}
	bool Size::operator==(const Size& value) const
	{
		return this->width == value.width && this->height == value.height;
	}
	bool Size::operator!=(const Size& value) const
	{
		return !operator==(value);
	}
    
    bool Size::parse(const string& str, Size& size)
    {
        StringArray texts;
        Convert::splitStr(str, ',', texts);
        if (texts.count() == 2)
        {
            Size temp;
            if(Convert::parseSingle(Convert::trimStr(texts[0]), temp.width) &&
               Convert::parseSingle(Convert::trimStr(texts[1]), temp.height))
            {
                size = temp;
                return true;
            }
        }
        return false;
    }
}
