#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <string>
#include "common/common_global.h"
#include "SkCanvas.h"

using namespace std;
using namespace Common;

namespace Drawing
{
    typedef SkCanvas Graphics;
    typedef SkPaint Paint;
    
    class GraphicsRestore
    {
    public:
        GraphicsRestore(Graphics* graphics)
        {
            _graphics = graphics;
            
            graphics->save();
        }
        ~GraphicsRestore()
        {
            _graphics->restore();
            _graphics = nullptr;
        }
        
    private:
        Graphics* _graphics;
    };
}

#endif	// GRAPHICS_H
