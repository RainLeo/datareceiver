#ifndef SIZE_H
#define SIZE_H

#include <string>
#include "common/common_global.h"
#include "common/data/Array.h"

using namespace std;
using namespace Common;

namespace Drawing
{
	struct Size
	{
	public:
		Size(float width = 0.0f, float height = 0.0f);
		Size(int width, int height);

		bool isEmpty() const;
		string toString() const;

		void operator=(const Size& value);
		bool operator==(const Size& value) const;
		bool operator!=(const Size& value) const;
        
    public:
        static bool parse(const string& str, Size& size);

	public:
		float width;
		float height;

		static const Size Empty;
        static const Size MaxSize;
	};
    typedef Array<Size> Sizes;
}

#endif	// SIZE_H
