#ifndef GRAPHICSPATH_H
#define GRAPHICSPATH_H

#include <string>
#include "common/common_global.h"
#include "SkPath.h"

using namespace std;

namespace Drawing
{
    typedef SkPath GraphicsPath;
}

#endif	// GRAPHICSPATH_H
