#include "Image.h"
#include "common/io/File.h"
#include "common/io/Path.h"
#include "common/system/Convert.h"
#include "common/data/PrimitiveType.h"
#include "SkData.h"
#include "SkImageDecoder.h"

namespace Drawing
{
    Image::Image()
    {
        this->source = Source::FileSource;
        _image = nullptr;
    }
    Image::~Image()
    {
        if(_image != nullptr)
        {
            delete _image;
            _image = nullptr;
        }
    }
    
    SkImage* Image::toSkImage()
    {
        if(_image == nullptr)
        {
            update();
        }
        return _image;
    }
    
    void Image::update()
    {
        if(source == Source::FileSource)
        {
            _image = readImage(configSource, filename);
        }
        else if(source == Source::Resource)
        {
            
        }
        else
        {
            assert(false);
        }
    }
    
    int Image::width()
    {
        SkImage* image = toSkImage();
        return image != nullptr ? image->width() : 0;
    }
    int Image::height()
    {
        SkImage* image = toSkImage();
        return image != nullptr ? image->height() : 0;
    }
    
    bool Image::isEmpty() const
    {
        return filename.empty();
    }
    string Image::toString() const
    {
        return Convert::convertStr("Image fileName:%s", filename.c_str());
    }
    
    void Image::operator=(const Image& value)
    {
        this->id = value.id;
        this->source = value.source;
        this->filename = value.filename;
        this->configSource = value.configSource;
    }
    bool Image::operator==(const Image& value) const
    {
        return this->id == value.id &&
            this->source == value.source &&
            this->filename == value.filename &&
            this->configSource == value.configSource;
    }
    bool Image::operator!=(const Image& value) const
    {
        return !operator==(value);
    }
    
    bool Image::parse(const string& str, Image& image)
    {
        // filename:images/test.jpg;source:file;id:test_image
        StringArray texts;
        Convert::splitItems(str, texts);
        if(texts.count() > 0)
        {
            Image temp;
            for (uint i=0; i<texts.count(); i++)
            {
                StringArray values;
                Convert::splitItems(texts[i], values, ':');
                if(values.count() == 2)
                {
                    string name = Convert::trimStr(values[0], ' ', '{', '}');
                    string text = Convert::trimStartStr(values[1], ' ', '{', '}');
                    text = Convert::trimEndStr(text, ' ', '{', '}');
                    
                    if(String::stringEquals(name, "filename", true))
                    {
                        temp.filename = text;
                    }
                    else if(String::stringEquals(name, "source", true))
                    {
                        temp.source = parseSource(text);
                    }
                    else if(String::stringEquals(name, "id", true))
                    {
                        temp.id = text;
                    }
                }
            }
            
            image = temp;
            return true;
        }
        return false;
    }
    void Image::setConfigSource(const ConfigFile& cf)
    {
        if(cf.isZip())
        {
            configSource.zip = cf.zip;
            configSource.rootPath = Path::getDirectoryName(cf.fileName);
        }
        else
        {
            configSource.zip = nullptr;
            configSource.rootPath = cf.rootPath;
        }
    }
    
    Image::Source Image::parseSource(const string& value)
    {
        if(String::stringEquals(value, "file", true))
        {
            return Source::FileSource;
        }
        else if(String::stringEquals(value, "resource", true))
        {
            return Source::Resource;
        }
        return Source::FileSource;
    }
    string Image::convertSourceStr(Source value)
    {
        switch (value)
        {
            case Source::FileSource:
                return "file";
            case Source::Resource:
                return "resource";
            default:
                return "file";
        }
    }
    
    SkImage* Image::readImage(const ConfigSource& cs, const string& filename)
    {
        string fname = filename;
        SkData* data = nullptr;
        if(cs.isZip())
        {
            // read file from zip.
            fname = Path::combine(cs.rootPath, filename);
            ByteArray buffer;
            if(cs.zip->read(fname, buffer))
            {
                data = SkData::NewWithCopy(buffer.data(), buffer.count());
            }
        }
        else
        {
            // read file from real file.
            if(!Path::isPathRooted(filename.c_str()))
            {
                fname = Path::combine(cs.rootPath, filename);
            }
            if(File::exists(fname))
            {
                data = SkData::NewFromFileName(fname.c_str());
            }
        }
        if(data != nullptr)
        {
            return SkImage::NewFromEncoded(data);
        }
        return nullptr;
    }
    bool Image::readBitmap(const ConfigFile& cf, const string& fileName, Bitmap& bitmap)
    {
        string fname = fileName;
        SkData* data = nullptr;
        if(cf.isZip())
        {
            // read file from zip.
            fname = Path::combine(Path::getDirectoryName(cf.fileName), fileName);
            ByteArray buffer;
            if(cf.zip->read(fname, buffer))
            {
                data = SkData::NewWithCopy(buffer.data(), buffer.count());
            }
        }
        else
        {
            // read file from real file.
            if(!Path::isPathRooted(fileName.c_str()))
            {
                fname = Path::combine(cf.rootPath, fileName);
            }
            if(File::exists(fname))
            {
                data = SkData::NewFromFileName(fname.c_str());
            }
        }
        if(data != nullptr)
        {
            if(SkImageDecoder::DecodeMemory(data->data(), data->size(), &bitmap))
                return true;
        }
        return false;
    }
}
