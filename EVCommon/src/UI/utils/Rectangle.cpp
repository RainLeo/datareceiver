﻿#include "Rectangle.h"
#include "common/system/Convert.h"
#include "common/system/Math.h"

using namespace Common;

namespace Drawing
{
	const Rectangle Rectangle::Empty;

	Rectangle::Rectangle(float x, float y, float width, float height)
	{
		this->x = x;
		this->y = y;
		this->width = width;
		this->height = height;
	}
	Rectangle::Rectangle(Point location, Size size)
	{
		this->x = location.x;
		this->y = location.y;
		this->width = size.width;
		this->height = size.height;
	}

	bool Rectangle::isEmpty() const
	{
		return x == 0.0f && y == 0.0f && width == 0.0f && height == 0.0f;
	}
	string Rectangle::toString() const
	{
		return Convert::convertStr("%f,%f,%f,%f", x, y, width, height);
	}

	Point Rectangle::location() const
	{
		return Point(x, y);
	}
	Size Rectangle::size() const
	{
		return Size(width, height);
	}

	float Rectangle::left() const
	{
		return x;
	}
	float Rectangle::top() const
	{
		return y;
	}
	float Rectangle::right() const
	{
		return x + width;
	}
	float Rectangle::bottom() const
	{
		return y + height;
	}

	void Rectangle::operator=(const Rectangle& value)
	{
		this->x = value.x;
		this->y = value.y;
		this->width = value.width;
		this->height = value.height;
	}
	bool Rectangle::operator==(const Rectangle& value) const
	{
		return x == value.x && y == value.y && width == value.width && height == value.height;
	}
	bool Rectangle::operator!=(const Rectangle& value) const
	{
		return !operator==(value);
	}

	bool Rectangle::contains(float x, float y) const
	{
		return this->x <= x &&
			x < this->x + this->width &&
			this->y <= y &&
			y < this->y + this->height;
	}
	bool Rectangle::contains(const Point& point) const
	{
		return contains(point.x, point.y);
	}
	bool Rectangle::contains(const Rectangle& rect) const
	{
		return(this->x <= rect.x) &&
			((rect.x + rect.width) <= (this->x + this->width)) &&
			(this->y <= rect.y) &&
			((rect.y + rect.height) <= (this->y + this->height));
	}

	void Rectangle::inflate(float width, float height)
	{
		this->x -= width;
		this->y -= height;
		this->width += 2 * width;
		this->height += 2 * height;
	}
	void Rectangle::inflate(const Size& size)
	{
		inflate(size.width, size.height);
	}

	Rectangle Rectangle::intersect(const Rectangle& a, const Rectangle& b)
	{
		float x1 = Math::max(a.x, b.x);
		float x2 = Math::min(a.x + a.width, b.x + b.width);
		float y1 = Math::max(a.y, b.y);
		float y2 = Math::min(a.y + a.height, b.y + b.height);

		if (x2 >= x1 && y2 >= y1)
		{
			return Rectangle(x1, y1, x2 - x1, y2 - y1);
		}
		return Rectangle::Empty;
	}
	void Rectangle::intersect(const Rectangle& rect)
	{
		Rectangle result = Rectangle::intersect(rect, *this);

		this->x = result.x;
		this->y = result.y;
		this->width = result.width;
		this->height = result.height;
	}
	bool Rectangle::intersectsWith(const Rectangle& rect) const
	{
		return(rect.x < this->x + this->width) &&
			(this->x < (rect.x + rect.width)) &&
			(rect.y < this->y + this->height) &&
			(this->y < rect.y + rect.height);
	}

	Rectangle Rectangle::unions(const Rectangle& a, const Rectangle& b)
	{
		float x1 = Math::min(a.x, b.x);
		float x2 = Math::max(a.x + a.width, b.x + b.width);
		float y1 = Math::min(a.y, b.y);
		float y2 = Math::max(a.y + a.height, b.y + b.height);

		return Rectangle(x1, y1, x2 - x1, y2 - y1);
	}

	void Rectangle::offset(const Point& pos)
	{
		offset(pos.x, pos.y);
	}
	void Rectangle::offset(float dx, float dy)
	{
		this->x += dx;
		this->y += dy;
	}
    
    void Rectangle::unions(const Rectangle& rect)
    {
        Rectangle value = unions(*this, rect);
        this->operator=(value);
    }
    
    bool Rectangle::parse(const string& str, Rectangle& rect)
    {
        StringArray texts;
        Convert::splitStr(str, ',', texts);
        if (texts.count() == 4)
        {
            Rectangle temp;
            if(Convert::parseSingle(Convert::trimStr(texts[0]), temp.x) &&
               Convert::parseSingle(Convert::trimStr(texts[1]), temp.y) &&
               Convert::parseSingle(Convert::trimStr(texts[2]), temp.width) &&
               Convert::parseSingle(Convert::trimStr(texts[3]), temp.height))
            {
                rect = temp;
                return true;
            }
        }
        return false;
    }
    
    Point Rectangle::center() const
    {
        if(size().isEmpty())
            return location();
        
        return Point(x + width/2.0f, y + height/2.0f);
    }
}
