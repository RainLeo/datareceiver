﻿#include "Pen.h"
#include "common/system/Convert.h"
#include "common/data/PrimitiveType.h"
#include "SkDashPathEffect.h"

using namespace Common;

namespace Drawing
{
    Pen::Pen() : Pen(Color(), 1.0f)
    {
    }
	Pen::Pen(const Color& color, float width)
	{
        this->enable = true;
        
		this->color = color;
		this->width = width;
        this->miter = 0.0f;
        
        this->cap = SkPaint::Cap::kDefault_Cap;
        this->join = SkPaint::Join::kDefault_Join;
        this->phase = 0.0f;
	}
    Pen::Pen(const Pen& pen)
    {
        this->operator=(pen);
    }

    bool Pen::isEmpty() const
    {
        return this->color.isEmpty() && this->width == 0.0f;
    }
	string Pen::toString() const
	{
		return Convert::convertStr("[Pen: Color=%s,Width=%.2f]", color.toString().c_str(), width);
	}

	void Pen::operator=(const Pen& value)
	{
        this->enable = value.enable;
		this->color = value.color;
		this->width = value.width;
        this->miter = value.miter;
        
        this->cap = value.cap;
        this->join = value.join;
        this->intervals = value.intervals;
        this->phase = value.phase;
	}
	bool Pen::operator==(const Pen& value) const
	{
		return this->enable == value.enable &&
            this->color == value.color &&
            this->width == value.width &&
            this->miter == value.miter &&
            this->cap == value.cap &&
            this->join == value.join &&
            this->intervals == value.intervals &&
            this->phase == value.phase;
	}
	bool Pen::operator!=(const Pen& value) const
	{
		return !operator==(value);
	}
    
    bool Pen::parse(const string& str, Pen& pen)
    {
        // width:2;color:{gold};miter:0;cap:butt;join:miter;dash:{intervals:{10;20};phase:0}
        Pen temp;
        StringArray texts;
        Convert::splitItems(str, texts);
        for (uint i=0; i<texts.count(); i++)
        {
            StringArray stexts;
            Convert::splitItems(texts[i], stexts, ':');
            if(stexts.count() == 2)
            {
                string name = Convert::trimStr(stexts[0], ' ', '{', '}');
                string value = Convert::trimStartStr(stexts[1], ' ', '{', '}');
                value = Convert::trimEndStr(value, ' ', '{', '}');
                
                if(String::stringEquals(name, "enable", true))
                {
                    if(!Convert::parseBoolean(value, temp.enable))
                        return false;
                }
                else if(String::stringEquals(name, "color", true))
                {
                    if(!Color::parse(value, temp.color))
                        return false;
                }
                else if(String::stringEquals(name, "width", true))
                {
                    if(!(Convert::parseSingle(value, temp.width) && temp.width >= 0))
                        return false;
                }
                else if(String::stringEquals(name, "miter", true))
                {
                    if(!(Convert::parseSingle(value, temp.miter) && temp.miter >= 0))
                        return false;
                }
                else if(String::stringEquals(name, "cap", true))
                {
                    temp.cap = parseCap(value);
                }
                else if(String::stringEquals(name, "join", true))
                {
                    temp.join = parseJoin(value);
                }
                else if(String::stringEquals(name, "dash", true))
                {
                    if(!parseDash(value, temp))
                        return false;
                }
            }
        }
        
        pen = temp;
        return true;
    }
    bool Pen::parseDash(const string& str, Pen& pen)
    {
        bool enable;
        if(Convert::parseBoolean(Convert::trimStr(str), enable) &&
           !enable)
        {
            return true;
        }
        
        StringArray texts;
        Convert::splitItems(str, texts);
        if(texts.count() > 0)
        {
            float phase = 0.0f;
            Array<float> intervals;
            for (uint i=0; i<texts.count(); i++)
            {
                StringArray values;
                Convert::splitItems(texts[i], values, ':');
                if(values.count() == 2)
                {
                    string name = Convert::trimStr(values[0], ' ', '{', '}');
                    string value = Convert::trimStartStr(values[1], ' ', '{', '}');
                    value = Convert::trimEndStr(value, ' ', '{', '}');
                    
                    if(String::stringEquals(name, "intervals", true))
                    {
                        StringArray itexts;
                        Convert::splitItems(value, itexts, ';');
                        if(itexts.count() > 0)
                        {
                            float interval;
                            for (uint j=0; j<itexts.count(); j++)
                            {
                                string istr = Convert::trimStr(Convert::trimStr(itexts[j]));
                                if(!istr.empty())
                                {
                                    if(!Convert::parseSingle(istr, interval))
                                        return false;
                                    intervals.add(interval);
                                }
                            }
                        }
                    }
                    else if(String::stringEquals(name, "phase", true))
                    {
                        if(!(Convert::parseSingle(value, phase) && phase >= 0))
                        {
                            return false;
                        }
                    }
                }
            }
            
            pen.intervals.addRange(&intervals);
            pen.phase = phase;
            
            return true;
        }
        return false;
    }
    
    bool Pen::toSkPaint(SkPaint& paint) const
    {
        if(enable)
        {
            paint.setFlags(SkPaint::Flags::kAntiAlias_Flag);
            paint.setStyle(SkPaint::Style::kStroke_Style);
            paint.setColor(color.toArgb());
            paint.setStrokeWidth(width);
            paint.setStrokeMiter(miter);
            paint.setStrokeCap(cap);
            paint.setStrokeJoin(join);
            
            if(isDash())
            {
                paint.setPathEffect(SkDashPathEffect::Create(intervals.data(), (int)intervals.count(), phase))->unref();
            }
            return true;
        }
        return false;
    }
    
    bool Pen::isDash() const
    {
        return intervals.count() >= 2 && (intervals.count() % 2 == 0);
    }
    
    SkPaint::Cap Pen::parseCap(const string& value)
    {
        if(String::stringEquals(value, "butt", true))
        {
            return SkPaint::Cap::kButt_Cap;
        }
        else if(String::stringEquals(value, "capcount", true))
        {
            return SkPaint::Cap::kCapCount;
        }
        else if(String::stringEquals(value, "round", true))
        {
            return SkPaint::Cap::kRound_Cap;
        }
        else if(String::stringEquals(value, "square", true))
        {
            return SkPaint::Cap::kSquare_Cap;
        }
        return SkPaint::Cap::kButt_Cap;
    }
    string Pen::convertCapStr(SkPaint::Cap value)
    {
        switch (value)
        {
            case SkPaint::Cap::kButt_Cap:
                return "butt";
            case SkPaint::Cap::kCapCount:
                return "capcount";
            case SkPaint::Cap::kRound_Cap:
                return "round";
            case SkPaint::Cap::kSquare_Cap:
                return "square";
            default:
                return "butt";
        }
    }
    
    SkPaint::Join Pen::parseJoin(const string& value)
    {
        if(String::stringEquals(value, "miter", true))
        {
            return SkPaint::Join::kMiter_Join;
        }
        else if(String::stringEquals(value, "round", true))
        {
            return SkPaint::Join::kRound_Join;
        }
        else if(String::stringEquals(value, "joincount", true))
        {
            return SkPaint::Join::kJoinCount;
        }
        else if(String::stringEquals(value, "bevel", true))
        {
            return SkPaint::Join::kBevel_Join;
        }
        return SkPaint::Join::kMiter_Join;
    }
    string Pen::convertJoinStr(SkPaint::Join value)
    {
        switch (value)
        {
            case SkPaint::Join::kMiter_Join:
                return "miter";
            case SkPaint::Join::kRound_Join:
                return "round";
            case SkPaint::Join::kJoinCount:
                return "joincount";
            case SkPaint::Join::kBevel_Join:
                return "bevel";
            default:
                return "miter";
        }
    }
}
