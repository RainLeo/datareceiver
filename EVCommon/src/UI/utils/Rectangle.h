#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "common/common_global.h"
#include "Point.h"
#include "Size.h"

using namespace std;

namespace Drawing
{
	struct Rectangle
	{
	public:
		Rectangle(float x = 0.0f, float y = 0.0f, float width = 0.0f, float height = 0.0f);
		Rectangle(Point location, Size size);

		bool isEmpty() const;
		string toString() const;

		Point location() const;
		Size size() const;

		float left() const;
		float top() const;
		float right() const;
		float bottom() const;

		void operator=(const Rectangle& value);
		bool operator==(const Rectangle& value) const;
		bool operator!=(const Rectangle& value) const;

		bool contains(float x, float y) const;
		bool contains(const Point& point) const;
		bool contains(const Rectangle& rect) const;

		void inflate(float width, float height);
		void inflate(const Size& size);

		void intersect(const Rectangle& rect);
		bool intersectsWith(const Rectangle& rect) const;

		void offset(const Point& pos);
		void offset(float dx, float dy);
        
        void unions(const Rectangle& rect);
        
        Point center() const;

	public:
		static Rectangle intersect(const Rectangle& a, const Rectangle& b);

		// Creates a rectangle that represents the union between a and b.
		static Rectangle unions(const Rectangle& a, const Rectangle& b);

        static bool parse(const string& str, Rectangle& rect);

	public:
		float x;
		float y;
		float width;
		float height;

		static const Rectangle Empty;
	};
    typedef Array<Rectangle> Rectangles;
}

#endif	// RECTANGLE_H
