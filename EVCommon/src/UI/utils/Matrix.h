#ifndef MATRIX_H
#define MATRIX_H

#include <string>
#include "common/common_global.h"
#include "SkMatrix.h"

using namespace std;
using namespace Common;

namespace Drawing
{
    typedef SkMatrix Matrix;
}

#endif	// MATRIX_H
