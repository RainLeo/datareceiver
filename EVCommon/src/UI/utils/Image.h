#ifndef IMAGE_H
#define IMAGE_H

#include <string>
#include "common/common_global.h"
#include "common/xml/ConfigFile.h"
#include "SkImage.h"

using namespace std;
using namespace Common;

namespace Drawing
{
    typedef SkBitmap Bitmap;
    
    class Image
    {
    public:
        enum Source
        {
            FileSource = 1,
            Resource = 2,
        };
        
        // resource id, todo.
        string id;
        Source source;
        string filename;
        ConfigSource configSource;
        
        Image();
        ~Image();
        
        int width();
        int height();
        
        bool isEmpty() const;
        string toString() const;
        
        void operator=(const Image& value);
        bool operator==(const Image& value) const;
        bool operator!=(const Image& value) const;
        
        SkImage* toSkImage();
        
        void setConfigSource(const ConfigFile& cf);
        
    public:
        static bool parse(const string& str, Image& image);
        
    private:
        void update();
        
    private:
        static Source parseSource(const string& value);
        static string convertSourceStr(Source value);
        
    private:
        SkImage* _image;
        
    public:
        static SkImage* readImage(const ConfigSource& cs, const string& filename);
        static bool readBitmap(const ConfigFile& cf, const string& fileName, Bitmap& bitmap);
    };
}

#endif	// IMAGE_H
