/*
 * Copyright 2015 Google Inc.
 *
 *
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 *
 */

#ifndef CANVASWINDOW_DEFINED
#define CANVASWINDOW_DEFINED

#include "SkSurface.h"
#include "SkWindow.h"
#include "SkTouchGesture.h"
#include "../components/Canvas.h"

class GrContext;
struct GrGLInterface;
class GrRenderTarget;
class SkCanvas;

using namespace Drawing;

SkOSWindow* create_sk_window(void* hwnd, int , char** );

class CanvasWindow : public SkOSWindow {
public:
    CanvasWindow(void* hwnd);
    virtual ~CanvasWindow() override;

    // Changes the device type of the object.
    bool setUpBackend();
    
    Canvas* canvas() const;
    
    void draw();
    
#ifdef __ANDROID__
    void postInvalDelay();
#endif

#ifdef DEBUG
    void setSampleDraw(bool sampleDraw);
#endif

protected:
    SkSurface* createSurface() override {
            SkSurfaceProps props(INHERITED::getSurfaceProps());
            return SkSurface::NewRenderTargetDirect(fRenderTarget, &props);
        static const SkImageInfo info = SkImageInfo::MakeN32Premul(
                SkScalarRoundToInt(this->width()), SkScalarRoundToInt(this->height()));
        return fSurface = SkSurface::NewRaster(info);
    }
    
    void draw(SkCanvas* canvas) override;

    void onSizeChange() override;
    bool onHandleChar(SkUnichar unichar) override;
    bool onHandleKey(SkKey key) override;
    bool onEvent(const SkEvent& evt) override;
    
    virtual bool onDispatchClick(int x, int y, Click::State, void* owner,
                                 unsigned modi) override;
    bool onClick(Click* click) override;
    virtual Click* onFindClickHandler(SkScalar x, SkScalar y,
                                      unsigned modi) override;

private:
    void darwContext(SkCanvas* canvas);
    
    void setTitle();
    void setUpRenderTarget();
    
    void tearDownBackend();
    
    void setZoomCenter(float x, float y);
    void changeZoomLevel(float delta);
    bool handleTouch(int ownerId, float x, float y,
                     SkView::Click::State state);
    
    bool zoomIn();
    bool zoomOut();
    void updatePointer(int x, int y);
    void magnify(SkCanvas* canvas);
    void showZoomer(SkCanvas* canvas);
    void updateMatrix();
    
    friend void invalidateInner(SkWindow* win);
    friend void invalidateRectInner(SkWindow* win, const Drawing::Rectangle& rect);

private:
#ifdef __ANDROID__
    static bool isInvalEvent(const SkEvent& evt);
#endif

private:
    Canvas* _canvas;

    // support framework
    SkSurface* fSurface;
    GrContext* fContext;
    GrRenderTarget* fRenderTarget;
    AttachmentInfo fAttachmentInfo;
    const GrGLInterface* fInterface;
    
    // The following are for the 'fatbits' drawing
    // Latest position of the mouse.
    int fMouseX, fMouseY;
    int fFatBitsScale;
    bool fShowZoomer;
    bool fMagnify;
    
    SkTouchGesture fGesture;
    SkScalar fZoomLevel;
    SkScalar fZoomScale;
    
    SkScalar fZoomCenterX, fZoomCenterY;
    
#ifdef DEBUG
    bool _sampleDraw;
#endif

    typedef SkOSWindow INHERITED;
};

#endif	// CANVASWINDOW_DEFINED
