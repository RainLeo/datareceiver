#ifndef POINT_H
#define POINT_H

#include <string>
#include "common/common_global.h"
#include "common/data/Array.h"

using namespace std;
using namespace Common;

namespace Drawing
{
	struct Point
	{
	public:
		Point(float x = 0.0f, float y = 0.0f);

		bool isEmpty() const;
		string toString() const;

		void operator=(const Point& value);
		bool operator==(const Point& value) const;
		bool operator!=(const Point& value) const;
        
        void offset(const Point& pos);
        void offset(float dx, float dy);
        
    public:
        static bool parse(const string& str, Point& point);

	public:
		float x;
		float y;

		static const Point Empty;
        static const Point MinPoint;
        static const Point MaxPoint;
	};
    
    class Points : public Array<Point>
    {
    public:
        Points(uint capacity = Array<Point>::DefaultCapacity) : Array<Point>(capacity)
        {
        }
        
        static bool parse(const string& str, Points& colors);
    };
}

#endif	// POINT_H
