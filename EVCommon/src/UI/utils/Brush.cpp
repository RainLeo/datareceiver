﻿#include "Brush.h"
#include "common/system/Convert.h"
#include "common/data/PrimitiveType.h"
#include "common/system/Math.h"
#include "SkGradientShader.h"
#include "SkConvert.h"
#include "SkComposeShader.h"
#include "Matrix.h"

using namespace Common;

namespace Drawing
{
    bool Brush::SolidInfo::isEmpty() const
    {
        return color.isEmpty();
    }
    string Brush::SolidInfo::toString() const
    {
        return Convert::convertStr("Solid brush: Color=%s]", color.toString().c_str());
    }
    void Brush::SolidInfo::operator=(const SolidInfo& value)
    {
        this->color = value.color;
    }
    bool Brush::SolidInfo::operator==(const SolidInfo& value) const
    {
        return this->color == value.color;
    }
    bool Brush::SolidInfo::operator!=(const SolidInfo& value) const
    {
        return !operator==(value);
    }
    bool Brush::SolidInfo::parse(const string& str, SolidInfo& value)
    {
        // color:navy
        // color:{A:255;R:255;G:255;B:255}
        StringArray values;
        Convert::splitItems(str, values, ':');
        if(values.count() == 2)
        {
            string name = Convert::trimStr(values[0], ' ', '{', '}');
            string text = Convert::trimStartStr(values[1], ' ', '{', '}');
            text = Convert::trimEndStr(text, ' ', '{', '}');
            return Color::parse(text, value.color);
        }
        return false;
    }
    
    bool Brush::LinearInfo::isEmpty() const
    {
        return colors.count() == 0;
    }
    string Brush::LinearInfo::toString() const
    {
        return Convert::convertStr("Linear gradient brush: angle=%.2f, colors=%s, tile mode=%s",
                                   angle, colors.toString().c_str(), convertTileModeStr(tileMode).c_str());
    }
    void Brush::LinearInfo::operator=(const LinearInfo& value)
    {
        this->angle = value.angle;
        this->colors = value.colors;
        this->positions = value.positions;
        this->tileMode = value.tileMode;
    }
    bool Brush::LinearInfo::operator==(const LinearInfo& value) const
    {
        return this->angle == value.angle &&
            this->colors == value.colors &&
            this->positions == value.positions &&
            this->tileMode == value.tileMode;
    }
    bool Brush::LinearInfo::operator!=(const LinearInfo& value) const
    {
        return !operator==(value);
    }
    SkShader::TileMode Brush::LinearInfo::parseTileMode(const string& value)
    {
        if(String::stringEquals(value, "mirror", true))
        {
            return SkShader::kMirror_TileMode;
        }
        else if(String::stringEquals(value, "repeat", true))
        {
            return SkShader::kRepeat_TileMode;
        }
        else if(String::stringEquals(value, "clamp", true))
        {
            return SkShader::kClamp_TileMode;
        }
        return SkShader::kMirror_TileMode;
    }
    string Brush::LinearInfo::convertTileModeStr(SkShader::TileMode value)
    {
        switch (value)
        {
            case SkShader::kMirror_TileMode:
                return "mirror";
            case SkShader::kRepeat_TileMode:
                return "repeat";
            case SkShader::kClamp_TileMode:
                return "clamp";
            default:
                return "mirror";
        }
    }
    bool Brush::LinearInfo::parse(const string& str, LinearInfo& value)
    {
        // angle:0.0f;colors:{color:{A:255;R:0;G:255;B:0};color:{red}};mode:mirror
        StringArray texts;
        Convert::splitItems(str, texts);
        if(texts.count() > 0)
        {
            LinearInfo temp;
            for (uint i=0; i<texts.count(); i++)
            {
                StringArray values;
                Convert::splitItems(texts[i], values, ':');
                if(values.count() == 2)
                {
                    string name = Convert::trimStr(values[0], ' ', '{', '}');
                    string text = Convert::trimStartStr(values[1], ' ', '{', '}');
                    text = Convert::trimEndStr(text, ' ', '{', '}');
                    
                    if(String::stringEquals(name, "angle", true))
                    {
                        if(!Convert::parseSingle(text, temp.angle))
                            return false;
                    }
                    else if(String::stringEquals(name, "colors", true))
                    {
                        if(!Colors::parse(text, temp.colors))
                            return false;
                    }
                    else if(String::stringEquals(name, "positions", true))
                    {
                        StringArray texts;
                        Convert::splitItems(text, texts, ';');
                        if(texts.count() > 0)
                        {
                            float value;
                            for (uint j=0; j<texts.count(); j++)
                            {
                                if(!Convert::parseSingle(Convert::trimStr(texts[j]), value))
                                    return false;
                                temp.positions.add(value);
                            }
                        }
                    }
                    else if(String::stringEquals(name, "mode", true))
                    {
                        temp.tileMode = parseTileMode(text);
                    }
                }
            }
            
            value = temp;
            
            return true;
        }
        return false;
    }
    
    bool Brush::RadialInfo::isEmpty() const
    {
        return colors.count() == 0;
    }
    string Brush::RadialInfo::toString() const
    {
        return Convert::convertStr("Radial gradient brush: colors=%s, tile mode=%s",
                                   colors.toString().c_str(), LinearInfo::convertTileModeStr(tileMode).c_str());
    }
    void Brush::RadialInfo::operator=(const RadialInfo& value)
    {
        this->colors = value.colors;
        this->positions = value.positions;
        this->tileMode = value.tileMode;
    }
    bool Brush::RadialInfo::operator==(const RadialInfo& value) const
    {
        return this->colors == value.colors &&
            this->positions == value.positions &&
			this->tileMode == value.tileMode;
    }
    bool Brush::RadialInfo::operator!=(const RadialInfo& value) const
    {
        return !operator==(value);
    }
    bool Brush::RadialInfo::parse(const string& str, RadialInfo& value)
    {
        // colors:{color:{A:255;R:0;G:255;B:0};color:{red}};mode:mirror
        StringArray texts;
        Convert::splitItems(str, texts);
        if(texts.count() > 0)
        {
            RadialInfo temp;
            for (uint i=0; i<texts.count(); i++)
            {
                StringArray values;
                Convert::splitItems(texts[i], values, ':');
                if(values.count() == 2)
                {
                    string name = Convert::trimStr(values[0], ' ', '{', '}');
                    string text = Convert::trimStartStr(values[1], ' ', '{', '}');
                    text = Convert::trimEndStr(text, ' ', '{', '}');
                    
                    if(String::stringEquals(name, "colors", true))
                    {
                        if(!Colors::parse(text, temp.colors))
                            return false;
                    }
                    else if(String::stringEquals(name, "positions", true))
                    {
                        StringArray posTexts;
                        Convert::splitItems(text, posTexts);
                        if(posTexts.count() > 0)
                        {
                            float pos;
                            for (uint j=0; j<posTexts.count(); j++)
                            {
                                if(!Convert::parseSingle(Convert::trimStr(posTexts[j]), pos))
                                    return false;
                                temp.positions.add(pos);
                            }
                        }
                    }
                    else if(String::stringEquals(name, "mode", true))
                    {
                        temp.tileMode = LinearInfo::parseTileMode(text);
                    }
                }
            }
            
            value = temp;
            
            return true;
        }
        return false;
    }
    
    bool Brush::TextureInfo::isEmpty() const
    {
        return image.isEmpty();
    }
    string Brush::TextureInfo::toString() const
    {
        return Convert::convertStr("Texture brush: image=%s]", image.toString().c_str());
    }
    void Brush::TextureInfo::operator=(const TextureInfo& value)
    {
        this->layout = value.layout;
        this->tileModeX = value.tileModeX;
        this->tileModeY = value.tileModeY;
        this->position = value.position;
        this->image = value.image;
    }
    bool Brush::TextureInfo::operator==(const TextureInfo& value) const
    {
        return this->layout == value.layout &&
            this->tileModeX == value.tileModeX &&
            this->tileModeY == value.tileModeY &&
            this->position == value.position &&
            this->image == value.image;
    }
    bool Brush::TextureInfo::operator!=(const TextureInfo& value) const
    {
        return !operator==(value);
    }
    bool Brush::TextureInfo::parse(const string& str, TextureInfo& value)
    {
        // image:{filename:images/test.jpg;source:file};layout:center;position:owner;modex:repeat;modey:repeat
        StringArray texts;
        Convert::splitItems(str, texts);
        if(texts.count() > 0)
        {
            TextureInfo temp;
            for (uint i=0; i<texts.count(); i++)
            {
                StringArray values;
                Convert::splitItems(texts[i], values, ':');
                if(values.count() == 2)
                {
                    string name = Convert::trimStr(values[0], ' ', '{', '}');
                    string text = Convert::trimStartStr(values[1], ' ', '{', '}');
                    text = Convert::trimEndStr(text, ' ', '{', '}');
                    
                    if(String::stringEquals(name, "image", true))
                    {
                        if(!Image::parse(text, temp.image))
                            return false;
                    }
                    else if(String::stringEquals(name, "layout", true))
                    {
                        temp.layout = parseFillLayout(text);
                    }
                    else if(String::stringEquals(name, "modex", true))
                    {
                        temp.tileModeX = LinearInfo::parseTileMode(text);
                    }
                    else if(String::stringEquals(name, "modey", true))
                    {
                        temp.tileModeY = LinearInfo::parseTileMode(text);
                    }
                    else if(String::stringEquals(name, "position", true))
                    {
                        temp.position = parsePosition(text);
                    }
                }
            }
            
            value = temp;
            
            return true;
        }
        return false;
    }
    Brush::TextureInfo::FillLayout Brush::TextureInfo::parseFillLayout(const string& value)
    {
        if(String::stringEquals(value, "wrap", true))
        {
            return FillLayout::Wrap;
        }
        else if(String::stringEquals(value, "center", true))
        {
            return FillLayout::Center;
        }
        else if(String::stringEquals(value, "stretch", true))
        {
            return FillLayout::Stretch;
        }
        return FillLayout::Wrap;
    }
    string Brush::TextureInfo::convertFillLayoutStr(FillLayout value)
    {
        switch (value)
        {
            case FillLayout::Wrap:
                return "wrap";
            case FillLayout::Center:
                return "center";
            case FillLayout::Stretch:
                return "stretch";
            default:
                return "wrap";
        }
    }
    Brush::TextureInfo::Position Brush::TextureInfo::parsePosition(const string& value)
    {
        if(String::stringEquals(value, "owner", true))
        {
            return TextureInfo::Owner;
        }
        else if(String::stringEquals(value, "parent", true))
        {
            return TextureInfo::Parent;
        }
        return TextureInfo::Owner;
    }
    string Brush::TextureInfo::convertPositionStr(TextureInfo::Position value)
    {
        switch (value)
        {
            case TextureInfo::Owner:
                return "owner";
            case TextureInfo::Parent:
                return "parent";
            default:
                return "owner";
        }
    }
    
    Brush::Brush()
    {
        this->enable = true;
        this->type = BrushTypes::Empty;
        
    }
    Brush::Brush(const Brush& brush)
    {
        this->operator=(brush);
    }
    Brush::Brush(const Color& color)
    {
        enable = true;
        solid.color = color;
        type = BrushTypes::Solid;
    }
    bool Brush::parse(const string& str, Brush& brush)
    {
        // eanble:true;color:navy
        // eanble:true;color:{A:255;R:255;G:255;B:255}
        // eanble:true;solid:{color:navy}
        // eanble:true;solid:{color:{A:255;R:255;G:255;B:255}}
        // eanble:true;linear:{angle:0.0f;colors:{color:{A:255;R:0;G:0;B:0};color:{white}};mode:mirror}
        // eanble:true;radial:{colors:{color:{A:255;R:0;G:255;B:0};color:{red}};mode:mirror}
        // eanble:true;texture:{filename:images/kingview.jpg;layout:center;wrapmode:tile;position:owner}
        StringArray texts;
        Convert::splitItems(str, texts);
        for (uint i=0; i<texts.count(); i++)
        {
            StringArray stexts;
            Convert::splitItems(texts[i], stexts, ':');
            if(stexts.count() == 2)
            {
                string name = Convert::trimStr(stexts[0], ' ', '{', '}');
                string value = Convert::trimStartStr(stexts[1], ' ', '{', '}');
                value = Convert::trimEndStr(value, ' ', '{', '}');
                
                if(String::stringEquals(name, "enable", true))
                {
                    if(!Convert::parseBoolean(value, brush.enable))
                        return false;
                }
                else if(String::stringEquals(name, "solid") &&
                        SolidInfo::parse(value, brush.solid))
                {
                    brush.type = BrushTypes::Solid;
                    return true;
                }
                else if(String::stringEquals(name, "linear") &&
                        LinearInfo::parse(value, brush.linear))
                {
                    brush.type = BrushTypes::LinearGradient;
                    return true;
                }
                else if(String::stringEquals(name, "radial") &&
                        RadialInfo::parse(value, brush.radial))
                {
                    brush.type = BrushTypes::RadialGradient;
                    return true;
                }
                else if(String::stringEquals(name, "texture") &&
                        TextureInfo::parse(value, brush.texture))
                {
                    brush.type = BrushTypes::Texture;
                    return true;
                }
                else if(SolidInfo::parse(str, brush.solid))
                {
                    brush.type = BrushTypes::Solid;
                    return true;
                }
            }
        }
        return false;
    }
    
    bool Brush::isEmpty() const
    {
        return type == BrushTypes::Empty;
    }
    string Brush::toString() const
    {
        switch (type)
        {
            case BrushTypes::Solid:
                return solid.toString();
            case BrushTypes::LinearGradient:
                return linear.toString();
            case BrushTypes::RadialGradient:
                return radial.toString();
            case BrushTypes::Texture:
                return texture.toString();
            default:
                break;
        }
        return "";
    }
    
    void Brush::operator=(const Brush& value)
    {
        this->enable = value.enable;
        this->type = value.type;
        switch (value.type)
        {
            case BrushTypes::Solid:
                this->solid = value.solid;
                break;
            case BrushTypes::LinearGradient:
                this->linear = value.linear;
                break;
            case BrushTypes::RadialGradient:
                this->radial = value.radial;
                break;
            case BrushTypes::Texture:
                this->texture = value.texture;
                break;
            default:
                break;
        }
    }
    bool Brush::operator==(const Brush& value) const
    {
        if(this->enable != value.enable)
            return false;
        if(this->type != value.type)
            return false;
        
        switch (type)
        {
            case BrushTypes::Solid:
                return this->solid == value.solid;
            case BrushTypes::LinearGradient:
                return this->linear == value.linear;
            case BrushTypes::RadialGradient:
                return this->radial == value.radial;
            case BrushTypes::Texture:
                return this->texture == value.texture;
            default:
                break;
        }
        return false;
    }
    bool Brush::operator!=(const Brush& value) const
    {
        return !operator==(value);
    }
    
    Brush::BrushTypes Brush::parseType(const string& value)
    {
        if(String::stringEquals(value, "solid", true))
        {
            return BrushTypes::Solid;
        }
        else if(String::stringEquals(value, "linear", true))
        {
            return BrushTypes::LinearGradient;
        }
        else if(String::stringEquals(value, "texture", true))
        {
            return BrushTypes::Texture;
        }
        else if(String::stringEquals(value, "radial", true))
        {
            return BrushTypes::RadialGradient;
        }
        else if(String::stringEquals(value, "texture", true))
        {
            return BrushTypes::Texture;
        }
        return BrushTypes::Empty;
    }
    string Brush::convertTypeStr(BrushTypes value)
    {
        switch (value)
        {
            case BrushTypes::Solid:
                return "solid";
            case BrushTypes::LinearGradient:
                return "linear";
            case BrushTypes::RadialGradient:
                return "radial";
            case BrushTypes::Texture:
                return "texture";
            default:
                return "empty";
        }
    }
    
    void Brush::update(const Rectangle& bounds)
    {
        switch (type)
        {
            case BrushTypes::Solid:
                break;
            case BrushTypes::LinearGradient:
                linear._bounds = bounds;
                break;
            case BrushTypes::RadialGradient:
                radial._bounds = bounds;
                break;
            case BrushTypes::Texture:
                texture._bounds = bounds;
                break;
            default:
                break;
        }
    }
    
    bool Brush::toSkPaint(SkPaint& paint)
    {
        if(enable)
        {
            switch (type)
            {
                case BrushTypes::Solid:
                    return toSolid(solid, paint);
                case BrushTypes::LinearGradient:
                    return toLinearGradient(linear, paint);
                case BrushTypes::RadialGradient:
                    return toRadialGradient(radial, paint);
                case BrushTypes::Texture:
                    return toTexture(texture, paint);
                default:
                    break;
            }
        }
        return false;
    }
    
    bool Brush::toSolid(const SolidInfo& info, SkPaint& paint)
    {
        paint.setFlags(SkPaint::Flags::kAntiAlias_Flag);
        paint.setStyle(SkPaint::Style::kFill_Style);
        paint.setColor(info.color.toArgb());
        return true;
    }
    bool Brush::toLinearGradient(const LinearInfo& info, SkPaint& paint)
    {
        Rectangle bounds = info._bounds;
        if(bounds.width == 0.0f)
            return false;
        if(bounds.isEmpty())
            return false;
        if(bounds.size().isEmpty())
            return false;
        
        SkPoint p1, p2;
        Point center = bounds.center();
        float angle = info.angle;
        float angle1 = 0.0f, angle2 = 0.0f;
        if(angle >= 0.0f && angle <= 90.0f)
        {
            angle2 = angle;
            angle1 = angle + 180.0f;
            angle = angle;
        }
        else if(angle > 90.0f && angle <= 180.0f)
        {
            angle2 = angle;
            angle1 = angle + 180.0f;
            angle = 180.0f - angle;
        }
        else if(angle > 180.0f && angle <= 270.0f)
        {
            angle2 = angle;
            angle1 = angle - 180.0f;
            angle = angle - 180.0f;
        }
        else if(angle > 270.0f && angle <= 360.0f)
        {
            angle2 = angle;
            angle1 = angle - 180.0f;
            angle = 360.0f - angle;
        }
        
        float x, y, l;
        float height = center.y - bounds.y;
        assert(angle >= 0.0f && angle <= 90.0f);
        float sValue = Math::sin(angle);
        l = sValue != 0.0f ? height / sValue : bounds.width/2.0f;
        Math::calcCoordinate(angle1, l, x, y);
        p1 = SkPoint::Make(x, y);
        p1.offset(center.x, center.y);
        Math::calcCoordinate(angle2, l, x, y);
        p2 = SkPoint::Make(x, y);
        p2.offset(center.x, center.y);
        
        SkPoint linearPoints[] = { p1, p2 };
        SkColors linearColors;
        SkColorConvert::toSkColor(info.colors, linearColors);
        if((linearColors.count() == info.positions.count() && linearColors.count() >= 2) ||
           (info.positions.count() == 0 && linearColors.count() >= 2))
        {
            paint.setFlags(SkPaint::Flags::kAntiAlias_Flag);
            paint.setStyle(SkPaint::Style::kFill_Style);
            
            SkShader* shader = SkGradientShader::CreateLinear(
                                                              linearPoints, linearColors.data(),
                                                              info.positions.count() >= 2 ? info.positions.data() : NULL,
                                                              linearColors.count(), info.tileMode);
            SkAutoUnref shader_deleter(shader);
            
            paint.setShader(shader);
            
            return true;
        }
        return false;
    }
    bool Brush::toRadialGradient(const RadialInfo& info, SkPaint& paint)
    {
        Rectangle bounds = info._bounds;
        if(bounds.width == 0.0f)
            return false;
        if(bounds.isEmpty())
            return false;
        if(bounds.size().isEmpty())
            return false;
        
        Point center = bounds.center();
        SkPoint centerPoint = SkPointConvert::convert(center);
        float radian = Math::atan(bounds.height / bounds.width);
        float radius = bounds.height * Math::sin(radian);
        SkColors radialColors;
        SkColorConvert::toSkColor(info.colors, radialColors);
        if((radialColors.count() == info.positions.count() && radialColors.count() >= 2) ||
           (info.positions.count() == 0 && radialColors.count() >= 2))
        {
            paint.setFlags(SkPaint::Flags::kAntiAlias_Flag);
            paint.setStyle(SkPaint::Style::kFill_Style);
            
            SkShader* shader = SkGradientShader::CreateRadial(
                                                              centerPoint, radius, radialColors.data(),
                                                              info.positions.count() >= 2 ? info.positions.data() : NULL,
                                                              radialColors.count(), info.tileMode);
            SkAutoUnref shader_deleter(shader);
            
            paint.setShader(shader);
            
            return true;
        }
        return false;
    }
    bool Brush::toTexture(TextureInfo& info, SkPaint& paint)
    {
        Rectangle bounds = info._bounds;
        if(bounds.width == 0.0f)
            return false;
        if(bounds.isEmpty())
            return false;
        if(bounds.size().isEmpty())
            return false;
        
        SkImage* image = info.image.toSkImage();
        if(image != nullptr)
        {
            paint.setFlags(SkPaint::Flags::kAntiAlias_Flag);
            paint.setStyle(SkPaint::Style::kFill_Style);

            SkShader* shader;
            if(info.layout == TextureInfo::FillLayout::Stretch)
            {
                Matrix matrix;
                matrix.reset();
                matrix.postScale(bounds.width / image->width(), bounds.height / image->height());
                if(info.position == TextureInfo::Position::Owner)
                    matrix.postTranslate(bounds.x, bounds.y);
                shader = image->newShader(SkShader::TileMode::kClamp_TileMode, SkShader::TileMode::kClamp_TileMode, &matrix);
            }
            else if(info.layout == TextureInfo::FillLayout::Center)
            {
                Rectangle drawingBounds;
                Size imageSize(image->width(), image->height());
                Size drawingSize = bounds.size();
                float widthOffset = drawingSize.width - imageSize.width;
                float heightOffset = drawingSize.height - imageSize.height;
                if (widthOffset <= 0.0f)
                {
                    if (heightOffset <= 0.0f)
                    {
                        drawingBounds = bounds;
                    }
                    else
                    {
                        float offsetY = (heightOffset / 2.0f);
                        float drawingY = (bounds.y + offsetY);
                        drawingBounds = Rectangle(bounds.x, drawingY, drawingSize.width, imageSize.height);
                    }
                }
                else
                {
                    if (heightOffset <= 0.0f)
                    {
                        float offsetX = (widthOffset / 2.0f);
                        float drawingX = (bounds.x + offsetX);
                        drawingBounds = Rectangle(drawingX, bounds.y, imageSize.width, drawingSize.height);
                    }
                    else
                    {
                        float offsetX = (widthOffset / 2.0f);
                        float offsetY = (heightOffset / 2.0f);
                        float drawingX = (bounds.x + offsetX);
                        float drawingY = (bounds.y + offsetY);
                        drawingBounds = Rectangle(drawingX, drawingY, imageSize.width, imageSize.height);
                    }
                }
                if(info.position == TextureInfo::Position::Parent)
                {
                    drawingBounds.x -= bounds.x;
                    drawingBounds.y -= bounds.y;
                }

                Matrix matrix;
                matrix.reset();
                float scaleX = drawingBounds.width / imageSize.width;
                float scaleY = drawingBounds.height / imageSize.height;
                matrix.postScale(scaleX, scaleY);
                matrix.postTranslate(drawingBounds.x, drawingBounds.y);
                shader = image->newShader(SkShader::TileMode::kClamp_TileMode, SkShader::TileMode::kClamp_TileMode, &matrix);
            }
            else
            {
                assert(info.layout == TextureInfo::FillLayout::Wrap);
                shader = image->newShader(info.tileModeX, info.tileModeY, nullptr);
            }
            
            SkAutoUnref shader_deleter(shader);
            
            paint.setShader(shader);
            
            return true;
        }
        return false;
    }
    
    void Brush::setConfigSource(const ConfigFile& cf)
    {
        if(type == Brush::Texture)
            texture.image.setConfigSource(cf);
    }
}
