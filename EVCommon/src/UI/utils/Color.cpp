﻿#include "Color.h"
#include "SystemColor.h"
#include "common/system/Convert.h"
#include "common/data/PrimitiveType.h"

using namespace Common;

namespace Drawing
{
	const Color Color::Empty;

	Color::Color(uint value)
	{
		_value = value;
	}

	bool Color::isEmpty() const
	{
		return _value == 0;
	}
	string Color::toString() const
	{
        // R:255;G:255;B:255
        // A:255;R:255;G:255;B:255
        // system color: suck like 'red'
        string str;
        if(SystemColor::toSystemColorStr(*this, str))
        {
            return str;
        }
        return Convert::convertStr("a:%d;r:%d;g:%d;b:%d", a(), r(), g(), b());
	}

	byte Color::a() const
	{
		return(byte)((_value >> ARGBAlphaShift) & 0xFF);
	}
	byte Color::r() const
	{
		return(byte)((_value >> ARGBRedShift) & 0xFF);
	}
	byte Color::g() const
	{
		return(byte)((_value >> ARGBGreenShift) & 0xFF);
	}
	byte Color::b() const
	{
		return(byte)((_value >> ARGBBlueShift) & 0xFF);
	}

	void Color::operator=(const Color& value)
	{
		this->_value = value._value;
	}
	bool Color::operator==(const Color& value) const
	{
		return this->_value == value._value;
	}
	bool Color::operator!=(const Color& value) const
	{
		return !operator==(value);
	}

	Color Color::fromArgb(byte alpha, byte red, byte green, byte blue)
	{
		return Color(makeArgb(alpha, red, green, blue));
	}
	uint Color::makeArgb(byte alpha, byte red, byte green, byte blue)
	{
		return(uint)((uint)(red << ARGBRedShift |
			green << ARGBGreenShift |
			blue << ARGBBlueShift |
			alpha << ARGBAlphaShift)) & 0xffffffff;
	}

	/// <include file='doc\Color.uex' path='docs/doc[@for="Color.GetBrightness"]/*' />
	/// <devdoc>
	///    <para>
	///       Returns the Hue-Saturation-Brightness (HSB) brightness
	///       for this <see cref='System.Drawing.Color'/> .
	///    </para>
	/// </devdoc>
	float Color::brightness() const
	{
		float r1 = (float)r() / 255.0f;
		float g1 = (float)g() / 255.0f;
		float b1 = (float)b() / 255.0f;

		float max, min;

		max = r1; min = r1;

		if (g1 > max) max = g1;
		if (b1 > max) max = b1;

		if (g1 < min) min = g1;
		if (b1 < min) min = b1;

		return(max + min) / 2;
	}


	/// <include file='doc\Color.uex' path='docs/doc[@for="Color.GetHue"]/*' />
	/// <devdoc>
	///    <para>
	///       Returns the Hue-Saturation-Brightness (HSB) hue
	///       value, in degrees, for this <see cref='System.Drawing.Color'/> .  
	///       If r() == g() == b(), the hue is meaningless, and the return value is 0.
	///    </para>
	/// </devdoc>
	float Color::hue() const
	{
		if (r() == g() && g() == b())
			return 0; // 0 makes as good an UNDEFINED value as any

		float r1 = (float)r() / 255.0f;
		float g1 = (float)g() / 255.0f;
		float b1 = (float)b() / 255.0f;

		float max, min;
		float delta;
		float hue = 0.0f;

		max = r1; min = r1;

		if (g1 > max) max = g1;
		if (b1 > max) max = b1;

		if (g1 < min) min = g1;
		if (b1 < min) min = b1;

		delta = max - min;

		if (r1 == max) {
			hue = (g1 - b1) / delta;
		}
		else if (g1 == max) {
			hue = 2 + (b1 - r1) / delta;
		}
		else if (b1 == max) {
			hue = 4 + (r1 - g1) / delta;
		}
		hue *= 60;

		if (hue < 0.0f) {
			hue += 360.0f;
		}
		return hue;
	}

	/// <include file='doc\Color.uex' path='docs/doc[@for="Color.GetSaturation"]/*' />
	/// <devdoc>
	///    <para>
	///       The Hue-Saturation-Brightness (HSB) saturation for this
	///    <see cref='System.Drawing.Color'/>
	///    .
	/// </para>
	/// </devdoc>
	float Color::saturation() const
	{
		float r1 = (float)r() / 255.0f;
		float g1 = (float)g() / 255.0f;
		float b1 = (float)b() / 255.0f;

		float max, min;
		float l, s = 0;

		max = r1; min = r1;

		if (g1 > max) max = g1;
		if (b1 > max) max = b1;

		if (g1 < min) min = g1;
		if (b1 < min) min = b1;

		// if max == min, then there is no color and
		// the saturation is zero.
		//
		if (max != min) {
			l = (max + min) / 2;

			if (l <= .5) {
				s = (max - min) / (max + min);
			}
			else {
				s = (max - min) / (2 - max - min);
			}
		}
		return s;
	}

	/// <include file='doc\Color.uex' path='docs/doc[@for="Color.ToArgb"]/*' />
	/// <devdoc>
	///    <para>
	///       Returns the ARGB value of this <see cref='System.Drawing.Color'/> .
	///    </para>
	/// </devdoc>
	uint Color::toArgb() const
	{
		return _value;
	}
    
    bool Color::parse(const string& str, Color& color)
    {
        if(parseARGB(str, color))
            return true;
        if(parseConstColor(str, color))
            return true;
        
        return false;
    }
    bool Color::parseConstColor(const string& str, Color& color)
    {
        // system color: suck like 'red'
        return SystemColor::parse(str, color);
    }
    bool Color::parseARGB(const string& str, Color& color)
    {
        // R:255;G:255;B:255
        // A:255;R:255;G:255;B:255
        byte c[4];      // a,r,g,b
        bool hasValue[4];   // a,r,g,b
        memset(hasValue, 0, sizeof(hasValue));
        StringArray texts;
        Convert::splitStr(str, ';', texts);
        if(texts.count() >= 3)
        {
            for (uint i=0; i<texts.count(); i++)
            {
                StringArray values;
                Convert::splitStr(texts[i], ':', values);
                if(values.count() == 2)
                {
                    string name = Convert::trimStr(values[0], ' ', '{', '}');
                    string value = Convert::trimStr(values[1], ' ', '{', '}');
                    
                    if(String::stringEquals(name, "a", true) &&
                       Convert::parseByte(value, c[0]))
                    {
                        hasValue[0] = true;
                    }
                    else if(String::stringEquals(name, "r", true) &&
                            Convert::parseByte(value, c[1]))
                    {
                        hasValue[1] = true;
                    }
                    else if(String::stringEquals(name, "g", true) &&
                            Convert::parseByte(value, c[2]))
                    {
                        hasValue[2] = true;
                    }
                    else if(String::stringEquals(name, "b", true) &&
                            Convert::parseByte(value, c[3]))
                    {
                        hasValue[3] = true;
                    }
                }
            }
            
            if(hasValue[1] && hasValue[2] && hasValue[3])
            {
                color = fromArgb(hasValue[0] ? c[0] : 255, c[1], c[2], c[3]);
                return true;
            }
        }
        return false;
    }
    
    string Colors::toString() const
    {
        StringBuilder sb;
        for (uint i=0; i<count(); i++)
        {
            if(i > 0)
                sb.append(";");
            
            sb.append(at(i).toString());
        }
        return sb.toString();
    }
    // color:{A:255;R:0;G:255;B:0};color:{red}
    bool Colors::parse(const string& str, Colors& colors)
    {
        Convert::KeyPairs pairs;
        if(Convert::splitItems(str, pairs))
        {
            Color color;
            for (uint i=0; i<pairs.count(); i++)
            {
                const Convert::KeyPair* kp = pairs[i];
                if(String::stringEquals(kp->name, "color", true) &&
                   Color::parse(kp->value, color))
                {
                    colors.add(color);
                }
                else
                {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}
