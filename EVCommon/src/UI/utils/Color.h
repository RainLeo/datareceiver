#ifndef COLOR_H
#define COLOR_H

#include <string>
#include "common/common_global.h"
#include "common/data/Array.h"

using namespace std;
using namespace Common;

namespace Drawing
{
    class SystemColor;
	struct Color
	{
	public:
        Color(uint value = 0);
        
		bool isEmpty() const;
		string toString() const;

		byte a() const;
		byte r() const;
		byte g() const;
		byte b() const;

		void operator=(const Color& value);
		bool operator==(const Color& value) const;
		bool operator!=(const Color& value) const;

		/// <include file='doc\Color.uex' path='docs/doc[@for="Color.GetBrightness"]/*' />
		/// <devdoc>
		///    <para>
		///       Returns the Hue-Saturation-Brightness (HSB) brightness
		///       for this <see cref='System.Drawing.Color'/> .
		///    </para>
		/// </devdoc>
		float brightness() const;

		/// <include file='doc\Color.uex' path='docs/doc[@for="Color.GetHue"]/*' />
		/// <devdoc>
		///    <para>
		///       Returns the Hue-Saturation-Brightness (HSB) hue
		///       value, in degrees, for this <see cref='System.Drawing.Color'/> .  
		///       If R == G == B, the hue is meaningless, and the return value is 0.
		///    </para>
		/// </devdoc>
		float hue() const;

		/// <include file='doc\Color.uex' path='docs/doc[@for="Color.GetSaturation"]/*' />
		/// <devdoc>
		///    <para>
		///       The Hue-Saturation-Brightness (HSB) saturation for this
		///    <see cref='System.Drawing.Color'/>
		///    .
		/// </para>
		/// </devdoc>
		float saturation() const;

		/// <include file='doc\Color.uex' path='docs/doc[@for="Color.ToArgb"]/*' />
		/// <devdoc>
		///    <para>
		///       Returns the ARGB value of this <see cref='System.Drawing.Color'/> .
		///    </para>
		/// </devdoc>
		uint toArgb() const;

	public:
		static Color fromArgb(byte alpha, byte red, byte green, byte blue);
        
        static bool parse(const string& str, Color& color);
        
	private:
		static uint makeArgb(byte alpha, byte red, byte green, byte blue);
        
        static bool parseARGB(const string& str, Color& color);
        static bool parseConstColor(const string& str, Color& color);

	public:
		static const Color Empty;

	private:
		uint _value;

	private:
        friend SystemColor;
        
		/**
		* Shift count and bit mask for A, R, G, B components in ARGB mode!
		*/
		const static int ARGBAlphaShift = 24;
		const static int ARGBRedShift = 16;
		const static int ARGBGreenShift = 8;
		const static int ARGBBlueShift = 0;
	};
    class Colors : public Array<Color>
    {
    public:
        Colors(uint capacity = Array<Color>::DefaultCapacity) : Array<Color>(capacity)
        {
        }
        
        string toString() const;
        
        static bool parse(const string& str, Colors& colors);
    };
}

#endif	// COLOR_H
