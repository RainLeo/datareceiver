#ifndef SYSTEMCOLOR_H
#define SYSTEMCOLOR_H

#include <string>
#include "common/common_global.h"
#include "Color.h"

using namespace std;

namespace Drawing
{
	class SystemColor
	{
	private:
		/// Enum containing handles to all known colors
		/// Since the first element is 0, second is 1, etc, we can use this to index
		/// directly into an array
		enum KnownColor : uint
		{
			// We've reserved the value "1" as unknown.  If for some odd reason "1" is added to the
			// list, redefined UnknownColor

			AliceBlue = 0xFFF0F8FF,
			AntiqueWhite = 0xFFFAEBD7,
			Aqua = 0xFF00FFFF,
			Aquamarine = 0xFF7FFFD4,
			Azure = 0xFFF0FFFF,
			Beige = 0xFFF5F5DC,
			Bisque = 0xFFFFE4C4,
			Black = 0xFF000000,
			BlanchedAlmond = 0xFFFFEBCD,
			Blue = 0xFF0000FF,
			BlueViolet = 0xFF8A2BE2,
			Brown = 0xFFA52A2A,
			BurlyWood = 0xFFDEB887,
			CadetBlue = 0xFF5F9EA0,
			Chartreuse = 0xFF7FFF00,
			Chocolate = 0xFFD2691E,
			Coral = 0xFFFF7F50,
			CornflowerBlue = 0xFF6495ED,
			Cornsilk = 0xFFFFF8DC,
			Crimson = 0xFFDC143C,
			Cyan = 0xFF00FFFF,
			DarkBlue = 0xFF00008B,
			DarkCyan = 0xFF008B8B,
			DarkGoldenrod = 0xFFB8860B,
			DarkGray = 0xFFA9A9A9,
			DarkGreen = 0xFF006400,
			DarkKhaki = 0xFFBDB76B,
			DarkMagenta = 0xFF8B008B,
			DarkOliveGreen = 0xFF556B2F,
			DarkOrange = 0xFFFF8C00,
			DarkOrchid = 0xFF9932CC,
			DarkRed = 0xFF8B0000,
			DarkSalmon = 0xFFE9967A,
			DarkSeaGreen = 0xFF8FBC8F,
			DarkSlateBlue = 0xFF483D8B,
			DarkSlateGray = 0xFF2F4F4F,
			DarkTurquoise = 0xFF00CED1,
			DarkViolet = 0xFF9400D3,
			DeepPink = 0xFFFF1493,
			DeepSkyBlue = 0xFF00BFFF,
			DimGray = 0xFF696969,
			DodgerBlue = 0xFF1E90FF,
			Firebrick = 0xFFB22222,
			FloralWhite = 0xFFFFFAF0,
			ForestGreen = 0xFF228B22,
			Fuchsia = 0xFFFF00FF,
			Gainsboro = 0xFFDCDCDC,
			GhostWhite = 0xFFF8F8FF,
			Gold = 0xFFFFD700,
			Goldenrod = 0xFFDAA520,
			Gray = 0xFF808080,
			Green = 0xFF008000,
			GreenYellow = 0xFFADFF2F,
			Honeydew = 0xFFF0FFF0,
			HotPink = 0xFFFF69B4,
			IndianRed = 0xFFCD5C5C,
			Indigo = 0xFF4B0082,
			Ivory = 0xFFFFFFF0,
			Khaki = 0xFFF0E68C,
			Lavender = 0xFFE6E6FA,
			LavenderBlush = 0xFFFFF0F5,
			LawnGreen = 0xFF7CFC00,
			LemonChiffon = 0xFFFFFACD,
			LightBlue = 0xFFADD8E6,
			LightCoral = 0xFFF08080,
			LightCyan = 0xFFE0FFFF,
			LightGoldenrodYellow = 0xFFFAFAD2,
			LightGreen = 0xFF90EE90,
			LightGray = 0xFFD3D3D3,
			LightPink = 0xFFFFB6C1,
			LightSalmon = 0xFFFFA07A,
			LightSeaGreen = 0xFF20B2AA,
			LightSkyBlue = 0xFF87CEFA,
			LightSlateGray = 0xFF778899,
			LightSteelBlue = 0xFFB0C4DE,
			LightYellow = 0xFFFFFFE0,
			Lime = 0xFF00FF00,
			LimeGreen = 0xFF32CD32,
			Linen = 0xFFFAF0E6,
			Magenta = 0xFFFF00FF,
			Maroon = 0xFF800000,
			MediumAquamarine = 0xFF66CDAA,
			MediumBlue = 0xFF0000CD,
			MediumOrchid = 0xFFBA55D3,
			MediumPurple = 0xFF9370DB,
			MediumSeaGreen = 0xFF3CB371,
			MediumSlateBlue = 0xFF7B68EE,
			MediumSpringGreen = 0xFF00FA9A,
			MediumTurquoise = 0xFF48D1CC,
			MediumVioletRed = 0xFFC71585,
			MidnightBlue = 0xFF191970,
			MintCream = 0xFFF5FFFA,
			MistyRose = 0xFFFFE4E1,
			Moccasin = 0xFFFFE4B5,
			NavajoWhite = 0xFFFFDEAD,
			Navy = 0xFF000080,
			OldLace = 0xFFFDF5E6,
			Olive = 0xFF808000,
			OliveDrab = 0xFF6B8E23,
			Orange = 0xFFFFA500,
			OrangeRed = 0xFFFF4500,
			Orchid = 0xFFDA70D6,
			PaleGoldenrod = 0xFFEEE8AA,
			PaleGreen = 0xFF98FB98,
			PaleTurquoise = 0xFFAFEEEE,
			PaleVioletRed = 0xFFDB7093,
			PapayaWhip = 0xFFFFEFD5,
			PeachPuff = 0xFFFFDAB9,
			Peru = 0xFFCD853F,
			Pink = 0xFFFFC0CB,
			Plum = 0xFFDDA0DD,
			PowderBlue = 0xFFB0E0E6,
			Purple = 0xFF800080,
			Red = 0xFFFF0000,
			RosyBrown = 0xFFBC8F8F,
			RoyalBlue = 0xFF4169E1,
			SaddleBrown = 0xFF8B4513,
			Salmon = 0xFFFA8072,
			SandyBrown = 0xFFF4A460,
			SeaGreen = 0xFF2E8B57,
			SeaShell = 0xFFFFF5EE,
			Sienna = 0xFFA0522D,
			Silver = 0xFFC0C0C0,
			SkyBlue = 0xFF87CEEB,
			SlateBlue = 0xFF6A5ACD,
			SlateGray = 0xFF708090,
			Snow = 0xFFFFFAFA,
			SpringGreen = 0xFF00FF7F,
			SteelBlue = 0xFF4682B4,
			Tan = 0xFFD2B48C,
			Teal = 0xFF008080,
			Thistle = 0xFFD8BFD8,
			Tomato = 0xFFFF6347,
			Transparent = 0x00FFFFFF,
			Turquoise = 0xFF40E0D0,
			Violet = 0xFFEE82EE,
			Wheat = 0xFFF5DEB3,
			White = 0xFFFFFFFF,
			WhiteSmoke = 0xFFF5F5F5,
			Yellow = 0xFFFFFF00,
			YellowGreen = 0xFF9ACD32,
			UnknownColor = 0x00000001
		};
        
        struct ConstColor
        {
            string name;
            uint value;
        };
        const static int ConstColorCount = 142;
        const static ConstColor ConstColors[ConstColorCount];

	public:
		/// <summary>
		/// Well-known color: AliceBlue
		/// </summary>
		static Color aliceBlue()
		{
			return Color(KnownColor::AliceBlue);
		}

		/// <summary>
		/// Well-known color: AntiqueWhite
		/// </summary>
		static Color antiqueWhite()
		{
			return Color(KnownColor::AntiqueWhite);
		}

		/// <summary>
		/// Well-known color: Aqua
		/// </summary>
		static Color aqua()
		{
			return Color(KnownColor::Aqua);
		}

		/// <summary>
		/// Well-known color: Aquamarine
		/// </summary>
		static Color aquamarine()
		{


			return Color(KnownColor::Aquamarine);

		}

		/// <summary>
		/// Well-known color: Azure
		/// </summary>
		static Color azure()
		{


			return Color(KnownColor::Azure);

		}

		/// <summary>
		/// Well-known color: Beige
		/// </summary>
		static Color beige()
		{


			return Color(KnownColor::Beige);

		}

		/// <summary>
		/// Well-known color: Bisque
		/// </summary>
		static Color bisque()
		{


			return Color(KnownColor::Bisque);

		}

		/// <summary>
		/// Well-known color: Black
		/// </summary>
		static Color black()
		{


			return Color(KnownColor::Black);

		}

		/// <summary>
		/// Well-known color: BlanchedAlmond
		/// </summary>
		static Color blanchedAlmond()
		{


			return Color(KnownColor::BlanchedAlmond);

		}

		/// <summary>
		/// Well-known color: Blue
		/// </summary>
		static Color blue()
		{


			return Color(KnownColor::Blue);

		}

		/// <summary>
		/// Well-known color: BlueViolet
		/// </summary>
		static Color blueViolet()
		{


			return Color(KnownColor::BlueViolet);

		}

		/// <summary>
		/// Well-known color: Brown
		/// </summary>
		static Color brown()
		{


			return Color(KnownColor::Brown);

		}

		/// <summary>
		/// Well-known color: BurlyWood
		/// </summary>
		static Color burlyWood()
		{


			return Color(KnownColor::BurlyWood);

		}

		/// <summary>
		/// Well-known color: CadetBlue
		/// </summary>
		static Color cadetBlue()
		{


			return Color(KnownColor::CadetBlue);

		}

		/// <summary>
		/// Well-known color: Chartreuse
		/// </summary>
		static Color chartreuse()
		{


			return Color(KnownColor::Chartreuse);

		}

		/// <summary>
		/// Well-known color: Chocolate
		/// </summary>
		static Color chocolate()
		{


			return Color(KnownColor::Chocolate);

		}

		/// <summary>
		/// Well-known color: Coral
		/// </summary>
		static Color coral()
		{


			return Color(KnownColor::Coral);

		}

		/// <summary>
		/// Well-known color: CornflowerBlue
		/// </summary>
		static Color cornflowerBlue()
		{


			return Color(KnownColor::CornflowerBlue);

		}

		/// <summary>
		/// Well-known color: Cornsilk
		/// </summary>
		static Color cornsilk()
		{


			return Color(KnownColor::Cornsilk);

		}

		/// <summary>
		/// Well-known color: Crimson
		/// </summary>
		static Color crimson()
		{


			return Color(KnownColor::Crimson);

		}

		/// <summary>
		/// Well-known color: Cyan
		/// </summary>
		static Color cyan()
		{


			return Color(KnownColor::Cyan);

		}

		/// <summary>
		/// Well-known color: DarkBlue
		/// </summary>
		static Color darkBlue()
		{


			return Color(KnownColor::DarkBlue);

		}

		/// <summary>
		/// Well-known color: DarkCyan
		/// </summary>
		static Color darkCyan()
		{


			return Color(KnownColor::DarkCyan);

		}

		/// <summary>
		/// Well-known color: DarkGoldenrod
		/// </summary>
		static Color darkGoldenrod()
		{


			return Color(KnownColor::DarkGoldenrod);

		}

		/// <summary>
		/// Well-known color: DarkGray
		/// </summary>
		static Color darkGray()
		{


			return Color(KnownColor::DarkGray);

		}

		/// <summary>
		/// Well-known color: DarkGreen
		/// </summary>
		static Color darkGreen()
		{


			return Color(KnownColor::DarkGreen);

		}

		/// <summary>
		/// Well-known color: DarkKhaki
		/// </summary>
		static Color darkKhaki()
		{


			return Color(KnownColor::DarkKhaki);

		}

		/// <summary>
		/// Well-known color: DarkMagenta
		/// </summary>
		static Color darkMagenta()
		{


			return Color(KnownColor::DarkMagenta);

		}

		/// <summary>
		/// Well-known color: DarkOliveGreen
		/// </summary>
		static Color darkOliveGreen()
		{


			return Color(KnownColor::DarkOliveGreen);

		}

		/// <summary>
		/// Well-known color: DarkOrange
		/// </summary>
		static Color darkOrange()
		{


			return Color(KnownColor::DarkOrange);

		}

		/// <summary>
		/// Well-known color: DarkOrchid
		/// </summary>
		static Color darkOrchid()
		{


			return Color(KnownColor::DarkOrchid);

		}

		/// <summary>
		/// Well-known color: DarkRed
		/// </summary>
		static Color darkRed()
		{


			return Color(KnownColor::DarkRed);

		}

		/// <summary>
		/// Well-known color: DarkSalmon
		/// </summary>
		static Color darkSalmon()
		{


			return Color(KnownColor::DarkSalmon);

		}

		/// <summary>
		/// Well-known color: DarkSeaGreen
		/// </summary>
		static Color darkSeaGreen()
		{


			return Color(KnownColor::DarkSeaGreen);

		}

		/// <summary>
		/// Well-known color: DarkSlateBlue
		/// </summary>
		static Color darkSlateBlue()
		{


			return Color(KnownColor::DarkSlateBlue);

		}

		/// <summary>
		/// Well-known color: DarkSlateGray
		/// </summary>
		static Color darkSlateGray()
		{


			return Color(KnownColor::DarkSlateGray);

		}

		/// <summary>
		/// Well-known color: DarkTurquoise
		/// </summary>
		static Color darkTurquoise()
		{


			return Color(KnownColor::DarkTurquoise);

		}

		/// <summary>
		/// Well-known color: DarkViolet
		/// </summary>
		static Color darkViolet()
		{


			return Color(KnownColor::DarkViolet);

		}

		/// <summary>
		/// Well-known color: DeepPink
		/// </summary>
		static Color deepPink()
		{


			return Color(KnownColor::DeepPink);

		}

		/// <summary>
		/// Well-known color: DeepSkyBlue
		/// </summary>
		static Color deepSkyBlue()
		{


			return Color(KnownColor::DeepSkyBlue);

		}

		/// <summary>
		/// Well-known color: DimGray
		/// </summary>
		static Color dimGray()
		{


			return Color(KnownColor::DimGray);

		}

		/// <summary>
		/// Well-known color: DodgerBlue
		/// </summary>
		static Color dodgerBlue()
		{


			return Color(KnownColor::DodgerBlue);

		}

		/// <summary>
		/// Well-known color: Firebrick
		/// </summary>
		static Color firebrick()
		{


			return Color(KnownColor::Firebrick);

		}

		/// <summary>
		/// Well-known color: FloralWhite
		/// </summary>
		static Color floralWhite()
		{


			return Color(KnownColor::FloralWhite);

		}

		/// <summary>
		/// Well-known color: ForestGreen
		/// </summary>
		static Color forestGreen()
		{


			return Color(KnownColor::ForestGreen);

		}

		/// <summary>
		/// Well-known color: Fuchsia
		/// </summary>
		static Color fuchsia()
		{


			return Color(KnownColor::Fuchsia);

		}

		/// <summary>
		/// Well-known color: Gainsboro
		/// </summary>
		static Color gainsboro()
		{


			return Color(KnownColor::Gainsboro);

		}

		/// <summary>
		/// Well-known color: GhostWhite
		/// </summary>
		static Color ghostWhite()
		{


			return Color(KnownColor::GhostWhite);

		}

		/// <summary>
		/// Well-known color: Gold
		/// </summary>
		static Color gold()
		{


			return Color(KnownColor::Gold);

		}

		/// <summary>
		/// Well-known color: Goldenrod
		/// </summary>
		static Color goldenrod()
		{


			return Color(KnownColor::Goldenrod);

		}

		/// <summary>
		/// Well-known color: Gray
		/// </summary>
		static Color gray()
		{


			return Color(KnownColor::Gray);

		}

		/// <summary>
		/// Well-known color: Green
		/// </summary>
		static Color green()
		{


			return Color(KnownColor::Green);

		}

		/// <summary>
		/// Well-known color: GreenYellow
		/// </summary>
		static Color greenYellow()
		{


			return Color(KnownColor::GreenYellow);

		}

		/// <summary>
		/// Well-known color: Honeydew
		/// </summary>
		static Color honeydew()
		{


			return Color(KnownColor::Honeydew);

		}

		/// <summary>
		/// Well-known color: HotPink
		/// </summary>
		static Color hotPink()
		{


			return Color(KnownColor::HotPink);

		}

		/// <summary>
		/// Well-known color: IndianRed
		/// </summary>
		static Color indianRed()
		{


			return Color(KnownColor::IndianRed);

		}

		/// <summary>
		/// Well-known color: Indigo
		/// </summary>
		static Color indigo()
		{


			return Color(KnownColor::Indigo);

		}

		/// <summary>
		/// Well-known color: Ivory
		/// </summary>
		static Color ivory()
		{


			return Color(KnownColor::Ivory);

		}

		/// <summary>
		/// Well-known color: Khaki
		/// </summary>
		static Color khaki()
		{


			return Color(KnownColor::Khaki);

		}

		/// <summary>
		/// Well-known color: Lavender
		/// </summary>
		static Color lavender()
		{


			return Color(KnownColor::Lavender);

		}

		/// <summary>
		/// Well-known color: LavenderBlush
		/// </summary>
		static Color lavenderBlush()
		{


			return Color(KnownColor::LavenderBlush);

		}

		/// <summary>
		/// Well-known color: LawnGreen
		/// </summary>
		static Color lawnGreen()
		{


			return Color(KnownColor::LawnGreen);

		}

		/// <summary>
		/// Well-known color: LemonChiffon
		/// </summary>
		static Color lemonChiffon()
		{


			return Color(KnownColor::LemonChiffon);

		}

		/// <summary>
		/// Well-known color: LightBlue
		/// </summary>
		static Color lightBlue()
		{


			return Color(KnownColor::LightBlue);

		}

		/// <summary>
		/// Well-known color: LightCoral
		/// </summary>
		static Color lightCoral()
		{


			return Color(KnownColor::LightCoral);

		}

		/// <summary>
		/// Well-known color: LightCyan
		/// </summary>
		static Color lightCyan()
		{


			return Color(KnownColor::LightCyan);

		}

		/// <summary>
		/// Well-known color: LightGoldenrodYellow
		/// </summary>
		static Color lightGoldenrodYellow()
		{


			return Color(KnownColor::LightGoldenrodYellow);

		}

		/// <summary>
		/// Well-known color: LightGray
		/// </summary>
		static Color lightGray()
		{


			return Color(KnownColor::LightGray);

		}

		/// <summary>
		/// Well-known color: LightGreen
		/// </summary>
		static Color lightGreen()
		{


			return Color(KnownColor::LightGreen);

		}

		/// <summary>
		/// Well-known color: LightPink
		/// </summary>
		static Color lightPink()
		{


			return Color(KnownColor::LightPink);

		}

		/// <summary>
		/// Well-known color: LightSalmon
		/// </summary>
		static Color lightSalmon()
		{


			return Color(KnownColor::LightSalmon);

		}

		/// <summary>
		/// Well-known color: LightSeaGreen
		/// </summary>
		static Color lightSeaGreen()
		{


			return Color(KnownColor::LightSeaGreen);

		}

		/// <summary>
		/// Well-known color: LightSkyBlue
		/// </summary>
		static Color lightSkyBlue()
		{


			return Color(KnownColor::LightSkyBlue);

		}

		/// <summary>
		/// Well-known color: LightSlateGray
		/// </summary>
		static Color lightSlateGray()
		{


			return Color(KnownColor::LightSlateGray);

		}

		/// <summary>
		/// Well-known color: LightSteelBlue
		/// </summary>
		static Color lightSteelBlue()
		{


			return Color(KnownColor::LightSteelBlue);

		}

		/// <summary>
		/// Well-known color: LightYellow
		/// </summary>
		static Color lightYellow()
		{


			return Color(KnownColor::LightYellow);

		}

		/// <summary>
		/// Well-known color: Lime
		/// </summary>
		static Color lime()
		{


			return Color(KnownColor::Lime);

		}

		/// <summary>
		/// Well-known color: LimeGreen
		/// </summary>
		static Color limeGreen()
		{


			return Color(KnownColor::LimeGreen);

		}

		/// <summary>
		/// Well-known color: Linen
		/// </summary>
		static Color linen()
		{


			return Color(KnownColor::Linen);

		}

		/// <summary>
		/// Well-known color: Magenta
		/// </summary>
		static Color magenta()
		{


			return Color(KnownColor::Magenta);

		}

		/// <summary>
		/// Well-known color: Maroon
		/// </summary>
		static Color maroon()
		{


			return Color(KnownColor::Maroon);

		}

		/// <summary>
		/// Well-known color: MediumAquamarine
		/// </summary>
		static Color mediumAquamarine()
		{


			return Color(KnownColor::MediumAquamarine);

		}

		/// <summary>
		/// Well-known color: MediumBlue
		/// </summary>
		static Color mediumBlue()
		{


			return Color(KnownColor::MediumBlue);

		}

		/// <summary>
		/// Well-known color: MediumOrchid
		/// </summary>
		static Color mediumOrchid()
		{


			return Color(KnownColor::MediumOrchid);

		}

		/// <summary>
		/// Well-known color: MediumPurple
		/// </summary>
		static Color mediumPurple()
		{


			return Color(KnownColor::MediumPurple);

		}

		/// <summary>
		/// Well-known color: MediumSeaGreen
		/// </summary>
		static Color mediumSeaGreen()
		{


			return Color(KnownColor::MediumSeaGreen);

		}

		/// <summary>
		/// Well-known color: MediumSlateBlue
		/// </summary>
		static Color mediumSlateBlue()
		{


			return Color(KnownColor::MediumSlateBlue);

		}

		/// <summary>
		/// Well-known color: MediumSpringGreen
		/// </summary>
		static Color mediumSpringGreen()
		{


			return Color(KnownColor::MediumSpringGreen);

		}

		/// <summary>
		/// Well-known color: MediumTurquoise
		/// </summary>
		static Color mediumTurquoise()
		{


			return Color(KnownColor::MediumTurquoise);

		}

		/// <summary>
		/// Well-known color: MediumVioletRed
		/// </summary>
		static Color mediumVioletRed()
		{


			return Color(KnownColor::MediumVioletRed);

		}

		/// <summary>
		/// Well-known color: MidnightBlue
		/// </summary>
		static Color midnightBlue()
		{


			return Color(KnownColor::MidnightBlue);

		}

		/// <summary>
		/// Well-known color: MintCream
		/// </summary>
		static Color mintCream()
		{


			return Color(KnownColor::MintCream);

		}

		/// <summary>
		/// Well-known color: MistyRose
		/// </summary>
		static Color mistyRose()
		{


			return Color(KnownColor::MistyRose);

		}

		/// <summary>
		/// Well-known color: Moccasin
		/// </summary>
		static Color moccasin()
		{


			return Color(KnownColor::Moccasin);

		}

		/// <summary>
		/// Well-known color: NavajoWhite
		/// </summary>
		static Color navajoWhite()
		{


			return Color(KnownColor::NavajoWhite);

		}

		/// <summary>
		/// Well-known color: Navy
		/// </summary>
		static Color navy()
		{


			return Color(KnownColor::Navy);

		}

		/// <summary>
		/// Well-known color: OldLace
		/// </summary>
		static Color oldLace()
		{


			return Color(KnownColor::OldLace);

		}

		/// <summary>
		/// Well-known color: Olive
		/// </summary>
		static Color olive()
		{


			return Color(KnownColor::Olive);

		}

		/// <summary>
		/// Well-known color: OliveDrab
		/// </summary>
		static Color oliveDrab()
		{


			return Color(KnownColor::OliveDrab);

		}

		/// <summary>
		/// Well-known color: Orange
		/// </summary>
		static Color orange()
		{


			return Color(KnownColor::Orange);

		}

		/// <summary>
		/// Well-known color: OrangeRed
		/// </summary>
		static Color orangeRed()
		{


			return Color(KnownColor::OrangeRed);

		}

		/// <summary>
		/// Well-known color: Orchid
		/// </summary>
		static Color orchid()
		{


			return Color(KnownColor::Orchid);

		}

		/// <summary>
		/// Well-known color: PaleGoldenrod
		/// </summary>
		static Color paleGoldenrod()
		{


			return Color(KnownColor::PaleGoldenrod);

		}

		/// <summary>
		/// Well-known color: PaleGreen
		/// </summary>
		static Color paleGreen()
		{


			return Color(KnownColor::PaleGreen);

		}

		/// <summary>
		/// Well-known color: PaleTurquoise
		/// </summary>
		static Color paleTurquoise()
		{


			return Color(KnownColor::PaleTurquoise);

		}

		/// <summary>
		/// Well-known color: PaleVioletRed
		/// </summary>
		static Color paleVioletRed()
		{


			return Color(KnownColor::PaleVioletRed);

		}

		/// <summary>
		/// Well-known color: PapayaWhip
		/// </summary>
		static Color papayaWhip()
		{


			return Color(KnownColor::PapayaWhip);

		}

		/// <summary>
		/// Well-known color: PeachPuff
		/// </summary>
		static Color peachPuff()
		{


			return Color(KnownColor::PeachPuff);

		}

		/// <summary>
		/// Well-known color: Peru
		/// </summary>
		static Color peru()
		{


			return Color(KnownColor::Peru);

		}

		/// <summary>
		/// Well-known color: Pink
		/// </summary>
		static Color pink()
		{


			return Color(KnownColor::Pink);

		}

		/// <summary>
		/// Well-known color: Plum
		/// </summary>
		static Color plum()
		{


			return Color(KnownColor::Plum);

		}

		/// <summary>
		/// Well-known color: PowderBlue
		/// </summary>
		static Color powderBlue()
		{


			return Color(KnownColor::PowderBlue);

		}

		/// <summary>
		/// Well-known color: Purple
		/// </summary>
		static Color purple()
		{


			return Color(KnownColor::Purple);

		}

		/// <summary>
		/// Well-known color: Red
		/// </summary>
		static Color red()
		{


			return Color(KnownColor::Red);

		}

		/// <summary>
		/// Well-known color: RosyBrown
		/// </summary>
		static Color rosyBrown()
		{


			return Color(KnownColor::RosyBrown);

		}

		/// <summary>
		/// Well-known color: RoyalBlue
		/// </summary>
		static Color royalBlue()
		{


			return Color(KnownColor::RoyalBlue);

		}

		/// <summary>
		/// Well-known color: SaddleBrown
		/// </summary>
		static Color saddleBrown()
		{


			return Color(KnownColor::SaddleBrown);

		}

		/// <summary>
		/// Well-known color: Salmon
		/// </summary>
		static Color salmon()
		{


			return Color(KnownColor::Salmon);

		}

		/// <summary>
		/// Well-known color: SandyBrown
		/// </summary>
		static Color sandyBrown()
		{


			return Color(KnownColor::SandyBrown);

		}

		/// <summary>
		/// Well-known color: SeaGreen
		/// </summary>
		static Color seaGreen()
		{


			return Color(KnownColor::SeaGreen);

		}

		/// <summary>
		/// Well-known color: SeaShell
		/// </summary>
		static Color seaShell()
		{


			return Color(KnownColor::SeaShell);

		}

		/// <summary>
		/// Well-known color: Sienna
		/// </summary>
		static Color sienna()
		{


			return Color(KnownColor::Sienna);

		}

		/// <summary>
		/// Well-known color: Silver
		/// </summary>
		static Color silver()
		{


			return Color(KnownColor::Silver);

		}

		/// <summary>
		/// Well-known color: SkyBlue
		/// </summary>
		static Color skyBlue()
		{


			return Color(KnownColor::SkyBlue);

		}

		/// <summary>
		/// Well-known color: SlateBlue
		/// </summary>
		static Color slateBlue()
		{


			return Color(KnownColor::SlateBlue);

		}

		/// <summary>
		/// Well-known color: SlateGray
		/// </summary>
		static Color slateGray()
		{


			return Color(KnownColor::SlateGray);

		}

		/// <summary>
		/// Well-known color: Snow
		/// </summary>
		static Color snow()
		{


			return Color(KnownColor::Snow);

		}

		/// <summary>
		/// Well-known color: SpringGreen
		/// </summary>
		static Color springGreen()
		{


			return Color(KnownColor::SpringGreen);

		}

		/// <summary>
		/// Well-known color: SteelBlue
		/// </summary>
		static Color steelBlue()
		{


			return Color(KnownColor::SteelBlue);

		}

		/// <summary>
		/// Well-known color: Tan
		/// </summary>
		static Color tan()
		{


			return Color(KnownColor::Tan);

		}

		/// <summary>
		/// Well-known color: Teal
		/// </summary>
		static Color teal()
		{


			return Color(KnownColor::Teal);

		}

		/// <summary>
		/// Well-known color: Thistle
		/// </summary>
		static Color thistle()
		{


			return Color(KnownColor::Thistle);

		}

		/// <summary>
		/// Well-known color: Tomato
		/// </summary>
		static Color tomato()
		{


			return Color(KnownColor::Tomato);

		}

		/// <summary>
		/// Well-known color: Transparent
		/// </summary>
		static Color transparent()
		{


			return Color(KnownColor::Transparent);

		}

		/// <summary>
		/// Well-known color: Turquoise
		/// </summary>
		static Color turquoise()
		{


			return Color(KnownColor::Turquoise);

		}

		/// <summary>
		/// Well-known color: Violet
		/// </summary>
		static Color violet()
		{


			return Color(KnownColor::Violet);

		}

		/// <summary>
		/// Well-known color: Wheat
		/// </summary>
		static Color wheat()
		{


			return Color(KnownColor::Wheat);

		}

		/// <summary>
		/// Well-known color: White
		/// </summary>
		static Color white()
		{


			return Color(KnownColor::White);

		}

		/// <summary>
		/// Well-known color: WhiteSmoke
		/// </summary>
		static Color whiteSmoke()
		{


			return Color(KnownColor::WhiteSmoke);

		}

		/// <summary>
		/// Well-known color: Yellow
		/// </summary>
		static Color yellow()
		{
			return Color(KnownColor::Yellow);
		}

		/// <summary>
		/// Well-known color: YellowGreen
		/// </summary>
		static Color yellowGreen()
		{
			return Color(KnownColor::YellowGreen);
		}
        
        static bool toSystemColorStr(const Color& color, string& str);
        static bool parse(const string& str, Color& color);
	};
}

#endif	// SYSTEMCOLOR_H
