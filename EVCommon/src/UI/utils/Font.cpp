﻿#include "Font.h"
#include "common/system/Convert.h"
#include "common/diag/Debug.h"
#include "skia/ports/SkFontMgr.h"

using namespace Common;

namespace Drawing
{
    Font::Font()
    {
        name = "";
        style = SkTypeface::Style::kNormal;
        encoding = SkPaint::TextEncoding::kUTF8_TextEncoding;
        align = ContentAlignment::MiddleCenter;
        size = 0.0f;
        scaleX = 1.0f;
        skewX = 0.0f;
        autoScale = false;

#if DEBUG && __ANDROID__
        static bool oneshot = false;
        if(!oneshot)
        {
        	oneshot = true;
            SkTypeface* tf = SkTypeface::RefDefault(SkTypeface::kNormal);
            assert(tf);
            SkString fname;
            tf->getFamilyName(&fname);
            Debug::writeFormatLine("default family name: %s", fname.c_str());

            SkFontMgr* fm = SkFontMgr::RefDefault();
            int count = fm->countFamilies();
            Debug::writeFormatLine("countFamilies: %d", count);
            for (int i=0; i<count; i++)
            {
                SkString familyName;
                fm->getFamilyName(i, &familyName);
                Debug::writeFormatLine("familyName: %s", familyName.c_str());
            }
        }
#endif
    }

    bool Font::isEmpty() const
    {
        return this->size == 0.0f;
    }
	string Font::toString() const
	{
		return Convert::convertStr("Font name:%s;size:%f", name.c_str(), size);
	}
    
    void Font::operator=(const Font& value)
    {
        this->name = value.name;
        this->style = value.style;
        this->encoding = value.encoding;
        this->align = value.align;
        this->size = value.size;
        this->scaleX = value.scaleX;
        this->skewX = value.skewX;
        this->autoScale = value.autoScale;
    }
    bool Font::operator==(const Font& value) const
    {
        return this->name == value.name &&
            this->style == value.style &&
            this->encoding == value.encoding &&
            this->align == value.align &&
            this->size == value.size &&
            this->scaleX == value.scaleX &&
            this->skewX == value.skewX &&
            this->autoScale == value.autoScale;
    }
    bool Font::operator!=(const Font& value) const
    {
        return !operator==(value);
    }
    
    bool Font::parse(const string& str, Font& font)
    {
        // name:Roma;size:20;scaleX:0;skewX:0;encoding:utf8
        StringArray texts;
        Convert::splitItems(str, texts);
        if(texts.count() > 0)
        {
            Font temp;
            for (uint i=0; i<texts.count(); i++)
            {
                StringArray values;
                Convert::splitItems(texts[i], values, ':');
                if(values.count() == 2)
                {
                    string name = Convert::trimStr(values[0], ' ', '{', '}');
                    string text = Convert::trimStartStr(values[1], ' ', '{', '}');
                    text = Convert::trimEndStr(text, ' ', '{', '}');

                    if(String::stringEquals(name, "name", true))
                    {
                        temp.name = text;
                    }
                    else if(String::stringEquals(name, "style", true))
                    {
                        temp.style = parseStyle(text);
                    }
                    else if(String::stringEquals(name, "encoding", true))
                    {
                        temp.encoding = parseTextEncoding(text);
                    }
                    else if(String::stringEquals(name, "align", true))
                    {
                        temp.align = parseAlign(text);
                    }
                    else if(String::stringEquals(name, "size", true))
                    {
                        if(!Convert::parseSingle(text, temp.size) || !(temp.size > 0))
                            return false;
                    }
                    else if(String::stringEquals(name, "scaleX", true))
                    {
                        if(!Convert::parseSingle(text, temp.scaleX))
                            return false;
                    }
                    else if(String::stringEquals(name, "skewX", true))
                    {
                        if(!Convert::parseSingle(text, temp.skewX))
                            return false;
                    }
                    else if(String::stringEquals(name, "autoscale", true))
                    {
                        if(!Convert::parseBoolean(text, temp.autoScale))
                            return false;
                    }
                }
            }
            
            font = temp;
            return true;
        }
        return false;
    }

    bool Font::toSkPaint(SkPaint& paint)
    {
        setFontName(name, style, paint);
        paint.setTextEncoding(encoding);
        paint.setTextSize(size);
        paint.setTextScaleX(scaleX);
        paint.setTextSkewX(skewX);
        return true;
    }
    void Font::setFontName(const string& name, SkTypeface::Style style, SkPaint& paint)
    {
        string fname = name;
#ifdef __APPLE__
# include <TargetConditionals.h>
#ifdef TARGET_OS_IPHONE
        fname = "Heiti SC";     // default font
#endif
#endif
#ifdef __ANDROID__
        fname = "miui";         // default font
#endif
        if(!fname.empty())
        {
            SkTypeface* pFace = SkTypeface::CreateFromName(fname.c_str(), style);
            paint.setTypeface(pFace);
        }
    }

    SkTypeface::Style Font::parseStyle(const string& value)
    {
        if(String::stringEquals(value, "Normal", true))
        {
            return SkTypeface::Style::kNormal;
        }
        else if(String::stringEquals(value, "Bold", true))
        {
            return SkTypeface::Style::kBold;
        }
        else if(String::stringEquals(value, "Italic", true))
        {
            return SkTypeface::Style::kItalic;
        }
        else if(String::stringEquals(value, "BoldItalic", true))
        {
            return SkTypeface::Style::kBoldItalic;
        }
        return SkTypeface::Style::kNormal;
    }
    string Font::convertStyleStr(SkTypeface::Style value)
    {
        switch (value)
        {
            case SkTypeface::Style::kBold:
                return "Bold";
            case SkTypeface::Style::kItalic:
                return "Italic";
            case SkTypeface::Style::kBoldItalic:
                return "BoldItalic";
            case SkTypeface::Style::kNormal:
            default:
                return "Normal";
        }
    }
    
    SkPaint::TextEncoding Font::parseTextEncoding(const string& value)
    {
        if(String::stringEquals(value, "UTF8", true))
        {
            return SkPaint::TextEncoding::kUTF8_TextEncoding;
        }
        else if(String::stringEquals(value, "UTF16", true))
        {
            return SkPaint::TextEncoding::kUTF16_TextEncoding;
        }
        else if(String::stringEquals(value, "UTF32", true))
        {
            return SkPaint::TextEncoding::kUTF32_TextEncoding;
        }
        else if(String::stringEquals(value, "glyph", true))
        {
            return SkPaint::TextEncoding::kGlyphID_TextEncoding;
        }
        return SkPaint::TextEncoding::kUTF8_TextEncoding;
    }
    string Font::convertTextEncodingStr(SkPaint::TextEncoding value)
    {
        switch (value)
        {
            case SkPaint::TextEncoding::kGlyphID_TextEncoding:
                return "glyph";
            case SkPaint::TextEncoding::kUTF32_TextEncoding:
                return "UTF32";
            case SkPaint::TextEncoding::kUTF16_TextEncoding:
                return "UTF16";
            case SkPaint::TextEncoding::kUTF8_TextEncoding:
            default:
                return "UTF8";
        }
    }
    
    Font::ContentAlignment Font::parseAlign(const string& value)
    {
        if(String::stringEquals(value, "topLeft", true))
        {
            return ContentAlignment::TopLeft;
        }
        else if(String::stringEquals(value, "topCenter", true))
        {
            return ContentAlignment::TopCenter;
        }
        else if(String::stringEquals(value, "topRight", true))
        {
            return ContentAlignment::TopRight;
        }
        else if(String::stringEquals(value, "middleLeft", true))
        {
            return ContentAlignment::MiddleLeft;
        }
        else if(String::stringEquals(value, "middleCenter", true))
        {
            return ContentAlignment::MiddleCenter;
        }
        else if(String::stringEquals(value, "middleRight", true))
        {
            return ContentAlignment::MiddleRight;
        }
        else if(String::stringEquals(value, "bottomLeft", true))
        {
            return ContentAlignment::BottomLeft;
        }
        else if(String::stringEquals(value, "bottomCenter", true))
        {
            return ContentAlignment::BottomCenter;
        }
        else if(String::stringEquals(value, "bottomRight", true))
        {
            return ContentAlignment::BottomRight;
        }
        return ContentAlignment::MiddleCenter;
    }
    string Font::convertAlignStr(ContentAlignment value)
    {
        switch (value)
        {
            case ContentAlignment::TopLeft:
                return "topLeft";
            case ContentAlignment::TopCenter:
                return "topCenter";
            case ContentAlignment::TopRight:
                return "topRight";
            case ContentAlignment::MiddleLeft:
                return "middleLeft";
            case ContentAlignment::MiddleCenter:
                return "middleCenter";
            case ContentAlignment::MiddleRight:
                return "middleRight";
            case ContentAlignment::BottomLeft:
                return "bottomLeft";
            case ContentAlignment::BottomCenter:
                return "bottomCenter";
            case ContentAlignment::BottomRight:
                return "bottomRight";
            default:
                return "middleCenter";
        }
    }
}
