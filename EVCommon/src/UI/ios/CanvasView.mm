//
//  Canvas.mm
//  common
//
//  Created by baowei on 15/3/21.
//  Copyright (c) 2015年 com. All rights reserved.
//

#import "CanvasView.h"
#include "CanvasWindow.h"

@implementation CanvasView

bool _winCreated = false;
void invalidateInner(SkWindow* win)
{
    // Call the Objective-C method using Objective-C syntax
    if ([NSThread isMainThread])
    {
        if(_winCreated)
        {
            win->inval(NULL);
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            if(_winCreated)
            {
                win->inval(NULL);
            }
        });
    }
}
void invalidateRectInner(SkWindow* win, const Rectangle& rect)
{
    // Call the Objective-C method using Objective-C syntax
    SkRect bounds = SkRectConvert::convert(rect);
    if ([NSThread isMainThread])
    {
        if(_winCreated)
        {
            win->inval((SkRect*)&bounds);
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            if(_winCreated)
            {
                win->inval((SkRect*)&bounds);
            }
        });
    }
}

- (id)initWithDefaults {
    if ((self = [super initWithDefaults])) {
        fWind = create_sk_window((__bridge void*)self, NULL, NULL);
        _winCreated = true;
    }
    return self;
}

- (void)dealloc{
    _winCreated = false;
}

- (CanvasWindow* )canvasWindow {
    return dynamic_cast<CanvasWindow*>(fWind);
}

- (void) deleteCanvasWindow {
    if (fWind != NULL)
    {
        delete fWind;
        fWind = NULL;
    }
}

@end
