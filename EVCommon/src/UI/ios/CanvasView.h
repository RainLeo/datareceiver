//
//  CanvasView.h
//  EVCommon
//
//  Created by baowei on 15/3/21.
//  Copyright (c) 2015 com. All rights reserved.
//

#import "SkUIView.h"
#include "../utils/CanvasWindow.h"

using namespace EVCommon;

@interface CanvasView : SkUIView
{
}

- (CanvasWindow*) canvasWindow;
- (void) deleteCanvasWindow;

@end
