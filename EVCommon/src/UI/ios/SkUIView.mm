#import "SkUIView.h"
#include "SkCanvas.h"
#include "SkCGUtils.h"
#include "SkSurface.h"

#define USE_GL_2

@implementation SkUIView

@synthesize fWind, fTitleItem;

- (id)initWithDefaults {
#ifdef USE_GL_1
    fGL.fContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES1];
#else
    fGL.fContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
#endif
    
    if (!fGL.fContext || ![EAGLContext setCurrentContext:fGL.fContext])
    {
        return nil;
    }
    
    // Create default framebuffer object. The backing will be allocated for the current layer in -resizeFromLayer
    glGenFramebuffers(1, &fGL.fFramebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, fGL.fFramebuffer);
    
    glGenRenderbuffers(1, &fGL.fRenderbuffer);
    glGenRenderbuffers(1, &fGL.fStencilbuffer);
    
    glBindRenderbuffer(GL_RENDERBUFFER, fGL.fRenderbuffer);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, fGL.fRenderbuffer);
    
    glBindRenderbuffer(GL_RENDERBUFFER, fGL.fStencilbuffer);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, fGL.fStencilbuffer);
    
    isUsingGL = NO;
    
    fWind = NULL;
    return self;
}

- (id)initWithCoder:(NSCoder*)coder {
    if ((self = [super initWithCoder:coder])) {
        self = [self initWithDefaults];
        [self setUpWindow];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self = [self initWithDefaults];
        [self setUpWindow];
    }
    return self;
}

- (void)setUpWindow {
    if (NULL != fWind) {
        fWind->setVisibleP(true);
        fWind->resize(self.frame.size.width, self.frame.size.height,
                      kN32_SkColorType);
    }
}

- (void)dealloc {
    delete fWind;
//    [fTitleItem release];
//    [super dealloc];
}

- (void)forceRedraw {
    if(isUsingGL){
        [self drawInGL];
    }
    else{
        [self drawInRaster];
    }
}

- (void)drawInRaster {
    SkCanvas canvas(fWind->getBitmap());
    fWind->draw(&canvas);
    
    CGImageRef cgimage = SkCreateCGImageRef(fWind->getBitmap());
    self.layer.contents = (__bridge id)cgimage;
    CGImageRelease(cgimage);
}

- (void)drawInGL {
    // This application only creates a single context which is already set current at this point.
    // This call is redundant, but needed if dealing with multiple contexts.
    [EAGLContext setCurrentContext:fGL.fContext];
    
    // This application only creates a single default framebuffer which is already bound at this point.
    // This call is redundant, but needed if dealing with multiple framebuffers.
    glBindFramebuffer(GL_FRAMEBUFFER, fGL.fFramebuffer);
    
    GLint scissorEnable;
    glGetIntegerv(GL_SCISSOR_TEST, &scissorEnable);
    glDisable(GL_SCISSOR_TEST);
    glClearColor(0,0,0,0);
    glClear(GL_COLOR_BUFFER_BIT);
    if (scissorEnable) {
        glEnable(GL_SCISSOR_TEST);
    }
    glViewport(0, 0, fGL.fWidth, fGL.fHeight);
    
    
    SkAutoTUnref<SkSurface> surface(fWind->createSurface());
    SkCanvas* canvas = surface->getCanvas();
    
    // if we're not "retained", then we have to always redraw everything.
    // This call forces us to ignore the fDirtyRgn, and draw everywhere.
    // If we are "retained", we can skip this call (as the raster case does)
    fWind->forceInvalAll();
    
    fWind->draw(canvas);
    
    // This application only creates a single color renderbuffer which is already bound at this point.
    // This call is redundant, but needed if dealing with multiple renderbuffers.
    glBindRenderbuffer(GL_RENDERBUFFER, fGL.fRenderbuffer);
    [fGL.fContext presentRenderbuffer:GL_RENDERBUFFER];
}

- (void)layoutSubviews {
    CGRect rect = [self bounds];
    fGL.fWidth = rect.size.width;
    fGL.fHeight = rect.size.height;
    fWind->resize(fGL.fWidth, fGL.fHeight);
    fWind->inval(NULL);
    
//    int W, H;
//    
//    // Allocate color buffer backing based on the current layer size
//    glBindRenderbuffer(GL_RENDERBUFFER, fGL.fRenderbuffer);
////    [fGL.fContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:fGLLayer];
//    
//    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &fGL.fWidth);
//    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &fGL.fHeight);
//    
//    glBindRenderbuffer(GL_RENDERBUFFER, fGL.fStencilbuffer);
//    glRenderbufferStorage(GL_RENDERBUFFER, GL_STENCIL_INDEX8, fGL.fWidth, fGL.fHeight);
//    
//    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
//        NSLog(@"Failed to make complete framebuffer object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
//    }
//    
//    if (isUsingGL) {
//        W = fGL.fWidth;
//        H = fGL.fHeight;
//    }
//    else {
//        CGRect rect = self.bounds;
//        W = (int)CGRectGetWidth(rect);
//        H = (int)CGRectGetHeight(rect);
//    }
//    
//    printf("---- layoutSubviews %d %d\n", W, H);
//    fWind->resize(W, H);
//    fWind->inval(NULL);
}

//Gesture Handlers
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    for (UITouch *touch in touches) {
        CGPoint loc = [touch locationInView:self];
        fWind->handleClick(loc.x, loc.y, SkView::Click::kDown_State, (__bridge void*)touch);
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    for (UITouch *touch in touches) {
        CGPoint loc = [touch locationInView:self];
        fWind->handleClick(loc.x, loc.y, SkView::Click::kMoved_State, (__bridge void*)touch);
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    for (UITouch *touch in touches) {
        CGPoint loc = [touch locationInView:self];
        fWind->handleClick(loc.x, loc.y, SkView::Click::kUp_State, (__bridge void*)touch);
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    for (UITouch *touch in touches) {
        CGPoint loc = [touch locationInView:self];
        fWind->handleClick(loc.x, loc.y, SkView::Click::kUp_State, (__bridge void*)touch);
    }
}

///////////////////////////////////////////////////////////////////////////////

- (void)setSkTitle:(const char *)title {
    if (fTitleItem) {
        fTitleItem.title = [NSString stringWithUTF8String:title];
    }
}

- (BOOL)onHandleEvent:(const SkEvent&)evt {
    return false;
}

- (void)getAttachmentInfo:(SkOSWindow::AttachmentInfo*)info {
    // we don't have a GL context.
    info->fSampleCount = 0;
    info->fStencilBits = 0;
}

#include "SkOSMenu.h"
- (void)onAddMenu:(const SkOSMenu*)menu {
//    [self.fOptionsDelegate view:self didAddMenu:menu];
}
- (void)onUpdateMenu:(SkOSMenu*)menu {
//    [self.fOptionsDelegate view:self didUpdateMenu:menu];
}

- (void)postInvalWithRect:(const SkIRect*)r {
    if(isUsingGL){
        [self performSelector:@selector(drawInGL) withObject:nil afterDelay:0];
    }
    else{
        [self performSelector:@selector(drawInRaster) withObject:nil afterDelay:0];
    }
    [self setNeedsDisplay];
}

@end
