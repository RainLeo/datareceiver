#include "LinearAnimation.h"
#include "common/system/Convert.h"
#include "common/diag/Debug.h"
#include "../components/Canvas.h"
#include "../utils/SkConvert.h"

using namespace Common;
using namespace rtdb;

namespace Drawing
{
    const LinearValue LinearValue::Empty = LinearValue();
    string LinearValue::toString() const
    {
        return Convert::convertStr("%f,%f,%f,%f", minExpressionValue, maxExpressionValue, minValue, maxValue);
    }
    bool LinearValue::k(float& value) const
    {
        if(maxExpressionValue != minExpressionValue)
        {
            value = (maxValue-minValue)/(maxExpressionValue-minExpressionValue);
            return true;
        }
        return false;
    }
    bool LinearValue::parse(const string& str, LinearValue& value)
    {
        StringArray texts;
        Convert::splitStr(str, ',', texts);
        if (texts.count() == 4)
        {
            LinearValue temp;
            if(Convert::parseSingle(Convert::trimStr(texts[0]), temp.minExpressionValue) &&
               Convert::parseSingle(Convert::trimStr(texts[1]), temp.maxExpressionValue) &&
               Convert::parseSingle(Convert::trimStr(texts[2]), temp.minValue) &&
               Convert::parseSingle(Convert::trimStr(texts[3]), temp.maxValue))
            {
                value = temp;
                return true;
            }
        }
        return false;
    }
    
    LinearAnimation::LinearAnimation(const string& expression, const LinearValue& value) : Animation(expression)
    {
        _linearValue = value;
    }
    const LinearValue& LinearAnimation::linearValue() const
    {
        return _linearValue;
    }
    bool LinearAnimation::getValue(float& value) const
    {
        Tag::Value temp = Animation::value(Tag::Type::Float32);
        float k;
        if(_linearValue.k(k))
        {
            value = temp.fValue * k;
            return true;
        }
        return false;
    }
    void LinearAnimation::read(XmlTextReader& reader)
    {
        Animation::read(reader);
        
        LinearValue::parse(reader.getAttribute("linearValue"), _linearValue);
    }
    void LinearAnimation::write(XmlTextWriter& writer)
    {
        // todo: write a node.
    }
    
    MotionAnimation::MotionAnimation(const string& expression, const LinearValue& value) : LinearAnimation(expression, value)
    {
    }
    const MotionAnimation::Direction MotionAnimation::direction() const
    {
        return _direction;
    }
    const string MotionAnimation::toDirectionStr(MotionAnimation::Direction direction)
    {
        switch (direction)
        {
            case MotionAnimation::MotionNone:
                return "None";
            case MotionAnimation::MotionHorz:
                return "Horz";
            case MotionAnimation::MotionVert:
                return "Vert";
            default:
                assert(false);
                break;
        }
        return "UnKnown";
    }
    bool MotionAnimation::toDirection(const string& str, MotionAnimation::Direction& direction)
    {
        if(String::stringEquals(str, "Horz", true))
        {
            direction = MotionAnimation::MotionHorz;
            return true;
        }
        else if(String::stringEquals(str, "Vert", true))
        {
            direction = MotionAnimation::MotionVert;
            return true;
        }
        
        return false;
    }
    void MotionAnimation::execute(GShape* shape)
    {
        float pos;
        if(getValue(pos))
        {
            if(_direction == MotionAnimation::MotionHorz)
                shape->setX(_absoluteCoord ? pos : _originalLocation.x+pos);
            else
                shape->setY(_absoluteCoord ? pos : _originalLocation.y+pos);
        }
    }
    void MotionAnimation::initialize(GShape* shape)
    {
        _originalLocation = shape->location();
        
        Animation::initialize(shape);
    }
    void MotionAnimation::read(XmlTextReader& reader)
    {
        LinearAnimation::read(reader);
        
        toDirection(reader.getAttribute("direction"), _direction);
        Convert::parseBoolean(reader.getAttribute("absoluteCoord"), _absoluteCoord);
    }
    void MotionAnimation::write(XmlTextWriter& writer)
    {
        // todo: write a node.
    }
    
    void percentFill_Painted(void* owner, void* sender, EventArgs* args)
    {
        PercentFillAnimation* animation = static_cast<PercentFillAnimation*>(owner);
        if(animation != NULL)
        {
            animation->percentFillPainted(dynamic_cast<PaintedEventArgs*>(args));
        }
    }
    PercentFillAnimation::PercentFillAnimation(const string& expression, const LinearValue& value) : LinearAnimation(expression, value)
    {
        _shape = NULL;
        _direction = Direction::DirectionNone;
        _percent = 0.0f;
    }
    PercentFillAnimation::~PercentFillAnimation()
    {
        if(_shape != NULL)
        {
            _shape->removePaintedHandler(Delegate(this, percentFill_Painted));
        }
        
        _shape = NULL;
    }
    const PercentFillAnimation::Direction PercentFillAnimation::direction() const
    {
        return _direction;
    }
    void PercentFillAnimation::execute(GShape* shape)
    {
        if(getValue(_percent))
        {
//            Debug::writeFormatLine("percent=%.2f", _percent);
        }
    }
    void PercentFillAnimation::percentFillPainted(PaintedEventArgs* args)
    {
        if(_shape != NULL && _shape->canFill())
        {
            Graphics* graphics = args->graphics;
            Rectangle rcShape = _shape->bounds();
            float width = _percent;
            float height = _percent;
            
            switch (_direction)
            {
                case Animation::FromLeftToRight:
                    rcShape = Rectangle(rcShape.x, rcShape.y, width, rcShape.height);
                    break;
                case Animation::FromRightToLeft:
                    rcShape = Rectangle(rcShape.right() - width, rcShape.y, width, rcShape.height);
                    break;
                case Animation::FromUpToDown:
                    rcShape = Rectangle(rcShape.x, rcShape.y, rcShape.width, height);
                    break;
                case Animation::FromDownToUp:
                    rcShape = Rectangle(rcShape.bottom() - height, rcShape.y, rcShape.width, height);
                    break;
                default:
                    break;
            }
            
            graphics->save();
            graphics->clipPath(_shape->path());
            SkPaint paint;
            if(_fill.toSkPaint(paint))
            {
                graphics->drawRect(SkRectConvert::convert(rcShape), paint);
            }
            graphics->restore();
        }
    }
    void PercentFillAnimation::initialize(GShape* shape)
    {
        _shape = dynamic_cast<GPathShape*>(shape);
        shape->addPaintedHandler(Delegate(this, percentFill_Painted));
        
        switch (_direction)
        {
            case Animation::FromLeftToRight:
            case Animation::FromRightToLeft:
                _linearValue.minValue = 0;
                _linearValue.maxValue = shape->width();
                break;
            case Animation::FromUpToDown:
            case Animation::FromDownToUp:
                _linearValue.minValue = 0;
                _linearValue.maxValue = shape->height();
                break;
            default:
                break;
        }
        
        Animation::initialize(shape);
    }
    void PercentFillAnimation::read(XmlTextReader& reader)
    {
        LinearAnimation::read(reader);
        
        Animation::toDirection(reader.getAttribute("direction"), _direction);
        if(Brush::parse(reader.getAttribute("fill"), _fill))
        {
            _fill.setConfigSource(reader.configFile());
        }
    }
    void PercentFillAnimation::write(XmlTextWriter& writer)
    {
        // todo: write a node.
    }
}
