#ifndef OUTPUTANIMATION_H
#define OUTPUTANIMATION_H

#include "Animation.h"

namespace Drawing
{
    class Canvas;
    class COMMON_EXPORT OutputAnimation : public Animation
    {
    public:
        OutputAnimation(const string& expression = "");
        
        void execute(GShape* shape);
        
        void read(XmlTextReader& reader);
        void write(XmlTextWriter& writer);
        
    protected:
    };
}

#endif // OUTPUTANIMATION_H
