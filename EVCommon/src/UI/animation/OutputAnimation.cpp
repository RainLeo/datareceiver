#include "OutputAnimation.h"
#include "common/system/Convert.h"
#include "common/diag/Debug.h"
#include "../components/Canvas.h"
#include "../components/GText.h"

using namespace Common;

namespace Drawing
{
    OutputAnimation::OutputAnimation(const string& expression) : Animation(expression)
    {
    }
 
    void OutputAnimation::execute(GShape* shape)
    {
        Tag::Value temp = value(Tag::Type::Float32);
        GText* text = dynamic_cast<GText*>(shape);
        if(text != NULL)
        {
            text->setText(Convert::convertStr(temp.fValue, 2));
        }
    }
    void OutputAnimation::read(XmlTextReader& reader)
    {
        Animation::read(reader);
    }
    void OutputAnimation::write(XmlTextWriter& writer)
    {
        // todo: write a node.
    }
}
