#include "Animation.h"
#include "common/system/Convert.h"
#include "LinearAnimation.h"
#include "OutputAnimation.h"
#include "DiscreteAnimation.h"
#include "SwitchAnimation.h"
#include "../components/Canvas.h"

using namespace Common;

namespace Drawing
{
    TagService* Animation::_tagService = NULL;
    void animation_tagValueChanged(void* owner, void* sender, EventArgs* args)
    {
        Animation* animation = static_cast<Animation*>(owner);
        assert(animation);
        animation->_valueChanged = true;
    }
    
    Animation::Animation(const string& expression)
    {
        this->_expression = expression;
        this->_valueChanged = false;
    }
    Animation::~Animation()
    {
    }
    
    const string& Animation::expression() const
    {
        return _expression;
    }
    const string& Animation::name() const
    {
        return _name;
    }
    
    bool Animation::isEmpty() const
    {
        return _expression.empty();
    }
    
    void Animation::operator=(const Animation& value)
    {
        this->_name = value._name;
        this->_expression = value._expression;
    }
    bool Animation::operator==(const Animation& value)
    {
        return this->_name == value._name &&
            this->_expression == value._expression;
    }
    bool Animation::operator!=(const Animation& value)
    {
        return !operator==(value);
    }
    
    void Animation::read(XmlTextReader& reader)
    {
        _name = reader.getAttribute("name");
        _expression = reader.getAttribute("expression");
    }
    void Animation::write(XmlTextWriter& writer)
    {
        // todo: write a node.
    }
    
    bool Animation::valueChanged() const
    {
        return _valueChanged;
    }
    void Animation::setValueChanged(bool valueChanged)
    {
        _valueChanged = valueChanged;
    }
    void Animation::initialize(GShape* shape)
    {
        execute(shape);
    }
    // It must be deleted if type is String.
    Tag::Value Animation::value(Tag::Type type) const
    {
        Tag::Value value = {0};
        TagService* ts = _tagService;
        if(ts != NULL)
        {
            ts->expressionValue(_expression, type, value);
        }
        return value;
    }
    void Animation::setTagService(TagService* service)
    {
        assert(service);
        _tagService = service;
    }
    
    const string Animation::toDirectionStr(Animation::Direction direction)
    {
        switch (direction)
        {
            case Animation::DirectionNone:
                return "None";
            case Animation::FromLeftToRight:
                return "Left";
            case Animation::FromRightToLeft:
                return "Right";
            case Animation::FromUpToDown:
                return "Up";
            case Animation::FromDownToUp:
                return "Down";
            case Animation::FromCenterToSide:
                return "Center";
            case Animation::FromSideToCenter:
                return "Side";
            case Animation::FromCenterToLeftRight:
                return "CenterToLeftRight";
            case Animation::FormCenterToUpDown:
                return "CenterToUpDown";
            case Animation::Positive:
                return "Positive";
            case Animation::Negative:
                return "Negative";
            case Animation::Clockwise:
                return "Clockwise";
            case Animation::Anticlockwise:
                return "Anticlockwise";
            default:
                assert(false);
                break;
        }
        return "UnKnown";
    }
    bool Animation::toDirection(const string& str, Animation::Direction& direction)
    {
        if(String::stringEquals(str, "Left", true))
        {
            direction = Animation::FromLeftToRight;
            return true;
        }
        else if(String::stringEquals(str, "Right", true))
        {
            direction = Animation::FromRightToLeft;
            return true;
        }
        else if(String::stringEquals(str, "Up", true))
        {
            direction = Animation::FromUpToDown;
            return true;
        }
        else if(String::stringEquals(str, "Down", true))
        {
            direction = Animation::FromDownToUp;
            return true;
        }
        else if(String::stringEquals(str, "Center", true))
        {
            direction = Animation::FromCenterToSide;
            return true;
        }
        else if(String::stringEquals(str, "Side", true))
        {
            direction = Animation::FromSideToCenter;
            return true;
        }
        else if(String::stringEquals(str, "CenterToLeftRight", true))
        {
            direction = Animation::FromCenterToLeftRight;
            return true;
        }
        else if(String::stringEquals(str, "CenterToUpDown", true))
        {
            direction = Animation::FormCenterToUpDown;
            return true;
        }
        else if(String::stringEquals(str, "Positive", true))
        {
            direction = Animation::Positive;
            return true;
        }
        else if(String::stringEquals(str, "Negative", true))
        {
            direction = Animation::Negative;
            return true;
        }
        else if(String::stringEquals(str, "Clockwise", true))
        {
            direction = Animation::Clockwise;
            return true;
        }
        else if(String::stringEquals(str, "Anticlockwise", true))
        {
            direction = Animation::Anticlockwise;
            return true;
        }
        
        return false;
    }
    
    Animation* Animations::atByName(const string& name) const
    {
        for (uint i=0; i<count(); i++)
        {
            Animation* animation = Vector<Animation>::at(i);
            if(String::stringEquals(animation->name(), name))
            {
                return animation;
            }
        }
        return nullptr;
    }
    
    Animation* Animation::createAnimation(const string& nodeName)
    {
        Animation* animation = nullptr;
        if(String::stringEquals(nodeName, "Motion", true))
        {
            animation = new MotionAnimation();
        }
        else if(String::stringEquals(nodeName, "Output", true))
        {
            animation = new OutputAnimation();
        }
        else if(String::stringEquals(nodeName, "Percentfill", true))
        {
            animation = new PercentFillAnimation();
        }
        else if(String::stringEquals(nodeName, "Fill", true))
        {
            animation = new FillAnimation();
        }
        else if(String::stringEquals(nodeName, "FlowSwitch", true))
        {
            animation = new FlowSwitchAnimation();
        }
        else if(String::stringEquals(nodeName, "RotateSwitch", true))
        {
            animation = new RotateSwitchAnimation();
        }
        else if(String::stringEquals(nodeName, "Hide", true))
        {
            animation = new HideAnimation();
        }
        return animation;
    }
}
