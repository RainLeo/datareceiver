#ifndef ANIMATION_H
#define ANIMATION_H

#include <string>
#include "common/common_global.h"
#include "common/data/Vector.h"
#include "common/xml/XmlTextReader.h"
#include "common/xml/XmlTextWriter.h"
#include "tag/Tag.h"
#include "tag/TagService.h"

using namespace std;
using namespace Common;
using namespace rtdb;

namespace Drawing
{
    class Canvas;
    class GShape;
    class COMMON_EXPORT Animation
    {
    public:
        enum Direction
        {
        	DirectionNone = 0,
            FromLeftToRight = 1,
            FromRightToLeft = 2,
            FromUpToDown = 3,
            FromDownToUp = 4,
            FromCenterToSide = 5,
            FromSideToCenter = 6,
            FromCenterToLeftRight = 7,
            FormCenterToUpDown = 8,
            
            Positive = 9,
            Negative = 10,
            
            Clockwise = 11,
            Anticlockwise = 12
        };
        
        Animation(const string& expression = "");
        virtual ~Animation();
        
        const string& expression() const;
        const string& name() const;
        
        virtual bool isEmpty() const;
        
        void operator=(const Animation& value);
        bool operator==(const Animation& value);
        bool operator!=(const Animation& value);
        
        virtual void read(XmlTextReader& reader);
        virtual void write(XmlTextWriter& writer);
        
        bool valueChanged() const;
        virtual void execute(GShape* shape) = 0;
        virtual void initialize(GShape* shape);
        
        Tag::Value value(Tag::Type type) const;
        
    public:
        static void setTagService(TagService* service);
        
        static Animation* createAnimation(const string& nodeName);
        
    protected:
        static const string toDirectionStr(Direction direction);
        static bool toDirection(const string& str, Direction& direction);
        
    private:
        void setValueChanged(bool valueChanged = false);
        friend void animation_tagValueChanged(void* owner, void* sender, EventArgs* args);
        
    protected:
        string _expression;
        string _name;
        
        static TagService* _tagService;
        
    private:
        friend Canvas;
        
        bool _valueChanged;
    };

    class COMMON_EXPORT Animations : public Vector<Animation>
    {
    public:
        Animations(bool autoDelete = true, uint capacity = Vector<Animation>::DefaultCapacity) : Vector<Animation>(autoDelete, capacity)
        {
        }
        
        Animation* atByName(const string& name) const;
    };
}

#endif // ANIMATION_H
