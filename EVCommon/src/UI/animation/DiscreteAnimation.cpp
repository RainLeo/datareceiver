#include "DiscreteAnimation.h"
#include "common/system/Convert.h"
#include "common/diag/Debug.h"
#include "../components/Canvas.h"
#include "../utils/SkConvert.h"

using namespace Common;
using namespace rtdb;

namespace Drawing
{
    FillAnimation::FillAnimation(const string& expression, const DiscreteValues<ExpressionfValue, Brush>& value)
        : DiscreteAnimation(expression, value)
    {
    }
    
    void FillAnimation::execute(GShape* shape)
    {
        Brush value;
        if(getValue(value))
        {
            shape->setFill(value);
        }
    }
    void FillAnimation::initialize(GShape* shape)
    {
        DiscreteAnimation::initialize(shape);
    }
    void FillAnimation::read(XmlTextReader& reader)
    {
        DiscreteAnimation::read(reader);
    }
    void FillAnimation::write(XmlTextWriter& writer)
    {
        DiscreteAnimation::write(writer);
    }
    
    HideAnimation::HideAnimation(const string& expression, const DiscreteValues<ExpressionbValue, BooleanValue>& value)
        : BooleanAnimation(expression, value)
    {
    }

    void HideAnimation::execute(GShape* shape)
    {
        BooleanValue value;
        if(getValue(value))
        {
            bool visible = value.value;
            shape->setVisible(visible);
        }
    }
    void HideAnimation::initialize(GShape* shape)
    {
        DiscreteAnimation::initialize(shape);
    }
    void HideAnimation::read(XmlTextReader& reader)
    {
        DiscreteAnimation::read(reader);
    }
    void HideAnimation::write(XmlTextWriter& writer)
    {
        DiscreteAnimation::write(writer);
    }
}
