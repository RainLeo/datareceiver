#ifndef LINEARANIMATION_H
#define LINEARANIMATION_H

#include "Animation.h"
#include "../utils/Point.h"
#include "../components/GPathShape.h"

namespace Drawing
{
    class Canvas;
    struct LinearValue
    {
    public:
        LinearValue(float minExpressionValue=0.0f, float maxExpressionValue=0.0f, float minValue=0.0f, float maxValue=0.0f)
        {
           this->minExpressionValue = minExpressionValue;
           this->maxExpressionValue = maxExpressionValue;
           this->minValue = minValue;
           this->maxValue = maxValue;
        }
        
        string toString() const;
        
        bool k(float& value) const;
        
    public:
        static bool parse(const string& str, LinearValue& value);
        
    public:
        float minExpressionValue;
        float maxExpressionValue;
        float minValue;
        float maxValue;
        
        const static LinearValue Empty;
    };
    
    class COMMON_EXPORT LinearAnimation : public Animation
    {
    public:
        LinearAnimation(const string& expression = "", const LinearValue& value = LinearValue::Empty);
        
        const LinearValue& linearValue() const;
        
        bool getValue(float& value) const;
        
        void read(XmlTextReader& reader);
        void write(XmlTextWriter& writer);
        
    protected:
        LinearValue _linearValue;
    };
    
    class COMMON_EXPORT MotionAnimation : public LinearAnimation
    {
    public:
        enum Direction
        {
            MotionNone = 0,
            MotionHorz = 1,
            MotionVert = 2
        };
        MotionAnimation(const string& expression = "", const LinearValue& value = LinearValue::Empty);
        
        const Direction direction() const;
        const bool absoluteCoord() const;
        
        void execute(GShape* shape);
        void initialize(GShape* shape);
        
        void read(XmlTextReader& reader);
        void write(XmlTextWriter& writer);
        
    private:
        static const string toDirectionStr(Direction direction);
        static bool toDirection(const string& str, Direction& direction);
        
    private:
        Direction _direction;
        bool _absoluteCoord;
        
        Point _originalLocation;
    };
    
    class COMMON_EXPORT PercentFillAnimation : public LinearAnimation
    {
    public:
        PercentFillAnimation(const string& expression = "", const LinearValue& value = LinearValue::Empty);
        ~PercentFillAnimation();
        
        const Direction direction() const;
        
        void execute(GShape* shape);
        void initialize(GShape* shape);
        
        void read(XmlTextReader& reader);
        void write(XmlTextWriter& writer);
        
    private:
        friend void percentFill_Painted(void* owner, void* sender, EventArgs* args);
        void percentFillPainted(PaintedEventArgs* args);
        
    private:
        Direction _direction;
        Brush _fill;
        GPathShape* _shape;
        float _percent;
    };
}

#endif // LINEARANIMATION_H
