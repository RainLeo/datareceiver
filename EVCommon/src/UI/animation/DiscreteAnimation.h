#ifndef DISCRETEANIMATION_H
#define DISCRETEANIMATION_H

#include "Animation.h"
#include "../utils/Point.h"
#include "../utils/Brush.h"
#include "../components/GPathShape.h"

namespace Drawing
{
    class Canvas;
    struct BooleanValue
    {
        bool value;
        BooleanValue(bool value = false)
        {
            this->value = value;
        }
        
        void operator=(const BooleanValue& value)
        {
            this->value = value.value;
        }
        bool operator==(const BooleanValue& value)
        {
            return this->value == value.value;
        }
        bool operator!=(const BooleanValue& value)
        {
            return !operator==(value);
        }
        
        string toString() const
        {
            return Convert::convertStr(value);
        }
        static bool parse(const string& str, BooleanValue& value)
        {
            return Convert::parseBoolean(str, value.value);
        }
    };
    struct FloatValue
    {
        float value;
        FloatValue(float value = 0.0f)
        {
            this->value = value;
        }
        
        void operator=(const FloatValue& value)
        {
            this->value = value.value;
        }
        bool operator==(const FloatValue& value)
        {
            return this->value == value.value;
        }
        bool operator!=(const FloatValue& value)
        {
            return !operator==(value);
        }
        
        string toString() const
        {
            return Convert::convertStr(value);
        }
        static bool parse(const string& str, FloatValue& value)
        {
            return Convert::parseSingle(str, value.value);
        }
    };
    struct ExpressionValue
    {
        Tag::Type type;
        Tag::Value value;
        ExpressionValue(Tag::Type type)
        {
            assert(type != Tag::Type::Text);
            this->type = type;
            value.nValue = {0};
        }
        
        string toString() const
        {
            return Tag::toValueString(type, value);
        }
        static bool parse(const string& str, ExpressionValue& value)
        {
            return Tag::parseValueString(str, value.type, value.value);
        }
    };
    struct ExpressionfValue : public ExpressionValue
    {
        ExpressionfValue(Tag::Type type = Tag::Type::Float32) : ExpressionValue(type)
        {
        }
        
        void operator=(const ExpressionfValue& value)
        {
            assert(value.type != Tag::Type::Text);
            this->type = value.type;
            this->value = value.value;
        }
        bool operator==(const ExpressionfValue& value) const
        {
            return this->type == value.type &&
                Tag::isValueEqual(type, this->value, value.value);
        }
        bool operator!=(const ExpressionfValue& value) const
        {
            return !operator==(value);
        }
    };
    struct ExpressionbValue : public ExpressionValue
    {
        ExpressionbValue(Tag::Type type = Tag::Type::Digital) : ExpressionValue(type)
        {
        }
        
        void operator=(const ExpressionbValue& value)
        {
            assert(value.type != Tag::Type::Text);
            this->type = value.type;
            this->value = value.value;
        }
        bool operator==(const ExpressionbValue& value) const
        {
            return this->type == value.type &&
                Tag::isValueEqual(type, this->value, value.value);
        }
        bool operator!=(const ExpressionbValue& value) const
        {
            return !operator==(value);
        }
    };
    
    // T: expression type; K: the value of shape property.
    template <class T, class K>
    class DiscreteValue
    {
    public:
        DiscreteValue(const T& expressionValue = T(), const K& value = K())
        {
            this->expressionValue = expressionValue;
            this->value = value;
        }
        
        string toString() const
        {
            return Convert::convertStr("%s,%s", expressionValue.toString().c_str(), value.toString().c_str());
        }
        static bool parse(const string& str, DiscreteValue* dv)
        {
            StringArray texts;
            Convert::splitItems(str, texts, ',');
            if(texts.count() == 2)
            {
                DiscreteValue temp;
                if(!T::parse(texts[0], temp.expressionValue))
                {
                    return false;
                }
                if(!K::parse(texts[1], temp.value))
                {
                    return false;
                }
                
                dv->expressionValue = temp.expressionValue;
                dv->value = temp.value;
                return true;
            }
            return false;
        }
        
    public:
        T expressionValue;
        K value;
    };
    template <class T, class K>
    class DiscreteValues : public Vector<DiscreteValue<T, K>>
    {
    public:
        DiscreteValues(bool autoDelete = true, uint capacity = Vector<DiscreteValue<T, K>>::DefaultCapacity) : Vector<DiscreteValue<T, K>>(autoDelete, capacity)
        {
        }
        
        const DiscreteValue<T, K>* at(const T& expressionValue) const
        {
            for (uint i=0; i<this->count(); i++)
            {
                const DiscreteValue<T, K>* dv = Vector<DiscreteValue<T, K>>::at(i);
                if(dv->expressionValue == expressionValue)
                    return dv;
            }
            return nullptr;
        }
        
        string toString() const
        {
            StringBuilder sb;
            for (uint i=0; i<this->count(); i++)
            {
                if(i != 0)
                    sb.append(';');
                
                const DiscreteValue<T, K>* dv = this->at(i);
                sb.append(dv->toString());
            }
            return sb.toString();
        }
        static bool parse(const string& str, DiscreteValues& dvs)
        {
            DiscreteValues temp(false);
            StringArray texts;
            Convert::splitItems(str, texts);
            for(uint i=0; i<texts.count(); i++)
            {
                DiscreteValue<T, K>* value = new DiscreteValue<T, K>();
                if(DiscreteValue<T, K>::parse(texts[i], value))
                {
                    temp.add(value);
                }
                else
                {
                    delete value;
                    return false;
                }
            }
            
            dvs.clear();
            dvs.addRange(&temp);
            return true;
        }
    };
    
    template <class T, class K>
    class COMMON_EXPORT DiscreteAnimation : public Animation
    {
    public:
        DiscreteAnimation(const string& expression = "", const DiscreteValues<T, K>& value = DiscreteValues<T, K>()) : Animation(expression)
        {
            _values.addRange(&value);
        }
        
    protected:
        const DiscreteValues<T, K>& discreteValues() const
        {
            return _values;
        }
        
        virtual bool getValue(K& value) const
        {
            T expressionValue;
            expressionValue.value = Animation::value(expressionValue.type);
            const DiscreteValue<T, K>* dv = _values.at(expressionValue);
            if(dv != nullptr)
            {
                value = dv->value;
                return true;
            }
            return false;
        }
        
        void read(XmlTextReader& reader) override
        {
            Animation::read(reader);
            
            DiscreteValues<T, K>::parse(reader.getAttribute("value"), _values);
        }
        void write(XmlTextWriter& writer) override
        {
            // todo: write a node.
        }
        
    protected:
        DiscreteValues<T, K> _values;
    };
    
    class COMMON_EXPORT BooleanAnimation : public DiscreteAnimation<ExpressionbValue, BooleanValue>
    {
    public:
        BooleanAnimation(const string& expression = "", const DiscreteValues<ExpressionbValue, BooleanValue>& value = DiscreteValues<ExpressionbValue, BooleanValue>()) : DiscreteAnimation<ExpressionbValue, BooleanValue>(expression, value)
        {
        }
        
    protected:
        bool getValue(BooleanValue& value) const override
        {
            if(_values.count() == 1)
            {
                ExpressionbValue expressionValue;
                expressionValue.value = Animation::value(expressionValue.type);
                const DiscreteValue<ExpressionbValue, BooleanValue>* dv = _values[0];
                value = (expressionValue.value.bValue == dv->expressionValue.value.bValue) ? dv->value.value : !dv->value.value;
                return true;
            }
            return false;
        }
    };
    
    class COMMON_EXPORT FillAnimation : public DiscreteAnimation<ExpressionfValue, Brush>
    {
    public:
        FillAnimation(const string& expression = "", const DiscreteValues<ExpressionfValue, Brush>& value = DiscreteValues<ExpressionfValue, Brush>());
        
        void execute(GShape* shape) override;
        void initialize(GShape* shape) override;
        
        void read(XmlTextReader& reader) override;
        void write(XmlTextWriter& writer) override;
    };
    
    class COMMON_EXPORT HideAnimation : public BooleanAnimation
    {
    public:
        HideAnimation(const string& expression = "", const DiscreteValues<ExpressionbValue, BooleanValue>& value = DiscreteValues<ExpressionbValue, BooleanValue>());
        
        void execute(GShape* shape) override;
        void initialize(GShape* shape) override;
        
        void read(XmlTextReader& reader) override;
        void write(XmlTextWriter& writer) override;
    };
}

#endif // DISCRETEANIMATION_H
