#ifndef SWITCHANIMATION_H
#define SWITCHANIMATION_H

#include "Animation.h"
#include "../utils/Point.h"
#include "../utils/Brush.h"
#include "../components/GPathShape.h"
#include "common/system/Singleton.h"
#include "common/system/Timespan.h"

namespace Drawing
{
    class Canvas;
    struct SwitchValue
    {
    public:
        SwitchValue(TimeSpan interval = TimeSpan::fromSeconds(1))
        {
            this->interval = interval;
        }
        
    public:
        TimeSpan interval;
        
        const static SwitchValue Empty;
    };
    
    class COMMON_EXPORT SwitchAnimation : public Animation
    {
    public:
        SwitchAnimation(const string& expression = "", const SwitchValue& value = SwitchValue::Empty);
        ~SwitchAnimation();
        
        const SwitchValue& switchValue() const;
        
        void execute(GShape* shape) override;
        void initialize(GShape* shape) override;
        
        void read(XmlTextReader& reader) override;
        void write(XmlTextWriter& writer) override;
        
        const Rectangle update();
        
    protected:
        virtual bool updateInner() = 0;
        
    protected:
        SwitchValue _switchValue;
        
        GShape* _shape;
    };
    
    class COMMON_EXPORT SwitchTimer
    {
    public:
        class Group
        {
        public:
            Animations animations;
            uint start;
            TimeSpan interval;
            string registerStr;
            
            Group();
            
            bool isTimeup();
        };
        class Groups : public Vector<Group>
        {
        public:
            Groups(bool autoDelete = true, uint capacity = Vector<Group>::DefaultCapacity) : Vector<Group>(autoDelete, capacity)
            {
            }
            
            Group* getSameGroup(const TimeSpan& interval) const;
        };
        
        SwitchTimer();
        ~SwitchTimer();
        
        void addAnimation(const SwitchAnimation* animation);
        void clearAnimations();
        const Rectangle process();
        
    private:
        const Rectangle refreshAnimations(const Animations& animations);
        
    private:
        Groups _groups;
        mutex _groupsMutex;
    };
    
    class COMMON_EXPORT FlowSwitchAnimation : public SwitchAnimation
    {
    public:
        FlowSwitchAnimation(const string& expression = "", const SwitchValue& value = SwitchValue::Empty);
        
        void execute(GShape* shape) override;
        void initialize(GShape* shape) override;
        
        void read(XmlTextReader& reader) override;
        void write(XmlTextWriter& writer) override;
        
    protected:
        bool updateInner() override;
        
    private:
        Direction _direction;       // positive & negative.
        int _step;                  // 0-100 %
        bool _state;                // true: flow
    };
    
    class COMMON_EXPORT RotateSwitchAnimation : public SwitchAnimation
    {
    public:
        RotateSwitchAnimation(const string& expression = "", const SwitchValue& value = SwitchValue::Empty);
        
        void execute(GShape* shape) override;
        void initialize(GShape* shape) override;
        
        void read(XmlTextReader& reader) override;
        void write(XmlTextWriter& writer) override;
        
    protected:
        bool updateInner() override;
        
    private:
        Direction _direction;       // clockwise & anticlockwise.
        int _step;                  // 0-100 %
        bool _state;                // true: rotate
    };
}

#endif // SWITCHANIMATION_H
