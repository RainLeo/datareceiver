#include "SwitchAnimation.h"
#include "common/system/Convert.h"
#include "common/diag/Debug.h"
#include "common/thread/TickTimeout.h"
#include "../components/Canvas.h"
#include "../utils/SkConvert.h"

using namespace Common;
using namespace rtdb;

namespace Drawing
{
    const SwitchValue SwitchValue::Empty = SwitchValue();
    
    SwitchTimer::Group::Group()
    {
        animations.setAutoDelete(false);
        start = 0;
    }
    bool SwitchTimer::Group::isTimeup()
    {
        if(start == 0)
        {
            start = TickTimeout::GetCurrentTickCount();
        }
        
        assert(interval != TimeSpan::Zero);
        bool result = TickTimeout::IsTimeout(start, start + (uint)interval.totalMilliseconds());
        if(result)
        {
            start = 0;
        }
        return result;
    }
    SwitchTimer::Group* SwitchTimer::Groups::getSameGroup(const TimeSpan& interval) const
    {
        for (uint i=0; i<count(); i++)
        {
            SwitchTimer::Group* group = at(i);
            if(group->interval == interval)
                return group;
        }
        return NULL;
    }
    
    SwitchTimer::SwitchTimer()
    {
    }
    SwitchTimer::~SwitchTimer()
    {
    }
    void SwitchTimer::addAnimation(const SwitchAnimation* animation)
    {
        Locker locker(&_groupsMutex);
        Group* group = _groups.getSameGroup(animation->switchValue().interval);
        if(group == nullptr)
        {
            group = new Group();
            group->interval = animation->switchValue().interval;
            _groups.add(group);
        }
        group->animations.add(animation);
    }
    void SwitchTimer::clearAnimations()
    {
        Locker locker(&_groupsMutex);
        _groups.clear();
    }
    const Rectangle SwitchTimer::process()
    {
        Rectangle rect;
        Locker locker(&_groupsMutex);
        for (uint i=0; i<_groups.count(); i++)
        {
            Group* group = _groups[i];
            if(group->isTimeup())
            {
                //                Debug::writeLine("switch time up!");
                
                const Rectangle bounds = refreshAnimations(group->animations);
                if(!bounds.isEmpty())
                {
                    rect.unions(bounds);
                }
            }
        }
        return rect;
    }
    const Rectangle SwitchTimer::refreshAnimations(const Animations& animations)
    {
        Rectangle rect;
        for (uint i=0; i<animations.count(); i++)
        {
            SwitchAnimation* sa = dynamic_cast<SwitchAnimation*>(animations[i]);
            if(sa != nullptr)
            {
                const Rectangle bounds = sa->update();
                if(!bounds.isEmpty())
                    rect.unions(bounds);
            }
        }
        return rect;
    }
    
    SwitchAnimation::SwitchAnimation(const string& expression, const SwitchValue& value) : Animation(expression)
    {
        _switchValue = value;
        _shape = nullptr;
    }
    SwitchAnimation::~SwitchAnimation()
    {
        _shape = nullptr;
    }
    const SwitchValue& SwitchAnimation::switchValue() const
    {
        return _switchValue;
    }
    void SwitchAnimation::read(XmlTextReader& reader)
    {
        Animation::read(reader);
        
        TimeSpan::parse(reader.getAttribute("interval"), _switchValue.interval);
    }
    void SwitchAnimation::write(XmlTextWriter& writer)
    {
        Animation::write(writer);
    }
    void SwitchAnimation::execute(GShape* shape)
    {
        updateInner();
    }
    void SwitchAnimation::initialize(GShape* shape)
    {
        _shape = shape;
        
        Animation::initialize(shape);
    }
    const Rectangle SwitchAnimation::update()
    {
        Rectangle rect;
        if(_shape != nullptr)
        {
            const Rectangle& old = _shape->invalidateBounds();
            if(updateInner())
            {
                rect = old;
                rect.unions(_shape->invalidateBounds());
            }
        }
        return rect;
    }
    
    FlowSwitchAnimation::FlowSwitchAnimation(const string& expression, const SwitchValue& value)
    : SwitchAnimation(expression, value)
    {
        _direction = Direction::DirectionNone;
        _step = 0;
        _state = true;
    }
    
    void FlowSwitchAnimation::execute(GShape* shape)
    {
        SwitchAnimation::execute(shape);
    }
    void FlowSwitchAnimation::initialize(GShape* shape)
    {
        SwitchAnimation::initialize(shape);
    }
    void FlowSwitchAnimation::read(XmlTextReader& reader)
    {
        SwitchAnimation::read(reader);
        
        Animation::toDirection(reader.getAttribute("direction"), _direction);
        Convert::parseBoolean(reader.getAttribute("state"), _state);
        Convert::parseInt32(reader.getAttribute("step"), _step);
    }
    void FlowSwitchAnimation::write(XmlTextWriter& writer)
    {
        SwitchAnimation::write(writer);
    }
    
    bool FlowSwitchAnimation::updateInner()
    {
        if(_shape != nullptr)
        {
            Tag::Value result = value(Tag::Type::Digital);
            if ((result.bValue && _state) ||
                (!result.bValue && !_state))
            {
                const Pen& pen = _shape->stroke();
                if(pen.isDash())
                {
                    float phase = pen.phase;
                    float totalIntervals = 0.0f;
                    for (uint i=0; i<pen.intervals.count(); i++)
                    {
                        totalIntervals += pen.intervals[i];
                    }
                    
                    float phaseStep = (float)_step / 100.0f * totalIntervals;
                    
                    if (_direction == Animation::Positive)
                    {
                        if ((phase += phaseStep) >= totalIntervals)
                        {
                            phase = 0.0f;
                        }
                    }
                    else if (_direction == Animation::Negative)
                    {
                        if ((phase -= phaseStep) <= -totalIntervals)
                        {
                            phase = 0.0f;
                        }
                    }
                    else
                    {
                        return false;
                    }
                    
                    //                    Debug::writeFormatLine("phase: %f", phase);
                    
                    Pen newPen = pen;
                    newPen.phase = phase;
                    _shape->setStroke(newPen);
                    return true;
                }
            }
        }
        return false;
    }
    
    RotateSwitchAnimation::RotateSwitchAnimation(const string& expression, const SwitchValue& value)
    : SwitchAnimation(expression, value)
    {
        _direction = Direction::DirectionNone;
        _step = 0;
        _state = true;
    }
    
    void RotateSwitchAnimation::execute(GShape* shape)
    {
        SwitchAnimation::execute(shape);
    }
    void RotateSwitchAnimation::initialize(GShape* shape)
    {
        SwitchAnimation::initialize(shape);
    }
    void RotateSwitchAnimation::read(XmlTextReader& reader)
    {
        SwitchAnimation::read(reader);
        
        Animation::toDirection(reader.getAttribute("direction"), _direction);
        Convert::parseBoolean(reader.getAttribute("state"), _state);
        Convert::parseInt32(reader.getAttribute("step"), _step);
    }
    void RotateSwitchAnimation::write(XmlTextWriter& writer)
    {
        SwitchAnimation::write(writer);
    }
    
    bool RotateSwitchAnimation::updateInner()
    {
        if(_shape != nullptr)
        {
            Tag::Value result = value(Tag::Type::Digital);
            if ((result.bValue && _state) ||
                (!result.bValue && !_state))
            {
                float angle = _shape->angle();
                if (_direction == Animation::Clockwise)
                {
                    if ((angle += _step) >= 360.0f)
                    {
                        angle = 0.0f;
                    }
                }
                else if (_direction == Animation::Anticlockwise)
                {
                    if ((angle -= _step) <= -360.0f)
                    {
                        angle = 0.0f;
                    }
                }
                else
                {
                    return false;
                }
                
                _shape->setAngle(angle);
                return true;
            }
        }
        return false;
    }
}
