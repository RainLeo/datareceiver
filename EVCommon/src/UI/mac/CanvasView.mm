//
//  Canvas.mm
//  common
//
//  Created by baowei on 15/3/21.
//  Copyright (c) 2015 com. All rights reserved.
//

#import "CanvasView.h"
#include "CanvasWindow.h"
#include <crt_externs.h>

@implementation CanvasView

void invalidateInner(SkWindow* win)
{
    // Call the Objective-C method using Objective-C syntax
    if ([NSThread isMainThread])
    {
        win->inval(NULL);
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            win->inval(NULL);
        });
    }
}
void invalidateRectInner(SkWindow* win, const Rectangle& rect)
{
    // Call the Objective-C method using Objective-C syntax
    SkRect bounds = SkRectConvert::convert(rect);
    if ([NSThread isMainThread])
    {
        win->inval((SkRect*)&bounds);
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            win->inval((SkRect*)&bounds);
        });
    }
}

- (id)initWithDefaults {
    if ((self = [super initWithDefaults])) {
//        fWind = create_sk_window((__bridge void*)self, *_NSGetArgc(), *_NSGetArgv());
    }
    return self;
}

- (void)dealloc{
    [super dealloc];
    
//    if (fWind != NULL)
//    {
//        delete fWind;
//        fWind = NULL;
//    }
}

- (void)scrollWheel:(NSEvent *)theEvent {
    
}

- (CanvasWindow*)canvasWin{
    return dynamic_cast<CanvasWindow*>(fWind);
}

@end
