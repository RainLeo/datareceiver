//
//  Canvas.h
//  testUI2
//
//  Created by baowei on 15/3/21.
//  Copyright (c) 2015年 com. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SkNSView.h"
#include "../utils/CanvasWindow.h"

@interface CanvasView : SkNSView
{
}

- (CanvasWindow*)canvasWin;

@end
