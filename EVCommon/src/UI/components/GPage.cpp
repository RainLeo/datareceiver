#include "GPage.h"
#include "common/IO/Path.h"
#include "common/system/Convert.h"
#include "../configuration/PageReader.h"
#include "GCombo.h"
#include "tag/TagService.h"

using namespace Common;

namespace Drawing
{
    GPage::GPage(PageInfo* page) : GContainer()
    {
        _name = page->name;
        _pageInfo = page;
        _loaded = false;
        _transfer = true;
    }
    
    const PageInfo* GPage::pageInfo() const
    {
        return _pageInfo;
    }
    
    bool GPage::load()
    {
        bool result = false;
        if (!_pageInfo->name.empty() &&
            !_pageInfo->rootFile.isEmpty() &&
            !_pageInfo->source.empty())
        {
            if (!_loaded)
            {
                ConfigFile file = _pageInfo->rootFile;
                file.fileName = Path::combine(file.fileName, _pageInfo->source);
                PageReader reader(file, this);
                result = reader.Configuration::load();
                
//                if (LoadedCommand != null)
//                {
//                    LoadedCommand.Run();
//                }
            }
            
            _loaded = true;
        }
        return result;
    }
    
    bool GPage::open(Canvas* canvas)
    {
        updateCanvas(canvas, _shapes);
        
        return true;
    }
    bool GPage::close()
    {
        return true;
    }
    void GPage::updateCanvas(Canvas* canvas, const GShapes& shapes)
    {
        for (uint i=0; i<shapes.count(); i++)
        {
            GShape* shape = _shapes[i];
            shape->_canvas = canvas;
            
            GCombo* combo = dynamic_cast<GCombo*>(shape);
            if(combo != nullptr)
            {
                updateCanvas(canvas, combo->_shapes);
            }
        }
    }
    
    void GPage::setBounds(const Rectangle& rect)
    {
        GContainer::setBounds(rect);
        
        _background.update(rect);
    }
    
    Brush& GPage::background() const
    {
        return (Brush&)_background;
    }
    void GPage::setBackground(const Brush& background)
    {
        _background = background;
    }
    
    void GPage::addAnimation(const Animation* animation)
    {
        _animations.add(animation);
    }
    
    bool GPage::transfer() const
    {
        return _transfer;
    }
    void GPage::setTransfer(bool transfer)
    {
        _transfer = transfer;
    }
}
