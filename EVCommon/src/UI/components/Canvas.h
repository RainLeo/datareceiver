#ifndef CANVAS_H
#define CANVAS_H

#include <string>
#include <mutex>
#include "common/common_global.h"
#include "common/thread/Thread.h"
#include "common/xml/ConfigFile.h"
#include "../configuration/UIInfo.h"
#include "tag/TagService.h"
#include "base/BaseService.h"
#include "GPage.h"
#include "../animation/SwitchAnimation.h"
#include "SkView.h"

using namespace std;
using namespace Common;

class SkWindow;
namespace Drawing
{
    typedef void* ViewRef;
    class COMMON_EXPORT Canvas
    {
    public:
        Canvas(SkWindow* view, const Rectangle& rect = Rectangle());
        virtual ~Canvas();
        
        void onPaint(Graphics* graphics, const Rectangle& rcClip);
        void onPaintBackground(Graphics* graphics);
        
        void invalidate();
        void invalidate(const Rectangle& rect);
        
        bool loadPages(const ConfigFile& file);
        
//        // touch events
//        void onTouchBegan(const Touches& touches);
//        void onTouchMoved(const Touches& touches);
//        void onTouchEnded(const Touches& touches);
//        
//        // mouse events
//        void onMouseDown(const Mouse& mouse);
//        void onMouseUp(const Mouse& mouse);
//        void onMouseMoved(const Mouse& mouse);
//        
//        // key events
//        void onKeyDown(const Key& key);
//        void onKeyUp(const Key& key);
        
        bool closeCurrentPage();
        bool reopenCurrentPage();
        
        bool openPage(const string& name);
        bool closePage(const string& name);

        void addServices(const Services* services);
        
        const Rectangle& bounds() const;
        void setBounds(const Rectangle& rect);
        
        void onClick(SkView::Click* click);
        
        bool transfer() const;
        
    public:
//        static const Point translateLocation(const SkMatrix& matrix, const Point& point);
//        static const Rectangle translateRect(const SkMatrix& matrix, const Rectangle& rect);
        
    private:
        bool openPage(PageInfo* info);
        bool closePage(GPage* page);
        
        const PageInfo* getPageInfo(const string& name) const;
        
        // touch events
        void onClick();
        void onDoubleClick();
        
        void bindTagAnimation(GPage* page);
        void bindTagAnimation(GShape* shape, TagService* ts);
        void unbindTagAnimation(GPage* page);
        void unbindTagAnimation(GShape* shape, TagService* ts);
        
        friend void animationProc(void* parameter);
        void animationProcInner();
        void processAnimation(GShape* shape, Rectangle& rect);
        
        TagService* tagService();
        template<class T>
        T* getService();
        
        float width() const;
        float height() const;
        
        bool panning() const;
        bool zooming() const;
        float zoomFactorX() const;
        float zoomFactorY() const;
        const Rectangle visibleBounds() const;
        const Point translateOriginalLocation(const Point& point) const;
        const Rectangle translateOriginalRect(const Rectangle& rect) const;
        // Translate the actual location, from actual coordinate to original coordinate.
        const Point translateActualLocation(const Point& point) const;
        // Translate the actual bounds, from actual coordinate to original coordinate.
        const Rectangle translateActualRect(const Rectangle& rect) const;
        
    private:
        SkWindow* _view;
        Rectangle _bounds;
        
        UIInfo _uiInfo;
        GPages _pages;
        
        GPage* _currentPage;
        mutex _currentPageMutex;
        
        Thread* _animationThread;
        
        Services _services;
        
        float _previousDistance;
        bool _pinchZoom;
        
        Point _previousPoint;
        bool _pinchPan;
        
        float _zoomFactorX;
        float _zoomFactorY;
        Point _autoScrollPosition;
        
        const int DefaultZoomFactor = 100;
        const int MaxZoomFactor = 1000;
        const int MinZoomFactor = 10;
        
        SwitchTimer _switchTimer;
    };    
}

#endif // CANVAS_H
