#ifndef GPATHSHAPE_H
#define GPATHSHAPE_H

#include <string>
#include "common/common_global.h"
#include "../utils/GraphicsPath.h"
#include "GShape.h"

using namespace std;

namespace Drawing
{
    class COMMON_EXPORT GPathShape : public GShape
    {
    public:
        GPathShape();
        GPathShape(const Rectangle& rect);
        ~GPathShape();
        
        bool isEmpty() const override;
        string toString() const override;
        
        void paint(Graphics* graphics, const Rectangle& rcClip) override;
        
        bool canFill() const override;
        
        Rectangle bounds() const override;
        void setBounds(const Rectangle& rect) override;
        
        const GraphicsPath& path() const;

        void operator=(const GPathShape& value);
        bool operator==(const GPathShape& value) const;
        bool operator!=(const GPathShape& value) const;
        
    protected:
        GraphicsPath _path;
    };
    
    class COMMON_EXPORT GPolygon : public GPathShape
    {
    public:
        GPolygon();
        GPolygon(const Rectangle& rect);
        
        void read(XmlTextReader& reader) override;
        void write(XmlTextWriter& writer) override;
    };
    
    class COMMON_EXPORT GPolyline : public GPathShape
    {
    public:
        GPolyline();
        GPolyline(const Rectangle& rect);
        
        void read(XmlTextReader& reader) override;
        void write(XmlTextWriter& writer) override;
    };
    
    class COMMON_EXPORT GLine : public GPathShape
    {
    public:
        GLine();
        GLine(const Rectangle& rect);
        GLine(const Point& start, const Point& end);
        
        void setBounds(const Rectangle& rect) override;
        
        void read(XmlTextReader& reader) override;
        void write(XmlTextWriter& writer) override;
        
        bool isVertical() const;
        bool isHorzontal() const;
        
    private:
        void addLine(const Point& start, const Point& end);
    };
    
    class COMMON_EXPORT GEllipse : public GPathShape
    {
    public:
        GEllipse();
        GEllipse(const Rectangle& rect);
        
        void setBounds(const Rectangle& rect) override;
        
        void read(XmlTextReader& reader) override;
        void write(XmlTextWriter& writer) override;
    };
    
    class COMMON_EXPORT GRectangle : public GPathShape
    {
    public:
        GRectangle();
        GRectangle(const Rectangle& rect);
        
        void setBounds(const Rectangle& rect) override;
        
        void read(XmlTextReader& reader) override;
        void write(XmlTextWriter& writer) override;
    };
    
    class COMMON_EXPORT GRoundRectangle : public GRectangle
    {
    public:
        GRoundRectangle();
        GRoundRectangle(const Rectangle& rect, float cornerWidth = 0.0f, float cornerHeight = 0.0f);
        
        void setBounds(const Rectangle& rect) override;
        
        void read(XmlTextReader& reader) override;
        void write(XmlTextWriter& writer) override;
        
    protected:
        float _cornerWidth;
        float _cornerHeight;
    };
    
    class COMMON_EXPORT GCurve : public GPathShape
    {
    public:
        GCurve();
        GCurve(const Rectangle& rect);
        
        void read(XmlTextReader& reader) override;
        void write(XmlTextWriter& writer) override;
    };
}

#endif	// GPATHSHAPE_H
