#include "GContainer.h"
#include "common/system/Convert.h"

using namespace Common;

namespace Drawing
{
    GContainer::GContainer()
    {
    }
    
    bool GContainer::isEmpty() const
    {
        for (uint i=0; i<_shapes.count(); i++)
        {
            GShape* shape = _shapes[i];
            if(!shape->isEmpty())
                return false;
        }
        return true;
    }
    string GContainer::toString() const
    {
        return Convert::convertStr("[GContainer: Name=%s,Count=%d]", _name.c_str(), _shapes.count());
    }
    
    void GContainer::addShape(const GShape* shape)
    {
        _shapes.add(shape);
    }
    
    void GContainer::paint(Graphics* graphics, const Rectangle& rcClip)
    {
        if(!_visible)
            return;
        
        GraphicsRestore gr(graphics);        
        graphics->concat(matrix());
        
        for (uint i=0; i<_shapes.count(); i++)
        {
            GShape* shape = _shapes[i];
            shape->paint(graphics, rcClip);
            shape->painted(graphics, rcClip);
        }
    }
    
    Rectangle GContainer::bounds() const
    {
        return _bounds;
    }
    void GContainer::setBounds(const Rectangle& rect)
    {
        _bounds = rect;
    }
    
    bool GContainer::canFill() const
    {
        for (uint i=0; i<_shapes.count(); i++)
        {
            GShape* shape = _shapes[i];
            if(shape->canFill())
                return true;
        }
        return false;
    }
    
    const GShapes* GContainer::shapes() const
    {
        return &_shapes;
    }
    
    void GContainer::onClick(SkView::Click* click)
    {
        if(!_bounds.contains(click->fCurr.x(), click->fCurr.y()))
            return;
        
        for (uint i=0; i<_shapes.count(); i++)
        {
            GShape* shape = _shapes[i];
            shape->onClick(click);
            if(shape->capture())
                break;
        }
    }
}