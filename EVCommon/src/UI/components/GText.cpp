#include "GText.h"
#include "common/system/Convert.h"
#include "SkTypeface.h"
#include "../utils/Render.h"

using namespace Common;

namespace Drawing
{
    GText::GText() : GShape()
    {
    }
    GText::GText(const Rectangle& rect) : GShape()
    {
        setBounds(rect);
    }
    
    bool GText::canFill() const
    {
        return false;
    }
    
    bool GText::isEmpty() const
    {
        return this->_bounds.isEmpty();
    }
    string GText::toString() const
    {
        return Convert::convertStr("[GText: Name=%s,Paint=%s,Bounds=%s]",
                                   _name.c_str(),
                                   _bounds.toString().c_str());
    }
    
    void GText::paint(Graphics* graphics, const Rectangle& rcClip)
    {
        if(!_visible)
            return;

        if(!_bounds.isEmpty() && _bounds.intersectsWith(rcClip))
        {
            SkPaint paint;
            if(_fill.toSkPaint(paint) && _font.toSkPaint(paint))
            {
                GraphicsRestore gr(graphics);
                graphics->concat(matrix());
                
//                paint.setTextAlign(SkPaint::Align::kLeft_Align);
//                paint.setTextScaleX(1.0f);
//                paint.setTextSize(_bounds.height);
//                paint.setTextEncoding(SkPaint::kUTF8_TextEncoding);
//                SkTypeface* pFace = SkTypeface::CreateFromName("宋体", SkTypeface::kNormal);
//                paint.setTypeface(pFace);

                if(!_text.empty() &&
                   _bounds.width > 0 && _bounds.height > 0)
                {
                    Render::drawText(graphics, paint, _text, _bounds, _font);
                }
            }
        }
    }
    
    Rectangle GText::bounds() const
    {
        return _bounds;
    }
    void GText::setBounds(const Rectangle& rect)
    {
        GShape::setBounds(rect);
        
        _bounds = rect;
    }
    
    void GText::operator=(const GText& value)
    {
        GShape::operator=(value);
        
        this->_text = value._text;
        this->_font = value._font;
        this->_bounds = value._bounds;
    }
    bool GText::operator==(const GText& value) const
    {
        if(!GShape::operator==(value))
            return false;
        
        return this->_text == value._text &&
        this->_font == value._font &&
        this->_bounds == value._bounds;
    }
    bool GText::operator!=(const GText& value) const
    {
        return !operator==(value);
    }
    
    string GText::text() const
    {
        return _text;
    }
    void GText::setText(const string& text)
    {
        _text = text;
    }
    
    void GText::read(XmlTextReader& reader)
    {
        if(reader.name() == "Text" && reader.nodeType() == XmlNodeType::Element)
        {
            _text = reader.getAttribute("text");
            Font::parse(reader.getAttribute("font"), _font);
            Rectangle::parse(reader.getAttribute("bounds"), _bounds);
            
            GShape::read(reader);
        }
    }
    void GText::write(XmlTextWriter& writer)
    {
        // todo: write a node.
    }
}
