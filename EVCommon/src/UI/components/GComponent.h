#ifndef EVCOMMON_GCOMPONENT_H
#define EVCOMMON_GCOMPONENT_H

#include <string>
#include "common/common_global.h"
#include "GShape.h"
#include "../utils/Image.h"
#include "../utils/Font.h"
#include "../commands/Command.h"

using namespace std;

namespace Drawing
{
    class COMMON_EXPORT GButton : public GShape
    {
    public:
        enum State
        {
            Normal               = 0,
            Highlighted          = 1,
            Disabled             = 2,
            Selected             = 3,
        };
        class Pattern
        {
        public:
            Color color;
            Image image;
            bool useColor;
            
            Pattern();
            
            static bool parse(const string& str, Pattern& pattern);
        };
        
        GButton();
        GButton(const Rectangle& rect);
        ~GButton();
        
        bool isEmpty() const override;
        string toString() const override;
        
        void paint(Graphics* graphics, const Rectangle& rcClip) override;
        
        bool canFill() const override;
        
        Rectangle bounds() const override;
        void setBounds(const Rectangle& rect) override;
        
        void read(XmlTextReader& reader) override;
        void write(XmlTextWriter& writer) override;
        
        void operator=(const GButton& value);
        bool operator==(const GButton& value) const;
        bool operator!=(const GButton& value) const;
        
    protected:
        void onClick(SkView::Click* click) override;
        bool hitTest(const Point& point) override;
        
    private:
        Pattern& getPattern(State state) const;
        void updateState(State state);
        
    protected:
        Rectangle _bounds;
        
        State _state;
        string _title;
        Font _font;
        
        Command _clickCommand;
        
        const static int PatternCount = 4;
        // 0: normal 1: highlighted 2: disabled 3: selected.
        Pattern _patterns[PatternCount];
    };
}

#endif /* EVCOMMON_GCOMPONENT_H */
