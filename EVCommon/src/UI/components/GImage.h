#ifndef GIMAGE_H
#define GIMAGE_H

#include <string>
#include "common/common_global.h"
#include "GShape.h"
#include "../utils/Image.h"

using namespace std;

namespace Drawing
{
    class COMMON_EXPORT GImage : public GShape
    {
    public:
        GImage();
        GImage(const Rectangle& rect);
//        GImage(const string& fileName);
        ~GImage();
        
        bool isEmpty() const override;
        string toString() const override;
        
        void paint(Graphics* graphics, const Rectangle& rcClip) override;
        
        bool canFill() const override;
        
        Rectangle bounds() const override;
        void setBounds(const Rectangle& rect) override;
        
        void read(XmlTextReader& reader) override;
        void write(XmlTextWriter& writer) override;
        
        void operator=(const GImage& value);
        bool operator==(const GImage& value) const;
        bool operator!=(const GImage& value) const;
        
    protected:
        Rectangle _bounds;
        Image _image;
    };
}

#endif	// GPOLYGON_H
