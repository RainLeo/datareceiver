#ifndef GPAGE_H
#define GPAGE_H

#include <string>
#include "common/common_global.h"
#include "common/data/Vector.h"
#include "GContainer.h"
#include "../configuration/PageInfo.h"
#include "../utils/Brush.h"
#include "../animation/Animation.h"

using namespace std;

namespace Drawing
{
    class Canvas;
    class COMMON_EXPORT GPage : public GContainer
    {
    public:
        GPage(PageInfo* page);
        
        bool load();
        
        bool open(Canvas* canvas);
        bool close();
        
        void setBounds(const Rectangle& rect) override;
        
        const PageInfo* pageInfo() const;
        
        Brush& background() const;
        void setBackground(const Brush& background);
        
        void addAnimation(const Animation* animation);
        
        bool transfer() const;
        void setTransfer(bool transfer);
        
    private:
        void updateCanvas(Canvas* canvas, const GShapes& shapes);

    private:
        friend Canvas;
        
        PageInfo* _pageInfo;
        bool _loaded;
        
        // include pan & zoom.
        bool _transfer;
        
        Brush _background;
        
        Animations _animations;
    };
    typedef Vector<GPage> GPages;
}

#endif	// GPAGE_H
