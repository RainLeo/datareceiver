#ifndef GTEXT_H
#define GTEXT_H

#include <string>
#include "common/common_global.h"
#include "GShape.h"
#include "../utils/Font.h"

using namespace std;

namespace Drawing
{
    class COMMON_EXPORT GText : public GShape
    {
    public:
        GText();
        GText(const Rectangle& rect);
        
        bool isEmpty() const override;
        string toString() const override;
        
        void paint(Graphics* graphics, const Rectangle& rcClip) override;
        
        bool canFill() const override;
        
        Rectangle bounds() const override;
        void setBounds(const Rectangle& rect) override;
        
        void operator=(const GText& value);
        bool operator==(const GText& value) const;
        bool operator!=(const GText& value) const;
        
        string text() const;
        void setText(const string& text);
        
        void read(XmlTextReader& reader) override;
        void write(XmlTextWriter& writer) override;
                
    protected:
        string _text;
        Font _font;
        Rectangle _bounds;
    };
}

#endif	// GTEXT_H
