#include "common/system/Convert.h"
#include "common/IO/Directory.h"
#include "common/IO/Path.h"
#include "common/IO/File.h"
#include "SkData.h"
#include "GComponent.h"
#include "../utils/SkConvert.h"
#include "../utils/Render.h"

using namespace Common;

namespace Drawing
{
    GButton::Pattern::Pattern()
    {
        useColor = true;
    }
    bool GButton::Pattern::parse(const string& str, Pattern& pattern)
    {
        // color:{A:255;R:0;G:200;B:0}
        // image:{filename:images/test3.jpg;source:file}
        StringArray values;
        Convert::splitItems(str, values, ':');
        if(values.count() == 2)
        {
            Pattern temp;
            string name = Convert::trimStr(values[0], ' ', '{', '}');
            string text = Convert::trimStartStr(values[1], ' ', '{', '}');
            text = Convert::trimEndStr(text, ' ', '{', '}');
            if(String::stringEquals(name, "image", true))
            {
                if(Image::parse(text, temp.image))
                {
                    pattern = temp;
                    pattern.useColor = false;
                    return true;
                }
            }
            else if(String::stringEquals(name, "color", true))
            {
                if(Color::parse(text, temp.color))
                {
                    pattern = temp;
                    pattern.useColor = true;
                    return true;
                }
            }
        }
        return false;
    }
    
    GButton::GButton()
    {
        _state = State::Normal;
    }
    GButton::GButton(const Rectangle& rect) : GButton()
    {
        setBounds(rect);
    }
    GButton::~GButton()
    {
    }
    
    bool GButton::isEmpty() const
    {
        return false;
    }
    string GButton::toString() const
    {
        return Convert::convertStr("[Button: Name=%s,Bounds=%s]",
                                   _name.c_str(),
                                   _bounds.toString().c_str());
    }
    
    bool GButton::canFill() const
    {
        return false;
    }
    
    void GButton::paint(Graphics* graphics, const Rectangle& rcClip)
    {
        if(!_visible)
            return;
        
        if(!_bounds.isEmpty() && _bounds.intersectsWith(rcClip) &&
           !isEmpty())
        {
            GraphicsRestore gr(graphics);
            graphics->concat(matrix());
            
            Pattern& pattern = getPattern(_state);
            if(pattern.useColor)
            {
                Brush brush(pattern.color);
                SkPaint paint;
                if(brush.toSkPaint(paint))
                {
                    graphics->drawRoundRect(SkRectConvert::convert(_bounds), 3, 3, paint);
                }
            }
            else
            {
                SkRect src = SkRect::MakeIWH(pattern.image.width(), pattern.image.height());
                graphics->drawImageRect(pattern.image.toSkImage(), src, SkRectConvert::convert(_bounds), NULL);
            }
            
            SkPaint paint;
            if(_fill.toSkPaint(paint) && _font.toSkPaint(paint))
            {
                Render::drawText(graphics, paint, _title, _bounds, _font);
            }
        }
    }
    
    Rectangle GButton::bounds() const
    {
        return _bounds;
    }
    void GButton::setBounds(const Rectangle& rect)
    {
        GShape::setBounds(rect);
        
        _bounds = rect;
    }
    
    void GButton::read(XmlTextReader& reader)
    {
        if(reader.name() == "Button" && reader.nodeType() == XmlNodeType::Element)
        {
            _title = reader.getAttribute("title");
            Font::parse(reader.getAttribute("font"), _font);
            Rectangle::parse(reader.getAttribute("bounds"), _bounds);
            Command::parse(reader.getAttribute("clickcommand"), _clickCommand);
            
            const char* patternStr[] = {"normal", "highlighted", "disabled", "selected"};
            for (uint i=0; i<PatternCount; i++)
            {
                if(Pattern::parse(reader.getAttribute(patternStr[i]), _patterns[i]))
                {
                    _patterns[i].image.setConfigSource(reader.configFile());
                }
            }
            
            GShape::read(reader);
        }
    }
    void GButton::write(XmlTextWriter& writer)
    {
        // todo: write a node.
    }
    
    void GButton::operator=(const GButton& value)
    {
        GShape::operator=(value);
        this->_bounds = value._bounds;
        this->_state = value._state;
        this->_title = value._title;
        this->_font = value._font;
        for (uint i=0; i<PatternCount; i++)
        {
            this->_patterns[i].useColor = value._patterns[i].useColor;
            this->_patterns[i].color = value._patterns[i].color;
            this->_patterns[i].image = value._patterns[i].image;
        }
    }
    bool GButton::operator==(const GButton& value) const
    {
        if(!GShape::operator==(value))
            return false;
        
        if(!(this->_bounds == value._bounds &&
             this->_state == value._state &&
             this->_title == value._title) &&
             this->_font == value._font)
            return false;
        
        for (uint i=0; i<PatternCount; i++)
        {
            if(!(this->_patterns[i].useColor == value._patterns[i].useColor &&
                 this->_patterns[i].color == value._patterns[i].color &&
                 this->_patterns[i].image == value._patterns[i].image))
                return false;
        }
        return true;
    }
    bool GButton::operator!=(const GButton& value) const
    {
        return !operator==(value);
    }
    
    GButton::Pattern& GButton::getPattern(State state) const
    {
        return (Pattern&)_patterns[state];
    }
    
    void GButton::updateState(State state)
    {
        if (state != _state)
        {
            _state = state;
            invalidate();
        }
    }
    
    void GButton::onClick(SkView::Click* click)
    {
        if(!(_enable && _visible))
            return;
        
        switch (click->fState)
        {
            case SkView::Click::kDown_State:
                if(hitTest(SkPointConvert::convert(click->fCurr)))
                {
                    setCapture(true);
                    updateState(State::Highlighted);
                }
                break;
            case SkView::Click::kMoved_State:
                if(hitTest(SkPointConvert::convert(click->fCurr)))
                {
//                    updateState(State::Selected);
                }
                else
                {
                    setCapture(false);
                    updateState(State::Normal);
                }
                break;
            case SkView::Click::kUp_State:
                if(hitTest(SkPointConvert::convert(click->fCurr)))
                {
                    updateState(State::Normal);
                    
                    _clickCommand.run(_canvas, this);
                }
                else
                {
                    updateState(State::Normal);
                }
                break;
        }
    }
    bool GButton::hitTest(const Point& point)
    {
        return _bounds.contains(point);
    }
}
