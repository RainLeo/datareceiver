#ifndef GCONTAINER_H
#define GCONTAINER_H

#include <string>
#include "common/common_global.h"
#include "GShape.h"

using namespace std;

namespace Drawing
{
    class GPage;
    class COMMON_EXPORT GContainer : public GShape
    {
    public:
        GContainer();
        
        bool isEmpty() const override;
        string toString() const override;
        
        void addShape(const GShape* shape);
        
        Rectangle bounds() const override;
        void setBounds(const Rectangle& rect) override;
        
        void paint(Graphics* graphics, const Rectangle& rcClip) override;
        
        const GShapes* shapes() const;
        
        bool canFill() const override;
        
    protected:
        void onClick(SkView::Click* click) override;
        
    protected:
        friend GPage;
        
        GShapes _shapes;
        Rectangle _bounds;
    };
}

#endif	// GCONTAINER_H
