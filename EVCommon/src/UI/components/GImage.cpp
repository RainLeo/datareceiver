#include "GImage.h"
#include "common/system/Convert.h"
#include "common/IO/Directory.h"
#include "common/IO/Path.h"
#include "common/IO/File.h"
#include "SkData.h"

using namespace Common;

namespace Drawing
{
    GImage::GImage()
    {
    }
    GImage::GImage(const Rectangle& rect) : GImage()
    {
        setBounds(rect);
    }
//    GImage::GImage(const string& fileName)
//    {
//        if(File::exists(fileName))
//        {
//            _image = Image::NewFromEncoded(SkData::NewFromFileName(fileName.c_str()));
//        }
//    }
    GImage::~GImage()
    {
    }
    
    bool GImage::isEmpty() const
    {
        return _image.isEmpty();
    }
    string GImage::toString() const
    {
        return Convert::convertStr("[GImage: Name=%s,Bounds=%s]",
                                   _name.c_str(),
                                   _bounds.toString().c_str());
    }
    
    bool GImage::canFill() const
    {
        return false;
    }
    
    void GImage::paint(Graphics* graphics, const Rectangle& rcClip)
    {
        if(!_visible)
            return;
        
        if(!_bounds.isEmpty() && _bounds.intersectsWith(rcClip) &&
           !isEmpty())
        {
            GraphicsRestore gr(graphics);
            graphics->concat(matrix());
            
            SkRect src = SkRect::MakeIWH(_image.width(), _image.height());
            graphics->drawImageRect(_image.toSkImage(), src, SkRectConvert::convert(_bounds), NULL);
        }
    }
    
    Rectangle GImage::bounds() const
    {
        return _bounds;
    }
    void GImage::setBounds(const Rectangle& rect)
    {
        GShape::setBounds(rect);
        
        _bounds = rect;
    }

    void GImage::read(XmlTextReader& reader)
    {
        if(reader.name() == "Image" && reader.nodeType() == XmlNodeType::Element)
        {
            Rectangle::parse(reader.getAttribute("bounds"), _bounds);
            if(Image::parse(reader.getAttribute("image"), _image))
            {
                _image.setConfigSource(reader.configFile());
            }
            
            GShape::read(reader);
        }
    }
    void GImage::write(XmlTextWriter& writer)
    {
        // todo: write a node.
    }
    
    void GImage::operator=(const GImage& value)
    {
        GShape::operator=(value);
        this->_bounds = value._bounds;
        this->_image = value._image;
    }
    bool GImage::operator==(const GImage& value) const
    {
        if(!GShape::operator==(value))
            return false;
        
        return this->_bounds == value._bounds &&
            this->_image == value._image;
    }
    bool GImage::operator!=(const GImage& value) const
    {
        return !operator==(value);
    }
}
