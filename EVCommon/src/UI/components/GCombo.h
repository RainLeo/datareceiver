#ifndef GCOMBO_H
#define GCOMBO_H

#include <string>
#include "common/common_global.h"
#include "GContainer.h"

using namespace std;

namespace Drawing
{
    class COMMON_EXPORT GCombo : public GContainer
    {
    public:
        GCombo();
        
        string toString() const override;
        
        void setBounds(const Rectangle& rect) override;
        
        void read(XmlTextReader& reader) override;
        void write(XmlTextWriter& writer) override;
    };
}

#endif	// GCOMBO_H
