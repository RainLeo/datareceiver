#include "Canvas.h"
#include "common/IO/Path.h"
#include "common/IO/File.h"
#include "common/system/Math.h"
#include "common/thread/Locker.h"
#include "common/diag/Trace.h"
#include "common/diag/Stopwatch.h"
#include "common/diag/Debug.h"
#include "common/system/Convert.h"
#include "../configuration/UIReader.h"
#include "../utils/Rectangle.h"
#include "../components/GCombo.h"
#include "SkWindow.h"
#include "SkGradientShader.h"

void invalidateInner(SkWindow* win);
void invalidateRectInner(SkWindow* win, const Drawing::Rectangle& rect);

namespace Drawing
{
    // defined in class Animation.
    void animation_tagValueChanged(void* owner, void* sender, EventArgs* args);
    
    void animationProc(void* parameter)
    {
        Canvas* canvas = (Canvas*)parameter;
        assert(canvas);
        canvas->animationProcInner();
    }
    
    Canvas::Canvas(SkWindow* view, const Rectangle& rect)
    {
        assert(view);
        _view = view;
        _bounds = rect;
        
        _currentPage = NULL;
        
        _animationThread = new Thread();
        _animationThread->setName("UI_animationProc");
        _animationThread->startProc(animationProc, this, 10);
        
        _services.setAutoDelete(false);
        
        _previousDistance = 0.0f;
        _pinchZoom = false;
        
        _previousPoint = Point(0, 0);
        _pinchPan = false;
        
        _zoomFactorX = DefaultZoomFactor * 1.f;
        _zoomFactorY = DefaultZoomFactor * 1.f;
        _autoScrollPosition = Point(0, 0);//(-100, -50);
    }
    Canvas::~Canvas()
    {
        closeCurrentPage();
        if(_animationThread != NULL)
        {
            _animationThread->stop();
            delete _animationThread;
            _animationThread = NULL;
        }
    }
    
    void Canvas::onPaint(Graphics* graphics, const Rectangle& rcClip)
    {
        if(_currentPage != NULL)
        {
            bool transform = false;
            if (panning())
            {
                transform = true;
                graphics->translate(-visibleBounds().x, -visibleBounds().y);
            }
            
            if (zooming())
            {
                transform = true;
                graphics->scale(zoomFactorX(), zoomFactorY());
            }
            
            Rectangle clip = transform ? translateActualRect(rcClip) : rcClip;
            _currentPage->paint(graphics, clip);
        }
    }
    void Canvas::onPaintBackground(Graphics* graphics)
    {
        bool fill = false;
        if(_currentPage != NULL)
        {
            SkPaint paint;
            Brush& background = _currentPage->background();
            if(!background.isEmpty())
            {
                if(background.toSkPaint(paint))
                {
                    fill = true;
                    graphics->drawRect(SkRectConvert::convert(bounds()), paint);
                }
            }
        }
        
        if(!fill)
        {
            // Clear background
            graphics->drawColor(SK_ColorWHITE);
        }
    }
//    void Canvas::onTouchBegan(const Touches& touches)
//    {
//        _pinchPan = false;
//        _pinchZoom = false;
//        
//        if(touches.count() == 1)
//        {
//            const Touch& touch = touches[0];
//            if(touch.tapCount == 1)
//            {
//                _previousPoint.x = touch.location.x + _autoScrollPosition.x;
//                _previousPoint.y = touch.location.y + _autoScrollPosition.y;
//                _pinchPan = true;
//                
//                onClick();
//            }
//            else if(touch.tapCount == 2)
//            {
//                _zoomFactorX = DefaultZoomFactor;
//                _zoomFactorY = DefaultZoomFactor;
//                _autoScrollPosition = Point;
//                invalidate();
//                
//                onDoubleClick();
//            }
//        }
//        else if(touches.count() == 2)
//        {
//            const Touch& touch1 = touches[0];
//            const Touch& touch2 = touches[1];
//            _previousDistance = Math::sqrt(Math::pow(touch1.location.x - touch2.location.x, 2.0f) +
//                                    Math::pow(touch1.location.y - touch2.location.y, 2.0f));
//            _pinchZoom = _previousDistance > 0.0f;
//        }
//    }
//    void Canvas::onTouchMoved(const Touches& touches)
//    {
//        if(_pinchPan && touches.count() == 1)
//        {
//            const Touch& touch = touches[0];
//            Point location = touch.location;
//            _autoScrollPosition.x = -(location.x - _previousPoint.x);
//            _autoScrollPosition.y = -(location.y - _previousPoint.y);
//            
//            invalidate();
//        }
//        else if(_pinchZoom && touches.count() == 2)
//        {
//            const Touch& touch1 = touches[0];
//            const Touch& touch2 = touches[1];
//            float distance = Math::sqrt(Math::pow(touch1.location.x - touch2.location.x, 2.0f) +
//                                  Math::pow(touch1.location.y - touch2.location.y, 2.0f));
//            float zoomFactor = (distance - _previousDistance) / _previousDistance * 100.0f;
//            _zoomFactorX = Math::abs(_zoomFactorX + zoomFactor);
//            _zoomFactorY = Math::abs(_zoomFactorY + zoomFactor);
//            if(_zoomFactorX < MinZoomFactor)
//                _zoomFactorX = MinZoomFactor;
//            if(_zoomFactorY < MinZoomFactor)
//                _zoomFactorY = MinZoomFactor;
//            if(_zoomFactorX > MaxZoomFactor)
//                _zoomFactorX = MaxZoomFactor;
//            if(_zoomFactorY > MaxZoomFactor)
//                _zoomFactorY = MaxZoomFactor;
//            
//            _autoScrollPosition.x += (distance - _previousDistance) / 2.0f;
//            _autoScrollPosition.y += (distance - _previousDistance) / 2.0f;
//            
//            _previousDistance = distance;
//            
//            invalidate();
//            
////            Debug::writeFormatLine("_zoomFactorX=%.2f, _zoomFactorY=%.2f", _zoomFactorX, _zoomFactorY);
//        }
//    }
//    void Canvas::onTouchEnded(const Touches& touches)
//    {
//        if(touches.count() != 1)
//        {
//            _pinchPan = false;
//            _previousPoint = Point;
//        }
//        if(touches.count() != 2)
//        {
//            _pinchZoom = false;
//            _previousDistance = 0.0f;
//        }
//    }
//    
//    void Canvas::onMouseDown(const Mouse& mouse)
//    {
//        
//    }
//    void Canvas::onMouseUp(const Mouse& mouse)
//    {
//        
//    }
//    void Canvas::onMouseMoved(const Mouse& mouse)
//    {
//        
//    }
//    
//    void Canvas::onKeyDown(const Key& key)
//    {
//#if DEBUG
//        if(key.key == KeyValues::F5)
//        {
//            reopenCurrentPage();
//        }
//#endif
//    }
//    void Canvas::onKeyUp(const Key& key)
//    {
//        
//    }
    
    void Canvas::onClick()
    {
        Debug::writeLine("onClick");
    }
    void Canvas::onDoubleClick()
    {
        Debug::writeLine("onDoubleClick");
    }
    
    void Canvas::invalidate()
    {
        invalidateInner(_view);
    }
    void Canvas::invalidate(const Rectangle& rect)
    {
        Rectangle rc = translateOriginalRect(rect);
        invalidateRectInner(_view, rc);
    }
    
    bool Canvas::loadPages(const ConfigFile& file)
    {
        ConfigFile temp = file;
        temp.fileName = "UI/pages.xml";
        UIReader reader(temp, &_uiInfo);
        if(!reader.Configuration::load())
        {
            Trace::writeFormatLine("Failed to load config file. name: %s", temp.fileName.c_str());
            return false;
        }
        
        const PageInfos* pages = _uiInfo.pages();
        for (uint i=0; i<_uiInfo.pageCount(); i++)
        {
            PageInfo* info = pages->at(i);
            if(info->loadedAtStartup)
            {
                GPage* page = new GPage(info);
                if(page->load())
                {
                    _pages.add(page);
                }
            }
        }
        
        for (uint i=0; i<_uiInfo.pageCount(); i++)
        {
            PageInfo* info = pages->at(i);
            if(info->mainPage)
            {
                openPage(info);
            }
        }
        return true;
    }
    
    bool Canvas::closeCurrentPage()
    {
        Locker locker(&_currentPageMutex);

        if(closePage(_currentPage))
        {
            _currentPage = nullptr;
            return true;
        }
        return false;
    }
    bool Canvas::openPage(PageInfo* info)
    {
        GPage* page = NULL;
        for (uint i=0; i<_pages.count(); i++)
        {
            GPage* temp = _pages[i];
            if(temp->name() == info->name)
            {
                page = temp;
                break;
            }
        }
        
        bool loaded = page != NULL;
		if (!loaded)
        {
            page = new GPage(info);
            if(page->load())
            {
                loaded = true;
                _pages.add(page);
            }
            else
            {
				Trace::writeFormatLine("Failed to load page file. name: %s", info->name.c_str());
                loaded = false;
            }
        }
        
        if(loaded)
        {
            if(page->open(this))
            {
                page->setBounds(bounds());
                
                Locker locker(&_currentPageMutex);
                _currentPage = page;
                bindTagAnimation(page);
                
                invalidate();
                return true;
            }
        }
        
        return false;
    }
    bool Canvas::closePage(GPage* page)
    {
        if(page != NULL)
        {
            unbindTagAnimation(page);
            
            _pages.remove(page);
            return true;
        }
        return false;
    }
    bool Canvas::reopenCurrentPage()
    {
        if(_currentPage != NULL)
        {
            const PageInfo* pi = _currentPage->pageInfo();
            closeCurrentPage();
            
            return openPage((PageInfo*)pi);
        }
        return false;
    }
    
    bool Canvas::openPage(const string& name)
    {
        if(_currentPage != nullptr &&
           String::stringEquals(_currentPage->name(), name, false))
            return false;
        
        GPage* old = _currentPage;
        const PageInfo* info = getPageInfo(name);
        if(info != nullptr)
        {
            if(openPage((PageInfo*)info))
            {
                closePage(old);
                return true;
            }
        }
        return false;
    }
    bool Canvas::closePage(const string& name)
    {
        GPage* page = nullptr;
        for (uint i=0; i<_pages.count(); i++)
        {
            GPage* temp = _pages[i];
            if(temp->name() == name)
            {
                page = temp;
                break;
            }
        }
        if(page != nullptr)
        {
            return closePage(page);
        }
        return false;
    }
    const PageInfo* Canvas::getPageInfo(const string& name) const
    {
        const PageInfos* pages = _uiInfo.pages();
        for (uint i=0; i<_uiInfo.pageCount(); i++)
        {
            const PageInfo* info = pages->at(i);
            if(String::stringEquals(info->name, name, false))
            {
                return info;
            }
        }
        return nullptr;
    }
    
    void Canvas::bindTagAnimation(GPage* page)
    {
        if(page != NULL)
        {
            TagService* ts = tagService();
            if(ts != NULL)
            {
                const GShapes* shapes = page->shapes();
                for (uint i=0; i<shapes->count(); i++)
                {
                    bindTagAnimation(shapes->at(i), ts);
                }
            }
        }
    }
    void Canvas::bindTagAnimation(GShape* shape, TagService* ts)
    {
        if(shape != nullptr)
        {
            const Animations* animations = shape->animations();
            for (uint i=0; i<animations->count(); i++)
            {
                Animation* animation = animations->at(i);
                animation->initialize(shape);
                ts->addTagValueChangedHandler(animation->expression(), Delegate(animation, animation_tagValueChanged));
                
                SwitchAnimation* sa = dynamic_cast<SwitchAnimation*>(animation);
                if(sa != nullptr)
                {
                    _switchTimer.addAnimation(sa);
                }
            }
            
            GCombo* combo = dynamic_cast<GCombo*>(shape);
            if(combo != nullptr)
            {
                const GShapes* shapes = combo->shapes();
                for (uint i=0; i<shapes->count(); i++)
                {
                    bindTagAnimation(shapes->at(i), ts);
                }
            }
        }
    }
    void Canvas::unbindTagAnimation(GPage* page)
    {
        if(page != NULL)
        {
            _switchTimer.clearAnimations();
            
            TagService* ts = tagService();
            if(ts != NULL)
            {
                const GShapes* shapes = page->shapes();
                for (uint i=0; i<shapes->count(); i++)
                {
                    unbindTagAnimation(shapes->at(i), ts);
                }
            }
        }
    }
    void Canvas::unbindTagAnimation(GShape* shape, TagService* ts)
    {
        if(shape != nullptr)
        {
            const Animations* animations = shape->animations();
            for (uint i=0; i<animations->count(); i++)
            {
                Animation* animation = animations->at(i);
                ts->removeTagValueChangedHandler(animation->expression(), Delegate(animation, animation_tagValueChanged));
            }
            
            GCombo* combo = dynamic_cast<GCombo*>(shape);
            if(combo != nullptr)
            {
                const GShapes* shapes = combo->shapes();
                for (uint i=0; i<shapes->count(); i++)
                {
                    unbindTagAnimation(shapes->at(i), ts);
                }
            }
        }
    }
    void Canvas::animationProcInner()
    {
        Locker locker(&_currentPageMutex);
        if(_currentPage != NULL)
        {
            Rectangle rect;
            const GShapes* shapes = _currentPage->shapes();
            for (uint i=0; i<shapes->count(); i++)
            {
                processAnimation(shapes->at(i), rect);
            }
            
            const Rectangle bounds = _switchTimer.process();
            if(rect.isEmpty())
                rect = bounds;
            else
                rect.unions(bounds);
            
            if(!rect.isEmpty())
            {
                //                Debug::writeFormatLine("animation invalidate rect:%s", rect.toString().c_str());
                invalidate(rect);
            }
        }
    }
    void Canvas::processAnimation(GShape* shape, Rectangle& rect)
    {
        const Animations* animations = shape->animations();
        for (uint i=0; i<animations->count(); i++)
        {
            Animation* animation = animations->at(i);
            if(animation->valueChanged())
            {
                if(rect.isEmpty())
                    rect = shape->invalidateBounds();
                else
                    rect.unions(shape->invalidateBounds());
                
                animation->execute(shape);
                
                rect.unions(shape->invalidateBounds());
                
                animation->setValueChanged(false);
            }
        }
        
        GCombo* combo = dynamic_cast<GCombo*>(shape);
        if(combo != nullptr)
        {
            const GShapes* shapes = combo->shapes();
            for (uint i=0; i<shapes->count(); i++)
            {
                processAnimation(shapes->at(i), rect);
            }
        }
    }
    
    void Canvas::addServices(const Services* services)
    {
        _services.addRange(services);
        
        TagService* ts = tagService();
        if(ts != NULL)
            Animation::setTagService(ts);
    }
    TagService* Canvas::tagService()
    {
        return getService<TagService>();
    }
    template<class T>
    T* Canvas::getService()
    {
        for (uint j=0; j<_services.count(); j++)
        {
            T* service = dynamic_cast<T*>(_services.at(j));
            if(service != NULL)
            {
                return service;
            }
        }
        return NULL;
    }
    
    float Canvas::width() const
    {
        return _bounds.width;
    }
    float Canvas::height() const
    {
        return _bounds.height;
    }
    const Rectangle& Canvas::bounds() const
    {
        return _bounds;
    }
    void Canvas::setBounds(const Rectangle& rect)
    {
        _bounds = rect;
        if(_currentPage != NULL)
        {
            _currentPage->setBounds(rect);
        }
    }
    
    void Canvas::onClick(SkView::Click* click)
    {
        if(_currentPage != NULL)
        {
            _currentPage->onClick(click);
        }
    }
    
    bool Canvas::transfer() const
    {
        return _currentPage != nullptr ? _currentPage->transfer() : false;
    }
    
    bool Canvas::panning() const
    {
        return !(visibleBounds().x == 0.0f && visibleBounds().y == 0.0f);
    }
    bool Canvas::zooming() const
    {
        return _zoomFactorX != DefaultZoomFactor || _zoomFactorY != DefaultZoomFactor;
    }
    const Rectangle Canvas::visibleBounds() const
    {
        return Rectangle(_autoScrollPosition.x, _autoScrollPosition.y, width(), height());
    }
    float Canvas::zoomFactorX() const
    {
        return (float)_zoomFactorX / (float)DefaultZoomFactor;
    }
    float Canvas::zoomFactorY() const
    {
        return (float)_zoomFactorY / (float)DefaultZoomFactor;
    }
    const Point Canvas::translateOriginalLocation(const Point& point) const
    {
        Point pt = point;
        if (zooming())
        {
            float scaleX = zoomFactorX() / 1.0f;
            float scaleY = zoomFactorY() / 1.0f;
            pt = Point(Math::round(pt.x * scaleX), Math::round(pt.y * scaleY));
        }
        if (panning())
        {
            pt.offset(-visibleBounds().x, -visibleBounds().y);
        }
        
        return pt;
    }
    const Rectangle Canvas::translateOriginalRect(const Rectangle& rect) const
    {
        if (panning() || zooming())
        {
            Point topLeft = translateOriginalLocation(Point(rect.x, rect.y));
            Point downRight = translateOriginalLocation(Point(rect.right(), rect.bottom()));
            
            return Rectangle(topLeft.x, topLeft.y, downRight.x - topLeft.x, downRight.y - topLeft.y);
        }
        else
        {
            return rect;
        }
    }
    // Translate the actual location, from actual coordinate to original coordinate.
    const Point Canvas::translateActualLocation(const Point& point) const
    {
        Point pt = point;
        if (zooming())
        {
            float scaleX = 1.0f / zoomFactorX();
            float scaleY = 1.0f / zoomFactorY();
            pt = Point(Math::round(pt.x * scaleX), Math::round(pt.y * scaleY));
        }
        if (panning())
        {
            pt.offset(visibleBounds().x, visibleBounds().y);
        }
        
        return pt;
    }
    
    // Translate the actual bounds, from actual coordinate to original coordinate.
    const Rectangle Canvas::translateActualRect(const Rectangle& rect) const
    {
        if (panning() || zooming())
        {
            Point topLeft = translateActualLocation(Point(rect.x, rect.y));
            Point downRight = translateActualLocation(Point(rect.right(), rect.bottom()));
            
            return Rectangle(topLeft.x, topLeft.y, downRight.x - topLeft.x, downRight.y - topLeft.y);
        }
        else
        {
            return rect;
        }
    }

//    const Point Canvas::translateLocation(const SkMatrix& matrix, const Point& point)
//    {
//        Point pt = point;
//        if (matrix.isScaleTranslate())
//        {
//            pt = Point(Math::round(pt.x * matrix.m11), Math::round(pt.y * matrix.m22));
//        }
//        if (matrix.isTranslated())
//        {
//            pt.x += matrix.dx;
//            pt.y += matrix.dy;
//        }
//        
//        return pt;
//    }
//
//    const Rectangle Canvas::translateRectangle(const SkMatrix& matrix, const Rectangle& rect)
//    {
//        if (matrix.isTranslated() || matrix.isScaled())
//        {
//            Point topLeft = translateLocation(matrix, rect.location());
//            Point downRight = translateLocation(matrix, Point(rect.x + rect.width, rect.y + rect.height));
//            
//            return Rectangle(topLeft.x,
//                             topLeft.y,
//                             Math::abs(downRight.x - topLeft.x), Math::abs(downRight.y - topLeft.y));
//        }
//        else
//        {
//            return rect;
//        }
//    }
}
