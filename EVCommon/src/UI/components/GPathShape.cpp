﻿#include "GPathShape.h"
#include "common/system/Convert.h"

using namespace Common;

namespace Drawing
{
    GPathShape::GPathShape() : GShape()
    {
    }
    GPathShape::GPathShape(const Rectangle& rect) : GShape()
    {
    }
    GPathShape::~GPathShape()
    {
    }
    
    bool GPathShape::isEmpty() const
    {
        return this->_path.isEmpty();
    }
    string GPathShape::toString() const
    {
        return Convert::convertStr("[%s: Name=%s,Paint=%s,Path=%s]",
                                   typeid(this).name(),
                                   _name.c_str(),
                                   SkPathConvert::toString(_path).c_str());
    }
    
    bool GPathShape::canFill() const
    {
        return _path.countPoints() > 2;
    }
    
    const GraphicsPath& GPathShape::path() const
    {
        return _path;
    }
    
    void GPathShape::paint(Graphics* graphics, const Rectangle& rcClip)
    {
        if(!_visible)
            return;
        
        if(!_path.isEmpty() && bounds().intersectsWith(rcClip))
        {
            GraphicsRestore gr(graphics);
            graphics->concat(matrix());
            
            if(canFill())
            {
                SkPaint paint;
                if(_fill.toSkPaint(paint))
                {
                    graphics->drawPath(_path, paint);
                }
            }
            
            if(canStroke())
            {
                SkPaint paint;
                if(_stroke.toSkPaint(paint))
                {
                    graphics->drawPath(_path, paint);
                }
            }
        }
    }
    
    Rectangle GPathShape::bounds() const
    {
        return SkRectConvert::convert(_path.getBounds());
    }
    void GPathShape::setBounds(const Rectangle& rect)
    {
        GShape::setBounds(rect);
        
        if(_path.countPoints() > 2)
        {
            if(rect.width == 0 || rect.height == 0)
                return;
            
            Rectangle old = bounds();
            if(old.width == 0 || old.height == 0)
                return;

            GraphicsPath temp = _path;
            float scaleW = rect.width / old.width;
            float scaleH = rect.height / old.height;

            _path.reset();
            const SkPoint& pt1 = temp.getPoint(0);
            float offsetX = rect.x + (pt1.x() - old.x);
            float offsetY = rect.y + (pt1.y() - old.y);
            _path.moveTo(offsetX * scaleW, offsetY * scaleH);
            for (int i=1; i<temp.countPoints(); i++)
            {
                const SkPoint& pt = temp.getPoint(i);
                offsetX = rect.x + (pt.x() - old.x);
                offsetY = rect.y + (pt.y() - old.y);
                _path.lineTo(offsetX * scaleW, offsetY * scaleH);
            }
            
            GraphicsPath::Iter iter(temp, false);
            if(iter.isClosedContour())
            {
                _path.close();
            }
        }
    }
    
    void GPathShape::operator=(const GPathShape& value)
    {
        GShape::operator=(value);
        this->_path = value._path;
    }
    bool GPathShape::operator==(const GPathShape& value) const
    {
        if(!GShape::operator==(value))
            return false;
        
        return this->_path == value._path;
    }
    bool GPathShape::operator!=(const GPathShape& value) const
    {
        return !operator==(value);
    }
    
    GPolygon::GPolygon()
    {
    }
    GPolygon::GPolygon(const Rectangle& rect) : GPathShape(rect)
    {
    }
    
    void GPolygon::read(XmlTextReader& reader)
    {
        if(reader.name() == "Polygon" && reader.nodeType() == XmlNodeType::Element)
        {
            SkPoints points;
            if(SkPointConvert::parse(reader.getAttribute("points"), points))
            {
                if(points.count() > 2)
                {
                    _path.moveTo(points[0]);
                    for (uint i=1; i<points.count(); i++)
                    {
                        _path.lineTo(points[i]);
                    }
                    _path.close();
                }
            }
            
            GPathShape::read(reader);
        }
    }
    void GPolygon::write(XmlTextWriter& writer)
    {
        // todo: write a node.
    }
    
    GPolyline::GPolyline()
    {
    }
    GPolyline::GPolyline(const Rectangle& rect) : GPathShape(rect)
    {
    }
    
    void GPolyline::read(XmlTextReader& reader)
    {
        if(reader.name() == "Polyline" && reader.nodeType() == XmlNodeType::Element)
        {
            SkPoints points;
            if(SkPointConvert::parse(reader.getAttribute("points"), points))
            {
                if(points.count() > 2)
                {
                    _path.moveTo(points[0]);
                    for (uint i=1; i<points.count(); i++)
                    {
                        _path.lineTo(points[i]);
                    }
                }
            }
            
            GPathShape::read(reader);
        }
    }
    void GPolyline::write(XmlTextWriter& writer)
    {
        // todo: write a node.
    }
    
    GLine::GLine() : GPathShape()
    {
    }
    GLine::GLine(const Rectangle& rect) : GPathShape(rect)
    {
        setBounds(rect);
    }
    GLine::GLine(const Point& start, const Point& end)
    {
        addLine(start, end);
    }
    
    bool GLine::isVertical() const
    {
        if(_path.countPoints() == 2)
        {
            return _path.getPoint(0).x() == _path.getPoint(1).x();
        }
        return false;
    }
    bool GLine::isHorzontal() const
    {
        if(_path.countPoints() == 2)
        {
            return _path.getPoint(0).y() == _path.getPoint(1).y();
        }
        return false;
    }
    
    void GLine::setBounds(const Rectangle& rect)
    {
        GShape::setBounds(rect);
        
        if(_path.countPoints() == 2)
        {
            float offsetX, offsetY;
            Rectangle old = bounds();
            if(isVertical())
            {
                assert(old.height != 0);
                if(rect.height == 0 || old.height == 0)
                    return;
            }
            else if(isHorzontal())
            {
                assert(old.width != 0);
                if(rect.width == 0 || old.width == 0)
                    return;
            }
            else
            {
                if(rect.width == 0 || rect.height == 0)
                    return;
                
                if(old.width == 0 || old.height == 0)
                    return;
            }
            
            GraphicsPath temp = _path;
            float scaleW = isVertical() ? 1 : rect.width / old.width;
            float scaleH = isHorzontal() ? 1 : rect.height / old.height;
            
            _path.reset();
            const SkPoint& pt1 = temp.getPoint(0);
            offsetX = rect.x + (pt1.x() - old.x);
            offsetY = rect.y + (pt1.y() - old.y);
            _path.moveTo(offsetX * scaleW, offsetY * scaleH);
            const SkPoint& pt2 = temp.getPoint(1);
            offsetX = rect.x + (pt2.x() - old.x);
            offsetY = rect.y + (pt2.y() - old.y);
            _path.lineTo(offsetX * scaleW, offsetY * scaleH);
        }
    }
    
    void GLine::read(XmlTextReader& reader)
    {
        if(reader.name() == "Line" && reader.nodeType() == XmlNodeType::Element)
        {
            Point start, end;
            if(Point::parse(reader.getAttribute("start"), start) &&
               Point::parse(reader.getAttribute("end"), end))
            {
                addLine(start, end);
            }
            
            GPathShape::read(reader);
        }
    }
    void GLine::write(XmlTextWriter& writer)
    {
        // todo: write a node.
    }
    
    void GLine::addLine(const Point& start, const Point& end)
    {
        _path.reset();
        _path.moveTo(start.x, start.y);
        _path.lineTo(end.x, end.y);
    }
    
    GEllipse::GEllipse() : GPathShape()
    {
        setBounds(Rectangle());
    }
    GEllipse::GEllipse(const Rectangle& rect) : GPathShape(rect)
    {
        setBounds(rect);
    }
    
    void GEllipse::setBounds(const Rectangle& rect)
    {
        GShape::setBounds(rect);
        
        _path.reset();
        _path.addOval(SkRectConvert::convert(rect));
    }
    
    void GEllipse::read(XmlTextReader& reader)
    {
        if(reader.name() == "Ellipse" && reader.nodeType() == XmlNodeType::Element)
        {
            Rectangle rect;
            if(Rectangle::parse(reader.getAttribute("bounds"), rect))
            {
                setBounds(rect);
            }
            
            GPathShape::read(reader);
        }
    }
    void GEllipse::write(XmlTextWriter& writer)
    {
        // todo: write a node.
    }
    
    GRectangle::GRectangle() : GPathShape()
    {
        setBounds(Rectangle());
    }
    GRectangle::GRectangle(const Rectangle& rect) : GPathShape(rect)
    {
        setBounds(rect);
    }
    
    void GRectangle::setBounds(const Rectangle& rect)
    {
        GShape::setBounds(rect);
        
        _path.reset();
        _path.addRect(SkRectConvert::convert(rect));
    }
    
    void GRectangle::read(XmlTextReader& reader)
    {
        if(reader.name() == "Rectangle" && reader.nodeType() == XmlNodeType::Element)
        {
            Rectangle rect;
            if(Rectangle::parse(reader.getAttribute("bounds"), rect))
            {
                setBounds(rect);
            }
            
            GPathShape::read(reader);
        }
    }
    void GRectangle::write(XmlTextWriter& writer)
    {
        // todo: write a node.
    }
    
    GRoundRectangle::GRoundRectangle() : GRectangle()
    {
    }
    GRoundRectangle::GRoundRectangle(const Rectangle& rect, float cornerWidth, float cornerHeight) : GRectangle(rect)
    {
        _cornerWidth = cornerWidth;
        _cornerHeight = cornerHeight;
    }
    
    void GRoundRectangle::setBounds(const Rectangle& rect)
    {
        GShape::setBounds(rect);
        
        _path.reset();
        _path.addRoundRect(SkRectConvert::convert(rect), _cornerWidth, _cornerHeight);
    }
    
    void GRoundRectangle::read(XmlTextReader& reader)
    {
        if(reader.name() == "RoundRectangle" && reader.nodeType() == XmlNodeType::Element)
        {
            Rectangle rect;
            if(Rectangle::parse(reader.getAttribute("bounds"), rect))
            {
                Convert::parseSingle(reader.getAttribute("cornerWidth"), _cornerWidth);
                Convert::parseSingle(reader.getAttribute("cornerHeight"), _cornerHeight);
                
                setBounds(rect);
            }
            
            GPathShape::read(reader);
        }
    }
    void GRoundRectangle::write(XmlTextWriter& writer)
    {
        // todo: write a node.
    }
    
    GCurve::GCurve()
    {
    }
    GCurve::GCurve(const Rectangle& rect) : GPathShape(rect)
    {
    }
    
    void GCurve::read(XmlTextReader& reader)
    {
        if(reader.name() == "Curve" && reader.nodeType() == XmlNodeType::Element)
        {
            SkPoints points;
            if(SkPointConvert::parse(reader.getAttribute("points"), points))
            {
                if(points.count() > 3)
                {
                    // The points parameter specifies an array of endpoints and control points of the connected curves.
                    // The first curve is constructed from the first point to the fourth point in the points array by
                    // using the second and third points as control points. In addition to the endpoint of the previous curve,
                    // each subsequent curve in the sequence needs exactly three more points: the next two points in the sequence
                    // are control points, and the third is the endpoint for the added curve.
                    _path.moveTo(points[0]);
                    for (uint i=1; i<points.count(); i+=3)
                    {
                        _path.cubicTo(points[i], points[i+1], points[i+2]);
                    }
                    _path.close();
                }
            }
            
            GPathShape::read(reader);
        }
    }
    void GCurve::write(XmlTextWriter& writer)
    {
        // todo: write a node.
    }
}
