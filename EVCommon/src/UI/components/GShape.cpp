﻿#include "GShape.h"
#include "Canvas.h"
#include "common/system/Convert.h"
#include "../components/GText.h"
#include "../components/GPathShape.h"
#include "../components/GImage.h"
#include "../components/GCombo.h"
#include "../components/GComponent.h"

using namespace Common;

namespace Drawing
{
    GShape* GShape::_captureShape = nullptr;
    
    GShape::GShape() : _visible(true), _enable(true), _center(Center::MiddleCenter),
        _angle(0), _skewX(0), _skewY(0), _isMirrorX(false), _isMirrorY(false)
    {
        _matrix.reset();
    }
    GShape::~GShape()
    {
		_animations.clear();
		_paintedDelegates.clear();
    }

    bool GShape::canStroke() const
    {
        return true;
    }
    
    void GShape::setBounds(const Rectangle& rect)
    {
        // update fill.
        _fill.update(rect);
    }
    Rectangle GShape::invalidateBounds() const
    {
        Rectangle rect = bounds();
        rect.inflate(5.0f, 5.0f);
        return rect;
    }
    float GShape::x() const
    {
        return bounds().x;
    }
    float GShape::y() const
    {
        return bounds().y;
    }
    float GShape::width() const
    {
        return bounds().width;
    }
    float GShape::height() const
    {
        return bounds().height;
    }
    Point GShape::location() const
    {
        Rectangle rect = bounds();
        return Point(rect.x, rect.y);
    }
    Point GShape::center() const
    {
        Rectangle rect = bounds();
        switch (_center)
        {
            case Center::TopCenter:
                return Point(rect.x + rect.width / 2.0f, rect.y);
            case Center::TopLeft:
                return Point(rect.x, rect.y);
            case Center::TopRight:
                return Point(rect.bottom(), rect.y);
            case Center::MiddleLeft:
                return Point(rect.x, rect.y + rect.height / 2.0f);
            case Center::MiddleRight:
                return Point(rect.right(), rect.y + rect.height / 2.0f);
            case Center::BottomLeft:
                return Point(rect.x, rect.bottom());
            case Center::BottomCenter:
                return Point(rect.x + rect.width / 2.0f, rect.bottom());
            case Center::BottomRight:
                return Point(rect.right(), rect.bottom());
            case Center::Custom:
                return _centerPoint;
            default:
            case Center::MiddleCenter:
                return Point(rect.x + rect.width / 2.0f, rect.y + rect.height / 2.0f);
        }
    }
    Size GShape::size() const
    {
        Rectangle rect = bounds();
        return Size(rect.width, rect.height);
    }
    
    void GShape::setX(float x)
    {
        Rectangle rect = bounds();
        setBounds(Rectangle(x, rect.y, rect.width, rect.height));
    }
    void GShape::setY(float y)
    {
        Rectangle rect = bounds();
        setBounds(Rectangle(rect.x, y, rect.width, rect.height));
    }
    void GShape::setWidth(float width)
    {
        Rectangle rect = bounds();
        setBounds(Rectangle(rect.x, rect.y, width, rect.height));
    }
    void GShape::setheight(float height)
    {
        Rectangle rect = bounds();
        setBounds(Rectangle(rect.x, rect.y, rect.width, height));
    }
    void GShape::setLocation(const Point& point)
    {
        Rectangle rect = bounds();
        setBounds(Rectangle(point.x, point.y, rect.width, rect.height));
    }
    void GShape::setSize(const Size& size)
    {
        Rectangle rect = bounds();
        setBounds(Rectangle(rect.x, rect.y, size.width, size.height));
    }

	void GShape::operator=(const GShape& value)
	{
        this->_name = value._name;
        this->_visible = value._visible;
        this->_enable = value._enable;
        
        this->_angle = value._angle;
        this->_skewX = value._skewX;
        this->_skewY = value._skewY;
        this->_isMirrorX = value._isMirrorX;
        this->_isMirrorY = value._isMirrorY;
        
        setStroke(value._stroke);
        setFill(value._fill);
	}
	bool GShape::operator==(const GShape& value) const
	{
		if(_name != value._name)
            return false;
        if(_visible != value._visible)
            return false;
        if(_enable != value._enable)
            return false;
        
        if(_stroke != value._stroke)
            return false;
        if(_fill != value._fill)
            return false;
        
        if(_angle != value._angle)
            return false;
        if(_skewX != value._skewX)
            return false;
        if(_skewY != value._skewY)
            return false;
        if(_isMirrorX != value._isMirrorX)
            return false;
        if(_isMirrorY != value._isMirrorY)
            return false;
        
        return true;
	}
	bool GShape::operator!=(const GShape& value) const
	{
		return !operator==(value);
	}
    
    void GShape::setName(const string& name)
    {
        _name = name;
    }
    const string& GShape::name() const
    {
        return _name;
    }
    void GShape::setVisible(const bool visible)
    {
        _visible = visible;
    }
    const bool GShape::visible() const
    {
        return _visible;
    }
    
    void GShape::setAngle(const float angle)
    {
        if(angle != _angle &&
           angle >= 0.0f && angle < 360.0f)
        {
            _angle = angle;
            
            updateMatrix();
        }
    }
    const float GShape::angle() const
    {
        return _angle;
    }
    
    const Matrix& GShape::matrix() const
    {
        return _matrix;
    }
    void GShape::updateMatrix()
    {
        _matrix.reset();
        
        Point center = this->center();
        if (_skewX != 0.0f || _skewY != 0.0f)
        {
            _matrix.postTranslate(-center.x, -center.y);
            _matrix.postSkew(_skewX, _skewY);
            _matrix.postTranslate(center.x, center.y);
        }
        
        if(_angle != 0.0f)
        {
            _matrix.postRotate(_angle, center.x, center.y);
        }
        
        if(_isMirrorX || _isMirrorY)
        {
            _matrix.postTranslate(-center.x, -center.y);
            _matrix.postScale(_isMirrorX ? -1.0f : 1.0f, _isMirrorY ? -1.0f : 1.0f);
            _matrix.postTranslate(center.x, center.y);
        }
    }
    
    void GShape::setStroke(const Pen& pen)
    {
        if(pen != _stroke)
        {
            _stroke = pen;
        }
    }
    const Pen& GShape::stroke() const
    {
        return _stroke;
    }
    void GShape::setFill(const Brush& brush)
    {
        if(brush != _fill)
        {
            _fill = brush;
            _fill.update(bounds());
        }
    }
    const Brush& GShape::fill() const
    {
        return _fill;
    }
    
    void GShape::addPaintedHandler(const Delegate& delegate)
    {
        _paintedDelegates.add(delegate);
    }
    void GShape::removePaintedHandler(const Delegate& delegate)
    {
        _paintedDelegates.remove(delegate);
    }
    void GShape::painted(Graphics* graphics, const Rectangle& rcClip)
    {
        PaintedEventArgs args(graphics, rcClip);
        _paintedDelegates.invoke(this, &args);
    }
    
    void GShape::read(XmlTextReader& reader)
    {
        if (reader.nodeType() == XmlNodeType::Element)
        {
            string nodeName = reader.name();
            readProperties(reader);
            
            // It have no animations if no child.
            if(!reader.isEmptyElement())
            {
                while (reader.read() &&
                       !(reader.name() == nodeName && reader.nodeType() == XmlNodeType::EndElement))
                {
                    readAnimations(reader);
                }
            }
        }
    }
    void GShape::readProperties(XmlTextReader& reader)
    {
        if (reader.nodeType() == XmlNodeType::Element)
        {
            _name = reader.getAttribute("name");
            
            Convert::parseBoolean(reader.getAttribute("enable"), _enable);
            Convert::parseBoolean(reader.getAttribute("visible"), _visible);
            
            _center = parseCenter(reader.getAttribute("center"), _centerPoint);
            Convert::parseSingle(reader.getAttribute("angle"), _angle);
            Convert::parseSingle(reader.getAttribute("skewX"), _skewX);
            Convert::parseSingle(reader.getAttribute("skewY"), _skewY);
            Convert::parseBoolean(reader.getAttribute("isMirrorX"), _isMirrorX);
            Convert::parseBoolean(reader.getAttribute("isMirrorY"), _isMirrorY);
            
            Pen stroke;
            if(Pen::parse(reader.getAttribute("stroke"), stroke))
            {
                setStroke(stroke);
            }
            Brush fill;
            if(Brush::parse(reader.getAttribute("fill"), fill))
            {
                fill.setConfigSource(reader.configFile());
                setFill(fill);
            }
        }
    }
    void GShape::readAnimations(XmlTextReader& reader)
    {
        if (reader.name() == "Animations" && reader.nodeType() == XmlNodeType::Element)
        {
            while (reader.read() &&
                   !(reader.name() == "Animations" && reader.nodeType() == XmlNodeType::EndElement))
            {
                if (reader.nodeType() == XmlNodeType::Element)
                {
                    Animation* animation = Animation::createAnimation(reader.name());
                    if(animation != NULL)
                    {
                        animation->read(reader);
                        _animations.add(animation);
                    }
                }
            }
        }
    }

    void GShape::write(XmlTextWriter& writer)
    {
        // todo: write a node.
    }

    const Animations* GShape::animations() const
    {
        return &_animations;
    }
    
    void GShape::invalidate()
    {
        if(_canvas != nullptr)
        {
            _canvas->invalidate(invalidateBounds());
        }
    }
    
    void GShape::onClick(SkView::Click* click)
    {
    }
    
    bool GShape::hitTest(const Point& point)
    {
        return false;
    }
    
    bool GShape::capture() const
    {
        return _captureShape == this;
    }
    void GShape::setCapture(bool capture)
    {
        _captureShape = capture ? this : nullptr;
    }
    
    GShape::Center GShape::parseCenter(const string& value, Point& point)
    {
        if(String::stringEquals(value, "topLeft", true))
        {
            return Center::TopLeft;
        }
        else if(String::stringEquals(value, "topCenter", true))
        {
            return Center::TopCenter;
        }
        else if(String::stringEquals(value, "topRight", true))
        {
            return Center::TopRight;
        }
        else if(String::stringEquals(value, "middleLeft", true))
        {
            return Center::MiddleLeft;
        }
        else if(String::stringEquals(value, "middleCenter", true))
        {
            return Center::MiddleCenter;
        }
        else if(String::stringEquals(value, "middleRight", true))
        {
            return Center::MiddleRight;
        }
        else if(String::stringEquals(value, "bottomLeft", true))
        {
            return Center::BottomLeft;
        }
        else if(String::stringEquals(value, "bottomCenter", true))
        {
            return Center::BottomCenter;
        }
        else if(String::stringEquals(value, "bottomRight", true))
        {
            return Center::BottomRight;
        }
        else if(Point::parse(value, point))
        {
            return Center::Custom;
        }
        return Center::MiddleCenter;
    }
    string GShape::convertCenterStr(Center value, const Point& point)
    {
        switch (value)
        {
            case Center::TopLeft:
                return "topLeft";
            case Center::TopCenter:
                return "topCenter";
            case Center::TopRight:
                return "topRight";
            case Center::MiddleLeft:
                return "middleLeft";
            case Center::MiddleCenter:
                return "middleCenter";
            case Center::MiddleRight:
                return "middleRight";
            case Center::BottomLeft:
                return "bottomLeft";
            case Center::BottomCenter:
                return "bottomCenter";
            case Center::BottomRight:
                return "bottomRight";
            case Center::Custom:
                return point.toString();
            default:
                return "middleCenter";
        }
    }
    
    GShape* GShape::createShape(const string& nodeName)
    {
        GShape* shape = NULL;
        if(nodeName == "Rectangle")
        {
            shape = new GRectangle();
        }
        else if(nodeName == "RoundRectangle")
        {
            shape = new GRoundRectangle();
        }
        else if(nodeName == "Text")
        {
            shape = new GText();
        }
        else if(nodeName == "Line")
        {
            shape = new GLine();
        }
        else if(nodeName == "Ellipse")
        {
            shape = new GEllipse();
        }
        else if(nodeName == "Polygon")
        {
            shape = new GPolygon();
        }
        else if(nodeName == "Polyline")
        {
            shape = new GPolyline();
        }
        else if(nodeName == "Image")
        {
            shape = new GImage();
        }
        else if(nodeName == "Combo")
        {
            shape = new GCombo();
        }
        else if(nodeName == "Curve")
        {
            shape = new GCurve();
        }
        else if(nodeName == "Button")
        {
            shape = new GButton();
        }
        return shape;
    }
}
