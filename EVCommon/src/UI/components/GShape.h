#ifndef GSHAPE_H
#define GSHAPE_H

#include <string>
#include "common/common_global.h"
#include "common/data/Vector.h"
#include "common/xml/XmlTextReader.h"
#include "common/xml/XmlTextWriter.h"
#include "../animation/Animation.h"
#include "../utils/SkConvert.h"
#include "../utils/Pen.h"
#include "../utils/Brush.h"
#include "../utils/Rectangle.h"
#include "../utils/Point.h"
#include "../utils/Size.h"
#include "../utils/Graphics.h"
#include "../utils/Matrix.h"
#include "SkView.h"

using namespace std;
using namespace Common;

namespace Drawing
{
    class PaintedEventArgs : public EventArgs
    {
    public:
        PaintedEventArgs(Graphics* graphics, const Rectangle& rcClip)
        {
            this->graphics = graphics;
            this->rcClip = rcClip;
        }
        
    public:
        Graphics* graphics;
        Rectangle rcClip;
    };
    
    class GPage;
    class Canvas;
    class GCombo;
    class GContainer;
    class PageReader;
    class COMMON_EXPORT GShape
    {
    public:
        enum Center {
            TopLeft = 0x001,
            TopCenter = 0x002,
            TopRight = 0x004,
            MiddleLeft = 0x010,
            MiddleCenter = 0x020,
            MiddleRight = 0x040,
            BottomLeft = 0x100,
            BottomCenter = 0x200,
            BottomRight = 0x400,
            Custom = 0x800,
        };
        
    public:
        GShape();
        virtual ~GShape();
        
        virtual bool isEmpty() const = 0;
        virtual string toString() const = 0;
        
        virtual void paint(Graphics* graphics, const Rectangle& rcClip) = 0;
        
        void addPaintedHandler(const Delegate& delegate);
        void removePaintedHandler(const Delegate& delegate);
        
        virtual Rectangle bounds() const = 0;
        virtual void setBounds(const Rectangle& rect);
        Rectangle invalidateBounds() const;
        
        float x() const;
        float y() const;
        float width() const;
        float height() const;
        Point location() const;
        Point center() const;
        Size size() const;
        
        void setX(float x);
        void setY(float y);
        void setWidth(float width);
        void setheight(float height);
        void setLocation(const Point& point);
        void setSize(const Size& size);
        
        void operator=(const GShape& value);
        bool operator==(const GShape& value) const;
        bool operator!=(const GShape& value) const;
        
        virtual bool canFill() const = 0;
        virtual bool canStroke() const;

        void setName(const string& name);
        const string& name() const;
        
        void setVisible(const bool visible);
        const bool visible() const;
        
        void setAngle(const float angle);
        const float angle() const;
        
        const Matrix& matrix() const;
        
        void setStroke(const Pen& pen);
        const Pen& stroke() const;
        void setFill(const Brush& brush);
        const Brush& fill() const;
        
        virtual void read(XmlTextReader& reader);
        virtual void write(XmlTextWriter& writer);
        
        const Animations* animations() const;
        
    public:
        static GShape* createShape(const string& nodeName);
        
        static Center parseCenter(const string& value, Point& point);
        static string convertCenterStr(Center value, const Point& point);
        
    protected:
        void painted(Graphics* graphics, const Rectangle& rcClip);
        
        void updateMatrix();
        
        void readProperties(XmlTextReader& reader);
        void readAnimations(XmlTextReader& reader);
        
        void invalidate();
        
        virtual void onClick(SkView::Click* click);
        virtual bool hitTest(const Point& point);
        
        bool capture() const;
        void setCapture(bool capture);
        
    protected:
        friend GCombo;
        friend GPage;
        friend GContainer;
        friend PageReader;
        
        string _name;
        bool _visible;
        bool _enable;
        
        Pen _stroke;
        Brush _fill;
        
        Canvas* _canvas;
        
        // from 0 to 360.
        Point _centerPoint;
        Center _center;
        float _angle;
        float _skewX;
        float _skewY;
        bool _isMirrorX;
        bool _isMirrorY;
        
        Matrix _matrix;
        
        Animations _animations;
        
        Delegates _paintedDelegates;
        
    private:
        static GShape* _captureShape;
    };
    typedef Vector<GShape> GShapes;
}

#endif	// GSHAPE_H
