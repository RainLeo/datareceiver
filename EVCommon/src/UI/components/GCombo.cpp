#include "GCombo.h"
#include "common/system/Convert.h"

using namespace Common;

namespace Drawing
{
    GCombo::GCombo()
    {
    }
    
    string GCombo::toString() const
    {
        return Convert::convertStr("[GCombo: Name=%s,Count=%d]", _name.c_str(), _shapes.count());
    }
    
    void GCombo::setBounds(const Rectangle& rect)
    {
        const Rectangle old = _bounds;
        if(rect != old)
        {
            GContainer::setBounds(rect);
            
            if(rect.width == 0 || rect.height == 0)
                return;
            if(old.width == 0 || old.height == 0)
                return;
            
            // set combo bounds.
            float scaleW = rect.width / old.width;
            float scaleH = rect.height / old.height;
            
            for (uint i=0; i<_shapes.count(); i++)
            {
                GShape* shape = _shapes[i];
                const Rectangle& shapeOld = shape->bounds();
                Rectangle shapeNew(rect.x + (shapeOld.x - old.x),
                                   rect.y + (shapeOld.y - old.y),
                                   shapeOld.width * scaleW,
                                   shapeOld.height * scaleH);
                shape->setBounds(shapeNew);
            }
        }
    }
    
    void GCombo::read(XmlTextReader& reader)
    {
        if(reader.name() == "Combo" && reader.nodeType() == XmlNodeType::Element)
        {
            GShape::readProperties(reader);
            
            Rectangle::parse(reader.getAttribute("bounds"), _bounds);
            
            while (reader.read() &&
                   !(reader.name() == "Combo" && reader.nodeType() == XmlNodeType::EndElement))
            {
                if (reader.name() == "Shapes" && reader.nodeType() == XmlNodeType::Element)
                {
                    while (reader.read() &&
                           !(reader.name() == "Shapes" && reader.nodeType() == XmlNodeType::EndElement))
                    {
                        if (reader.nodeType() == XmlNodeType::Element)
                        {
                            GShape* shape = GShape::createShape(reader.name());
                            if(shape != NULL)
                            {
                                shape->read(reader);
                                Point location = Point(GShape::x()+shape->x(), GShape::y()+shape->y());
                                shape->setLocation(location);
                                shape->updateMatrix();
                                _shapes.add(shape);
                            }
                        }
                    }
                }
                else if (reader.name() == "Animations" && reader.nodeType() == XmlNodeType::Element)
                {
                     readAnimations(reader);
                }
            }
        }
    }
    void GCombo::write(XmlTextWriter& writer)
    {
        // todo: write a node.
    }
    
//    void GCombo::operator=(const GCombo& value)
//    {
//        GShape::operator=(value);
//        this->_bounds = value._bounds;
//    }
//    bool GCombo::operator==(const GCombo& value) const
//    {
//        if(!GShape::operator==(value))
//            return false;
//        
//        return this->_bounds == value._bounds;
//    }
//    bool GCombo::operator!=(const GCombo& value) const
//    {
//        return !operator==(value);
//    }
}
