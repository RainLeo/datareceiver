#ifndef COMMAND_H
#define COMMAND_H

#include <string>
#include "common/common_global.h"

using namespace std;

namespace Drawing
{
    class Command
    {
    public:
        Command();
        ~Command();
        
        bool isEmpty() const;
        string toString() const;
        
        void operator=(const Command& value);
        bool operator==(const Command& value);
        bool operator!=(const Command& value);
        
        const string libName() const;
        const string funcName() const;
        const string tag() const;
        
        void run(void* owner, void* sender = nullptr) const;
        
    public:
        static bool parse(const string& str, Command& command);
        
    private:
        string _libName;
        string _funcName;
        string _tag;
    };
}

#endif	// ICOMMAND_H
