#ifndef SYSTEMCOMMAND_H
#define SYSTEMCOMMAND_H

#include "ICommand.h"

namespace Drawing
{
    class OpenCommand : public ICommand
    {
    public:
        OpenCommand(void* owner, void* sender = nullptr, const string& tag = "") : ICommand(owner, sender, tag)
        {
        }
        void run() const override;
    };
    
    class CloseCommand : public ICommand
    {
    public:
        CloseCommand(void* owner, void* sender = nullptr, const string& tag = "") : ICommand(owner, sender, tag)
        {
        }
        void run() const override;
    };
}

#endif	// SYSTEMCOMMAND_H