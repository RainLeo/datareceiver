#ifndef ICOMMAND_H
#define ICOMMAND_H

#include <string>
#include "common/common_global.h"

using namespace std;

namespace Drawing
{
    class ICommand
    {
    public:
        ICommand() : ICommand(nullptr)
        {
        }
        ICommand(void* owner, void* sender = nullptr, const string& tag = "")
        {
            this->owner = owner;
            this->sender = sender != nullptr ? sender : owner;
            this->tag = tag;
        }
		virtual ~ICommand()
        {
        }
        
        virtual void run() const = 0;
        
    public:
        string tag;
        void* sender;
        void* owner;
    };
    
    // the types of the class factories
    typedef ICommand* CommandCreate_t();
    typedef void CommandDestroy_t(ICommand*);
}

#endif	// ICOMMAND_H
