#include "SystemCommand.h"
#include "../components/Canvas.h"

namespace Drawing
{
    void OpenCommand::run() const
    {
        Canvas* canvas = static_cast<Canvas*>(owner);
        if(canvas != nullptr && !tag.empty())
        {
            canvas->openPage(tag);
        }
    }
    
    void CloseCommand::run() const
    {
        Canvas* canvas = static_cast<Canvas*>(owner);
        if(canvas != nullptr && !tag.empty())
        {
            canvas->closePage(tag);
        }
    }
}
