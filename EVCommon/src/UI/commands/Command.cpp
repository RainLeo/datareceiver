﻿#include "Command.h"
#include "common/system/Convert.h"
#include "common/data/PrimitiveType.h"
#include "SystemCommand.h"

using namespace Common;

namespace Drawing
{
	Command::Command()
	{
        _libName = "";
        _funcName = "";
        _tag = "";
	}
    Command::~Command()
    {
    }

	bool Command::isEmpty() const
	{
		return _funcName.empty();
	}
	string Command::toString() const
	{
		return Convert::convertStr("[Command: LibName=%s,FuncName=%s, Tag=%s]", _libName.c_str(), _funcName.c_str(), _tag.c_str());
	}

	void Command::operator=(const Command& value)
	{
		this->_libName = value._libName;
		this->_funcName = value._funcName;
        this->_tag = value._tag;
	}
	bool Command::operator==(const Command& value)
	{
        return this->_libName == value._libName &&
            this->_funcName == value._funcName &&
            this->_tag == value._tag;
	}
	bool Command::operator!=(const Command& value)
	{
		return !operator==(value);
	}
    
    const string Command::libName() const
    {
        return _libName;
    }
    const string Command::funcName() const
    {
        return _funcName;
    }
    const string Command::tag() const
    {
        return _tag;
    }
    
    void Command::run(void* owner, void* sender) const
    {
        if(isEmpty())
            return;
        
        // run system command if lib is empty.
        if(_libName.empty())
        {
            if(String::stringEquals(_funcName, "open", true))
            {
                OpenCommand command(owner, sender, _tag);
                command.run();
            }
            else if(String::stringEquals(_funcName, "close", true))
            {
                CloseCommand command(owner, sender, _tag);
                command.run();
            }
        }
    }
    
    bool Command::parse(const string& str, Command& command)
    {
        // lib:test;func:testfunc;tag:true
        // func:testfunc;tag:true
        StringArray texts;
        Convert::splitStr(str, ';', texts);
        for (uint i=0; i<texts.count(); i++)
        {
            string text = texts[i];
            StringArray elements;
            Convert::splitStr(text, ':', elements);
            if(elements.count() == 2)
            {
                string name = Convert::trimStr(elements[0]);
                string value = Convert::trimStr(elements[1]);
                if(String::stringEquals(name, "lib", true) ||
                   String::stringEquals(name, "libname", true))
                {
                    command._libName = value;
                }
                else if(String::stringEquals(name, "func", true) ||
                        String::stringEquals(name, "function", true) ||
                        String::stringEquals(name, "funcname", true))
                {
                    command._funcName = value;
                }
                else if(String::stringEquals(name, "tag", true) ||
                   String::stringEquals(name, "tagname", true))
                {
                    command._tag = value;
                }
            }
        }
        return true;
    }
}
