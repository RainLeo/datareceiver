#include "CommContext.h"

namespace Driver
{
    const char* DownloadProject::Phone = "phone";
    const char* DownloadProject::View = "view";
    const char* DownloadProject::Transfer = "transfer";
    
    DownloadProject::DownloadProject()
    {
    }
    DownloadProject::DownloadProject(const DownloadProject& value)
    {
        this->copyFrom(&value);
    }
    
    void DownloadProject::write(Stream* stream) const
    {
        id.write(stream);
        stream->writeStr(name, 1);
        stream->writeStr(actor, 1);
        version.write(stream);
    }
    void DownloadProject::read(Stream* stream)
    {
        id.read(stream);
        name = stream->readStr(1);
        actor = stream->readStr(1);
        version.read(stream);
    }
    void DownloadProject::copyFrom(const DownloadProject* value)
    {
        assert(!value->id.isEmpty());
        this->id = value->id;
        this->name = value->name;
        this->actor = value->actor;
        this->version = value->version;
        this->path = value->path;
    }
    
    bool DownloadProjects::isDifferent(const Projects& projects) const
    {
        if(count() != projects.count())
            return true;
        
        for (uint i=0; i<count(); i++)
        {
            const DownloadProject* dp = at(i);
            if(!projects.contains(dp->id))
            {
                return true;
            }
        }
        return false;
    }
    bool DownloadProjects::toProjects(Projects& projects)
    {
        return false;
    }
}