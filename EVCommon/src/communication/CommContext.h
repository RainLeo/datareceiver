//
//  CommContext.h
//  EVCommon
//
//  Created by baowei on 15/4/18.
//  Copyright (c) 2015 com. All rights reserved.
//

#ifndef COMMCONTEXT_H
#define COMMCONTEXT_H

#include "common/system/DateTime.h"
#include "common/data/PrimitiveType.h"
#include "common/driver/communication/BaseCommContext.h"
#include "../project/Project.h"

using namespace Common;
using namespace EVCommon;

namespace Driver
{
    class Heartbeat
    {
    public:
        DateTime curtime;
        
        Heartbeat()
        {
        	curtime = DateTime::now();
        }
        
        void write(Stream* stream) const
        {
        	curtime.writeBCDDateTime(stream);
        }
        void read(Stream* stream)
        {
        	curtime.readBCDDateTime(stream);
        }
        void copyFrom(const Heartbeat* value)
        {
            this->curtime = value->curtime;
        }
    };
    class Notify
    {
    public:
        string projectId;
        
        Notify(string projectId = "")
        {
            this->projectId = projectId;
        }
        
        void write(Stream* stream) const
        {
            stream->writeFixedLengthStr(projectId, ProjectIdLength);
        }
        void read(Stream* stream)
        {
            projectId = stream->readFixedLengthStr(ProjectIdLength);
        }
        void copyFrom(const Notify* value)
        {
            this->projectId = value->projectId;
        }
        
    private:
        const int ProjectIdLength = 36;
    };
    
    class Register
    {
    public:
        enum Result : byte
        {
            Succeed = 0,
            NonClient = 1,
            IdIncorrect = 2,
            NameIncorrect = 3,
            PasswordIncorrect = 4,
            DbError = 0xFE,
            CommunicationError = 0xFF
        };
        
        string id;
        string name;
        string password;
        
        Register()
        {
        }
        
        void write(Stream* stream) const
        {
            stream->writeStr(id, 1);
            stream->writeStr(name, 1);
            stream->writeStr(String::toBase64(password).c_str(), 1);
        }
        void read(Stream* stream)
        {
            id = stream->readStr(1);
            name = stream->readStr(1);
            password = String::fromBase64(stream->readStr(1));
        }
        void copyFrom(const Register* value)
        {
            this->id = value->id;
            this->name = value->name;
            this->password = value->password;
        }
    };
    
    class Login
    {
    public:
        enum Result : byte
        {
            Succeed = 0,
            NonClient = 1,
            IdIncorrect = 2,
            PasswordIncorrect = 3,
            DbError = 0xFE,
            CommunicationError = 0xFF
        };
        
        Uuid id;
        string actor;
        string password;
        
        Login()
        {
        }
        
        void write(Stream* stream) const
        {
            id.write(stream);
            stream->writeStr(actor, 1);
            stream->writeStr(String::toBase64(password).c_str(), 1);
        }
        void read(Stream* stream)
        {
            id.read(stream);
            actor = stream->readStr(1);
            password = String::fromBase64(stream->readStr(1));
        }
        void copyFrom(const Login* value)
        {
            this->id = value->id;
            this->actor = value->actor;
            this->password = value->password;
        }
    };
    class Logout
    {
    public:
        enum Result : byte
        {
            Succeed = 0,
            NonClient = 1,
            IdIncorrect = 2,
            DbError = 0xFE,
            CommunicationError = 0xFF
        };
        
        string id;
        
        Logout()
        {
        }
        
        void write(Stream* stream) const
        {
            stream->writeStr(id, 1);
        }
        void read(Stream* stream)
        {
            id = stream->readStr(1);
        }
        void copyFrom(const Logout* value)
        {
            this->id = value->id;
        }
    };
    class ChangePassword
    {
    public:
        enum Result : byte
        {
            Succeed = 0,
            NonClient = 1,
            IdIncorrect = 2,
            OldPasswordIncorrect = 3,
            NewPasswordIncorrect = 4,
            DbError = 0xFE,
            CommunicationError = 0xFF
        };
        
        string id;
        string oldPassword;
        string newPassword;
        
        ChangePassword()
        {
        }
        
        void write(Stream* stream) const
        {
            stream->writeStr(id, 1);
            stream->writeStr(String::toBase64(oldPassword).c_str(), 1);
            stream->writeStr(String::toBase64(newPassword).c_str(), 1);
        }
        void read(Stream* stream)
        {
            id = stream->readStr(1);
            oldPassword = String::fromBase64(stream->readStr(1));
            newPassword = String::fromBase64(stream->readStr(1));
        }
        void copyFrom(const ChangePassword* value)
        {
            this->id = value->id;
            this->oldPassword = value->oldPassword;
            this->newPassword = value->newPassword;
        }
    };
    
    class DownloadProject
    {
    public:
        enum Result : byte
        {
            Succeed = 0,
            NonClient = 1,
            IdIncorrect = 2,
            DbError = 0xFE,
            CommunicationError = 0xFF
        };
        
        Uuid id;
        string name;
        string actor;
        Version version;
        string path;
        
        DownloadProject();
        DownloadProject(const DownloadProject& value);
        
        void write(Stream* stream) const;
        void read(Stream* stream);
        void copyFrom(const DownloadProject* value);
        
    public:
        static const char* Phone;
        static const char* View;
        static const char* Transfer;
    };
    class DownloadProjects : public StreamVector<DownloadProject>
    {
    public:
        DownloadProjects(bool autoDelete = true, uint capacity = StreamVector<DownloadProject>::DefaultCapacity) : StreamVector<DownloadProject>(autoDelete, capacity)
        {
        }
        bool isDifferent(const Projects& projects) const;
        bool toProjects(Projects& projects);
    };
    
    class UploadProject
    {
    public:
        enum Result : byte
        {
            Succeed = 0,
            NonClient = 1,
            DbError = 0xFE,
            CommunicationError = 0xFF
        };
        
        Uuid id;
        string name;
        string actor;
        string filename;
        string path;
        
        UploadProject()
        {
        }
        
        void write(Stream* stream) const
        {
            id.write(stream);
            stream->writeStr(name, 1);
            stream->writeStr(actor, 1);
        }
        void read(Stream* stream)
        {
            id.read(stream);
            name = stream->readStr(1);
            actor = stream->readStr(1);
        }
        void copyFrom(const UploadProject* value)
        {
            this->id = value->id;
            this->name = value->name;
            this->actor = value->actor;
            this->filename = value->filename;
            this->path = value->path;
        }
    };
    
    class HeartbeatContext : public ElementAContext<Heartbeat>
    {
    };
    class NotifyContext : public ElementAContext<Notify>
    {
    };
    class RegisterContext : public ElementSContext<Register>
    {
    };
    class LoginContext : public ElementSContext<Login>
    {
    };
    class ChangePasswordContext : public ElementSContext<ChangePassword>
    {
    };
    class LogoutContext : public ElementSContext<Logout>
    {
    };
    class DownloadProjectContext : public FileContext<DownloadProject>
    {
    };
    class DownloadProjectListContext : public PacketContext<Login, DownloadProjects>
    {
    public:
        DownloadProjectListContext() : PacketContext<Login, DownloadProjects>(true)
        {
        }
    };
    class UploadProjectContext : public FileContext<UploadProject>
    {
    };
    class UploadProjectListContext : public PacketEContext<DownloadProjects>
    {
    };
//    class TagContext : public ElementContext<Tag, Empty>
//    {
//    };
}

#endif // COMMCONTEXT_H
