#include "ClientInstructions.h"

namespace EaseView
{
    ClientContext* HeartbeatInstruction::setValue(ClientContext* context)
    {
        return context;
    }
    ClientContext* RegisterInstruction::setValue(ClientContext* context)
    {
        return context;
    }
    ClientContext* LoginInstruction::setValue(ClientContext* context)
    {
        return context;
    }
    ClientContext* LogoutInstruction::setValue(ClientContext* context)
    {
        return context;
    }
    ClientContext* ChangePasswordInstruction::setValue(ClientContext* context)
    {
        return context;
    }
    ClientContext* DownloadProjectListInstruction::setValue(ClientContext* context)
    {
        return context;
    }
    ClientContext* DownloadProjectInstruction::setValue(ClientContext* context)
    {
        FileContext<DownloadProject>* rcontext = dynamic_cast<FileContext<DownloadProject>*>(context);
        if (rcontext != NULL)
        {
        }
        return DownloadFileInstruction<DownloadProject>::setValue(context);
    }
    ClientContext* UploadProjectInstruction::setValue(ClientContext* context)
    {
        FileContext<UploadProject>* rcontext = dynamic_cast<FileContext<UploadProject>*>(context);
        if (rcontext != NULL)
        {
        }
        return UploadFileInstruction<UploadProject>::setValue(context);
    }

//    ClientContext* NotifySendInstruction::getValue(ClientContext* context)
//    {
//        NotifyContext* nc = dynamic_cast<NotifyContext*>(context);
//        if(nc != NULL)
//        {
//            Notify notify(_projectId);
//            nc->setInputData(&notify);
//        }
//        return context;
//    }
//    
//    ClientContext* NotifyReceiveInstruction::setValue(ClientContext* context)
//    {
//        NotifyContext* nc = dynamic_cast<NotifyContext*>(context);
//        if(nc != NULL)
//        {
//            // get ip address
//            const Notify* notify = nc->inputData();
//            assert(notify);
//            if(Convert::stringEqual(_projectId, notify->projectId, true))
//            {
//                // create tcp device.
//                _service->openTcpDevice(nc->peerAddr());
//            }
//        }
//        return context;
//    }
//    
//    ClientContext* ReceiveTagInstruction::setValue(ClientContext* context)
//    {
//        TagContext* tc = dynamic_cast<TagContext*>(context);
//        if(tc != NULL)
//        {
//            TagService* ts = _tagService;
//            if(ts != NULL)
//            {
//                const Tag* tag = tc->inputData();
//                Tag* newTag = ts->getTag(tag->id());
//                if(newTag != NULL)
//                {
//                    newTag->operator=(tag->value());
//                }
//            }
//        }
//        return context;
//    }
}
