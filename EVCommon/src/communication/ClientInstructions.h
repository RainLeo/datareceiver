#ifndef CLIENTINSTRUCTIONS_H
#define CLIENTINSTRUCTIONS_H

#include "common/driver/communication/ServerInstruction.h"
#include "tag/Tag.h"
#include "communication/CommContext.h"
#include "tag/TagService.h"

using namespace Common;
using namespace Driver;
using namespace rtdb;

namespace EaseView
{
    // command: 0x10 - 0x2F
    class HeartbeatInstruction : public ElementAInstruction<Heartbeat>
    {
    public:
        HeartbeatInstruction(InstructionDescription* id) : ElementAInstruction<Heartbeat>(id)
        {
        }
        ~HeartbeatInstruction()
        {
        }
        byte command() const override
        {
            return 0x10;
        }
        
        bool allowLog() const override
        {
            return false;
        }
        
        ClientContext* setValue(ClientContext* context) override;
    };
    class RegisterInstruction : public ElementSInstruction<Register>
    {
    public:
        RegisterInstruction(InstructionDescription* id) : ElementSInstruction<Register>(id)
        {
        }
        ~RegisterInstruction()
        {
        }
        byte command() const override
        {
            return 0x13;
        }
        
        ClientContext* setValue(ClientContext* context) override;
    };
    class LoginInstruction : public ElementSInstruction<Login>
    {
    public:
        LoginInstruction(InstructionDescription* id) : ElementSInstruction<Login>(id)
        {
        }
        ~LoginInstruction()
        {
        }
        byte command() const override
        {
            return 0x14;
        }
        
        ClientContext* setValue(ClientContext* context) override;
    };
    class LogoutInstruction : public ElementSInstruction<Logout>
    {
    public:
        LogoutInstruction(InstructionDescription* id) : ElementSInstruction<Logout>(id)
        {
        }
        ~LogoutInstruction()
        {
        }
        byte command() const override
        {
            return 0x15;
        }
        
        ClientContext* setValue(ClientContext* context) override;
    };
    class ChangePasswordInstruction : public ElementSInstruction<ChangePassword>
    {
    public:
        ChangePasswordInstruction(InstructionDescription* id) : ElementSInstruction<ChangePassword>(id)
        {
        }
        ~ChangePasswordInstruction()
        {
        }
        byte command() const override
        {
            return 0x16;
        }
        
        ClientContext* setValue(ClientContext* context) override;
    };

    class DownloadProjectListInstruction : public DownloadPacketInstruction<Login, DownloadProjects>
    {
    public:
        DownloadProjectListInstruction(InstructionDescription* id) : DownloadPacketInstruction<Login, DownloadProjects>(id)
        {
        }
        ~DownloadProjectListInstruction()
        {
        }
        byte command() const override
        {
            return 0x17;
        }
        
        ClientContext* setValue(ClientContext* context) override;
    };
    class UploadProjectListInstruction : public UploadPacketEInstruction<DownloadProjects, DownloadProject>
    {
    public:
        UploadProjectListInstruction(InstructionDescription* id) : UploadPacketEInstruction<DownloadProjects, DownloadProject>(id)
        {
        }
        ~UploadProjectListInstruction()
        {
        }
        byte command() const override
        {
            return 0x18;
        }
        
//        ClientContext* setValue(ClientContext* context) override;
    };
    
    class DownloadProjectInstruction : public DownloadFileInstruction<DownloadProject>
    {
    public:
        DownloadProjectInstruction(InstructionDescription* id) : DownloadFileInstruction<DownloadProject>(id)
        {
        }
        ~DownloadProjectInstruction()
        {
        }
        byte command() const override
        {
            return 0x19;
        }
        
        ClientContext* setValue(ClientContext* context) override;
    };
    class UploadProjectInstruction : public UploadFileInstruction<UploadProject>
    {
    public:
        UploadProjectInstruction(InstructionDescription* id) : UploadFileInstruction<UploadProject>(id)
        {
        }
        ~UploadProjectInstruction()
        {
        }
        byte command() const override
        {
            return 0x1A;
        }
        
        ClientContext* setValue(ClientContext* context) override;
    };
//    class NotifySendInstruction : public ElementInstruction<Notify, Empty>
//    {
//    public:
//        NotifySendInstruction(InstructionDescription* id, string projectId) : ElementInstruction<Notify, Empty>(id)
//        {
//            assert(!projectId.empty());
//            _projectId = projectId;
//        }
//        ~NotifySendInstruction()
//        {
//        }
//        byte command() const override
//        {
//            return 0x12;
//        }
//        
//        bool allowLog(byte command) const override
//        {
//            return true;
//        }
//        
//        bool syncSendReceive(Interactive*) const override
//        {
//            return false;
//        }
//        
//        ClientContext* getValue(ClientContext* context) override;
//        
//    private:
//        string _projectId;
//    };
//    class NotifyReceiveInstruction : public ServerElementInstruction<Notify, Empty>
//    {
//    public:
//        NotifyReceiveInstruction(InstructionDescription* id, string projectId, ClientCommService* service)
//            : ServerElementInstruction<Notify, Empty>(id)
//        {
//            assert(!projectId.empty());
//            _projectId = projectId;
//            
//            if(service == NULL)
//                throw ArgumentNullException("service");
//            _service = service;
//        }
//        ~NotifyReceiveInstruction()
//        {
//        }
//        byte command() const override
//        {
//            return 0x13;
//        }
//        
//        bool allowLog(byte command) const override
//        {
//            return true;
//        }
//        bool autoResponsed() const override
//        {
//            return false;
//        }
//        
//        ClientContext* setValue(ClientContext* context) override;
//        
//    private:
//        string _projectId;
//        ClientCommService* _service;
//    };
    
//    class ReceiveTagInstruction : public ServerElementInstruction<Tag, Empty>
//    {
//    public:
//        ReceiveTagInstruction(InstructionDescription* id, TagService* service) : ServerElementInstruction<Tag, Empty>(id)
//        {
//            _tagService = service;
//        }
//        ~ReceiveTagInstruction()
//        {
//            _tagService = NULL;
//        }
//        byte command() const override
//        {
//            return 0x20;
//        }
//        
//        bool allowLog(byte command) const override
//        {
//            return false;
//        }
//        bool autoResponsed() const override
//        {
//            return false;
//        }
//        
//        ClientContext* setValue(ClientContext* context) override;
//        
//    private:
//        TagService* _tagService;
//    };
}

#endif // CLIENTINSTRUCTIONS_H
