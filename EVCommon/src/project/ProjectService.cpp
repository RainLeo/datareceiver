﻿#include "ProjectService.h"
#include "common/IO/File.h"
#include "common/IO/Path.h"
#include "common/IO/Directory.h"
#include "common/system/Convert.h"
#include "common/thread/Locker.h"
#include "common/system/Delegate.h"
#include "common/system/Application.h"
#include "ProjectConfig.h"
#include "archives/ArchiveService.h"
#include "driver/DriverService.h"
#include "tag/TagService.h"
#include "common/diag/Trace.h"

using namespace Common;
using namespace rtdb;
using namespace Driver;
using namespace Archives;

namespace EVCommon
{
#ifdef PHONE_OS
    bool _appInactived = false;
    void project_inactived(void* owner, void* sender, EventArgs* args)
    {
        if(!_appInactived)
        {
            _appInactived = true;
            
            ProjectService* ps = static_cast<ProjectService*>(owner);
            assert(ps);
            ps->inactive();
        }
    }
    void project_actived(void* owner, void* sender, EventArgs* args)
    {
        if(_appInactived)
        {
            _appInactived = false;
            
            ProjectService* ps = static_cast<ProjectService*>(owner);
            assert(ps);
            ps->active();
        }
    }
#endif
    
    ProjectService::ProjectService()
    {
    }
    ProjectService::~ProjectService()
    {
    }
    
    bool ProjectService::initialize()
    {
        ProjectSummaryService* pss = ProjectSummaryService::instance();
        assert(pss);
        if(pss->loadConfigFile() && pss->projectCount() > 0)
        {
            const ProjectSummaries& projects = pss->projects();
            for (uint i=0; i<projects.count(); i++)
            {
                initialize(projects[i]->id());
            }
            return true;
        }
        return false;
    }
    bool ProjectService::initialize(const Uuid& projectId)
    {
        Project* project = new Project(projectId);
        if(project->open(rootPath()))
        {
            ConfigFile temp = project->configFile();
            temp.fileName = ProjectConfig::ExtName;
            ProjectConfig reader(temp);
            if(!reader.Configuration::load(project))
            {
                project->close();
                delete project;
                return false;
            }
            // Fixbug: prevent projectId is not equal to file id.
            project->_id = projectId;
            _projects.add(project);
            return initialize(project);
        }
        else
        {
            delete project;
            return false;
        }
    }
    bool ProjectService::initialize(const String& projectId)
    {
        Uuid id;
        if(Uuid::parse(projectId, id))
        {
            return initialize(id);
        }
        return false;
    }
    bool ProjectService::initialize(Project* project)
    {
        if(!initServices(project))
            return false;
        
#ifdef PHONE_OS
        PhoneApplication* app = PhoneApplication::instance();
        app->addEventActived(Delegate(this, project_actived));
        app->addEventInactived(Delegate(this, project_inactived));
#endif
        return true;
    }
    
    bool ProjectService::unInitialize()
    {
        if(!unInitServices())
            return false;
        
        return true;
    }
    bool ProjectService::unInitialize(const Uuid& projectId)
    {
        Project* project = _projects.getProject(projectId);
        if(project != nullptr)
        {
            return unInitialize(project);
        }
        return false;
    }
    bool ProjectService::unInitialize(const String& projectId)
    {
        Uuid id;
        if(Uuid::parse(projectId, id))
        {
            return unInitialize(id);
        }
        return false;
    }
    bool ProjectService::unInitialize(Project* project)
    {
#ifdef PHONE_OS
        PhoneApplication* app = PhoneApplication::instance();
        app->removeEventActived(Delegate(this, project_actived));
        app->removeEventInactived(Delegate(this, project_inactived));
#endif
        
        bool result = unInitServices();
        
        if(!project->close())
            result = false;
        
        _projects.remove(project);
        
        return result;
    }
    
    void ProjectService::addServices(const Project* project, Services* services)
    {
        const uint count = 3;
        BaseService* temp[count] = {new TagService(), new ArchiveService(), new DriverService()};
        for (uint i=0; i<count; i++)
        {
            if(project->conainsService(temp[i]->name()))
                services->add(temp[i]);
            else
                delete temp[i];
        }
    }
    
    const String ProjectService::rootPath() const
    {
#ifdef PHONE_OS
        PhoneApplication* app = PhoneApplication::instance();
#else
        Application* app = Application::instance();
#endif
        assert(app != NULL && !app->rootPath().empty());

        String rootPath = Path::combine(app->rootPath(), "projects");
        return rootPath;
    }
    
    void ProjectService::active()
    {
        Locker locker(&_servicesMutex);
        for (uint i=0; i<_projects.count(); i++)
        {
            const Project* project = _projects[i];
            Services* services;
            if(_services.at(project->idStr(), services))
            {
                for (uint j=0; j<services->count(); j++)
                {
                    services->at(j)->active();
                }
            }
        }
    }
    void ProjectService::inactive()
    {
        Locker locker(&_servicesMutex);
        for (uint i=0; i<_projects.count(); i++)
        {
            const Project* project = _projects[i];
            Services* services;
            if(_services.at(project->idStr(), services))
            {
                for (uint j=0; j<services->count(); j++)
                {
                    services->at(j)->inactive();
                }
            }
        }
    }
    
    const ConfigFile& ProjectService::rootFile() const
    {
        Project* project = _projects[0];
        return project->configFile();
    }
    
    void ProjectService::addProject(const Project* project)
    {
        if(!_projects.contains(project->id()))
        {
            _projects.add(project);
        }
    }
    
    bool ProjectService::initServices(const String& rootPath)
    {
        for (uint i=0; i<_projects.count(); i++)
        {
            Project* project = _projects[i];
            if(project->open(rootPath))
            {
                initServices(project);
            }
        }
        return true;
    }
    bool ProjectService::initServices(const Project* project)
    {
        // initialize all of the services->
        Services* services = new Services();
        addServices(project, services);
        
        for (uint i=0; i<services->count(); i++)
        {
            BaseService* service1 = services->at(i);
            service1->addService(this);
            for (uint j=0; j<services->count(); j++)
            {
                const BaseService* service2 = services->at(j);
                if(service1 != service2)
                {
                    service1->addService(service2);
                }
            }
        }
        
        Instructions* instructions = project->instructions();
        const ConfigFile& cf = project->configFile();
        for (uint i=0; i<services->count(); i++)
        {
            BaseService* service = services->at(i);
            if(!service->initialize(&cf))
            {
                Trace::writeFormatLine("Failed to initialize service. name: %s", service->name().c_str());
                return false;
            }
            
            service->generateInstructions(instructions);
        }
        
        Locker locker(&_servicesMutex);
        _services.add(project->idStr(), services);
        
        return true;
    }
    bool ProjectService::unInitServices()
    {
        Locker locker(&_servicesMutex);
        for (int i=_projects.count()-1; i>=0; i--)
        {
            Project* project = _projects[i];
            
            Services* services;
            const String& idStr = project->idStr();
            if(_services.at(idStr, services))
            {
                for (int j=services->count()-1; j>=0; j--)
                {
                    services->at(j)->unInitialize();
                }
                delete services;
                _services.remove(idStr);
            }
            
            project->close();
        }
        
        return true;
    }
    
    Services* ProjectService::services(uint index)
    {
        if(index >= _projects.count())
            return NULL;
        
        Locker locker(&_servicesMutex);
        
        const String& projectId = _projects[index]->idStr();
        Services* services;
        if(_services.at(projectId, services))
        {
            return services;
        }
        return NULL;
    }
    
    const Projects& ProjectService::projects() const
    {
        return _projects;
    }
    
    ProjectSummaryService::ProjectSummaryService()
    {
    }
    ProjectSummaryService::~ProjectSummaryService()
    {
    }

    const ProjectSummaries& ProjectSummaryService::projects() const
    {
        return _projects;
    }
    bool ProjectSummaryService::loadConfigFile(const String& rootPath)
    {
        ConfigFile file(rootPath.isNullOrEmpty() ? this->rootPath().c_str() : rootPath.c_str(), ProjectSummaryConfig::ExtName);
        ProjectSummaryConfig reader(file);
        return reader.Configuration::load(&_projects);
    }
    
    bool ProjectSummaryService::updateProjects(const DownloadProjects& dps)
    {
        // removed all of old verion project files.
        String rootPath = this->rootPath();
        for(uint i=0;i<dps.count();i++)
        {
            const ProjectSummary* project = _projects.getProject(dps[i]->id);
            if(project != nullptr)
            {
                if(project->version() < dps[i]->version)
                {
                    project->removeFile(rootPath);
                }
            }
        }
        
        // update project list.
        _projects.clear();
        for(uint i=0;i<dps.count();i++)
        {
            ProjectSummary* project = new ProjectSummary(dps[i]->id, dps[i]->name, dps[i]->version);
            _projects.add(project);
        }
        
        // save projects.config
        ConfigFile file(rootPath.c_str(), ProjectSummaryConfig::ExtName);
        ProjectSummaryConfig writer(file);
        return writer.Configuration::save(&_projects);
    }
    bool ProjectSummaryService::exists(const ProjectSummary* project) const
    {
        return project != nullptr ? project->exists(rootPath()) : false;
    }
    const String ProjectSummaryService::rootPath() const
    {
#ifdef PHONE_OS
        PhoneApplication* app = PhoneApplication::instance();
#else
        Application* app = Application::instance();
#endif
        assert(app != NULL && !app->rootPath().empty());
        
        String rootPath = Path::combine(app->rootPath(), "projects");
        return rootPath;
    }
}
