﻿#include "Project.h"
#include "common/IO/Path.h"
#include "common/IO/File.h"
#include "common/IO/Directory.h"
#include "common/system/Convert.h"
#include "common/thread/Locker.h"
#include "common/diag/Debug.h"

using namespace Common;

namespace EVCommon
{
    const char* Project::ExtName = ".zip";

    Project::Project(const Uuid& id) : Project(id, String::Empty, Version::Empty)
    {
    }
    Project::Project(const Uuid& id, const String& name, const Version& version)
    {
        _id = id;
        _name = name;
        _version = version;
        
        _instructions.setAutoDelete(false);
    }
    Project::Project(const Uuid& id, const String& name, const Version& version, const StringArray& serviceNames) : Project(id, name, version)
    {
        _serviceNames.addRange(&serviceNames);
    }
    Project::~Project()
    {
    }
    
    const Uuid& Project::id() const
    {
        return _id;
    }
    const String Project::idStr() const
    {
        return _id.toString();
    }
    const String& Project::name() const
    {
        return _name;
    }
    const Version& Project::version() const
    {
        return _version;
    }
    bool Project::conainsService(const string& serviceName) const
    {
        return _serviceNames.contains(serviceName);
    }
    
    const ConfigFile& Project::configFile() const
    {
        return _configFile;
    }
    
    bool Project::open(const String& rootPath)
    {
        String path = Path::combine(rootPath, idStr());
        if(Directory::exists(path))
        {
            _configFile.rootPath = path;
            return true;
        }
        else
        {
            // read from a zip file.
            String fileName = (String)idStr() + Project::ExtName;
            String fullFileName = Path::combine(rootPath, fileName);
            if(!File::exists(fullFileName))
            {
                Trace::writeFormatLine("Can not find the file. '%s'", fullFileName.c_str());
                return false;
            }
            
            Zip* zip = new Zip(fullFileName);
            if(!zip->isValid())
            {
                delete zip;
                return false;
            }
            _configFile.zip = zip;
            return true;
        }
    }
    bool Project::close()
    {
        if(_configFile.isZip())
        {
            delete _configFile.zip;
            _configFile.zip = NULL;
        }
        return true;
    }
    
    Instructions* Project::instructions() const
    {
        return (Instructions*)&_instructions;
    }
    
    bool Projects::contains(const Uuid& id) const
    {
        for (uint i=0; i<count(); i++)
        {
            if(id == at(i)->id())
                return true;
        }
        return false;
    }
    
    Project* Projects::getProject(const Uuid& id) const
    {
        for (uint i=0; i<count(); i++)
        {
            Project* project = at(i);
            if(id == project->id())
                return project;
        }
        return nullptr;
    }
    Project* Projects::getProject(const String& id) const
    {
        Uuid projectId;
        if(Uuid::parse(id, projectId))
            return getProject(projectId);
        return nullptr;
    }
    
    ProjectSummary::ProjectSummary(const Uuid& id, const String& name, const Version& version)
    {
        _id = id;
        _name = name;
        _version = version;
    }
    ProjectSummary::~ProjectSummary()
    {
    }
    
    const Uuid& ProjectSummary::id() const
    {
        return _id;
    }
    const String ProjectSummary::idStr() const
    {
        return _id.toString();
    }
    const String& ProjectSummary::name() const
    {
        return _name;
    }
    const Version& ProjectSummary::version() const
    {
        return _version;
    }
    
    void ProjectSummary::removeFile(const String& rootPath) const
    {
        String path = Path::combine(rootPath, idStr());
        if(Directory::exists(path))
        {
            Directory::deleteDirectory(path);
        }
        
        String fileName = (String)idStr() + Project::ExtName;
        String fullFileName = Path::combine(rootPath, fileName);
        if(File::exists(fullFileName))
        {
            File::deleteFile(fullFileName);
        }
    }
    bool ProjectSummary::exists(const String& rootPath) const
    {
        String path = Path::combine(rootPath, idStr());
        if(Directory::exists(path))
        {
            return true;
        }
        
        String fileName = (String)idStr() + Project::ExtName;
        String fullFileName = Path::combine(rootPath, fileName);
        if(File::exists(fullFileName))
        {
            return true;
        }
        return false;
    }

    bool ProjectSummaries::contains(const Uuid& id) const
    {
        for (uint i=0; i<count(); i++)
        {
            if(id == at(i)->id())
                return true;
        }
        return false;
    }
    
    ProjectSummary* ProjectSummaries::getProject(const Uuid& id) const
    {
        for (uint i=0; i<count(); i++)
        {
            ProjectSummary* project = at(i);
            if(id == project->id())
                return project;
        }
        return nullptr;
    }
    ProjectSummary* ProjectSummaries::getProject(const String& id) const
    {
        Uuid projectId;
        if(Uuid::parse(id, projectId))
            return getProject(projectId);
        return nullptr;
    }
}
