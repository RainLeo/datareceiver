#include "ProjectConfig.h"
#include "common/system/TimeSpan.h"
#include "common/IO/Directory.h"
#include "common/IO/File.h"
#include "common/IO/Path.h"
#include "common/system/Convert.h"
#include "common/xml/XmlTextReader.h"
#include "common/xml/XmlDocument.h"
#include "project/Project.h"

using namespace Common;

namespace EVCommon
{
    const char* ProjectConfig::ExtName = "project.config";
    ProjectConfig::ProjectConfig(const ConfigFile& file) : Configuration(file)
    {
    }
    ProjectConfig::~ProjectConfig()
    {
    }
    
    bool ProjectConfig::load(XmlTextReader& reader, void* sender)
    {
        if (reader.nodeType() == XmlNodeType::Element &&
            reader.localName() == "project")
        {
            Project* project = static_cast<Project*>(sender);
            assert(project);

            Uuid id;
            if(Uuid::parse(reader.getAttribute("id"), id))
            {
                String name = reader.getAttribute("name");
                Version version;
                Version::parse(reader.getAttribute("version"), version);
                StringArray names;
                Convert::splitItems(reader.getAttribute("services"), names);
                project->_id = id;
                project->_name = name;
                project->_version = version;
                project->_serviceNames.addRange(&names);
                return true;
            }
        }
        return false;
    }
    
    const char* ProjectSummaryConfig::ExtName = "projects.config";
    ProjectSummaryConfig::ProjectSummaryConfig(const ConfigFile& file) : Configuration(file)
    {
    }
    ProjectSummaryConfig::~ProjectSummaryConfig()
    {
    }
    
    bool ProjectSummaryConfig::load(XmlTextReader& reader, void* sender)
    {
        if (reader.nodeType() == XmlNodeType::Element &&
            reader.localName() == "projects")
        {
            ProjectSummaries* projects = static_cast<ProjectSummaries*>(sender);
            assert(projects);
            
            while (reader.read())
            {
                if (reader.nodeType() == XmlNodeType::Element &&
                    reader.localName() == "project")
                {
                    Uuid id;
                    if(Uuid::parse(reader.getAttribute("id"), id))
                    {
                        String name = reader.getAttribute("name");
                        Version version;
                        Version::parse(reader.getAttribute("version"), version);
                        ProjectSummary* project = new ProjectSummary(id, name, version);
                        projects->add(project);
                    }
                }
            }
            return true;
        }
        return false;
    }
    bool ProjectSummaryConfig::save(XmlTextWriter& writer, void* sender)
    {
        ProjectSummaries* projects = static_cast<ProjectSummaries*>(sender);
        assert(projects);
        
        writer.writeStartElement("projects");
        
        for (uint i=0; i<projects->count(); i++)
        {
            const ProjectSummary* project = projects->at(i);
            writer.writeStartElement("project");
            writer.writeAttributeString("id", project->id().toString().c_str());
            writer.writeAttributeString("name", project->name().c_str());
            writer.writeAttributeString("version", project->version().toString().c_str());
            writer.writeEndElement();
        }
        
        writer.writeEndElement();
        
        return true;
    }
}
