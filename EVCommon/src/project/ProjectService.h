#ifndef PROJECTSERVICE_H
#define PROJECTSERVICE_H

#include "common/common_global.h"
#include "common/system/Singleton.h"
#include "common/thread/Thread.h"
#include "common/system/Delegate.h"
#include "common/data/PrimitiveType.h"
#include "../communication/CommContext.h"
#include "../base/BaseService.h"
#include "project/Project.h"

using namespace Common;
using namespace EVCommon;
using namespace Driver;

namespace EVCommon
{
    class ProjectConfig;
    class ProjectService
    {
    public:
        ~ProjectService();
        
        static ProjectService* instance()
        {
            return Singleton<ProjectService>::instance();
        }
        
        bool initialize();
        bool initialize(const Uuid& projectId);
        bool initialize(const String& projectId);
        
        bool unInitialize();
        bool unInitialize(const Uuid& projectId);
        bool unInitialize(const String& projectId);
        
        const ConfigFile& rootFile() const;
        const String rootPath() const;
        
        void addProject(const Project* project);
        uint projectCount() const
        {
            return _projects.count();
        }
        const Projects& projects() const;
        
        Services* services(uint index = 0);
        
        template<class T>
        T* getService(const string& projectId)
        {
            Locker locker(&_servicesMutex);
            
            Services* services;
            if(_services.at(projectId, services))
            {
                for (uint j=0; j<services->count(); j++)
                {
                    T* service = dynamic_cast<T*>(services->at(j));
                    if(service != NULL)
                    {
                        return service;
                    }
                }
            }
            return NULL;
        }
        template<class T>
        T* getDefaultService()
        {
            Vector<T> services;
            getServices(services);
            return services.count() == 1 ? services[0] : NULL;
        }
        template<class T>
        void getServices(Vector<T>& services)
        {
            Locker locker(&_servicesMutex);
            
            services.setAutoDelete(false);
            
            Vector<Services*> temp;
            _services.values(temp);
            
            for (uint i=0; i<temp.count(); i++)
            {
                Services* entry = *temp.at(i);
                for (uint j=0; j<entry->count(); j++)
                {
                    T* service = dynamic_cast<T*>(entry->at(j));
                    if(service != NULL)
                    {
                        services.add(service);
                    }
                }
            }
        }
        
    private:
        ProjectService();
        
        DECLARE_SINGLETON_CLASS(ProjectService);
        
        void addServices(const Project* project, Services* services);
        
        void active();
        friend void project_actived(void* owner, void* sender, EventArgs* args);
        void inactive();
        friend void project_inactived(void* owner, void* sender, EventArgs* args);
        
        bool initialize(Project* project);
        bool unInitialize(Project* project);
        
        bool initServices(const String& rootPath);
        bool initServices(const Project* project);
        bool unInitServices();
        
    private:
        Projects _projects;
        
        NamedServices _services;
        mutex _servicesMutex;
    };
    
    class ProjectSummaryConfig;
    class ProjectSummaryService
    {
    public:
        ~ProjectSummaryService();
        
        static ProjectSummaryService* instance()
        {
            return Singleton<ProjectSummaryService>::instance();
        }
        
        uint projectCount() const
        {
            return _projects.count();
        }
        const ProjectSummaries& projects() const;
        
        bool loadConfigFile(const String& rootPath = String::Empty);
        const String rootPath() const;
        
        bool updateProjects(const DownloadProjects& dps);
        bool exists(const ProjectSummary* project) const;
        
    private:
        ProjectSummaryService();
        
        DECLARE_SINGLETON_CLASS(ProjectSummaryService);
        
    private:
        ProjectSummaries _projects;
    };
}

#endif	// PROJECTSERVICE_H
