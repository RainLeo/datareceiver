#ifndef PROJECTCONFIG_H
#define PROJECTCONFIG_H

#include "common/common_global.h"
#include "common/system/Singleton.h"
#include "common/xml/Configuration.h"

using namespace Common;

namespace EVCommon
{
    class ProjectConfig : public Configuration
    {
    public:
        ProjectConfig(const ConfigFile& file);
        ~ProjectConfig();
        
    protected:
        bool load(XmlTextReader& reader, void* sender) override;
        
    public:
        static const char* ExtName;
    };
    
    class ProjectSummaryConfig : public Configuration
    {
    public:
        ProjectSummaryConfig(const ConfigFile& file);
        ~ProjectSummaryConfig();
        
    protected:
        bool load(XmlTextReader& reader, void* sender) override;
        bool save(XmlTextWriter& writer, void* sender) override;
        
    public:
        static const char* ExtName;
    };
}

#endif	// PROJECTCONFIG_H
