#ifndef PROJECT_H
#define PROJECT_H

#include "common/common_global.h"
#include "common/system/DateTime.h"
#include "common/data/Vector.h"
#include "common/data/StringArray.h"
#include "common/system/Version.h"
#include "common/xml/ConfigFile.h"
#include "common/data/PrimitiveType.h"
#include "common/driver/instructions/Instruction.h"

using namespace Common;
using namespace Driver;

namespace EVCommon
{
    class ProjectConfig;
    class ProjectService;
    class COMMON_EXPORT Project
    {
    public:
        Project(const Uuid& id);
        Project(const Uuid& id, const String& name, const Version& version);
        Project(const Uuid& id, const String& name, const Version& version, const StringArray& serviceNames);
        ~Project();
        
        const Uuid& id() const;
        const String idStr() const;
        const String& name() const;
        const Version& version() const;
        bool conainsService(const string& serviceName) const;
        
        const ConfigFile& configFile() const;
        
        bool open(const String& rootPath);
        bool close();
        
        Instructions* instructions() const;
        
    private:
        friend ProjectConfig;
        friend ProjectService;
        
        Uuid _id;
        String _name;
        Version _version;
        StringArray _serviceNames;
        
        ConfigFile _configFile;
        
        Instructions _instructions;
        
    public:
        static const char* ExtName;
    };

    class COMMON_EXPORT Projects : public Vector<Project>
    {
    public:
        Projects(bool autoDelete = true, uint capacity = Vector<Project>::DefaultCapacity) : Vector<Project>(autoDelete, capacity)
        {
        }
        
        bool contains(const Uuid& id) const;
        
        Project* getProject(const Uuid& id) const;
        Project* getProject(const String& id) const;
    };
    
    class COMMON_EXPORT ProjectSummary
    {
    public:
        ProjectSummary(const Uuid& id, const String& name, const Version& version);
        ~ProjectSummary();
        
        const Uuid& id() const;
        const String idStr() const;
        const String& name() const;
        const Version& version() const;
        
        void removeFile(const String& rootPath) const;
        bool exists(const String& rootPath) const;
        
    private:
        Uuid _id;
        String _name;
        Version _version;
    };
    
    class COMMON_EXPORT ProjectSummaries : public Vector<ProjectSummary>
    {
    public:
        ProjectSummaries(bool autoDelete = true, uint capacity = Vector<ProjectSummary>::DefaultCapacity) : Vector<ProjectSummary>(autoDelete, capacity)
        {
        }
        
        bool contains(const Uuid& id) const;
        
        ProjectSummary* getProject(const Uuid& id) const;
        ProjectSummary* getProject(const String& id) const;
    };
}

#endif	// PROJECT_H
