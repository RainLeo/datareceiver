#ifndef TAGINSTRUCTION_H
#define TAGINSTRUCTION_H

#include "common/common_global.h"
#include "common/driver/instructions/InstructionSet.h"
#include "common/driver/communication/ServerInstruction.h"
#include "Tag.h"

using namespace Common;
using namespace Driver;

namespace rtdb
{
    class TagService;
    class COMMON_EXPORT TagInstructionSet : public InstructionSet
    {
    public:
        TagInstructionSet(TagService* service);
        ~TagInstructionSet();
        
        void generateInstructions(Instructions* instructions);
        
        InstructionSet* clone() const
        {
            return new TagInstructionSet(_service);
        }
        
    private:
        TagService* _service;
    };
    
    class TagAContext : public ElementAContext<Tag>
    {
    };
    
    // command: 0x30 - 0x3F
    class SendTagAInstruction : public ElementAInstruction<Tag>
    {
    public:
        SendTagAInstruction(InstructionDescription* id) : ElementAInstruction<Tag>(id)
        {
        }
        ~SendTagAInstruction()
        {
        }
        byte command() const override
        {
            return 0x30;
        }
    };
    
    class ReceiveTagAInstruction : public ServerElementAInstruction<Tag>
    {
    public:
        ReceiveTagAInstruction(InstructionDescription* id, TagService* service) : ServerElementAInstruction<Tag>(id)
        {
            assert(service);
            _service = service;
        }
        ~ReceiveTagAInstruction()
        {
            _service = NULL;
        }
        byte command() const override
        {
            return 0x30;
        }
        
        ClientContext* setValue(ClientContext* context) override;
        
    private:
        TagService* _service;
    };
}

#endif	// TAGINSTRUCTION_H
