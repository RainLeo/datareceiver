#include "TagConfig.h"
#include "common/system/TimeSpan.h"
#include "common/IO/Directory.h"
#include "common/IO/File.h"
#include "common/IO/Path.h"
#include "common/system/Convert.h"
#include "common/xml/XmlTextReader.h"
#include "common/xml/XmlDocument.h"
#include "Tag.h"
#include "TagService.h"

using namespace Common;

namespace rtdb
{
    TagConfig::TagConfig(const ConfigFile& file, TagService* service) : Configuration(file)
    {
        if(service == NULL)
            throw ArgumentException("service");
        
        _service = service;
    }
    
    TagConfig::~TagConfig()
    {
        _service = NULL;
    }
    
    bool TagConfig::load(XmlTextReader& reader)
    {
        if (reader.nodeType() == XmlNodeType::Element &&
            reader.localName() == "tags")
        {
            while (reader.read())
            {
                if (reader.nodeType() == XmlNodeType::Element &&
                    reader.localName() == "tag")
                {
                    uint tagId;
                    if(Convert::parseUInt32(reader.getAttribute("id"), tagId))
                    {
                        string name = reader.getAttribute("name");
                        Tag::Type type = Tag::fromTypeStr(reader.getAttribute("type"));
                        if(Tag::isValidType(type))
                        {
                            Tag* tag = new Tag(type, tagId, name);
                            Tag::Value value, minValue, maxValue;
                            if(Tag::changeStringValue(reader.getAttribute("value"), type, value))
                            {
                                tag->setValue(value);
                            }
                            if(Tag::changeStringValue(reader.getAttribute("minValue"), type, minValue))
                            {
                                tag->setMinValue(minValue);
                            }
                            if(Tag::changeStringValue(reader.getAttribute("maxValue"), type, maxValue))
                            {
                                tag->setMaxValue(maxValue);
                            }
                            
                            _service->addTag(tag);
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }
}
