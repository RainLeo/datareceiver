#ifndef TAGSERVICE_H
#define TAGSERVICE_H

#include <string>
#include <mutex>
#include "common/common_global.h"
#include "common/thread/Thread.h"
#include "base/BaseService.h"
#include "Tag.h"

using namespace std;
using namespace Common;
using namespace EVCommon;

namespace rtdb
{
    class COMMON_EXPORT TagService : public BaseService
    {
    public:
        TagService();
        ~TagService();
        
        bool initialize(const ConfigFile* file = NULL) override;
        bool unInitialize() override;
        
        void generateInstructions(Instructions* instructions) override;
        
        Tag* getTag(uint tagId);
        Tag* getTag(const string& tagName);
        void addTag(Tag* tag);
        bool containsTag(int tagId);
        uint tagCount();
        const Tags& allTags();
        Tag::Type getTagType(uint tagId);
        
        bool expressionValue(const string& expression, Tag::Type type, Tag::Value& value);
        
        void addTagValueChangedHandler(const string& expression, const Delegate& delegate);
        void removeTagValueChangedHandler(const string& expression, const Delegate& delegate);
        
        void addTagValueChangedHandler(const Tag* tag, const Delegate& delegate);
        void removeTagValueChangedHandler(const Tag* tag, const Delegate& delegate);
        
        void addTagValueChangedHandler(uint tagId, const Delegate& delegate);
        void removeTagValueChangedHandler(uint tagId, const Delegate& delegate);
        
        void addTagValueChangedHandler(const Delegate& delegate, bool hasSystemTag = false);
        void removeTagValueChangedHandler(const Delegate& delegate, bool hasSystemTag = false);
        
        string name() const override
        {
            return "tag";
        }
        
    public:
        static bool isSystemTag(const Tag* tag);
        
    private:
        bool initSystemTag();

        friend void systemTagProc(void* parameter);
        void systemTagProcInner();
        
    private:
        enum SystemTagId
        {
            Second = 1,
            Millisecond = 2,
        };
        
        const static uint MaxSystemTagId = 499;
        
        Tags _tags;
        mutex _tagsMutex;
        
        Thread* _systemTagThread;
    };
}

#endif	// TAGSERVICE_H
