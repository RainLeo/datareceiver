﻿#include "TagService.h"
#include "common/diag/Trace.h"
#include "common/system/Convert.h"
#include "common/thread/Locker.h"
#include "TagConfig.h"
#include "TagInstructions.h"

using namespace Common;

namespace rtdb
{
    void systemTagProc(void* parameter)
    {
        TagService* ts = (TagService*)parameter;
        assert(ts);
        ts->systemTagProcInner();
    }
    
    TagService::TagService()
    {
        _systemTagThread = NULL;
    }
    TagService::~TagService()
    {
        if(_systemTagThread != NULL)
        {
            _systemTagThread->stop();
            delete _systemTagThread;
            _systemTagThread = NULL;
        }
    }
    
    bool TagService::initialize(const ConfigFile* file)
    {
        if(!initSystemTag())
            return false;
        
        ConfigFile temp = *file;
        temp.fileName = "tag.config";
        TagConfig reader(temp, this);
        if(!reader.Configuration::load())
        {
            Trace::writeFormatLine("Failed to load config file. name: %s", temp.fileName.c_str());
            return false;
        }
        
        return true;
    }
    bool TagService::unInitialize()
    {
        if(_systemTagThread != NULL)
        {
            _systemTagThread->stop();
            delete _systemTagThread;
            _systemTagThread = NULL;
        }
        
        Locker locker(&_tagsMutex);
        _tags.clear();
        
        return true;
    }
    
    void TagService::generateInstructions(Instructions* instructions)
    {
        instructions->add(new SendTagAInstruction(new InstructionDescription("SendTagA", new TagAContext())));
        instructions->add(new ReceiveTagAInstruction(new InstructionDescription("ReceiveTagA", new TagAContext()), this));
    }
    
    bool TagService::initSystemTag()
    {
        Locker locker(&_tagsMutex);
        
        Tag* tag;
        Tag::Value minValue, maxValue;
        minValue.nValue = 0;
        maxValue.nValue = 59;
        tag = new Tag(Tag::Integer32, Second, "$(Second)", minValue, maxValue);
        _tags.addTag(tag);
        
        minValue.nValue = 0;
        maxValue.nValue = 999;
        tag = new Tag(Tag::Integer32, Millisecond, "$(Millisecond)", minValue, maxValue);
        _tags.addTag(tag);
        
        _systemTagThread = new Thread();
        _systemTagThread->setName("tag_systemTagProc");
        _systemTagThread->startProc(systemTagProc, this, 100);
        
        return true;
    }
    
    void TagService::systemTagProcInner()
    {
        DateTime now = DateTime::now();
        
        Locker locker(&_tagsMutex);
        
        Tag* tag;
        tag = _tags.getTag(Second);
        tag->setValue(now.second());
        
        tag = _tags.getTag(Millisecond);
        tag->setValue(now.millisecond());
    }
    
    Tag* TagService::getTag(uint tagId)
    {
        Locker locker(&_tagsMutex);
        return _tags.getTag(tagId);
    }
    Tag* TagService::getTag(const string& tagName)
    {
        Locker locker(&_tagsMutex);
        return _tags.getTag(tagName);
    }
    void TagService::addTag(Tag* tag)
    {
        if(tag != NULL && !isSystemTag(tag))
        {
            Locker locker(&_tagsMutex);
            _tags.addTag(tag);
        }
    }
    bool TagService::containsTag(int tagId)
    {
        Locker locker(&_tagsMutex);
        return _tags.containsTag(tagId);
    }
    uint TagService::tagCount()
    {
        Locker locker(&_tagsMutex);
        return _tags.count();
    }
    const Tags& TagService::allTags()
    {
        Locker locker(&_tagsMutex);
        return _tags;
    }
    Tag::Type TagService::getTagType(uint tagId)
    {
        const Tag* tag = getTag(tagId);
        return tag != NULL ? tag->type() : Tag::Type::Null;
    }
    
    bool TagService::isSystemTag(const Tag* tag)
    {
        return tag != NULL ? tag->id() <= MaxSystemTagId : false;
    }
    
    bool TagService::expressionValue(const string& expression, Tag::Type type, Tag::Value& value)
    {
        if(expression.empty())
            return false;
        
        // for test.
        bool temp;
        if(Convert::parseBoolean(expression, temp) &&
           Tag::changeDigitalValue(temp, type, value))
        {
            return true;
        }
        
        Locker locker(&_tagsMutex);
        for (uint i=0; i<_tags.count(); i++)
        {
            Tag* tag = _tags[i];
            if(tag != NULL)
            {
                if(String::stringEquals(expression, tag->name(), true))
                {
                    return tag->getValue(type, value);
                }
            }
        }
        return false;
    }
    
    void TagService::addTagValueChangedHandler(const string& expression, const Delegate& delegate)
    {
        Locker locker(&_tagsMutex);
        for (uint i=0; i<_tags.count(); i++)
        {
            Tag* tag = _tags[i];
            if(tag != NULL)
            {
                if(String::stringEquals(expression, tag->name(), true))
                {
                    tag->valueChangedDelegates()->add(delegate);
                }
            }
        }
    }
    void TagService::removeTagValueChangedHandler(const string& expression, const Delegate& delegate)
    {
        Locker locker(&_tagsMutex);
        for (uint i=0; i<_tags.count(); i++)
        {
            Tag* tag = _tags[i];
            if(tag != NULL)
            {
                if(String::stringEquals(expression, tag->name(), true))
                {
                    tag->valueChangedDelegates()->remove(delegate);
                }
            }
        }
    }
    
    void TagService::addTagValueChangedHandler(const Tag* tag, const Delegate& delegate)
    {
        if(tag != NULL)
        {
            Tag* temp = getTag(tag->id());
            if(temp != NULL)
            {
                temp->valueChangedDelegates()->add(delegate);
            }
        }
    }
    void TagService::removeTagValueChangedHandler(const Tag* tag, const Delegate& delegate)
    {
        if(tag != NULL)
        {
            Tag* temp = getTag(tag->id());
            if(temp != NULL)
            {
                temp->valueChangedDelegates()->remove(delegate);
            }
            
        }
    }
    
    void TagService::addTagValueChangedHandler(uint tagId, const Delegate& delegate)
    {
        Tag* tag = getTag(tagId);
        if(tag != NULL)
        {
            tag->valueChangedDelegates()->add(delegate);
        }
    }
    void TagService::removeTagValueChangedHandler(uint tagId, const Delegate& delegate)
    {
        Tag* tag = getTag(tagId);
        if(tag != NULL)
        {
            tag->valueChangedDelegates()->remove(delegate);
        }
    }
    
    void TagService::addTagValueChangedHandler(const Delegate& delegate, bool hasSystemTag)
    {
        Locker locker(&_tagsMutex);
        for (uint i=0; i<_tags.count(); i++)
        {
            Tag* tag = _tags[i];
            if(tag != NULL)
            {
                if(!hasSystemTag && isSystemTag(tag))
                    continue;
                
                tag->valueChangedDelegates()->add(delegate);
            }
        }
    }
    void TagService::removeTagValueChangedHandler(const Delegate& delegate, bool hasSystemTag)
    {
        Locker locker(&_tagsMutex);
        for (uint i=0; i<_tags.count(); i++)
        {
            Tag* tag = _tags[i];
            if(tag != NULL)
            {
                if(!hasSystemTag && isSystemTag(tag))
                    continue;
                
                tag->valueChangedDelegates()->remove(delegate);
            }
        }
    }
}
