#ifndef TAGCONFIG_H
#define TAGCONFIG_H

#include <string>
#include "common/common_global.h"
#include "common/system/Singleton.h"
#include "common/xml/Configuration.h"
#include "Tag.h"

using namespace std;
using namespace Common;

namespace rtdb
{
    class TagService;
    class COMMON_EXPORT TagConfig : public Configuration
    {
    public:
        TagConfig(const ConfigFile& file, TagService* service);
        ~TagConfig();
        
    protected:
        bool load(XmlTextReader& reader);
        
    private:
        TagService* _service;
    };
}

#endif	// TAGCONFIG_H
