﻿#include "Tag.h"
#include "common/system/Convert.h"
#include "common/thread/Locker.h"
#include "common/diag/Debug.h"

using namespace Common;

namespace rtdb
{
    Tag::Tag(Type type, uint tagId, const string& tagName)
	{
        _id = tagId;
        _type = type;
        _name = tagName;
        _value.nValue = 0;
        _qualitystamp = InitValue;
        changeStringValue("0", type, _minValue);
        changeStringValue("0", type, _maxValue);
    }
    Tag::Tag(Type type, uint tagId, const string& tagName, const Value& minValue, const Value& maxValue) : Tag(type, tagId, tagName)
    {
        setMinValue(minValue);
        setMaxValue(maxValue);
    }
    Tag::~Tag()
    {
    }

	bool Tag::isEmpty() const
	{
		return (int)_id == InvalidTagId;
	}

	void Tag::operator=(const Tag& tag)
	{
		this->_name = tag._name;
		this->_id = tag._id;
        this->_type = tag._type;
        this->_value = tag._value;
        this->_minValue = tag._minValue;
        this->_maxValue = tag._maxValue;
        this->_timestamp = tag._timestamp;
        this->_qualitystamp = tag._qualitystamp;
	}
	bool Tag::operator==(const Tag& tag) const
    {
        if(!(this->_name == tag._name &&
             this->_id == tag._id &&
             this->_type == tag._type &&
             this->_timestamp == tag._timestamp &&
             this->_qualitystamp == tag._qualitystamp))
        {
            return false;
        }
        
        return isValueEqual(_type, _value, tag._value) &&
            isValueEqual(_type, _minValue, tag._minValue) &&
            isValueEqual(_type, _minValue, tag._minValue);
    }
	bool Tag::operator!=(const Tag& tag) const
	{
		return !operator==(tag);
	}
    void Tag::operator=(const Value& value)
    {
        setValue(value);
    }
    bool Tag::operator==(const Value& value) const
    {
        return isValueEqual(_type, _value, value);
    }
    bool Tag::operator!=(const Value& value) const
    {
        return !operator==(value);
    }
    
    void Tag::operator=(const bool& value)
    {
        setValue(value);
    }
    Tag::operator bool() const
    {
        bool v = false;
        getValue(v);
        return v;
    }
    bool Tag::operator==(const bool& value) const
    {
        return compareValue(value);
    }
    bool Tag::operator!=(const bool& value) const
    {
        return !operator==(value);
    }
    
    void Tag::operator=(const short& value)
    {
        setValue(value);
    }
    Tag::operator short() const
    {
        int v = 0;
        getValue(v);
        return v;
    }
    bool Tag::operator==(const short& value) const
    {
        return compareValue(value);
    }
    bool Tag::operator!=(const short& value) const
    {
        return !operator==(value);
    }
    
    void Tag::operator=(const int& value)
    {
        setValue(value);
    }
    Tag::operator int() const
    {
        int v = 0;
        getValue(v);
        return v;
    }
    bool Tag::operator==(const int& value) const
    {
        return compareValue(value);
    }
    bool Tag::operator!=(const int& value) const
    {
        return !operator==(value);
    }
    
    void Tag::operator=(const int64_t& value)
    {
        setValue(value);
    }
    Tag::operator int64_t() const
    {
        int64_t v = 0;
        getValue(v);
        return v;
    }
    bool Tag::operator==(const int64_t& value) const
    {
        return compareValue(value);
    }
    bool Tag::operator!=(const int64_t& value) const
    {
        return !operator==(value);
    }
    
    void Tag::operator=(const float& value)
    {
        setValue(value);
    }
    Tag::operator float() const
    {
        float v = 0.0f;
        getValue(v);
        return v;
    }
    bool Tag::operator==(const float& value) const
    {
        return compareValue(value);
    }
    bool Tag::operator!=(const float& value) const
    {
        return !operator==(value);
    }
    
    void Tag::operator=(const double& value)
    {
        setValue(value);
    }
    Tag::operator double() const
    {
        double v = 0.0;
        getValue(v);
        return v;
    }
    bool Tag::operator==(const double& value) const
    {
        return compareValue(value);
    }
    bool Tag::operator!=(const double& value) const
    {
        return !operator==(value);
    }

    void Tag::operator=(const string& value)
    {
        setValue(value);
    }
    Tag::operator string() const
    {
        string v = "";
        getValue(v);
        return v;
    }
    bool Tag::operator==(const string& value) const
    {
        return compareValue(value);
    }
    bool Tag::operator!=(const string& value) const
    {
        return !operator==(value);
    }
    
    void Tag::operator=(const DateTime& value)
    {
        setValue(value);
    }
    Tag::operator DateTime() const
    {
        DateTime v = DateTime::MinValue;
        getValue(v);
        return v;
    }
    bool Tag::operator==(const DateTime& value) const
    {
        return compareValue(value);
    }
    bool Tag::operator!=(const DateTime& value) const
    {
        return !operator==(value);
    }
    
    bool Tag::isValueEqual(Type type, const Value& value1, const Value& value2)
    {
        switch (type)
        {
            case Null:
                return true;
            case Digital:
                return value1.bValue == value2.bValue;
            case Integer16:
                return value1.sValue == value2.sValue;
            case Integer32:
                return value1.nValue == value2.nValue;
            case Integer64:
                return value1.lValue == value2.lValue;
            case Float32:
                return value1.fValue == value2.fValue;
            case Float64:
                return value1.dValue == value2.dValue;
            case Text:
                return strcmp(value1.strValue, value2.strValue) == 0;
            case Date:
                return value1.tValue == value2.tValue;
            default:
                break;
        }
		return false;
    }
    
    void Tag::setStringValue(const string& str)
    {
        setStringValue(str.c_str());
    }
    void Tag::setStringValue(const char* str)
    {
        setStringValue(str, _type, _value);
    }
    void Tag::setStringValue(const string& str, Type type, Value& value)
    {
        setStringValue(str.c_str(), type, value);
    }
    void Tag::setStringValue(const char* str, Type type, Value& value)
    {
        if (str == NULL)
            return;
        
        if (type == Text)
        {
            if (value.strValue != NULL)
            {
                delete[] value.strValue;
            }
            size_t len = strlen(str);
            if (len > 0)
            {
                value.strValue = new char[len + 1];
                strcpy(value.strValue, str);
            }
            else
            {
                value.strValue = new char[1];
                value.strValue[0] = '\0';
            }
        }
    }
    
    bool Tag::isNullValue(const Tag& tag)
    {
        return (tag._qualitystamp == InitValue);
    }
    bool Tag::isNullValue() const
    {
        return isNullValue(*this);
    }
    void Tag::setNullValue()
    {
        _qualitystamp = InitValue;
    }
    
    Tag::Type Tag::type() const
    {
        return _type;
    }

    void Tag::setValue(const Value& v)
    {
        setValueInner(v);
        
        valueChanged();
    }
    bool Tag::setValue(const bool& v)
    {
        if(compareValue(v))
            return false;
        
        Value temp;
        if(changeValueInner(v, type(), temp))
        {
            setValue(temp);
            return true;
        }
        return false;
    }
    bool Tag::setValue(const short& v)
    {
        if(compareValue(v))
            return false;
        
        Value temp;
        if(changeValueInner(v, type(), temp))
        {
            setValue(temp);
            return true;
        }
        return false;
    }
    bool Tag::setValue(const int& v)
    {
        if(compareValue(v))
            return false;
        
        Value temp;
        if(changeValueInner(v, type(), temp))
        {
            setValue(temp);
            return true;
        }
        return false;
    }
    bool Tag::setValue(const int64_t& v)
    {
        if(compareValue(v))
            return false;
        
        Value temp;
        if(changeValueInner(v, type(), temp))
        {
            setValue(temp);
            return true;
        }
        return false;
    }
    bool Tag::setValue(const float& v)
    {
        if(compareValue(v))
            return false;
        
        Value temp;
        if(changeValueInner(v, type(), temp))
        {
            setValue(temp);
            return true;
        }
        return false;
    }
    bool Tag::setValue(const double& v)
    {
        if(compareValue(v))
            return false;
        
        Value temp;
        if(changeValueInner(v, type(), temp))
        {
            setValue(temp);
            return true;
        }
        return false;
    }
    bool Tag::setValue(const string& v)
    {
        if(compareValue(v))
            return false;
        
        Value temp;
        if(changeStringValue(v, type(), temp))
        {
            setValue(temp);
            return true;
        }
        return false;
    }
    bool Tag::setValue(const DateTime& v)
    {
        if(compareValue(v))
            return false;
        
        Value temp;
        if(changeDateValue(v, type(), temp))
        {
            setValue(temp);
            return true;
        }
        return false;
    }
    void Tag::setValue(const Value& v, Qualitystamp qualitystamp, DateTime timestamp)
    {
        setValueInner(v, qualitystamp, timestamp);
        
        valueChanged();
    }
    bool Tag::setValue(const bool& v, Qualitystamp qualitystamp, DateTime timestamp)
    {
        if(compareValue(v))
            return false;
        
        Value temp;
        if(changeValueInner(v, type(), temp))
        {
            setValue(temp, qualitystamp, timestamp);
            return true;
        }
        return false;
    }
    bool Tag::setValue(const short& v, Qualitystamp qualitystamp, DateTime timestamp)
    {
        if(compareValue(v))
            return false;
        
        Value temp;
        if(changeValueInner(v, type(), temp))
        {
            setValue(temp, qualitystamp, timestamp);
            return true;
        }
        return false;
    }
    bool Tag::setValue(const int& v, Qualitystamp qualitystamp, DateTime timestamp)
    {
        if(compareValue(v))
            return false;
        
        Value temp;
        if(changeValueInner(v, type(), temp))
        {
            setValue(temp, qualitystamp, timestamp);
            return true;
        }
        return false;
    }
    bool Tag::setValue(const int64_t& v, Qualitystamp qualitystamp, DateTime timestamp)
    {
        if(compareValue(v))
            return false;
        
        Value temp;
        if(changeValueInner(v, type(), temp))
        {
            setValue(temp, qualitystamp, timestamp);
            return true;
        }
        return false;
    }
    bool Tag::setValue(const float& v, Qualitystamp qualitystamp, DateTime timestamp)
    {
        if(compareValue(v))
            return false;
        
        Value temp;
        if(changeValueInner(v, type(), temp))
        {
            setValue(temp, qualitystamp, timestamp);
            return true;
        }
        return false;
    }
    bool Tag::setValue(const double& v, Qualitystamp qualitystamp, DateTime timestamp)
    {
        if(compareValue(v))
            return false;
        
        Value temp;
        if(changeValueInner(v, type(), temp))
        {
            setValue(temp, qualitystamp, timestamp);
            return true;
        }
        return false;
    }
    bool Tag::setValue(const string& v, Qualitystamp qualitystamp, DateTime timestamp)
    {
        if(compareValue(v))
            return false;
        
        Value temp;
        if(changeStringValue(v, type(), temp))
        {
            setValue(temp, qualitystamp, timestamp);
            return true;
        }
        return false;
    }
    bool Tag::setValue(const DateTime& v, Qualitystamp qualitystamp, DateTime timestamp)
    {
        if(compareValue(v))
            return false;
        
        Value temp;
        if(changeDateValue(v, type(), temp))
        {
            setValue(temp, qualitystamp, timestamp);
            return true;
        }
        return false;
    }
    
    void Tag::setValueInner(const Value& value)
    {
        setValueInner(value, Qualitystamp::Good, DateTime::utcNow());
    }
    void Tag::setValueInner(const Value& value, Qualitystamp qualitystamp, DateTime timestamp)
    {
        if (_type == Text)
        {
            setStringValue(value.strValue);
        }
        else
        {
            _value = value;
        }
        
        _timestamp = timestamp;
        _qualitystamp = qualitystamp;
        
    }
    const string& Tag::name() const
    {
        return _name;
    }
    uint Tag::id() const
    {
        return _id;
    }
    
    bool Tag::isAnalogValue() const
    {
        return isAnalogValue(type());
    }
    
    void Tag::setMinValue(const Value& value)
    {
        if (_type != Text)
        {
            _minValue = value;
        }
    }
    void Tag::setMaxValue(const Value& value)
    {
        if (_type != Text)
        {
            _maxValue = value;
        }
    }
    
    Tag::Value Tag::value() const
    {
        return _value;
    }
    bool Tag::getValue(Tag::Type type, Tag::Value& value) const
    {
        switch (_type)
        {
            case Null:
                break;
            case Digital:
                return changeDigitalValue(_value.bValue, type, value);
            case Integer16:
                return changeInt16Value(_value.sValue, type, value);
            case Integer32:
                return changeInt32Value(_value.nValue, type, value);
            case Integer64:
                return changeInt64Value(_value.lValue, type, value);
            case Float64:
                return changeFloat64Value(_value.dValue, type, value);
            case Text:
                return changeStringValue(_value.strValue, type, value);
            case Float32:
                return changeFloat32Value(_value.fValue, type, value);
            case Date:
                return changeDateValue(_value.tValue, type, value);
            default:
                break;
        }
        return false;
    }
    template<class T>
    bool Tag::getValue(T& v) const
    {
        return changeValueInner(type(), value(), v);
    }
    bool Tag::getValue(bool& value) const
    {
        return getValue<bool>(value);
    }
    bool Tag::getValue(short& value) const
    {
        return getValue<short>(value);
    }
    bool Tag::getValue(int& value) const
    {
        return getValue<int>(value);
    }
    bool Tag::getValue(int64_t& value) const
    {
        return getValue<int64_t>(value);
    }
    bool Tag::getValue(float& value) const
    {
        return getValue<float>(value);
    }
    bool Tag::getValue(double& value) const
    {
        return getValue<double>(value);
    }
    bool Tag::getValue(string& v) const
    {
        return changeStringValue(type(), value(), v);
    }
    bool Tag::getValue(DateTime& v) const
    {
        return changeDateValue(type(), value(), v);
    }
    
    bool Tag::compareValue(const Value& v) const
    {
        switch (type())
        {
            case Null:
                return false;
            case Digital:
                return _value.bValue == v.bValue;
            case Integer16:
                return _value.sValue == v.sValue;
            case Integer32:
                return _value.nValue == v.nValue;
            case Integer64:
                return _value.lValue == v.lValue;
            case Float32:
                return _value.fValue == v.fValue;
            case Float64:
                return _value.dValue == v.dValue;
            case Text:
                return strcmp(_value.strValue, v.strValue) == 0;
            case Date:
                return _value.tValue == v.tValue;
            default:
                return false;
        }
    }
    bool Tag::compareValue(const bool& v) const
    {
        return compareValue<bool>(type(), value(), v);
    }
    bool Tag::compareValue(const short& v) const
    {
        return compareValue<short>(type(), value(), v);
    }
    bool Tag::compareValue(const int& v) const
    {
        return compareValue<int>(type(), value(), v);
    }
    bool Tag::compareValue(const int64_t& v) const
    {
        return compareValue<int64_t>(type(), value(), v);
    }
    bool Tag::compareValue(const float& v) const
    {
        return compareValue<float>(type(), value(), v);
    }
    bool Tag::compareValue(const double& v) const
    {
        return compareValue<double>(type(), value(), v);
    }
    bool Tag::compareValue(const string& v) const
    {
        string str;
        if(changeStringValue(type(), value(), str))
        {
            return str == v;
        }
        return false;
    }
    bool Tag::compareValue(const DateTime& v) const
    {
        DateTime time;
        if(changeDateValue(type(), value(), time))
        {
            return time == v;
        }
        return false;
    }
    
    bool Tag::changeValue(const Tag* tag, Type destType, Value& destValue)
    {
        if(tag == NULL)
            return false;
        
        return changeValue(tag->type(), tag->value(), destType, destValue);
    }
    bool Tag::changeValue(Type type, const Value& value, Type destType, Value& destValue)
    {
        if(type == destType)
        {
            if(type == Text)
            {
                setStringValue(value.strValue, destType, destValue);
            }
            else
            {
                destValue = value;
            }
            return true;
        }
        else
        {
            switch (type)
            {
                case Null:
                    return false;
                case Digital:
                    return changeDigitalValue(value.bValue, destType, destValue);
                case Integer16:
                    return changeInt16Value(value.sValue, destType, destValue);
                case Integer32:
                    return changeInt32Value(value.nValue, destType, destValue);
                case Integer64:
                    return changeInt64Value(value.lValue, destType, destValue);
                case Float32:
                    return changeFloat32Value(value.fValue, destType, destValue);
                case Float64:
                    return changeFloat64Value(value.dValue, destType, destValue);
                case Text:
                    return changeStringValue(value.strValue, destType, destValue);
                case Date:
                    return changeDateValue(DateTime(value.lValue), destType, destValue);
                default:
                    return false;
            }
        }
        return false;
    }
    
    bool Tag::compareValue(Type type1, const Value& value1, Type type2, const Value& value2)
    {
        Value destValue;
        if(!changeValue(type1, value1, type2, destValue))
            return false;
        
        switch (type1)
        {
            case Null:
                return false;
            case Digital:
                return value1.bValue == destValue.bValue;
            case Integer16:
                return value1.sValue == destValue.sValue;
            case Integer32:
                return value1.nValue == destValue.nValue;
            case Integer64:
                return value1.lValue == destValue.lValue;
            case Float32:
                return value1.fValue == destValue.fValue;
            case Float64:
                return value1.dValue == destValue.dValue;
            case Text:
            {
                bool result = strcmp(value1.strValue, destValue.strValue) == 0;
                delete[] destValue.strValue;
                return result;
            }
            case Date:
                return value1.lValue == destValue.lValue;
            default:
                return false;
        }
        return true;
    }
    
    bool Tag::changeDigitalValue(bool v, Type type, Value& value)
    {
        switch (type)
        {
            case Null:
                return false;
            case Digital:
                value.bValue = v;
                break;
            case Integer16:
                value.sValue = v ? 1 : 0;
                break;
            case Integer32:
                value.nValue = v ? 1 : 0;
                break;
            case Integer64:
                value.lValue = v ? 1 : 0;
                break;
            case Float32:
                value.fValue = v ? 1.f : 0.f;
                break;
            case Float64:
                value.dValue = v ? 1.0 : 0.0;
                break;
            case Text:
                setStringValue(Convert::convertStr(v), type, value);
                break;
            case Date:
                return false;
            default:
                return false;
        }
        return true;
    }
    bool Tag::changeByteValue(byte v, Type type, Value& value)
    {
        return changeValueInner<byte>(v, type, value);
    }
    bool Tag::changeInt16Value(short v, Type type, Value& value)
    {
        return changeValueInner<short>(v, type, value);
    }
    bool Tag::changeInt32Value(int v, Type type, Value& value)
    {
        return changeValueInner<int>(v, type, value);
    }
    bool Tag::changeInt64Value(int64_t v, Type type, Value& value)
    {
        return changeValueInner<int64_t>(v, type, value);
    }
    bool Tag::changeFloat32Value(float v, Type type, Value& value)
    {
        return changeValueInner<float>(v, type, value);
    }
    bool Tag::changeFloat64Value(double v, Type type, Value& value)
    {
        return changeValueInner<double>(v, type, value);
    }
    bool Tag::changeStringValue(const string& v, Type type, Value& value)
    {
        return changeStringValue(v.c_str(), type,value);
    }
    bool Tag::changeStringValue(const char* v, Type type, Value& value)
    {
        if(v == NULL)
            return false;
        
        bool changed = false;
        switch (type)
        {
            case Null:
                changed = false;
                break;
            case Digital:
                if(Convert::parseBoolean(v, value.bValue))
                    changed = true;
                break;
            case Integer16:
                if(Convert::parseInt16(v, value.sValue))
                    changed = true;
                break;
            case Integer32:
                if(Convert::parseInt32(v, value.nValue))
                    changed = true;
                break;
            case Integer64:
                if(Convert::parseInt64(v, value.lValue))
                    changed = true;
                break;
            case Float32:
                if(Convert::parseSingle(v, value.fValue))
                    changed = true;
                break;
            case Float64:
                if(Convert::parseInt32(v, value.nValue))
                    changed = true;
                break;
            case Text:
                setStringValue(v, type, value);
                changed = true;
                break;
            case Date:
            {
                DateTime time;
                if (DateTime::parse(v, time))
                {
                    value.tValue = time.ticks();
                    changed = true;
                }
                break;
            }
            default:
                changed = false;
                break;
        }
        return changed;
    }
    bool Tag::changeDateValue(const DateTime& v, Type type, Value& value)
    {
        bool changed = false;
        switch (type)
        {
            case Null:
                changed = false;
                break;
            case Digital:
                changed = false;
                break;
            case Integer16:
                changed = false;
                break;
            case Integer32:
                changed = false;
                break;
            case Integer64:
                changed = false;
                break;
            case Float32:
                changed = false;
                break;
            case Float64:
                changed = false;
                break;
            case Text:
                setStringValue(v.toString(), type, value);
                changed = true;
                break;
            case Date:
                value.tValue = v.ticks();
                changed = true;
                break;
            default:
                changed = false;
                break;
        }
        return changed;
    }
    
    bool Tag::changeDigitalValue(Type type, const Value& value, bool& v)
    {
        return changeValueInner<bool>(type, value, v);
    }
    bool Tag::changeByteValue(Type type, const Value& value, byte& v)
    {
        return changeValueInner<byte>(type, value, v);
    }
    bool Tag::changeInt16Value(Type type, const Value& value, short& v)
    {
        return changeValueInner<short>(type, value, v);
    }
    bool Tag::changeInt32Value(Type type, const Value& value, int& v)
    {
        return changeValueInner<int>(type, value, v);
    }
    bool Tag::changeInt64Value(Type type, const Value& value, int64_t& v)
    {
        return changeValueInner<int64_t>(type, value, v);
    }
    bool Tag::changeFloat32Value(Type type, const Value& value, float& v)
    {
        return changeValueInner<float>(type, value, v);
    }
    bool Tag::changeFloat64Value(Type type, const Value& value, double& v)
    {
        return changeValueInner<double>(type, value, v);
    }
    bool Tag::changeStringValue(Type type, const Value& value, string& v)
    {
        bool changed = true;
        switch (type)
        {
            case Null:
                changed = false;
                break;
            case Digital:
                v = Convert::convertStr(value.bValue);
                break;
            case Integer16:
                v = Convert::convertStr(value.sValue);
                break;
            case Integer32:
                v = Convert::convertStr(value.nValue);
                break;
            case Integer64:
                v = Convert::convertStr(value.lValue);
                break;
            case Float32:
                v = Convert::convertStr(value.fValue);
                break;
            case Float64:
                v = Convert::convertStr(value.dValue);
                break;
            case Text:
                v = value.strValue;
                break;
            case Date:
            {
                DateTime time(value.lValue);
                v = time.toString();
                break;
            }
            default:
                changed = false;
                break;
        }
        return changed;
    }
    bool Tag::changeDateValue(Type type, const Value& value, DateTime& v)
    {
        bool changed = false;
        switch (type)
        {
            case Null:
                changed = false;
                break;
            case Digital:
                changed = false;
                break;
            case Integer16:
                changed = false;
                break;
            case Integer32:
                changed = false;
                break;
            case Integer64:
                changed = false;
                break;
            case Float32:
                changed = false;
                break;
            case Float64:
                changed = false;
                break;
            case Text:
            {
                DateTime time;
                if (DateTime::parse(value.strValue, time))
                {
                    v = time;
                    changed = true;
                }
                break;
            }
            case Date:
                v = DateTime(value.tValue);
                changed = true;
                break;
            default:
                changed = false;
                break;
        }
        return changed;
    }
    
    double Tag::toAnalogValue(Type type, const Value& value)
    {
        if(isAnalogValue(type))
        {
            double result = 0.0;
            changeValueInner<double>(type, value, result);
            return result;
        }
        return 0.0;
    }
    Tag::Value Tag::fromAnalogValue(Type type, double value)
    {
        Value result;
        result.nValue = 0;
        changeValueInner<double>(value, type, result);
        return result;
    }
    
    template<class T>
    bool Tag::changeValueInner(T v, Type type, Value& value)
    {
        switch (type)
        {
            case Null:
                return false;
            case Digital:
                value.bValue = v != 0;
                break;
            case Integer16:
                value.sValue = (short)v;
                break;
            case Integer32:
                value.nValue = (int)v;
                break;
            case Integer64:
                value.lValue = (int64_t)v;
                break;
            case Float32:
                value.fValue = (float)v;
                break;
            case Float64:
                value.dValue = (double)v;
                break;
            case Text:
                setStringValue(Convert::convertStr(v), type, value);
                break;
            case Date:
                return false;
            default:
                return false;
        }
        return true;
    }
    
    template<class T>
    bool Tag::changeValueInner(Type type, const Value& value, T& v)
    {
        switch (type)
        {
            case Null:
                return false;
            case Digital:
                v = (T)value.bValue;
                break;
            case Integer16:
                v = (T)value.sValue;
                break;
            case Integer32:
                v = (T)value.nValue;
                break;
            case Integer64:
                v = (T)value.lValue;
                break;
            case Float32:
                v = (T)value.fValue;
                break;
            case Float64:
                v = (T)value.dValue;
                break;
            case Text:
            {
                if(Convert::parseStr(value.strValue, v))
                {
                    return true;
                }
                return false;
            }
            case Date:
                return false;
            default:
                return false;
        }
        return true;
    }
    
    void Tag::valueChanged()
    {
        //Debug::writeLine("Tag::valueChanged");
        _valueChangedDelegates.invoke(this);
    }
    
    Delegates* Tag::valueChangedDelegates()
    {
        return &_valueChangedDelegates;
    }
    
    string Tag::typeStr() const
    {
        return toTypeStr(_type);
    }
    
    DateTime Tag::timestamp() const
    {
        return _timestamp;
    }
    Tag::Qualitystamp Tag::qualitystamp() const
    {
        return _qualitystamp;
    }
    
    void Tag::writeValue(Stream* stream, Tag::Type type, const Tag::Value& value)
    {
        switch (type)
        {
            case Null:
                break;
            case Digital:
                stream->writeBoolean(value.bValue);
                break;
            case Integer16:
                stream->writeInt16(value.sValue);
                break;
            case Integer32:
                stream->writeInt32(value.nValue);
                break;
            case Integer64:
                stream->writeInt64(value.lValue);
                break;
            case Float32:
                stream->writeFloat(value.fValue);
                break;
            case Float64:
                stream->writeDouble(value.dValue);
                break;
            case Text:
                stream->writeStr(value.strValue);
                break;
            case Date:
                stream->writeUInt64(value.tValue);
                break;
            default:
                break;
        }
    }
    void Tag::readValue(Stream* stream, Tag::Type type, Tag::Value& value)
    {
        switch (type)
        {
            case Null:
                break;
            case Digital:
                value.bValue = stream->readBoolean();
                break;
            case Integer16:
                value.sValue = stream->readInt16();
                break;
            case Integer32:
                value.nValue = stream->readInt32();
                break;
            case Integer64:
                value.lValue = stream->readInt64();
                break;
            case Float32:
                value.fValue = stream->readFloat();
                break;
            case Float64:
                value.dValue = stream->readDouble();
                break;
            case Text:
                setStringValue(stream->readStr(), Text, value);
                break;
            case Date:
                value.tValue = stream->readUInt64();
                break;
            default:
                break;
        }
    }
    int Tag::valueSize(Type type, const Value& value)
    {
        int size = 0;
        switch (type)
        {
            case Null:
                size = 0;
                break;
            case Digital:
                size = sizeof(value.bValue);
                break;
            case Integer16:
                size = sizeof(value.sValue);
                break;
            case Integer32:
                size = sizeof(value.nValue);
                break;
            case Integer64:
                size = sizeof(value.lValue);
                break;
            case Float32:
                size = sizeof(value.fValue);
                break;
            case Float64:
                size = sizeof(value.dValue);
                break;
            case Text:
                size = sizeof(short) + (int)strlen(value.strValue);
                break;
            case Date:
                size = sizeof(value.tValue);
                break;
            default:
                break;
        }
        return size;
    }
    
    string Tag::toTypeStr(Type type)
    {
        switch (type)
        {
            case Null:
                return "Null";
            case Digital:
                return "Digital";
            case Integer16:
                return "Integer16";
            case Integer32:
                return "Integer32";
            case Integer64:
                return "Integer64";
            case Float32:
                return "Float32";
            case Float64:
                return "Float64";
            case Text:
                return "Text";
            case Date:
                return "Date";
            default:
                return "Null";
        }
    }
    Tag::Type Tag::fromTypeStr(const string& str)
    {
        if(String::stringEquals(str, "Digital") ||
           String::stringEquals(str, "Boolean"))
        {
            return Type::Digital;
        }
        else if(String::stringEquals(str, "Int16") ||
                String::stringEquals(str, "Short") ||
                String::stringEquals(str, "Integer16"))
        {
            return Type::Integer16;
        }
        else if(String::stringEquals(str, "Int32") ||
                String::stringEquals(str, "Integer32") ||
                String::stringEquals(str, "Integer"))
        {
            return Type::Integer32;
        }
        else if(String::stringEquals(str, "Integer64"))
        {
            return Type::Integer64;
        }
        else if(String::stringEquals(str, "Float32") ||
                String::stringEquals(str, "Single") ||
                String::stringEquals(str, "Float"))
        {
            return Type::Float32;
        }
        else if(String::stringEquals(str, "Float64") ||
                String::stringEquals(str, "Double"))
        {
            return Type::Float64;
        }
        else if(String::stringEquals(str, "Text"))
        {
            return Type::Text;
        }
        else if(String::stringEquals(str, "Date"))
        {
            return Type::Date;
        }
        return Type::Null;
    }
    bool Tag::isValidType(Type type)
    {
        return type != Type::Null;
    }
    
    bool Tag::isAnalogValue(Type type)
    {
        return (type == Tag::Integer16 || type == Tag::Integer32 || type == Tag::Integer64 ||
                type == Tag::Float32 || type == Tag::Float64);
    }
    
    void Tag::write(Stream* stream, bool bigEndian) const
    {
        stream->writeStr(_name, 1);
        stream->writeUInt32(_id, bigEndian);
        stream->writeByte(_type);

        switch (_type)
        {
            case Null:
                break;
            case Digital:
                stream->writeBoolean(_value.bValue);
                break;
            case Integer16:
                stream->writeInt16(_value.sValue, bigEndian);
                stream->writeInt16(_minValue.sValue, bigEndian);
                stream->writeInt16(_maxValue.sValue, bigEndian);
                break;
            case Integer32:
                stream->writeInt32(_value.nValue, bigEndian);
                stream->writeInt32(_minValue.nValue, bigEndian);
                stream->writeInt32(_maxValue.nValue, bigEndian);
                break;
            case Integer64:
                stream->writeInt64(_value.lValue, bigEndian);
                stream->writeInt64(_minValue.lValue, bigEndian);
                stream->writeInt64(_maxValue.lValue, bigEndian);
                break;
            case Float32:
                stream->writeFloat(_value.fValue);
                stream->writeFloat(_minValue.fValue);
                stream->writeFloat(_maxValue.fValue);
                break;
            case Float64:
                stream->writeDouble(_value.dValue);
                stream->writeDouble(_minValue.dValue);
                stream->writeDouble(_maxValue.dValue);
                break;
            case Text:
                stream->writeStr(_value.strValue);
                break;
            case Date:
            {
                DateTime time(_value.tValue);
                time.write(stream);
                DateTime minTime(_minValue.tValue);
                minTime.write(stream);
                DateTime maxTime(_maxValue.tValue);
                maxTime.write(stream);
            }
                break;
            default:
                break;
        }
        
        _timestamp.write(stream);
        stream->writeUInt16(_qualitystamp);
    }
    void Tag::read(Stream* stream, bool bigEndian)
    {
        _name = stream->readStr(1);
        _id = stream->readUInt32(bigEndian);
        _type = (Type)stream->readByte();
        
        switch (_type)
        {
            case Null:
                break;
            case Digital:
                _value.bValue = stream->readBoolean();
                break;
            case Integer16:
                _value.sValue = stream->readInt16(bigEndian);
                _minValue.sValue = stream->readInt16(bigEndian);
                _maxValue.sValue = stream->readInt16(bigEndian);
                break;
            case Integer32:
                _value.nValue = stream->readInt32(bigEndian);
                _minValue.nValue = stream->readInt32(bigEndian);
                _maxValue.nValue = stream->readInt32(bigEndian);
                break;
            case Integer64:
                _value.lValue = stream->readInt64(bigEndian);
                _minValue.lValue = stream->readInt64(bigEndian);
                _maxValue.lValue = stream->readInt64(bigEndian);
                break;
            case Float32:
                _value.fValue = stream->readFloat();
                _minValue.fValue = stream->readFloat();
                _maxValue.fValue = stream->readFloat();
                break;
            case Float64:
                _value.dValue = stream->readDouble();
                _minValue.dValue = stream->readDouble();
                _maxValue.dValue = stream->readDouble();
                break;
            case Text:
                setStringValue(stream->readStr());
                break;
            case Date:
            {
                DateTime time;
                time.read(stream);
                _value.tValue = time.ticks();
                time.read(stream);
                _minValue.tValue = time.ticks();
                time.read(stream);
                _maxValue.tValue = time.ticks();
            }
                break;
            default:
                break;
        }
        
        _timestamp.read(stream);
        _qualitystamp = (Qualitystamp)stream->readUInt16(bigEndian);
    }
    void Tag::copyFrom(const Tag* value)
    {
        _name = value->_name;
        _id = value->_id;
        _type = value->_type;
        _value = value->_value;
        _minValue = value->_minValue;
        _maxValue = value->_maxValue;
        _timestamp = value->_timestamp;
        _qualitystamp = value->_qualitystamp;
    }
    
    bool Tag::IsGoodQuality(Tag::Qualitystamp qualitystamp)
    {
        return qualitystamp == Tag::Qualitystamp::Good;
    }
    
    string Tag::toValueString(Type type, const Value& value)
    {
        string str;
        if(Tag::changeStringValue(type, value, str))
            return str;
        return "";
    }
    bool Tag::parseValueString(const string& str, Type type, Value& value)
    {
        return Tag::changeStringValue(str, type, value);
    }
}
