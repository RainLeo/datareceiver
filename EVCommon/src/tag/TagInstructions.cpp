﻿#include "TagInstructions.h"
#include "TagService.h"

namespace rtdb
{
    TagInstructionSet::TagInstructionSet(TagService* service)
    {
        if(service == nullptr)
            throw ArgumentNullException("serivice");
        
        _service = service;
    }
    TagInstructionSet::~TagInstructionSet()
    {
    }    
    void TagInstructionSet::generateInstructions(Instructions* instructions)
    {
        instructions->add(new SendTagAInstruction(new InstructionDescription("SendTagA", new TagAContext())));
        instructions->add(new ReceiveTagAInstruction(new InstructionDescription("ReceiveTagA", new TagAContext()), _service));
    }
    
    ClientContext* ReceiveTagAInstruction::setValue(ClientContext* context)
    {
        TagAContext* tc = dynamic_cast<TagAContext*>(context);
        if(tc != NULL)
        {
            TagService* ts = _service;
            if(ts != nullptr)
            {
                const Tag* inputTag = tc->inputData();
                Tag* tag = ts->getTag(inputTag->id());
                if(tag != nullptr)
                {
                    tag->setValue(inputTag->value());
                }
            }
        }
        return context;
    }
}
