#ifndef TAG_H
#define TAG_H

#include <string>
#include <mutex>
#include "common/common_global.h"
#include "common/system/DateTime.h"
#include "common/data/Vector.h"
#include "common/system/Delegate.h"
#include "common/system/Convert.h"
#include "common/thread/Locker.h"

using namespace std;
using namespace Common;

namespace rtdb
{
    class COMMON_EXPORT Tag
    {
    public:
        union Value
        {
            bool bValue;
            short sValue;
            int nValue;
            int64_t lValue;
            float fValue;
            double dValue;
            char* strValue;
            uint64_t tValue;	// 100-nanosecond ticks
        };
        enum Type : byte
        {
            Null = 0,
            Digital,
            Integer16,
            Integer32,
            Integer64,
            Float32,
            Float64,
            Text,
            Date,
        };
        enum Qualitystamp : ushort
        {
            Good = 192,
            Bad = 0,
            InitValue = 0xFFFF
        };
        
        Tag(Type type = Null, uint tagId = InvalidTagId, const string& tagName = "");
        Tag(Type type, uint tagId, const string& tagName, const Value& minValue, const Value& maxValue);
        ~Tag();
        
        bool isEmpty() const;
        
        void operator=(const Tag& tag);
        bool operator==(const Tag& tag) const;
        bool operator!=(const Tag& tag) const;
        
        void operator=(const Value& value);
        bool operator==(const Value& value) const;
        bool operator!=(const Value& value) const;
        
        void operator=(const bool& value);
        operator bool() const;
        bool operator==(const bool& value) const;
        bool operator!=(const bool& value) const;
        
        void operator=(const short& value);
        operator short() const;
        bool operator==(const short& value) const;
        bool operator!=(const short& value) const;
        
        void operator=(const int& value);
        operator int() const;
        bool operator==(const int& value) const;
        bool operator!=(const int& value) const;
        
        void operator=(const int64_t& value);
        operator int64_t() const;
        bool operator==(const int64_t& value) const;
        bool operator!=(const int64_t& value) const;
        
        void operator=(const float& value);
        operator float() const;
        bool operator==(const float& value) const;
        bool operator!=(const float& value) const;
        
        void operator=(const double& value);
        operator double() const;
        bool operator==(const double& value) const;
        bool operator!=(const double& value) const;
        
        void operator=(const string& value);
        operator string() const;
        bool operator==(const string& value) const;
        bool operator!=(const string& value) const;
        
        void operator=(const DateTime& value);
        operator DateTime() const;
        bool operator==(const DateTime& value) const;
        bool operator!=(const DateTime& value) const;
        
        bool isNullValue() const;
        void setNullValue();
        
        Type type() const;
        const string& name() const;
        uint id() const;
        
        bool isAnalogValue() const;
        
        void setValue(const Value& v);
        bool setValue(const bool& v);
        bool setValue(const short& v);
        bool setValue(const int& v);
        bool setValue(const int64_t& v);
        bool setValue(const float& v);
        bool setValue(const double& v);
        bool setValue(const string& v);
        bool setValue(const DateTime& v);
        void setValue(const Value& v, Qualitystamp qualitystamp, DateTime timestamp);
        bool setValue(const bool& v, Qualitystamp qualitystamp, DateTime timestamp);
        bool setValue(const short& v, Qualitystamp qualitystamp, DateTime timestamp);
        bool setValue(const int& v, Qualitystamp qualitystamp, DateTime timestamp);
        bool setValue(const int64_t& v, Qualitystamp qualitystamp, DateTime timestamp);
        bool setValue(const float& v, Qualitystamp qualitystamp, DateTime timestamp);
        bool setValue(const double& v, Qualitystamp qualitystamp, DateTime timestamp);
        bool setValue(const string& v, Qualitystamp qualitystamp, DateTime timestamp);
        bool setValue(const DateTime& v, Qualitystamp qualitystamp, DateTime timestamp);
        
        void setMinValue(const Value& v);
        void setMaxValue(const Value& v);
        
        Value value() const;
        bool getValue(Type type, Value& v) const;
        bool getValue(bool& v) const;
        bool getValue(short& v) const;
        bool getValue(int& v) const;
        bool getValue(int64_t& v) const;
        bool getValue(float& v) const;
        bool getValue(double& v) const;
        bool getValue(string& v) const;
        bool getValue(DateTime& v) const;
        
        bool compareValue(const Value& v) const;
        bool compareValue(const bool& v) const;
        bool compareValue(const short& v) const;
        bool compareValue(const int& v) const;
        bool compareValue(const int64_t& v) const;
        bool compareValue(const float& v) const;
        bool compareValue(const double& v) const;
        bool compareValue(const string& v) const;
        bool compareValue(const DateTime& v) const;
        
        void write(Stream* stream, bool bigEndian = true) const;
        void read(Stream* stream, bool bigEndian = true);
        void copyFrom(const Tag* value);
        
        Delegates* valueChangedDelegates();
        
        string typeStr() const;
        
        DateTime timestamp() const;
        Qualitystamp qualitystamp() const;
        
    public:
        static bool isNullValue(const Tag& tag);
        
        static bool isValueEqual(Type type, const Value& value1, const Value& value2);
        
        static string toTypeStr(Type type);
        static Type fromTypeStr(const string& str);
        static bool isValidType(Type type);
        
        static bool isAnalogValue(Type type);
        
        static bool changeValue(const Tag* tag, Type destType, Value& destValue);
        static bool changeValue(Type type, const Value& value, Type destType, Value& destValue);
        
        static bool compareValue(Type type1, const Value& value1, Type type2, const Value& value2);
        template<class T>
        static bool compareValue(Type type, const Value& value, const T& v)
        {
            return false; // do not compare value to support same value acquisition, added by HYC 2015/9/16
            switch (type)
            {
                case Null:
                    return false;
                case Digital:
                {
                    bool temp = v != 0 ? true : false;
                    return value.bValue == temp;
                }
                case Integer16:
                    return value.sValue == (short)v;
                case Integer32:
                    return value.nValue == (int)v;
                case Integer64:
                    return value.lValue == (int64_t)v;
                case Float32:
                    return value.fValue == (float)v;
                case Float64:
                    return value.dValue == (double)v;
                case Text:
                    return strcmp(value.strValue, Convert::convertStr(v).c_str()) == 0;
                case Date:
                    return false;
                default:
                    return false;
            }
            return true;
        }

        static bool changeDigitalValue(bool v, Type type, Value& value);
        static bool changeByteValue(byte v, Type type, Value& value);
        static bool changeInt16Value(short v, Type type, Value& value);
        static bool changeInt32Value(int v, Type type, Value& value);
        static bool changeInt64Value(int64_t v, Type type, Value& value);
        static bool changeFloat64Value(double v, Type type, Value& value);
        static bool changeFloat32Value(float v, Type type, Value& value);
        static bool changeStringValue(const char* v, Type type, Value& value);
        static bool changeStringValue(const string& v, Type type, Value& value);
        static bool changeDateValue(const DateTime& v, Type type, Value& value);
        
        static bool changeDigitalValue(Type type, const Value& value, bool& v);
        static bool changeByteValue(Type type, const Value& value, byte& v);
        static bool changeInt16Value(Type type, const Value& value, short& v);
        static bool changeInt32Value(Type type, const Value& value, int& v);
        static bool changeInt64Value(Type type, const Value& value, int64_t& v);
        static bool changeFloat32Value(Type type, const Value& value, float& v);
        static bool changeFloat64Value(Type type, const Value& value, double& v);
        static bool changeStringValue(Type type, const Value& value, string& v);
        static bool changeDateValue(Type type, const Value& value, DateTime& v);
        
        static double toAnalogValue(Type type, const Value& value);
        static Value fromAnalogValue(Type type, double value);
        
        static void writeValue(Stream* stream, Type type, const Value& value);
        static void readValue(Stream* stream, Type type, Value& value);
        static int valueSize(Type type, const Value& value);
        
        static bool IsGoodQuality(Qualitystamp qualitystamp);
        
        static string toValueString(Type type, const Value& value);
        static bool parseValueString(const string& str, Type type, Value& value);
        
    private:
        void setStringValue(const string& str);
        void setStringValue(const char* str);
        static void setStringValue(const string& str, Type type, Value& value);
        static void setStringValue(const char* str, Type type, Value& value);
        
        void valueChanged();
        
        void setValueInner(const Value& value);
        void setValueInner(const Value& value, Qualitystamp qualitystamp, DateTime timestamp);
        
        template<class T>
        bool getValue(T& v) const;
        
    private:
        template<class T>
        static bool changeValueInner(T v, Type type, Value& value);
        template<class T>
        static bool changeValueInner(Type type, const Value& value, T& v);
       
    private:
        string _name;
        uint _id;
        Type _type;
        Value _value;
        Value _minValue;
        Value _maxValue;
        DateTime _timestamp;
        Qualitystamp _qualitystamp;
        
        Delegates _valueChangedDelegates;
        
    private:
        const static int InvalidTagId = -1;
    };
    
    class COMMON_EXPORT Tags : public Vector<Tag>
    {
    public:
        Tags(bool autoDelete = true, uint capacity = Vector<Tag>::DefaultCapacity) : Vector<Tag>(autoDelete, capacity)
        {
        }
        
        inline void addTag(const Tag* tag)
        {
            uint tagId = tag->id();
            if(tagId > 0)
                Vector<Tag>::set(tagId - 1, tag, true);
        }
        inline Tag* getTag(uint tagId) const
        {
            if(tagId > 0 && tagId <= count())
                return at(tagId - 1);
            return NULL;
        }
        inline Tag* getTag(const string& tagName) const
        {
            for (uint i=0; i<count(); i++)
            {
                Tag* tag = at(i);
                if(tag != NULL &&
                   String::stringEquals(tag->name(), tagName, true))
                {
                    return tag;
                }
            }
            return NULL;
        }
        inline bool containsTag(int tagId) const
        {
            return getTag(tagId) != NULL;
        }
    };
}

#endif	// TAG_H
